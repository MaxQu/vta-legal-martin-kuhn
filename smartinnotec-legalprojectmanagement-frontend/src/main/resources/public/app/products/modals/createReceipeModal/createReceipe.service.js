(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('createReceipeModalService', createReceipeModalService);
        
    createReceipeModalService.$inject = ['$modal', '$stateParams', 'productsService', 'receipeService', 'documentFileService', 'utilService', 'projectTemplateService'];
    
    function createReceipeModalService($modal, $stateParams, productsService, receipeService, documentFileService, utilService, projectTemplateService) {
		var service = {
			showCreateReceipeModal: showCreateReceipeModal
		};		
		return service;
		
		////////////
				
		function showCreateReceipeModal(currentUser, product, products, receipe, allReceipesOfProduct, userAuthorizationUserContainer, unityTypes) {
			 var createReceipeModal = $modal.open({
				controller: CreateReceipeModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	receipe: function() {
			    		return receipe;
			    	},
			    	allReceipesOfProduct: function() {
			    		return allReceipesOfProduct;
			    	}, 
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	unityTypes: function() {
			    		return unityTypes;
			    	}
			    }, 
				templateUrl: 'app/products/modals/createReceipeModal/createReceipe.modal.html'
			});
			return createReceipeModal;
			
			function CreateReceipeModalController($modalInstance, $scope, product, products, receipe, allReceipesOfProduct, userAuthorizationUserContainer, unityTypes) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.receipe = receipe;
		    	vm.allReceipesOfProduct = allReceipesOfProduct;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.unityTypes = unityTypes;

		    	vm.updateProduct = updateProduct;
		    	vm.createDocumentUrl = createDocumentUrl;
		    	vm.showTemplateModal = showTemplateModal;
		    	vm.closeCreateReceipeModal = closeCreateReceipeModal;  
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	function updateProduct(productId) {
		    		productsService.findProduct(productId).then(function(response) {
		    			var product = response.data;
		    			if(vm.products != null) {
			    			for(var i = 0; i < vm.products.length; i++) {
			    				if(vm.products[i].id == productId) {
			    					var selected = vm.products[i].selected;
			    					vm.products[i] = product;
			    					vm.products[i].selected = selected;
			    					break;
			    				}
			    			}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.service#updateProduct'); 
		  	   		 });
		    	}
		    	
		    	function createDocumentUrl(user, product, receipeAttachment) {		    	
		    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
		    		return 'api/filedownloads/filedownload/' + user.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
		    	}
		    	
		    	function showTemplateModal(url, receipeAttachment) {
		    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
		    			var documentFile = response.data;
		    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
		    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#getProductsByTerm'); 
		  	   		 });
		    	}
		    	
		    	function closeCreateReceipeModal() {
	    			vm.receipe.productId = '';
	    			vm.receipe.date = new Date();
	    			vm.receipe.receipeProducts = [];
	    			vm.receipe.information = '';
	    			vm.receipe.receipeAttachments = [];
	    			vm.receipe.receipeType = '';
	    			vm.receipe.receipeAdditionals = null;
	    			
					$modalInstance.dismiss('cancel'); 
				}
			} 
		}
    }
})();