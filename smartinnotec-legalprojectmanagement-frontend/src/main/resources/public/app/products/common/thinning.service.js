(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('thinningService', thinningService);
        
    thinningService.$inject = ['$http', 'api_config'];
    
    function thinningService($http, api_config) {
		var service = {
			createThinning: createThinning,
			updateThinning: updateThinning,
			findThinningsOfProductInRange: findThinningsOfProductInRange,
			findLastThinningOfProduct: findLastThinningOfProduct,
			findAllThinningsOfProduct: findAllThinningsOfProduct,
			deleteThinning: deleteThinning
		};
		
		return service;
		
		////////////
		
		function createThinning(thinning) {
			return $http.post(api_config.BASE_URL + '/thinnings/thinning', thinning);
		}
		
		function updateThinning(thinning) {
			return $http.put(api_config.BASE_URL + '/thinnings/thinning', thinning);
		}
		
		function findThinningsOfProductInRange(productId, start, end) {
			return $http.get(api_config.BASE_URL + '/thinnings/thinning/' + productId + '/' + start + '/' + end + '/');
		}
		
		function findLastThinningOfProduct(productId) {
			return $http.get(api_config.BASE_URL + '/thinnings/thinning/' + productId + '/last');
		}
		
		function findAllThinningsOfProduct(productId) {
			return $http.get(api_config.BASE_URL + '/thinnings/thinning/' + productId);
		}
		
		function deleteThinning(thinningId) {
			return $http.delete(api_config.BASE_URL + '/thinnings/thinning/' + thinningId);
		}
    }
})();
