(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.directive('registrationDirective', registrationDirective);
    
    registrationDirective.$inject = ['$timeout'];
    
	function registrationDirective($timeout) {
		var directive = {
			restrict: 'E',
			scope: {
				invoker: '=',
				mandator: '='
			},
			templateUrl: 'app/home/directives/registrationDirective/registration.html',
			link: function($scope) {
				
				$scope.signupError = {};
				
				$scope.user = {
						username: '',
						password: '',
						title: '',
						firstname: '',
						surname: '',
						sex: 'FEMALE',
						birthdate: new Date(),
						email: '',
						confession: 'ROEM_CATH',
						telephone: '', 
						mandators: [],
						address: {
							country: 'austria',
							postalCode: '',
							region: '',
							street: ''
						}
					};
				$scope.openedDatePicker = false;
				
				$scope.unsetUserValues = function() {
					$scope.user.username = '';
					$scope.user.password = '';
					$scope.user.title = '';
					$scope.user.firstname = '';
					$scope.user.surname = '';
					$scope.user.tenant.name = '';
					$scope.user.sex = 'FEMALE';
					$scope.user.email = '';
					$scope.user.telephone = '';
					$scope.user.address.country = 'austria';  
					$scope.user.address.postalCode = ''; 
					$scope.user.address.region = ''; 
					$scope.user.address.street = ''; 
				};
				
				$scope.signup = function(user) {
					$scope.openedDatePicker = false;
					if($scope.validateUserObject(user) === false) {
						return;
					}
					$scope.invoker.signup(user, function() {
						$scope.unsetUserValues();
					}, function(response) {
						$scope.signupError.message = response.data.error;
						$scope.setTimeout();
					});
				};
				
				$scope.openDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
				};
				
				$scope.setTimeout = function() {
		      		$timeout(function() {
		      			$scope.usernameError = false;
		      			$scope.passwordError = false;
		      			$scope.firstnameError = false;
		      			$scope.surnameError = false;
		      			$scope.emailError = false;
		      			$scope.tenantError = false;
		      			$scope.signupError = {};
		  			   }, 2000); 
		      	};
				
				$scope.validateUserObject = function(user) { 
					if(user.username == null || user.username === '') {
						$scope.usernameError = true;
						$scope.setTimeout();
						return false;
					} else if(user.password == null || user.password === '') {
						$scope.passwordError = true;
						$scope.setTimeout();
						return false;
					} else if(user.firstname == null || user.firstname === '') {
						$scope.firstnameError = true;
						$scope.setTimeout();
						return false;
					} else if(user.surname == null || user.surname === '') {
						$scope.surnameError = true;
						$scope.setTimeout();
						return false;
					} else if(user.tenant == null) { 
						$scope.tenantError = true; 
						$scope.setTimeout();
						return false;
					}
					return true;
				};
			 }
		};
		return directive;
		
		////////////
	}
})();