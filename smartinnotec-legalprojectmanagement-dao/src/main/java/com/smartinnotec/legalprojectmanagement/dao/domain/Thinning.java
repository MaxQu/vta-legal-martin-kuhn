package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "Thinning")
public @Data class Thinning implements Comparable<Thinning> {

    @Id
    private String id;
    private String productId;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime date;

    private Float ironContent; // fe-Gehalt
    private Float targetContent; // zielgehalt
    private Float density; // Dichte
    private Float amountLiter; // Menge in Liter
    private Float amount; // Menge

    private Float iron;
    private Float water;
    private boolean polymer;
    private boolean ascorbinSaeure;

    private Integer polymerSubtract;
    private Float polymerPromille;
    private Integer ascorbinSaeureSubtract;

    private Float waterAmountCalculated;

    private String informationTop;
    private String information;

    private String labelLeft1;
    private String labelLeft2;
    private String labelRight1;

    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime creationDate;
    @DBRef
    private Tenant tenant;

    public Thinning() {
        labelLeft1 = "Fe- Gehalt";
        labelLeft2 = "Fe- Gehalt";
        labelRight1 = "Eisenlösung";
    }

    @Override
    public int compareTo(final Thinning thinning) {
        return this.date.compareTo(thinning.date);
    }
}
