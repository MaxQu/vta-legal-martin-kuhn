package com.smartinnotec.legalprojectmanagement.serializer;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class TimeToDateTimeDeserializer extends JsonDeserializer<DateTime> {

    @Override
    public DateTime deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("HH:mm");

        final String date = jsonparser.getText();
        final DateTime dateTime = dateFormatter.parseDateTime(date);
        return dateTime;
    }
}
