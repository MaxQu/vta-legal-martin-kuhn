(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('communicationMessageService', communicationMessageService);
        
    communicationMessageService.$inject = ['$modal', '$stateParams', '$timeout', 'communicationService'];
    
    function communicationMessageService($modal, $stateParams, $timeout, communicationService) {
		var service = {
			showCommunicationMessageModal: showCommunicationMessageModal
		};		
		return service;
		
		////////////
				
		function showCommunicationMessageModal(communicationUserConnectionSent, type) {
			 var communicationMessageModal = $modal.open({
				controller: CommunicationMessageModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	communicationUserConnectionSent: function() {
			    		return communicationUserConnectionSent;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/communicationMessage/communicationMessage.modal.html'
			});
			return communicationMessageModal;
			
			function CommunicationMessageModalController($modalInstance, $scope, communicationUserConnectionSent, type) {
		    	var vm = this; 
		    	vm.communicationUserConnectionSent = communicationUserConnectionSent;
		    	vm.type = type;
		    	vm.readUpdateSuccessful = false;
		    	vm.executedUpdateSuccessful = false;
		    	
		    	vm.updateCommunicationUserConnection = updateCommunicationUserConnection;
		    	vm.closeCommunicationMessageModal = closeCommunicationMessageModal;
		    	
		    	////////////
		    	
		    	function updateCommunicationUserConnection(communicationUserConnection, updateType) {
		    		communicationService.updateCommunicationUserConnection(communicationUserConnection).then(function successCallback(response) {
		    			if(updateType == 'READ') {
		    				vm.readUpdateSuccessful = true;
		    			} else if(updateType == 'EXECUTED') {
		    				vm.executedUpdateSuccessful = true;
		    			}
		    			setUpdateCommunicationUserConnectionTimeout();
		    			console.log('update of communicationUserConnection successful in communicationMessage.service.js#updateCommunicationUserConnection');
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error in communicationMessage.service.js#updateCommunicationUserConnection');
		  	   		 });
		    	}
		    	
		    	function setUpdateCommunicationUserConnectionTimeout() {
		      		$timeout(function() {
		      			vm.readUpdateSuccessful = false;
		      			vm.executedUpdateSuccessful = false;
		  			   }, 2000); 
		      	}
		    	
		    	function closeCommunicationMessageModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();