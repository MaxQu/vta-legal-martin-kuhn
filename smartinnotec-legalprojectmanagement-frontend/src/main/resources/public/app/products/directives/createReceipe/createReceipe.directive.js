(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products') 
    	.directive('createReceipeDirective', createReceipeDirective);
    
    createReceipeDirective.$inject = ['$timeout', 'productsService', 'documentFileService', 'utilService', 'receipeService', 'dateUtilityService', 'projectTemplateService', 'contactsService', 'printingPDFService', 'productOverviewModalService', 'printModalService'];
    
	function createReceipeDirective($timeout, productsService, documentFileService, utilService, receipeService, dateUtilityService, projectTemplateService, contactsService, printingPDFService, productOverviewModalService, printModalService) {
		var directive = {
			restrict: 'E',
			scope: {
				index: '=',
				currentUser: '=',
				receipe: '=',
				product: '=',
				invoker: '=',
				type: '=',
				receipeType: '=',
				userAuthorizationUserContainer: '=',
				unityTypes: '='
			},
			templateUrl: '/app/products/directives/createReceipe/createReceipe.html',
			link: function($scope) {
				
				$scope.receipe.date = $scope.receipe.date instanceof Date ? $scope.receipe.date : dateUtilityService.convertToDateTimeOrUndefined($scope.receipe.date);
				$scope.selectedContact = $scope.receipe.contact; 
				$scope.createReceipeDateDatePicker = false;
				$scope.percentageResultIs100 = false;
				$scope.amountInKiloError = false;
				$scope.addAdditionalsToProductTrigger = $scope.receipe.receipeAdditionals != null ? true : false;
				$scope.receipeDateTime = $scope.receipe.date;
				
				$scope.setReceipeTimeout = function(receipe) {
		      		$timeout(function() {
		      			$scope.receipe.updated = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.setTimeout = function() {  
		      		$timeout(function() {
		      			$scope.amountInKiloError = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.setContactTimeout = function(receipe) { 
		      		$timeout(function() {
		      			if($scope.receipe.contact != null) {
		      				$scope.receipe.contact.contactSelected = false;
		      			}
		      			$scope.receipe.contactTextSelected = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.moveUp = function(index) {
		      		$scope.receipe.receipeProducts.splice(index-1, 0, $scope.receipe.receipeProducts.splice(index, 1)[0]);
		      	};
		      	
		      	$scope.moveDown = function(index) {
		      		$scope.receipe.receipeProducts.splice(index+1, 0, $scope.receipe.receipeProducts.splice(index, 1)[0]);
		      	};
				
				$scope.openCreateReceipeDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.createReceipeDateDatePicker = true; 
				};
				
				$scope.getProductName = function(receipeProduct) { 
					if(receipeProduct == null || receipeProduct.product == null) { 
						return '';
					}
					return receipeProduct.product.name;
				};
				
				$scope.showProductOverviewModalService = function(product) {
		    		productsService.findProduct(product.id).then(function(response) {
		    			productOverviewModalService.showProductOverviewModal(response.data, $scope);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error createReceipe.directive#showProductOverviewModalService');
		 	   		 });
		    	};
				
				$scope.getProductsByTerm = function(term) {
					return productsService.findProductsByTerm(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#getProductsByTerm'); 
		  	   		 });
				};
				
				$scope.productSelected = function(selectedReceipeProduct, index) {
					var addedProduct = false;
		    		for(var i = 0; i < $scope.receipe.receipeProducts.length; i++) {
		    			if(Object.keys($scope.receipe.receipeProducts[i]).length === 0) {
		    				var receipeProduct = {};
		    				receipeProduct.product = selectedReceipeProduct;
		    				receipeProduct.percentage = null;
		    				$scope.receipe.receipeProducts[i] = receipeProduct;
		    				addedProduct = true;
		    				break;
		    			}
		    		}
		    		if(addedProduct == false) {
		    			$scope.receipe.receipeProducts[index].product = selectedReceipeProduct;
		    			$scope.receipe.receipeProducts[index].density = selectedReceipeProduct.density;
		    		}
		    	};
		    	
		    	$scope.contactSelected = function(receipe) { 
		    		$scope.receipe.contactText = null;
		    		$scope.receipe.contact = receipe.contactOrContactText;
		    		$scope.receipe.contact.contactSelected = true;
		    		$scope.setContactTimeout($scope.receipe);
		    	};
		    	
		    	$scope.setContactText = function(receipe) {
		    		$scope.receipe.contactText = receipe.contactOrContactText;
		    		$scope.receipe.contactTextSelected = true;
		    		$scope.setContactTimeout($scope.receipe);
		    		$scope.receipe.contact = null;
		    	};
		    	
		    	$scope.checkIfPercentageResultIs100 = function(receipeProducts) {
		    		if(receipeProducts == null) { 
		    			receipeProducts.percentageResult = 0;
		    			$scope.percentageResultIs100 = false;
		    			return;
		    		} 
		    		var result = 0;
		    		for(var i = 0; i < receipeProducts.length; i++) {
		    			if(receipeProducts[i].percentage != null) {
		    				result += receipeProducts[i].percentage;
		    				receipeProducts[i].errorPercentage = false;
		    			}
		    		}
		    		var resultInteger = parseFloat(result).toFixed(2);
		    		if(resultInteger == parseFloat(100).toFixed(2)) {
		    			receipeProducts.percentageResult = resultInteger;
		    			$scope.percentageResultIs100 = true;
		    			return;
		    		}
		    		receipeProducts.percentageResult = resultInteger;
		    		$scope.percentageResultIs100 = false;
		    	};
		    	$scope.checkIfPercentageResultIs100($scope.receipe.receipeProducts);
		    	
		    	$scope.addReceipeProduct = function() { 
		    		var receipeProduct = {}; 
		    		$scope.receipe.receipeProducts.push(receipeProduct);
		    	};
		    	
		    	$scope.additionalProductSelected = function(selectedReceipeProduct, index) {
		    		$scope.receipe.receipeAdditionals[index].product = selectedReceipeProduct;
		    		$scope.receipe.receipeAdditionals[index].productError = false;
		    		$scope.calculateAllReceipeAdditionals();
		    	};
		    	
		    	$scope.addReceipeAdditional = function() {
		    		var receipeAdditional = {};
		    		receipeAdditional.product = null;
		    		receipeAdditional.unityType = null;
		    		receipeAdditional.amount = 0;
		    		if($scope.receipe.receipeAdditionals == null) {
		    			$scope.receipe.receipeAdditionals = []; 
		    		}
		    		$scope.receipe.receipeAdditionals.push(receipeAdditional);
		    	};
		    	
		    	$scope.calculateAllReceipeAdditionals = function() {
		    		if($scope.receipe.receipeAdditionals != null) {
			    		for(var i = 0; i < $scope.receipe.receipeAdditionals.length; i++) {
			    			$scope.calculateReceipeAdditional($scope.receipe.receipeAdditionals[i], i);
			    		}
		    		}
		    	};
		    	
		    	$scope.calculateReceipeAdditional = function(receipeAdditional, index) {
		    		var amountInKilo = $scope.receipe.amountInKilo;
		    		if(amountInKilo == null) {
		    			receipeAdditional.result = null;
		    			$scope.amountInKiloError = true;
		    			$scope.setTimeout();
		    			return;
		    		}
		    		
		    		var product = $scope.receipe.receipeAdditionals[index].product;
	    			if(product == null) {
	    				console.log('product not set in createReceipe.directive#calculateReceipeAdditional');
	    				receipeAdditional.result = null;
	    				receipeAdditional.productError = true;
	    				return;
	    			} else {
	    				receipeAdditional.productError = false;
	    			}
		    		
		    		$scope.amountInKiloError = false;
		    		var kiloGramPerTon = null;
		    		var result = null;
		    		switch(receipeAdditional.unityType) {
		    		case 'GRAM':
		    			kiloGramPerTon = receipeAdditional.amount * 0.001;
		    			result = kiloGramPerTon * amountInKilo;
		    			receipeAdditional.result = result;
		    			break;
		    		case 'LITER':		    			
		    			kiloGramPerTon = receipeAdditional.amount * 0.001;
		    			result = kiloGramPerTon * amountInKilo;
		    			receipeAdditional.result = result;
		    			break;
		    		}
		    	};
		    	
		    	$scope.removeReceipeProduct = function(index) {
		    		$scope.receipe.receipeProducts.splice(index, 1);
		    		for(var i = 0; i < $scope.receipe.receipeProducts.length; i++) {
		    			$scope.receipe.receipeProducts[i].selectedReceipeProduct = $scope.getProductName($scope.receipe.receipeProducts[i]);
		    		}
		    		$scope.checkIfPercentageResultIs100($scope.receipe.receipeProducts);
		    	};
		    	
		    	$scope.removeReceipeAdditional = function(index) {
		    		$scope.receipe.receipeAdditionals.splice(index, 1);
		    		for(var i = 0; i < $scope.receipe.receipeAdditionals.length; i++) {
		    			$scope.receipe.receipeAdditionals[i].selectedReceipeAdditional = $scope.getProductName($scope.receipe.receipeAdditionals[i]);
		    		}
		    		if($scope.receipe.receipeAdditionals.length == 0) {
		    			$scope.receipe.receipeAdditionals = null;
		    			$scope.addAdditionalsToProductTrigger = false;
		    		}
		    	};
		    	
		    	$scope.attachFile = function(selected, selectedDocumentFile, documentFileVersion) {
		    		if(!selected) {
		    			for(var i = 0; i < $scope.receipe.receipeAttachments.length; i++) {
		    				if($scope.receipe.receipeAttachments[i].filename == selectedDocumentFile.fileName && $scope.receipe.receipeAttachments[i].version == documentFileVersion.version) {
		    					$scope.receipe.receipeAttachments.splice(i, 1);
		    					break;
		    				}
		    			}
		    		} else {
		    			var receipeAttachment = {};
		    			receipeAttachment.documentFileId = selectedDocumentFile.id;
		    			receipeAttachment.filepath = selectedDocumentFile.subFilePath + '/' + selectedDocumentFile.documentFileVersions[0].filePathName;
		    			receipeAttachment.filename = selectedDocumentFile.fileName;
		    			receipeAttachment.ending = selectedDocumentFile.ending;
		    			receipeAttachment.version = documentFileVersion.version;
		    			receipeAttachment.originalFilename = selectedDocumentFile.fileName;
		    			receipeAttachment.isImage = utilService.isImage(selectedDocumentFile.ending);
		    			$scope.receipe.receipeAttachments.push(receipeAttachment);
		    		}
		    	};
		    	
		    	$scope.removeFile = function(index) {
		    		$scope.receipe.receipeAttachments.splice(index, 1);
		    	};
		    	
		    	$scope.findDocumentsByTerm = function(term) {
					return documentFileService.findDocumentFilesByBySearchStringAndProjectIdAndFolderId(term, $scope.product.id, '').then(function(response) {
						var documentFiles = response.data;
						for(var i = 0; i < documentFiles.length; i++) {
							documentFiles[i].vm = $scope;
						}
			    		return documentFiles;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#findDocumentsByTerm'); 
		  	   		 });
				};
				
				$scope.createReceipe = function() {
					$scope.receipe.productId = $scope.product.id;
					$scope.receipe.receipeType = 'STANDARD_MIXTURE';
					Date.prototype.toJSON = function () { return this.toLocaleString(); };
					$scope.receipe.date = dateUtilityService.joinDateObjectsToDateTimeObject($scope.receipe.date, $scope.receipeDateTime);
		    		receipeService.createReceipe($scope.receipe).then(function(response) {
		    			$scope.resetReceipe();
		    			$scope.invoker.closeCreateReceipeModal();
		    			$scope.invoker.updateProduct($scope.product.id);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#createReceipe'); 
		  	   		 });
		    	};
		    	
		    	$scope.changeReceipe = function() {
		    		receipeService.updateReceipe($scope.receipe).then(function(response) {
		    			$scope.receipe.updated = true;
		    			$scope.setReceipeTimeout($scope.receipe);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#createReceipe'); 
		  	   		 });
		    	};
		    	
		    	$scope.deleteReceipe = function(receipe, canDeleteReceipeOrMixture) {
		    		if(canDeleteReceipeOrMixture == false) {
		    			return;
		    		}
		    		receipeService.deleteReceipe($scope.receipe).then(function(response) {
		    			$scope.invoker.deleteReceipe(receipe);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#deleteReceipe'); 
		  	   		 });
		    	};
		    	
		    	$scope.calculateKilogramAndLiterOfAllProducts = function(receipe) {
		    		for(var i = 0; i < receipe.receipeProducts.length; i++) {
		    			$scope.calculateKilogramAndLiter(receipe, receipe.receipeProducts[i]);
		    		}
		    	};
		    	
		    	$scope.calculateKilogramAndLiter = function(receipe, receipeProduct) {
		    		if($scope.receipeType == 'CHANGED_MIXTURE') {
		    			var amountInKilo = receipe.amountInKilo;
		    			if(amountInKilo == null) {
		    				$scope.amountInKiloError = true;
		    				$scope.setTimeout();
		    				return;
		    			} else {
		    				// kilogram = (amountInKilo * percentage) / 100;
		    				// liter = kilogram / density;
		    				var density = receipeProduct.density;
		    				var percentage = receipeProduct.percentage;
		    				var kilogram = (amountInKilo * percentage) / 100;
		    				var liter = kilogram / density;
		    				receipeProduct.kilogram = kilogram;
		    				receipeProduct.liter = liter;
		    			}
		    		}
		    	};
		    	
		    	$scope.resetReceipe = function() { 
		    		$scope.receipe.productId = '';
		    		$scope.receipe.date = new Date();
		    		$scope.receipe.receipeProducts = [];
		    		$scope.receipe.information = '';
		    		$scope.receipe.receipeAttachments = [];
		    		$scope.receipe.receipeType = '';
		    		$scope.receipe.receipeAdditionals = null;
		    	};
		    	
		    	$scope.getContactsByTerm = function(term) {
					return contactsService.findContactBySearchString(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#getContactsByTerm'); 
		  	   		 });
				};
		    	
		    	$scope.createDocumentUrl = function(user, product, receipeAttachment) {		    	
		    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
		    		return 'api/filedownloads/filedownload/' + user.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
		    	};
		    	
		    	$scope.showTemplateModal = function(url, receipeAttachment) {
		    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
		    			var documentFile = response.data;
		    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
		    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#showTemplateModal'); 
		  	   		 });
		    	};
		    	
		    	$scope.findLastReceipesOfProductOfType = function(receipeProduct, receipeType) { 
		    		receipeService.findLastReceipesOfProductOfType(receipeProduct.product.id, receipeType).then(function(response) {
		    			$scope.toolTipProductInfo = response.data; 
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#findLastReceipesOfProductOfType'); 
		  	   		 });
		    	};
		    	
		    	$scope.printContainer = function(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	};
		    	
		    	$scope.showPrintModal = function(receipe) {
		    		printModalService.showPrintModal($scope.product, receipe, $scope.receipeType, $scope.currentUser);
		    	};
		    	
		    	$scope.closeCreateReceipeModal = function() {   
		    		$scope.resetReceipe();
		    		$scope.invoker.closeCreateReceipeModal();
		    	};
				
			}
		};
		return directive;
		
		////////////
	}
})();
