package com.smartinnotec.legalprojectmanagement.restservice;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectDataSheet;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;
import com.smartinnotec.legalprojectmanagement.restservice.container.ProjectLogoNameContainer;
import com.smartinnotec.legalprojectmanagement.service.CalendarEventService;
import com.smartinnotec.legalprojectmanagement.service.FileHandlingService;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.ProjectDataSheetService;
import com.smartinnotec.legalprojectmanagement.service.ProjectLogoHandlingBean;
import com.smartinnotec.legalprojectmanagement.service.ProjectService;
import com.smartinnotec.legalprojectmanagement.service.ProjectUserConnectionService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class ProjectRestService extends AbstractRestService {

    private static final String PROJECT_START;
    private static final String PROJECT_END;

    static {
        PROJECT_START = "Projektstart";
        PROJECT_END = "Projektende";
    }

    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProjectDataSheetService projectDataSheetService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private CalendarEventService calendarEventService;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;
    @Autowired
    private ProjectLogoHandlingBean projectLogoHandlingBean;
    @Autowired
    private FileHandlingService fileHandlingService;

    @RequestMapping(value = "/projects/project/{createcalendarentry}", method = {
        RequestMethod.POST }, consumes = "application/json;charset=utf-8")
    public Project create(final @PathVariable("createcalendarentry") Boolean createcalendarentry, final @RequestBody Project project)
            throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in ProjectRestService#create", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": creation of project with name '{}' in ProjectRestService#create",
            project.getName());

        // set tenant of user
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        final Tenant tenant = userService.getTenantOfUser(super.getLoggedInUserId());
        project.setTenant(tenant);
        // set creation date of project
        project.setCreationDate(new DateTime(System.currentTimeMillis()));
        final Project createdProject = projectService.create(project);
        logger.debug("created project with id '{}' in ProjectRestService#create", createdProject.getId());

        // create calendar entry
        if (createcalendarentry) {
            calendarEventService.createCalendarEventsForProject(createdProject, user);
        }
        final String message = createdProject.getName();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.CREATED_PROJECT)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ProjectRestService#create", createdHistory.getId());

        return createdProject;
    }

    @RequestMapping(value = "/projects/project/projectlogo/upload/{userid}", method = RequestMethod.POST)
    public @ResponseBody ProjectLogoNameContainer uploadProjectLogo(@PathVariable(value = "userid")
    final String userId, @RequestParam("file") MultipartFile multipartFile) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in ProjectRestService#uploadProjectLogo", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": handle project logo image upload in ProjectRestService#uploadProjectLogo");
        final String originalFilename = multipartFile.getOriginalFilename();

        final String[] fileNameParts = originalFilename.split("\\.");
        final String ending = fileNameParts[fileNameParts.length - 1];
        final String fileName = fileNameParts[0] + "_" + dateTimeFormatterWithoutSeparator.print(System.currentTimeMillis()) + "." + ending;

        if (!multipartFile.isEmpty()) {
            try {
                final String folderName = projectLogoHandlingBean.getProjectLogoFolderPrefixName();
                final String dirName = projectLogoHandlingBean.getProjectLogoUploadPath() + File.separator + folderName;
                final String absolutePath = fileHandlingService.storeFile(dirName, fileName, multipartFile);
                logger.info("project logo image uploaded to path '{}' in ProjectRestService#uploadProjectLogo", absolutePath);
                final ProjectLogoNameContainer projectLogoNameContainer = new ProjectLogoNameContainer();
                projectLogoNameContainer.setLogoName(fileName);
                return projectLogoNameContainer;
            } catch (Exception e) {
                logger.error("error '{}' in ProjectRestService#uploadProjectLogo: " + e.getMessage());
            }
        }
        return null;
    }

    @RequestMapping(value = "/projects/project/checkuniqueprojectname", method = { RequestMethod.GET }, produces = "application/json")
    public @ResponseBody Boolean checkUniqueProjectName(final @RequestParam String projectName) throws BusinessException {
        BusinessAssert.notNull(projectName, "projectName is mandatory in ProjectRestService#checkUniqueProjectName", "400");
        logger.debug(
            getInvokedRestServiceUserInformation() + ": check if projectName is unique in ProjectRestService#checkUniqueProjectName");
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);

        final List<Project> projects = projectService.findAllProjectsByTenant(user.getTenant());
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            if (projectUserConnection.getProject().getName().equals(projectName)) {
                logger.info("projectName '{}' is not unique in ProjectRestService#checkUniqueProjectName", projectName);
                return false; // projectName is not unique
            }
        }
        logger.info("projectName '{}' is unique in ProjectRestService#checkUniqueProjectName", projectName);
        return true;
    }

    @RequestMapping(value = "/projects/project/all", method = { RequestMethod.GET }, produces = "application/json")
    public @ResponseBody List<Project> getAllProjectsOfTenant() throws BusinessException {
        logger.debug(getInvokedRestServiceUserInformation() + ": check if username is unique in ProjectRestService#getAllProjectsOfTenant");
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        final List<Project> projects = projectService.findAllProjectsByTenant(user.getTenant());
        return projects;
    }

    @RequestMapping(value = "/projects/project/projectdatasheet/{projectid}", method = { RequestMethod.GET })
    public ProjectDataSheet findProjectDataSheet(final @PathVariable("projectid") String projectId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in ProjectRestService#findProjectDataSheet", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in ProjectRestService#findProjectDataSheet", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": find projectDataSheet of project with id '{}' in ProjectRestService#findProjectDataSheet", projectId);
        final ProjectDataSheet projectDataSheet = projectDataSheetService.findProjectDataSheetByProjectId(projectId);
        return projectDataSheet;
    }

    @RequestMapping(value = "/projects/project/projectdatasheet", method = {
        RequestMethod.POST }, consumes = "application/json;charset=utf-8")
    public ProjectDataSheet create(final @RequestBody ProjectDataSheet projectDataSheet) throws BusinessException {
        BusinessAssert.notNull(projectDataSheet, "projectDataSheet is mandatory in ProjectRestService#create", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": create projectDataSheet in ProjectRestService#create");
        return projectDataSheetService.create(projectDataSheet);
    }

    @RequestMapping(value = "/projects/project/projectdatasheet", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody ProjectDataSheet update(@RequestBody
    final ProjectDataSheet projectDataSheet) throws BusinessException {
        BusinessAssert.notNull(projectDataSheet, "projectDataSheet is mandatory in ProjectRestService#updateProjectDataSheet", "400");
        logger.info("update projectDataSheet with id '{}' in ProjectRestService#update", projectDataSheet.getId());
        return projectDataSheetService.update(projectDataSheet);
    }

    @RequestMapping(value = "/projects/project/projectdatasheet/{projectid}", method = RequestMethod.DELETE)
    public void deleteProjectDataSheet(final @PathVariable(value = "projectid") String projectId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in ProjectRestService#deleteProjectDataSheet", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in ProjectRestService#deleteProjectDataSheet", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": delete projectDataSheet of project with id '{}' in ProjectRestService#deleteProjectDataSheet", projectId);
        projectDataSheetService.delete(projectId);
    }

    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @RequestMapping(value = "/projects/project/default/{userid}", method = { RequestMethod.GET })
    public Project getDefaultProject(final @PathVariable("userid") String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in ProjectRestService#getDefaultProject", "400");
        BusinessAssert.isId(userId, "userId must be an id in ProjectRestService#getDefaultProject", "400");
        logger.info(
            getInvokedRestServiceUserInformation() + ": get default project of user with id '{}' in ProjectRestService#getDefaultProject",
            userId);
        final User user = userService.findUser(userId);
        final List<ProjectUserConnection> projectUserConnectionsOfUser = projectUserConnectionService.findByUserAndActive(user, true);
        final Optional<ProjectUserConnection> projectUserConnection = projectUserConnectionsOfUser.stream()
            .filter(s -> s.getDefaultProject() != null && s.getDefaultProject() == true).findFirst();
        if (projectUserConnection.isPresent()) {
            return projectUserConnection.get().getProject();
        }
        return null;
    }

    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @RequestMapping(value = "/projects/project/{projectid}", method = { RequestMethod.GET })
    public Project findProject(final @PathVariable("projectid") String projectId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in ProjectRestService#findProject", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in ProjectRestService#findProject", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": get project with id '{}' in ProjectRestService#findProject", projectId);
        final Project project = projectService.findProject(projectId);
        return project;
    }

    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @RequestMapping(value = "/projects/project/user/{userid}", method = { RequestMethod.GET })
    public List<Project> findProjectsOfUser(final @PathVariable("userid") String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in ProjectRestService#findProjectsOfUser", "400");
        BusinessAssert.isId(userId, "userId must be an id in ProjectRestService#findProjectsOfUser", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": get projects of user with id '{}' in ProjectRestService#findProjectsOfUser",
            userId);
        final User user = userService.findUser(userId);
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);
        final List<Project> projectsOfUser = new ArrayList<>();
        projectUserConnections.stream().forEach(s -> projectsOfUser.add(s.getProject()));
        final List<Project> projectsGeneralAvailable = projectService.findAllGeneralAvailableProjects();
        Optional.ofNullable(projectsGeneralAvailable).ifPresent(projectsOfUser::addAll);
        return projectsOfUser;
    }

    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @RequestMapping(value = "/projects/project/generalaccess", method = { RequestMethod.GET })
    public List<Project> findProjectsGeneralAvailable() throws BusinessException {
        logger.info("find projects general available in ProjectRestService#findProjectsGeneralAvailable");
        final List<Project> projectsGeneralAvailable = projectService.findAllGeneralAvailableProjects();
        return projectsGeneralAvailable;
    }

    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @RequestMapping(value = "/projects/project", method = { RequestMethod.GET })
    public List<Project> findAllProjects() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": find all projects in ProjectRestService#findProject");
        final Tenant tenant = userService.getTenantOfUser(super.getLoggedInUserId());
        if (tenant == null) {
            return new ArrayList<>();
        }
        final List<Project> projects = projectService.findAllProjectsByTenant(tenant);
        return projects;
    }

    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @RequestMapping(value = "/projects/project/{searchstring}/search", method = { RequestMethod.GET })
    public List<Project> findProjectBySearchString(final @PathVariable("searchstring") String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in ProjectRestService#findProjectByTerm", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": get projects by search string '{}' in ProjectRestService#findProject",
            searchString);
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        final List<Project> foundedProjects = projectService.findProjectByTerm(searchString);

        final String message = searchString;
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.SEARCHED_PROJECT)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ProjectRestService#findProjectBySearchString", createdHistory.getId());

        return foundedProjects;
    }

    @RequestMapping(value = "/projects/project", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody Project update(@RequestBody
    final Project project) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in ProjectRestService#update");
        logger.info(getInvokedRestServiceUserInformation() + ": update project with id '{}' in ProjectRestService#update", project.getId());
        final Project updatedProject = projectService.update(project);
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final CalendarEvent calendarEventStart = calendarEventService.findByLocationAndProject(PROJECT_START, updatedProject);
        if (calendarEventStart != null) {
            calendarEventStart.setTitle(updatedProject.getName());
            calendarEventStart.setStartsAt(updatedProject.getStart().withHourOfDay(8).getMillis());
            calendarEventStart.setEndsAt(updatedProject.getStart().withHourOfDay(9).getMillis());
            final List<CalendarEvent> updatedCalendarEventsStart = calendarEventService.update(calendarEventStart, user);
            final CalendarEvent updatedCalendarEventStart = updatedCalendarEventsStart != null && !updatedCalendarEventsStart.isEmpty()
                ? updatedCalendarEventsStart.get(0)
                : null;
            logger.info("updated calendarEvent start with id '{}' of project with id '{}' in ProjectRestService#update",
                updatedCalendarEventStart != null ? updatedCalendarEventStart.getId() : "--", updatedProject.getId());
        }
        final CalendarEvent calendarEventEnd = calendarEventService.findByLocationAndProject(PROJECT_END, updatedProject);
        if (calendarEventEnd != null) {
            calendarEventEnd.setTitle(updatedProject.getName());
            calendarEventEnd.setStartsAt(updatedProject.getEnd().withHourOfDay(8).getMillis());
            calendarEventEnd.setEndsAt(updatedProject.getEnd().withHourOfDay(9).getMillis());
            final List<CalendarEvent> updatedCalendarEventsEnd = calendarEventService.update(calendarEventEnd, user);
            final CalendarEvent updatedCalendarEventEnd = updatedCalendarEventsEnd != null && !updatedCalendarEventsEnd.isEmpty()
                ? updatedCalendarEventsEnd.get(0)
                : null;
            logger.info("updated calendarEvent end with id '{}' of project with id '{}' in ProjectRestService#update",
                updatedCalendarEventEnd != null ? updatedCalendarEventEnd.getId() : "--", updatedProject.getId());
        }

        final String message = updatedProject.getName();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.UPDATED_PROJECT)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("updated history with id '{}' in ProjectRestService#update", createdHistory.getId());
        return updatedProject;
    }

    @RequestMapping(value = "/projects/project/projectpagesize", method = { RequestMethod.GET })
    public Integer getProjectPageSize() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": get page amount in ProjectRestService#getProjectPageSize");
        return projectService.getProjectPageSize();
    }

    @RequestMapping(value = "/projects/project/{projectid}", method = RequestMethod.DELETE)
    public void deleteProject(final @PathVariable(value = "projectid") String projectId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in ProjectRestService#deleteProject", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in ProjectRestService#deleteProject", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": delete project with id '{}' in ProjectRestService#deleteProject",
            projectId);
        final String username = super.getLoggedInUsername();
        final String userId = super.getLoggedInUserId();

        projectService.deleteProject(projectId, userId, username);
    }

    @Override
    public String toString() {
        return "[ProjectRestService]";
    }
}
