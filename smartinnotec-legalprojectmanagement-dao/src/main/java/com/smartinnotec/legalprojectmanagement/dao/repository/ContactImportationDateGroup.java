package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

public @Data class ContactImportationDateGroup implements Comparable<ContactImportationDateGroup> {

    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime importationDate;
    private long total;

    @Override
    public int compareTo(final ContactImportationDateGroup contactImportationDateGroup) {
        return this.getImportationDate().compareTo(contactImportationDateGroup.getImportationDate());
    }

}
