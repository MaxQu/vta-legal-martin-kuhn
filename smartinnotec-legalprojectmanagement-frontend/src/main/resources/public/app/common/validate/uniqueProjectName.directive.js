(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('uniqueProjectName', uniqueProjectName);
    
    uniqueProjectName.$inject = ['validateService'];
    
	function uniqueProjectName(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////
		
		function link(scope, element, attrs, ngModel) {
			if(attrs.uniqueProjectName == 'CREATE') {
				ngModel.$parsers.unshift(parser);
				ngModel.$formatters.unshift(formatter);
			}
			
			////////////
			
			function parser(value) { 
				if(value.length > 3) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProjectName(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 3) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProjectName(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();