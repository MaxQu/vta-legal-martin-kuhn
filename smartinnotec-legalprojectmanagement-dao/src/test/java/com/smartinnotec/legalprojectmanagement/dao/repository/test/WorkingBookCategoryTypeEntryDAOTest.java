package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEntry;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;

public class WorkingBookCategoryTypeEntryDAOTest extends AbstractDAOTest {

    @Test
    public void shouldCreateWorkingBookCategoryTypeEntry() {
        final Tenant tenant = new Tenant();
        tenant.setName("TestTenant3");
        final Tenant createdTenant = tenantRepository.insert(tenant);
        Assert.assertNotNull(createdTenant);

        final WorkingBookCategoryTypeEntry workingBookCategoryTypeEntry = new WorkingBookCategoryTypeEntry();
        workingBookCategoryTypeEntry.setTenant(createdTenant);
        workingBookCategoryTypeEntry.setWorkingBookCategoryType(WorkingBookCategoryTypeEnum.TAEGLICH);
        workingBookCategoryTypeEntry.setWorkingBookCategoryText("Text1 Control");
        final WorkingBookCategoryTypeEntry createdWorkingBookCategoryTypeEntry = workingBookCategoryTypeEntryRepository
            .insert(workingBookCategoryTypeEntry);
        Assert.assertNotNull(createdWorkingBookCategoryTypeEntry);
    }

}
