package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.joda.time.DateTime;

import lombok.Getter;

public class UserBuilder {

    private @Getter final String title;
    private @Getter final String firstname;
    private @Getter final String surname;
    private @Getter final DateTime birthdate;
    private @Getter final SexEnum sex;
    private @Getter final String telephone;
    private @Getter final String email;
    private @Getter final boolean emailActive;
    private @Getter final String username;
    private @Getter final String password;
    private @Getter final String postalCode;
    private @Getter final String street;
    private @Getter final String region;
    private @Getter final CountryTypeEnum country;
    private @Getter final boolean active;
    private @Getter final String color;
    private @Getter DateTime startRecording;
    private @Getter ConfessionEnum confession;
    private @Getter UserRoleEnum userRole;
    private @Getter String profileImagePath;
    private @Getter List<String> mandators;
    private @Getter Tenant tenant;
    private @Getter List<String> productIds;
    private @Getter List<String> projectUserConnectionIds;
    private @Getter List<AgentNumberContainer> agentNumberContainers;
    private @Getter String colorOfProducts;
    private @Getter String colorOfProjects;
    private @Getter String colorOfCalendarEvents;
    private @Getter String colorOfReceivedOrders;
    private @Getter String colorOfSentOrders;
    private @Getter String colorOfContactsImported;
    private @Getter boolean allContactsAvailable;

    private UserBuilder(final Builder builder) {
        this.title = builder.title;
        this.firstname = builder.firstname;
        this.surname = builder.surname;
        this.birthdate = builder.birthdate;
        this.sex = builder.sex;
        this.telephone = builder.telephone;
        this.email = builder.email;
        this.emailActive = builder.emailActive;
        this.username = builder.username;
        this.password = builder.password;
        this.postalCode = builder.postalCode;
        this.street = builder.street;
        this.region = builder.region;
        this.country = builder.country;
        this.active = builder.active;
        this.color = builder.color;
        this.startRecording = builder.startRecording;
        this.confession = builder.confession;
        this.userRole = builder.userRole;
        this.profileImagePath = builder.profileImagePath;
        this.tenant = builder.tenant;
        this.productIds = builder.productIds;
        this.projectUserConnectionIds = builder.projectUserConnectionIds;
        this.agentNumberContainers = builder.agentNumberContainers;
        this.colorOfProducts = builder.colorOfProducts;
        this.colorOfProjects = builder.colorOfProjects;
        this.colorOfCalendarEvents = builder.colorOfCalendarEvents;
        this.colorOfReceivedOrders = builder.colorOfReceivedOrders;
        this.colorOfSentOrders = builder.colorOfSentOrders;
        this.colorOfContactsImported = builder.colorOfContactsImported;
        this.allContactsAvailable = builder.allContactsAvailable;
    }

    public static class Builder {

        private String title;
        private String firstname;
        private String surname;
        private DateTime birthdate;
        private SexEnum sex;
        private String username;
        private String password;
        private String telephone;
        private String email;
        private boolean emailActive;
        private String postalCode;
        private String street;
        private String region;
        private CountryTypeEnum country;
        private boolean active;
        private String color;
        private DateTime startRecording;
        private ConfessionEnum confession;
        private UserRoleEnum userRole;
        private String profileImagePath;
        private Tenant tenant;
        private List<String> productIds;
        private List<String> projectUserConnectionIds;
        private List<AgentNumberContainer> agentNumberContainers;
        private String colorOfProducts;
        private String colorOfProjects;
        private String colorOfCalendarEvents;
        private String colorOfReceivedOrders;
        private String colorOfSentOrders;
        private String colorOfContactsImported;
        private boolean allContactsAvailable;

        public Builder setTitle(final String title) {
            this.title = title;
            return this;
        }

        public Builder setFirstname(final String firstname) {
            this.firstname = firstname;
            return this;
        }

        public Builder setSurname(final String surname) {
            this.surname = surname;
            return this;
        }

        public Builder setSex(final SexEnum sex) {
            this.sex = sex;
            return this;
        }

        public Builder setTelephone(final String telephone) {
            this.telephone = telephone;
            return this;
        }

        public Builder setEmail(final String email) {
            this.email = email;
            return this;
        }

        public Builder setEmailActive(final boolean emailActive) {
            this.emailActive = emailActive;
            return this;
        }

        public Builder setUsername(final String username) {
            this.username = username;
            return this;
        }

        public Builder setPassword(final String password) {
            this.password = password;
            return this;
        }

        public Builder setPostalcode(final String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public Builder setStreet(final String street) {
            this.street = street;
            return this;
        }

        public Builder setRegion(final String region) {
            this.region = region;
            return this;
        }

        public Builder setCountry(final CountryTypeEnum country) {
            this.country = country;
            return this;
        }

        public Builder setActive(final boolean active) {
            this.active = active;
            return this;
        }

        public Builder setColor(final String color) {
            this.color = color;
            return this;
        }

        public Builder setStartRecording(final DateTime startRecording) {
            this.startRecording = startRecording;
            return this;
        }

        public Builder setUserRole(final UserRoleEnum userRole) {
            this.userRole = userRole;
            return this;
        }

        public Builder setProfileImagePath(final String profileImagePath) {
            this.profileImagePath = profileImagePath;
            return this;
        }

        public Builder setTenant(final Tenant tenant) {
            this.tenant = tenant;
            return this;
        }

        public Builder setPostalCode(final String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public Builder setProductIds(final List<String> productIds) {
            this.productIds = productIds;
            return this;
        }

        public Builder setProjectUserConnectionIds(final List<String> projectUserConnectionIds) {
            this.projectUserConnectionIds = projectUserConnectionIds;
            return this;
        }

        public Builder setBirthdate(final DateTime birthdate) {
            this.birthdate = birthdate;
            return this;
        }

        public Builder setConfession(final ConfessionEnum confession) {
            this.confession = confession;
            return this;
        }

        public Builder setAgentNumberContainers(final List<AgentNumberContainer> agentNumberContainers) {
            this.agentNumberContainers = agentNumberContainers;
            return this;
        }

        public Builder setColorOfProducts(final String colorOfProducts) {
            this.colorOfProducts = colorOfProducts;
            return this;
        }

        public Builder setColorOfProjects(final String colorOfProjects) {
            this.colorOfProjects = colorOfProjects;
            return this;
        }

        public Builder setColorOfCalendarEvents(final String colorOfCalendarEvents) {
            this.colorOfCalendarEvents = colorOfCalendarEvents;
            return this;
        }

        public Builder setColorOfReceivedOrders(final String colorOfReceivedOrders) {
            this.colorOfReceivedOrders = colorOfReceivedOrders;
            return this;
        }

        public Builder setColorOfSentOrders(final String colorOfSentOrders) {
            this.colorOfSentOrders = colorOfSentOrders;
            return this;
        }

        public Builder setColorOfContactsImported(final String colorOfContactsImported) {
            this.colorOfContactsImported = colorOfContactsImported;
            return this;
        }

        public void setAllContactsAvailable(final boolean allContactsAvailable) {
            this.allContactsAvailable = allContactsAvailable;
        }

        public UserBuilder build() {
            return new UserBuilder(this);
        }
    }
}
