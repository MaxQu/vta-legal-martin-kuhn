(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();