package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.LabelsPredefined;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.repository.LabelsPredefinedRepository;

@Service("labelsPredefinedService")
public class LabelsPredefinedServiceImpl extends AbstractService implements LabelsPredefinedService {

    @Autowired
    private LabelsPredefinedRepository labelsPredefinedRepository;

    public LabelsPredefinedServiceImpl() {
    }

    @Override
    public List<LabelsPredefined> getAllLabelsPredefinedByTenant(final Tenant tenant) throws BusinessException {
        logger.info("get all labels predefined in LabelsPredefinedServiceImpl#getAllLabelsPredefined");
        return labelsPredefinedRepository.findByTenant(tenant);
    }

    @Override
    public Set<LabelsPredefined> findLabelsBySearchString(final String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in LabelsPredefinedServiceImpl#findLabelsBySearchString", "400");
        logger.info("find labels by search string '{}' in LabelsPredefinedServiceImpl#findLabelsBySearchString", searchString);
        return labelsPredefinedRepository.findLabelsPredefinedByNameStartsWith(searchString);
    }

    @Override
    public LabelsPredefined findLabelsPredefinedById(String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in LabelsPredefinedServiceImpl#findLabelsPredefinedById", "400");
        BusinessAssert.isId(id, "id must be an id in LabelsPredefinedServiceImpl#findLabelsPredefinedById", "400");
        logger.info("find labelsPredefined by id '{}' in LabelsPredefinedServiceImpl#findLabelsPredefinedById", id);
        return labelsPredefinedRepository.findOne(id);
    }

    @Override
    public LabelsPredefined create(final LabelsPredefined labelPredefined) throws BusinessException {
        BusinessAssert.notNull(labelPredefined, "labelPredefined is mandatory in LabelsPredefinedServiceImpl#create", "400");
        logger.info("create label '{}' in LabelsPredefinedServiceImpl#create", labelPredefined);
        return labelsPredefinedRepository.insert(labelPredefined);
    }

    @Override
    public void delete(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in LabelsPredefinedServiceImpl#deleteLabelPredefined", "400");
        BusinessAssert.isId(id, "labelPredefined must be an id in LabelsPredefinedServiceImpl#deleteLabelPredefined", "400");
        logger.info("delete labelPredefined with id '{}' in LabelsPredefinedServiceImpl#create", id);
        labelsPredefinedRepository.delete(id);
    }

    @Override
    public String toString() {
        return "[LabelsPredefinedServiceImpl]";
    }
}
