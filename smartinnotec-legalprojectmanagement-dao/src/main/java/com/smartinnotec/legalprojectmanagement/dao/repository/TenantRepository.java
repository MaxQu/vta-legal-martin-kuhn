package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface TenantRepository extends MongoRepository<Tenant, String> {

    Tenant findByName(final String name);

}
