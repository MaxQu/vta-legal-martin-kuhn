package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.ContactPreviousImported;

public interface ContactPreviousImportedRepository extends MongoRepository<ContactPreviousImported, String> {

}
