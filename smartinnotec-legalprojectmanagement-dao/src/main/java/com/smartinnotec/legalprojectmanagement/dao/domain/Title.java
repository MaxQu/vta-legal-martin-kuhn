package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "Title")
public @Data class Title {

    private String title;

}
