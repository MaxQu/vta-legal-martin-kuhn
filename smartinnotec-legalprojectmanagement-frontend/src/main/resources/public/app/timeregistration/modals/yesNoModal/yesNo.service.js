(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.factory('yesNoTimeRegistrationModalService', yesNoTimeRegistrationModalService);
        
    yesNoTimeRegistrationModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoTimeRegistrationModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, timeRegistration) {
			 var yesNoModal = $modal.open({
				controller: YesNoTimeRegistrationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	timeRegistration: function() {
			    		return timeRegistration;
			    	}
			    }, 
				templateUrl: 'app/timeregistration/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoTimeRegistrationModalController($modalInstance, $scope, invoker, timeRegistration) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.timeRegistration = timeRegistration;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.deleteTimeRegistration(vm.timeRegistration);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();