package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface ProjectRepository extends MongoRepository<Project, String> {

    List<Project> findByTenant(final Tenant tenant);

    @Query("{$text : { $search : ?0 } }")
    List<Project> findProjectBySearchString(final String searchString);

    List<Project> findByGeneralAvailable(final Boolean generalAccess);

    Long countById(final String id);

}
