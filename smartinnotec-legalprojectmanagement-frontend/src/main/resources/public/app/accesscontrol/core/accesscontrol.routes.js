(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.accesscontrol')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getAccessControlState());
    	 
    	////////////
			    	
    	function getAccessControlState() {
    		var state = {
    			name: 'auth.accesscontrol',
				url: '/accesscontrol/:userId',
				templateUrl: 'app/accesscontrol/accesscontrol/accesscontrol.html',
				controller: 'AccessControlController',
				controllerAs: 'vm',
				resolve: {
					accessControlService: 'accessControlService',
					projectsService: 'projectsService',
					accessControls: function(accessControlService) {
						return accessControlService.getAllAccessControls();
					},
					projects: function findProjectsOfUser($stateParams, projectsService) {
						return projectsService.findProjectsOfUser($stateParams.userId);
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					}
				}
    		};
    		return state;
    	}
	}
})();