(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactChangedModalService', contactChangedModalService);
        
    contactChangedModalService.$inject = ['$modal', '$stateParams'];
    
    function contactChangedModalService($modal, $stateParams) {
		var service = {
			showContactChangedModal: showContactChangedModal
		};		
		return service;
		
		////////////
				
		function showContactChangedModal(invoker, currentUser, contactChanged, index, countryTypes, titles, provinceTypes, contactTypes) {
			 var contactChangedModal = $modal.open({
				controller: ContactChangedModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",  
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	contactChanged: function() {
			    		return contactChanged;
			    	},
			    	index: function() {
			    		return index;
			    	},
			    	countryTypes: function() {
			    		return countryTypes;
			    	},
			    	titles: function() {
			    		return titles;
			    	},
			    	provinceTypes: function() {
			    		return provinceTypes;
			    	},
			    	contactTypes: function() {
			    		return contactTypes;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/contactChangedModal/contactChanged.modal.html'
			});
			return contactChangedModal;
			
			function ContactChangedModalController($modalInstance, $scope, invoker, currentUser, contactChanged, index, countryTypes, titles, provinceTypes, contactTypes) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.currentUser = currentUser;
		    	vm.contactChanged = contactChanged;
		    	vm.index = index;
		    	vm.countryTypes = countryTypes;
		    	vm.titles = titles;
		    	vm.provinceTypes = provinceTypes;
		    	vm.contactTypes = contactTypes;
		    	vm.date = new Date();
		    	
		    	vm.confirmContactChanged = confirmContactChanged;
		    	vm.closeContactChangedModal = closeContactChangedModal;
		    	
		    	////////////
		    	
		    	function confirmContactChanged() {
		    		vm.invoker.commitContactChanged(vm.contactChanged, vm.index);
		    		vm.closeContactChangedModal();
		    	}
		    	
		    	function closeContactChangedModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();