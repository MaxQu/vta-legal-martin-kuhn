package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.service.UtilService;

@Component
public class SystemImportContactComparation {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UtilService utilService;

    public boolean contactChanged(final Contact contact, final ContactImported mergedContactImported) throws BusinessException {
        BusinessAssert.notNull(mergedContactImported,
            "mergedContactImported is mandatory in SystemImportContactComparation#contactChanged");
        logger.info(
            "contact changed detected with contact with id '{}' and mergedContact '{}' in SystemImportContactComparation#contactChanged",
            (contact != null ? contact.getId() : null), mergedContactImported.getId());

        // if imported contact is new
        if (contact == null) {
            mergedContactImported.setNewContact(true);
            return true;
        }

        boolean contactChanged = false;

        if (utilService.hasStringPropertyChanged(contact.getInstitution(), mergedContactImported.getInstitution())) {
            mergedContactImported.setInstitutionChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(contact.getAdditionalNameInformation(),
            mergedContactImported.getAdditionalNameInformation())) {
            mergedContactImported.setAdditionalNameInformationChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(contact.getContactPerson(), mergedContactImported.getContactPerson())) {
            mergedContactImported.setContactPersonChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(contact.getShortCode(), mergedContactImported.getShortCode())) {
            mergedContactImported.setShortCodeChanged(true);
            contactChanged = true;
        }
        if (utilService.hasCustomerNumberPropertyChanged(contact.getCustomerNumberContainers(),
            mergedContactImported.getCustomerNumberContainers())) {
            mergedContactImported.setCustomerNumberContainersChanged(true);
            contactChanged = true;
        }
        if (utilService.hasAgentNumberPropertyChanged(contact.getAgentNumberContainers(),
            mergedContactImported.getAgentNumberContainers())) {
            mergedContactImported.setAgentNumberContainersChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringListChanged(contact.getTelephones(), mergedContactImported.getTelephones())) {
            mergedContactImported.setTelephonesChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringListChanged(contact.getEmails(), mergedContactImported.getEmails())) {
            mergedContactImported.setEmailsChanged(true);
            contactChanged = true;
        }

        final Address address = contact.getAddress();
        final AddressImported addressImported = mergedContactImported.getAddress();

        final boolean addressChanged = compareAddresses(address, addressImported);
        contactChanged = !contactChanged ? addressChanged : contactChanged;

        // contactAddressWithProducts
        final List<ContactAddressWithProducts> contactAddressesWithProducts = contact.getAddressesWithProducts();
        final List<ContactImportedAddressImported> contactsImportedAddressesImported = mergedContactImported.getAddressesWithProducts();

        if (contactAddressesWithProductsHasDifferentSize(contactAddressesWithProducts, contactsImportedAddressesImported)) {
            mergedContactImported.setContactImportedAddressesImportedChanged(true);
            contactChanged = true;
        }
        if (contactAddressesWithProductsHasNotSameFlfId(contactAddressesWithProducts, contactsImportedAddressesImported)) {
            mergedContactImported.setContactImportedAddressesImportedChanged(true);
            contactChanged = true;
        }
        if (contactAddressesWithProducts == null && contactsImportedAddressesImported == null) {
            return contactChanged;
        } else if (contactAddressesWithProducts == null && contactsImportedAddressesImported != null) {
            mergedContactImported.setContactImportedAddressesImportedChanged(true);
        } else if (contactAddressesWithProducts != null && contactsImportedAddressesImported == null) {
            mergedContactImported.setContactImportedAddressesImportedChanged(true);
        } else if (contactAddressesWithProducts.size() > contactsImportedAddressesImported.size()) {
            for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
                final ContactImportedAddressImported contactImportedAddressImported = getContactImportedAddressImported(
                    contactAddressWithProducts, contactsImportedAddressesImported);
                if (contactImportedAddressImported == null) {
                    mergedContactImported.setContactImportedAddressesImportedChanged(true);
                    contactChanged = true;
                    continue;
                }
                final Address addressOfContactAddressWithProducts = contactAddressWithProducts.getAddress();
                final AddressImported addressOfContactImportedAddressImported = contactImportedAddressImported.getAddress();

                final boolean addressChangedOfContactAddressWithProducts = compareAddresses(addressOfContactAddressWithProducts,
                    addressOfContactImportedAddressImported);
                contactChanged = !contactChanged ? addressChangedOfContactAddressWithProducts : contactChanged;
                if (addressChangedOfContactAddressWithProducts) {
                    mergedContactImported.setContactImportedAddressesImportedChanged(true);
                }
            }
        } else if (contactAddressesWithProducts.size() < contactsImportedAddressesImported.size()
            || contactAddressesWithProducts.size() == contactsImportedAddressesImported.size()) {
            for (final ContactImportedAddressImported contactsImportedAddressImported : contactsImportedAddressesImported) {
                final ContactAddressWithProducts contactAddressWithProducts = getContactAddressWithProducts(contactsImportedAddressImported,
                    contactAddressesWithProducts);
                if (contactAddressWithProducts == null) {
                    mergedContactImported.setContactImportedAddressesImportedChanged(true);
                    contactChanged = true;
                    continue;
                }
                final Address addressOfContactAddressWithProducts = contactAddressWithProducts.getAddress();
                final AddressImported addressOfContactImportedAddressImported = contactsImportedAddressImported.getAddress();

                final boolean addressChangedOfContactAddressWithProducts = compareAddresses(addressOfContactAddressWithProducts,
                    addressOfContactImportedAddressImported);
                contactChanged = !contactChanged ? addressChangedOfContactAddressWithProducts : contactChanged;
                if (addressChangedOfContactAddressWithProducts) {
                    mergedContactImported.setContactImportedAddressesImportedChanged(true);
                }
            }
        }
        return contactChanged;
    }

    private boolean compareAddresses(final Address address, final AddressImported addressImported) throws BusinessException {
        logger.info("compare addresses in SystemImportContactComparation#compareAddresses");
        boolean contactChanged = false;
        if (utilService.hasStringPropertyChanged(address.getFlfId(), addressImported.getFlfId())) {
            addressImported.setFlfIdChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getAdditionalInformation(), addressImported.getAdditionalInformation())) {
            addressImported.setAdditionalInformationChanged(true);
            contactChanged = true;
        }
        /*
         * if (utilService.hasCustomerNumberPropertyChanged(address.getCustomerNumberContainers(),
         * addressImported.getCustomerNumberContainers())) { addressImported.setCustomerNumberContainersChanged(true);
         * contactChanged = true; }
         */
        if (utilService.hasStringPropertyChanged(address.getStreet(), addressImported.getStreet())) {
            addressImported.setStreetChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getIntSign(), addressImported.getIntSign())) {
            addressImported.setIntSignChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getPostalCode(), addressImported.getPostalCode())) {
            addressImported.setPostalCodeChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getRegion(), addressImported.getRegion())) {
            addressImported.setRegionChanged(true);
            contactChanged = true;
        }
        if ((address.getProvinceType() == null && addressImported.getProvinceType() != null)
            || (address.getProvinceType() != null && addressImported.getProvinceType() == null)
            || (address.getProvinceType() != null && addressImported.getProvinceType() != null && utilService
                .hasStringPropertyChanged(address.getProvinceType().toString(), addressImported.getProvinceType().toString()))) {
            addressImported.setProvinceTypeChanged(true);
            contactChanged = true;
        }
        if ((address.getCountry() != null && addressImported.getCountry() == null)
            || (address.getCountry() == null && addressImported.getCountry() != null)
            || (address.getCountry() != null && addressImported.getCountry() != null
                && utilService.hasStringPropertyChanged(address.getCountry().toString(), addressImported.getCountry().toString()))) {
            addressImported.setCountryChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getTelephone(), addressImported.getTelephone())) {
            addressImported.setTelephoneChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getEmail(), addressImported.getEmail())) {
            addressImported.setEmailChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getInformation(), addressImported.getInformation())) {
            addressImported.setInformationChanged(true);
            contactChanged = true;
        }

        return contactChanged;
    }

    // imported contactImportedAddressImported -> equal with ContactAddressWithProducts without products
    private ContactImportedAddressImported getContactImportedAddressImported(final ContactAddressWithProducts contactAddressWithProducts,
            final List<ContactImportedAddressImported> contactsImportedAddressesImported) {
        logger.info("get contactImported in SystemImportContactComparation#getContactImportedAddressImported");
        for (final ContactImportedAddressImported contactImportedAddressImported : contactsImportedAddressesImported) {
            if (contactImportedAddressImported.getAddress().getFlfId().equals(contactAddressWithProducts.getAddress().getFlfId())) {
                return contactImportedAddressImported;
            }
        }
        return null;
    }

    private ContactAddressWithProducts getContactAddressWithProducts(final ContactImportedAddressImported contactImportedAddressImported,
            final List<ContactAddressWithProducts> contactAddressesWithProducts) {
        logger.info("get contactAddressWithProducts in SystemImportContactComparation#getContactAddressWithProducts");
        for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
            if (contactImportedAddressImported.getAddress().getFlfId().equals(contactAddressWithProducts.getAddress().getFlfId())) {
                return contactAddressWithProducts;
            }
        }
        return null;
    }

    private boolean contactAddressesWithProductsHasDifferentSize(final List<ContactAddressWithProducts> contactAddressesWithProducts,
            final List<ContactImportedAddressImported> contactsImportedAddressesImported) {
        if (((contactAddressesWithProducts == null || contactAddressesWithProducts.isEmpty())
            && (contactsImportedAddressesImported != null && !contactsImportedAddressesImported.isEmpty()))
            || ((contactAddressesWithProducts != null && !contactAddressesWithProducts.isEmpty())
                && (contactsImportedAddressesImported == null || contactsImportedAddressesImported.isEmpty()))
            || (contactAddressesWithProducts != null && contactsImportedAddressesImported != null
                && contactAddressesWithProducts.size() != contactsImportedAddressesImported.size())) {
            return true;
        }
        return false;
    }

    private boolean contactAddressesWithProductsHasNotSameFlfId(final List<ContactAddressWithProducts> contactAddressesWithProducts,
            final List<ContactImportedAddressImported> contactsImportedAddressesImported) {
        if (contactAddressesWithProducts != null) {
            for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
                final String flfId = contactAddressWithProducts.getAddress().getFlfId();
                boolean exists = false;
                if (contactsImportedAddressesImported != null) {
                    for (final ContactImportedAddressImported contactImportedAddressImported : contactsImportedAddressesImported) {
                        final String importedFlfId = contactImportedAddressImported.getAddress().getFlfId();
                        if (flfId != null && flfId.equals(importedFlfId)) {
                            exists = true;
                        }
                    }
                }
                if (exists == false) {
                    return true;
                }
            }
        }
        return false;
    }

}
