(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.communication')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getCommunicationState());
    	 
    	////////////
			    	
    	function getCommunicationState() {
    		var state = {
    			name: 'auth.communication',
				url: '/communication/:userId',
				templateUrl: 'app/communication/communication/communication.html',
				controller: 'CommunicationController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					projectsService: 'projectsService',
					communicationService: 'communicationService',
					communicationGroupUserConnections: function findCommunicationGroupUserConnectionsByUser(communicationService, $stateParams) {
						return null;
					},
					communicationUserConnectionsOfUserAndPage: function getCommunicationUserConnectionsOfUserAndPage($stateParams, communicationService) {
						return null;
					},
					allUsers: function findAllUsers(userService) {
						return userService.findAllUsers();
					},
					sentAndOpenOrders: function findSentAndOpenOrders($stateParams, communicationService) {
						return communicationService.findSentAndOpenOrders($stateParams.userId, false);
					},
					receivedAndOpenOrders: function findReceivedAndOpenOrders($stateParams, communicationService) {
						return communicationService.findReceivedAndOpenOrders($stateParams.userId, false);
					},
					projects: function findProjectsOfUser($stateParams, projectsService) {
						return projectsService.findProjectsOfUser($stateParams.userId);
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					}
				}
    		};
    		return state;
    	}
	}
})();