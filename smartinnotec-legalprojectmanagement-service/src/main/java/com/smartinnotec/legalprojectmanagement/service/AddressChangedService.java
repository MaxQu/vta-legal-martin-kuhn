package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressChanged;

public interface AddressChangedService {

    AddressChanged convertFromAddressToAddressChanged(final Address address) throws BusinessException;

    AddressChanged create(final AddressChanged addressChanged) throws BusinessException;

    AddressChanged update(final AddressChanged addressChanged) throws BusinessException;

    void delete(final AddressChanged addressChanged) throws BusinessException;

    void deleteAll() throws BusinessException;

}
