package com.smartinnotec.legalprojectmanagement.service;

import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.ThinningPolymerDateTimeValue;
import com.smartinnotec.legalprojectmanagement.dao.repository.ThinningPolymerDateTimeValueRepository;

@Service("thinningPolymerDateTimeValueService")
public class ThinningPolymerDateTimeValueServiceImpl extends AbstractService implements ThinningPolymerDateTimeValueService {

    @Autowired
    private ThinningPolymerDateTimeValueRepository thinningPolymerDateTimeValueRepository;

    @Override
    public List<ThinningPolymerDateTimeValue> findAll() throws BusinessException {
        logger.info("find all thinningPolymerDateTimeValues in ThinningPolymerDateTimeValueServiceImpl#findAll");
        final List<ThinningPolymerDateTimeValue> allThinningPolymerDateTimeValues = thinningPolymerDateTimeValueRepository.findAll();
        if (allThinningPolymerDateTimeValues != null && !allThinningPolymerDateTimeValues.isEmpty()) {
            Collections.sort(allThinningPolymerDateTimeValues);
        }
        final ThinningPolymerDateTimeValue defaultThinningPolymerDateTimeValue = getDefaultThinningPolymerDateTimeValue();
        allThinningPolymerDateTimeValues.add(defaultThinningPolymerDateTimeValue);
        return allThinningPolymerDateTimeValues;
    }

    @Override
    public ThinningPolymerDateTimeValue getThinningPolymerDateTimeValueByDate(final DateTime dateTime) throws BusinessException {
        final List<ThinningPolymerDateTimeValue> thinningPolymerDateTimeValues = thinningPolymerDateTimeValueRepository.findAll();
        if (thinningPolymerDateTimeValues != null && !thinningPolymerDateTimeValues.isEmpty()) {
            Collections.sort(thinningPolymerDateTimeValues);
        }
        ThinningPolymerDateTimeValue foundedThinningPolymerDateTimeValue = getDefaultThinningPolymerDateTimeValue();
        for (final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue : thinningPolymerDateTimeValues) {
            if (thinningPolymerDateTimeValue.getDateTime().withTimeAtStartOfDay().isEqual(dateTime.withTimeAtStartOfDay())
                || dateTime.withTimeAtStartOfDay().isAfter(thinningPolymerDateTimeValue.getDateTime().withTimeAtStartOfDay())) {
                foundedThinningPolymerDateTimeValue = thinningPolymerDateTimeValue;
            }
        }
        return foundedThinningPolymerDateTimeValue;
    }

    @Override
    public ThinningPolymerDateTimeValue create(final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue) throws BusinessException {
        BusinessAssert.notNull(thinningPolymerDateTimeValue,
            "thinningPolymerDateTimeValue is mandatory in ThinningPolymerDateTimeValueServiceImpl#create", "400");
        logger.info("create thinningPolymerDateTimeValue in ThinningPolymerDateTimeValueServiceImpl#create");
        thinningPolymerDateTimeValue.setDateTime(thinningPolymerDateTimeValue.getDateTime().withTimeAtStartOfDay());
        return thinningPolymerDateTimeValueRepository.insert(thinningPolymerDateTimeValue);
    }

    @Override
    public ThinningPolymerDateTimeValue update(final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue) throws BusinessException {
        BusinessAssert.notNull(thinningPolymerDateTimeValue,
            "thinningPolymerDateTimeValue is mandatory in ThinningPolymerDateTimeValueServiceImpl#update", "400");
        logger.info("update ThinningPolymerDateTimeValue with id '{}' in ThinningPolymerDateTimeValueServiceImpl#update");
        return thinningPolymerDateTimeValueRepository.save(thinningPolymerDateTimeValue);
    }

    @Override
    public void delete(final String thinningPolymerDateTimeValueId) throws BusinessException {
        BusinessAssert.notNull(thinningPolymerDateTimeValueId,
            "thinningPolymerDateTimeValueId is mandatory in ThinningPolymerDateTimeValueServiceImpl#delete", "400");
        BusinessAssert.isId(thinningPolymerDateTimeValueId,
            "thinningPolymerDateTimeValueId must be an id in ThinningPolymerDateTimeValueServiceImpl#delete", "400");
        logger.info("delete thinningPolymerDateTimeValue with id '{}' in ThinningPolymerDateTimeValueServiceImpl#delete",
            thinningPolymerDateTimeValueId);
        thinningPolymerDateTimeValueRepository.delete(thinningPolymerDateTimeValueId);
    }

    private ThinningPolymerDateTimeValue getDefaultThinningPolymerDateTimeValue() {
        final ThinningPolymerDateTimeValue tinningPolymerDateTimeValue = new ThinningPolymerDateTimeValue();
        tinningPolymerDateTimeValue.setDateTime(null);
        tinningPolymerDateTimeValue.setValue("0.003");
        return tinningPolymerDateTimeValue;
    }
}
