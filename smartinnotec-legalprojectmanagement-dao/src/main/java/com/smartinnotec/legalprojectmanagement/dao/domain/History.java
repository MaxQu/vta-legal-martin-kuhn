package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "History")
public @Data class History {

    @Id
    private String id;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime date;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime actionDate;
    private String projectName;
    private String message;
    private HistoryTypeEnum historyType;
    private String filePath;
    @DBRef
    private User user;
    @DBRef
    private Tenant tenant;

    public History() {
    }

    public History(final HistoryBuilder historyBuilder) {
        this.date = historyBuilder.date;
        this.actionDate = historyBuilder.actionDate;
        this.projectName = historyBuilder.projectName;
        this.message = historyBuilder.message;
        this.historyType = historyBuilder.historyType;
        this.filePath = historyBuilder.filePath;
        this.user = historyBuilder.user;
        this.tenant = historyBuilder.tenant;
    }

    public static class HistoryBuilder {

        private DateTime date;
        private DateTime actionDate;
        private String projectName;
        private String message;
        private HistoryTypeEnum historyType;
        private String filePath;
        private User user;
        private Tenant tenant;

        public HistoryBuilder() {
        }

        public HistoryBuilder setDate(final DateTime date) {
            this.date = date;
            return this;
        }

        public HistoryBuilder setActionDate(final DateTime actionDate) {
            this.actionDate = actionDate;
            return this;
        }

        public HistoryBuilder setProjectName(final String projectName) {
            this.projectName = projectName;
            return this;
        }

        public HistoryBuilder setMessage(final String message) {
            this.message = message;
            return this;
        }

        public HistoryBuilder setHistoryType(final HistoryTypeEnum historyType) {
            this.historyType = historyType;
            return this;
        }

        public HistoryBuilder setFilePath(final String filePath) {
            this.filePath = filePath;
            return this;
        }

        public HistoryBuilder setUser(final User user) {
            this.user = user;
            return this;
        }

        public HistoryBuilder setTenant(final Tenant tenant) {
            this.tenant = tenant;
            return this;
        }

        public History build() {
            return new History(this);
        }
    }
}
