package com.smartinnotec.legalprojectmanagement.restservice;

import java.text.SimpleDateFormat;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import com.smartinnotec.legalprojectmanagement.security.xauth.SimpleUserDetails;

//Required = Default, transaction must always exist and this one is used by inner methods/classes
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
@RequestMapping("/api")
@ConfigurationProperties(locations = "classpath:application.properties", ignoreUnknownFields = true, prefix = "legalprojectmanagement.restservice")
public abstract class AbstractRestService {

    public static String BAD_REQUEST_CODE = HttpStatus.BAD_REQUEST.toString();

    protected static final String SYSTEM_IMPORT_CONTACT_FOLDER;
    protected static final String COMMUNICATION_FOLDER;
    protected static final SimpleDateFormat sdf;

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected DateTimeFormatter dateFormatter;
    protected DateTimeFormatter dateFormatterReverse;
    protected DateTimeFormatter dateTimeFormatter;
    protected DateTimeFormatter dateTimePreciseFormatter;
    protected DateTimeFormatter dateTimeFormatterWithoutSeparator;

    @Autowired
    protected Environment environment;

    static {
        sdf = new SimpleDateFormat("yyyyMMdd");
        SYSTEM_IMPORT_CONTACT_FOLDER = "SystemImportContactFiles";
        COMMUNICATION_FOLDER = "CommunicationFiles";
    }

    public AbstractRestService() {
        dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        dateFormatterReverse = DateTimeFormat.forPattern("yyyy-MM-dd");
        dateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        dateTimePreciseFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss.SSS");
        dateTimeFormatterWithoutSeparator = DateTimeFormat.forPattern("yyyyMMdd_HHmmSS");
    }

    protected String getInvokedRestServiceUserInformation() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((SimpleUserDetails)principal).getId() + "/" + ((SimpleUserDetails)principal).getUsername();
        } else {
            return "no principal";
        }
    }

    protected String getLoggedInUserId() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((SimpleUserDetails)principal).getId();
        } else {
            return "no principal";
        }
    }

    protected String getLoggedInUsername() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((SimpleUserDetails)principal).getUsername();
        } else {
            return "no principal";
        }
    }

    @Override
    public String toString() {
        return "[AbstractRestService]";
    }
}
