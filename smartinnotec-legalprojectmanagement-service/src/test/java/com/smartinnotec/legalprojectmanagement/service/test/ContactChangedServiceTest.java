package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChangedTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProvinceTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class ContactChangedServiceTest extends AbstractServiceTest {

    @Test
    public void shouldCreateContactsChanged() throws BusinessException {
        final User user = new User();
        final Tenant tenant = tenantService.findTenantByName("vta_rottenbach");
        user.setTenant(tenant);
        final User createdUser = userService.createUser(user);
        for (int i = 0; i < 400; i++) {
            final Contact contact = createContact("institution" + i);
            final ContactChanged convertedContactChanged = contactChangedService.convertFromContactToContactChanged(contact,
                ContactChangedTypeEnum.CONTACT_CREATED);

            final Contact updatedContact = contactService.create(contact, createdUser, true);
            convertedContactChanged.setTenant(tenant);
            convertedContactChanged.setCommitted(false);
            contact.setInstitution(contact.getInstitution() + " Edit");
            convertedContactChanged.setInstitution("test " + i);
            final ContactChanged createdContactChanged = contactChangedService.create(convertedContactChanged, createdUser);
            Assert.assertNotNull(createdContactChanged);
        }
    }

    @Test
    public void shouldCreateContactChanged() throws BusinessException {

        final User user = new User();
        final Tenant tenant = new Tenant();
        tenant.setName("test_tenant");
        user.setTenant(tenant);

        final Contact contact = createContact("institution");
        final ContactChanged contactChanged = contactChangedService.convertFromContactToContactChanged(contact,
            ContactChangedTypeEnum.CONTACT_CREATED);
        final ContactChanged createdContactChanged = contactChangedService.create(contactChanged, user);
        Assert.assertNotNull(createdContactChanged);
    }

    @Test
    public void shouldFindUniqueContacts() throws BusinessException {

        final Contact contact = createContact("institution");

        final ContactChanged contactChanged = contactChangedService.convertFromContactToContactChanged(contact,
            ContactChangedTypeEnum.CONTACT_CREATED);

        Assert.assertNotNull(contactChanged);
        Assert.assertEquals(false, contactChanged.isCommitted());
        Assert.assertEquals(ContactChangedTypeEnum.CONTACT_CREATED, contactChanged.getContactChangedType());
        Assert.assertEquals(contact.getInstitution(), contactChanged.getInstitution());
        Assert.assertEquals(contact.getAdditionalNameInformation(), contactChanged.getAdditionalNameInformation());
        Assert.assertEquals(contact.getShortCode(), contactChanged.getShortCode());
        Assert.assertEquals(contact.getCustomerNumberContainers().get(0).getContactTenant(),
            contactChanged.getCustomerNumberContainers().get(0).getContactTenant());
        Assert.assertEquals(contact.getCustomerNumberContainers().get(0).getCustomerNumber(),
            contactChanged.getCustomerNumberContainers().get(0).getCustomerNumber());
        Assert.assertEquals(contact.getAgentNumberContainers().get(0).getCustomerNumber(),
            contactChanged.getAgentNumberContainers().get(0).getCustomerNumber());
        Assert.assertEquals(contact.getInformation(), contactChanged.getInformation());
        Assert.assertEquals(contact.getEmails().get(0), contactChanged.getEmails().get(0));
        Assert.assertEquals(contact.getTelephones().get(0), contactChanged.getTelephones().get(0));

        Assert.assertEquals(contact.getAddress().getFlfId(), contactChanged.getAddress().getFlfId());
        Assert.assertEquals(contact.getAddress().getAdditionalInformation(), contactChanged.getAddress().getAdditionalInformation());
        Assert.assertEquals(contact.getAddress().getStreet(), contactChanged.getAddress().getStreet());
        Assert.assertEquals(contact.getAddress().getIntSign(), contactChanged.getAddress().getIntSign());
        Assert.assertEquals(contact.getAddress().getPostalCode(), contactChanged.getAddress().getPostalCode());
        Assert.assertEquals(contact.getAddress().getRegion(), contactChanged.getAddress().getRegion());
        Assert.assertEquals(contact.getAddress().getProvinceType(), contactChanged.getAddress().getProvinceType());
        Assert.assertEquals(contact.getAddress().getCountry(), contactChanged.getAddress().getCountry().toString());
        Assert.assertEquals(contact.getAddress().getEmail(), contactChanged.getAddress().getEmail());
        Assert.assertEquals(contact.getAddress().getTelephone(), contactChanged.getAddress().getTelephone());

        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getFlfId(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getFlfId());
        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getAdditionalInformation(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getAdditionalInformation());
        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getStreet(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getStreet());
        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getIntSign(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getIntSign());
        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getPostalCode(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getPostalCode());
        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getProvinceType(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getProvinceType());
        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getCountry(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getCountry().toString());
        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getEmail(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getEmail());
        Assert.assertEquals(contact.getAddressesWithProducts().get(0).getAddress().getTelephone(),
            contactChanged.getAddressesWithProducts().get(0).getAddress().getTelephone());
    }

    private Contact createContact(final String name) throws BusinessException {
        final Contact contact = new Contact();
        contact.setInstitution(name);
        contact.setAdditionalNameInformation("Additional Name Information");
        contact.setShortCode("Short Code");
        final List<CustomerNumberContainer> customerNumberContainers = new ArrayList<>();
        final CustomerNumberContainer customerNumberContainer = new CustomerNumberContainer();
        customerNumberContainer.setContactTenant(CountryTypeEnum.AUSTRIA.toString());
        customerNumberContainer.setCustomerNumber("12345");
        customerNumberContainers.add(customerNumberContainer);
        contact.setCustomerNumberContainers(customerNumberContainers);

        final List<AgentNumberContainer> agentNumberContainers = new ArrayList<>();
        final AgentNumberContainer agentNumberContainer = new AgentNumberContainer();
        agentNumberContainer.setCustomerNumber("12345");
        agentNumberContainers.add(agentNumberContainer);
        contact.setAgentNumberContainers(agentNumberContainers);

        contact.setInformation("Information");
        final List<String> emails = new ArrayList<>();
        emails.add("test@mail.com");
        contact.setEmails(emails);

        final List<String> telephones = new ArrayList<>();
        telephones.add("987654321");
        contact.setTelephones(telephones);

        final Address address = new Address();
        address.setFlfId("45676");
        address.setAdditionalInformation("Add Info");
        address.setStreet("Street");
        address.setIntSign("AT");
        address.setPostalCode("4444");
        address.setRegion("Region");
        address.setProvinceType(ProvinceTypeEnum.APPENZELL_AUSSERRHODEN);
        address.setCountry(CountryTypeEnum.BULGARIA.toString());
        address.setTelephone("12323");
        address.setEmail("email@test.com");
        contact.setAddress(address);

        final List<ContactAddressWithProducts> addressesWithProducts = new ArrayList<>();
        final ContactAddressWithProducts contactAddressWithProducts1 = new ContactAddressWithProducts();
        final Address address1 = new Address();
        address1.setFlfId("45676");
        address1.setAdditionalInformation("Add Info");
        address1.setStreet("Street");
        address1.setIntSign("AT");
        address1.setPostalCode("4444");
        address1.setRegion("Region");
        address1.setProvinceType(ProvinceTypeEnum.APPENZELL_AUSSERRHODEN);
        address1.setCountry(CountryTypeEnum.BULGARIA.toString());
        address1.setTelephone("12323");
        address1.setEmail("email@test.com");
        contactAddressWithProducts1.setAddress(address1);
        addressesWithProducts.add(contactAddressWithProducts1);
        contact.setAddressesWithProducts(addressesWithProducts);

        return contact;
    }
}
