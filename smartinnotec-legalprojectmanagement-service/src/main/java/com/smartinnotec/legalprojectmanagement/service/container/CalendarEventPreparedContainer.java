package com.smartinnotec.legalprojectmanagement.service.container;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventSerialDateTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;

import lombok.Data;
import lombok.Getter;

public @Data class CalendarEventPreparedContainer {

    private @Getter String id;
    private @Getter String title;
    private @Getter String location;
    private @Getter String timeRange;
    private @Getter String contactAddressWithProductsText;
    private @Getter Contact contact;
    private @Getter CalendarEventColorObject color;
    private @Getter Long startsAt;
    private @Getter Long endsAt;
    private @Getter boolean createContact;
    private @Getter boolean draggable;
    private @Getter boolean resizable;
    private @Getter String projectId;
    private @Getter String projectName;
    private @Getter Boolean serialDate;
    private @Getter Long serialDateNumber;
    private @Getter CalendarEventSerialDateTypeEnum calendarEventSerialDateType;
    private @Getter CalendarEventTypeEnum calendarEventTyp;
    private @Getter DateTime serialEndDate;
    private @Getter String cssClass;
    private @Getter Boolean confirmed;
    private @Getter HistoryTypeEnum archivedHistoryType;
    private @Getter String information;
    private @Getter Boolean archived;
    private @Getter Integer remainingDays;
    private @Getter List<CalendarEventUserConnection> calendarEventUserConnections;

    public CalendarEventPreparedContainer() {
    }

    public CalendarEventPreparedContainer(final CalendarEventPreparedContainerBuilder calendarEventPreparedContainerBuilder) {
        this.id = calendarEventPreparedContainerBuilder.id;
        this.title = calendarEventPreparedContainerBuilder.title;
        this.location = calendarEventPreparedContainerBuilder.location;
        this.timeRange = calendarEventPreparedContainerBuilder.timeRange;
        this.contactAddressWithProductsText = calendarEventPreparedContainerBuilder.contactAddressWithProductsText;
        this.contact = calendarEventPreparedContainerBuilder.contact;
        this.color = calendarEventPreparedContainerBuilder.color;
        this.startsAt = calendarEventPreparedContainerBuilder.startsAt;
        this.endsAt = calendarEventPreparedContainerBuilder.endsAt;
        this.createContact = calendarEventPreparedContainerBuilder.createContact;
        this.draggable = calendarEventPreparedContainerBuilder.draggable;
        this.resizable = calendarEventPreparedContainerBuilder.resizable;
        this.projectId = calendarEventPreparedContainerBuilder.projectId;
        this.projectName = calendarEventPreparedContainerBuilder.projectName;
        this.serialDate = calendarEventPreparedContainerBuilder.serialDate;
        this.serialDateNumber = calendarEventPreparedContainerBuilder.serialDateNumber;
        this.calendarEventSerialDateType = calendarEventPreparedContainerBuilder.calendarEventSerialDateType;
        this.serialEndDate = calendarEventPreparedContainerBuilder.serialEndDate;
        this.cssClass = calendarEventPreparedContainerBuilder.cssClass;
        this.confirmed = calendarEventPreparedContainerBuilder.confirmed;
        this.archivedHistoryType = calendarEventPreparedContainerBuilder.archivedHistoryType;
        this.calendarEventTyp = calendarEventPreparedContainerBuilder.calendarEventTyp;
        this.information = calendarEventPreparedContainerBuilder.information;
        this.archived = calendarEventPreparedContainerBuilder.archived;
        this.remainingDays = calendarEventPreparedContainerBuilder.remainingDays;
        this.calendarEventUserConnections = calendarEventPreparedContainerBuilder.calendarEventUserConnections;
    }

    public static class CalendarEventPreparedContainerBuilder {

        private String id;
        private String title;
        private String location;
        private String timeRange;
        private String contactAddressWithProductsText;
        private Contact contact;
        private CalendarEventColorObject color;
        private Long startsAt;
        private Long endsAt;
        private boolean createContact;
        private boolean draggable;
        private boolean resizable;
        private String projectId;
        private String projectName;
        private Boolean serialDate;
        private Long serialDateNumber;
        private CalendarEventSerialDateTypeEnum calendarEventSerialDateType;
        private DateTime serialEndDate;
        private String cssClass;
        private Boolean confirmed;
        private HistoryTypeEnum archivedHistoryType;
        private CalendarEventTypeEnum calendarEventTyp;
        private String information;
        private Boolean archived;
        private Integer remainingDays;
        private List<CalendarEventUserConnection> calendarEventUserConnections;

        public CalendarEventPreparedContainerBuilder() {
        }

        public CalendarEventPreparedContainerBuilder setId(final String id) {
            this.id = id;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setTitle(final String title) {
            this.title = title;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setLocation(final String location) {
            this.location = location;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setTimeRange(final String timeRange) {
            this.timeRange = timeRange;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setContact(final Contact contact) {
            this.contact = contact;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setContactAddressWithProductsText(final String contactAddressWithProductsText) {
            this.contactAddressWithProductsText = contactAddressWithProductsText;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setColor(final String primary, final String secondary) {
            final CalendarEventColorObject calendarEventColorObject = new CalendarEventColorObject();
            calendarEventColorObject.setPrimary(primary);
            calendarEventColorObject.setSecondary(secondary);
            this.color = calendarEventColorObject;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setStartsAt(final Long startsAt) {
            this.startsAt = startsAt;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setEndsAt(final Long endsAt) {
            this.endsAt = endsAt;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setCreateContact(final boolean createContact) {
            this.createContact = createContact;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setDraggable(final boolean draggable) {
            this.draggable = draggable;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setResizable(final boolean resizable) {
            this.resizable = resizable;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setProjectId(final String projectId) {
            this.projectId = projectId;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setProjectName(final String projectName) {
            this.projectName = projectName;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setSerialDate(final Boolean serialDate) {
            this.serialDate = serialDate;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setSerialDateNumber(final Long serialDateNumber) {
            this.serialDateNumber = serialDateNumber;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setColor(final CalendarEventColorObject color) {
            this.color = color;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setCalendarEventSerialDateType(
                final CalendarEventSerialDateTypeEnum calendarEventSerialDateType) {
            this.calendarEventSerialDateType = calendarEventSerialDateType;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setSerialEndDate(final DateTime serialEndDate) {
            this.serialEndDate = serialEndDate;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setCssClass(final String cssClass) {
            this.cssClass = cssClass;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setConfirmed(final Boolean confirmed) {
            this.confirmed = confirmed;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setArchivedHistoryType(final HistoryTypeEnum archivedHistoryType) {
            this.archivedHistoryType = archivedHistoryType;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setCalendarEventTyp(final CalendarEventTypeEnum calendarEventTyp) {
            this.calendarEventTyp = calendarEventTyp;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setInformation(final String information) {
            this.information = information;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setArchived(final Boolean archived) {
            this.archived = archived;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setRemainingDays(final Integer remainingDays) {
            this.remainingDays = remainingDays;
            return this;
        }

        public CalendarEventPreparedContainerBuilder setCalendarEventUserConnections(
                final List<CalendarEventUserConnection> calendarEventUserConnections) {
            this.calendarEventUserConnections = calendarEventUserConnections;
            return this;
        }

        public CalendarEventPreparedContainer build() {
            return new CalendarEventPreparedContainer(this);
        }
    }
}
