(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.directive('createEditContactDirective', createEditContactDirective);
    
    createEditContactDirective.$inject = ['$timeout', 'dateUtilityService', 'documentFileService', 'utilService', 'projectTemplateService', 'productsService', 'optionsService'];
    
	function createEditContactDirective($timeout, dateUtilityService, documentFileService, utilService, projectTemplateService, productsService, optionsService) {
		var directive = {
			restrict: 'E',
			scope: {
				contact: '=',
				titles: '=',
				provinceTypes: '=',
				contactTypes: '=',
				countryTypes: '=',
				invoker: '=',
				modalInvoker: '=',
				type: '=',
				currentUser: '=',
				roles: '=',
				usersOfProject: '=',
				disabled: '='
			},
			templateUrl: 'app/contacts/directives/createEditContactDirective/createEditContact.html',
			link: function($scope) {
				    
				$scope.customerSinceDateDatePicker = false;
				$scope.importationDateDatePicker = false;
				$scope.developmentDateDatePicker = false;
				$scope.provinceTypesOfCountry = null;
				$scope.provinceTypesOfAccountCountry = null;
				if($scope.contact != null) {
					$scope.contact.creationDate = $scope.contact.creationDate != null ? dateUtilityService.convertToDateOrUndefined($scope.contact.creationDate) : null;
					$scope.contact.importationDate = $scope.contact.importationDate != null ? dateUtilityService.convertToDateOrUndefined($scope.contact.importationDate) : null;
					$scope.contact.customerSince = $scope.contact.customerSince != null ? dateUtilityService.convertToDateOrUndefined($scope.contact.customerSince) : null;
				}
				
				if($scope.contact != null && $scope.contact.emails == null) {
					$scope.contact.emails = [];
					$scope.contact.emails.push('');
				}
				
				if($scope.contact != null && $scope.contact.emails.length == 0) {
					$scope.contact.emails.push('');
				}
				
				if($scope.contact != null && $scope.contact.telephones == null) {
					$scope.contact.telephones = [];
					$scope.contact.telephones.push('');
				}
				
				if($scope.contact != null && $scope.contact.telephones.length == 0) {
					$scope.contact.telephones.push('');
				}
				
				$scope.openDevelopmentDateDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.developmentDateDatePicker = true;
				};
				   
				$scope.openImportationDateDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.importationDateDatePicker = true;
				};
				
				$scope.openCustomerSinceDateDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.customerSinceDateDatePicker = true;
				};
				
				$scope.initProvinceTypes = function() {
					if($scope.type == 'EDIT' || $scope.type == 'SHOW') {
						if($scope.contact != null && $scope.contact.address != null && $scope.contact.address.country != null) {
							optionsService.getProvinceTypesOfCountryType($scope.contact.address.country).then(function (response) {
								$scope.provinceTypesOfCountry = response.data;
							}).catch(function (data) {
								console.log('error in createEditContact.directive#initProvinceTypes: ' + data);
							});
						}
						if($scope.contact != null && $scope.contact.accountAddress != null && $scope.contact.accountAddress.country != null) {
							optionsService.getProvinceTypesOfCountryType($scope.contact.accountAddress.country).then(function (response) {
								$scope.provinceTypesOfAccountCountry = response.data;
							}).catch(function (data) {
								console.log('error in createEditContact.directive#initProvinceTypes: ' + data);
							});
						}
						
						if($scope.contact != null && $scope.contact.addressesWithProducts != null) {
							for(var i = 0; i < $scope.contact.addressesWithProducts.length; i++) {
								var addressWithProducts = $scope.contact.addressesWithProducts[i];
								if(addressWithProducts != null && addressWithProducts.address != null && addressWithProducts.address.country != null) {
									optionsService.getProvinceTypesOfCountryType(addressWithProducts.address.country).then(function (response) {
										for(var j = 0; j < $scope.contact.addressesWithProducts.length; j++) {
											var provinceTypes = response.data;
											var provinceType = $scope.contact.addressesWithProducts[j].address.provinceType;
											for(var k = 0; k < provinceTypes.length; k++) {
												if(provinceTypes[k] == provinceType) {
													$scope.contact.addressesWithProducts[j].addressLocationCountries = response.data;
													break;
												}
											}
										}
									}).catch(function (data) {
										console.log('error in createEditContact.directive#initProvinceTypes: ' + data);
									});
								}
							}
						}
					}
				};
				$scope.initProvinceTypes();
				
				$scope.setAddressLocationCountries = function() {
					if($scope.type == 'EDIT') {
						if($scope.contact.addressesWithProducts != null) {
							for(var i = 0; i < $scope.contact.addressesWithProducts.length; i++) {
								$scope.contact.addressesWithProducts[i].addressLocationCountries = $scope.provinceTypes;
							}
						}
					}
				};
				$scope.setAddressLocationCountries();
				
				
				$scope.addAddressWithProducts = function() {
					var addressWithProduct = {
							address: {
								additionalInformation: '',
								street: '',
								postalCode: '',  
								region: '',
								email: '',
								telefone: '',
								provinceType: 'EMPTY_PROVINCE',
								country: 'EMPTY_COUNTRY'
								},
							products: []
							};
					if($scope.contact.addressesWithProducts == null) {
						$scope.contact.addressesWithProducts = [];
					}
					$scope.contact.addressesWithProducts.push(addressWithProduct); 
				};
				
				$scope.addAccountAddress = function() {
					var accountAddress = {
							street: '',
							postalCode: '', 
							region: '',
							provinceType: 'EMPTY_PROVINCE',
							country: 'EMPTY_COUNTRY'
							};
					$scope.contact.accountAddress = accountAddress; 
				};
				
				$scope.changedCountry = function(countryType) {
					optionsService.getProvinceTypesOfCountryType(countryType).then(function (response) {
						$scope.provinceTypesOfCountry = response.data;
					}).catch(function (data) {
						console.log('error in createEditContact.directive#getProvinceTypesOfCountryType: ' + data);
					});
				};
				
				$scope.changedAccountCountry = function(countryType) {
					optionsService.getProvinceTypesOfCountryType(countryType).then(function (response) {
						$scope.provinceTypesOfAccountCountry = response.data;
					}).catch(function (data) {
						console.log('error in createEditContact.directive#changedAccountCountry: ' + data);
					});
				};
				
				$scope.changedAddressLocationCountry = function(addressWithProducts, countryType) {
					optionsService.getProvinceTypesOfCountryType(countryType).then(function (response) {
						addressWithProducts.addressLocationCountries = response.data;
					}).catch(function (data) {
						console.log('error in createEditContact.directive#changedAddressLocationCountry: ' + data);
					});
				};
				
				$scope.removeContactLocation = function(index) {
	    			$scope.contact.addressesWithProducts.splice(index, 1);
				};
				
				$scope.addContactPerson = function() {
					var contactPerson = {
					    title: '',
					    firstname: '',
					    surname: '',
					    department: '',
					    emails: [''],
					    telephones: ['']
						};
					if($scope.contact.contactPersons == null) {
						$scope.contact.contactPersons = [];
					}
					$scope.contact.contactPersons.push(contactPerson);
				};
				
				$scope.addContactPersonToAddressesWithProducts = function(addressesWithProducts) {
					var contactPerson = {
					    title: '',
					    firstname: '',
					    surname: '',
					    department: '',
					    emails: [''],
					    telephones: ['']
						};
					if(addressesWithProducts.contactPersons == null) {
						addressesWithProducts.contactPersons = [];
					}
					addressesWithProducts.contactPersons.push(contactPerson);
				};
				
				$scope.removeContactPerson = function(index) {
		    		$scope.contact.contactPersons.splice(index, 1);
				};
				
				$scope.addContactPersonEmail = function(contactPersonIndex) {
					$scope.contact.contactPersons[contactPersonIndex].emails.push('');
				};
				
				$scope.addContactPersonAddressesWithProductsEmail = function(contactPerson) {
					contactPerson.emails.push('');
				};
				
				$scope.removeContactPersonEmail = function(contactPersonIndex, index) {
		    		if($scope.contact.contactPersons[contactPersonIndex].emails.length > 1) {
		    			$scope.contact.contactPersons[contactPersonIndex].emails.splice(index, 1);
		    		}
		    	};
		    	
		    	$scope.removeContactPersonAddressesWithProductsEmail = function(addressesWithProducts, parentParentIndex, parentIndex, index) {
		    		addressesWithProducts[parentParentIndex].contactPersons[parentIndex].emails.splice(index, 1);
		    	};
		    	
		    	$scope.addContactPersonTelephone = function(contactPersonIndex) {
					$scope.contact.contactPersons[contactPersonIndex].telephones.push('');
				};  
				
				$scope.removeContactPersonAddressesWithProductsTelephone = function(addressesWithProducts, parentParentIndex, parentIndex, index) {
		    		addressesWithProducts[parentParentIndex].contactPersons[parentIndex].telephones.splice(index, 1);
		    	};
				
				$scope.addContactPersonAddressesWithProductsTelephone = function(contactPerson) {
					contactPerson.telephones.push('');
				};
				
				$scope.removeContactPersonTelephone = function(contactPersonIndex, index) {
		    		if($scope.contact.contactPersons[contactPersonIndex].telephones.length > 1) {
		    			$scope.contact.contactPersons[contactPersonIndex].telephones.splice(index, 1);
		    		}
		    	};
		    	
		    	$scope.addCustomerNumberContainer = function() {
		    		var customerNumberContainer = {};
		    		customerNumberContainer.contactTenant = 'EMPTY_COUNTRY';
		    		customerNumberContainer.customerNumber = '';
		    		if($scope.contact.customerNumberContainers == null) {
		    			$scope.contact.customerNumberContainers = [];
		    		}
		    		$scope.contact.customerNumberContainers.push(customerNumberContainer);
		    	};
		    	
		    	$scope.removeCustomerNumberContainer = function(index) {
		    		$scope.contact.customerNumberContainers.splice(index, 1);
		    	};
		    	
		    	$scope.addDeliveryNumberContainer = function() {
		    		var deliveryNumberContainer = {};
		    		deliveryNumberContainer.contactTenant = 'EMPTY_COUNTRY';
		    		deliveryNumberContainer.deliveryNumber = '';
		    		if($scope.contact.deliveryNumberContainers == null) {
		    			$scope.contact.deliveryNumberContainers = [];
		    		}
		    		$scope.contact.deliveryNumberContainers.push(deliveryNumberContainer);
		    	};
		    	
		    	$scope.removeDeliveryNumberContainer = function(index) {
		    		$scope.contact.deliveryNumberContainers.splice(index, 1);
		    	};
				
				$scope.addContactEmail = function() {
					$scope.contact.emails.push('');
				};
				
				$scope.removeContactEmail = function(index) {
		    		if($scope.contact.emails.length > 1) {
		    			$scope.contact.emails.splice(index, 1);
		    		}
		    	};
				
				$scope.addContactTelephone = function() {
					$scope.contact.telephones.push('');
				};
				
				$scope.removeContactTelephone = function(index) {
		    		if($scope.contact.telephones.length > 1) {
		    			$scope.contact.telephones.splice(index, 1);
		    		}
		    	};
				
				$scope.createContact = function(contact) {
					if($scope.type == 'CREATE') {
						$scope.createContactClicked = true;
						$scope.setContactCreatedTimeout();
						$scope.removeProductObjectsFromContact(contact);
						$scope.invoker.createContact(contact);
					} else if($scope.type == 'EDIT') {
						$scope.removeProductObjectsFromContact(contact);
						$scope.invoker.updateContact(contact);
						$scope.modalInvoker.closeEditContactModal();
					}
				};
				
				$scope.removeProductObjectsFromContact = function(contact) {
					contact.products = null;
					if(contact.addressesWithProducts != null) {
						for(var i = 0; i < contact.addressesWithProducts.length; i++) {
							contact.addressesWithProducts[i].products = [];
						}
					}
				};
				
				$scope.setContactCreatedTimeout = function() {
		      		$timeout(function() {
		      			$scope.createContactClicked = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.isContactTypeSelected = function(contact, contactType) {
		      		if(contact != null && contact.contactTypes == null) {
		      			return false;
		      		}
		      		if(contact != null) {
						for(var i = 0; i < contact.contactTypes.length; i++) {
							if(contact.contactTypes[i] == contactType) {
								return true;
							}
						}
		      		}
					return false;
				};
				
				$scope.addContactType = function(contact, contactType) {
					var contactTypeStillExists = false;
					if(contact != null && contact.contactTypes == null) {
						contact.contactTypes = [];
					}
					if(contact != null) {
						for(var i = 0; i < contact.contactTypes.length; i++) {
							if(contact.contactTypes[i] == contactType) {
								contactTypeStillExists = true;
								break;
							}
						}
					}
					if(!contactTypeStillExists) {
						contact.contactTypes.push(contactType);
					}
				};
				
				$scope.removeContactType = function(contact, contactType) {
					if(contact != null) {
						for(var i = 0; i < contact.contactTypes.length; i++) {
							if(contact.contactTypes[i] == contactType) {
								contact.contactTypes.splice(i, 1);
								break;
							}
						}
					}
				};
				
				$scope.findDocumentsByTerm = function(term) {
					return documentFileService.findDocumentFilesBySearchStringOverAllProjectsAndProducts(term).then(function(response) {
						var documentFiles = response.data;
						for(var i = 0; i < documentFiles.length; i++) {
							documentFiles[i].vm = $scope;
						}
			    		return documentFiles;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createEditContact.directive#findDocumentsByTerm'); 
		  	   		 });
				};
				
				$scope.attachFile = function(selected, selectedDocumentFile, documentFileVersion) {
		    		if(!selected) {
		    			for(var i = 0; i < $scope.contact.contactAttachments.length; i++) {
		    				if($scope.contact.contactAttachments[i].filename == selectedDocumentFile.fileName && $scope.contact.contactAttachments[i].version == documentFileVersion.version) {
		    					$scope.contact.contactAttachments.splice(i, 1);
		    					break;
		    				}
		    			}
		    		} else {
		    			var contactAttachment = {};
		    			contactAttachment.documentFileId = selectedDocumentFile.id;
		    			contactAttachment.filepath = selectedDocumentFile.subFilePath + '/' + selectedDocumentFile.documentFileVersions[0].filePathName;
		    			contactAttachment.filename = selectedDocumentFile.fileName;
		    			contactAttachment.ending = selectedDocumentFile.ending; 
		    			contactAttachment.version = documentFileVersion.version;
		    			contactAttachment.originalFilename = selectedDocumentFile.fileName;
		    			contactAttachment.isImage = utilService.isImage(selectedDocumentFile.ending);
		    			if($scope.contact.contactAttachments == null) {
		    				$scope.contact.contactAttachments = [];
		    			}
		    			$scope.contact.contactAttachments.push(contactAttachment);
		    		}
		    	};
		    	
		    	$scope.removeFile = function(index) {
		    		 $scope.contact.contactAttachments.splice(index, 1);
		    	};
		    	
		    	$scope.createDocumentUrl = function(user, receipeAttachment) {		    	
		    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
		    		return 'api/filedownloads/filedownload/' + $scope.currentUser.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
		    	};
		    	
		    	$scope.showTemplateModal = function(url, receipeAttachment) {
		    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
		    			var documentFile = response.data;
		    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
		    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createEditContact.directive#showTemplateModal'); 
		  	   		 });
		    	};
		    	
		    	$scope.getProductsByTerm = function(term) {
		    		return productsService.findProductsByTerm(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createEditContact.directive#getProductsByTerm'); 
		  	   		 });
		    	};
		    	
		    	$scope.productSelected = function(selectedProduct) {
		    		if($scope.contact.products == null) {
		    			$scope.contact.products = [];
		    		}
		    		for(var i = 0; i < $scope.contact.products.length; i++) {
		    			if($scope.contact.products[i].id == selectedProduct.id) { 
		    				$scope.productStillAdded = true;
		    				$scope.setTimeout();
		    				return;
		    			}
		    		}
		    		if($scope.contact.productIds == null) {
		    			$scope.contact.productIds = [];
		    		}
		    		$scope.contact.productIds.push(selectedProduct.id);
		    		$scope.contact.products.push(selectedProduct);
		    	};
		    	
		    	$scope.productSelectedAtLocation = function(selectedProduct, addressWithProducts) {
		    		if(addressWithProducts.products == null) {
		    			addressWithProducts.products = [];
		    		}
		    		for(var i = 0; i < addressWithProducts.products.length; i++) {
		    			if(addressWithProducts.products[i].id == selectedProduct.id) { 
		    				addressWithProducts.productStillAdded = true;
		    				$scope.setAddressWithProductsTimeout(addressWithProducts);
		    				return;
		    			}
		    		}
		    		if(addressWithProducts.productIds == null) {
		    			addressWithProducts.productIds = [];
		    		}
		    		addressWithProducts.productIds.push(selectedProduct.id);
		    		addressWithProducts.products.push(selectedProduct);
		    	};
		    	
		    	$scope.removeProduct = function(index) {
		    		var product = $scope.contact.products[index];
		    		for(var i = 0; i < $scope.contact.productIds.length; i++) {
		    			if($scope.contact.productIds[i] == product.id) {
		    				$scope.contact.productIds.splice(i, 1);
		    				break;
		    			}
		    		}
		    		$scope.contact.products.splice(index, 1);
		    	};
		    	
		    	$scope.removeProductAtLocation = function(index, addressWithProducts) {
		    		var product = addressWithProducts.products[index];
		    		for(var i = 0; i < addressWithProducts.productIds.length; i++) {
		    			if(addressWithProducts.productIds[i] == product.id) {
		    				addressWithProducts.productIds.splice(i, 1);
		    				break;
		    			}
		    		}
		    		addressWithProducts.products.splice(index, 1);
		    	};
		    	
		    	$scope.setTimeout = function() { 
		      		$timeout(function() { 
		      			$scope.productStillAdded = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.setAddressWithProductsTimeout = function(addressWithProducts) { 
		      		$timeout(function() {
		      			addressWithProducts.productStillAdded = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.hideAddressWithProducts = function(addressWithProducts) {
					var address = addressWithProducts.address;
					if(address.userIdBlackList != null && address.userIdBlackList.indexOf($scope.currentUser.id) !== -1) {
						return true;
					}
					if(address.rolesBlackList != null && address.rolesBlackList.indexOf($scope.currentUser.projectUserConnectionRole) !== -1) {
						return true;
					}
					return false;
				};
		      	
		      	$scope.isRoleBlackListed = function(role, address) {
					if(address == null || address.rolesBlackList == null) {
						return false;
					}
					for(var i = 0; i < address.rolesBlackList.length; i++) {
						if(address.rolesBlackList[i] == role) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addBlackListRole = function(address, role) {
					if(address.rolesBlackList == null) {
						address.rolesBlackList = [];
					}
					address.rolesBlackList.push(role);
				};
				
				$scope.removeBlackListRole = function(address, role) {
					for(var i = 0; i < address.rolesBlackList.length; i++) {
						if(address.rolesBlackList[i] == role) {
							address.rolesBlackList.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.isBlackListed = function(user, address) {
					if(address.userIdBlackList == null) {
						return false;
					}
					for(var i = 0; i < address.userIdBlackList.length; i++) {
						if(address.userIdBlackList[i] == user.id) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addBlackListId = function(address, user) {
					if(address.userIdBlackList == null) {
						address.userIdBlackList = [];
					}
					address.userIdBlackList.push(user.id);
				};
				
				$scope.removeBlackListId = function(address, user) {
					for(var i = 0; i < address.userIdBlackList.length; i++) {
						if(address.userIdBlackList[i] == user.id) {
							address.userIdBlackList.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.addAgentNumberContainer = function(contact) {
		    		if($scope.contact.agentNumberContainers == null) {
		    			contact.agentNumberContainers = [];
		    		}
		    		var agentNumberContainer = {};
		    		agentNumberContainer.customerNumber = '';
		    		contact.agentNumberContainers.push(agentNumberContainer);
		    	};
		    	
				$scope.removeAgentNumberContainer = function(contact, index) {
					contact.agentNumberContainers.splice(index, 1);
		    	};
		    	
		    	$scope.contactTypesCheck = function(contact) {
		    		if(contact.contactTypes == null || contact.contactTypes.length == 0) {
		    			return true;
		    		}
		    		for(var i = 0; i < contact.contactTypes.length; i++) {
		    			if(contact.contactTypes[i] == 'POTENTIAL_BUYER') {
		    				return false;
		    			}
		    		}
		    		return true;
		    	};
			 }
		};
		return directive;
		
		////////////
	}
})();