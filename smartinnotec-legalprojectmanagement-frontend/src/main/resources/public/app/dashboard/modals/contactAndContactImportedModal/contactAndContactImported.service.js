(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactAndContactImportedModalService', contactAndContactImportedModalService);
        
    contactAndContactImportedModalService.$inject = ['$modal', '$stateParams', 'printingPDFService'];
    
    function contactAndContactImportedModalService($modal, $stateParams, printingPDFService) {
		var service = {
			showContactAndContactImportedModal: showContactAndContactImportedModal
		};		
		return service;
		
		////////////
				
		function showContactAndContactImportedModal(invoker, currentUser, contactAndContactImportedContainer, index, countryTypes, titles, provinceTypes, contactTypes) {
			 var contactAndContactImportedModal = $modal.open({
				controller: ContactAndContactImportedModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",  
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	contactAndContactImportedContainer: function() {
			    		return contactAndContactImportedContainer;
			    	},
			    	index: function() {
			    		return index;
			    	},
			    	countryTypes: function() {
			    		return countryTypes;
			    	},
			    	titles: function() {
			    		return titles;
			    	},
			    	provinceTypes: function() {
			    		return provinceTypes;
			    	},
			    	contactTypes: function() {
			    		return contactTypes;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/contactAndContactImportedModal/contactAndContactImported.modal.html'
			});
			return contactAndContactImportedModal;
			
			function ContactAndContactImportedModalController($modalInstance, $scope, invoker, currentUser, contactAndContactImportedContainer, index, countryTypes, titles, provinceTypes, contactTypes) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.currentUser = currentUser;
		    	vm.contactAndContactImportedContainer = contactAndContactImportedContainer;
		    	vm.index = index;
		    	vm.countryTypes = countryTypes;
		    	vm.titles = titles;
		    	vm.provinceTypes = provinceTypes;
		    	vm.contactTypes = contactTypes;
		    	vm.date = new Date();
		    	
		    	vm.printContainer = printContainer;
		    	vm.confirmContactImported = confirmContactImported;
		    	vm.closeContactAndContactImportedModal = closeContactAndContactImportedModal;
		    	
		    	////////////
		    	
		    	function printContainer(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	}
		    	
		    	function confirmContactImported() {
		    		vm.invoker.confirmContactImported(vm.contactAndContactImportedContainer, vm.index);
		    		vm.closeContactAndContactImportedModal();
		    	}
		    	
		    	function closeContactAndContactImportedModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();