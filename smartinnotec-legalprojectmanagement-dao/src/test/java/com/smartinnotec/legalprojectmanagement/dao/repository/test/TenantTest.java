package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.dao.DuplicateKeyException;

import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public class TenantTest extends AbstractDAOTest {

    @Test
    public void shouldCreateTenant() {
        tenantRepository.deleteAll();

        final Tenant tenant = new Tenant();
        tenant.setName("TestTenant");
        final Tenant createdTenant = tenantRepository.save(tenant);
        Assert.assertNotNull(createdTenant);
        Assert.assertEquals("TestTenant", createdTenant.getName());

        final Tenant tenant2 = new Tenant();
        tenant2.setName("TestTenant2");
        final Tenant createdTenant2 = tenantRepository.save(tenant2);
        Assert.assertNotNull(createdTenant2);
        Assert.assertEquals("TestTenant2", createdTenant2.getName());
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldCreateTenantFailed() {
        tenantRepository.deleteAll();

        final Tenant tenant = new Tenant();
        tenant.setName("TestTenant");
        final Tenant createdTenant = tenantRepository.save(tenant);
        Assert.assertNotNull(createdTenant);
        Assert.assertEquals("TestTenant", createdTenant.getName());

        final Tenant tenant2 = new Tenant();
        tenant2.setName("TestTenant");
        final Tenant createdTenant2 = tenantRepository.save(tenant2);
        Assert.assertNotNull(createdTenant2);
        Assert.assertEquals("TestTenant", createdTenant2.getName());
    }

}
