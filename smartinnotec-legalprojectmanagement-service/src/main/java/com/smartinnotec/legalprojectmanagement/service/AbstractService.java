package com.smartinnotec.legalprojectmanagement.service;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties(locations = "classpath:application.properties", ignoreUnknownFields = true, prefix = "legalprojectmanagement.generalServiceConfig")
public @Data abstract class AbstractService {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected final DateTimeFormatter dateFormatter;
    protected final DateTimeFormatter timeFormatter;
    protected final DateTimeFormatter dateTimeFormatter;
    protected final DateTimeFormatter dateTimeFormatter2;
    protected final DateTimeFormatter dateTimeFormatterStandard1;
    protected final DateTimeFormatter dateTimeFormatterStandard2;
    protected final DateTimeFormatter dateTimeFormatterStandard3;
    protected PeriodFormatter periodFormatter;
    protected String uploadPath;
    protected Integer contactImportedSize;
    protected Integer contactToExportSize;
    protected Integer projectPageSize;
    protected Integer historyPageSize;
    protected Integer contactPageSize;
    protected Integer documentPageSize;
    protected Integer fuelPageSize;
    protected Integer workingBookPageSize;
    protected Integer productPageSize;
    protected String testEmailPrefix;
    protected String suppliers;
    protected String addresses;

    public AbstractService() {
        dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        timeFormatter = DateTimeFormat.forPattern("HH:mm");
        dateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        dateTimeFormatter2 = DateTimeFormat.forPattern("yyyyMMdd_HHmmss");
        dateTimeFormatterStandard1 = DateTimeFormat.forPattern("yyyyMMdd'T'HHmmss'Z'");
        dateTimeFormatterStandard2 = DateTimeFormat.forPattern("yyyyMMdd'T'HHmmss");
        dateTimeFormatterStandard3 = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");
        periodFormatter = new PeriodFormatterBuilder().printZeroAlways().minimumPrintedDigits(2).appendHours().appendSeparator(":")
            .appendMinutes().rejectSignedValues(false).maximumParsedDigits(2).toFormatter();
    }
}
