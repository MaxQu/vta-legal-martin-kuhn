package com.smartinnotec.legalprojectmanagement.dao.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;
import lombok.Data;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Fuel")
public @Data class Fuel {
    @Id
    private String id;

    /*@DBRef
    private Contact supplierContact;

    @DBRef
    private Project project;*/


    // TODO make REF's
    private String supplierContact;
    private String project;

    // TODO standort (aus Projekt)
    // private Location location;
    private String location;

    // TODO
    // @DBRef
    // private Product product
    private String product;


    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime deliveryDate;

    private String deliveryNote;

    // TODO artikel (aus admin brennstoff verwaltung)

    private String quantity;

    private String amount;

    @DBRef
    private Tenant tenant;

    private boolean paid;

    private String additionalInfo;

    @DBRef
    private User user;



    @Override
    public boolean equals(final Object obj) {
        final Contact contact = (Contact)obj;
        if (this.getId().equals(contact.getId())) {
            return true;
        }
        return false;
    }


}
