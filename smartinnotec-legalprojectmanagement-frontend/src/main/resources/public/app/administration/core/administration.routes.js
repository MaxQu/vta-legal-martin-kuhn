(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.administration')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getAdministrationState());
    	 
    	////////////
			    	
    	function getAdministrationState() {
    		var state = {
    			name: 'auth.administration',
				url: '/administration/:userId',
				templateUrl: 'app/administration/administration/administration.html',
				controller: 'AdministrationController',
				controllerAs: 'vm',
				resolve: {
					optionsService: 'optionsService',
					projectsService: 'projectsService',
					labelsPredefinedService: 'labelsPredefinedService',
					calendarEventScheduleDaysService: 'calendarEventScheduleDaysService',
					allLabelsPredefined: function getAllLabelsPredefined($stateParams, labelsPredefinedService) {
						return [];
					},
			    	userRoles: function getUserRoles(optionsService) {
			    		return optionsService.getUserRoles();
			    	},
			    	allUsers: function findAllUsers(userService) {
			    		return userService.findAllUsers();
			    	},
			    	projectUserConnectionRoles: function getProjectUserConnectionRoles(optionsService) {
			    		return optionsService.getRoles();
			    	},
			    	userAuthorizationUserContainer: function($stateParams, userService) {
			    		return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
			    	},
			    	calendarEventScheduleDays: function($stateParams, calendarEventScheduleDaysService) {
			    		return calendarEventScheduleDaysService.findCalendarEventScheduleDaysByTenant($stateParams.userId);
			    	},
					projects: function getAllProjects(projectsService) {
						return projectsService.findAllProjects();
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					},
					countryTypes: function getCountryTypes(optionsService) {
						return optionsService.getCountryTypes();
					}
				}
    		};
    		return state;
    	}
	}
})();