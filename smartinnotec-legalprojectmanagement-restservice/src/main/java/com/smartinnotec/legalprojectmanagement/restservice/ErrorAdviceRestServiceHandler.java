package com.smartinnotec.legalprojectmanagement.restservice;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;

@ControllerAdvice
public class ErrorAdviceRestServiceHandler {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public ErrorAdviceRestServiceHandler() {
    }

    // returns exactly the same error- json (with timestamp, error, message, ....) like spring boot does
    @ExceptionHandler({ Exception.class, BusinessException.class })
    public @ResponseBody ErrorAdviceHolder handleBadRequests(final HttpServletResponse response, final Exception e) throws IOException {
        logger.info("handle request error '{}' with exception name '{}' in ErrorAdviceRestServiceHandler#handleBadRequests", e.getMessage(),
            e.getClass().getSimpleName());

        final ErrorAdviceHolder errorAdviceHolder = new ErrorAdviceHolder();
        errorAdviceHolder.setMessage(e.getMessage());
        errorAdviceHolder.setError(e.getMessage());
        errorAdviceHolder.setException(e.getClass().getSimpleName());
        errorAdviceHolder.setPath("");
        errorAdviceHolder.setTimestamp(new Timestamp(System.currentTimeMillis()));

        switch (e.getClass().getSimpleName()) {
        case "HttpMessageNotReadableException":
        case "HttpRequestMethodNotSupportedException":
        case "HttpMediaTypeNotSupportedException":
            errorAdviceHolder.setStatus(HttpStatus.BAD_REQUEST.toString());
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            break;
        case "BusinessException":
            if (((BusinessException)e).getErrorCode() == null) {
                return errorAdviceHolder;
            }
            switch (((BusinessException)e).getErrorCode()) {
            case "400":
                errorAdviceHolder.setStatus(HttpStatus.BAD_REQUEST.toString());
                errorAdviceHolder.setErrorCodeType(((BusinessException)e).getErrorCodeType());
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                break;
            case "401":
                errorAdviceHolder.setStatus(HttpStatus.UNAUTHORIZED.toString());
                errorAdviceHolder.setErrorCodeType(((BusinessException)e).getErrorCodeType());
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                break;
            case "403":
                errorAdviceHolder.setStatus(HttpStatus.FORBIDDEN.toString());
                errorAdviceHolder.setErrorCodeType(((BusinessException)e).getErrorCodeType());
                response.setStatus(HttpStatus.FORBIDDEN.value());
                break;
            case "404":
                errorAdviceHolder.setStatus(HttpStatus.NOT_FOUND.toString());
                errorAdviceHolder.setErrorCodeType(((BusinessException)e).getErrorCodeType());
                response.setStatus(HttpStatus.NOT_FOUND.value());
                break;
            default:
                System.out.println(((BusinessException)e).getErrorCode());
                errorAdviceHolder.setStatus(HttpStatus.FORBIDDEN.toString());
                errorAdviceHolder.setErrorCodeType(((BusinessException)e).getErrorCodeType());
                response.setStatus(HttpStatus.FORBIDDEN.value());
            }
            break;
        default:
            errorAdviceHolder.setStatus(HttpStatus.FORBIDDEN.toString());
            response.setStatus(HttpStatus.FORBIDDEN.value());
        }

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return errorAdviceHolder;
    }

    @Override
    public String toString() {
        return "[ErrorAdviceRestServiceHandler]";
    }
}
