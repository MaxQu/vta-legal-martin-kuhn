(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('asExcelDownload', asExcelDownload);
    
	function asExcelDownload() {
		var directive = {
			restrict: 'E',
			scope: {
				projectId: '=',
				start: '=',
				end: '='
			},
			templateUrl: 'app/common/excelDownload/asExcelDownload.html'
		};
		return directive;
		
		////////////
	}
})();



