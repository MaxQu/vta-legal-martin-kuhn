package com.smartinnotec.legalprojectmanagement.service;

public class CommunicationMessageUtil {

    public CommunicationMessageUtil() {
    }

    public static String getTitle(final String message) {
        if (message.contains("\n")) {
            return message.substring(0, message.indexOf("\n"));
        } else {
            return "";
        }
    }

    public static String getMessageBody(final String message, final int titleLength) {
        final StringBuilder messageBody = new StringBuilder();
        if (message.contains("\n")) {
            messageBody.append(message.substring(titleLength));
        } else {
            messageBody.append(message);
        }
        return messageBody.toString();
    }

    @Override
    public String toString() {
        return "[ChatMessageUtil]";
    }
}
