package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum UserRoleEnum {

    SUPER_ADMIN("SUPER_ADMIN", 1), // can see all and also delete all
    ADMIN("ADMIN", 2), // can see all but is not allowed to delete all
    USER("USER", 4), // normale Mitarbeiter: sieht von den anderen die Dienstpläne aber nur von seiner Apo
    USER_DEACTIVATED("USER_DEACTIVATED", 5),
    GUEST("GUEST", 6), // Demo account
    OFFICE_PERSONNEL("OFFICE_PERSONNEL", 7); // Bürokraft

    private String value;
    private Integer order;

    private UserRoleEnum(final String value, final Integer order) {
        this.value = value;
        this.order = order;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(final Integer order) {
        this.order = order;
    }
}
