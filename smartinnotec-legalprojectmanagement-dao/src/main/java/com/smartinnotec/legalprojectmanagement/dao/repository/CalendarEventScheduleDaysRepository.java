package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventScheduleDays;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface CalendarEventScheduleDaysRepository extends MongoRepository<CalendarEventScheduleDays, String> {

    List<CalendarEventScheduleDays> findByTenant(final Tenant tenant);

}
