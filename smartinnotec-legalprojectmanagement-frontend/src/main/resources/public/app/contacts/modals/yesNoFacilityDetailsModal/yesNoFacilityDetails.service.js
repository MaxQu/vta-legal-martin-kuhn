(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoFacilityDetailsContactModalService', yesNoFacilityDetailsContactModalService);
        
    yesNoFacilityDetailsContactModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoFacilityDetailsContactModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, contact, facilityDetails) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contact: function() {
			    		return contact;
			    	},
			    	facilityDetails: function() {
			    		return facilityDetails;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/yesNoFacilityDetailsModal/yesNoFacilityDetails.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, contact, facilityDetails) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contact = contact;
		    	vm.facilityDetails = facilityDetails;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.removeFacilityDetail(vm.facilityDetails);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();