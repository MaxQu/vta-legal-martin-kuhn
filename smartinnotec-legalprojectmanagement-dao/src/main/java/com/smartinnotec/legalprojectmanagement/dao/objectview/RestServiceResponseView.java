package com.smartinnotec.legalprojectmanagement.dao.objectview;

public class RestServiceResponseView {

    public interface RolePublic {
    }

    public interface UserPublic extends TenantPublic {
    }

    public interface AddressPublic {
    }

    public interface UserPublicAndAddress extends UserPublic, AddressPublic {
    }

    public interface ProjectPublic {
    }
    
    public interface ProductPublic {
    }

    public interface ProjectUserConnectionPublic {
    }

    public interface TenantPublic {
    }

    @Override
    public String toString() {
        return "[RestServiceResponseView]";
    }
}
