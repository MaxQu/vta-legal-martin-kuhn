(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('receipeService', receipeService);
        
    receipeService.$inject = ['$http', 'api_config'];
    
    function receipeService($http, api_config) {
		var service = {
			createReceipe: createReceipe,
			updateReceipe: updateReceipe,
			findReceipesOfProductOfTypeInRange: findReceipesOfProductOfTypeInRange,
			findAllReceipesOfProductOfType: findAllReceipesOfProductOfType,
			findLastReceipesOfProductOfType: findLastReceipesOfProductOfType,
			deleteReceipe: deleteReceipe
		};
		
		return service;
		
		////////////
		
		function createReceipe(receipe) {
			return $http.post(api_config.BASE_URL + '/receipes/receipe', receipe);
		}
		
		function updateReceipe(receipe) {
			return $http.put(api_config.BASE_URL + '/receipes/receipe', receipe);
		}
		
		function findReceipesOfProductOfTypeInRange(productId, receipeType, start, end) {
			return $http.get(api_config.BASE_URL + '/receipes/receipe/' + productId + '/' + receipeType + '/' + start + '/' + end + '/');
		}
		
		function findAllReceipesOfProductOfType(productId, receipeType) {
			return $http.get(api_config.BASE_URL + '/receipes/receipe/' + productId + '/' + receipeType);
		}
		
		function findLastReceipesOfProductOfType(productId, receipeType) {
			return $http.get(api_config.BASE_URL + '/receipes/receipe/' + productId + '/' + receipeType + '/last');
		}
		
		function deleteReceipe(receipe) {
			return $http.delete(api_config.BASE_URL + '/receipes/receipe/' + receipe.id);
		}
    }
})();
