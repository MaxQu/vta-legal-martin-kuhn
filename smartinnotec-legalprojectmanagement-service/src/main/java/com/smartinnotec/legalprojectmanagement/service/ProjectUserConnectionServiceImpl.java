package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ProjectUserConnectionRepository;

@Service("projectUserConnectionService")
public class ProjectUserConnectionServiceImpl extends AbstractService implements ProjectUserConnectionService {

    @Autowired
    private ContactService contactService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectUserConnectionRepository projectUserConnectionRepository;
    @Autowired
    private DocumentFileService documentFileService;

    @Override
    public ProjectUserConnection findProjectUserConnection(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ProjectUserConnectionServiceImpl#findProjectUserConnection", "400");
        BusinessAssert.isId(id, "id must be an id in ProjectUserConnectionServiceImpl#findProjectUserConnection", "400");
        logger.info("find projectUserConnection of id '{}' in ProjectUserConnectionServiceImpl#findProjectUserConnection", id);
        return projectUserConnectionRepository.findOne(id);
    }

    @Override
    public ProjectUserConnection findProjectUserConnectionByUserAndProject(final User user, final Project project)
            throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ProjectUserConnectionServiceImpl#findProjectUserConnectionByUserAndProject",
            "400");
        BusinessAssert.notNull(project,
            "project is mandatory in ProjectUserConnectionServiceImpl#findProjectUserConnectionByUserAndProject", "400");
        logger.info(
            "find projectUserConnection by user with id '{}' and project with id '{}' in ProjectUserConnectionServiceImpl#findProjectUserConnectionByUserAndProject",
            user.getId(), project.getId());
        return projectUserConnectionRepository.findByUserAndProjectAndActive(user, project, true);
    }

    @Override
    public List<ProjectUserConnection> findByUserAndActive(final User user, final boolean active) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ProjectUserConnectionServiceImpl#findByUser", "400");
        logger.info("find projectUserConnections by user with id '{}' in ProjectUserConnectionServiceImpl#findByUser", user.getId());
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionRepository.findByUserAndActiveAndContactIsNull(user,
            active);
        // remove generalAvailable ProjectUserConnection references
        // generalAvailable projects are handled extra
        projectUserConnections.removeIf(s -> s.getProject().getGeneralAvailable() == true);
        logger.info("found '{}' projectUserConnections by user with id '{}' in ProjectUserConnectionServiceImpl#findByUser",
            projectUserConnections.size(), user.getId());
        return projectUserConnections;
    }

    @Override
    public List<ProjectUserConnection> findByProjectAndActive(final Project project, final boolean active) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in ProjectUserConnectionServiceImpl#findByProjectAndActive", "400");
        logger.info("find projectUserConnections by user with id '{}' in ProjectUserConnectionServiceImpl#findByProjectAndActive",
            project.getId());
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionRepository.findByProjectAndActive(project, active);
        logger.info("found '{}' projectUserConnections by user with id '{}' in ProjectUserConnectionServiceImpl#findByProjectAndActive",
            projectUserConnections.size(), project.getId());
        return projectUserConnections;
    }

    @Override
    public List<ProjectUserConnection> findProjectUserConnectionsOfContactAndProject(final String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId,
            "contactId is mandatory in ProjectUserConnectionServiceImpl#findProjectUserConnectionsOfContactAndProject", "400");
        BusinessAssert.isId(contactId,
            "contactId is mandatory in ProjectUserConnectionServiceImpl#findProjectUserConnectionsOfContactAndProject", "400");
        logger.info(
            "find projectUserConnections of contact with id '{}' in ProjectUserConnectionServiceImpl#findProjectUserConnectionsOfContactAndProject",
            contactId);
        final Contact contact = contactService.findContactById(contactId);
        return projectUserConnectionRepository.findByContactAndUserIsNull(contact);
    }

    @Override
    public List<ProjectUserConnection> findProjectUserConnectionsOfContactAndUser(final String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId,
            "contactId is mandatory in ProjectUserConnectionServiceImpl#findProjectUserConnectionsOfContactAndUser", "400");
        BusinessAssert.isId(contactId,
            "contactId is mandatory in ProjectUserConnectionServiceImpl#findProjectUserConnectionsOfContactAndUser", "400");
        logger.info(
            "find projectUserConnections of contact with id '{}' in ProjectUserConnectionServiceImpl#findProjectUserConnectionsOfContactAndUser",
            contactId);
        final Contact contact = contactService.findContactById(contactId);
        return projectUserConnectionRepository.findByContactAndProjectIsNull(contact);
    }

    @Override
    public ProjectUserConnection findByProjectAndContactAndActive(final Project project, final Contact contact, boolean active)
            throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in ProjectUserConnectionServiceImpl#findByProjectAndContactAndActive", "400");
        BusinessAssert.notNull(contact, "contact is mandatory in ProjectUserConnectionServiceImpl#findByProjectAndContactAndActive", "400");
        logger.info(
            "find projectUserConnections of project with id '{}' and contact with id '{}' in ProjectUserConnectionServiceImpl#findByProjectAndContactAndActive",
            project.getId(), contact.getId());
        return projectUserConnectionRepository.findByProjectAndContactAndActive(project, contact, active);
    }

    @Override
    public ProjectUserConnection findByUserAndContactAndActive(final User user, final Contact contact, final boolean active)
            throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ProjectUserConnectionServiceImpl#findByUserAndContactAndActive", "400");
        BusinessAssert.notNull(contact, "contact is mandatory in ProjectUserConnectionServiceImpl#findByUserAndContactAndActive", "400");
        logger.info(
            "find projectUserConnections of user with id '{}' and contact with id '{}' in ProjectUserConnectionServiceImpl#findByUserAndContactAndActive",
            user.getId(), contact.getId());
        return projectUserConnectionRepository.findByUserAndContactAndActive(user, contact, active);
    }

    @Override
    public List<ProjectUserConnection> findProjectUserConnectionsOfUserByProjectSearchString(final String userId, final String searchTerm)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in ProjectServiceImpl#findProjectOfUserByProjectSearchString", "400");
        BusinessAssert.isId(userId, "userId must be an id in ProjectServiceImpl#findProjectOfUserByProjectSearchString", "400");
        BusinessAssert.notNull(searchTerm, "searchTerm is mandatory in ProjectServiceImpl#findProjectOfUserByProjectSearchString", "400");
        logger.info("find projects of user with id '{}' and searchTerm '{}' in ProjectServiceImpl#findProjectOfUserByProjectSearchString",
            userId, searchTerm);
        final User user = userService.findUser(userId);
        final List<Project> projects = projectService.findProjectByTerm(searchTerm);

        final List<ProjectUserConnection> projectUserConnections = new ArrayList<>();
        for (final Iterator<Project> iterator = projects.iterator(); iterator.hasNext();) {
            final Project project = iterator.next();
            final ProjectUserConnection projectUserConnection = findProjectUserConnectionByUserAndProject(user, project);
            if (projectUserConnection != null) {
                projectUserConnections.add(projectUserConnection);
            }
        }
        return projectUserConnections;
    }

    @Override
    public List<ProjectUserConnection> findProjectUserConnectionsByUserAndContactIsNotNull(final User user) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ProjectServiceImpl#findProjectUserConnectionsByUserAndContactIsNotNull", "400");
        logger.info(
            "find projectUserConnections by user with id '{}' and contact is not null in ProjectServiceImpl#findProjectUserConnectionsByUserAndContactIsNotNull",
            user.getId());
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionRepository
            .findByUserAndActiveAndContactIsNotNull(user, true);
        return projectUserConnections;
    }

    @Override
    public ProjectUserConnection create(final ProjectUserConnection projectUserConnection) throws BusinessException {
        BusinessAssert.notNull(projectUserConnection, "projectUserConnection is mandatory in ProjectUserConnectionServiceImpl#create",
            "400");
        logger.info(
            "create projectUserConnection for user or contact with id '{}' and project with id '{}' in ProjectUserConnectionServiceImpl#create",
            projectUserConnection.getUser() != null ? projectUserConnection.getUser().getId() : projectUserConnection.getContact().getId(),
            projectUserConnection.getProject() != null ? projectUserConnection.getProject().getId() : null);
        final ProjectUserConnection createdProjectUserConnection = projectUserConnectionRepository.insert(projectUserConnection);
        return createdProjectUserConnection;
    }

    @Override
    public ProjectUserConnection update(final ProjectUserConnection projectUserConnection) throws BusinessException {
        BusinessAssert.notNull(projectUserConnection, "projectUserConnection is mandatory in ProjectUserConnectionServiceImpl#update",
            "400");
        logger.info("update projectUserConnection with id '{}' in ProjectUserConnectionServiceImpl#update", projectUserConnection.getId());
        return projectUserConnectionRepository.save(projectUserConnection);
    }

    @Override
    public void delete(final String projectUserConnectionId, final User user) throws BusinessException {
        BusinessAssert.notNull(projectUserConnectionId, "projectUserConnectionId is mandatory in ProjectUserConnectionServiceImpl#delete",
            "400");
        BusinessAssert.isId(projectUserConnectionId, "projectUserConnectionId must an id in ProjectUserConnectionServiceImpl#delete",
            "400");
        final ProjectUserConnection projectUserConnection = this.findProjectUserConnection(projectUserConnectionId);
        if (projectUserConnection.getUser() != null && projectUserConnection.getProject() != null) {
            final List<DocumentFile> assignedDocumentFiles = documentFileService
                .findAssignedDocumentFilesByProjectId(projectUserConnection.getProject().getId(), projectUserConnection.getUser().getId());
            if (assignedDocumentFiles == null || !assignedDocumentFiles.isEmpty()) {
                throw new BusinessException("ASSIGNED_DOCUMENTS_TO_USER", "400");
            }
        }
        projectUserConnectionRepository.delete(projectUserConnectionId);
        if (user != null) {
            user.removeProjectUserConnectionId(projectUserConnectionId);
            final User updatedUser = userService.updateUser(user);
        }
        logger.info("delete projectUserConnection with id '{}' in ProjectUserConnectionServiceImpl#delete", projectUserConnectionId);
    }

    @Override
    public boolean canProjectUserConnectionRoleSeeConfidentialDocumentFiles(final ProjectUserConnectionRoleEnum projectUserConnectionRole)
            throws BusinessException {
        if (projectUserConnectionRole == null) {
            return false;
        }
        logger.info(
            "can projectUserConnection with role '{}' see cofidential documentFiles in ProjectUserConnectionServiceImpl#canProjectUserConnectionRoleSeeConfidentialDocumentFiles",
            projectUserConnectionRole);
        switch (projectUserConnectionRole) {
        case ADMIN:
        case GENERAL_MANAGER:
        case PROJECT_MANAGER:
        case LABOR:
            return true;
        case PROJECT_WORKER:
        case PRODUCTION:
        case MARKETING:
        case CUSTOMER_SERVICE:
        case EXTERNAL_WORKER:
        case GUEST:
        case CONTACT:
            return false;
        }
        return false;
    }

    @Override
    public String toString() {
        return "[ProjectUserConnectionServiceImpl]";
    }
}
