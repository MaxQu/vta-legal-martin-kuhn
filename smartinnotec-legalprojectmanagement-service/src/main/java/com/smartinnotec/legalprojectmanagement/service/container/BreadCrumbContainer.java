package com.smartinnotec.legalprojectmanagement.service.container;

import java.util.ArrayList;
import java.util.List;

import com.smartinnotec.legalprojectmanagement.dao.domain.Folder;

import lombok.Data;

public @Data class BreadCrumbContainer {

    private List<Folder> breadCrumbOfFolders;
    private List<String> breadCrumbStringOfFolders;

    public void addBreadCrumbStringOfFolders(final String breadCrumb) {
        if (breadCrumbStringOfFolders == null) {
            breadCrumbStringOfFolders = new ArrayList<>();
        }
        breadCrumbStringOfFolders.add(breadCrumb);
    }

}
