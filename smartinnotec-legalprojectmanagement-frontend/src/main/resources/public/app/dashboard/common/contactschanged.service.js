(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactsChangedService', contactsChangedService);
        
    contactsChangedService.$inject = ['$http', 'api_config'];
    
    function contactsChangedService($http, api_config) {
		var service = {
			getPagedContactsChangedByCommitted: getPagedContactsChangedByCommitted,
			getAmountOfContactsChanged: getAmountOfContactsChanged,
			getAmountOfContactsChangedByCommitted: getAmountOfContactsChangedByCommitted,
			getContactsChangedByCommitted: getContactsChangedByCommitted,
			commitContactChanged: commitContactChanged,
			deleteContactChanged: deleteContactChanged
		};
		
		return service;
		
		////////////
		
		function getPagedContactsChangedByCommitted(page, committed) {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchange/' + page + '/' + committed);
		}
		
		function getAmountOfContactsChanged() {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchanged/amount');
		}
		
		function getAmountOfContactsChangedByCommitted(committed) {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchanged/amount/' + committed);
		}
		
		function getContactsChangedByCommitted(committed) {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchanged/' + committed);
		}
		
		function commitContactChanged(id) {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchanged/commit/' + id);
		}
		
		function deleteContactChanged(id) {
			return $http.delete(api_config.BASE_URL + '/contactschanged/contactchanged/' + id);
		}
    }
})();
