package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.ContactChangedService;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class ContactChangedRestService extends AbstractRestService {

    @Autowired
    private ContactChangedService contactChangedService;
    @Autowired
    private UserService userService;
    @Autowired
    private HistoryService historyService;

    @RequestMapping(value = "/contactschanged/contactchanged/amount", method = { RequestMethod.GET }, produces = "application/json")
    public Integer getAmountOfContactsChanged() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": get amount of contacts changed in ContactChangedRestService#getAmountOfContactsChanged");
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        return contactChangedService.getAmountOfContactsChanged(user.getTenant());
    }

    @RequestMapping(value = "/contactschanged/contactchanged/amount/{committed}", method = {
        RequestMethod.GET }, produces = "application/json")
    public Integer getAmountOfContactsChangedByCommitted(final @PathVariable("committed") Boolean committed) throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": get amount of contacts changed in ContactChangedRestService#getAmountOfContactsChangedByCommitted");
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        final Integer amount = contactChangedService.getAmountOfContactsChangedByCommitted(user.getTenant(), committed);
        return amount;
    }

    @RequestMapping(value = "/contactschanged/contactchange/{page}/{committed}", method = {
        RequestMethod.GET }, produces = "application/json")
    public @ResponseBody List<ContactChanged> getPagedContactsChangedByCommitted(final @PathVariable("page") Integer page,
            final @PathVariable("committed") Boolean committed)
            throws BusinessException {
        BusinessAssert.notNull(page, "page is mandatory in ContactChangedRestService#getPagedContactsChanged", "400");
        BusinessAssert.notNull(committed, "committed is mandatory in ContactChangedRestService#getPagedContactsChanged", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": get paged contacts changed starting of page '{}' in ContactChangedRestService#getPagedContactsChanged", page);
        final List<ContactChanged> contactsChanged = contactChangedService.findPagedContactsChanged(page, committed);
        return contactsChanged;
    }

    @RequestMapping(value = "/contactschanged/contactchanged/{committed}", method = { RequestMethod.GET }, produces = "application/json")
    public List<ContactChanged> getContactsChangedByCommitted(final @PathVariable("committed") Boolean committed) throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": get contact changed by committed '{}' in ContactChangedRestService#getContactsChangedByCommitted", committed);
        return contactChangedService.getContactsChangedByCommitted(committed);
    }

    @RequestMapping(value = "/contactschanged/contactchanged/commit/{id}", method = { RequestMethod.GET }, produces = "application/json")
    public ContactChanged commitContactChanged(final @PathVariable("id") String id) throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": commit contact changed with id '{}' in ContactChangedRestService#commitContactChanged", id);

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final ContactChanged contactChanged = contactChangedService.findById(id);
        contactChanged.setCommitted(true);
        final ContactChanged updatedContactChanged = contactChangedService.update(contactChanged, user);

        final String message = updatedContactChanged.getInstitution();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.COMMITED_CONTACT_CHANGED)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ContactChangedRestService#commitContactChanged", createdHistory.getId());

        return updatedContactChanged;
    }

    @RequestMapping(value = "/contactschanged/contactchanged/{id}", method = { RequestMethod.DELETE }, produces = "application/json")
    public void deleteContactChanged(final @PathVariable("id") String id) throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": delete contact changed with id '{}' in ContactChangedRestService#deleteContactChanged", id);
        final ContactChanged contactChanged = contactChangedService.findById(id);
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final String message = contactChanged.getInstitution();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DELETED_CONTACT_CHANGED)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ContactChangedRestService#deleteContactChanged", createdHistory.getId());

        contactChangedService.delete(id);
    }

    @Override
    public String toString() {
        return "[ContactChangedRestService]";
    }
}
