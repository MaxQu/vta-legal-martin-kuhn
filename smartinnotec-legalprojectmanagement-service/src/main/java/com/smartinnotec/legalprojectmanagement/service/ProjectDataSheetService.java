package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectDataSheet;

public interface ProjectDataSheetService {

    ProjectDataSheet create(final ProjectDataSheet projectDataSheet) throws BusinessException;

    ProjectDataSheet update(final ProjectDataSheet projectDataSheet) throws BusinessException;

    ProjectDataSheet findProjectDataSheetByProjectId(final String projectId) throws BusinessException;

    void delete(final String projectId) throws BusinessException;

}
