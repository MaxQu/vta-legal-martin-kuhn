package com.smartinnotec.legalprojectmanagement.dao.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;

import lombok.Data;

public @Data class AgentNumberContainer {

    @JsonView(RestServiceResponseView.UserPublicAndAddress.class)
    private String customerNumber;

    public AgentNumberContainer deepCopy() {
        final AgentNumberContainer copiedAgentNumberContainer = new AgentNumberContainer();
        copiedAgentNumberContainer.setCustomerNumber(this.getCustomerNumber());
        return copiedAgentNumberContainer;
    }

    @Override
    public boolean equals(final Object obj) {
        final AgentNumberContainer agentNumberContainer = (AgentNumberContainer)obj;
        if (agentNumberContainer.getCustomerNumber().equals(this.customerNumber)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public String toString() {
        return "[customerNumber:" + customerNumber + "]";
    }
}
