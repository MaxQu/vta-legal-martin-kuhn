(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.user')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	$stateProvider
		    .state(getUserProfileState())
            .state(getUserSettingsState());
    	
    	////////////

	    function getUserProfileState() {
		    var state = {
			    name: 'auth.userprofile',
			    url: '/userprofile/:userId',
			    templateUrl: 'app/user/profile/userProfile.html',
			    controller: 'UserProfileController',
			    controllerAs: 'vm',
			    resolve: {
			    	optionsService: 'optionsService',
			    	userService: 'userService',
			    	communicationModes: function getCommunicationModes(optionsService) {
			    		return optionsService.getCommunicationModes();
			    	},
			    	user: function getCurrentUser(userService, $stateParams) {
			    		return userService.getUserById($stateParams.userId);
			    	}
				}
		    };
		    return state;
	    }

	    function getUserSettingsState() {
		    var state = {
			    name: 'auth.usersettings',
			    url: '/usersettings',
			    templateUrl: 'app/user/settings/usersettings.html',
			    controller: 'UserSettingsController',
			    controllerAs: 'vm',
			    resolve: {
				}
		    };

		    return state;
	    }
	}
})();