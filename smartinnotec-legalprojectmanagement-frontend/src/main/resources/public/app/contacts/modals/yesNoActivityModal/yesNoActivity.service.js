(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoActivityContactModalService', yesNoActivityContactModalService);
        
    yesNoActivityContactModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoActivityContactModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, contact, activity) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contact: function() {
			    		return contact;
			    	},
			    	activity: function() {
			    		return activity;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/yesNoActivityModal/yesNoActivity.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, contact, activity) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contact = contact;
		    	vm.activity = activity;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.removeActivity(vm.activity);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();