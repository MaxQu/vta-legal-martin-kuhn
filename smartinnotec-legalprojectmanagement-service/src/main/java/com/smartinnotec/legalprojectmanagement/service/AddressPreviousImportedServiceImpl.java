package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressPreviousImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.repository.AddressPreviousImportedRepository;

@Service("addressPreviousImportedService")
public class AddressPreviousImportedServiceImpl extends AbstractService implements AddressPreviousImportedService {

    @Autowired
    private AddressPreviousImportedRepository addressPreviousImportedRepository;

    @Override
    public AddressPreviousImported create(final AddressPreviousImported addressPreviousImported) throws BusinessException {
        BusinessAssert.notNull(addressPreviousImported, "addressPreviousImported is mandatory in AddressPreviousImportedServiceImpl#create",
            "400");
        logger.info("create addressPreviousImported in AddressPreviousImportedServiceImpl#create");
        final AddressPreviousImported createdAddressPreviousImported = addressPreviousImportedRepository.insert(addressPreviousImported);
        return createdAddressPreviousImported;
    }

    @Override
    public AddressPreviousImported convertFromAddressImportedToAddressPreviousImported(final AddressImported addressImported)
            throws BusinessException {
        BusinessAssert.notNull(addressImported,
            "addressImported is mandatory in AddressPreviousImportedServiceImpl#convertFromAddressImportedToAddressPreviousImported",
            "400");
        logger.info(
            "convert from addressImported with id '{}' to addressPreviousImported in AddressPreviousImportedServiceImpl#convertFromAddressImportedToAddressPreviousImported",
            addressImported.getId());

        final AddressPreviousImported addressPreviousImported = new AddressPreviousImported();
        addressPreviousImported.setFlfId(addressImported.getFlfId());
        addressPreviousImported.setFlfIdChanged(addressImported.isFlfIdChanged());
        addressPreviousImported.setAdditionalInformation(addressImported.getAdditionalInformation());
        addressPreviousImported.setAdditionalInformationChanged(addressImported.isAdditionalInformationChanged());

        final List<CustomerNumberContainer> customerNumberContainers = new ArrayList<>();
        if (addressImported.getCustomerNumberContainers() != null) {
            for (final CustomerNumberContainer customerNumberContainer : addressImported.getCustomerNumberContainers()) {
                final CustomerNumberContainer copiedCustomerNumberContainer = customerNumberContainer.deepCopy();
                customerNumberContainers.add(copiedCustomerNumberContainer);
            }
        }
        addressPreviousImported.setCustomerNumberContainers(customerNumberContainers);
        addressPreviousImported.setCustomerNumberContainersChanged(addressImported.isCustomerNumberContainersChanged());
        addressPreviousImported.setStreet(addressImported.getStreet());
        addressPreviousImported.setStreetChanged(addressImported.isStreetChanged());
        addressPreviousImported.setIntSign(addressImported.getIntSign());
        addressPreviousImported.setIntSignChanged(addressImported.isIntSignChanged());
        addressPreviousImported.setPostalCode(addressImported.getPostalCode());
        addressPreviousImported.setPostalCodeChanged(addressImported.isPostalCodeChanged());
        addressPreviousImported.setRegion(addressImported.getRegion());
        addressPreviousImported.setRegionChanged(addressImported.isRegionChanged());
        addressPreviousImported.setProvinceType(addressImported.getProvinceType());
        addressPreviousImported.setProvinceTypeChanged(addressImported.isProvinceTypeChanged());
        addressPreviousImported.setCountry(addressImported.getCountry());
        addressPreviousImported.setCountryChanged(addressImported.isCountryChanged());
        addressPreviousImported.setTelephone(addressImported.getTelephone());
        addressPreviousImported.setTelephoneChanged(addressImported.isTelephoneChanged());
        addressPreviousImported.setEmail(addressImported.getEmail());
        addressPreviousImported.setEmailChanged(addressImported.isEmailChanged());
        addressPreviousImported.setInformation(addressImported.getInformation());
        addressPreviousImported.setInformationChanged(addressImported.isInformationChanged());

        return addressPreviousImported;
    }

    @Override
    public String toString() {
        return "[AddressPreviousImportedServiceImpl]";
    }
}
