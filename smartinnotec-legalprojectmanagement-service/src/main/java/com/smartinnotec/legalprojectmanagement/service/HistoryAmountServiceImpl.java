package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryAmount;
import com.smartinnotec.legalprojectmanagement.dao.repository.HistoryAmountRepository;

@Service("historyAmountService")
public class HistoryAmountServiceImpl extends AbstractService implements HistoryAmountService {

    @Autowired
    private HistoryAmountRepository historyAmountRepository;

    @Override
    public HistoryAmount create(final HistoryAmount historyAmount) throws BusinessException {
        BusinessAssert.notNull(historyAmount, "historyAmount is mandatory in HistoryAmountServiceImpl#create", "400");
        logger.info("create historyAmount in HistoryAmountServiceImpl#create");
        return historyAmountRepository.save(historyAmount);
    }
    
    @Override
	public List<HistoryAmount> getHistoryAmountsBetweenDates(final DateTime start, final DateTime end) throws BusinessException {
    	BusinessAssert.notNull(start, "start is mandatory in HistoryAmountServiceImpl#getHistoryAmountsBetweenDates", "400");
    	BusinessAssert.notNull(end, "end is mandatory in HistoryAmountServiceImpl#getHistoryAmountsBetweenDates", "400");
    	logger.info("get historyAmount between '{}' and '{}' in HistoryAmountServiceImpl#getHistoryAmountsBetweenDates", start, end);
    	return historyAmountRepository.getHistoryAmountByDateBetween(start, end);
	}

    @Override
    public String toString() {
        return "[HistoryAmountServiceImpl]";
    }
}
