package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.Folder;

public interface FolderRepository extends MongoRepository<Folder, String> {

    List<Folder> findFolderByProjectIdAndSuperFolderId(final String projectId, final String superFolderId);

    @Query("{$text : { $search : ?0 }}")
    List<Folder> findBySearchString(final String searchString);

    @Query("{$text : { $search : ?0 }, 'projectId' : ?1 }")
    List<Folder> findBySearchStringAndProjectId(final String searchString, final String projectId);

    @Query("{$text : { $search : ?0 }, 'projectId' : ?1, 'superFolderId' : ?2 }")
    List<Folder> findBySearchStringAndProjectIdAndSuperFolderId(final String searchString, final String projectId,
            final String superFolderId);

    List<Folder> findByProjectId(final String projectId);

    List<Folder> findBySuperFolderId(final String superFolderId);

    List<Folder> findByNameAndSuperFolderId(final String name, final String superFolderId);

}
