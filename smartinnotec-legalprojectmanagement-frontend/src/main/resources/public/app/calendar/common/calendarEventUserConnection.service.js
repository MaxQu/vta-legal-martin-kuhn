(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarEventUserConnectionService', calendarEventUserConnectionService);
        
    calendarEventUserConnectionService.$inject = ['$http', 'api_config'];
    
    function calendarEventUserConnectionService($http, api_config) {
		var service = {
			create: create,
			update: update,
			getPreparedCalendarEventsOfUser: getPreparedCalendarEventsOfUser,
			getPreparedCalendarEventsOfAllUsers: getPreparedCalendarEventsOfAllUsers,
			getPreparedCalendarEventsOfAllUsersInRange: getPreparedCalendarEventsOfAllUsersInRange,
			getPreparedCalendarEventsOfUserOfNextWeeks: getPreparedCalendarEventsOfUserOfNextWeeks,
			findCalendarEventUserConnectionsByCalendarEvent: findCalendarEventUserConnectionsByCalendarEvent,
			findCalendarEventUserConnectionsByContact: findCalendarEventUserConnectionsByContact
		};
		
		return service;
		
		////////////
		
		function create(calendarEventId, calendarEventUserConnection, creationIndex) {   
			return $http.post(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + calendarEventId + '/' + creationIndex, calendarEventUserConnection);
		}
		
		function update(calendarEventId, calendarEventUserConnection) {
			return $http.put(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + calendarEventId, calendarEventUserConnection);
		}
		
		function getPreparedCalendarEventsOfUser(userId, calendarViewType, day, month, year, archived) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + userId + '/' + calendarViewType + '/' + day + '/' + month + '/' + year + '/' + archived);
		}	
		
		function getPreparedCalendarEventsOfAllUsersInRange(userId, start, end) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + userId + '/' + start + '/' + end + '/all');
		}
		
		function getPreparedCalendarEventsOfAllUsers(userId, calendarViewType, day, month, year) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + userId + '/' + calendarViewType + '/' + day + '/' + month + '/' + year + '/all');
		}	
		
		function getPreparedCalendarEventsOfUserOfNextWeeks(userId, amountOfWeeks) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + userId + '/' + amountOfWeeks);
		}	
		
		function findCalendarEventUserConnectionsByCalendarEvent(calendarEventId) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + calendarEventId);
		}	
		
		function findCalendarEventUserConnectionsByContact(contactId) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + contactId + '/contact');
		}	
    }
})();