package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventSerialDateTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.FacilityDetails;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class FacilityDetailsServiceTest extends AbstractServiceTest {

    @Test
    public void shouldGetFacilityDetailOfPreviousCalendarEvent() throws BusinessException {

        final User user1 = new User();
        user1.setFirstname("firstname");
        user1.setSurname("surname");
        final User createdUser1 = userService.createUser(user1);
        Assert.assertNotNull(createdUser1);

        final Contact contact1 = new Contact();
        contact1.setInstitution("TestKontakt1");
        final Contact createdContact1 = contactService.create(contact1, user1, false);
        Assert.assertNotNull(createdContact1);

        /////////////////////////// CalendarEvent 1 ///////////////////////
        final CalendarEvent calendarEvent1 = new CalendarEvent();
        calendarEvent1.setTitle("TestTermin1");
        calendarEvent1.setLocation("TestLocation1");
        calendarEvent1.setStartsAt(formatter.parseDateTime("05.05.2018 10:00").getMillis());
        calendarEvent1.setEndsAt(formatter.parseDateTime("05.05.2018 11:00").getMillis());
        final List<CalendarEvent> createdCalendarEvents1 = calendarEventService.create(calendarEvent1, createdUser1, false, null,
            CalendarEventSerialDateTypeEnum.NO_SERIAL_DATE);
        Assert.assertNotNull(createdCalendarEvents1);

        final CalendarEventUserConnection calendarEventUserConnectionOfContact1 = new CalendarEventUserConnection();
        calendarEventUserConnectionOfContact1.setCalendarEvent(createdCalendarEvents1.get(0));
        calendarEventUserConnectionOfContact1.setContact(createdContact1);
    }
}
