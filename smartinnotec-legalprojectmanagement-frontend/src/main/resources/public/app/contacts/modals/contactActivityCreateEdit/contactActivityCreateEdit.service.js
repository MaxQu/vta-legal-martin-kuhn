(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactActivityCreateEditModalService', contactActivityCreateEditModalService);
        
    contactActivityCreateEditModalService.$inject = ['$modal', '$timeout', '$stateParams', 'dateUtilityService', 'activityService'];
    
    function contactActivityCreateEditModalService($modal, $timeout, $stateParams, dateUtilityService, activityService) {
		var service = {
			showContactActivityCreateEditModal: showContactActivityCreateEditModal
		};		
		return service;
		
		////////////
				
		function showContactActivityCreateEditModal(contact, activity, activityTypes) {
			 var contactActivityCreateEditModal = $modal.open({
				controller: ContactActivityCreateEditModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {   
			    	contact: function() {
			    		return contact; 
			    	}, 
			    	activity: function() {
			    		return activity;
			    	}, 
			    	activityTypes: function() {
			    		return activityTypes;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/contactActivityCreateEdit/contactActivityCreateEdit.modal.html'
			});
			return contactActivityCreateEditModal;
			
			function ContactActivityCreateEditModalController($modalInstance, $scope, contact, activity, activityTypes) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.activity = activity;
		    	vm.activityTypes = activityTypes;
		    	vm.activityUpdatedError = false;
		    	
		    	vm.addOrRemoveActivityType = addOrRemoveActivityType;
		    	vm.updateActivity = updateActivity;
		    	vm.isActivityTypeSelected = isActivityTypeSelected;
		    	vm.cancelFile = cancelFile;
		    	vm.closeContactActivityCreateEditModal = closeContactActivityCreateEditModal;
		    	
		    	////////////

		    	function setTimeout() {
		      		$timeout(function() {
				    	vm.activityUpdatedError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function isActivityTypeSelected(activityType) {
		    		for(var i = 0; i < vm.activity.activityTypes.length; i++) {
		    			if(vm.activity.activityTypes[i] === activityType) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function updateActivity() {
		    		activityService.update(vm.activity).then(function(response) {
		    			closeContactActivityCreateEditModal();
		  	   		 }, function errorCallback(response) {
		  	   			 vm.activityUpdatedError = true;
			  	   		 setTimeout();
		  	   			console.log('error contactActivityCreateEdit.service#updateActivity');
		  	   		 });
		    	}
		    	
		    	function addOrRemoveActivityType(activityType) {
		    		var removed = false;
		    		for(var i = 0; i < vm.activity.activityTypes.length; i++) {
		    			if(vm.activity.activityTypes[i] === activityType) {
		    				vm.activity.activityTypes.splice(i, i);
		    				removed = true;
		    				break;
		    			}
		    		}
		    		if(removed === false) {
		    			vm.activity.activityTypes.push(activityType);
		    		}
		    	}
		    	
		    	$scope.getFileNameWithoutExtension = function(activityDocumentFile) {
					var fileName = activityDocumentFile.fileName;
					var fileNameWithoutEnding = activityDocumentFile.fileName;
					var fileNameEnding = fileName.substr(fileName.lastIndexOf('.')+1, fileName.length);
			        if(fileNameEnding.length === 3) {
			            fileNameWithoutEnding = fileName.substr(0, fileName.lastIndexOf('.'));
			            return fileNameWithoutEnding;
			        } else {
			        	return fileName;
			        }
				};
				
				$scope.getFileNameExtension = function(activityDocumentFile) {
					var fileName = activityDocumentFile.fileName;
					var fileNameEnding = fileName.substr(fileName.lastIndexOf('.')+1, fileName.length);
			        if(fileNameEnding.length === 3) {
			            return fileNameEnding;
			        } else {
			        	return 'NO_ENDING';
			        }
				};
				
				function cancelFile(activityDocumentFile) {
					for(var i = 0; i < vm.activity.activityDocumentFiles.length; i++) {
						if(vm.activity.activityDocumentFiles[i].originalFileName === activityDocumentFile.originalFileName) {
							vm.activity.activityDocumentFiles.splice(i, 1);
							break;
						}
					}
				}
		    	
		    	function closeContactActivityCreateEditModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();