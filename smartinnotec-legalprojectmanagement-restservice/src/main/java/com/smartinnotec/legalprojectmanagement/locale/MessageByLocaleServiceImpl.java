package com.smartinnotec.legalprojectmanagement.locale;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class MessageByLocaleServiceImpl implements MessageByLocaleService {

    @Autowired
    private MessageSource messageSource;

    public MessageByLocaleServiceImpl() {
    }

    @Override
    public String getMessage(final String key) {
        final Locale locale = LocaleContextHolder.getLocale();
        final String message = messageSource.getMessage(key, null,
            "key of message not found in messages.properties file in MessageByLocaleServiceImpl#getMessage", locale);
        return message;
    }

    @Override
    public Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }

    @Override
    public String toString() {
        return "[MessageByLocaleServiceImpl]";
    }
}
