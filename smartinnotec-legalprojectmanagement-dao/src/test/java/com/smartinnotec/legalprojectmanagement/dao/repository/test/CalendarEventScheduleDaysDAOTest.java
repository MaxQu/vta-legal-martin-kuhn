package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventScheduleDays;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public class CalendarEventScheduleDaysDAOTest extends AbstractDAOTest {

    @Test
    public void shouldCreateCalendarEventScheduleDays() {

        final Tenant tenant = new Tenant();
        tenant.setName("TestTenant3");
        final Tenant createdTenant = tenantRepository.insert(tenant);
        Assert.assertNotNull(createdTenant);

        final CalendarEventScheduleDays calendarEventScheduleDays = new CalendarEventScheduleDays();
        calendarEventScheduleDays.setTenant(tenant);
        calendarEventScheduleDays.setDays(1);
        final CalendarEventScheduleDays createdCalendarEventScheduleDays = calendarEventScheduleDaysRepository
            .insert(calendarEventScheduleDays);
        Assert.assertNotNull(createdCalendarEventScheduleDays);

    }

}
