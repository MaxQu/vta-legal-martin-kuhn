(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectsService', projectsService);
        
    projectsService.$inject = ['$http', 'api_config'];
    
    function projectsService($http, api_config) {
		var service = {
			createProject: createProject,
			createProjectDataSheet: createProjectDataSheet,
			updateProjectDataSheet: updateProjectDataSheet,
			deleteProjectDataSheet: deleteProjectDataSheet,
			findProjectDataSheet: findProjectDataSheet,
			findProject: findProject,
			findProjectByTerm: findProjectByTerm,
			findAllProjects: findAllProjects,
			findProjectsOfUser: findProjectsOfUser,
			findPreparedProject: findPreparedProject,
			findProjectsGeneralAvailable: findProjectsGeneralAvailable,
			getDefaultProject: getDefaultProject,
			updateProject: updateProject,
			getProjectPageSize: getProjectPageSize,
			getAllProjectsOfTenant: getAllProjectsOfTenant,
			deleteProject: deleteProject 
		};
		
		return service;
		
		////////////
		
		function createProject(project, createCalendarEntry) {
			return $http.post(api_config.BASE_URL + '/projects/project/' + createCalendarEntry, project);
		}
		
		function createProjectDataSheet(projectDataSheet) {
			return $http.post(api_config.BASE_URL + '/projects/project/projectdatasheet', projectDataSheet);
		}
		
		function updateProjectDataSheet(projectDataSheet) {
			return $http.put(api_config.BASE_URL + '/projects/project/projectdatasheet', projectDataSheet);
		}
		
		function findProjectDataSheet(projectId) {
			return $http.get(api_config.BASE_URL + '/projects/project/projectdatasheet/' + projectId);
		}
		
		function deleteProjectDataSheet(projectId) {
			return $http.delete(api_config.BASE_URL + '/projects/project/projectdatasheet/' + projectId);
		}
		
		function findProject(projectId) {
			return $http.get(api_config.BASE_URL + '/projects/project/' + projectId);
		}
		
		function findAllProjects() {
			return $http.get(api_config.BASE_URL + '/projects/project');
		}
		
		function findProjectsOfUser(userId) {
			return $http.get(api_config.BASE_URL + '/projects/project/user/' + userId);
		}
		
		function findPreparedProject(projectId) {
			return $http.get(api_config.BASE_URL + '/projects/project/' + projectId + '/prepared');
		}
		
		function findProjectByTerm(searchTerm) {
			return $http.get(api_config.BASE_URL + '/projects/project/' + searchTerm + '/search');
		}
		
		function findProjectsGeneralAvailable() {
			return $http.get(api_config.BASE_URL + '/projects/project/generalaccess');
		}
		
		function getDefaultProject(userId) {
			return $http.get(api_config.BASE_URL + '/projects/project/default/' + userId);
		}
		
		function updateProject(project) {
			return $http.put(api_config.BASE_URL + '/projects/project', project);
		}
		
		function getProjectPageSize() {
			return $http.get(api_config.BASE_URL + '/projects/project/projectpagesize');
		}
		
		function getAllProjectsOfTenant() {
			return $http.get(api_config.BASE_URL + '/projects/project/all');
		}
		
		function deleteProject(projectId) {
			return $http.delete(api_config.BASE_URL + '/projects/project/' + projectId);
		}
    }
})();
