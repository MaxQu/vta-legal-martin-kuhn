(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.accesscontrol')
    	.factory('accessControlService', accessControlService);
        
    accessControlService.$inject = ['$http', 'api_config'];
    
    function accessControlService($http, api_config) {
		var service = {
			startListening: startListening,
			stopListening: stopListening,
			getAllAccessControls: getAllAccessControls,
			getAccessControlsInRange: getAccessControlsInRange
		};
		
		return service;
		 
		////////////
		
		function startListening() {
			return $http.get(api_config.BASE_URL + '/accesscontrols/accesscontrol/start');
		}
		
		function stopListening() {
			return $http.get(api_config.BASE_URL + '/accesscontrols/accesscontrol/stop');
		}
		
		function getAllAccessControls() {
			return $http.get(api_config.BASE_URL + '/accesscontrols/accesscontrol');
		}
		
		function getAccessControlsInRange(start, end) {
			return $http.get(api_config.BASE_URL + '/accesscontrols/accesscontrol/' + start + '/' + end + '/');
		}
    }
})();