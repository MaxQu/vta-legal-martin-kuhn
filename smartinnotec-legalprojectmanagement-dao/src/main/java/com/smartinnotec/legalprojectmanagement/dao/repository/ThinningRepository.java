package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Thinning;

public interface ThinningRepository extends MongoRepository<Thinning, String> {

    List<Thinning> findByProductId(final String productId);

    List<Thinning> findByProductIdAndDateBetween(final String productId, final DateTime start, final DateTime end);

    Thinning findByProductIdOrderByDateDesc(final String productId);

    Long countByProductId(final String productId);

}
