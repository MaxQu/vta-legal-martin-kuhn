package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;

public interface DocumentFileRepository extends MongoRepository<DocumentFile, String> {

    List<DocumentFile> findByProjectProductId(final String projectProductId);

    // list all documentfiles of product in adm
    List<DocumentFile> findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(final String projectProductId, final String userId, final boolean confident, final boolean active);
    
    DocumentFile findByProjectProductIdAndFileName(final String projectProductId, final String fileName);

    DocumentFile findByProjectProductIdAndFileNameAndFolderId(final String projectProductId, final String fileName, final String folderId);

    List<DocumentFile> findPagedDocumentFilesByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(final String projectProductId,
            final String userId, final boolean confident, final boolean active, final Pageable pageable);

    Integer countByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(final String projectProductId, final String userId,
            final boolean confident, final boolean active);

    Integer countByProjectProductIdAndUserIdBlackListNotInAndFolderIdAndConfidentAndActive(final String projectProductId,
            final String userId, final boolean confident, final boolean active);

    List<DocumentFile> findPagedDocumentFilesByProjectProductIdAndFolderIdAndUserIdBlackListNotInAndConfidentAndActiveOrUserIdWhiteListIn(
            final String projectProductId, final String folderId, final String userIdInBlackList, final boolean confident,
            final boolean active, final String userIdInWhiteList, final Pageable pageable);

    Integer countByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActiveOrUserIdWhiteListIn(final String projectProductId,
            final String userIdInBlackList, final boolean confident, final boolean active, final String userIdInWhiteList);

    Integer countByProjectProductIdAndUserIdBlackListNotInAndFolderIdAndConfidentAndActiveOrUserIdWhiteListIn(final String projectProductId,
            final String userIdInBlackList, final String folderId, final boolean confident, final boolean active,
            final String userIdInWhiteList);

    List<DocumentFile> findPagedDocumentFilesByProjectProductIdAndFolderIdAndUserIdBlackListNotInAndActive(final String projectProductId,
            final String folderId, final String userId, final boolean active, final Pageable pageable);

    List<DocumentFile> findPagedAndConfidentDocumentFilesByProjectProductIdAndFolderIdAndUserIdBlackListNotInAndConfidentAndActive(
            final String projectProductId, final String folderId, final String userId, final boolean confident, final boolean active,
            final Pageable pageable);

    List<DocumentFile> findDocumentFilesByProjectProductIdAndUserIdWhiteListInAndActive(final String projectProductId, final String userId,
            final boolean active);

    List<DocumentFile> findDocumentFilesByProjectProductIdAndUserIdWhiteListNotInAndConfidentAndActive(final String projectProductId,
            final String userId, final boolean confident, final boolean active);

    Integer countByProjectProductIdAndUserIdBlackListNotInAndActive(final String projectProductId, final String userId,
            final boolean active);

    Integer countByProjectProductIdAndUserIdBlackListNotInAndFolderIdAndActive(final String projectProductId, final String userId,
            final String folderId, final boolean active);

    @Query("{$text : { $search : ?0 }}")
    List<DocumentFile> findBySearchString(final String searchString);

    @Query("{$text : { $search : ?0 }, 'projectProductId' : ?1 }")
    List<DocumentFile> findBySearchStringAndProjectProductId(final String searchString, final String projectProductId);

    @Query("{'fileName' : { $regex : ?0, $options:'i' } }")
    List<DocumentFile> findByRegexString(final String searchString);

    @Query("{$text : { $search : ?0 }, 'projectProductId' : ?1, 'folderId' : ?2 }")
    List<DocumentFile> findBySearchStringAndProjectProductIdAndFolderId(final String searchString, final String projectProductId,
            final String folderId);

}
