(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('printModalService', printModalService);
        
    printModalService.$inject = ['$modal', '$stateParams'];
    
    function printModalService($modal, $stateParams) {
		var service = {
			showPrintModal: showPrintModal
		};		
		return service;
		
		////////////
				
		function showPrintModal(product, receipeOfProduct, receipeType, currentUser) {   
			 var printModal = $modal.open({
				controller: PrintModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	},
			    	receipeOfProduct: function() {
			    		return receipeOfProduct;
			    	}, 
			    	receipeType: function() {
			    		return receipeType;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}
			    }, 
				templateUrl: 'app/products/modals/printModal/print.modal.html'
			});
			return printModal;
			
			function PrintModalController($modalInstance, $scope, product, receipeOfProduct, receipeType, currentUser) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.receipeOfProduct = receipeOfProduct;
		    	vm.receipeType = receipeType;
		    	vm.currentUser = currentUser;
		    	vm.urlsWithMetadata = [];
		    	vm.documentName = null;
		    	vm.ending = null;
		    	vm.version = null;
		    	vm.date = new Date();
		    	createDocumentUrl();
		    	
		    	vm.closePrintModal = closePrintModal;
		    	
		    	////////////
		    	
		    	function createDocumentUrl() { 
		    		if(vm.receipeOfProduct.receipeAttachments == null || vm.receipeOfProduct.receipeAttachments.length == 0) {
		    			return;
		    		}
		    		for(var i = 0; i < vm.receipeOfProduct.receipeAttachments.length; i++) {
		    			var version = vm.receipeOfProduct.receipeAttachments[i].version === undefined ? '_Version1' : vm.receipeOfProduct.receipeAttachments[i].version;
		    			var url = 'api/filedownloads/filedownload/' + currentUser.id + '/' + vm.receipeOfProduct.receipeAttachments[i].documentFileId + '/' + vm.receipeOfProduct.receipeAttachments[i].version + '/';
		    			var preparedUrl = preparePdfUrl(url, vm.receipeOfProduct.receipeAttachments[i].ending);
		    			var urlWithMetadata = {};
		    			urlWithMetadata.url = preparedUrl;
		    			urlWithMetadata.documentName = vm.receipeOfProduct.receipeAttachments[i].filename;
		    			urlWithMetadata.ending = vm.receipeOfProduct.receipeAttachments[i].ending;
		    			urlWithMetadata.version = vm.receipeOfProduct.receipeAttachments[i].version;
		    			vm.urlsWithMetadata.push(urlWithMetadata);
		    		}
		    		$scope.pdfUrl = vm.urlsWithMetadata[0].url; 
		    		vm.documentName = vm.urlsWithMetadata[0].documentName;
			    	vm.ending = vm.urlsWithMetadata[0].ending;
			    	vm.version = vm.urlsWithMetadata[0].version;
		    	}
		    	
		    	function preparePdfUrl(pdfUrl, ending) {
		    		if(ending.indexOf('pdf') === -1 && ending.indexOf('PDF') === -1) {
		    			return pdfUrl + 'previewpdf';
		    		}
		    		return pdfUrl;
		    	}
		    	
		    	function closePrintModal() {
		    		$modalInstance.dismiss('cancel');  
		    	}
			} 
		}
    }
})();