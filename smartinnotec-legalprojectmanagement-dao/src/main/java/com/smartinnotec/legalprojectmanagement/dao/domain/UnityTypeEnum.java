package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum UnityTypeEnum {

    LITER("LITER"),
    GRAM("GRAM");

    private String value;

    private UnityTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
