(function() {
    'use strict';
    
    
    angular
    	.module('legalprojectmanagement.history')
    	.controller('HistoryController', HistoryController);
    
    HistoryController.$inject = ['$scope', 'histories', 'startDate', 'endDate', 'historyPageSize', 'amountOfPages', 'historyService', 'dateUtilityService', 'printingPDFService'];
       
    function HistoryController($scope, histories, startDate, endDate, historyPageSize, amountOfPages, historyService, dateUtilityService, printingPDFService) {
	    $scope.vm = this;  
    	var vm = this;
    	
    	vm.histories = histories.data;
    	vm.historyPageSize = historyPageSize.data;
    	vm.amountOfPages = amountOfPages.data;
    	vm.historyFromTrigger = false;
    	vm.historyUntilTrigger = false;
    	vm.historyFrom = startDate;
    	vm.historyUntil = endDate;
    	vm.historyError = false;
    	vm.currentPage = 0;
    	
    	vm.printContainer = printContainer;
    	vm.pageChange = pageChange;
    	vm.isCurrentPage = isCurrentPage;
    	vm.arrayRang = arrayRang;
    	vm.openHistoryFrom = openHistoryFrom;
    	vm.openHistoryUntil = openHistoryUntil;
    	vm.getHistory = getHistory;
    	vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    	
    	////////////
    	
    	$scope.$watch('vm.historyFrom', function(newValue, oldValue) {
    		if(oldValue != newValue && newValue != null) {
    			if(vm.historyFrom != null && vm.historyUntil != null) {
    				getHistory();
    			}
    		}
		});
    	
    	$scope.$watch('vm.historyUntil', function(newValue, oldValue) {
    		if(oldValue != newValue && newValue != null) {
    			if(vm.historyFrom != null && vm.historyUntil != null) {
    				getHistory();
    			}
    		}
		});
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function openHistoryFrom($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.historyFromTrigger = true;
		}
    	
    	function openHistoryUntil($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.historyUntilTrigger = true;
		}
    	
    	function isCurrentPage(indexHistory) {
    		if(indexHistory == (vm.currentPage+1)) {
    			return true;
    		}
    		return false; 
    	}
    	
    	function pageChange(page) {
    		if(page < 0 || page > vm.amountOfPagesArray.length-1) {
    			return;
    		}
    		var formattedDateFrom = dateUtilityService.formatDateToString(vm.historyFrom);
    		var formattedDateUntil = dateUtilityService.formatDateToString(vm.historyUntil);
    		historyService.findPagedHistoryBetweenDates(formattedDateFrom, formattedDateUntil, page).then(function successCallback(response) {
    			vm.histories = response.data;
    			vm.currentPage = page;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error history.controller.js#pageChange');
  	   		 });
    	}
    	
    	function getHistory() {
    		if(vm.historyFrom == null || vm.historyUntil == null) {
    			vm.historyError = true;
    			return;
    		}
    		vm.currentPage = 0;
    		vm.historyError = false;
    		var formattedDateFrom = dateUtilityService.formatDateToString(vm.historyFrom);
    		var formattedDateUntil = dateUtilityService.formatDateToString(vm.historyUntil);
    		historyService.calculateAmountOfPages(formattedDateFrom, formattedDateUntil).then(function (response) {
    			vm.amountOfPages = response.data;
    			vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    		}).catch(function (data) {
				console.log('error history.controller#calculateAmountOfPages: ' + data);
			});
    		historyService.findPagedHistoryBetweenDates(formattedDateFrom, formattedDateUntil, 0).then(function (response) {
    			vm.histories = response.data;
    		}).catch(function (data) {
				console.log('error history.controller#findPagedHistoryBetweenDates: ' + data);
			});
    	}
    	
    	function arrayRang(min, max, step) {
    	    step = step || 1;
    	    var input = [];
    	    for (var i = min; i <= max; i += step) {
    	        input.push(i);
    	    }
    	    return input;
    	}
	} 
})();