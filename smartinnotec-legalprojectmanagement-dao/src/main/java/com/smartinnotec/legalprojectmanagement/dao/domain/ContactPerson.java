package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.springframework.data.mongodb.core.index.TextIndexed;

import lombok.Data;

public @Data class ContactPerson {

    @TextIndexed
    private String title;
    @TextIndexed
    private String firstname;
    @TextIndexed
    private String surname;
    @TextIndexed
    private List<String> emails;
    @TextIndexed
    private List<String> telephones;
    @TextIndexed
    private String department;
    @TextIndexed
    private String additionalInformation;

}
