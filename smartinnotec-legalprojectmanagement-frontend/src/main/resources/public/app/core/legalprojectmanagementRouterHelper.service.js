(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement')
    	.factory('RouterHelper', RouterHelper);
    
    RouterHelper.$inject = ['$stateProvider', '$urlRouterProvider', '$state'];

    function RouterHelper($stateProvider, $urlRouterProvider, $state) {

        console.log('RouterHelper');
        var hasOtherwise = false;

        var service = {
    		addState: addState,
    		configureOtherwise: configureOtherwise,
            getStates: getStates
        };

        return service;

        ///////////////


        function addState(stateConfig) {
            console.log('stateConfig', stateConfig);
        	return $stateProvider.state(stateConfig);
        }
        
        function configureOtherwise(otherwisePath) {
            if (otherwisePath && !hasOtherwise) {
                hasOtherwise = true;
                $urlRouterProvider.otherwise(otherwisePath);
            }
        }

        function getStates() { 
        	return $state.get(); 
        }
    }
})();
