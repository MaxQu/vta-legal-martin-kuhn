package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.Data;

public @Data class ContactPreviousImportedAddressImported {

    @DBRef
    private AddressPreviousImported addressPreviousImported;

    @Override
    public boolean equals(final Object obj) {
        final ContactPreviousImportedAddressImported contactImportedAddressImported = (ContactPreviousImportedAddressImported)obj;
        if (this.addressPreviousImported == null || contactImportedAddressImported.getAddressPreviousImported() == null) {
            return false;
        } else if (this.addressPreviousImported.getId().equals(contactImportedAddressImported.getAddressPreviousImported().getId())) {
            return true;
        }
        return false;
    }
}
