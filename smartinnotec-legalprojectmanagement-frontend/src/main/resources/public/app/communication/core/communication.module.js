(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();