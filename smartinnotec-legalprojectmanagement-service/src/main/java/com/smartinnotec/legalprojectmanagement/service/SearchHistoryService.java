package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.SearchHistory;

public interface SearchHistoryService {

    SearchHistory createOrUpdateSearchHistory(final SearchHistory searchHistory) throws BusinessException;

    List<SearchHistory> findPagedSearchHistoryOrderBySearchDateDesc(final String userId) throws BusinessException;

}
