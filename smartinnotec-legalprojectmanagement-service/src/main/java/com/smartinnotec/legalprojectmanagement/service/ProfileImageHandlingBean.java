package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(locations = "classpath:application.properties", ignoreUnknownFields = true, prefix = "legalprojectmanagement.profileimage")
public @Data class ProfileImageHandlingBean {

    private String profileUploadPath;
    private String profileFolderPrefixName;

}
