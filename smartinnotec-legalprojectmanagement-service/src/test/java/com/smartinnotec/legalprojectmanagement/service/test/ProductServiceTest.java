package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class ProductServiceTest extends AbstractServiceTest {

    @Test
    public void shouldSearchProductBySubString() throws BusinessException {

        final Tenant tenant = new Tenant();
        tenant.setName("TestTenant");
        final Tenant createdTenant = tenantService.create(tenant);
        Assert.assertNotNull(createdTenant);

        final Product product1 = new Product();
        product1.setName("Wander");
        product1.setDescription("Das ist das Test Produkt 1");
        product1.setTenant(createdTenant);
        final Product createdProduct1 = productService.create(product1);
        Assert.assertNotNull(createdProduct1);

        final Product product2 = new Product();
        product2.setName("Wunder baum");
        product2.setDescription("Ein different Produkt 2");
        product2.setTenant(createdTenant);
        final Product createdProduct2 = productService.create(product2);
        Assert.assertNotNull(createdProduct2);

        final List<Product> allProducts = productService.findAllProductsByTenant(createdTenant);
        Assert.assertEquals(2, allProducts.size());

        final List<Product> products = productService.findProductsByRegex("Wand^");
        Assert.assertEquals(1, products.size());
    }
}
