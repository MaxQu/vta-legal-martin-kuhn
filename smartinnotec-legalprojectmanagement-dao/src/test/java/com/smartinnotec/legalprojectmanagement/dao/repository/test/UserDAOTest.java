package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public class UserDAOTest extends AbstractDAOTest {

    @Test
    public void shouldGetUserOfAddresses() {
        int count = 0;
        final List<User> users = userRepository.findAll();
        final List<String> addressIds = new ArrayList<>();
        final List<User> foundedUsers = new ArrayList<>();
        addressIds.add("59f746d0c2dc8211734e5ab9");
        addressIds.add("5b47500dc2dc0371165522be");
        addressIds.add("5b486803c2dc0371165522eb");
        addressIds.add("5b83f5dbc2dc01e6d220933b");
        addressIds.add("5b843830c2dc01e6d22093a4");
        addressIds.add("5b9f9164c2dc01e6d2209d74");
        for (final User user : users) {
            final Address mainAddress = user.getAddress();
            if (mainAddress != null && addressIds.contains(mainAddress.getId())) {
                foundedUsers.add(user);
                count++;
            }
        }
        Assert.assertEquals(0, foundedUsers.size());
    }

    @Test
    public void shouldGetUser() {
        final String userId = "5589929b887dc1fdb501cdba";
        final User user = userRepository.findOne(userId);
        Assert.assertNotNull(user);
    }

}
