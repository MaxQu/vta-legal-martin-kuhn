package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "CommunicationGroup")
public @Data class CommunicationGroup {

    @Id
    private String id;
    private String name;
    private String description;
    private String color;
    @DBRef
    private User userCreatedMessageGroup;

}
