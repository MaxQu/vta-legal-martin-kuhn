(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarEventClickedModalService', calendarEventClickedModalService);
        
    calendarEventClickedModalService.$inject = ['$modal', '$stateParams'];
    
    function calendarEventClickedModalService($modal, $stateParams) {
		var service = {
			showCalendarEventClickedModal: showCalendarEventClickedModal
		};		
		return service;
		
		////////////
				
		function showCalendarEventClickedModal(calendarEvent) {
			 var calendarEventClickedModal = $modal.open({
				controller: CalendarEventClickedModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEvent: function() {
			    		return calendarEvent;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/calendarEventClicked/calendarEventClicked.modal.html'
			});
			return calendarEventClickedModal;
			
			function CalendarEventClickedModalController($modalInstance, $scope, calendarEvent) {
		    	var vm = this;
		    	vm.calendarEvent = calendarEvent;
		    	
		    	vm.closeCalendarEventClickedModal = closeCalendarEventClickedModal;
		    	
		    	function closeCalendarEventClickedModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();