(function() {
    'use strict'; 

    angular
    	.module('legalprojectmanagement.dashboard')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getDashboardState());
    	 
    	////////////
			    	
    	function getDashboardState() {
    		var state = {
    			name: 'auth.dashboard',
				url: '/dashboard/:userId',
				templateUrl: 'app/dashboard/dashboard/dashboard.html',
				controller: 'DashboardController',
				controllerAs: 'vm',
				resolve: {
					projectsService: 'projectsService',
					calendarEventUserConnectionService: 'calendarEventUserConnectionService',
					authUserService: 'authUserService',
					calendarService: 'calendarService',
					contactsService: 'contactsService',
					contactsChangedService: 'contactsChangedService',
					communicationService: 'communicationService',
					contactsImportedService: 'contactsImportedService',
					contactsPreviousImportedService: 'contactsPreviousImportedService',
					amountOfContacts: function countByTenantAndActive($stateParams, contactsService) {
						return contactsService.countByTenantAndActive($stateParams.userId);
					},
					calendarEventsOfNextWeeks: function calendarEventsOfNextWeeks($stateParams, calendarEventUserConnectionService) {
						return null;
					},
					communicationUserConnectionsSent: function findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived($stateParams, communicationService) {
						return null;
					},
					communicationUserConnectionsReceived: function findCommunicationUserConnectionByUserCreatedMessageNotAndAndUserReceivedMessageAndTenantAndArchived($stateParams, communicationService) {
						return null;
					},
					sentAndOpenOrders: function findSentAndOpenOrders($stateParams, communicationService) {
						return null;
					},
					receivedAndOpenOrders: function findReceivedAndOpenOrders($stateParams, communicationService) {
						return null;
					},
					amountOfProducts: function(productsService) {
						return 0;
					},
					amountOfContactsImported: function(contactsImportedService) {
						return contactsImportedService.getAmountOfContactsImported();
					},
					amountOfContactsToExport: function(contactsChangedService) {
						return contactsChangedService.getAmountOfContactsChangedByCommitted(false);
					},
					amountOfContactsPreviousImported: function(contactsPreviousImportedService) {
						return contactsPreviousImportedService.getAmountOfContactsPreviousImported();
					},
					amountOfContactsChanged: function(contactsChangedService) {
						return contactsChangedService.getAmountOfContactsChanged();
					},
					amountOfCommittedContactsChanged: function(contactsChangedService) {
						return contactsChangedService.getAmountOfContactsChangedByCommitted(true);
					},
					lastLogin: function getLastLogin($stateParams, authUserService) {
						return authUserService.getLastLogin($stateParams.userId);
					},
					projects: function findProjectsOfUser($stateParams, projectsService) {
						return projectsService.findProjectsOfUser($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					countryTypes: function getCountryTypes(optionsService) {
						return optionsService.getCountryTypes();
					},
					titles: function getAllTitles(optionsService) {
						return optionsService.getAllTitles();
					},
					provinceTypes: function getProvinceTypes(optionsService) {
						return optionsService.getProvinceTypes();
					},
					contactTypes: function getContactTypes($stateParams, optionsService) {
						return optionsService.getContactTypes();
					}
				}
    		};
    		return state;
    	}
	}
})();