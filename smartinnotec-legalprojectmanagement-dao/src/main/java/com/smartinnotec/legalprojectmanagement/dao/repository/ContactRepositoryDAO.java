package com.smartinnotec.legalprojectmanagement.dao.repository;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;

@Repository
public class ContactRepositoryDAO {

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<ContactImportationDateGroup> getContactsGroupedByImportationDateAndImported() {
        final Aggregation agg = newAggregation(Contact.class, group("importationDate").count().as("total"),
            project("total").and("importationDate").previousOperation());
        final AggregationResults<ContactImportationDateGroup> contactCreationDateGroupsAggregation = mongoTemplate.aggregate(agg,
            Contact.class, ContactImportationDateGroup.class);
        final List<ContactImportationDateGroup> contactCreationDateGroups = contactCreationDateGroupsAggregation.getMappedResults();
        final List<ContactImportationDateGroup> contactImportationDateGroupsResult = new ArrayList<>();
        for (final Iterator<ContactImportationDateGroup> it = contactCreationDateGroups.iterator(); it.hasNext();) {
            final ContactImportationDateGroup contactImportationDateGroup = it.next();
            if (contactImportationDateGroup.getImportationDate() != null) {
                contactImportationDateGroupsResult.add(contactImportationDateGroup);
            }
        }
        if (contactImportationDateGroupsResult != null && !contactImportationDateGroupsResult.isEmpty()) {
            Collections.sort(contactImportationDateGroupsResult, Collections.reverseOrder());
        }
        return contactImportationDateGroupsResult;
    }

    public List<Contact> findContactsOfAddress(final Address address) {
        final Query query = new Query();
        query.addCriteria(new Criteria("addressesWithProducts").ne(new ArrayList<>()))
            .addCriteria(new Criteria("addressesWithProducts.address").is(address));
        final List<Contact> contacts = mongoTemplate.find(query, Contact.class);
        return contacts;
    }
}
