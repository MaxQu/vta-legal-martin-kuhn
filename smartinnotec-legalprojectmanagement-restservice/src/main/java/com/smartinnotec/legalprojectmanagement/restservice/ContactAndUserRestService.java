package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.restservice.container.ContactUserContainer;
import com.smartinnotec.legalprojectmanagement.restservice.container.ContactUserTypeEnum;
import com.smartinnotec.legalprojectmanagement.service.ContactService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class ContactAndUserRestService extends AbstractRestService {

    @Autowired
    private ContactService contactService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/contactsandusers/contactanduser/", method = { RequestMethod.GET })
    public List<ContactUserContainer> findAllContactsAndUsers() throws BusinessException {
        logger.info(
            getInvokedRestServiceUserInformation() + ": find all contacts and users in ContactAndUserRestService#findAllContactsAndUsers");
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        final Tenant tenant = user.getTenant();
        final List<Contact> contacts = contactService.findAllTenantContacts(tenant);
        final List<User> users = userService.findAllUsersByTenant(tenant);
        final List<ContactUserContainer> contactUserContainers = new ArrayList<>();
        final List<ContactUserContainer> contactUserContainersOfContacts = convertContactToContactUserContainer(contacts);
        contactUserContainers.addAll(contactUserContainersOfContacts);
        contactUserContainers.addAll(convertUserToContactUserContainer(users));
        return contactUserContainers;
    }

    @RequestMapping(value = "/contactsandusers/contactanduser/{searchstring}/search", method = { RequestMethod.GET })
    public List<ContactUserContainer> findContactsAndUsersBySearchString(final @PathVariable("searchstring") String searchString)
            throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in ContactAndUserRestService#findContactsAnsUsersBySearchString",
            "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": find contacts and users by search string '{}' in ContactAndUserRestService#findContactsAnsUsersBySearchString",
            searchString);
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        final List<Contact> foundedContacts = contactService.findContactBySearchString(user, searchString);
        final List<User> foundedUsers = userService.findUserBySearchString(searchString);
        final List<ContactUserContainer> contactUserContainers = new ArrayList<>();
        contactUserContainers.addAll(convertContactToContactUserContainer(foundedContacts));
        contactUserContainers.addAll(convertUserToContactUserContainer(foundedUsers));
        return contactUserContainers;
    }

    private List<ContactUserContainer> convertContactToContactUserContainer(final List<Contact> contacts) {
        final List<ContactUserContainer> contactUserContainers = new ArrayList<>();
        for (final Contact contact : contacts) {
            final ContactUserContainer contactUserContainer = new ContactUserContainer();
            contactUserContainer.setId(contact.getId());
            contactUserContainer.setInstitution(contact.getInstitution());
            if (contact.getContactPersons() != null && !contact.getContactPersons().isEmpty()) {
                contactUserContainer.setFirstname(contact.getContactPersons().get(0).getFirstname());
                contactUserContainer.setSurname(contact.getContactPersons().get(0).getSurname());
            }
            if (contact.getEmails() != null && !contact.getEmails().isEmpty()) {
                contactUserContainer.setEmail(contact.getEmails().get(0));
            }
            contactUserContainer.setContact(contact);
            contactUserContainer.setContactUserType(ContactUserTypeEnum.CONTACT);
            contactUserContainers.add(contactUserContainer);
        }
        return contactUserContainers;
    }

    private List<ContactUserContainer> convertUserToContactUserContainer(final List<User> users) {
        final List<ContactUserContainer> contactUserContainers = new ArrayList<>();
        for (final User user : users) {
            if (excludeUser(user)) {
                continue;
            }
            final ContactUserContainer contactUserContainer = new ContactUserContainer();
            contactUserContainer.setId(user.getId());
            contactUserContainer.setInstitution("");
            contactUserContainer.setFirstname(user.getFirstname());
            contactUserContainer.setSurname(user.getSurname());
            contactUserContainer.setEmail(user.getEmail());
            contactUserContainer.setUser(user);
            contactUserContainer.setContactUserType(ContactUserTypeEnum.USER);
            contactUserContainers.add(contactUserContainer);
        }
        return contactUserContainers;
    }

    private boolean excludeUser(final User user) {
        if (user.getFirstname().toLowerCase().equals("superadmin") || user.getSurname().toLowerCase().equals("superadmin")
            || user.getUsername().toLowerCase().equals("superadmin")) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "[ContactAndUserRestService]";
    }
}
