package com.smartinnotec.legalprojectmanagement.security.xauth;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.DefaultSecurityFilterChain;

import com.smartinnotec.legalprojectmanagement.dao.domain.UserRoleEnum;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] restEndpointsToSecure;
    @Autowired
    private XAuthTokenConfigurer xAuthTokenConfigurer;

    static {
        restEndpointsToSecure = new String[0];
    }

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private Environment environment;
    @Autowired
    private UserService userService;

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        Objects.requireNonNull(http, "http object is mandatory and must not be null in WebSecurityConfig#configure");
        final List<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());
        logger.info("active profiles in WebSecurityConfig method configure '{}'", activeProfiles);

        final String[] restEndpointsToSecure = WebSecurityConfig.restEndpointsToSecure;
        for (final String endpoint : restEndpointsToSecure) {
            http.authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/" + endpoint + "/**").permitAll().antMatchers("/" + endpoint + "/**")
                .hasRole(UserRoleEnum.USER.toString());
        }

        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        xAuthTokenConfigurer.setDetailsService(userDetailsServiceBean());
        final SecurityConfigurer<DefaultSecurityFilterChain, HttpSecurity> securityConfigurerAdapter = xAuthTokenConfigurer;
        http.apply(securityConfigurerAdapter);
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder authManagerBuilder) throws Exception {
        authManagerBuilder.userDetailsService(new CustomUserDetailsService(userService));
    }

    @Bean
    public CsrfTokenResponseHeaderBindingFilter csrfTokenResponseHeaderBindingFilter() {
        return new CsrfTokenResponseHeaderBindingFilter();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return super.userDetailsServiceBean();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public String toString() {
        return "[WebSecurityConfig]";
    }
}
