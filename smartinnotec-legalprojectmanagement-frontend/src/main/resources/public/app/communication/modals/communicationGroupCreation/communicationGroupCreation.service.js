(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationGroupCreationModalService', communicationGroupCreationModalService);
        
    communicationGroupCreationModalService.$inject = ['$modal', '$stateParams', 'userService', 'communicationService'];
    
    function communicationGroupCreationModalService($modal, $stateParams, userService, communicationService) {
		var service = {
			showCommunicationGroupCreationModal: showCommunicationGroupCreationModal
		};		
		return service;
		
		////////////
				
		function showCommunicationGroupCreationModal(mode, currentUser, membersOfCommunicationGroup, communicationGroupUserConnection, communicationGroupUserConnections) {
			 var communicationGroupCreationModal = $modal.open({
				controller: CommunicationGroupCreationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	mode: function() {
			    		return mode;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}, 
			    	membersOfCommunicationGroup: function() {
			    		return membersOfCommunicationGroup;
			    	},
			    	communicationGroupUserConnection: function() {
			    		return communicationGroupUserConnection;
			    	},
			    	communicationGroupUserConnections: function() {
			    		return communicationGroupUserConnections;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/communicationGroupCreation/communicationGroupCreation.modal.html'
			});
			return communicationGroupCreationModal;
			
			function CommunicationGroupCreationModalController($modalInstance, $scope, mode, currentUser, membersOfCommunicationGroup, communicationGroupUserConnection, communicationGroupUserConnections) {
		    	var vm = this; 
		    	vm.mode = mode;
		    	vm.currentUser = currentUser;
		    	vm.selectedUser = null;
		    	vm.membersOfCommunicationGroup = membersOfCommunicationGroup != null ? membersOfCommunicationGroup : [];
		    	vm.usersOfGroup = vm.mode === 'EDIT' ? addUsersOfGroup() : [currentUser];
		    	vm.communicationGroupName = vm.mode === 'EDIT' ? communicationGroupUserConnection.communicationGroup.name : '';
		    	vm.communicationGroupDescription = vm.mode === 'EDIT' ? communicationGroupUserConnection.communicationGroup.description : '';
		    	vm.communicationGroupColor = vm.mode === 'CREATE' ? '#ff0000' : communicationGroupUserConnection.communicationGroup.color;
		    	vm.communicationGroupUserConnections = communicationGroupUserConnections;
		    	
		    	vm.getUsersByTerm = getUsersByTerm;
		    	vm.addUserToGroup = addUserToGroup;
		    	vm.removeUserFromGroup = removeUserFromGroup;
		    	vm.createCommunicationGroup = createCommunicationGroup;
		    	vm.updateCommunicationGroup = updateCommunicationGroup;
		    	vm.closeCommunicationGroupCreationModal = closeCommunicationGroupCreationModal;
		    	
		    	////////////
		    	
		    	function addUsersOfGroup() {
	    			  var usersOfGroupToEdit = [];
	    			  for(var i = 0; i <  vm.membersOfCommunicationGroup.length; i++) {
	    				  usersOfGroupToEdit.push(vm.membersOfCommunicationGroup[i].user);
	    			  }
	    			  return usersOfGroupToEdit;
	    		  }
		    	
		    	function getUsersByTerm(term) {
		    		return userService.findUsersByTerm(term).then(function successCallback(response) {
		    			return response.data;
		 	   		 }, function errorCallback(response) {
		 	   			  console.log('error administration.controller#searchUsersByTerm');
		 	   		 });
		    	}
		    	
		    	function addUserToGroup() {
		    		if(typeof vm.selectedUser === 'undefined') {
	    				  return;
	    			  }
	    			  var addTrigger = true;
	    			  for(var i = 0; i < vm.usersOfGroup.length; i++) {
	    				  if(vm.selectedUser != null && vm.usersOfGroup[i].id === vm.selectedUser.id) {
	    					  addTrigger = false;
	    					  vm.communicationGroupCreationError = true;
	    					  break;
	    				  }
	    			  }
	    			  if(addTrigger) {
	    				  vm.communicationGroupCreationError = false;
	    				  vm.usersOfGroup.push(vm.selectedUser);
	        			  vm.selectedUser = null;
	    			  }
		    	}
		    	
		    	function removeUserFromGroup(userOfGroup) {
	    			  for(var i = 0; i < vm.usersOfGroup.length; i++) {
	    				  if(vm.usersOfGroup[i].id === userOfGroup.id) {
	    					  vm.usersOfGroup.splice(i, 1);
	    					  break;
	    				  }
	    			  }
	    		  }
		    	
		    	function createCommunicationGroup() {
	    			  var communicationGroup = {};
	    			  communicationGroup.name = vm.communicationGroupName;
	    			  communicationGroup.description = vm.communicationGroupDescription; 
	    			  communicationGroup.color = vm.communicationGroupColor;
	    			  communicationGroup.userCreatedCommunicationGroup = currentUser;
	    			    
	    			  communicationService.createCommunicationGroup(communicationGroup).then(function (response) {
	    				  var createdCommunicationGroup = response.data;
	    				  communicationService.createCommunicationGroupUserConnections(createdCommunicationGroup.id, vm.usersOfGroup).then(function (response) {
	    					  for(var i = 0; i < response.data.length; i++) {
	    						  if(response.data[i].user.id == currentUser.id) {
	    							  vm.communicationGroupUserConnections.push(response.data[i]);
	    							  vm.closeCommunicationGroupCreationModal();
	    							  break;
	    						  } 
	    					  }
	    				  }).catch(function (data) {
		   	  				   console.log('error communicationGroupCreation#createCommunicationGroupUserConnections: ' + data);
		   	  			   });
		  			   }).catch(function (data) {
		  				   console.log('error communicationGroupCreation#createCommunicationGroup: ' + data);
		  			   });
	    		  }
	    		  
	    		  function updateCommunicationGroup() {
	    			  var communicationGroup = {};
	    			  communicationGroup.id = communicationGroupUserConnection.communicationGroup.id;
	    			  communicationGroup.name = vm.communicationGroupName;
	    			  communicationGroup.description = vm.communicationGroupDescription; 
	    			  communicationGroup.color = vm.communicationGroupColor;
	    			  communicationGroup.userCreatedCommunicationGroup = currentUser;
	    			  communicationService.updateCommunicationGroup(communicationGroup).then(function (response) {
	    				  for(var i = 0; i < communicationGroupUserConnections.length; i++) {
	    					  if(communicationGroupUserConnections[i].communicationGroup.id === communicationGroupUserConnection.communicationGroup.id) {
	    						  communicationGroupUserConnections[i].communicationGroup.name = communicationGroup.name;
	    						  communicationGroupUserConnections[i].communicationGroup.description = communicationGroup.description;
	    						  communicationGroupUserConnections[i].communicationGroup.color = communicationGroup.color;
	    						  
	    						  communicationService.updateCommunicationGroupUserConnections(communicationGroupUserConnection.communicationGroup.id, vm.usersOfGroup).then(function (response) {
	    							  communicationGroupUserConnection.showMembers = false;
	    							  vm.closeCommunicationGroupCreationModal();
	    						  }).catch(function (data) {
	    			  				   console.log('error communicationGroupCreation.service#updateCommunicationGroupUserConnection: ' + data);
	    			  			   });
	    						  break;
	    					  }
	    				  }
		  			   }).catch(function (data) {
		  				   console.log('error communicationGroupCreation.service#updateCommunicationGroup: ' + data);
		  			   });
	    		  }
		    	
		    	function closeCommunicationGroupCreationModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
