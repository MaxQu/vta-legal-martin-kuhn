package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "Communication")
public @Data class Communication {

    @Id
    private String id;
    private String title;
    private String message;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime receiveDate;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime elapsedDate;
    @DBRef
    private List<CommunicationGroup> messageGroups;
    private List<CommunicationAttachedDocumentFile> attachedFileUrls;
    private List<CommunicationAttachment> communicationAttachments;

    public void addMessageGroup(final CommunicationGroup messageGroup) {
        if (messageGroups == null) {
            messageGroups = new ArrayList<>();
        }
        messageGroups.add(messageGroup);
    }

    public void addMessageGroups(final List<CommunicationGroup> newMessageGroups) {
        if (messageGroups == null) {
            messageGroups = new ArrayList<>();
        }
        messageGroups.addAll(newMessageGroups);
    }

}
