package com.smartinnotec.legalprojectmanagement.security.xauth;

import com.smartinnotec.legalprojectmanagement.dao.domain.User;

import lombok.Data;

public @Data class UserTransferObject {

    private User user;
    private String token;

    public UserTransferObject() {
    }

    public UserTransferObject(final User user, final String token) {
        this.user = user;
        this.token = token;
    }
}
