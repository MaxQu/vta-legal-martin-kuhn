package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Communication;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface CommunicationUserConnectionRepository extends MongoRepository<CommunicationUserConnection, String> {

    List<CommunicationUserConnection> findCommunicationUserConnectionByProject(final Project project);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessage(final User user);

    List<CommunicationUserConnection> findByUserReceivedMessageAndArchivedOrderBySortDateAsc(final User user, final boolean archived,
            Pageable pageable);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived(
            final User userCreatedMessage, final User userReceivedMessage, final Tenant tenant, final boolean archived);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageNotAndUserReceivedMessageAndTenantAndArchived(
            final User userCreatedMessage, final User userReceivedMessage, final Tenant tenant, final boolean archived);

    List<CommunicationUserConnection> findCommunicationUserConnectionsByCommunication(final Communication message);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndConfirmationNeededAndRead(
            final User userCreatedMessage, final User userReceivedMessage, final Boolean confirmationNeeded, final Boolean read);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(
            final User userReceivedMessage, final Boolean confirmationNeeded, final Boolean read, final Boolean executed);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(
            final User userCreatedMessage, final Boolean confirmationNeeded, final Boolean read, final Boolean executed);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndExecutionDateNotNullAndExecutedAndArchived(
            final User userReceivedMessage, final Boolean executed, final Boolean archived);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessage(final User userCreatedMessage);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndExecutionDateNotNullAndExecutedAndArchived(
            final User userCreatedMessage, final Boolean executed, final Boolean archived);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween(final User userCreatedMessage,
            final DateTime start, final DateTime end);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween(final User userReceivedMessage,
            final DateTime start, final DateTime end);

}
