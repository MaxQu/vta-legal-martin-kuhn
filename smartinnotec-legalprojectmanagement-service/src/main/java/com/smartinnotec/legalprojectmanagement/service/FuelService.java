package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Fuel;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

import java.util.List;

public interface FuelService {

    List<Fuel> findAll(Tenant tenant) throws BusinessException;

    Fuel save(Fuel fuel);

    List<Fuel> findPagedFuelsByTenant(final Tenant tenant, final Integer page) throws BusinessException;

    Integer calculateAmountOfPages(final Tenant tenant);

    Integer getFuelPageSize();

    void deleteFuel(final User user, final String fuelId) throws BusinessException;
}
