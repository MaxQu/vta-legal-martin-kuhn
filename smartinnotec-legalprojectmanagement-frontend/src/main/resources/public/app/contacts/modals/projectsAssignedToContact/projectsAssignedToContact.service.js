(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('projectsAssignedToContactModalService', projectsAssignedToContactModalService);
        
    projectsAssignedToContactModalService.$inject = ['$modal', '$stateParams', 'projectUserConnectionService'];
    
    function projectsAssignedToContactModalService($modal, $stateParams, projectUserConnectionService) {
		var service = {
			showProjectsAssignedToContactModal: showProjectsAssignedToContactModal
		};		
		return service;
		
		////////////
				
		function showProjectsAssignedToContactModal(contact, projects, projectUserConnections) {
			 var projectsAssignedToContactModal = $modal.open({
				controller: ProjectsAssignedToContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	contact: function() {
			    		return contact;
			    	},
			    	projects: function() {
			    		return projects;
			    	},
			    	projectUserConnections: function() {
			    		return projectUserConnections;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/projectsAssignedToContact/projectsAssignedToContact.modal.html'
			});
			return projectsAssignedToContactModal;
			
			function ProjectsAssignedToContactModalController($modalInstance, $scope, contact, projects, projectUserConnections) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.projects = projects;
		    	vm.projectUserConnections = projectUserConnections;
		    	
		    	vm.projectSelectionChanged = projectSelectionChanged;
		    	vm.projectStillAdded = projectStillAdded;
		    	vm.uncheckProject = uncheckProject;
		    	vm.closeProjectAssignedToContactModal = closeProjectAssignedToContactModal;
		    	
		    	////////////
		    	
		    	function projectSelectionChanged(project, checked) {
		    		if(checked === true) {
		    			addContactToProject(project);
		    		} else if(checked === false) {
		    			removeContactFromProject(project);
		    		}
		    	} 
		    	
		    	function addContactToProject(project) {
		    		var projectUserConnection = {};
		    		projectUserConnection.contact = vm.contact;
					projectUserConnection.project = project;
					projectUserConnection.projectUserConnectionRole = 'CONTACT';
					projectUserConnectionService.createProjectUserConnection(projectUserConnection).then(function successCallback(response) {
						vm.projectUserConnections.push(response.data);
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error projectsAssignedToContact.service#addContactToProject');
		   	   		 });	
		    	}
		    	
		    	function removeContactFromProject(project) {
		    		projectUserConnectionService.deleteProjectUserConnectionOverProjectAndContact(project.id, vm.contact.id).then(function successCallback(response) {
		    			for(var i = 0; i < vm.projectUserConnections.length; i++) {
		    				if(vm.projectUserConnections[i].id == response.data.id) {
		    					vm.projectUserConnections.splice(i, 1);
		    					break;
		    				}
		    			}
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error projectsAssignedToContact.service#removeUserFromProject');
		   	   		 });	
		    	}
		    	
		    	function projectStillAdded(project) {
		    		for(var i = 0; i < vm.projectUserConnections.length; i++) {
		    			if(vm.projectUserConnections[i].project.id == project.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function uncheckProject(project) {
		    		for(var i = 0; i < vm.projects.length; i++) {
		    			if(vm.projects[i].id === project.id) {
		    				vm.projects[i].checked = false;
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function closeProjectAssignedToContactModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();