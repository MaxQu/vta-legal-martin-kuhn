package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.index.TextIndexed;

import lombok.Data;

public @Data class ActivityDocumentFile {

    @TextIndexed
    private String originalFileName;
    @TextIndexed
    private String fileName;
    @TextIndexed
    private String url;
    @TextIndexed
    private String version;
    private boolean isImage;

}
