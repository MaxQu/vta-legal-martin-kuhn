package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.repository.TenantRepository;

@Service("tenantService")
public class TenantServiceImpl extends AbstractService implements TenantService {

    @Autowired
    private TenantRepository tenantRepository;

    public TenantServiceImpl() {
    }

    @Override
    public Tenant create(final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in TenantServiceImpl#create", "400");
        logger.info("create tenant with name '{}' in TenantServiceImpl#create", tenant.getName());
        return tenantRepository.save(tenant);
    }

    @Override
    public List<Tenant> findAll() throws BusinessException {
        logger.info("find all tenants in TenantServiceImpl#findAll");
        return tenantRepository.findAll();
    }

    public Tenant findTenantByName(final String tenantName) throws BusinessException {
        BusinessAssert.notNull(tenantName, "tenantName is mandatory in TenantServiceImpl#getTenantByName", "400");
        logger.info("get tenant by name '{}' in TenantServiceImpl#getTenantByName", tenantName);
        return tenantRepository.findByName(tenantName);
    }

    @Override
    public Tenant findTenantById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in TenantServiceImpl#findTenantById", "400");
        BusinessAssert.isId(id, "id is mandatory in TenantServiceImpl#findTenantById", "400");
        logger.info("find tenant by id '{}' in TenantServiceImpl#findTenantById", id);
        return tenantRepository.findOne(id);
    }

    @Override
    public void delete(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in TenantServiceImpl#delete", "400");
        BusinessAssert.isId(id, "id must be an id in TenantServiceImpl#delete", "400");
        logger.info("delete tenant with id '{}' in TenantServiceImpl#delete", id);
        tenantRepository.delete(id);
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all tenants in TenantServiceImpl#deleteAll");
        tenantRepository.deleteAll();
    }

    @Override
    public String toString() {
        return "[TenantServiceImpl]";
    }
}
