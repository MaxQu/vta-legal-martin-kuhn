package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.LoginHistory;
import com.smartinnotec.legalprojectmanagement.dao.domain.LoginHistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface LoginHistoryRepository extends MongoRepository<LoginHistory, String> {

    LoginHistory findByLoginHistoryTypeAndUser(final LoginHistoryTypeEnum loginHistoryType, final User user);

}
