package com.smartinnotec.legalprojectmanagement.service.container;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventTypeEnum;

import lombok.Data;

public @Data class CalendarEventSearchCriteriaContainer {

    private String start;
    private String end;
    private String userId;
    private CalendarEventTypeEnum calendarEventType;

}
