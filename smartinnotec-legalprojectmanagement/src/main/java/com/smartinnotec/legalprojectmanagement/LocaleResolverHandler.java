package com.smartinnotec.legalprojectmanagement;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/* VTA */
@Component
public class LocaleResolverHandler {

    public LocaleResolverHandler() {
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.GERMAN);
        return slr;
    }

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:public/app/lang/i18n/backend/messages");
        messageSource.setCacheSeconds(3600);
        return messageSource;
    }

    @Override
    public String toString() {
        return "[LocaleResolverHandler]";
    }
}
