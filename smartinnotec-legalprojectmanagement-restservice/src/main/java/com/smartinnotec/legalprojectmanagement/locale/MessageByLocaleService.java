package com.smartinnotec.legalprojectmanagement.locale;

import java.util.Locale;

public interface MessageByLocaleService {

    String getMessage(final String key);

    Locale getLocale();
}
