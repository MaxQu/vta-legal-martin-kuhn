package com.smartinnotec.legalprojectmanagement.restservice;

import com.smartinnotec.legalprojectmanagement.dao.domain.*;
import com.smartinnotec.legalprojectmanagement.service.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

@RestController
public class GeocodeRestService {

  @Autowired
  private GeocodeService geocodeService;

  @Autowired
  private AddressService addressService;

  public GeocodeRestService() {
  }

  @RequestMapping(value = "/api/geocode/reverse", method = {RequestMethod.GET}, produces = "application/json")
  public @ResponseBody Address reverseGeocode(@RequestParam("lat") double lat, @RequestParam("lng") double lng) {
    return geocodeService.reverseGeocode(new Geolocation(lat, lng));
  }

  @RequestMapping(value = "/api/address/geocode", method = {RequestMethod.POST}, produces = "application/json")
  public void geocodeAddresses() {
    this.addressService.performGeocodingWhenRequired();
  }
}
