package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.Data;

public @Data class ContactImportedAddressImported {

    @DBRef
    private AddressImported address;

    @Override
    public boolean equals(final Object obj) {
        final ContactImportedAddressImported contactImportedAddressImported = (ContactImportedAddressImported)obj;
        if (this.address == null || contactImportedAddressImported.getAddress() == null) {
            return false;
        } else if (this.address.getId().equals(contactImportedAddressImported.getAddress().getId())) {
            return true;
        }
        return false;
    }

    public ContactImportedAddressImported deepCopy() {
        final ContactImportedAddressImported contactImportedAddressImported = new ContactImportedAddressImported();
        final AddressImported copiedAddressChanged = this.address.deepCopy();
        contactImportedAddressImported.setAddress(copiedAddressChanged);
        return contactImportedAddressImported;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        return result;
    }
}
