module.exports = function(grunt) {

	grunt.initConfig({
		clean: {
			  js: {
			    src: ['src/main/resources/public/build/js']
			  },
			  css: {
				src: ['src/main/resources/public/build/css']
			  }
			},
		concat: {
		    js: {
		      src: ['src/main/resources/public/app/constants/constants.js',
		            'src/main/resources/public/app/common/common.module.js',
		            'src/main/resources/public/app/administration/core/administration.module.js',
		            'src/main/resources/public/app/calendar/core/calendar.module.js',
		            'src/main/resources/public/app/communication/core/communication.module.js',
		            'src/main/resources/public/app/contacts/core/contacts.module.js',
		            'src/main/resources/public/app/core/legalprojectmanagement.module.js',
		            'src/main/resources/public/app/dashboard/core/dashboard.module.js',
		            'src/main/resources/public/app/error/error.module.js',
		            'src/main/resources/public/app/history/core/history.module.js',
		            'src/main/resources/public/app/home/core/home.module.js',
		            'src/main/resources/public/app/lang/lang.module.js',
		            'src/main/resources/public/app/nav/nav.module.js',
		            'src/main/resources/public/app/project/core/project.module.js',
		            'src/main/resources/public/app/fuel/core/fuel.module.js',
		            'src/main/resources/public/app/projects/core/projects.module.js',
		            'src/main/resources/public/app/search/core/search.module.js',
		            'src/main/resources/public/app/superadministration/core/superadministration.module.js',
		            'src/main/resources/public/app/timeregistration/core/timeregistration.module.js',
		            'src/main/resources/public/app/user/core/user.module.js',
		            'src/main/resources/public/app/workingbook/core/workingbook.module.js',
		            'src/main/resources/public/app/accesscontrol/core/accesscontrol.module.js',
		            'src/main/resources/public/app/products/core/products.module.js',
		            'src/main/resources/public/app/reporting/core/reporting.module.js',
		            'src/main/resources/public/app/**/*.js'],
		      dest: 'src/main/resources/public/build/js/legalprojectmanagement.scripts.js',
		    },
		    css: {
		      src: ['src/main/resources/public/assets/bower_components/fontawesome/css/font-awesome.min.css',
		            'src/main/resources/public/assets/bower_components/bootstrap/dist/css/bootstrap.min.css',
		            'src/main/resources/public/assets/bower_components/angular-bootstrap-toggle/dist/angular-bootstrap-toggle.min.css',
		            'src/main/resources/public/assets/bower_components/angular-xeditable/dist/css/xeditable.css',
		            'src/main/resources/public/assets/bower_components/angular-ui-bootstrap-datetimepicker/datetimepicker.css',
		            'src/main/resources/public/assets/bower_components/angular-smilies/dist/angular-smilies.min.css',
		            'src/main/resources/public/assets/bower_components/ngToast/dist/ngToast.min.css',
		            'src/main/resources/public/assets/bower_components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css',
		            'src/main/resources/public/app/**/*.css'],
		      dest: 'src/main/resources/public/build/css/legalprojectmanagement.styles.css',
		    }
		  },		 
		  jshint: {
			    options: {
			      curly: true,
			      eqnull: true,
			      loopfunc: true,
			      browser: true,
			      globals: {
			        jQuery: true
			      },
			    },
			    uses_defaults: ['Gruntfile.js', 'src/main/resources/public/app/**/*.js'],
			  },
		  cssmin: {
			  target: {
			    files: [{
			      expand: true,
			      cwd: 'src/main/resources/public/build/css',
			      src: ['*.css'],
			      dest: 'src/main/resources/public/build/css',
			      ext: '.styles.min.css'
			   }]
			}
		 },
		 uglify: {
			  target: {
				options: {
					mangle: false
			     },
			    files: [{
			    	expand: true,
			    	cwd: 'src/main/resources/public/build/js',
			    	src: ['*.js'],
			    	dest: 'src/main/resources/public/build/js',
			    	ext: '.scripts.min.js'
			   }]
			}
		 },
		 copy: {
			  smilies: {
			    nonull: true,
			    cwd: 'src/main/resources/public/assets/bower_components/angular-smilies/dist/',
			    expand: true,
			    src: 'angular-smilies.png',
			    dest: 'src/main/resources/public/build/css/',
			  },
			  bootstrapFonts: {
				    nonull: true,
				    cwd: 'src/main/resources/public/assets/bower_components/bootstrap/fonts',
				    expand: true,
				    src: '*',
				    dest: 'src/main/resources/public/build/fonts',
			   },
			   fontawesomeFonts: {
				    nonull: true,
				    cwd: 'src/main/resources/public/assets/bower_components/fontawesome/fonts',
				    expand: true,
				    src: '*',
				    dest: 'src/main/resources/public/build/fonts',
			   }
		 },
		 htmlmin: {                                     
			    dist: {                                      
			      options: {                                 
			        removeComments: true,
			        collapseWhitespace: true
			      },
			      files: {                                   
			        'src/main/resources/public/index.min.html': 'src/main/resources/public/index.html'    
			      }
			    },
		 	},
		  watch: {
			  js: {
				  files: ['src/main/resources/public/app/**/*.js'],
				  tasks: ['clean:js', 'concat:js', 'uglify'],
			  },
			  css: {
				  files: ['src/main/resources/public/app/**/*.css'],
				  tasks: ['clean:css', 'concat:css', 'cssmin', 'copy'],
				  },
			  html: {
				  files: ['src/main/resources/public/index.html'],
				  tasks: ['htmlmin'],
				  }
			},
		watchdev: {
			js: {
				files: ['src/main/resources/public/app/**/*.js'],
				tasks: ['concat:js'],
			},
			css: {
				files: ['src/main/resources/public/app/**/*.css'],
				tasks: ['clean:css', 'concat:css', 'cssmin', 'copy'],
			},
			html: {
				files: ['src/main/resources/public/index.html'],
				tasks: ['htmlmin'],
			}
		},
		});
	
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.registerTask('default', ['clean', 'concat', 'jshint', 'watch', 'cssmin']);
	grunt.registerTask('dev', ['concat', 'jshint', 'watchdev']);
};
