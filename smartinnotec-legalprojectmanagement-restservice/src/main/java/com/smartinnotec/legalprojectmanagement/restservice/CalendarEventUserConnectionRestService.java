package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.CalendarEventService;
import com.smartinnotec.legalprojectmanagement.service.CalendarEventViewTypeEnum;
import com.smartinnotec.legalprojectmanagement.service.ContactService;
import com.smartinnotec.legalprojectmanagement.service.UserService;
import com.smartinnotec.legalprojectmanagement.service.container.CalendarEventPreparedContainer;

@RestController
public class CalendarEventUserConnectionRestService extends AbstractRestService {
	
    @Autowired
    private CalendarEventService calendarEventService;
    @Autowired
    private UserService userService;
    @Autowired
    private ContactService contactService;

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{calendareventid}/{creationindex}", method = {
        RequestMethod.POST }, consumes = "application/json;charset=utf-8")
    public List<CalendarEventUserConnection> create(final @PathVariable("calendareventid") String calendarEventId,
            final @PathVariable("creationindex") Integer creationIndex,
            final @RequestBody List<CalendarEventUserConnection> calendarEventUserConnections)
            throws BusinessException {
        BusinessAssert.notNull(calendarEventId, "calendarEventId is mandatory in CalendarEventUserConnectionRestService#create", "400");
        BusinessAssert.isId(calendarEventId, "calendarEventId must be an id in CalendarEventUserConnectionRestService#create", "400");
        BusinessAssert.notNull("calendarEventUserConnection is mandatory in CalendarEventUserConnectionRestService#create", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ":create calendarEventUserConnection in CalendarEventUserConnectionRestService#create");
        final User user = userService.findUser(super.getLoggedInUserId());
        return null;
    }

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{calendareventid}", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody List<CalendarEventUserConnection> update(final @PathVariable("calendareventid") String calendarEventId,
            final @RequestBody List<CalendarEventUserConnection> calendarEventUserConnections)
            throws BusinessException {
        BusinessAssert.notNull(calendarEventId, "calendarEventId is mandatory in CalendarEventUserConnectionRestService#update", "400");
        BusinessAssert.isId(calendarEventId, "calendarEventId must be an id in CalendarEventUserConnectionRestService#update", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ":update calendarEventUserConnections for calendarEvent with id '{}' in CalendarEventUserConnectionRestService#update",
            calendarEventId);
        final User user = userService.findUser(super.getLoggedInUserId());
        final CalendarEvent calendarEvent = calendarEventService.findById(calendarEventId);
        if (calendarEvent.getSerialDate() != null && calendarEvent.getSerialDate()) {
            final List<CalendarEvent> serialCalendarEvents = calendarEventService
                .findBySerialDateNumber(calendarEvent.getSerialDateNumber());
            List<CalendarEventUserConnection> allCalendarEventUserConnections = new ArrayList<>();
            for (int i = 0; i < serialCalendarEvents.size(); i++) {
                

            }
            return allCalendarEventUserConnections;
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{userid}/{amountofweeks}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CalendarEventPreparedContainer> getPreparedCalendarEventsOfUserOfNextWeeks(@PathVariable("userid")
    final String userId, @PathVariable("amountofweeks")
    final Integer amountOfWeeks) throws BusinessException {
        BusinessAssert.notNull(userId,
            "userId is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUserOfNextWeeks", "400");
        BusinessAssert.isId(userId,
            "userId must be an id in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUserOfNextWeeks", "400");
        BusinessAssert.notNull(amountOfWeeks,
            "amountOfWeeks is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUserOfNextWeeks", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ":get prepared calendarEvents of user with id '{}' and amount of weeks '{}' in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUserOfNextWeeks",
            userId, amountOfWeeks);
        final User user = userService.findUser(userId);
       return null;
    }

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{userid}/{calendarviewtype}/{day}/{month}/{year}/{archived}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CalendarEventPreparedContainer> getPreparedCalendarEventsOfUser(@PathVariable("userid")
    final String userId, @PathVariable("calendarviewtype")
    final CalendarEventViewTypeEnum calendarViewType, @PathVariable("day")
    final String day, @PathVariable("month")
    final String month, @PathVariable("year")
    final String year, @PathVariable("archived")
    final Boolean archived) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUser",
            "400");
        BusinessAssert.isId(userId, "userId must be an id in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUser",
            "400");
        BusinessAssert.notNull(calendarViewType,
            "calendarViewType is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUser", "400");
        BusinessAssert.notNull(day, "day is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUser", "400");
        BusinessAssert.notNull(month, "month is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUser",
            "400");
        BusinessAssert.notNull(year, "year is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUser", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ":get prepared calendarEvents of user with id '{}' with range '{}' started from day '{}', month '{}' and year '{}' in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfUser",
            userId, calendarViewType, day, month, year);
        final User user = userService.findUser(userId);
        return null;
    }

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{userid}/{calendarviewtype}/{day}/{month}/{year}/all", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CalendarEventPreparedContainer> getPreparedCalendarEventsOfAllUsers(@PathVariable("userid")
    final String userId, @PathVariable("calendarviewtype")
    final CalendarEventViewTypeEnum calendarViewType, @PathVariable("day")
    final String day, @PathVariable("month")
    final String month, @PathVariable("year")
    final String year) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsers",
            "400");
        BusinessAssert.isId(userId, "userId must be an id in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsers",
            "400");
        BusinessAssert.notNull(calendarViewType,
            "calendarViewType is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsers", "400");
        BusinessAssert.notNull(day, "day is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsers",
            "400");
        BusinessAssert.notNull(month, "month is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsers",
            "400");
        BusinessAssert.notNull(year, "year is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsers",
            "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ":get prepared calendarEvents of all users with range '{}' started from day '{}', month '{}' and year '{}' in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsers",
            userId, calendarViewType, day, month, year);

        final User user = userService.findUser(userId);
        final Integer dayInteger = Integer.valueOf(day);
        final Integer monthInteger = Integer.valueOf(month);
        final Integer yearInteger = Integer.valueOf(year);

        Long start = 0L;
        Long end = 0L;
        final DateTime date = new DateTime().withDayOfMonth(dayInteger).withMonthOfYear(monthInteger).withYear(yearInteger);

        switch (calendarViewType) {
        case DAY:
            start = date.withTimeAtStartOfDay().getMillis();
            end = date.withTimeAtStartOfDay().plusDays(1).getMillis();
            break;
        case WEEK:
            start = date.dayOfWeek().withMinimumValue().withTimeAtStartOfDay().getMillis();
            end = date.dayOfWeek().withMinimumValue().withTimeAtStartOfDay().plusWeeks(1).getMillis();
            break;
        case MONTH:
            start = date.dayOfMonth().withMinimumValue().withTimeAtStartOfDay().getMillis();
            end = date.dayOfMonth().withMinimumValue().withTimeAtStartOfDay().plusMonths(1).getMillis();
            break;
        case YEAR:
            start = date.dayOfYear().withMinimumValue().withTimeAtStartOfDay().getMillis();
            end = date.dayOfYear().withMinimumValue().withTimeAtStartOfDay().plusYears(1).getMillis();
            break;
        }
        final List<CalendarEventPreparedContainer> allCalendarEvents = calendarEventService.getCalendarEvents(user.getTenant(), start, end,
            false, false);
        return allCalendarEvents;
    }

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{userid}/{start}/{end}/all", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CalendarEventPreparedContainer> getPreparedCalendarEventsOfAllUsersInRange(@PathVariable("userid")
    final String userId, @PathVariable("start")
    final String start, @PathVariable("end")
    final String end) throws BusinessException {
        BusinessAssert.notNull(userId,
            "userId is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsersInRange", "400");
        BusinessAssert.isId(userId,
            "userId must be an id in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsersInRange", "400");
        BusinessAssert.notNull(start,
            "start is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsersInRange", "400");
        BusinessAssert.notNull(end, "end is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsersInRange",
            "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ":get prepared calendarEvents of all users from '{}' to '{}' in CalendarEventUserConnectionRestService#getPreparedCalendarEventsOfAllUsersInRange",
            start, end);
        final User user = userService.findUser(userId);
        final DateTime startDateTime = dateFormatter.parseDateTime(start);
        final DateTime endDateTime = dateFormatter.parseDateTime(end).withTimeAtStartOfDay().plusDays(1).minusMillis(1);
        final List<CalendarEventPreparedContainer> allCalendarEvents = calendarEventService.getCalendarEvents(user.getTenant(),
            startDateTime.getMillis(), endDateTime.getMillis(), false, false);
        return allCalendarEvents;
    }

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{calendareventid}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CalendarEventUserConnection> findCalendarEventUserConnectionsByCalendarEvent(@PathVariable("calendareventid")
    final String calendarEventId) throws BusinessException {
        BusinessAssert.notNull(calendarEventId,
            "calendarEventId is mandatory in CalendarEventUserConnectionRestService#findCalendarEventUserConnectionsByCalendarEvent",
            "400");
        BusinessAssert.isId(calendarEventId,
            "calendarEventId must be an id in CalendarEventUserConnectionRestService#findCalendarEventUserConnectionsByCalendarEvent",
            "400");
        final CalendarEvent calendarEvent = calendarEventService.findById(calendarEventId);
        return null;
    }

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{contactid}/contact", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CalendarEventUserConnection> findCalendarEventUserConnectionsByContact(@PathVariable("contactid")
    final String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId,
            "contactId is mandatory in CalendarEventUserConnectionRestService#findCalendarEventUserConnectionsByContact", "400");
        BusinessAssert.isId(contactId,
            "contactId must be an id in CalendarEventUserConnectionRestService#findCalendarEventUserConnectionsByContact", "400");
        final Contact contact = contactService.findContactById(contactId);
        final List<CalendarEventUserConnection> calendarEventUserConnections = null;
        return calendarEventUserConnections;
    }

    @RequestMapping(value = "/calendareventuserconnections/calendareventuserconnection/{userid}/{contactid}/calendarevents", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CalendarEventPreparedContainer> getPreparedCalendarEventsByUserAndContact(@PathVariable("userid")
    final String userId, @PathVariable("contactid")
    final String contactId) throws BusinessException {
        BusinessAssert.notNull(userId,
            "userId is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsByUserAndContact", "400");
        BusinessAssert.isId(userId,
            "userId must be an id in CalendarEventUserConnectionRestService#getPreparedCalendarEventsByUserAndContact", "400");
        BusinessAssert.notNull(contactId,
            "contactId is mandatory in CalendarEventUserConnectionRestService#getPreparedCalendarEventsByUserAndContact", "400");
        BusinessAssert.isId(contactId,
            "contactId must be an id in CalendarEventUserConnectionRestService#getPreparedCalendarEventsByUserAndContact", "400");
        final Contact contact = contactService.findContactById(contactId);
        final List<CalendarEventUserConnection> calendarEventUserConnections = null;
        final List<CalendarEvent> calendarEvents = new ArrayList<>();
        for (final CalendarEventUserConnection calendarEventUserConnection : calendarEventUserConnections) {
            final CalendarEvent calendarEvent = calendarEventUserConnection.getCalendarEvent();
            final CalendarEvent calendarEventOfUser = calendarEventUserConnection.getCalendarEvent();
            calendarEvent.setContact(calendarEventUserConnection.getContact());
            calendarEvent.setContactAddressWithProducts(calendarEventUserConnection.getContactAddressWithProducts());
            calendarEvents.add(calendarEventOfUser);
        }
        final List<CalendarEventPreparedContainer> calendarEventsPrepared = calendarEventService.prepareCalendarEvents(calendarEvents,
            false);
        return calendarEventsPrepared;
    }

    @Override
    public String toString() {
        return "[CalendarEventUserConnectionRestService]";
    }
}
