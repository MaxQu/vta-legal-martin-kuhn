(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationAttachmentFilesModalService', communicationAttachmentFilesModalService);
        
    communicationAttachmentFilesModalService.$inject = ['$modal', '$timeout', '$stateParams', 'documentFileService'];
    
    function communicationAttachmentFilesModalService($modal, $timeout, $stateParams, documentFileService) {
		var service = {
			showCommunicationAttachmentFilesModal: showCommunicationAttachmentFilesModal
		};		
		return service;
		
		////////////
				
		function showCommunicationAttachmentFilesModal(invoker) {
			 var communicationAttachmentFilesModal = $modal.open({
				controller: CommunicationAttachmentFilesModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/communicationAttachmentFiles/communicationAttachmentFiles.modal.html'
			});
			return communicationAttachmentFilesModal;
			
			function CommunicationAttachmentFilesModalController($modalInstance, $scope, invoker) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.selectedDocumentFiles = [];
		    	vm.selectedDocumentFileSuccess = false;
		    	vm.selectedDocumentFileStillExists = false;
		    	
		    	vm.findDocumentFilesBySearchString = findDocumentFilesBySearchString;
		    	vm.addDocumentFile = addDocumentFile;
		    	vm.documentFileVersionSelected = documentFileVersionSelected;
		    	vm.existingDocumentFile = existingDocumentFile;
		    	vm.removeDocumentFile = removeDocumentFile;
		    	vm.attachFiles = attachFiles;
		    	vm.closeCommunicationAttachmentFilesModal = closeCommunicationAttachmentFilesModal;
		    	
		    	function findDocumentFilesBySearchString(searchString) {
		    		return documentFileService.findDocumentFilesBySearchString(searchString).then(function (response) {
		    			return response.data;
		    		}).catch(function (data) {
						console.log('error communicationAttachmentFiles.controller#findDocumentFilesBySearchString: ' + data);
					});
		    	}
		    	
		    	function addDocumentFile(selectedDocumentFile) {
		    		vm.selectedDocumentFileStillExists = false;
		    		if(selectedDocumentFile.documentFileVersions.length == 1) {
		    			if(existingDocumentFile(selectedDocumentFile, selectedDocumentFile.documentFileVersions[0]) === true) {
			    			return;
			    		}
		    			var selectedDocumentFileWithVersion = {};
		    			selectedDocumentFileWithVersion.selectedDocumentFile = selectedDocumentFile;
		    			selectedDocumentFileWithVersion.documentFileVersion = selectedDocumentFile.documentFileVersions[0];
			    		vm.selectedDocumentFiles.push(selectedDocumentFileWithVersion);
			    		vm.selectedDocumentFileSuccess = true;
			    		setAddDocumentFileTimeout();
			    		$scope.selectedDocumentFile = null;
		    		} else {
		    			vm.showVersionsOfDocumentFile = true;
		    		}
		    	}
		    	
		    	function documentFileVersionSelected(selectedDocumentFile, documentFileVersion, selectedState) {
		    		if(selectedState === true && existingDocumentFile(selectedDocumentFile, documentFileVersion) === true) {
		    			return;
		    		}
		    		if(selectedState === true) {
		    			var selectedDocumentFileWithVersion = {};
		    			selectedDocumentFileWithVersion.selectedDocumentFile = selectedDocumentFile;
		    			selectedDocumentFileWithVersion.documentFileVersion = documentFileVersion;
		    			vm.selectedDocumentFiles.push(selectedDocumentFileWithVersion);
		    		} else if(selectedState === false) {
		    			removeDocumentFile(selectedDocumentFile, documentFileVersion);
		    		}
		    	}
		    	
		    	function existingDocumentFile(selectedDocumentFile, documentFileVersion) {
		    		for(var i = 0; i < vm.selectedDocumentFiles.length; i++) {
		    			if(vm.selectedDocumentFiles[i].selectedDocumentFile.id == selectedDocumentFile.id &&
		    					vm.selectedDocumentFiles[i].documentFileVersion.version == documentFileVersion.version) {
		    				vm.selectedDocumentFileStillExists = true;
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function removeDocumentFile(selectedDocumentFile, documentFileVersion) {
		    		for(var i = 0; i < vm.selectedDocumentFiles.length; i++) {
		    			if(vm.selectedDocumentFiles[i].selectedDocumentFile.id == selectedDocumentFile.id &&
		    					vm.selectedDocumentFiles[i].documentFileVersion.version == documentFileVersion.version) {
		    				vm.selectedDocumentFiles.splice(i, 1);
		    				break;
		    			}
		    		}
		    	}
		   
		    	function attachFiles() {
		    		vm.invoker.attachFiles(vm.selectedDocumentFiles);
		    		closeCommunicationAttachmentFilesModal();
		    	}
		    	
		    	function setAddDocumentFileTimeout() {
		      		$timeout(function() {
		      			vm.selectedDocumentFileSuccess = false;
		  			   }, 2000); 
		      	}
		    	
		    	function closeCommunicationAttachmentFilesModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
