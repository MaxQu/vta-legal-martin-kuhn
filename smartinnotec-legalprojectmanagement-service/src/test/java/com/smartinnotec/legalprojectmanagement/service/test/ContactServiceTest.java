package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.DeliveryNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProvinceTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class ContactServiceTest extends AbstractServiceTest {

    @Test
    public void shouldCheckIfContactIdASupplier() throws BusinessException {
        final Contact contact1 = new Contact();
        final boolean isContactASupplier1 = contactService.isContactASupplier(contact1);
        Assert.assertTrue(isContactASupplier1);

        final Contact contact2 = new Contact();
        final List<DeliveryNumberContainer> deliveryNumberContainers1 = new ArrayList<>();
        final DeliveryNumberContainer deliveryNumberContainer1 = new DeliveryNumberContainer();
        deliveryNumberContainer1.setDeliveryNumber("22222");
        deliveryNumberContainers1.add(deliveryNumberContainer1);
        contact2.setDeliveryNumberContainers(deliveryNumberContainers1);
        final boolean isContactASupplier2 = contactService.isContactASupplier(contact2);
        Assert.assertTrue(isContactASupplier2);

        final Contact contact3 = new Contact();
        final List<DeliveryNumberContainer> deliveryNumberContainers3 = new ArrayList<>();
        final DeliveryNumberContainer deliveryNumberContainer3 = new DeliveryNumberContainer();
        deliveryNumberContainer3.setDeliveryNumber("22222");
        deliveryNumberContainers3.add(deliveryNumberContainer3);
        contact3.setDeliveryNumberContainers(deliveryNumberContainers3);
        final List<CustomerNumberContainer> customerNumberContainers3 = new ArrayList<>();
        final CustomerNumberContainer customerNumberContainer3 = new CustomerNumberContainer();
        customerNumberContainer3.setCustomerNumber("22222");
        customerNumberContainers3.add(customerNumberContainer3);
        contact3.setCustomerNumberContainers(customerNumberContainers3);
        final boolean isContactASupplier3 = contactService.isContactASupplier(contact3);
        Assert.assertTrue(isContactASupplier3);

        final Contact contact4 = new Contact();
        final List<CustomerNumberContainer> customerNumberContainers4 = new ArrayList<>();
        final CustomerNumberContainer customerNumberContainer4 = new CustomerNumberContainer();
        customerNumberContainer4.setCustomerNumber("22222");
        customerNumberContainers4.add(customerNumberContainer4);
        contact4.setCustomerNumberContainers(customerNumberContainers4);
        final boolean isContactASupplier4 = contactService.isContactASupplier(contact4);
        Assert.assertFalse(isContactASupplier4);

        final Contact contact5 = new Contact();
        final List<CustomerNumberContainer> customerNumberContainers5 = new ArrayList<>();
        final CustomerNumberContainer customerNumberContainer5 = new CustomerNumberContainer();
        customerNumberContainer5.setCustomerNumber("22222");
        customerNumberContainers5.add(customerNumberContainer5);
        final CustomerNumberContainer customerNumberContainer51 = new CustomerNumberContainer();
        customerNumberContainer51.setCustomerNumber("32222");
        customerNumberContainers5.add(customerNumberContainer51);
        contact4.setCustomerNumberContainers(customerNumberContainers5);
        final boolean isContactASupplier5 = contactService.isContactASupplier(contact5);
        Assert.assertTrue(isContactASupplier5);

        // customer number does not start with 2 -> it is a supplier
        final Contact contact6 = new Contact();
        final List<CustomerNumberContainer> customerNumberContainers6 = new ArrayList<>();
        final CustomerNumberContainer customerNumberContainer6 = new CustomerNumberContainer();
        customerNumberContainer6.setCustomerNumber("32222");
        customerNumberContainers6.add(customerNumberContainer6);
        contact6.setCustomerNumberContainers(customerNumberContainers6);
        final boolean isContactASupplier6 = contactService.isContactASupplier(contact6);
        Assert.assertTrue(isContactASupplier6);
    }

    @Test
    public void shouldFindUniqueContacts() throws BusinessException {
        final List<Contact> contacts = new ArrayList<>();
        final List<Contact> contactsFoundedOverAddresses = new ArrayList<>();

        final Contact contact1 = new Contact();
        contact1.setId("1");
        contact1.setInstitution("TestContact1");
        contacts.add(contact1);
        contactsFoundedOverAddresses.add(contact1);

        final Contact contact2 = new Contact();
        contact2.setId("2");
        contact2.setInstitution("TestContact2");
        contacts.add(contact2);

        final List<Contact> foundedContacts = contactService.findUniqueContacts(contacts, contactsFoundedOverAddresses);
        Assert.assertEquals(2, foundedContacts.size());

    }

    @Test
    public void shouldCreateContact() throws BusinessException {
        final Tenant tenant = new Tenant();
        tenant.setName("TestTenant");
        final Tenant createdTenant = tenantService.create(tenant);

        final User user = new User();
        user.setTenant(createdTenant);

        final Contact contact = new Contact();
        contact.setInstitution("Institution");
        final CustomerNumberContainer customerNumberContainer = new CustomerNumberContainer();
        customerNumberContainer.setCustomerNumber("1234");
        final List<CustomerNumberContainer> customerNumberContainers = new ArrayList<>();
        customerNumberContainers.add(customerNumberContainer);
        contact.setCustomerNumberContainers(customerNumberContainers);

        final List<ContactTypeEnum> contactTypes = new ArrayList<>();
        contactTypes.add(ContactTypeEnum.LAUB_FROSCH);
        contact.setContactTypes(contactTypes);
        contact.setHomepage("http://www.test.de");
        contact.setTenant(createdTenant);
        contact.setCreationDate(new DateTime(System.currentTimeMillis()));
        contact.setActive(true);

        final Contact createdContact = contactService.create(contact, user, false);
        Assert.assertNotNull(createdContact);
        Assert.assertNull(createdContact.getAddress());

        final Address address = new Address();
        address.setStreet("Hanson Strasse 12");
        address.setRegion("Berlin");
        address.setPostalCode("23323");
        address.setCountry(CountryTypeEnum.AUSTRIA.toString());
        address.setProvinceType(ProvinceTypeEnum.OBEROESTERREICH);
        address.setAdditionalInformation("Meine Adresse");
        createdContact.setAddress(address);

        final Contact updatedContact = contactService.update(createdContact, user, false);
        Assert.assertNotNull(updatedContact);
        Assert.assertNotNull(updatedContact.getAddress());
    }

    @Test
    public void shouldUpdateAddressOfContact() throws BusinessException {
        final Tenant tenant = new Tenant();
        tenant.setName("TestTenant");
        final Tenant createdTenant = tenantService.create(tenant);

        final User user = new User();
        user.setTenant(createdTenant);

        final Contact contact = new Contact();
        contact.setInstitution("Institution");

        final List<CustomerNumberContainer> customerNumberContainers = new ArrayList<>();
        final CustomerNumberContainer customerNumberContainer = new CustomerNumberContainer();
        customerNumberContainer.setCustomerNumber("123");
        customerNumberContainers.add(customerNumberContainer);
        contact.setCustomerNumberContainers(customerNumberContainers);

        final Address address = new Address();
        address.setStreet("Hanson Strasse 12");
        address.setRegion("Berlin");
        address.setPostalCode("23323");
        address.setCountry(CountryTypeEnum.AUSTRIA.toString());
        address.setProvinceType(ProvinceTypeEnum.OBEROESTERREICH);
        address.setAdditionalInformation("Meine Adresse");
        final Address createdAddress = addressService.create(address);
        Assert.assertNotNull(createdAddress);
        contact.setAddress(createdAddress);

        final List<ContactTypeEnum> contactTypes = new ArrayList<>();
        contactTypes.add(ContactTypeEnum.LAUB_FROSCH);
        contact.setContactTypes(contactTypes);
        contact.setHomepage("http://www.test.de");
        contact.setTenant(createdTenant);
        contact.setCreationDate(new DateTime(System.currentTimeMillis()));
        contact.setActive(true);

        final Contact createdContact = contactService.create(contact, user, false);
        Assert.assertNotNull(createdContact);
        Assert.assertEquals("Berlin", createdContact.getAddress().getRegion());

        //////////// update contact address /////////////

        createdContact.getAddress().setRegion("Hamburg");
        createdContact.getAddress().setCountry(CountryTypeEnum.GERMANY.toString());
        createdContact.getAddress().setPostalCode("1111");
        createdContact.getAddress().setProvinceType(ProvinceTypeEnum.BURGENLAND);
        final Contact updatedContact1 = contactService.update(createdContact, user, false);
        Assert.assertNotNull(updatedContact1);
        Assert.assertEquals("Hamburg", updatedContact1.getAddress().getRegion());

        /////////// delete address of contact and update contact //////

        updatedContact1.setAddress(null);
        final Contact updatedContact2 = contactService.update(updatedContact1, user, false);
        Assert.assertNotNull(updatedContact1);
        Assert.assertNull(updatedContact2.getAddress());
    }

    @Test
    public void shouldTestContactAddressesWithProductsOfContact() throws BusinessException {
        final Contact contact = new Contact();
        contact.setInstitution("Institution");

        final Address address1 = new Address();
        address1.setStreet("Hanson Strasse 1");
        address1.setRegion("Berlin");
        address1.setPostalCode("1111");
        address1.setCountry(CountryTypeEnum.AUSTRIA.toString());
        address1.setProvinceType(ProvinceTypeEnum.OBEROESTERREICH);
        address1.setAdditionalInformation("Meine Adresse 1");

        final Address address2 = new Address();
        address2.setStreet("Hanson Strasse 2");
        address2.setRegion("Berlin");
        address2.setPostalCode("2222");
        address2.setCountry(CountryTypeEnum.GERMANY.toString());
        address2.setProvinceType(ProvinceTypeEnum.NIEDEROESTERREICH);
        address2.setAdditionalInformation("Meine Adresse 2");

        final List<ContactAddressWithProducts> contactAddressesWithProducts = new ArrayList<>();

        final ContactAddressWithProducts contactAddressWithProducts1 = new ContactAddressWithProducts();
        contactAddressWithProducts1.setAddress(address1);
        contactAddressesWithProducts.add(contactAddressWithProducts1);

        final ContactAddressWithProducts contactAddressWithProducts2 = new ContactAddressWithProducts();
        contactAddressWithProducts2.setAddress(address2);
        contactAddressesWithProducts.add(contactAddressWithProducts2);

        contact.setAddressesWithProducts(contactAddressesWithProducts);

        final Contact createdContact = contactService.create(contact, new User(), false);
        Assert.assertNotNull(createdContact);
        Assert.assertEquals(2, createdContact.getAddressesWithProducts().size());

        /////////////// delete one address //////////////

        createdContact.getAddressesWithProducts().remove(0);
        final Contact updatedContact = contactService.update(createdContact, new User(), false);
        Assert.assertNotNull(updatedContact);
        Assert.assertEquals(1, createdContact.getAddressesWithProducts().size());

        /////////////// add one address //////////////

        final Address address3 = new Address();
        address3.setStreet("Neue Strasse");
        address3.setRegion("NeuRegion");
        address3.setPostalCode("3333");
        address3.setCountry(CountryTypeEnum.EMPTY_COUNTRY.toString());
        address3.setProvinceType(ProvinceTypeEnum.BURGENLAND);
        address3.setAdditionalInformation("Info Neue Adresse");

        final ContactAddressWithProducts contactAddressWithProducts3 = new ContactAddressWithProducts();
        contactAddressWithProducts3.setAddress(address3);
        updatedContact.getAddressesWithProducts().add(contactAddressWithProducts3);

        final Contact updatedContact2 = contactService.update(updatedContact, new User(), false);
        Assert.assertNotNull(updatedContact2);
        Assert.assertEquals(2, updatedContact2.getAddressesWithProducts().size());

        final List<Address> allAddresses = addressService.findAll();
        Assert.assertEquals(2, allAddresses.size());

        /////////////// update address //////////////

        final List<ContactAddressWithProducts> addressesWithProducts = updatedContact2.getAddressesWithProducts();
        final ContactAddressWithProducts contactAddressWithProducts = addressesWithProducts.get(1);
        contactAddressWithProducts.getAddress().setRegion("Updated Region");

        final Contact updatedContact3 = contactService.update(updatedContact2, new User(), false);
        Assert.assertNotNull(updatedContact3);
        Assert.assertEquals("Updated Region", updatedContact3.getAddressesWithProducts().get(1).getAddress().getRegion());

        final List<Address> allAddresses2 = addressService.findAll();
        Assert.assertEquals(2, allAddresses2.size());

        /////////////// delete addresses //////////////

        updatedContact3.setAddressesWithProducts(null);
        final Contact updatedContact4 = contactService.update(updatedContact3, new User(), false);
        Assert.assertNotNull(updatedContact4);
        final List<Address> allAddresses3 = addressService.findAll();
        Assert.assertEquals(0, allAddresses3.size());

    }

    @Test
    public void shouldGetAllContactsOfUser() throws BusinessException {

        // User 1
        final User user1 = new User();
        user1.setFirstname("Firstname1");
        user1.setSurname("Surname1");
        final User createdUser1 = userService.createUser(user1);
        Assert.assertNotNull(createdUser1);

        // User 2
        final User user2 = new User();
        user2.setFirstname("Firstname2");
        user2.setSurname("Surname2");
        final User createdUser2 = userService.createUser(user2);
        Assert.assertNotNull(createdUser2);

        // Contact 1
        final Contact contact1 = new Contact();
        contact1.setInstitution("Contact1");
        final Contact createdContact1 = contactService.create(contact1, user1, false);
        Assert.assertNotNull(createdContact1);

        // Contact 2
        final Contact contact2 = new Contact();
        contact2.setInstitution("Contact2");
        final Contact createdContact2 = contactService.create(contact2, user2, false);
        Assert.assertNotNull(createdContact2);

        // ProjectUserConnection 1
        final ProjectUserConnection projectUserConnection1 = new ProjectUserConnection();
        projectUserConnection1.setUser(createdUser1);
        projectUserConnection1.setContact(createdContact1);
        final ProjectUserConnection createdProjectUserConnection1 = projectUserConnectionService.create(projectUserConnection1);
        Assert.assertNotNull(createdProjectUserConnection1);

        // ProjectUserConnection 2
        final ProjectUserConnection projectUserConnection2 = new ProjectUserConnection();
        projectUserConnection2.setUser(createdUser1);
        projectUserConnection2.setContact(createdContact2);
        final ProjectUserConnection createdProjectUserConnection2 = projectUserConnectionService.create(projectUserConnection2);
        Assert.assertNotNull(createdProjectUserConnection2);

        // ProjectUserConnection 32
        final ProjectUserConnection projectUserConnection3 = new ProjectUserConnection();
        projectUserConnection3.setUser(createdUser2);
        projectUserConnection3.setContact(createdContact2);
        final ProjectUserConnection createdProjectUserConnection3 = projectUserConnectionService.create(projectUserConnection3);
        Assert.assertNotNull(createdProjectUserConnection3);

        final List<Contact> contactsOfUser1 = contactService.findAllContactsOfUserWithoutProducts(createdUser1);
        Assert.assertNotNull(contactsOfUser1);
        Assert.assertEquals(2, contactsOfUser1.size());

        final List<Contact> contactsOfUser2 = contactService.findAllContactsOfUserWithoutProducts(createdUser2);
        Assert.assertNotNull(contactsOfUser2);
        Assert.assertEquals(1, contactsOfUser2.size());
    }
}
