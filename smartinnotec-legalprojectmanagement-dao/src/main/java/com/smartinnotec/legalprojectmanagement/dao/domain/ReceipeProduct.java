package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.Data;

public @Data class ReceipeProduct {

    private Float density;
    private Float liter;
    private Float kilogram;
    private Float percentage;
    private String information;
    @DBRef
    private Product product;
}
