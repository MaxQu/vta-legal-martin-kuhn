package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum FacilityDetailsTypeEnum {
	
    BELEBUNGS_BECKEN("BELEBUNGS_BECKEN", true),
    VORKLAERUNG("VORKLAERUNG", false), // changed to vorreinigung
    VORREINIGUNG("VORREINIGUNG", true),
    	SEDIMENTATIONS_BECKEN("SEDIMENTATIONS_BECKEN", true),
    	FLOTATIONS_BECKEN("FLOTATIONS_BECKEN", true),
    TROPF_KOERPER("TROPF_KOERPER", true),
    NACHKLAER_BECKEN("NACHKLAER_BECKEN", true),
    FETT_UND_SANDFANG("FETT_UND_SANDFANG", true),
    FAULTURM("FAULTURM", true),
    SBR_REACTOR("SBR_REACTOR", true),
    ENTWAESSERUNGS_AGGREGAT("ENTWAESSERUNGS_AGGREGAT", true);

    private String value;
    private boolean visible;

    private FacilityDetailsTypeEnum(final String value, final boolean visible) {
        this.value = value;
        this.visible = visible;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(final boolean visible) {
		this.visible = visible;
	}
}
