(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.history')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getHistoryState());
    	 
    	////////////
			    	
    	function getHistoryState() {
    		var state = {
    			name: 'auth.history',
				url: '/history/:userId',
				templateUrl: 'app/history/history/history.html',
				controller: 'HistoryController',
				controllerAs: 'vm',
				resolve: {
					historyService: 'historyService',
					histories: function getHistories(historyService, dateUtilityService) {
						var startDate = new Date();
						startDate.setHours(0,0,0,0);
						startDate.setDate(startDate.getDate()-1);
						var endDate = new Date();
						var formattedStartDate = dateUtilityService.formatDateToString(startDate);
						var formattedEndDate = dateUtilityService.formatDateToString(endDate);
						return historyService.findPagedHistoryBetweenDates(formattedStartDate, formattedEndDate, 0);
					},
					startDate: function getStartDate() {
						var startDate = new Date();
						startDate.setHours(0,0,0,0);
						startDate.setDate(startDate.getDate()-1);
						return startDate;
					},
					endDate: function getStartDate() {
						var endDate = new Date();
						return endDate;
					},
					historyPageSize: function getHistoryPageSize(historyService) {
						return historyService.getHistoryPageSize();
					},
					amountOfPages: function calculateAmountOfPages(historyService, dateUtilityService) {
						var startDate = new Date();
						startDate.setHours(0,0,0,0);
						startDate.setDate(startDate.getDate()-1);
						var endDate = new Date();
						var formattedStartDate = dateUtilityService.formatDateToString(startDate);
						var formattedEndDate = dateUtilityService.formatDateToString(endDate);
						return historyService.calculateAmountOfPages(formattedStartDate, formattedEndDate);
					} 
				}
    		};
    		return state;
    	}
	}
})();