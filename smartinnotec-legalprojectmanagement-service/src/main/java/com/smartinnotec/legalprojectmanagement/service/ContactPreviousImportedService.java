package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactPreviousImported;

public interface ContactPreviousImportedService {

    ContactPreviousImported convertFromContactImportedToContactPreviousImported(final ContactImported contactImported)
            throws BusinessException;

    Integer count() throws BusinessException;

    ContactPreviousImported create(final ContactPreviousImported contactPreviousImported) throws BusinessException;

    ContactPreviousImported update(final ContactPreviousImported contactPreviousImported) throws BusinessException;

    void delete(final ContactPreviousImported contactPreviousImported) throws BusinessException;

}
