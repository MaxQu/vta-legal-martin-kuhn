(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.products')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getProductsState());
    	 
    	////////////
			    	
    	function getProductsState() {
    		var state = {
    			name: 'auth.products',
				url: '/products/:userId/:productId',
				templateUrl: 'app/products/products/products.html',
				controller: 'ProductsController',
				controllerAs: 'vm',
				resolve: { 
					productsService: 'productsService',
					userService: 'userService',
					optionsService: 'optionsService',
					thinningPolymerDateTimeValueService: 'thinningPolymerDateTimeValueService',
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					productTypes: function($stateParams, optionsService) {
						return optionsService.getProductTypes();
					},
					amountOfProducts: function(productsService) {
						return productsService.getAmountOfProducts();
					},
					clickedProduct: function($stateParams, productsService) {
						if($stateParams.productId != null && $stateParams.productId != '') {
							return productsService.findProduct($stateParams.productId);
						} else {
							return null;
						}
					},
					unityTypes: function(optionsService) {
						return optionsService.getUnityTypes();
					},
					productAggregateConditionTypes: function(optionsService) {
						return optionsService.getProductAggregateConditionTypes();
					},
					productId: function($stateParams) {
						return $stateParams.productId;
					},
					usersOfProject: function findProjectUserConnectionByProject(userService) {
						return userService.findAllUsers();
					},
					roles: function(optionsService) {
						return optionsService.getRoles();
					},
					currentThinningPolymerDateTimeValue: function findThinningPolymerDateTimeValueOfDate(thinningPolymerDateTimeValueService) {
						var date = new Date();
						var dateString = date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear();
 						return thinningPolymerDateTimeValueService.findThinningPolymerDateTimeValueOfDate(dateString);
					}
				}
    		};
    		return state;
    	}
	}
})();