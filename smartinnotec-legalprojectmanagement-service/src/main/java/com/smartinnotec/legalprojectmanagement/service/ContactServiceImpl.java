package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChangedTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.DeliveryNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ActivityRepositoryDAO;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactImportationDateGroup;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactRepositoryDAO;

@Service("contactService")
public class ContactServiceImpl extends AbstractService implements ContactService {

    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private AddressService addressService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private DocumentFileService documentFileService;
    @Autowired
    private ContactRepositoryDAO contactRepositoryDAO;
    @Autowired
    private I18NService i18NService;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;
    @Autowired
    private SearchService searchService;
    @Autowired
    private UtilService utilService;
    @Autowired
    private ContactChangedService contactChangedService;
    @Autowired
    private ContactComparation contactComparation;
    @Autowired
    protected ActivityRepositoryDAO activityRepositoryDAO;
    @Autowired
    private GeocodeService geocodeService;



    @Override
    public Contact create(Contact contact, final User user, final boolean createContactChanged) throws BusinessException {
        BusinessAssert.notNull(contact, "contact is mandatory in ContactServiceImpl#create", "400");
        logger.info("create contact in ContactServiceImpl#create");
        // set creation date of contact
        contact.setCreationDate(new DateTime(System.currentTimeMillis()));
        // set active to true
        contact.setActive(true);
        contact.setNewContact(true);

        // Geocoding -> resolve lat/lng for address
        geocodeService.geocodeAddress(contact.getAddress());

        final Contact copiedContact = contact.deepCopy();
        contact = resetAllChangedFlags(contact);

        // create address
        final Address address = contact.getAddress();
        if (address != null) {
            final Address createdAddress = addressService.create(address);
            contact.setAddress(createdAddress);
        }
        // create accountAddress
        final Address accountAddress = contact.getAccountAddress();
        if (accountAddress != null) {
            final Address createdAccountAddress = addressService.create(accountAddress);
            contact.setAccountAddress(createdAccountAddress);
        }
        // create contactAddressWithProducts
        final List<ContactAddressWithProducts> addressesWithProducts = contact.getAddressesWithProducts();
        if (addressesWithProducts != null && !addressesWithProducts.isEmpty()) {
            for (final ContactAddressWithProducts addressWithProducts : addressesWithProducts) {
                final Address additionalAddress = addressWithProducts.getAddress();
                if (additionalAddress != null) {
                    final Address createdAdditionalAddress = addressService.create(additionalAddress);
                    addressWithProducts.setAddress(createdAdditionalAddress);
                }
            }
        }

        final Contact createdContact = contactRepository.insert(contact);
        copiedContact.setId(createdContact.getId());
        final ContactChanged contactChanged = contactChangedService.convertFromContactToContactChanged(copiedContact,
            ContactChangedTypeEnum.CONTACT_CREATED);

        if (createContactChanged && !createdContact.containsContactType(ContactTypeEnum.POTENTIAL_BUYER)) {
            final ContactChanged createdContactChanged = contactChangedService.create(contactChanged, user);
            logger.info("created contactChanged with id '{}' in ContactServiceImpl#create", createdContactChanged.getId());
        }

        return createdContact;
    }

    @Override
    public Contact update(Contact contact, final User user, final boolean createContactChanged) throws BusinessException {
        BusinessAssert.notNull(contact, "contact is mandatory in ContactServiceImpl#update", "400");
        logger.info("create contact in ContactServiceImpl#update");
        final Contact existingContact = findContactById(contact.getId());
        contact.setNewContact(false);

        contactComparation.contactChanged(existingContact, contact);
        final Contact copiedContact = contact.deepCopy();
        contact = resetAllChangedFlags(contact);

        // update address
        final Address address = contact.getAddress();
        final Address existingAddress = existingContact.getAddress();
        if (existingAddress == null && address != null) {
            final Address updatedAddress = addressService.create(address);
            contact.setAddress(updatedAddress);
        } else if (existingAddress != null && address != null) {
            final Address updatedAddress = addressService.update(address);
            contact.setAddress(updatedAddress);
        } else if (existingAddress != null && address == null) {
            addressService.delete(existingAddress);
            contact.setAddress(null);
        }

        // Geocoding -> resolve lat/lng for address
        geocodeService.geocodeAddress(contact.getAddress());

        // update accountAddress
        final Address accountAddress = contact.getAccountAddress();
        final Address existingAccountAddress = existingContact.getAccountAddress();
        if (existingAccountAddress == null && accountAddress != null) {
            final Address updateAccountAddress = addressService.update(accountAddress);
            contact.setAccountAddress(updateAccountAddress);
        } else if (existingAccountAddress != null && accountAddress != null) {
            final Address updatedAccountAddress = addressService.update(accountAddress);
            contact.setAccountAddress(updatedAccountAddress);
        } else if (existingAccountAddress != null && accountAddress == null) {
            addressService.delete(existingAccountAddress);
            contact.setAccountAddress(null);
        }
        // update contactAddressWithProducts
        final List<ContactAddressWithProducts> existingAddressesWithProducts = existingContact.getAddressesWithProducts();
        final List<ContactAddressWithProducts> addressesWithProducts = contact.getAddressesWithProducts();
        if (addressesWithProducts != null) {
            for (final ContactAddressWithProducts addressWithProducts : addressesWithProducts) {

                final ContactAddressWithProducts contactAddressWithProducts = getContactAddressWithProducts(addressWithProducts,
                    existingAddressesWithProducts);
                if (contactAddressWithProducts != null) { // update
                    final Address existingContactAddress = contactAddressWithProducts.getAddress();
                    final Address newContactAddress = addressWithProducts.getAddress();
                    existingContactAddress.setStreet(newContactAddress.getStreet());
                    existingContactAddress.setRegion(newContactAddress.getRegion());
                    existingContactAddress.setEmail(newContactAddress.getEmail());
                    existingContactAddress.setTelephone(newContactAddress.getTelephone());
                    existingContactAddress.setProvinceType(newContactAddress.getProvinceType());
                    existingContactAddress.setPostalCode(newContactAddress.getPostalCode());
                    existingContactAddress.setCountry(newContactAddress.getCountry());
                    existingContactAddress.setAdditionalInformation(newContactAddress.getAdditionalInformation());
                    existingContactAddress.setInformation(newContactAddress.getInformation());
                    final Address updatedAddress = addressService.update(existingContactAddress);
                    contactAddressWithProducts.setAddress(updatedAddress);
                } else if (contactAddressWithProducts == null) { // create
                    final Address createdAddress = addressService.create(addressWithProducts.getAddress());
                    addressWithProducts.setAddress(createdAddress);
                }
            }
        }
        final List<ContactAddressWithProducts> contactAddressesWithProductsToDelete = getContactAddressesWithProductsToDelete(
            existingAddressesWithProducts, addressesWithProducts);
        for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProductsToDelete) {
            final Address addressToDelete = contactAddressWithProducts.getAddress();
            addressService.delete(addressToDelete);
        }

        final Contact updatedContact = contactRepository.save(contact);
        copiedContact.setId(updatedContact.getId());
        final ContactChanged contactChanged = contactChangedService.convertFromContactToContactChanged(copiedContact,
            ContactChangedTypeEnum.CONTACT_UPDATED);

        if (createContactChanged && !updatedContact.containsContactType(ContactTypeEnum.POTENTIAL_BUYER)) {
            final ContactChanged createdContactChanged = contactChangedService.create(contactChanged, user);
            logger.info("created contactChanged with id '{}' in ContactServiceImpl#update", createdContactChanged.getId());
        }

        return updatedContact;
    }

    private ContactAddressWithProducts getContactAddressWithProducts(final ContactAddressWithProducts addressWithProducts,
            final List<ContactAddressWithProducts> existingAddressesWithProducts) {
        if (existingAddressesWithProducts == null) {
            return null;
        }
        for (final ContactAddressWithProducts contactAddressWithProducts : existingAddressesWithProducts) {
            if (contactAddressWithProducts.getAddress() != null && addressWithProducts.getAddress() != null
                && contactAddressWithProducts.getAddress().getId().equals(addressWithProducts.getAddress().getId())) {
                return contactAddressWithProducts;
            }
        }
        return null;
    }

    private List<ContactAddressWithProducts> getContactAddressesWithProductsToDelete(
            final List<ContactAddressWithProducts> currentContactAddressesWithProducts,
            final List<ContactAddressWithProducts> newContactAddressesWithProducts) {
        logger.info("get contactAddressesWithProducts to delete in ContactServiceImpl#getContactAddressesWithProductsToDelete");
        if (currentContactAddressesWithProducts == null || currentContactAddressesWithProducts.isEmpty()) {
            return new ArrayList<>();
        }
        if (newContactAddressesWithProducts == null || newContactAddressesWithProducts.isEmpty()) {
            return currentContactAddressesWithProducts;
        }
        final List<ContactAddressWithProducts> contactAddressesWithProductsToDelete = new ArrayList<>();
        for (final ContactAddressWithProducts currentContactAddressWithProducts : currentContactAddressesWithProducts) {
            if (!newContactAddressesWithProducts.contains(currentContactAddressWithProducts)) {
                contactAddressesWithProductsToDelete.add(currentContactAddressWithProducts);
            }
        }
        return contactAddressesWithProductsToDelete;
    }

    @Override
    public List<Contact> findAll() throws BusinessException {
        logger.info("find all contacts in ContactServiceImpl#findAll");
        return contactRepository.findAll();
    }

    @Override
    public List<Contact> findAllImportedContacts(final Tenant tenant, final boolean imported) throws BusinessException {
        logger.info("find all imported contacts in ContactServiceImpl#findAllImportedContacts");
        return contactRepository.findByTenantAndImportedAndActive(tenant, imported, true);
    }

    @Override
    public List<Contact> findByTenantAndImportedAndImportConfirmedAndImportationDate(final Tenant tenant, final boolean imported,
            final boolean importConfirmed, final DateTime importationDate)
            throws BusinessException {
        logger.info(
            "find all imported and not confirmed contacts of date in ContactServiceImpl#findByTenantAndImportedAndImportConfirmedAndImportationDate");
        return contactRepository.findByTenantAndImportedAndImportConfirmedAndImportationDateAndActive(tenant, imported, importConfirmed,
            importationDate, true);
    }

    @Override
    public List<Contact> findAllTenantContacts(final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in ContactServiceImpl#findAllTenantContacts", "400");
        logger.info("find all tenant contacts of tenant with name '{}' in ContactServiceImpl#createContact", tenant.getName());
        return contactRepository.findByTenantAndActive(tenant, true);
    }

    @Override
    public Contact findByCustomerNumberContainersIn(final CustomerNumberContainer customerNumberContainer) throws BusinessException {
        BusinessAssert.notNull(customerNumberContainer,
            "customerNumberContainer is mandatory in ContactServiceImpl#findByCustomerNumberContainersIn", "400");
        logger.info("find contact by customerNumberContainer '{}' in ContactServiceImpl#findByCustomerNumberContainersIn",
            customerNumberContainer);
        return contactRepository.findByCustomerNumberContainersIn(customerNumberContainer);
    }

    @Override
    public List<Contact> findByContactAttachmentsNotNull() throws BusinessException {
        logger.info("find contacts where contact Attachments are not Null in ContactServiceImpl#findByContactAttachmentsNotNull");
        return contactRepository.findByContactAttachmentsNotNull();
    }

    @Override
    public Contact setProductsToContact(final Contact contact) throws BusinessException {
        BusinessAssert.notNull(contact, "contact is mandatory in ContactServiceImpl#setProductsToContact", "400");
        logger.info("set products to contact with in ContactServiceImpl#setProductsToContac");
        final List<String> productIds = contact.getProductIds();
        if (productIds != null && !productIds.isEmpty()) {
            for (final String productId : productIds) {
                final Product product = productService.findProduct(productId);
                contact.addProduct(product);
            }
        }

        final List<ContactAddressWithProducts> addressesWithProducts = contact.getAddressesWithProducts();
        if (addressesWithProducts != null) {
            for (final ContactAddressWithProducts contactAddressWithProducts : addressesWithProducts) {
                final List<String> productIdsOfLocation = contactAddressWithProducts.getProductIds();
                if (productIdsOfLocation != null && !productIdsOfLocation.isEmpty()) {
                    for (final String productId : productIdsOfLocation) {
                        final Product product = productService.findProduct(productId);
                        contactAddressWithProducts.addProduct(product);
                    }
                }
            }
        }
        return contact;
    }

    @Override
    public List<Contact> findContactBySearchString(final User user, String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString must not be null in ContactServiceImpl#findContactBySearchString", "400");
        logger.info("find contact by search string '{}' in ContactServiceImpl#findContactBySearchString", searchString);
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        List<Contact> contacts = new ArrayList<>();
        String preparedSearchString = searchString;
        if (combineSearchStringWithAnd) {
            preparedSearchString = preparedSearchString.replaceAll("\\+", "");
            preparedSearchString = preparedSearchString.replaceAll("\\*", "");
            final List<Contact> contactsFoundOverAddresses = findUniqueContactsByAddress(preparedSearchString);
            if (searchString.contains("*")) {
                contacts = contactRepository.findContactByRegexString(preparedSearchString);
                contacts = findUniqueContacts(contacts, contactsFoundOverAddresses);
            } else {
                contacts = contactRepository.findContactBySearchString(preparedSearchString);
                contacts = findUniqueContacts(contacts, contactsFoundOverAddresses);
            }
            removeContactsNotContainingSearchString(preparedSearchString, contacts);
        } else {
            contacts = contactRepository.findContactBySearchString(preparedSearchString);
            final List<Contact> contactsFoundOverAddresses = findUniqueContactsByAddress(preparedSearchString);
            contacts = findUniqueContacts(contacts, contactsFoundOverAddresses);
        }

        final ContactTypeEnum contactType = i18NService.getContactTypeOfString(preparedSearchString);
        if (contactType != null) {
            final List<Contact> contactsOfContactType = contactRepository.findByContactTypesIn(contactType);
            contacts.addAll(contactsOfContactType);
        }

        // find contacts with activities containing search string
        final List<Contact> contactsWithActivitiesContainingSearchString = getContactsWithActivitiesContainingSearchString(
            preparedSearchString);
        contacts = findUniqueContacts(contacts, contactsWithActivitiesContainingSearchString);

        return addProductsToContactsAndRemoveAttachmentsOfContactsConcerningPermissions(user, contacts);
    }

    private List<Contact> getContactsWithActivitiesContainingSearchString(final String searchString) throws BusinessException {
        logger.info(
            "get contacts with activities containing search string '{}' in ContactServiceImpl#getContactsWithActivitiesContainingSearchString",
            searchString);
        final List<Contact> contactsWithActivitiesContainingSearchString = new ArrayList<>();
        final List<Activity> activities = activityRepositoryDAO.findActivities(searchString);
        for (final Activity activity : activities) {
            final Contact contact = this.findContactById(activity.getContactId());
            contactsWithActivitiesContainingSearchString.add(contact);
        }
        return contactsWithActivitiesContainingSearchString;
    }

    @Override
    public List<Contact> findUniqueContactsByAddress(String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString must not be null in ContactServiceImpl#findUniqueContactsByAddress", "400");
        logger.info("find unique contacts by address in ContactServiceImpl#findUniqueContactsByAddress");
        searchString = utilService.escapeString(searchString);
        final List<Address> addresses = addressService.findAddressesBySearchString(searchString);
        final List<Contact> contacts = new ArrayList<>();
        for (final Address address : addresses) {
            final List<Contact> foundedContacts = findContactsByAddress(address);
            contacts.addAll(foundedContacts);
        }
        return contacts;
    }

    @Override
    public List<Contact> findUniqueContacts(final List<Contact> contacts, final List<Contact> contactsFoundedOverAddresses)
            throws BusinessException {
        logger.info("find unique contacts in ContactServiceImpl#findUniqueContacts");
        final List<Contact> foundedContacts = new ArrayList<>();
        if (contacts != null && !contacts.isEmpty()) {
            foundedContacts.addAll(contacts);
        }
        if (contactsFoundedOverAddresses != null && !contactsFoundedOverAddresses.isEmpty()) {
            for (final Contact contact : contactsFoundedOverAddresses) {
                if (!foundedContacts.contains(contact)) {
                    foundedContacts.add(contact);
                }
            }
        }
        return foundedContacts;
    }

    @Override
    public List<Contact> findContactsByExistingContactsAnSearchString(final User user, String searchString, final List<Contact> contacts)
            throws BusinessException {
        BusinessAssert.notNull(searchString,
            "searchString must not be null in ContactServiceImpl#findContactsByExistingContactsAnSearchString", "400");
        logger.info("find contact by search string '{}' in ContactServiceImpl#findContactsByExistingContactsAnSearchString", searchString);
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        if (combineSearchStringWithAnd) {
            searchString = searchString.replaceAll("\\+", "");
            if (searchString.contains("*")) {
                searchString = searchString.replaceAll("\\*", "");
            }
        }
        removeContactsNotContainingSearchString(searchString, contacts);
        return addProductsToContactsAndRemoveAttachmentsOfContactsConcerningPermissions(user, contacts);
    }

    private void removeContactsNotContainingSearchString(final String searchString, final List<Contact> contacts) {
        logger.info("remove contacts not containing searchString '{}' in ContactServiceImpl#removeContactsNotContainingSearchString",
            searchString);
        for (final Iterator<Contact> iterator = contacts.iterator(); iterator.hasNext();) {
            final Contact contact = iterator.next();
            final String addressesWithProductsProperties = contact.getAddressesWithProducts() != null
                ? contact.getAddressesWithProducts().toString()
                : "";
            final String contactProperties = contact.getInstitution() + contact.getInformation() + contact.getHomepage()
                + contact.getCustomerNumberContainers() + contact.getDeliveryNumberContainers() + addressesWithProductsProperties;
            if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(contactProperties.toLowerCase()::contains)) {
                iterator.remove();
            }
        }
    }

    private List<Contact> addProductsToContactsAndRemoveAttachmentsOfContactsConcerningPermissions(final User user,
            final List<Contact> contacts)
            throws BusinessException {
        logger.info(
            "add products to contacts and remove attachments of contacts concerning permissions in ContactServiceImpl#addProductsToContactsAndRemoveAttachmentsOfContactsConcerningPermissions");
        final boolean canSeeConfidentDocumentFiles = projectUserConnectionService
            .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());
        final List<Contact> contactsWithProducts = new ArrayList<>();
        for (final Contact contact : contacts) {
            final Contact contactWithProducts = setProductsToContact(contact);
            contactsWithProducts.add(contactWithProducts);
            removeAttachmentsConcerningPermissions(contact, user, canSeeConfidentDocumentFiles);
        }
        return contactsWithProducts;
    }

    @Override
    public List<Contact> findContactsOfUserBySearchString(final User user, final String searchString) throws BusinessException {
        BusinessAssert.notNull(user, "user must not be null in ContactServiceImpl#findContactsOfUserBySearchString", "400");
        BusinessAssert.notNull(searchString, "searchString must not be null in ContactServiceImpl#findContactsOfUserBySearchString", "400");
        logger.info("find contacts of user with id '{}' and searchString '{}' in ContactServiceImpl#findContactsOfUserBySearchString",
            user.getId(), searchString);
        final List<Contact> foundedContacts = this.findContactBySearchString(user, searchString);
        final List<Contact> contactsOfUser = new ArrayList<>();
        for (final Contact contact : foundedContacts) {
            final ProjectUserConnection projectUserConnection = projectUserConnectionService.findByUserAndContactAndActive(user, contact,
                true);
            if (projectUserConnection != null) {
                contactsOfUser.add(projectUserConnection.getContact());
            }
            if (contact.containsContactType(ContactTypeEnum.POTENTIAL_BUYER)) {
                contactsOfUser.add(contact);
            }
        }
        return contactsOfUser;
    }

    @Override
    public Contact findContactById(String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId must not be null in ContactServiceImpl#findContactById", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ContactServiceImpl#findContactById", "400");
        logger.info("find contact by id '{}' in ContactServiceImpl#findContactById", contactId);
        final Contact contact = contactRepository.findOne(contactId);
        if (contact == null) {
            return null;
        }
        final Contact contactWithProducts = setProductsToContact(contact);
        return contactWithProducts;
    }

    @Override
    public Contact findContactByIdWithoutProducts(final String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId must not be null in ContactServiceImpl#findContactByIdWithoutProducts", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ContactServiceImpl#findContactByIdWithoutProducts", "400");
        logger.info("find contact by id '{}' in ContactServiceImpl#findContactByIdWithoutProducts", contactId);
        return contactRepository.findOne(contactId);
    }

    @Override
    public Integer countByTenantAndActive(final Tenant tenant, final boolean active) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in ContactServiceImpl#countByTenantAndActive", "400");
        logger.info("count amount of contacts of tenant with id '{}' in ContactServiceImpl#countByTenantAndActive", tenant.getId());
        return contactRepository.countByTenantAndActive(tenant, active);
    }

    @Override
    public Integer countByTenantAndActiveAndDeliveryNumberContainersIsNull(final Tenant tenant, final boolean active)
            throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in ContactServiceImpl#countByTenantAndActive", "400");
        logger.info("count amount of customer contacts of tenant with id '{}' in ContactServiceImpl#countByTenantAndActive",
            tenant.getId());
        final List<Contact> allContacts = contactRepository.findByTenantAndActive(tenant, active);
        allContacts.removeIf(s -> isContactASupplier(s));
        return allContacts.size();
    }

    @Override
    public List<Contact> findPagedContactsByTenant(final User user, final Tenant tenant, final Integer page, String sortingType)
            throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ContactServiceImpl#findPagedContactsByTenant", "400");
        BusinessAssert.notNull(page, "page is mandatory in ContactServiceImpl#findPagedContactsByTenant", "400");
        logger.info("find paged contacts page '{}' in ContactServiceImpl#findPagedContactsByTenant", page);

        if (tenant == null) {
            return new ArrayList<>();
        }

        Direction sortDirection = Sort.Direction.ASC;
        if (sortingType.startsWith("-")) {
            sortDirection = Sort.Direction.DESC;
            sortingType = sortingType.substring(1);
        }

        Integer contactPageSize = this.getContactPageSize();
        if (user.getContactPageSize() != null && user.getContactPageSize() > 0) {
            contactPageSize = user.getContactPageSize();
        }

        final Pageable pageable = new PageRequest(page, contactPageSize, new Sort(sortDirection, sortingType));
        final List<Contact> pagedContacts = contactRepository.findPagedContactsByTenantAndActive(tenant, pageable, true);
        if (pagedContacts == null || pagedContacts.isEmpty()) {
            return new ArrayList<>();
        }
        final boolean canSeeConfidentDocumentFiles = projectUserConnectionService
            .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());
        // set products into contacts and remove documentFiles of contact concerning permission
        for (final Contact contact : pagedContacts) {
            removeAttachmentsConcerningPermissions(contact, user, canSeeConfidentDocumentFiles);
        }
        return pagedContacts;
    }

    @Override
    public List<Contact> findAllContactsOfUserWithoutProducts(final User user) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ContactServiceImpl#findAllContactsOfUserWithoutProducts", "400");
        logger.info("find all contacts of user with id '{}' in ContactServiceImpl#findAllContactsOfUserWithoutProducts", user.getId());
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService
            .findProjectUserConnectionsByUserAndContactIsNotNull(user);
        final List<Contact> contacts = new ArrayList<>();
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            if (projectUserConnection.getContact() != null) {
                contacts.add(projectUserConnection.getContact());
            }
        }
        return contacts;
    }

    @Override
    public List<Contact> findAllContactsAssignedOverAgentNumbers(final User user) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ContactServiceImpl#findAllContactsAssignedOverAgentNumbers", "400");
        logger.info(
            "find all contacts of user with id '{}' assigned oder agentNumbers in ContactServiceImpl#findAllContactsAssignedOverAgentNumbers",
            user.getId());
        final List<AgentNumberContainer> agentNumberContainers = user.getAgentNumberContainers();
        if (agentNumberContainers == null || agentNumberContainers.isEmpty()) {
            return new ArrayList<>();
        }
        final List<Contact> contacts = contactRepository.findByAgentNumberContainersIn(agentNumberContainers);
        return contacts;
    }

    private void removeAttachmentsConcerningPermissions(final Contact contact, final User user, final boolean canSeeConfidentDocumentFiles)
            throws BusinessException {
        final List<ContactAttachment> contactAttachments = contact.getContactAttachments();
        if (contactAttachments != null && !contactAttachments.isEmpty()) {
            for (final Iterator<ContactAttachment> iterator = contactAttachments.iterator(); iterator.hasNext();) {
                final ContactAttachment contactAttachment = iterator.next();
                final String documentFileId = contactAttachment.getDocumentFileId();
                final DocumentFile documentFile = documentFileService.findById(documentFileId);
                final String projectProductId = documentFile.getProjectProductId();
                // can not see confident documentFiles
                if (canSeeConfidentDocumentFiles == false && documentFile.isConfident()) {
                    iterator.remove();
                    continue;
                }
                // if user is black listed for documentFile
                if (documentFile.getUserIdBlackList().contains(user.getId())) {
                    iterator.remove();
                    continue;
                }
                if (productService.checkIfProductExists(projectProductId)) {
                    // if role is black listed
                    if (documentFile.getRolesBlackList() != null
                        && documentFile.getRolesBlackList().contains(user.getProjectUserConnectionRole())) {
                        iterator.remove();
                        continue;
                    }
                } else if (projectService.checkIfProjectExists(projectProductId)) {
                    final Project project = projectService.findProject(projectProductId);
                    final ProjectUserConnection projectUserConnection = projectUserConnectionService
                        .findProjectUserConnectionByUserAndProject(user, project);
                    // user not member of project
                    if (projectUserConnection == null) {
                        iterator.remove();
                        continue;
                    }

                } else {
                    throw new BusinessException(
                        "documentFile projectProductId is either project not product in ContactServiceImpl#findPagedContactsByTenant",
                        "400", "400");
                }
            }
        }
    }

    @Override
    public List<ContactAttachment> getAddedContactAttachments(final List<ContactAttachment> currentContactAttachments,
            final List<ContactAttachment> newContactAttachments) {
        logger.info("get added contactAttachments in ContactServiceImpl#getAddedContactAttachments");
        final List<ContactAttachment> addedContactAttachments = new ArrayList<>();
        if ((currentContactAttachments == null || currentContactAttachments.isEmpty()) && newContactAttachments != null
            && !newContactAttachments.isEmpty()) {
            return newContactAttachments;
        } else if (currentContactAttachments != null && !currentContactAttachments.isEmpty()
            && (newContactAttachments == null || newContactAttachments.isEmpty())) {
            return addedContactAttachments;
        } else if ((currentContactAttachments == null || currentContactAttachments.isEmpty())
            && (newContactAttachments == null || newContactAttachments.isEmpty())) {
            return addedContactAttachments;
        }
        for (final ContactAttachment newContactAttachment : newContactAttachments) {
            if (!currentContactAttachments.contains(newContactAttachment)) {
                addedContactAttachments.add(newContactAttachment);
            }
        }
        return addedContactAttachments;
    }

    @Override
    public List<ContactAttachment> getRemovedContactAttachments(final List<ContactAttachment> currentContactAttachments,
            final List<ContactAttachment> newContactAttachments) {
        logger.info("get removed contactAttachments in ContactServiceImpl#getRemovedContactAttachments");
        final List<ContactAttachment> removedContactAttachments = new ArrayList<>();
        if ((currentContactAttachments != null && !currentContactAttachments.isEmpty())
            && (newContactAttachments == null || newContactAttachments.isEmpty())) {
            return currentContactAttachments;
        } else if ((currentContactAttachments == null || currentContactAttachments.isEmpty())
            && (newContactAttachments != null && !newContactAttachments.isEmpty())) {
            return removedContactAttachments;
        } else if ((currentContactAttachments == null || currentContactAttachments.isEmpty())
            && (newContactAttachments == null || newContactAttachments.isEmpty())) {
            return removedContactAttachments;
        }
        for (final ContactAttachment currentContactAttachment : currentContactAttachments) {
            if (!newContactAttachments.contains(currentContactAttachment)) {
                removedContactAttachments.add(currentContactAttachment);
            }
        }
        return removedContactAttachments;
    }

    @Override
    public Integer calculateAmountOfPages(final User user, final Tenant tenant) throws BusinessException {
        logger.info("calculate amount of pages in ContactServiceImpl#calculateAmountOfPages");
        Integer contactPageSize = this.getContactPageSize();
        if (user.getContactPageSize() != null && user.getContactPageSize() > 0) {
            contactPageSize = user.getContactPageSize();
        }
        final Integer amountOfContacts = contactRepository.countByTenantAndActive(tenant, true);
        final Integer amountOfPageSizes = (amountOfContacts + contactPageSize - 1) / contactPageSize;
        logger.info("calculated amount of pages to '{}' in ContactServiceImpl#calculateAmountOfPages", amountOfPageSizes);
        return amountOfPageSizes;
    }

    @Override
    public Integer countByActive(final boolean active) throws BusinessException {
        logger.info("count amount of contacts in ContactServiceImpl#countByTenantAndActive");
        return contactRepository.countByActive(active);
    }

    @Override
    public List<ContactImportationDateGroup> getContactImportationDateGroups() throws BusinessException {
        logger.info("get contactCreationDateGroups in ContactServiceImpl#getContactImportationDateGroups");
        return contactRepositoryDAO.getContactsGroupedByImportationDateAndImported();
    }

    @Override
    public void deleteContact(final String contactId, final User user) throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId is mandatory in ContactServiceImpl#deleteContact", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ContactServiceImpl#deleteContact", "400");
        logger.info("delete contact with id '{}' in ContactServiceImpl#deleteContact", contactId);

        final Contact contactToDelete = findContactById(contactId);

        

        if (!contactToDelete.containsContactType(ContactTypeEnum.POTENTIAL_BUYER)) {
            final ContactChanged contactChanged = contactChangedService.convertFromContactToContactChanged(contactToDelete,
                ContactChangedTypeEnum.CONTACT_DELETED);
            final ContactChanged createdContactChanged = contactChangedService.create(contactChanged, user);
            logger.info("created contactChanged with id '{}' in ContactServiceImpl#delete", createdContactChanged.getId());
        }

        // delete address
        final Address address = contactToDelete.getAddress();
        if (address != null) {
            addressService.delete(address);
        }
        // delete accountAddress
        final Address accountAddress = contactToDelete.getAccountAddress();
        if (accountAddress != null) {
            addressService.delete(accountAddress);
        }
        // delete contactAddressWithProducts
        final List<ContactAddressWithProducts> addressesWithProducts = contactToDelete.getAddressesWithProducts();
        if (addressesWithProducts != null && !addressesWithProducts.isEmpty()) {
            for (final ContactAddressWithProducts addressWithProducts : addressesWithProducts) {
                final Address additionalAddress = addressWithProducts.getAddress();
                if (additionalAddress != null) {
                    addressService.delete(additionalAddress);
                }
            }
        }

        final List<ContactAttachment> contactAttachments = contactToDelete.getContactAttachments();
        if (contactAttachments != null && !contactAttachments.isEmpty()) {
            for (final ContactAttachment contactAttachment : contactAttachments) {
                final String documentFileId = contactAttachment.getDocumentFileId();
                final DocumentFile documentFile = documentFileService.findById(documentFileId);
                documentFile.setContactReferenceCounter(documentFile.getContactReferenceCounter() - 1);
                final DocumentFile updatedDocumentFile = documentFileService.update(documentFile);
                logger.info("updated documentFile with id '{}' in ContactRestService#delete", updatedDocumentFile.getId());
            }
        }

        contactRepository.delete(contactId);
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all contacts and corresponding addresses in ContactServiceImpl#deleteAll");
        final List<Contact> contacts = contactRepository.findAll();
        for (final Contact contact : contacts) {
            deleteContact(contact);
        }
    }

    @Override
    public void deleteAllCustomerContacts(final CountryTypeEnum countryType) throws BusinessException {
        logger.info("delete all contacts and corresponding addresses in ContactServiceImpl#deleteAllCustomerContact");
        final List<Contact> contacts = contactRepository.findAll();
        for (final Contact contact : contacts) {
            if (isContactASupplier(contact)) {
                continue;
            }
            if (fistCustomerNumberContainerIsOfCountryType(contact, countryType) == false) {
                continue;
            }
            deleteContact(contact);
        }
    }

    @Override
    public List<Contact> findContactsByAddress(final Address address) throws BusinessException {
        BusinessAssert.notNull(address, "address is mandatory in ContactServiceImpl#findByAddressesWithProducts", "400");
        logger.info("find contacts by addressesWithProducts in ContactServiceImpl#findByAddressesWithProducts");
        final List<Contact> contacts = contactRepositoryDAO.findContactsOfAddress(address);
        return contacts;
    }

    @Override
    public List<Contact> findAllContactsOfContactType(final ContactTypeEnum contactType) throws BusinessException {
        BusinessAssert.notNull(contactType, "contactType is mandatory in ContactServiceImpl#findAllContactOfContactType", "400");
        logger.info("find all contacts of contactType '{}' in ContactServiceImpl#findAllContactOfContactType", contactType);
        return contactRepository.findByContactTypesIn(contactType);
    }

    @Override
    public List<Contact> findAllSupplierContacts(Tenant tenant) {
        return contactRepository.findByTenantAndContactTypesIn(tenant, ContactTypeEnum.SUPPLIER);
    }

    private boolean fistCustomerNumberContainerIsOfCountryType(final Contact contact, final CountryTypeEnum countryType) {
        final List<CustomerNumberContainer> customerNumberContainers = contact.getCustomerNumberContainers();
        if (customerNumberContainers == null || customerNumberContainers.isEmpty()) {
            return false;
        }
        final CustomerNumberContainer customerNumberContainer = customerNumberContainers.get(0);
        if (customerNumberContainer.getContactTenant().toUpperCase().equals(countryType.toString())) {
            return true;
        }
        return false;
    }

    @Override
    public boolean importContacts(final User user, final CountryTypeEnum selectedCountryType, final byte[] bytes) throws BusinessException {    
        return false;
    }

    @Override
    public Contact resetAllChangedFlags(final Contact contact) {
        contact.setInstitutionChanged(false);
        contact.setAdditionalNameInformationChanged(false);
        contact.setContactPersonChanged(false);
        contact.setShortCodeChanged(false);
        contact.setCustomerNumberContainersChanged(false);
        contact.setDeliveryNumberContainersChanged(false);
        contact.setAgentNumberContainersChanged(false);
        contact.setEmailsChanged(false);
        contact.setTelephonesChanged(false);
        contact.setInformationChanged(false);
        contact.setAddressesWithProductsChanged(false);

        if (contact.getAddress() != null) {
            contact.getAddress().setFlfIdChanged(false);
            contact.getAddress().setAdditionalInformationChanged(false);
            contact.getAddress().setInformationChanged(false);
            contact.getAddress().setCustomerNumberContainersChanged(false);
            contact.getAddress().setStreetChanged(false);
            contact.getAddress().setIntSignChanged(false);
            contact.getAddress().setPostalCodeChanged(false);
            contact.getAddress().setRegionChanged(false);
            contact.getAddress().setProvinceTypeChanged(false);
            contact.getAddress().setCountryChanged(false);
            contact.getAddress().setTelephoneChanged(false);
            contact.getAddress().setEmailChanged(false);
            contact.getAddress().setInformationChanged(false);
        }

        if (contact.getAddressesWithProducts() != null && !contact.getAddressesWithProducts().isEmpty()) {
            for (final ContactAddressWithProducts contactAddressWithProducts : contact.getAddressesWithProducts()) {
                final Address address = contactAddressWithProducts.getAddress();
                address.setFlfIdChanged(false);
                address.setAdditionalInformationChanged(false);
                address.setInformationChanged(false);
                address.setCustomerNumberContainersChanged(false);
                address.setStreetChanged(false);
                address.setIntSignChanged(false);
                address.setPostalCodeChanged(false);
                address.setRegionChanged(false);
                address.setProvinceTypeChanged(false);
                address.setCountryChanged(false);
                address.setTelephoneChanged(false);
                address.setEmailChanged(false);
                address.setInformationChanged(false);
            }
        }
        return contact;
    }

    @Override
    public boolean isContactASupplier(final Contact contact) {
        final List<CustomerNumberContainer> customerNumberContainers = contact.getCustomerNumberContainers();
        final List<DeliveryNumberContainer> deliveryNumberContainers = contact.getDeliveryNumberContainers();

        // if a delivery number is set for contact
        if (deliveryNumberContainers != null && !deliveryNumberContainers.isEmpty()) {
            return true;
        }
        // if customer number is not set for contact
        if (customerNumberContainers == null || customerNumberContainers.isEmpty()) {
            return true;
        }
        return false;
    }

    private void deleteContact(final Contact contact) throws BusinessException {
        final Address address = contact.getAddress();
        final Address accountAddress = contact.getAccountAddress();
        final List<ContactAddressWithProducts> contactAddressesWithProducts = contact.getAddressesWithProducts();
        if (address != null) {
            addressService.delete(address);
        }
        if (accountAddress != null) {
            addressService.delete(accountAddress);
        }
        if (contactAddressesWithProducts != null) {
            for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
                final Address contactAddress = contactAddressWithProducts.getAddress();
                if (contactAddress != null) {
                    addressService.delete(contactAddress);
                }
            }
        }
        contactRepository.delete(contact);
    }

    @Override
    public String toString() {
        return "[ContactServiceImpl]";
    }

}
