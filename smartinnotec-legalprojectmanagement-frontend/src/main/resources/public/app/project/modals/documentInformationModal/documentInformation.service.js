(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('documentInformationModalService', documentInformationModalService);
        
    documentInformationModalService.$inject = ['$modal', '$stateParams'];
    
    function documentInformationModalService($modal, $stateParams) {
		var service = {
			showDocumentInformationModal: showDocumentInformationModal
		};		
		return service;
		
		////////////
				
		function showDocumentInformationModal(documentFile, currentUser, userAuthorizationUserContainer, type, invoker, disabled) {
			 var yesNoModal = $modal.open({
				controller: DocumentInformationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	documentFile: function() {
			    		return documentFile;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	type: function() {
			    		return type;
			    	},
			    	invoker: function() {
			    		return invoker; 
			    	},
			    	disabled: function() {
			    		return disabled;
			    	}
			    }, 
				templateUrl: 'app/project/modals/documentInformationModal/documentInformation.modal.html'
			});
			return yesNoModal;
			
			function DocumentInformationModalController($modalInstance, $scope, documentFile, currentUser, userAuthorizationUserContainer, type, invoker, disabled) {
		    	var vm = this; 
		    	vm.documentFile = documentFile;
		    	vm.currentUser = currentUser;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.type = type;
		    	vm.invoker = invoker;
		    	vm.disabled = disabled;
		    	
		    	vm.storeInformation = storeInformation;
		    	vm.closeDocumentInformationModal = closeDocumentInformationModal;
		    	
		    	////////////
		    	
		    	function storeInformation() {
		    		vm.invoker.updateDocumentFile(vm.documentFile, false); 
		    		closeDocumentInformationModal();
		    	}
		    
		    	function closeDocumentInformationModal() {
					$modalInstance.dismiss('cancel'); 
				}
			} 
		}
    }
})();