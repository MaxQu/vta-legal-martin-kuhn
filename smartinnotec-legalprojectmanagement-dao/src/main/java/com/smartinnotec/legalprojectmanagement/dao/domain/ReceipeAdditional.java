package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.Data;

public @Data class ReceipeAdditional {

    @DBRef
    private Product product;
    private UnityTypeEnum unityType;
    private Float amount;
    private Float result;
}
