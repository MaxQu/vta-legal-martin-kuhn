(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.controller('CalendarController', CalendarController);
    
    CalendarController.$inject = ['$scope', '$filter', 'calendarConfig', 'moment', 'calendarEventSerialDateTypes', 'calendarEventTypeEnums', 'currentUser', 'defaultProject', 'userAuthorizationUserContainer', 'optionsService', 'projectsService', 'calendarEvents', 'createNewEventModalService', 'contactAndUserService', 'deleteCalendarEventModalService', 'userOfCalendarEventModalService', 'calendarService', 'printCalendarEventsModalService', 'calendarEventUserConnectionService', 'calendarICSLinkModalService', 'calendarEventClickedModalService', 'calendarEventInListModalService', 'dateUtilityService'];
    
    function CalendarController($scope, $filter, calendarConfig, moment, calendarEventSerialDateTypes, calendarEventTypeEnums, currentUser, defaultProject, userAuthorizationUserContainer, optionsService, projectsService, calendarEvents, createNewEventModalService, contactAndUserService, deleteCalendarEventModalService, userOfCalendarEventModalService, calendarService, printCalendarEventsModalService, calendarEventUserConnectionService, calendarICSLinkModalService, calendarEventClickedModalService, calendarEventInListModalService, dateUtilityService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	moment.locale('de', { 
  		  week : {
  		    dow : 1
  		  }
  		});
    	calendarConfig.dateFormatter = 'moment';
      	calendarConfig.i18nStrings.weekNumber = 'Woche {week}'; 
      	calendarConfig.allDateFormats.moment.date.hour = 'HH:mm';
      	calendarConfig.displayAllMonthEvents = true; 
      	vm.currentUser = currentUser;
    	vm.calendarView = 'month';
    	vm.calendarDate = new Date();
    	vm.calendarEvents = prepareCalendarEvents(calendarEvents.data);
    	vm.filteredEvents = vm.calendarEvents;
    	vm.calendarEventTypes = calendarEventTypeEnums.data;
    	vm.allContactsAndUsers = null;
    	vm.projects = null;
    	vm.selectedFilterProject = null;
    	vm.defaultProject = defaultProject.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.calendarEventSerialDateTypes = calendarEventSerialDateTypes.data;
    	vm.allCalendarEventsTrigger = false;
    	vm.deactivatedCalendarEventsTrigger = false;
    	vm.loadAllContactsAndUsers = true;
    	
    	vm.showCreateNewEventModal = showCreateNewEventModal;
    	vm.prepareAndAddCalendarEvent = prepareAndAddCalendarEvent;
    	vm.prepareAndUpdateCalendarEvent = prepareAndUpdateCalendarEvent;
    	vm.deleteCalendarEvent = deleteCalendarEvent;
    	vm.setSelectedFilterProject = setSelectedFilterProject;
    	vm.unselectProjects = unselectProjects;
    	vm.showPrintCalendarEvents = showPrintCalendarEvents;
    	vm.showCalendarICSLink = showCalendarICSLink;
    	vm.eventClicked = eventClicked;
    	vm.searchCalendarEvents = searchCalendarEvents;
    	vm.showAllCalendarEvents = showAllCalendarEvents;
    	vm.showDeactivatedCalendarEvents = showDeactivatedCalendarEvents;
    	vm.showCalendarEventsInList = showCalendarEventsInList;
    	findProjectsOfUser();
    	getAllContactsAndUsers();
    	
    	////////////
    	
    	$scope.$watch('vm.calendarView', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			if(vm.allCalendarEventsTrigger == true) {
    				showAllCalendarEvents();
    			} else {
    				loadPreparedCalendarEvents(false);
    			}
    		}
		});
    	
    	$scope.$watch('vm.calendarDate', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			if(vm.allCalendarEventsTrigger == true) {
    				showAllCalendarEvents();
    			} else {
    				loadPreparedCalendarEvents(false);
    			}
    		}
		});
    	
    	$scope.$watch('vm.allCalendarEventsTrigger', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			if(vm.allCalendarEventsTrigger == true) {
    				showAllCalendarEvents();
    			} else {
    				loadPreparedCalendarEvents(false);
    			}
    		}
		});
    	
    	$scope.$watch('vm.selectedFilterProject', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			setFilteredEvents();
    		}
		});
    	
    	function prepareAndUpdateCalendarEvent(calendarEvent) {
    		if(vm.allCalendarEventsTrigger == true) {
				showAllCalendarEvents();
			} else {
				loadPreparedCalendarEvents(false);
			}
    	}
    	
    	function searchCalendarEvents() {
			var day = vm.calendarDate.getDate();
    		var month = vm.calendarDate.getMonth()+1;
    		var year = vm.calendarDate.getFullYear();   
			if(vm.calendarEventSearchString != null && vm.calendarEventSearchString.length > 3) {
        		calendarService.getPreparedFilteredCalendarEvents(currentUser.id, vm.calendarView.toUpperCase(), vm.calendarEventSearchString, day, month, year).then(function (response) {
        			vm.calendarEvents = prepareCalendarEvents(response.data);
        			setFilteredEvents();
    			}).catch(function (data) {
    				console.log('error in calendar.controller.js#searchCalendarEvents: ' + data);
    			});
			} else {
				calendarEventUserConnectionService.getPreparedCalendarEventsOfUser(currentUser.id, vm.calendarView.toUpperCase(), day, month, year, false).then(function (response) {
        			vm.calendarEvents = prepareCalendarEvents(response.data);
        			setFilteredEvents();
    			}).catch(function (data) {
    				console.log('error in calendar.controller.js#searchCalendarEvents: ' + data);
    			});
			}
		}
    	
    	function eventClicked(calendarEvent) {
    		calendarEventClickedModalService.showCalendarEventClickedModal(calendarEvent);
    	}
    	
    	function setSelectedFilterProject(project) {
    		unselectProjects();
    		vm.selectedFilterProject = project;
    		project.selected = true;
    	}
    	
    	function unselectProjects() {
    		vm.selectedFilterProject = null;
    		for(var i = 0; i < vm.projects.length; i++) {
    			vm.projects[i].selected = false;
    		}
    		if(vm.allCalendarEventsTrigger == true) {
				showAllCalendarEvents();
			} else {
				loadPreparedCalendarEvents(false);
			}
    	}
    	
    	function setFilteredEvents() { 
    		var events = vm.calendarEvents;
    		if(vm.selectedFilterProject != null) {
    			events = $filter('filter')(events, {projectId: vm.selectedFilterProject.id});
    		}
    		vm.filteredEvents = events;
    	}
    	
    	function loadPreparedCalendarEvents(archived) {
    		var day = vm.calendarDate.getDate();
    		var month = vm.calendarDate.getMonth()+1;
    		var year = vm.calendarDate.getFullYear();
    		
    		calendarEventUserConnectionService.getPreparedCalendarEventsOfUser(currentUser.id, vm.calendarView.toUpperCase(), day, month, year, archived).then(function (response) {
    			vm.calendarEvents = prepareCalendarEvents(response.data);
    			setFilteredEvents();
			}).catch(function (data) {
				console.log('error in calendar.controller#loadPreparedCalendarEvents: ' + data);
			});
		}
    	
    	function showAllCalendarEvents() {
    		var day = vm.calendarDate.getDate();
    		var month = vm.calendarDate.getMonth()+1;
    		var year = vm.calendarDate.getFullYear();
    		calendarEventUserConnectionService.getPreparedCalendarEventsOfAllUsers(currentUser.id, vm.calendarView.toUpperCase(), day, month, year).then(function (response) {
    			vm.filteredEvents = prepareCalendarEvents(response.data);
			}).catch(function (data) {
				console.log('error in calendar.controller#showAllCalendarEvents: ' + data);
			});
    	}
    	
    	function showDeactivatedCalendarEvents() {
    		if(vm.deactivatedCalendarEventsTrigger) {
    			loadPreparedCalendarEvents(true);
    		} else {
    			loadPreparedCalendarEvents(false);
    		}
    	}
    	
    	function showCreateNewEventModal() {
    		createNewEventModalService.showCreateNewEventModal(vm.calendarEventSerialDateTypes, currentUser, vm.selectedFilterProject, vm.projects, vm.allContactsAndUsers, vm, null, /*calendarEventUserConnections*/[], 'CREATE');
    	}
    	
    	function findProjectsOfUser() {
    		projectsService.findProjectsOfUser(currentUser.id).then(function (response) {
    			vm.projects = response.data;
    			if(vm.projects.length == 1) {
    				vm.selectedFilterProject = vm.projects[0];
    				vm.projects[0].selected = true;
    			}
    			setDefaultProject();
    		}).catch(function (data) { 
				console.log('error calendar.controller#findProjectsOfUser: ' + data);
			});
    	}
    	
    	function setDefaultProject() {
    		if(vm.defaultProject != null) {
    			for(var i = 0; i < vm.projects.length; i++) {
    				if(vm.projects[i].id == vm.defaultProject.id) {
    					setSelectedFilterProject(vm.projects[i]);
    					vm.projects[i].selected = true;
    		    		break;
    				}
    			}
	    	}
    	}
    	
    	function getAllContactsAndUsers() {
    		contactAndUserService.findAllContactsAndUsers().then(function (response) {
    			vm.allContactsAndUsers = response.data;
    			vm.loadAllContactsAndUsers = false;
			}).catch(function (data) {
				console.log('error createNewCalendarEvent.service#addProject#getAllContactsAndUsers: ' + data);
			});
    	}
    	
    	function prepareCalendarEvents(calendarEvents) {
    		angular.forEach(calendarEvents, function(value, key) {
    			value.actions = createActions();
			});
    		return calendarEvents;
    	}
    	
    	function prepareAndAddCalendarEvent(calendarEvents) {
    		for(var i = 0; i < calendarEvents.length; i++) {
    			calendarEvents[i].actions = createActions();
        		vm.filteredEvents.push(calendarEvents[i]);
    		}
    	}
    	
    	function deleteCalendarEvent(calendarEvent, deleteSerialDates) {
    		var i  = vm.filteredEvents.length;
    		while(i--) {
    			if(deleteSerialDates === false) {
	    			if(vm.filteredEvents[i].id == calendarEvent.id) {
	    				vm.filteredEvents.splice(i, 1);
	    				break;
	    			}
    			} else if(deleteSerialDates === true) {
    				if(vm.filteredEvents[i].serialDateNumber == calendarEvent.serialDateNumber) {
	    				vm.filteredEvents.splice(i, 1);
	    			}
    			}
			}
    	}
    	
    	function createActions() {
    		var actions = [{
  		      label: vm.currentUser.userAuthorizationUserContainer.userAuthorizations[7].state === true ? '<i class=\'glyphicon glyphicon-pencil\'></i>' : '',
  		      onClick: function(args) {
  		    	calendarService.findCalendarEventById(args.calendarEvent.id).then(function (response) {
  		    		var originalCalendarEvent = response.data;
  		    		calendarEventUserConnectionService.findCalendarEventUserConnectionsByCalendarEvent(args.calendarEvent.id).then(function (response) {
  		    			var calendarEventUserConnections = response.data;
  		    			createNewEventModalService.showCreateNewEventModal(vm.calendarEventSerialDateTypes, currentUser, null, vm.projects, vm.allContactsAndUsers, vm, originalCalendarEvent, calendarEventUserConnections, 'EDIT');
  		    		}).catch(function (data) {
  		    			console.log('error calendar.controller#userOnClick: ' + data);
  		    		});
		    	}).catch(function (data) {
					console.log('error calendar.controller#userOnClick: ' + data);
				});
  		      }
  		    }, {
  		      label: userAuthorizationUserContainer.data.userAuthorizations[5].state === true ? '<i class=\'glyphicon glyphicon-remove\'></i>' : '',
  		      onClick: function(args) {
  		    	deleteCalendarEventModalService.showDeleteCalendarEventModal(args.calendarEvent, vm);
  		      }
  		    },{
		      label: '<i class=\'glyphicon glyphicon-user\'></i>',
		      onClick: function(args) {
		    	  calendarEventUserConnectionService.findCalendarEventUserConnectionsByCalendarEvent(args.calendarEvent.id).then(function (response) {
		    		  var calendarEventUserConnections = response.data;
		    		 userOfCalendarEventModalService.showUserOfCalendarEventModal(args.calendarEvent, calendarEventUserConnections, vm);
		    	  }).catch(function (data) {
					console.log('error calendar.controller#userOnClick: ' + data);
		    	  });
  		      }
  		    }];
    		return actions;
    	}
 
    	function showPrintCalendarEvents(calendarEvents, calendarEventSearchString) {
    		printCalendarEventsModalService.showPrintCalendarEventsModal(calendarEvents, calendarEventSearchString, vm.allCalendarEventsTrigger);
    	}
    	
    	function showCalendarICSLink() { 
    		calendarService.getICSLinkOfUser(currentUser.id).then(function (response) {
    			calendarICSLinkModalService.showCalendarICSLinkModal(vm.currentUser, response.data);
			}).catch(function (data) {
				console.log('error calendar.controller#showCalendarICSLink: ' + data);
			});
    	}
    	
    	function showCalendarEventsInList() {
    		var start = dateUtilityService.oneMonthBack(new Date());
     		var end = new Date();
     		var startString = dateUtilityService.formatDateToString(start);
     		var endString = dateUtilityService.formatDateToString(end);
     		
     		calendarEventInListModalService.showCalendarEventInListModal(start, end, vm.calendarEventTypes);
    	}
	} 
})();