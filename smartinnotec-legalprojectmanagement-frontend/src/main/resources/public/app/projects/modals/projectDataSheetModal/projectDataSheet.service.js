(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectDataSheetService', projectDataSheetService);
        
    projectDataSheetService.$inject = ['$modal', '$stateParams'];
    
    function projectDataSheetService($modal, $stateParams) {
		var service = {
			showProjectDataSheetModal: showProjectDataSheetModal
		};		
		return service;
		
		////////////
				
		function showProjectDataSheetModal(projectDataSheet) {
			 var projectDataSheetModal = $modal.open({
				controller: ProjectDataSheetModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: { 
			    	projectDataSheet: function() {
			    		return projectDataSheet;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/projectDataSheetModal/projectDataSheet.modal.html'
			});
			return projectDataSheetModal;
			
			function ProjectDataSheetModalController($modalInstance, $scope, projectDataSheet) {
		    	var vm = this; 
		    	vm.projectDataSheet = projectDataSheet;
		    	
		    	vm.closeProjectDataSheetModal = closeProjectDataSheetModal;
		    	
		    	////////////
		    	
		    	function closeProjectDataSheetModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();