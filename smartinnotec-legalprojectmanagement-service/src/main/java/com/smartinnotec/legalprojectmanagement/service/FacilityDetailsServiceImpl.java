package com.smartinnotec.legalprojectmanagement.service;

import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.FacilityDetails;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.FacilityDetailsRepository;

@Service("facilityDetailsService")
public class FacilityDetailsServiceImpl extends AbstractService implements FacilityDetailsService {

    private static final int LOOP_AMOUNT;
    @Autowired
    private FacilityDetailsRepository facilityDetailsRepository;
    @Autowired
    private CalendarEventService calendarEventService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private UserService userService;

    static {
        LOOP_AMOUNT = 20;
    }

    @Override
	public Integer count() throws BusinessException {
    	logger.info("count facilityDetails in FacilityDetailsServiceImpl#count");
		final Long facilityDetails = facilityDetailsRepository.count();
		return facilityDetails.intValue();
	}
    
    @Override
    public FacilityDetails create(final FacilityDetails facilityDetails) throws BusinessException {
        BusinessAssert.notNull(facilityDetails, "facilityDetails is mandatory in FacilityDetailsServiceImpl#create", "400");
        logger.info("create facilityDetails in FacilityDetailsServiceImpl#create");
        facilityDetails.setCreationDate(new DateTime());
        return facilityDetailsRepository.insert(facilityDetails);
    }

    @Override
    public FacilityDetails update(FacilityDetails facilityDetails) throws BusinessException {
        BusinessAssert.notNull(facilityDetails, "facilityDetails is mandatory in FacilityDetailsServiceImpl#update", "400");
        logger.info("update facilityDetails with id '{}' in FacilityDetailsServiceImpl#update", facilityDetails.getId());
        return facilityDetailsRepository.save(facilityDetails);
    }

    @Override
    public FacilityDetails findById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in FacilityDetailsServiceImpl#findById", "400");
        BusinessAssert.isId(id, "id must be an id in FacilityDetailsServiceImpl#findById", "400");
        logger.info("find facilityDetail with id '{}' in FacilityDetailsServiceImpl#findById", id);
        return facilityDetailsRepository.findOne(id);
    }

    @Override
    public List<FacilityDetails> getFacilityDetailsByCalendarEventId(final String calendarEventId) throws BusinessException {
        BusinessAssert.notNull(calendarEventId, "calendarEventId is mandatory in FacilityDetailsServiceImpl#getByCalendarEventId", "400");
        BusinessAssert.isId(calendarEventId, "calendarEventId must be an id in FacilityDetailsServiceImpl#getByCalendarEventId", "400");
        logger.info("get facilityDetails by calendarEvent with id '{}' in FacilityDetailsServiceImpl#getByCalendarEventId",
            calendarEventId);
        return facilityDetailsRepository.findByCalendarEventId(calendarEventId);
    }

    @Override
    public FacilityDetails getLastFacilityDetailOfCalendarEventIdOrContact(final String calendarEventId) throws BusinessException {
        BusinessAssert.notNull(calendarEventId,
            "calendarEventId is mandatory in FacilityDetailsServiceImpl#getLastFacilityDetailOfCalendarEventIdOrContact", "400");
        BusinessAssert.isId(calendarEventId,
            "calendarEventId must be an id in FacilityDetailsServiceImpl#getLastFacilityDetailOfCalendarEventIdOrContact", "400");
        logger.info(
            "get last facilityDetail of calendarEvent with id '{}' or contact in FacilityDetailsServiceImpl#getLastFacilityDetailOfCalendarEventIdOrContact",
            calendarEventId);
        FacilityDetails facilityDetail = facilityDetailsRepository.findFirst1ByCalendarEventIdOrderByCreationDateDesc(calendarEventId);
        if (facilityDetail == null) {
           
            
        }
        return facilityDetail;
    }

    @Override
    public FacilityDetails getLastFacilityDetailOfCalendarEventId(final String calendarEventId) throws BusinessException {
        BusinessAssert.notNull(calendarEventId,
            "calendarEventId is mandatory in FacilityDetailsServiceImpl#getLastFacilityDetailOfCalendarEventId", "400");
        BusinessAssert.isId(calendarEventId,
            "calendarEventId must be an id in FacilityDetailsServiceImpl#getLastFacilityDetailOfCalendarEventId", "400");
        logger.info(
            "get last facilityDetail of calendarEvent with id '{}' in FacilityDetailsServiceImpl#getLastFacilityDetailOfCalendarEventId",
            calendarEventId);
        return facilityDetailsRepository.findFirst1ByCalendarEventIdOrderByCreationDateDesc(calendarEventId);
    }

    @Override
    public List<FacilityDetails> getByContactId(final String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId is mandatory in FacilityDetailsServiceImpl#getByContactId", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in FacilityDetailsServiceImpl#getByContactId", "400");
        logger.info("get facilityDetails of contact with id '{}' in FacilityDetailsServiceImpl#getByContactId", contactId);
        final List<FacilityDetails> facilityDetails = facilityDetailsRepository.findByContactId(contactId);
        for (final FacilityDetails facilityDetail : facilityDetails) {
            final CalendarEvent calendarEvent = calendarEventService.findById(facilityDetail.getCalendarEventId());
            final Contact contact = contactService.findContactById(facilityDetail.getContactId());
            final User user = userService.findUser(facilityDetail.getUserId());
            facilityDetail.setCalendarEvent(calendarEvent);
            facilityDetail.setContact(contact);
            facilityDetail.setUser(user);
        }
        return facilityDetails;
    }

    @Override
    public List<FacilityDetails> getByContactIdInRange(final String calendarEventId, final DateTime startDateTime,
            final DateTime endDateTime) throws BusinessException {
        BusinessAssert.notNull(calendarEventId, "contactId is mandatory in FacilityDetailsServiceImpl#getByContactIdInRange", "400");
        BusinessAssert.isId(calendarEventId, "contactId must be an id in FacilityDetailsServiceImpl#getByContactIdInRange", "400");
        BusinessAssert.notNull(startDateTime, "startDateTime is mandatory in FacilityDetailsServiceImpl#getByContactIdInRange", "400");
        BusinessAssert.notNull(endDateTime, "endDateTime is mandatory in FacilityDetailsServiceImpl#getByContactIdInRange", "400");
        logger.info(
            "get facilityDetails of contact with id '{}' from start '{}' to end '{}' in FacilityDetailsServiceImpl#getByContactIdInRange",
            calendarEventId, dateTimeFormatter.print(startDateTime), dateTimeFormatter.print(endDateTime));

        final List<FacilityDetails> facilityDetails = facilityDetailsRepository
            .findByContactIdAndCalendarEventStartDateBetween(calendarEventId, startDateTime, endDateTime);
        for (final FacilityDetails facilityDetail : facilityDetails) {
            final CalendarEvent calendarEvent = calendarEventService.findById(facilityDetail.getCalendarEventId());
            final Contact contact = contactService.findContactById(facilityDetail.getContactId());
            final User user = userService.findUser(facilityDetail.getUserId());
            facilityDetail.setCalendarEvent(calendarEvent);
            facilityDetail.setContact(contact);
            facilityDetail.setUser(user);
        }
        return facilityDetails;
    }
    
    @Override
	public List<FacilityDetails> findByCalendarEventStartDateBetween(final DateTime start, final DateTime end)
			throws BusinessException {
    	BusinessAssert.notNull(start, "start is mandatory in FacilityDetailsServiceImpl#findByCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in FacilityDetailsServiceImpl#findByCalendarEventStartDateBetween", "400");
        logger.info("find facilityDetails from start '{}' to end '{}' in FacilityDetailsServiceImpl#findByCalendarEventStartDateBetween", start, end);
		return facilityDetailsRepository.findByCalendarEventStartDateBetween(start, end);
	}

	@Override
	public List<FacilityDetails> findByCalendarEventIdAndCalendarEventStartDateBetween(final String calendarEventId,
			final DateTime start, final DateTime end) throws BusinessException {
		BusinessAssert.notNull(calendarEventId, "calendarEventId is mandatory in FacilityDetailsServiceImpl#findByCalendarEventIdAndCalendarEventStartDateBetween", "400");
		BusinessAssert.isId(calendarEventId, "calendarEventId must be an id in FacilityDetailsServiceImpl#findByCalendarEventIdAndCalendarEventStartDateBetween", "400");
		BusinessAssert.notNull(start, "start is mandatory in FacilityDetailsServiceImpl#findByCalendarEventIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in FacilityDetailsServiceImpl#findByCalendarEventIdAndCalendarEventStartDateBetween", "400");
		return facilityDetailsRepository.findByCalendarEventIdAndCalendarEventStartDateBetween(calendarEventId, start, end);
	}

	@Override
	public List<FacilityDetails> findByUserIdAndCalendarEventStartDateBetween(final String userId, final DateTime start,
			DateTime end) throws BusinessException {
		BusinessAssert.notNull(userId, "userId is mandatory in FacilityDetailsServiceImpl#findByUserIdAndCalendarEventStartDateBetween", "400");
		BusinessAssert.isId(userId, "userId must be an id in FacilityDetailsServiceImpl#findByUserIdAndCalendarEventStartDateBetween", "400");
		BusinessAssert.notNull(start, "start is mandatory in FacilityDetailsServiceImpl#findByUserIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in FacilityDetailsServiceImpl#findByUserIdAndCalendarEventStartDateBetween", "400");
		return facilityDetailsRepository.findByUserIdAndCalendarEventStartDateBetween(userId, start, end);
	}

	@Override
	public List<FacilityDetails> findByUserIdAndContactIdAndCalendarEventStartDateBetween(final String userId,
			final String contactId, final DateTime start, final DateTime end) throws BusinessException {
		BusinessAssert.notNull(userId, "userId is mandatory in FacilityDetailsServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
		BusinessAssert.isId(userId, "userId must be an id in FacilityDetailsServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
		BusinessAssert.notNull(contactId, "contactId is mandatory in FacilityDetailsServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
		BusinessAssert.isId(contactId, "contactId must be an id in FacilityDetailsServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
		BusinessAssert.notNull(start, "start is mandatory in FacilityDetailsServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in FacilityDetailsServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
		return facilityDetailsRepository.findByUserIdAndContactIdAndCalendarEventStartDateBetween(userId, contactId, start, end);
	}

    @Override
    public void delete(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in FacilityDetailsServiceImpl#delete", "400");
        BusinessAssert.isId(id, "id must be an id in FacilityDetailsServiceImpl#delete", "400");
        logger.info("delete facilityDetail with id '{}' in FacilityDetailsServiceImpl#delete", id);
        facilityDetailsRepository.delete(id);
    }

    @Override
    public String toString() {
        return "[FacilityDetailsServiceImpl]";
    }
}
