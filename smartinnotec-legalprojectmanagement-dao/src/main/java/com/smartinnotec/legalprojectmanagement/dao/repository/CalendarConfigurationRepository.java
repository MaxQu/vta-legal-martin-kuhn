package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarConfiguration;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarConfigurationTypeEnum;

public interface CalendarConfigurationRepository extends MongoRepository<CalendarConfiguration, String> {

    List<CalendarConfiguration> findByCalendarConfigurationTypeOrderByOrderNumberAsc(
            final CalendarConfigurationTypeEnum calendarConfigurationType);

}
