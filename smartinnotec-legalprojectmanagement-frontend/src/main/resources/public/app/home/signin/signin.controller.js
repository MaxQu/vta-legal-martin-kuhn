(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.controller('SigninController', SigninController);
    
    SigninController.$inject = ['$scope', '$state', 'authUserService'];
    
    function SigninController($scope, $state, authUserService) {
	    $scope.vm = this;
    	var vm = this;
    	
    	vm.navigator = navigator;
    	vm.objappVersion = navigator.appVersion; 
    	vm.objAgent = navigator.userAgent; 
    	vm.objbrowserName = navigator.appName; 
    	vm.objfullVersion = ''+parseFloat(navigator.appVersion); 
    	vm.objBrMajorVersion = parseInt(navigator.appVersion,10); 
    	vm.objOffsetName = null;
    	vm.objOffsetVersion = null;
    	vm.ix = null; 
    	
    	vm.user = {
    		username: '',
    		password: ''
    	};
    	
		vm.signin = signin;
		detectBrowser();
		
		////////////
			
		function signin(user) {
			authUserService.signin(user.username, user.password).then(function (response) {
				$state.go('auth.dashboard', {userId : response.data.user.id});
			}).catch(function (response) {
				vm.signinError = response.data;
			});
		}
		
		function detectBrowser() {
			if ((vm.objOffsetVersion=vm.objAgent.indexOf("Chrome"))!=-1) { 
				vm.objbrowserName = "Chrome"; 
				vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+7); 
	    	} 
	    	else if ((vm.objOffsetVersion=vm.objAgent.indexOf("MSIE"))!=-1) { 
	    		vm.objbrowserName = "Microsoft Internet Explorer"; 
	    		vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+5); 
	    	} 
	    	else if ((vm.objOffsetVersion=vm.objAgent.indexOf("Firefox"))!=-1) { 
	    		vm.objbrowserName = "Firefox"; 
	    	} 
	    	else if ((vm.objOffsetVersion=vm.objAgent.indexOf("Safari"))!=-1) { 
	    		vm.objbrowserName = "Safari"; 
	    		vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+7); 
	    		if ((vm.objOffsetVersion=vm.objAgent.indexOf("Version"))!=-1) {
	    			vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+8);
	    		}
	    	} 
	    	else if ((vm.objOffsetVersion=vm.objAgent.indexOf("Opera"))!=-1) { 
	    		vm.objbrowserName = "Opera"; 
	    		vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+7); 
	    		if ((vm.objOffsetVersion=vm.objAgent.indexOf("Version"))!=-1) {
	    			vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+8);
	    		}
	    	} 
	    	// For other browser "name/version" is at the end of userAgent 
	    	else if ( (vm.objOffsetName=vm.objAgent.lastIndexOf(' ')+1) < (vm.objOffsetVersion=vm.objAgent.lastIndexOf('/')) ) { 
	    		vm.objbrowserName = vm.objAgent.substring(vm.objOffsetName,vm.objOffsetVersion); 
	    		vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+1); 
	    		if (vm.objbrowserName.toLowerCase()==vm.objbrowserName.toUpperCase()) { 
	    			vm.objbrowserName = navigator.appName;
	    			} 
	    		} 
	    	// trimming the fullVersion string at semicolon/space if present 
	    	if ((vm.ix=vm.objfullVersion.indexOf(";"))!=-1) {
	    		vm.objfullVersion=vm.objfullVersion.substring(0,vm.ix);
	    	}
	    	if ((vm.ix=vm.objfullVersion.indexOf(" "))!=-1) { 
	    		vm.objfullVersion=vm.objfullVersion.substring(0,vm.ix);
	    	}
	    	vm.objBrMajorVersion = parseInt(''+vm.objfullVersion,10); 
	    	if (isNaN(vm.objBrMajorVersion)) { 
	    		vm.objfullVersion = ''+parseFloat(navigator.appVersion); 
	    		vm.objBrMajorVersion = parseInt(navigator.appVersion,10); } 
		}
	}
    
})();