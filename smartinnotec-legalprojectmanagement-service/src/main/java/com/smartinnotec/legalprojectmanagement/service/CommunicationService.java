package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Communication;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationAttachedDocumentFile;

public interface CommunicationService {

    Communication findCommunication(final String id) throws BusinessException;

    List<Communication> findByAttachedFileUrlsNotNull() throws BusinessException;

    Communication createCommunication(final Communication communication) throws BusinessException;

    Communication updateCommunication(final Communication communication) throws BusinessException;

    void deleteCommunication(final String id) throws BusinessException;

    List<CommunicationAttachedDocumentFile> createAttachmentFileUrls(final String userIdSender,
            final List<CommunicationSelectedDocumentFile> communicationSelectedDocumentFiles)
            throws BusinessException;

}
