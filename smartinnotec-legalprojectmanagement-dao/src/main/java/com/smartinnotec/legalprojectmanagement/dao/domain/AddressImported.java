package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;

import lombok.Data;

@Document(collection = "AddressImported")
public @Data class AddressImported {

    @JsonView(RestServiceResponseView.AddressPublic.class)
    @Id
    private String id;
    private String flfId;
    private boolean flfIdChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String additionalInformation;
    private boolean additionalInformationChanged;
    @TextIndexed
    private List<CustomerNumberContainer> customerNumberContainers;
    private boolean customerNumberContainersChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String street;
    private boolean streetChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String intSign;
    private boolean intSignChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String postalCode;
    private boolean postalCodeChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String region;
    private boolean regionChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private ProvinceTypeEnum provinceType;
    private boolean provinceTypeChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private CountryTypeEnum country;
    private boolean countryChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String telephone;
    private boolean telephoneChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String email;
    private boolean emailChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String information;
    private boolean informationChanged;

    public void addCustomerNumberContainer(final CustomerNumberContainer customerNumberContainer) {
        if (customerNumberContainers == null) {
            customerNumberContainers = new ArrayList<>();
        }
        customerNumberContainers.add(customerNumberContainer);
    }

    public AddressImported deepCopy() {
        final AddressImported addressImported = new AddressImported();
        addressImported.setFlfId(this.flfId);
        addressImported.setFlfIdChanged(this.isFlfIdChanged());
        addressImported.setAdditionalInformation(this.getAdditionalInformation());
        addressImported.setAdditionalInformationChanged(this.isAdditionalInformationChanged());
        if (this.customerNumberContainers != null) {
            final List<CustomerNumberContainer> customerNumberContainers = new ArrayList<>();
            for (final CustomerNumberContainer customerNumberContainer : this.customerNumberContainers) {
                final CustomerNumberContainer copiedCustomerNumberContainer = customerNumberContainer.deepCopy();
                customerNumberContainers.add(copiedCustomerNumberContainer);
            }
        }
        addressImported.setCustomerNumberContainers(customerNumberContainers);
        addressImported.setCustomerNumberContainersChanged(this.isCustomerNumberContainersChanged());
        addressImported.setStreet(this.getStreet());
        addressImported.setStreetChanged(this.isStreetChanged());
        addressImported.setIntSign(this.intSign);
        addressImported.setIntSignChanged(this.isIntSignChanged());
        addressImported.setPostalCode(this.postalCode);
        addressImported.setPostalCodeChanged(this.isPostalCodeChanged());
        addressImported.setRegion(this.region);
        addressImported.setRegionChanged(this.isRegionChanged());
        addressImported.setProvinceType(this.provinceType);
        addressImported.setProvinceTypeChanged(this.isProvinceTypeChanged());
        addressImported.setCountry(this.getCountry());
        addressImported.setCountryChanged(this.isCountryChanged());
        addressImported.setTelephone(telephone);
        addressImported.setTelephoneChanged(this.isTelephoneChanged());
        addressImported.setEmail(this.email);
        addressImported.setEmailChanged(this.isEmailChanged());
        addressImported.setInformation(this.information);
        addressImported.setInformationChanged(this.isInformationChanged());

        return addressImported;
    }
}
