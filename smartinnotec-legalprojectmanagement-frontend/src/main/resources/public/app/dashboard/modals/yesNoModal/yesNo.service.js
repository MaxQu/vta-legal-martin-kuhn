(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('dashboardYesNoModalService', dashboardYesNoModalService);
        
    dashboardYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function dashboardYesNoModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(vm, communicationUserConnection, type) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return vm;
			    	},
			    	communicationUserConnection: function() {
			    		return communicationUserConnection;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, communicationUserConnection, type) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.communicationUserConnection = communicationUserConnection;
		    	vm.type = type;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		invoker.archiveCommunicationUserConnection(communicationUserConnection, type);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();