package com.smartinnotec.legalprojectmanagement;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.smartinnotec.legalprojectmanagement.dao.repository.UserRepository;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableMongoRepositories(basePackageClasses = { UserRepository.class })
@SpringBootApplication
@EnableAutoConfiguration
public class SmartinnotecLegalProjectManagementApplication {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    /** */
    public SmartinnotecLegalProjectManagementApplication() {
    }

    public SmartinnotecLegalProjectManagementApplication(final String[] args) {
        logger.info("Start application legalProjectManagement");
        final ConfigurableApplicationContext applicationContext = SpringApplication.run(SmartinnotecLegalProjectManagementApplication.class,
            args);
        if (args.length > 1 && args[1].equals("verbose")) {
            final ConfigurableEnvironment configurableEnvironment = applicationContext.getEnvironment();
            final Map<String, Object> systemEnvironment = configurableEnvironment.getSystemEnvironment();
            for (final String key : systemEnvironment.keySet()) {
                System.out.println("key: " + key + " - " + systemEnvironment.get(key));
            }
        }
        logger.info("Welcome to application legalProjectManagement");
    }

    public static void main(final String[] args) {
        new SmartinnotecLegalProjectManagementApplication(args);
    }

    @Override
    public String toString() {
        return "[SmartinnotecLegalProjectManagementApplication]";
    }
}
