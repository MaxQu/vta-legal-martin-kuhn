(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactAndUserService', contactAndUserService);
        
    contactAndUserService.$inject = ['$http', 'api_config'];
    
    function contactAndUserService($http, api_config) {
		var service = {
			findAllContactsAndUsers: findAllContactsAndUsers,
			findContactsAndUsersBySearchString: findContactsAndUsersBySearchString
		};
		
		return service;
		
		////////////
		
		function findAllContactsAndUsers() {
			return $http.get(api_config.BASE_URL + '/contactsandusers/contactanduser/');
		}
		
		function findContactsAndUsersBySearchString(searchString) {
			return $http.get(api_config.BASE_URL + '/contactsandusers/contactanduser/' + searchString + '/search');
		}
    }
})();