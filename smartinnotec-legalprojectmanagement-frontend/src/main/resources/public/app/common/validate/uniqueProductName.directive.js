(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('uniqueProductName', uniqueProductName);
    
    uniqueProductName.$inject = ['validateService']; 
    
	function uniqueProductName(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////
		
		function link(scope, element, attrs, ngModel) {
			if(attrs.uniqueProductName == 'CREATE') {
				ngModel.$parsers.unshift(parser);
				ngModel.$formatters.unshift(formatter);
			}
			
			////////////
			
			function parser(value) { 
				if(value.length > 3) {
					validateService.checkUniqueProductName(value, attrs.uniqueProductName).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 3) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProductName(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();