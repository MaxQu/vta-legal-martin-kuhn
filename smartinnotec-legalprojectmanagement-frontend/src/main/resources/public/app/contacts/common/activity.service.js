(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('activityService', activityService);
        
    activityService.$inject = ['$http', 'api_config'];
    
    function activityService($http, api_config) {
		var service = {
			getActivitiesOfContactId: getActivitiesOfContactId,
			getActivitiesOfContactIdInRange: getActivitiesOfContactIdInRange,
			update: update,
			deleteActivity: deleteActivity
		};
		
		return service;
		
		////////////
		
		function getActivitiesOfContactId(contactId) {
			return $http.get(api_config.BASE_URL + '/activities/activity/' + contactId + '/contact');
		}
		
		function getActivitiesOfContactIdInRange(contactId, start, end) {
			return $http.get(api_config.BASE_URL + '/activities/activity/' + contactId + '/contact/' + start + '/' + end + '/');
		}
		
		function update(activity) {
			return $http.put(api_config.BASE_URL + '/activities/activity', activity);
		}
		
		function deleteActivity(activity) {
			return $http.delete(api_config.BASE_URL + '/activities/activity/' + activity.id);
		}
    }
})();