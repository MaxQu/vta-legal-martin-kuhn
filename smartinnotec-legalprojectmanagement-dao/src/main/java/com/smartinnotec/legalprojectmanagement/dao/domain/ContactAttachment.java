package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class ContactAttachment {

    private String documentFileId;
    private String filepath;
    private String filename;
    private String version;
    private String ending;
    private String originalFilename;
    private boolean isImage;

}
