package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.web.multipart.MultipartFile;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;

public interface FileHandlingService {

    String storeFile(final String dirName, final String filename, final MultipartFile multipartFile) throws BusinessException;

}
