(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('editThinningModalService', editThinningModalService);
        
    editThinningModalService.$inject = ['$modal', '$stateParams', 'productsService', 'thinningService', 'dateUtilityService'];
    
    function editThinningModalService($modal, $stateParams, productsService, thinningService, dateUtilityService) { 
		var service = {
			showEditThinningModal: showEditThinningModal
		};		
		return service;
		
		////////////
				
		function showEditThinningModal(currentUser, product, products, thinningsOfProduct, start, end, userAuthorizationUserContainer, polymerPromille) {
			 var editThinningModal = $modal.open({
				controller: EditThinningModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	thinningsOfProduct: function() {
			    		return thinningsOfProduct;
			    	},
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	polymerPromille: function() {
			    		return polymerPromille;
			    	}
			    }, 
				templateUrl: 'app/products/modals/editThinningModal/editThinning.modal.html'
			});
			return editThinningModal;
			
			function EditThinningModalController($modalInstance, $scope, currentUser, product, products, thinningsOfProduct, start, end, userAuthorizationUserContainer, polymerPromille) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.thinningsOfProduct = prepareDateOfThinnings(thinningsOfProduct);
		    	vm.start = start;
		    	vm.end = end;
		    	vm.thinningDateFrom = start;
		    	vm.thinningDateUntil = end;
		    	vm.searchThinninigTerm = null;
		    	vm.searchThinningDateFromDatePicker = false;
		    	vm.searchThinningDateUntilDatePicker = false;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.polymerPromille = polymerPromille;
		    	
		    	vm.findThinningsOfProductOfTypeInLastMonth = findThinningsOfProductOfTypeInLastMonth;
		    	vm.openSearchThinningFromDatePicker = openSearchThinningFromDatePicker;
		    	vm.openSearchThinningUntilDatePicker = openSearchThinningUntilDatePicker;
		    	vm.deleteThinning = deleteThinning;
		    	vm.closeEditThinningModal = closeEditThinningModal;  
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	$scope.$watch('vm.thinningDateFrom', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.thinningDateFrom, vm.thinningDateUntil);
			    		if(vm.thinningDateFrom != null && vm.thinningDateUntil != null && checkDateRange == false) {
			    			searchThinningsInTimeRange();
			    		}
		    		}
		        });
		    	
		    	$scope.$watch('vm.thinningDateUntil', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.thinningDateFrom, vm.thinningDateUntil);
			    		if(vm.thinningDateFrom != null && vm.thinningDateUntil != null && checkDateRange == false) {
			    			searchThinningsInTimeRange();
			    		}
		    		}
		        });
		    	
		    	function prepareDateOfThinnings(thinningsOfProduct) {
		    		for(var i = 0; i < thinningsOfProduct.length; i++) {
		    			thinningsOfProduct[i].date = dateUtilityService.convertToDateOrUndefined(thinningsOfProduct[i].date);
		    		}
		    		return thinningsOfProduct;
		    	}
		    	
		    	function searchThinningsInTimeRange() {
		    		var start = dateUtilityService.formatDateToString(vm.thinningDateFrom);
					var end = dateUtilityService.formatDateToString(vm.thinningDateUntil);
					thinningService.findThinningsOfProductInRange(vm.product.id, start, end).then(function(response) {
						vm.thinningsOfProduct = prepareDateOfThinnings(response.data);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error editThinning.service#searchThinningsInTimeRange');
		 	   		 });
		    	}
		    	
		    	function findThinningsOfProductOfTypeInLastMonth() {
		    		vm.thinningDateFrom = dateUtilityService.oneMonthBack(new Date());
			    	vm.thinningDateUntil = new Date();
			    	var startDateString = dateUtilityService.formatDateToString(start);
		    		var endDateString = dateUtilityService.formatDateToString(end);
		    		vm.searchThinninigTerm = null;
			    	thinningService.findThinningsOfProductInRange(product.id, startDateString, endDateString).then(function(response) {
			    		vm.thinningsOfProduct = prepareDateOfThinnings(response.data);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error editThinning.controller#findThinningsOfProductOfTypeInLastMonth');
		 	   		 });
		    	}
		    	
		    	function openSearchThinningFromDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchThinningDateFromDatePicker = true; 
				}
		    	
		    	function openSearchThinningUntilDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchThinningDateUntilDatePicker = true; 
				}
		    	
		    	function deleteThinning(thinning) {
		    		for(var i = 0; i < vm.thinningsOfProduct.length; i++) {
		    			if(vm.thinningsOfProduct[i].id == thinning.id) {
		    				vm.thinningsOfProduct.splice(i, 1);
		    				updateProduct(vm.product.id);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function updateProduct(productId) {
		    		productsService.findProduct(productId).then(function(response) {
		    			var product = response.data;
		    			if(vm.products != null) {
			    			for(var i = 0; i < vm.products.length; i++) {
			    				if(vm.products[i].id == productId) {
			    					vm.products[i] = product;
			    					break;
			    				}
			    			}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error editThinning.service#updateProduct'); 
		  	   		 });
		    	}
		    	
		    	function closeEditThinningModal() {
					$modalInstance.dismiss('cancel'); 
				}
			} 
		}
    }
})();