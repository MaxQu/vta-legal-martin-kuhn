(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.nav', [
    	    'ui.router'
        ]);
})();