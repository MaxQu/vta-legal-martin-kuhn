package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;

import lombok.Data;

@Document(collection = "Address")
public @Data class Address {

    @JsonView(RestServiceResponseView.AddressPublic.class)
    @Id
    private String id;
    private String flfId;
    private boolean flfIdChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String additionalInformation;
    private boolean additionalInformationChanged;
    @TextIndexed
    private List<CustomerNumberContainer> customerNumberContainers;
    private boolean customerNumberContainersChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String street;
    private boolean streetChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String intSign;
    private boolean intSignChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String postalCode;
    private boolean postalCodeChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String region;
    private boolean regionChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private ProvinceTypeEnum provinceType;
    private boolean provinceTypeChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String country;
    private boolean countryChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String telephone;
    private boolean telephoneChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String email;
    private boolean emailChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String information;
    private boolean informationChanged;
    private List<String> userIdBlackList; // if userId added user cannot see this address
    private List<ProjectUserConnectionRoleEnum> rolesBlackList;

    private Geolocation geolocation;

    public Address() {
        this.information = "";
    }

    public void addCustomerNumberContainer(final CustomerNumberContainer customerNumberContainer) {
        if (customerNumberContainers == null) {
            customerNumberContainers = new ArrayList<>();
        }
        customerNumberContainers.add(customerNumberContainer);
    }

    public Address deepCopy() {
        final Address copiedAddress = new Address();
        copiedAddress.setFlfId(this.flfId);
        copiedAddress.setFlfIdChanged(this.isFlfIdChanged());
        copiedAddress.setAdditionalInformation(this.getAdditionalInformation());
        copiedAddress.setAdditionalInformationChanged(this.isAdditionalInformationChanged());
        if (this.getCustomerNumberContainers() != null && !this.getCustomerNumberContainers().isEmpty()) {
            final List<CustomerNumberContainer> newCustomerNumberContainers = new ArrayList<>();
            for (final CustomerNumberContainer customerNumberContainer : this.getCustomerNumberContainers()) {
                final CustomerNumberContainer newCustomerNumberContainer = new CustomerNumberContainer();
                newCustomerNumberContainer.setContactTenant(customerNumberContainer.getContactTenant());
                newCustomerNumberContainer.setCustomerNumber(customerNumberContainer.getCustomerNumber());
                newCustomerNumberContainers.add(newCustomerNumberContainer);
            }
            copiedAddress.setCustomerNumberContainers(newCustomerNumberContainers);
            copiedAddress.setCustomerNumberContainersChanged(this.isCustomerNumberContainersChanged());
        }
        copiedAddress.setStreet(this.getStreet());
        copiedAddress.setStreetChanged(this.isStreetChanged());
        copiedAddress.setIntSign(this.getIntSign());
        copiedAddress.setIntSignChanged(this.isIntSignChanged());
        copiedAddress.setPostalCode(this.getPostalCode());
        copiedAddress.setPostalCodeChanged(this.isPostalCodeChanged());
        copiedAddress.setRegion(this.getRegion());
        copiedAddress.setRegionChanged(this.isRegionChanged());
        copiedAddress.setProvinceType(this.getProvinceType());
        copiedAddress.setProvinceTypeChanged(this.isProvinceTypeChanged());
        copiedAddress.setCountry(this.getCountry());
        copiedAddress.setCountryChanged(this.isCountryChanged());
        copiedAddress.setTelephone(this.getTelephone());
        copiedAddress.setTelephoneChanged(this.isTelephoneChanged());
        copiedAddress.setEmail(this.getEmail());
        copiedAddress.setEmailChanged(this.isEmailChanged());
        copiedAddress.setInformation(this.getInformation());
        copiedAddress.setInformationChanged(this.isInformationChanged());
        copiedAddress.setGeolocation(this.geolocation);

        if (this.getUserIdBlackList() != null && !this.getUserIdBlackList().isEmpty()) {
            final List<String> copiedUserIdBlackList = new ArrayList<>();
            for (final String userId : this.getUserIdBlackList()) {
                copiedUserIdBlackList.add(userId);
            }
            copiedAddress.setUserIdBlackList(copiedUserIdBlackList);
        }

        if (this.getRolesBlackList() != null && !this.getRolesBlackList().isEmpty()) {
            final List<ProjectUserConnectionRoleEnum> copiedRolesBlackList = new ArrayList<>();
            for (final ProjectUserConnectionRoleEnum roleBlackList : this.getRolesBlackList()) {
                copiedRolesBlackList.add(roleBlackList);
            }
            copiedAddress.setRolesBlackList(copiedRolesBlackList);
        }
        return copiedAddress;
    }
}
