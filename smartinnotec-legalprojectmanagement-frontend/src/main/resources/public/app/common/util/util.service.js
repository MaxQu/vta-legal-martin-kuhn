(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('utilService', utilService);
    
	utilService.$inject = ['$http', '$q', 'api_config'];
	
	function utilService($http, $q, api_config) {
		var service = {		
			isImage: isImage,
			replaceAll: replaceAll,
			prepareSearchTerm: prepareSearchTerm
		};
		
		return service;
		
		////////////
		
		function isImage(ending) {
			if(ending == 'jpg' || ending == 'JPG' || ending == 'png' || ending == 'PNG' || ending == 'gif' || ending == 'GIF' || ending == 'gif') {
				return true;
			}
			return false;
		}
		
		function replaceAll(str, find, replace) {
		    return str.replace(new RegExp(find, 'g'), replace);
		}
		
		function prepareSearchTerm(searchTerm) {
			if(searchTerm.indexOf('Rohstoff') > -1) {
				searchTerm = searchTerm.replace('Rohstoff', 'RAW_MATERIAL');
			}
			if(searchTerm.indexOf('rohstoff') > -1) {
				searchTerm = searchTerm.replace('rohstoff', 'RAW_MATERIAL');
			}
			if(searchTerm == 'Reine' || searchTerm == 'Handelsware') {
				searchTerm = searchTerm.replace('Reine', 'TRADING_GOODS');
				searchTerm = searchTerm.replace('Handelsware', 'TRADING_GOODS');
			}
			if(searchTerm == 'reine' || searchTerm == 'handelsware') {
				searchTerm = searchTerm.replace('reine', 'TRADING_GOODS');
				searchTerm = searchTerm.replace('handelsware', 'TRADING_GOODS');
			}
			if(searchTerm == 'Labor') {
				searchTerm = searchTerm.replace('Labor', 'LAB_CHEMICAL');
			} else if(searchTerm == 'labor') {
				searchTerm = searchTerm.replace('labor', 'LAB_CHEMICAL');
			} else if(searchTerm == 'chemikalie') {
				searchTerm = searchTerm.replace('chemikalie', 'LAB_CHEMICAL');
			} else if(searchTerm == 'Laborchemikalie') {
				searchTerm = searchTerm.replace('Laborchemikalie', 'LAB_CHEMICAL');
			} else if(searchTerm == 'laborchemikalie') {
				searchTerm = searchTerm.replace('laborchemikalie', 'LAB_CHEMICAL');
			}
			var preparedSearchTerm = searchTerm.replace('/', '-slash-');
			return preparedSearchTerm;
		}
	}
})();