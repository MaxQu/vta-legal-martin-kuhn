package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "AccessControlEntry")
public @Data class AccessControlEntry {

    @Id
    private String id;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime eventDateTime;
    private String operationDescription;
    private String userName;
    private String userExtID;
    private String doorName;

}
