package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class CalendarEventUserConnectionServiceTest extends AbstractServiceTest {

    @Test
    public void shouldGetCalendarEventUserConnectionWithLastCalendarEvent() throws BusinessException {

        final List<CalendarEventUserConnection> calendarEventUserConnections = new ArrayList<>();

        // calendarEventUserConnection 1
        final CalendarEventUserConnection calendarEventUserConnection1 = new CalendarEventUserConnection();
        final CalendarEvent calendarEvent1 = new CalendarEvent();
        final DateTime startsAt1 = formatterDate.parseDateTime("22.04.2020");
        calendarEvent1.setEndsAt(startsAt1.getMillis());
        calendarEvent1.setStartsAt(startsAt1.getMillis());
        calendarEventUserConnection1.setCalendarEvent(calendarEvent1);
        calendarEventUserConnections.add(calendarEventUserConnection1);

        // calendarEventUserConnection 2
        final CalendarEventUserConnection calendarEventUserConnection2 = new CalendarEventUserConnection();
        final CalendarEvent calendarEvent2 = new CalendarEvent();
        final DateTime startsAt2 = formatterDate.parseDateTime("28.04.2020");
        calendarEvent2.setEndsAt(startsAt2.getMillis());
        calendarEvent2.setStartsAt(startsAt2.getMillis());
        calendarEventUserConnection2.setCalendarEvent(calendarEvent2);
        calendarEventUserConnections.add(calendarEventUserConnection2);

        // calendarEventUserConnection 3
        final CalendarEventUserConnection calendarEventUserConnection3 = new CalendarEventUserConnection();
        final CalendarEvent calendarEvent3 = new CalendarEvent();
        final DateTime startsAt3 = formatterDate.parseDateTime("01.04.2020");
        calendarEvent3.setEndsAt(startsAt3.getMillis());
        calendarEvent3.setStartsAt(startsAt3.getMillis());
        calendarEventUserConnection3.setCalendarEvent(calendarEvent3);
        calendarEventUserConnections.add(calendarEventUserConnection3);

        // calendarEventUserConnection 3
        final CalendarEventUserConnection calendarEventUserConnection4 = new CalendarEventUserConnection();
        final CalendarEvent calendarEvent4 = new CalendarEvent();
        final DateTime startsAt4 = formatterDate.parseDateTime("07.04.2020");
        calendarEvent4.setEndsAt(startsAt4.getMillis());
        calendarEvent4.setStartsAt(startsAt4.getMillis());
        calendarEventUserConnection4.setCalendarEvent(calendarEvent4);
        calendarEventUserConnections.add(calendarEventUserConnection4);

        
    }
}
