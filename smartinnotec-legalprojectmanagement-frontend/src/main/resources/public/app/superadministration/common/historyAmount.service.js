(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.superadministration')
    	.factory('historyAmountService', historyAmountService);
        
    historyAmountService.$inject = ['$http', 'api_config'];
    
    function historyAmountService($http, api_config) {
		var service = {
			getHistoryAmountsBetweenDates: getHistoryAmountsBetweenDates
		};
		
		return service;
		
		////////////
		
		function getHistoryAmountsBetweenDates(start, end) {
			return $http.get(api_config.BASE_URL + '/historyamounts/historyamount/' + start + '/' + end + '/');
		}
    }
})();


