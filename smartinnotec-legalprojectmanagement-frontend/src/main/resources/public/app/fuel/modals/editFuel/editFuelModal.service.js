(function() {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .factory('editFuelModalService', editFuelModalService);

    editFuelModalService.$inject = ['$modal', '$stateParams'];

    function editFuelModalService($modal, $stateParams) {
        var service = {
            showEditFuelModal: showEditFuelModal
        };
        return service;

        ////////////

        function showEditFuelModal(fuel, invoker, currentUser, roles, usersOfProject) {
            var editFuelModal = $modal.open({
                controller: EditFuelModalController,
                controllerAs: 'vm',
                size: 'lg',
                windowClass: "modal fade in",
                resolve: {
                    fuel: function() {
                        return fuel;
                    },
                    invoker: function() {
                        return invoker;
                    },
                    currentUser: function() {
                        return currentUser;
                    },
                    roles: function() {
                        return roles;
                    },
                    usersOfProject: function() {
                        return usersOfProject;
                    }
                },
                templateUrl: 'app/fuel/modals/editFuel/editFuel.modal.html'
            });
            return editFuelModal;

            function EditFuelModalController($modalInstance, $scope, fuel, invoker, currentUser, roles, usersOfProject) {
                var vm = this;
                vm.fuel = fuel;
                vm.invoker = invoker;
                vm.currentUser = currentUser;
                vm.roles = roles;
                vm.usersOfProject = usersOfProject;

                vm.closeEditFuelModal = closeEditFuelModal;

                ////////////

                function closeEditFuelModal() {
                    $modalInstance.dismiss('cancel');
                }
            }
        }
    }
})();
