package com.smartinnotec.legalprojectmanagement.restservice.container;

import java.io.PrintWriter;
import java.util.List;

import org.springframework.stereotype.Component;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.RecipeContainer;

@Component
public class SystemReceipeExportCsvResponse {

    public void writeReceipes(final PrintWriter writer, final List<RecipeContainer> receipeContainers) throws BusinessException {
        try {
            final ColumnPositionMappingStrategy<RecipeContainer> mapStrategy = new ColumnPositionMappingStrategy<RecipeContainer>();
            mapStrategy.setType(RecipeContainer.class);
            mapStrategy.generateHeader();
            final String[] columns = new String[] {
                "date", "productName", "recipeProducts", "information", "amountInKilo", "recipeType", "creationDate" };
            mapStrategy.setColumnMapping(columns);
            writer.append("date, productname, recipeProducts, information, amountInKilo, recipeType, creationDate \n");
            final StatefulBeanToCsv<RecipeContainer> btcsv = new StatefulBeanToCsvBuilder<RecipeContainer>(writer)
                .withQuotechar(CSVWriter.DEFAULT_QUOTE_CHARACTER).withMappingStrategy(mapStrategy).withSeparator(',').build();
            btcsv.write(receipeContainers);
        } catch (final Exception e) {
            throw new BusinessException(e.getMessage(), "400");
        }
    }
}
