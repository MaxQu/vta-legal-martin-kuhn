package com.smartinnotec.legalprojectmanagement.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.service.ContactPreviousImportedService;

@RestController
public class ContactPreviousImportedRestService extends AbstractRestService {

    @Autowired
    private ContactPreviousImportedService contactPreviousImportedService;

    @RequestMapping(value = "/contactspreviousimported/contactpreviousimported/amount", method = {
        RequestMethod.GET }, produces = "application/json")
    public Integer getAmountOfContactsPreviousImported() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": get amount of products in ContactPreviousImportedRestService#getAmountOfContactsPreviousImported");
        return contactPreviousImportedService.count();
    }

    @Override
    public String toString() {
        return "[ContactPreviousImportedRestService]";
    }
}
