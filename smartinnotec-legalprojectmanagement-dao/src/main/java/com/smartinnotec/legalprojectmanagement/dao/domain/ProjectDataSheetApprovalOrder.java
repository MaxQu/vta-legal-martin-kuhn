package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

public @Data class ProjectDataSheetApprovalOrder {

    private String approvalOrderAgency;
    private String approvalOrderNumber;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime approvalOrderDate;

}
