(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();