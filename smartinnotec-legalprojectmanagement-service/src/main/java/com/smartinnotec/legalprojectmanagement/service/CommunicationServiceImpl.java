package com.smartinnotec.legalprojectmanagement.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Communication;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationAttachedDocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileVersion;
import com.smartinnotec.legalprojectmanagement.dao.repository.CommunicationRepository;

@Service("messageService")
public class CommunicationServiceImpl extends AbstractService implements CommunicationService {

    @Autowired
    protected CommunicationRepository messageRepository;
    @Autowired
    private FileUploadService fileUploadService;
    @Autowired
    private DocumentFileService documentFileService;

    public CommunicationServiceImpl() {
    }

    @Override
    public Communication findCommunication(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in MessageServiceImpl#findMessage", "400");
        logger.debug("get message by id '{}' in MessageServiceImpl#findMessage", id);
        return messageRepository.findOne(id);
    }

    @Override
    public List<Communication> findByAttachedFileUrlsNotNull() throws BusinessException {
        logger.info("find communications where attachments not null in CommunicationServiceImpl#findByCommunicationAttachmentsNotNull");
        return messageRepository.findByAttachedFileUrlsNotNull();
    }

    @Override
    public Communication createCommunication(final Communication message) throws BusinessException {
        BusinessAssert.notNull(message, "message is mandatory in MessageServiceImpl#createMessage", "400");
        logger.debug("create message in MessageServiceImpl#createMessage");
        return messageRepository.insert(message);
    }

    @Override
    public Communication updateCommunication(final Communication message) throws BusinessException {
        BusinessAssert.notNull(message, "message is mandatory in MessageServiceImpl#updateMessage", "400");
        logger.debug("update message with id '{}' in MessageServiceImpl#updateMessage", message.getId());
        return messageRepository.save(message);
    }

    @Override
    public void deleteCommunication(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in MessageServiceImpl#deleteMessage", "400");
        logger.debug("delete message with id '{}' in MessageServiceImpl#deleteMessage", id);
        messageRepository.delete(id);
    }

    @Override
    public List<CommunicationAttachedDocumentFile> createAttachmentFileUrls(final String userIdSender,
            final List<CommunicationSelectedDocumentFile> communicationSelectedDocumentFiles)
            throws BusinessException {
        if (communicationSelectedDocumentFiles == null) {
            return new ArrayList<>();
        }
        logger.info("create attachment file urls in CommunicationServiceImpl#createAttachmentFileUrls");
        final List<CommunicationAttachedDocumentFile> attachmentDocumentFileUrls = new ArrayList<>();
        for (final CommunicationSelectedDocumentFile communicationSelectedDocumentFile : communicationSelectedDocumentFiles) {
            final String fileName = communicationSelectedDocumentFile.getSelectedDocumentFile().getFileName();
            final String projectId = communicationSelectedDocumentFile.getSelectedDocumentFile().getProjectProductId();
            final String documentFileId = communicationSelectedDocumentFile.getSelectedDocumentFile().getId();
            final String version = communicationSelectedDocumentFile.getDocumentFileVersion().getVersion();
            final StringBuilder documentFileUrlSb = new StringBuilder();
            documentFileUrlSb.append("api/filedownloads/filedownload/").append(userIdSender).append("/").append(documentFileId).append("/")
                .append(version).append("/");
            String storedFilePath = "";
            final DocumentFile documentFile = documentFileService.findById(documentFileId);
            for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
                if (documentFileVersion.getVersion().equals(version)) {
                    storedFilePath = documentFile.getSubFilePath() + File.separator + documentFileVersion.getFilePathName();
                    break;
                }
            }
            final CommunicationAttachedDocumentFile communicationAttachedDocumentFile = new CommunicationAttachedDocumentFile();
            communicationAttachedDocumentFile.setProjectId(projectId);
            communicationAttachedDocumentFile.setDocumentFileId(communicationSelectedDocumentFile.getSelectedDocumentFile().getId());
            communicationAttachedDocumentFile.setFileName(fileName);
            communicationAttachedDocumentFile.setUrl(documentFileUrlSb.toString());
            communicationAttachedDocumentFile.setVersion(version);
            communicationAttachedDocumentFile.setImage(fileUploadService.isImage(new File(storedFilePath)));

            attachmentDocumentFileUrls.add(communicationAttachedDocumentFile);
        }
        return attachmentDocumentFileUrls;
    }

    @Override
    public String toString() {
        return "[CommunicationServiceImpl]";
    }
}
