package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.SearchHistory;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationStateEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationTypeEnum;
import com.smartinnotec.legalprojectmanagement.service.ApplicationCategorieEnum;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.SearchHistoryService;
import com.smartinnotec.legalprojectmanagement.service.SearchService;
import com.smartinnotec.legalprojectmanagement.service.UserAuthorizationUserContainerService;
import com.smartinnotec.legalprojectmanagement.service.UserService;
import com.smartinnotec.legalprojectmanagement.service.container.SearchResult;

@RestController
public class SearchRestService extends AbstractRestService {

    @Autowired
    private UserService userService;
    @Autowired
    private SearchService searchService;
    @Autowired
    private UserAuthorizationUserContainerService userAuthorizationUserContainerService;
    @Autowired
    private SearchHistoryService searchHistoryService;
    @Autowired
    private HistoryService historyService;

    @RequestMapping(value = "/search/{searchstring}/{lastsearchresultlength}", method = {
        RequestMethod.PUT }, produces = "application/json")
    public List<SearchResult> search(@PathVariable("searchstring") String searchString,
            final @PathVariable("lastsearchresultlength") Integer lastSearchResultLength,
            final @RequestBody List<ApplicationCategorieEnum> applicationCategories) throws BusinessException {
        logger.info(
            getInvokedRestServiceUserInformation() + ": search issues by searchString '{}' and categories '{}' in SearchRestService#search",
            searchString, applicationCategories);

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        searchString = searchService.prepareSearchTerm(searchString);

        final List<SearchResult> searchResults = new ArrayList<>();
        if (applicationCategories == null || applicationCategories.isEmpty()) {
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.PROJECT,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> projectsSearchResult = searchService.searchInProjects(searchString, user);
                searchResults.addAll(projectsSearchResult);
            }
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.PROJECT,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> documentFilesSearchResult = searchService.searchInDocumentFiles(searchString, user);
                searchResults.addAll(documentFilesSearchResult);
            }
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.PROJECT,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> foldersSearchResult = searchService.searchInProjectFolders(searchString, user);
                searchResults.addAll(foldersSearchResult);
            }
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.PRODUCT,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> documentFilesSearchResult = searchService.searchDocumentFilesInProducts(searchString, user);
                searchResults.addAll(documentFilesSearchResult);
            }
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.PRODUCT,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> productsSearchResult = searchService.searchInProducts(searchString, user);
                searchResults.addAll(productsSearchResult);

                final List<SearchResult> foldersSearchResult = searchService.searchInProductFolders(searchString, user);
                searchResults.addAll(foldersSearchResult);
            }

            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.CALENDAR,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> calendarSearchResult = searchService.searchInCalendar(searchString, user);
                searchResults.addAll(calendarSearchResult);
            }
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.CONTACT,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> contactSearchResult = searchService.searchInContacts(searchString, user);
                searchResults.addAll(contactSearchResult);
            }
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.WORKING_BOOK,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> workingBookSearchResult = searchService.searchInWorkingBook(searchString, user);
                searchResults.addAll(workingBookSearchResult);
            }
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.COMMUNICATION,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> communicationSearchResult = searchService.searchInCommunication(searchString, user);
                searchResults.addAll(communicationSearchResult);
            }
            if (userAuthorizationUserContainerService.isUserAuthorized(user, UserAuthorizationTypeEnum.TIME_REGISTRATION,
                UserAuthorizationStateEnum.READ)) {
                final List<SearchResult> timeRegistrationSearchResult = searchService.searchInTimeRegistration(searchString, user);
                searchResults.addAll(timeRegistrationSearchResult);
            }
        } else {
            for (final ApplicationCategorieEnum applicationCategorie : applicationCategories) {
                switch (applicationCategorie) {
                case PROJECTS:
                    final List<SearchResult> projectsSearchResult = searchService.searchInProjects(searchString, user);
                    searchResults.addAll(projectsSearchResult);
                    break;
                case PRODUCTS:
                    final List<SearchResult> productsSearchResult = searchService.searchInProducts(searchString, user);
                    searchResults.addAll(productsSearchResult);
                    break;
                case DOCUMENTS:
                    final List<SearchResult> documentFilesSearchResult = searchService.searchInDocumentFiles(searchString, user);
                    searchResults.addAll(documentFilesSearchResult);
                    final List<SearchResult> documentFilesProductSearchResult = searchService.searchDocumentFilesInProducts(searchString,
                        user);
                    searchResults.addAll(documentFilesProductSearchResult);
                    break;
                case CALENDAR:
                    final List<SearchResult> calendarSearchResult = searchService.searchInCalendar(searchString, user);
                    searchResults.addAll(calendarSearchResult);
                    break;
                case CONTACTS:
                    final List<SearchResult> contactSearchResult = searchService.searchInContacts(searchString, user);
                    searchResults.addAll(contactSearchResult);
                    break;
                case WORKINGBOOK:
                    final List<SearchResult> workingBookSearchResult = searchService.searchInWorkingBook(searchString, user);
                    searchResults.addAll(workingBookSearchResult);
                    break;
                case COMMUNICATION:
                    final List<SearchResult> communicationSearchResult = searchService.searchInCommunication(searchString, user);
                    searchResults.addAll(communicationSearchResult);
                    break;
                case TIME_REGISTRATION:
                    final List<SearchResult> timeRegistrationSearchResult = searchService.searchInTimeRegistration(searchString, user);
                    searchResults.addAll(timeRegistrationSearchResult);
                    break;
                case FOLDERS:
                    break;
                case PRODUCT_FOLDERS:
                    break;
                default:
                    break;
                }
            }
        }
        logger.info("found '{}' searchResults in SearchRestService#search", searchResults.size());

        if (searchResults.size() > 0 && lastSearchResultLength != searchResults.size()) {
            final SearchHistory searchHistory = new SearchHistory();
            searchHistory.setUserId(user.getId());
            searchHistory.setSearchString(searchString);
            searchHistory.setSearchDate(new DateTime(System.currentTimeMillis()));
            searchHistory.setAmountOfResults(searchResults.size());
            final SearchHistory createdSearchHistory = searchHistoryService.createOrUpdateSearchHistory(searchHistory);
            logger.info("created search history with id '{}' in SearchRestService#search", createdSearchHistory.getId());
        }
        
        final String message = searchString + " (" + (searchResults != null ? searchResults.size() : 0) + ")";
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.GENERAL_SEARCH)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in SearchRestService#search", createdHistory.getId());

        return searchResults;
    }

    @RequestMapping(value = "/search/searchhistory/{userid}", method = { RequestMethod.GET }, produces = "application/json")
    public List<SearchHistory> getSearchHistory(final @PathVariable("userid") String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in SearchRestService#getSearchHistory", "400");
        BusinessAssert.isId(userId, "userId must be an id in SearchRestService#getSearchHistory", "400");
        logger.info(
            getInvokedRestServiceUserInformation() + ": get search history for user with id '{}' in SearchRestService#getSearchHistory",
            userId);
        final List<SearchHistory> searchHistories = searchHistoryService.findPagedSearchHistoryOrderBySearchDateDesc(userId);
        return searchHistories;
    }
}
