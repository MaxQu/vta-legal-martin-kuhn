(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('deleteImportedContactsModalService', deleteImportedContactsModalService);
        
    deleteImportedContactsModalService.$inject = ['$modal', '$stateParams', '$timeout', 'contactsService'];
    
    function deleteImportedContactsModalService($modal, $stateParams, $timeout, contactsService) {
		var service = {
			showDeleteImportedContactsModal: showDeleteImportedContactsModal
		};		
		return service;
		
		////////////
				
		function showDeleteImportedContactsModal(invoker, contactImportationDateGroups) {
			 var deleteImportedContactsModal = $modal.open({
				controller: DeleteImportedContactsModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: { 
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contactImportationDateGroups: function() {
			    		return contactImportationDateGroups;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/deleteImportedContacts/deleteImportedContacts.modal.html'
			});
			return deleteImportedContactsModal;
			
			function DeleteImportedContactsModalController($modalInstance, $scope, invoker, contactImportationDateGroups) {
		    	var vm = this;
		    	vm.invoker = invoker;
		    	vm.contactImportationDateGroups = contactImportationDateGroups;
		    	vm.selectedContactImportationDateGroup = null;
		    	vm.contactImportationDateGroupsTrigger = false;
		    	vm.contactImportationDateGroupNotSelectedError = false;
		    	
		    	vm.deleteImportedContactsOfBulk = deleteImportedContactsOfBulk;
		    	vm.closeDeleteImportedContactsModal = closeDeleteImportedContactsModal;
		    	
		    	////////////

		    	function setTimeout() {
		      		$timeout(function() {
		      			vm.contactImportationDateGroupNotSelectedError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function deleteImportedContactsOfBulk() {
		    		if(vm.selectedContactImportationDateGroup == null) {
		    			vm.contactImportationDateGroupNotSelectedError = true;
		    			setTimeout();
		    			return;
		    		}
		    		vm.contactImportationDateGroupsTrigger = true;
		    		contactsService.deleteImportedContactsOfBulk(vm.selectedContactImportationDateGroup.importationDate).then(function(response) {
		    			vm.invoker.pageChange(0);
		    			vm.contactImportationDateGroupsTrigger = false;
		    			vm.closeDeleteImportedContactsModal();
					}).catch(function (data) {
						console.log('error in contacts.controller.js#deleteImportedContacts: ' + data);
					});
		    	}
		    	
		    	function closeDeleteImportedContactsModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();