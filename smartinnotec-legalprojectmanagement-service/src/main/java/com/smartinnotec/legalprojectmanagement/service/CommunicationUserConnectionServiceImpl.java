package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Communication;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.CommunicationUserConnectionRepository;

@Service("communicationUserConnectionService")
public class CommunicationUserConnectionServiceImpl extends AbstractService implements CommunicationUserConnectionService {

    private static final int MESSAGE_PAGE_SIZE;

    static {
        MESSAGE_PAGE_SIZE = 80;
    }

    @Autowired(required = true)
    protected CommunicationUserConnectionRepository communicationUserConnectionRepository;

    public CommunicationUserConnectionServiceImpl() {
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByProject(final Project project) throws BusinessException {
        BusinessAssert.notNull(project,
            "project is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByProject", "400");
        logger.debug(
            "find communicationUserConnections of project with id '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByProject",
            project.getId());
        return communicationUserConnectionRepository.findCommunicationUserConnectionByProject(project);
    }

    @Override
    public CommunicationUserConnection findCommunicationUserConnection(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnection", "400");
        logger.debug(
            "get communicationUserConnection with id '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnection", id);
        return communicationUserConnectionRepository.findOne(id);
    }

    @Override
    public CommunicationUserConnection createCommunicationUserConnection(final CommunicationUserConnection messageUserConnection,
            final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(messageUserConnection,
            "messageUserConnection is mandatory in CommunicationUserConnectionServiceImpl#createCommunicationUserConnection", "400");
        logger.debug("create CommunicationUserConnection in CommunicationUserConnectionServiceImpl#createCommunicationUserConnection");
        messageUserConnection.setTenant(tenant);
        return communicationUserConnectionRepository.insert(messageUserConnection);
    }

    @Override
    public CommunicationUserConnection updateCommunicationUserConnection(final CommunicationUserConnection communicationUserConnection)
            throws BusinessException {
        BusinessAssert.notNull(communicationUserConnection,
            "communicationUserConnection is mandatory in CommunicationUserConnectionServiceImpl#updateCommunicationUserConnection", "400");
        BusinessAssert.notNull(communicationUserConnection.getId(),
            "communicationUserConnection#id is mandatory in CommunicationUserConnectionServiceImpl#updateCommunicationUserConnection",
            "400");
        logger.debug(
            "update CommunicationUserConnection with id '{}' in CommunicationUserConnectionServiceImpl#updateCommunicationUserConnection",
            communicationUserConnection.getId());
        return communicationUserConnectionRepository.save(communicationUserConnection);
    }

    @Override
    public CommunicationUserConnection archiveCommunicationUserConnection(final CommunicationUserConnection messageUserConnection)
            throws BusinessException {
        messageUserConnection.setArchived(true);
        logger.debug(
            "archive communicationUserConnection with id '{}' in CommunicationUserConnectionServiceImpl#archiveCommunicationUserConnection",
            messageUserConnection.getId());
        return communicationUserConnectionRepository.save(messageUserConnection);
    }

    @Override
    public void deleteCommunicationUserConnection(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in CommunicationUserConnectionServiceImpl#deleteCommunicationUserConnection");
        logger.debug(
            "delete communicationUserConnection with id '{}' in CommunicationUserConnectionServiceImpl#deleteCommunicationUserConnection",
            id);
        communicationUserConnectionRepository.delete(id);
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived(
            final User userCreatedMessage, final User userReceivedMessage, final Tenant tenant, final boolean archived)
            throws BusinessException {
        BusinessAssert.notNull(userCreatedMessage,
            "userCreatedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived",
            "400");
        BusinessAssert.notNull(userReceivedMessage,
            "userReceivedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived",
            "400");
        logger.debug(
            "find communicationUserConnection by user created messages with id '{}' in  CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived",
            userCreatedMessage.getId());
        if (tenant == null) {
            return new ArrayList<>();
        }
        return communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived(userCreatedMessage,
                userCreatedMessage, tenant, archived);
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageNotAndUserReceivedMessageAndTenantAndArchived(
            User userCreatedMessage, User userReceivedMessage, Tenant tenant, boolean archived) throws BusinessException {
        BusinessAssert.notNull(userCreatedMessage,
            "userCreatedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageNotAndUserReceivedMessageAndTenantAndArchived",
            "400");
        BusinessAssert.notNull(userReceivedMessage,
            "userReceivedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageNotAndUserReceivedMessageAndTenantAndArchived",
            "400");
        logger.debug(
            "find communicationUserConnection by user created messages with id '{}' in  CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageNotAndUserReceivedMessageAndTenantAndArchived",
            userCreatedMessage.getId());
        if (tenant == null) {
            return new ArrayList<>();
        }
        final List<CommunicationUserConnection> communicationUserConnection = communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserCreatedMessageNotAndUserReceivedMessageAndTenantAndArchived(userCreatedMessage,
                userCreatedMessage, tenant, archived);
        return communicationUserConnection;
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessage(final User userReceivedMessage)
            throws BusinessException {
        BusinessAssert.notNull(userReceivedMessage,
            "userReceivedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessage",
            "400");
        logger.debug(
            "find communicationUserConnection by user received messages with id '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessage",
            userReceivedMessage.getId());
        return communicationUserConnectionRepository.findCommunicationUserConnectionByUserReceivedMessage(userReceivedMessage);
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndArchived(final User userReceivedMessage,
            final boolean archived, final Integer page) throws BusinessException {
        BusinessAssert.notNull(userReceivedMessage,
            "userReceivedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndArchived",
            "400");
        logger.debug(
            "find communicationUserConnection by user received messages with id '{}' and archived flag is '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndArchived",
            userReceivedMessage.getId(), archived);

        final List<CommunicationUserConnection> messateUserConnections = new ArrayList<>();
        for (int i = 0; i <= page; i++) {
            final Pageable pageable = new PageRequest(i, MESSAGE_PAGE_SIZE);
            final List<CommunicationUserConnection> messageUserConnectionsOfPage = communicationUserConnectionRepository
                .findByUserReceivedMessageAndArchivedOrderBySortDateAsc(userReceivedMessage, archived, pageable);
            messateUserConnections.addAll(messageUserConnectionsOfPage);
        }
        return messateUserConnections;
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionsByCommunication(final Communication message)
            throws BusinessException {
        BusinessAssert.notNull(message,
            "message is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionsByCommunication", "400");
        logger.debug(
            "find communicationUserConnection by message with id '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionsByCommunication",
            message.getId());

        return communicationUserConnectionRepository.findCommunicationUserConnectionsByCommunication(message);
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(
            final User userReceivedMessage, final Boolean confirmationNeeded, final Boolean read, final Boolean executed)
            throws BusinessException {
        BusinessAssert.notNull(userReceivedMessage,
            "userReceivedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            "400");
        BusinessAssert.notNull(confirmationNeeded,
            "confirmationNeeded is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            "400");
        BusinessAssert.notNull(read,
            "read is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            "400");
        BusinessAssert.notNull(executed,
            "executed is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            "400");
        logger.info(
            "find communicationUserConnection by user received message with id '{}', confirmation needed '{}', read '{}' and executed '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            userReceivedMessage.getId(), confirmationNeeded, read, executed);
        return communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(
                userReceivedMessage, confirmationNeeded, read, executed);
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(
            final User userCreatedMessage, final Boolean confirmationNeeded, final Boolean read, final Boolean executed)
            throws BusinessException {
        BusinessAssert.notNull(userCreatedMessage,
            "userCreatedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            "400");
        BusinessAssert.notNull(confirmationNeeded,
            "confirmationNeeded is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            "400");
        BusinessAssert.notNull(read,
            "read is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            "400");
        BusinessAssert.notNull(executed,
            "executed is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            "400");
        logger.info(
            "find communicationUserConnection by user created message with id '{}', confirmation needed '{}', read '{}' and executed '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted",
            userCreatedMessage.getId(), confirmationNeeded, read, executed);
        final List<CommunicationUserConnection> communicationUserConnection = communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(
                userCreatedMessage, confirmationNeeded, read, executed);
        return communicationUserConnection;
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndExecutionDateNotNullAndExecuted(
            final User userReceivedMessage, final Boolean executed) throws BusinessException {
        BusinessAssert.notNull(userReceivedMessage,
            "userReceivedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndExecutionDateNotNullAndExecuted",
            "400");
        BusinessAssert.notNull(executed,
            "executed is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndExecutionDateNotNullAndExecuted",
            "400");
        logger.info(
            "find communicationUserConnection by user received message with id '{}' and execution date not null and executed is '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndExecutionDateNotNullAndExecuted",
            userReceivedMessage.getId(), executed);
        final List<CommunicationUserConnection> communicationUserConnections = communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserReceivedMessageAndExecutionDateNotNullAndExecutedAndArchived(userReceivedMessage,
                executed, false);
        communicationUserConnections.removeIf(s -> s.getUserCreatedMessage().getId().equals(s.getUserReceivedMessage().getId()));
        return communicationUserConnections;
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessage(final User userCreatedMessage)
            throws BusinessException {
        BusinessAssert.notNull(userCreatedMessage,
            "userCreatedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessage",
            "400");
        logger.info(
            "find communicationUserConnection by user created message with id '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessage",
            userCreatedMessage.getId());
        return communicationUserConnectionRepository.findCommunicationUserConnectionByUserCreatedMessage(userCreatedMessage);
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndExecutionDateNotNullAndExecuted(
            final User userCreatedMessage, Boolean executed) throws BusinessException {
        BusinessAssert.notNull(userCreatedMessage,
            "userCreatedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndExecutionDateNotNullAndExecuted",
            "400");
        BusinessAssert.notNull(executed,
            "executed is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndExecutionDateNotNullAndExecuted",
            "400");
        logger.info(
            "find communicationUserConnection by user received message with id '{}' and execution date not null and executed is '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndExecutionDateNotNullAndExecuted",
            userCreatedMessage.getId(), executed);
        final List<CommunicationUserConnection> communicationUserConnections = communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserCreatedMessageAndExecutionDateNotNullAndExecutedAndArchived(userCreatedMessage, executed,
                false);
        communicationUserConnections.removeIf(s -> s.getUserCreatedMessage().getId().equals(s.getUserReceivedMessage().getId()));
        return communicationUserConnections;
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween(
            final User userCreatedMessage, final DateTime start, final DateTime end) throws BusinessException {
        BusinessAssert.notNull(userCreatedMessage,
            "userCreatedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween",
            "400");
        BusinessAssert.notNull(start,
            "start is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween",
            "400");
        BusinessAssert.notNull(end,
            "end is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween",
            "400");
        logger.info(
            "find communicationUserConnection by userCreatedMessage with id '{}', start '{}' and end '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween",
            userCreatedMessage.getId(), start, end);
        final List<CommunicationUserConnection> communicationUserConnections = communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween(userCreatedMessage, start, end);
        communicationUserConnections.removeIf(
            s -> isCreatedOrReceivedUserNotSet(s) || s.getUserCreatedMessage().getId().equals(s.getUserReceivedMessage().getId()));
        return communicationUserConnections;
    }

    @Override
    public boolean isCreatedOrReceivedUserNotSet(final CommunicationUserConnection communicationUserConnection) {
        return communicationUserConnection.getUserCreatedMessage() == null || communicationUserConnection.getUserReceivedMessage() == null;
    }

    @Override
    public List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween(
            final User userReceivedMessage, final DateTime start, final DateTime end) throws BusinessException {
        BusinessAssert.notNull(userReceivedMessage,
            "userReceivedMessage is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween",
            "400");
        BusinessAssert.notNull(start,
            "start is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween",
            "400");
        BusinessAssert.notNull(end,
            "end is mandatory in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween",
            "400");
        logger.info(
            "find communicationUserConnection by userReceivedMessage with id '{}', start '{}' and end '{}' in CommunicationUserConnectionServiceImpl#findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween",
            userReceivedMessage.getId(), start, end);
        final List<CommunicationUserConnection> communicationUserConnections = communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween(userReceivedMessage, start, end);
        communicationUserConnections.removeIf(s -> s.getUserCreatedMessage().getId().equals(s.getUserReceivedMessage().getId()));
        return communicationUserConnections;
    }

    @Override
	public Integer count() throws BusinessException {
		logger.info("count communicationUserConnection in CommunicationUserConnectionServiceImpl#count");
		final Long communicationUserConnections = communicationUserConnectionRepository.count();
		return communicationUserConnections.intValue();
	}
    
    @Override
    public String toString() {
        return "[CommunicationUserConnectionServiceImpl]";
    }
}
