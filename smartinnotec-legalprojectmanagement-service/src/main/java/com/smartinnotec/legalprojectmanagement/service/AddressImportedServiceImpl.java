package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.repository.AddressImportedRepository;

@Service("addressImportedService")
public class AddressImportedServiceImpl extends AbstractService implements AddressImportedService {

    @Autowired
    private AddressImportedRepository addressImportedRepository;

    @Override
    public boolean hasAddressImportedChanged(AddressImported address) throws BusinessException {
        logger.info("has addressImported changed in AddressImportedServiceImpl#hasAddressImportedChanged");
        if (address != null) {
            if (address.isFlfIdChanged()) {
                return true;
            }
            if (address.isAdditionalInformationChanged()) {
                return true;
            }
            if (address.isCustomerNumberContainersChanged()) {
                return true;
            }
            if (address.isStreetChanged()) {
                return true;
            }
            if (address.isIntSignChanged()) {
                return true;
            }
            if (address.isPostalCodeChanged()) {
                return true;
            }
            if (address.isRegionChanged()) {
                return true;
            }
            if (address.isProvinceTypeChanged()) {
                return true;
            }
            if (address.isCountryChanged()) {
                return true;
            }
            if (address.isTelephoneChanged()) {
                return true;
            }
            if (address.isEmailChanged()) {
                return true;
            }
            if (address.isInformationChanged()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public AddressImported create(final AddressImported addressImported) throws BusinessException {
        BusinessAssert.notNull(addressImported, "addressImported is mandatory in AddressImportedServiceImpl#create", "400");
        logger.info("create addressImported in AddressImportedServiceImpl#create");
        final AddressImported createdAddressImported = addressImportedRepository.insert(addressImported);
        return createdAddressImported;
    }

    @Override
    public AddressImported update(final AddressImported addressImported) throws BusinessException {
        BusinessAssert.notNull(addressImported, "addressImported is mandatory in AddressImportedServiceImpl#update", "400");
        logger.info("update addressImported with id '{}' in AddressImportedServiceImpl#create", addressImported.getId());
        return addressImportedRepository.save(addressImported);
    }

    @Override
    public void delete(final AddressImported addressImported) throws BusinessException {
        BusinessAssert.notNull(addressImported, "addressImported is mandatory in AddressImportedServiceImpl#delete", "400");
        logger.info("delete addressImported with id '{}' in AddressImportedServiceImpl#delete", addressImported.getId());
        addressImportedRepository.delete(addressImported.getId());
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all addresses in AddressImportedServiceImpl#deleteAll");
        addressImportedRepository.deleteAll();
    }

    @Override
    public String toString() {
        return "[AddressImportedServiceImpl]";
    }
}
