package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;
import lombok.Getter;

public @Data class CommunicationMessage {

    private @Getter String id;
    private @Getter String communicationUserConnectionId;
    private @Getter String name;
    private @Getter String message;
    private @Getter Boolean associatedToGroup;
    private @Getter Project project;
    private @Getter ArrayList<String> communicationGroupIds;
    private @Getter ArrayList<String> usersSelected;
    private @Getter Boolean confirmation;
    private @Getter String elapsedDate;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private @Getter DateTime executionDate;
    private @Getter boolean relevantForWorkingBook;
    private @Getter List<CommunicationSelectedDocumentFile> selectedDocumentFiles;
    private List<CommunicationAttachment> communicationAttachments;

}
