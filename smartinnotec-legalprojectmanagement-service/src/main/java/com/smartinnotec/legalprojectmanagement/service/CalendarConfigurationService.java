package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;

public interface CalendarConfigurationService {

    String getCalendarConfigurationHeader() throws BusinessException;

    String getCalendarConfigurationMessage(final String projectName, final String name, final String title, final String location,
            final Long start, final Long end) throws BusinessException;

    String getCalendarConfigurationHeaderEnd() throws BusinessException;

}
