package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ProductAggregateConditionTypeEnum {

    HARD("HARD"),
    LIQUID("LIQUID");

    private String value;

    private ProductAggregateConditionTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
