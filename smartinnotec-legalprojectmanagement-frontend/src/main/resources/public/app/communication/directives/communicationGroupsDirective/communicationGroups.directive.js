(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.directive('communicationGroupsDirective', communicationGroupsDirective);
    
    communicationGroupsDirective.$inject = ['$timeout', 'communicationService', 'communicationGroupCreationModalService', 'communicationGroupDeletionModalService'];
    
	function communicationGroupsDirective($timeout, communicationService, communicationGroupCreationModalService, communicationGroupDeletionModalService) {
		var directive = {
			restrict: 'E',
			scope: {
				selectedCommunicationGroups: '=',
				communicationGroupUserConnections: '=',
				currentUser: '='
			},
			templateUrl: 'app/communication/directives/communicationGroupsDirective/communicationGroups.html',
			link: function($scope) {
				$scope.membersOfMessageGroup = null;
				
				$scope.communicationGroupUserConnectionSelected = function(communicationGroupUserConnection) {
					if(typeof $scope.selectedCommunicationGroups != 'undefined') { 
						if(communicationGroupUserConnection.selected === true) {
							$scope.selectedCommunicationGroups.push(communicationGroupUserConnection.communicationGroup.id);
						} else if(communicationGroupUserConnection.selected === false) {
							for(var i = 0; i < $scope.selectedCommunicationGroups.length; i++) {
								if($scope.selectedCommunicationGroups[i] == communicationGroupUserConnection.communicationGroup.id) {
									$scope.selectedCommunicationGroups.splice(i, 1);
									break;
								}
							}
						}
					}
				};
				
				$scope.showMembersOfCommunicationGroup = function(communicationGroupUserConnection) {
					for(var i = 0; i < $scope.communicationGroupUserConnections.length; i++) {
		        		if($scope.communicationGroupUserConnections[i].id != communicationGroupUserConnection.id) {
		        			$scope.communicationGroupUserConnections[i].showMembers = false;
		        		}
		        	}
		        	
		        	if(communicationGroupUserConnection.showMembers == null || communicationGroupUserConnection.showMembers === false) {
		        		communicationService.findCommunicationGroupUserConnectionsByCommunicationGroup(communicationGroupUserConnection.communicationGroup.id).then(function (response) {
		        			$scope.membersOfCommunicationGroup = response.data;
		        			communicationGroupUserConnection.showMembers = true;
		        		}).catch(function (data) {
		        			console.log('error communicationGroups.directive#showMembersOfCommunicationGroup: ' + data);
		        		});
					  } else {
						  communicationGroupUserConnection.showMembers = false;
					  }
				};
				
				$scope.showCommunicationGroupEditModal = function(mode, communicationGroupUserConnection) {
					communicationService.findCommunicationGroupUserConnectionsByCommunicationGroup(communicationGroupUserConnection.communicationGroup.id).then(function (response) {
						var membersOfCommunicationGroup = response.data;
						communicationGroupCreationModalService.showCommunicationGroupCreationModal(mode, $scope.currentUser, membersOfCommunicationGroup, communicationGroupUserConnection, $scope.communicationGroupUserConnections);
		    		}).catch(function (data) {
						console.log('error communicationGroups.directive#showCommunicationGroupEditModal: ' + data);
					});
				};
				
				$scope.showCommunicationGroupDeletionModal = function(communicationGroup, communicationGroupUserConnections) {
		    		communicationGroupDeletionModalService.showCommunicationGroupDeletionModal(communicationGroup, communicationGroupUserConnections);
		    	};
			 }
		};
		return directive;
		
		////////////
	}
})();