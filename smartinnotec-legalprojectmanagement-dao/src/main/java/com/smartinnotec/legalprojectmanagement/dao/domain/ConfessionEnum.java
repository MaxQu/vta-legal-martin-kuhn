package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ConfessionEnum {

    ROEM_CATH("ROEM_CATH"),
    PROTESTANT("PROTESTANT"),
    OTHER("OTHER");

    private String value;

    private ConfessionEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
