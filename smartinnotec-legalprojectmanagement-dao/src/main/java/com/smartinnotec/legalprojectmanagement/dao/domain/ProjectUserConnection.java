package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "ProjectUserConnection")
public @Data class ProjectUserConnection {

    @Id
    private String id;
    @DBRef
    private User user;
    @DBRef
    private Contact contact;
    @DBRef
    private Project project;
    private ProjectUserConnectionRoleEnum projectUserConnectionRole;
    private boolean active;
    private Boolean defaultProject;

    public ProjectUserConnection() {
        active = true;
    }

    public ProjectUserConnection deepCopy() {
        final ProjectUserConnection copiedProjectUserConnection = new ProjectUserConnection();
        copiedProjectUserConnection.setUser(user);
        copiedProjectUserConnection.setProject(project);
        copiedProjectUserConnection.setProjectUserConnectionRole(projectUserConnectionRole);
        copiedProjectUserConnection.setActive(active);
        copiedProjectUserConnection.setDefaultProject(defaultProject);
        return copiedProjectUserConnection;
    }
}
