package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.FacilityDetails;

public interface FacilityDetailsService {

	Integer count() throws BusinessException;
	
    FacilityDetails create(final FacilityDetails facilityDetails) throws BusinessException;

    FacilityDetails update(final FacilityDetails facilityDetails) throws BusinessException;

    FacilityDetails findById(final String id) throws BusinessException;

    List<FacilityDetails> getFacilityDetailsByCalendarEventId(final String calendarEventId) throws BusinessException;

    FacilityDetails getLastFacilityDetailOfCalendarEventIdOrContact(final String calendarEventId) throws BusinessException;

    FacilityDetails getLastFacilityDetailOfCalendarEventId(final String calendarEventId) throws BusinessException;

    List<FacilityDetails> getByContactId(final String contactId) throws BusinessException;

    List<FacilityDetails> getByContactIdInRange(final String calendarEventId, final DateTime startDateTime, final DateTime endDateTime)
            throws BusinessException;
    
    List<FacilityDetails> findByCalendarEventStartDateBetween(final DateTime start, final DateTime end) throws BusinessException;
    
    List<FacilityDetails> findByCalendarEventIdAndCalendarEventStartDateBetween(final String calendarEventId, final DateTime start, final DateTime end) throws BusinessException;
    
    List<FacilityDetails> findByUserIdAndCalendarEventStartDateBetween(final String userId, final DateTime start, final DateTime end) throws BusinessException;
        
    List<FacilityDetails> findByUserIdAndContactIdAndCalendarEventStartDateBetween(final String userId, final String contactId, final DateTime start, final DateTime end) throws BusinessException;


    void delete(final String id) throws BusinessException;

}
