package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.AddressPreviousImported;

public interface AddressPreviousImportedRepository extends MongoRepository<AddressPreviousImported, String> {

}
