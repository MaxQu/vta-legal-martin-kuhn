package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEntry;

@Service("workingBookExcelServiceImpl")
public class WorkingBookExcelServiceImpl extends ExcelService implements WorkingBookExcelService {

    protected int offset;

    public WorkingBookExcelServiceImpl() {
        offset = 3;
    }

    @Override
    public HSSFWorkbook createExcelFile(final String additionalText, final Project project, final List<WorkingBook> workingBookEntries)
            throws BusinessException {
        final HSSFWorkbook workbook = new HSSFWorkbook();
        final HSSFSheet sheet = workbook.createSheet(project.getName());
        // create additional Text
        this.createAdditionalText(workbook, sheet, additionalText);
        // create header
        this.createHeader(workbook, sheet, getWorkingBookExcelHeader());
        // create data
        this.createData(workbook, sheet, workingBookEntries);
        for (int i = 0; i < 6; i++) {
            sheet.autoSizeColumn(i);
        }
        return workbook;
    }

    private void createAdditionalText(final HSSFWorkbook workbook, final HSSFSheet sheet, final String additionalText) {
        final Row additionalTextRow = sheet.createRow(0);
        final CellStyle style = workbook.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(this.getHeaderFont(workbook));
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 15));
        final Cell cell = additionalTextRow.createCell(0); // Spalte, Zeile
        cell.setCellValue(additionalText);
        cell.setCellStyle(style);
    }

    private void createHeader(final HSSFWorkbook workbook, final HSSFSheet sheet, final List<String> headerList) {
        final Row headerRow = sheet.createRow(offset);
        final CellStyle style = workbook.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(this.getHeaderFont(workbook));
        for (int i = 0; i < headerList.size(); i++) {
            final Cell cell = headerRow.createCell(i); // Spalte, Zeile
            cell.setCellValue(headerList.get(i));
            cell.setCellStyle(style);
        }
    }

    private void createData(final HSSFWorkbook workbook, final HSSFSheet sheet, final List<WorkingBook> workingBookEntries)
            throws BusinessException {
        // create data entries
        for (int i = 0; i < workingBookEntries.size(); i++) {
            final Row row = sheet.createRow(i + 1 + offset);
            final WorkingBook workingBook = workingBookEntries.get(i);
            final HSSFCellStyle style = this.getStyle(workbook);

            this.setLineAmount(row, i, style);
            this.setStartDateTime(row, workingBook, style);
            this.setEndDateTime(row, workingBook, style);
            this.setUser(row, workingBook, style);
            this.setProject(row, workingBook, style);
            this.setWorkingText(row, workingBook, style);
            this.setCreationDateTime(row, workingBook, style);
        }
    }

    protected void setLineAmount(final Row row, final int i, final HSSFCellStyle style) {
        final Cell cell0 = row.createCell(0);
        cell0.setCellStyle(style);
        cell0.setCellValue(i + 1 + "");
    }

    protected void setStartDateTime(final Row row, final WorkingBook workingBook, final HSSFCellStyle style) {
        final Cell cell1 = row.createCell(1);
        cell1.setCellStyle(style);
        cell1.setCellValue(dateTimeFormatter.print(workingBook.getDateTimeFrom()));
    }

    protected void setEndDateTime(final Row row, final WorkingBook workingBook, final HSSFCellStyle style) {
        final Cell cell1 = row.createCell(2);
        cell1.setCellStyle(style);
        cell1.setCellValue(dateTimeFormatter.print(workingBook.getDateTimeUntil()));
    }

    protected void setUser(final Row row, final WorkingBook workingBook, final HSSFCellStyle style) {
        final String userName = workingBook.getUser().getTitle() + " " + workingBook.getUser().getFirstname() + " "
            + workingBook.getUser().getSurname();
        final Cell cell1 = row.createCell(3);
        cell1.setCellStyle(style);
        cell1.setCellValue(userName);
    }

    protected void setProject(final Row row, final WorkingBook workingBook, final HSSFCellStyle style) {
        final Cell cell1 = row.createCell(4);
        cell1.setCellStyle(style);
        cell1.setCellValue(workingBook.getProject().getName());
    }

    protected void setWorkingText(final Row row, final WorkingBook workingBook, final HSSFCellStyle style) {
        final Cell cell1 = row.createCell(5);
        cell1.setCellStyle(style);
        final StringBuilder sb = new StringBuilder();
        sb.append(workingBook.getCategoryType()).append(": ");
        for (final WorkingBookCategoryTypeEntry workingBookCategoryTypeEntry : workingBook.getWorkingBookCategoryTypeEntries()) {
            final String workingBookCategoryText = workingBookCategoryTypeEntry.getWorkingBookCategoryText();
            final String enteredText = workingBookCategoryTypeEntry.getEnteredText();
            sb.append(workingBookCategoryText).append(": ").append(enteredText);
        }
        sb.append(workingBook.getWorkingText());
        if (workingBook.getAttachments() != null && !workingBook.getAttachments().isEmpty()) {
            for (final WorkingBookAttachment workingBookAttachment : workingBook.getAttachments()) {
                sb.append("\n").append(workingBookAttachment.getOriginalFilename());
            }
        }
        cell1.setCellValue(sb.toString());
    }

    protected void setCreationDateTime(final Row row, final WorkingBook workingBook, final HSSFCellStyle style) {
        final Cell cell1 = row.createCell(6);
        cell1.setCellStyle(style);
        cell1.setCellValue(dateTimeFormatter.print(workingBook.getCreationDateTime()));
    }

    private List<String> getWorkingBookExcelHeader() {
        final List<String> headerList = new ArrayList<>();
        headerList.add("#");
        headerList.add("Von");
        headerList.add("Bis");
        headerList.add("Ersteller");
        headerList.add("Projekt");
        headerList.add("Taetigkeitstext");
        headerList.add("Erstelldatum");
        return headerList;
    }
}
