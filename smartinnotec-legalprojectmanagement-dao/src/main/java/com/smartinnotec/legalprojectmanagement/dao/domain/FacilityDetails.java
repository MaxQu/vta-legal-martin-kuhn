package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "FacilityDetails")
public @Data class FacilityDetails implements Comparable<FacilityDetails> {

    @Id
    private String id;
    private String calendarEventId;
    @Transient
    private CalendarEvent calendarEvent;
    private String contactId;
    @Transient
    private Contact contact;
    private String userId;
    @Transient
    private User user;
    private List<ActivityTypeEnum> activityTypes;
    
    private String activityRoutineVisitationInformation;
    private String activityPolymerInformation;
    private String activityFaellungsMittelInformation;
    private String activityOtherChemicalsInformation;
    private String activityTechniqueInformation;
    private String activityServiceInformation;
    private String activityOtherInformation;
    
    private String discharge; // Direkt- oder Indirekteinleiter
    
    private String otherVendors; // andere Anbieter
    private String situationOfFacility; // Anlagensituation
    private String ewBuild; // EW (ausgebaut)
    private String ewLoaded; // EW (belastet)
    private List<FacilityDetailsTypeEnum> facilityDetailsTypes;
    private String vorreinigungInformation;
    private String entwaesserungsAggregateInformation;
    private String others;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime creationDate;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime calendarEventStartDate; // need for search in range
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime calendarEventEndDate; // need for search in range
    @DBRef
    private Tenant tenant;

    @Override
    public int compareTo(final FacilityDetails facilityDetails) {
        return this.getCalendarEventStartDate().compareTo(facilityDetails.getCalendarEventStartDate());
    }
}
