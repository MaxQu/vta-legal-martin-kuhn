(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.accesscontrol', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();