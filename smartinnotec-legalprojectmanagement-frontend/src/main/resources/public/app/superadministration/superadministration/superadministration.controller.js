(function() {
    'use strict';
    
    
    angular
    	.module('legalprojectmanagement.superadministration')
    	.controller('SuperAdministrationController', SuperAdministrationController);
     
    SuperAdministrationController.$inject = ['$scope', '$timeout', 'currentUser', 'tenants', 'thinningPolymerDateTimeValues', 'currentThinningPolymerDateTimeValue', 'superAdministrationService', 'userService', 'thinningPolymerDateTimeValueService', 'dateUtilityService', 'historyAmountService'];
    
    function SuperAdministrationController($scope, $timeout, currentUser, tenants, thinningPolymerDateTimeValues, currentThinningPolymerDateTimeValue, superAdministrationService, userService, thinningPolymerDateTimeValueService, dateUtilityService, historyAmountService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	vm.tenants = tenants.data;
    	vm.thinningPolymerDateTimeValues = thinningPolymerDateTimeValues.data;
    	vm.currentThinningPolymerDateTimeValue = currentThinningPolymerDateTimeValue.data;
    	vm.tenantStillExistsError = false;
    	vm.tenantSelected = false;
    	vm.tenantUpdated = false;
    	vm.thinningPolymerDateTimeValue = {
    		id: null,
    		dateTime: new Date(),
    		value: 0
    	};
    	vm.historyAmounts = [];
    	vm.labels = [];
    	vm.series = [];
    	vm.historyAmountFromTrigger = false;
    	vm.historyAmountUntilTrigger = false;
    	vm.historyAmountFrom = dateUtilityService.oneMonthBack(new Date());
    	vm.historyAmountUntil = new Date();
    	vm.options = { legend: { display: true } };
    	
    	vm.addNewTenant = addNewTenant;
    	vm.deleteTenant = deleteTenant;
    	vm.setTenantForSuperAdmin = setTenantForSuperAdmin;
    	vm.createThinningPolymerDateTimeValue = createThinningPolymerDateTimeValue;
    	vm.updateThinningPolymerDateTimeValue = updateThinningPolymerDateTimeValue;
    	vm.deleteThinningPolymerDateTimeValue = deleteThinningPolymerDateTimeValue;
    	vm.getHistoryAmountsBetweenDates = getHistoryAmountsBetweenDates;
    	vm.openHistoryAmountFrom = openHistoryAmountFrom;
    	vm.openHistoryAmountUntil = openHistoryAmountUntil;
    	initializeTenantOfCurrentUser();
    	getHistoryAmountsBetweenDates();
    	
    	////////////

    	function setTenantCreatedTimeout() {
    		$timeout(function() {
				vm.createTenantClicked = false;
			   }, 2000); 
    	}
    	
    	function setTenantSelectedTimeout(tenant) {
    		$timeout(function() {
    			vm.tenantUpdated = false;
			   }, 2000); 
    	}
    	
    	function openHistoryAmountFrom($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.historyAmountFromTrigger = true;
		}
    	
    	function openHistoryAmountUntil($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.historyAmountUntilTrigger = true;
		}
    	
    	function addNewTenant(newTenant) {
    		vm.tenantStillExistsError = false;
    		if(tenantStillExists(newTenant)) {
    			return;
    		}
    		var tenant = {};
    		tenant.name = newTenant;
    		superAdministrationService.createTenant(tenant).then(function successCallback(response) {
    			vm.tenants.push(response.data);
    			vm.newTenant = null;
    			vm.createTenantClicked = true;
    			setTenantCreatedTimeout();
  	   		 }, function errorCallback(response) {
  	   			  console.log('error superadministration.controller#addNewTenant');
  	   		 });
    	}
    	
    	function tenantStillExists(newTenant) {
    		vm.tenantStillExistsError = false;
    		for(var i = 0; i < vm.tenants.length; i++) {
    			if(vm.tenants[i].name == newTenant) {
    				vm.tenants[i].stillExists = true;
    				vm.tenantStillExistsError = true;
    			} else {
    				vm.tenants[i].stillExists = false;
    			}
    		}
    		return vm.tenantStillExistsError;
    	}
    	
    	function deleteTenant(tenant) {
    		superAdministrationService.deleteTenant(tenant.id).then(function successCallback(response) {
    			removeTenant(tenant);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error superadministration.controller#deleteTenant');
  	   		 });
    	}
    	
    	function removeTenant(tenant) {
    		for(var i = 0; i < vm.tenants.length; i++) {
    			if(vm.tenants[i].id == tenant.id) {
    				vm.tenants.splice(i, 1);
    			}
    		}
    	}
    	
    	function initializeTenantOfCurrentUser() {
    		if(currentUser.tenant != null) {
    			for(var i = 0; i < vm.tenants.length; i++) {
        			if(vm.tenants[i].id != currentUser.tenant.id) {
        				vm.tenants[i].selected = true;
        			}
        		}
    		}
    	}
    	
    	function setTenantForSuperAdmin(tenant) {
    		for(var i = 0; i < vm.tenants.length; i++) {
    			if(vm.tenants[i].id != tenant.id) {
    				vm.tenants[i].selected = false;
    			}
    		}
    		if(tenant.selected) {
    			currentUser.tenant = tenant;
    		} else if(!tenant.selected) {
    			currentUser.tenant = null;
    		}
    		userService.updateUser(currentUser).then(function successCallback(response) {
    			vm.tenantUpdated = true;
    			setTenantSelectedTimeout(currentUser.tenant);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error superadministration.controller#setTenantForSuperAdmin');
 	   		 });
    	}
    	
    	function createThinningPolymerDateTimeValue() {
    		thinningPolymerDateTimeValueService.create(vm.thinningPolymerDateTimeValue).then(function(response) {
    			thinningPolymerDateTimeValueService.findAll().then(function(response) {
    				vm.thinningPolymerDateTimeValues = response.data;
     	   		 }, function errorCallback(response) {
     	   			  console.log('error superadministration.controller#createThinningPolymerDateTimeValue');
     	   		 });
 	   		 }, function errorCallback(response) {
 	   			  console.log('error superadministration.controller#createThinningPolymerDateTimeValue');
 	   		 });
    	}
    	
    	function updateThinningPolymerDateTimeValue(thinningPolymerDateTimeValue) {
    		thinningPolymerDateTimeValueService.update(thinningPolymerDateTimeValue).then(function(response) {
	   		 }, function errorCallback(response) {
	   			  console.log('error superadministration.controller#updateThinningPolymerDateTimeValue');
	   		 });
    	}
    	
    	function deleteThinningPolymerDateTimeValue(thinningPolymerDateTimeValue) {
    		thinningPolymerDateTimeValueService.deleteThinningPolymerDateTimeValue(thinningPolymerDateTimeValue.id).then(function(response) {
    			for(var i = 0; i < vm.thinningPolymerDateTimeValues.length; i++) {
    				if(vm.thinningPolymerDateTimeValues[i].id === thinningPolymerDateTimeValue.id) {
    					vm.thinningPolymerDateTimeValues.splice(i, 1);
    					return;
    				}
    			}
 	   		 }, function errorCallback(response) {
 	   			  console.log('error superadministration.controller#deleteThinningPolymerDateTimeValue');
 	   		 });
    	}
    	
    	function getHistoryAmountsBetweenDates() {
    		vm.historyAmounts = [];
        	vm.labels = [];
        	vm.series = [];
    		var start = dateUtilityService.formatDateToString(vm.historyAmountFrom);
    		var end = dateUtilityService.formatDateToString(vm.historyAmountUntil);
    		historyAmountService.getHistoryAmountsBetweenDates(start, end).then(function(response) {
				var historyAmounts = response.data;
				
				var contacts = [];
				var activities = [];
				var facilityDetails = [];
				var documents = [];
				var products = [];
				var projects = [];
				var calendarEvents = [];
				var receipes = [];
				var mixtures = [];
				var thinnings = [];
				var communicationUserConnections = [];

				for(var i = 0; i < historyAmounts.length; i++) {
					vm.labels.push(historyAmounts[i].date);
					contacts.push(historyAmounts[i].contacts);
					activities.push(historyAmounts[i].activities);
					facilityDetails.push(historyAmounts[i].facilityDetails);
					documents.push(historyAmounts[i].documents);
					products.push(historyAmounts[i].products);
					projects.push(historyAmounts[i].projects);
					calendarEvents.push(historyAmounts[i].calendarEvents);
					receipes.push(historyAmounts[i].receipes);
					mixtures.push(historyAmounts[i].mixtures);
					thinnings.push(historyAmounts[i].thinnings);
					communicationUserConnections.push(historyAmounts[i].communicationUserConnections);
				}
				
				vm.series.push('Kontakt');
				vm.series.push('Tätigkeit');
				vm.series.push('Anlagendetails');
				vm.series.push('Dokumente');
				vm.series.push('Produkt');
				vm.series.push('Projekt');
				vm.series.push('Termine');
				vm.series.push('Rezepte');
				vm.series.push('Mischungen');
				vm.series.push('Verdünnungen');
				vm.series.push('Arbeitsaufträge');
				
				vm.historyAmounts.push(contacts);
				vm.historyAmounts.push(activities);
				vm.historyAmounts.push(facilityDetails);
				vm.historyAmounts.push(documents);
				vm.historyAmounts.push(products);
				vm.historyAmounts.push(projects);
				vm.historyAmounts.push(calendarEvents);
				vm.historyAmounts.push(receipes);
				vm.historyAmounts.push(mixtures);
				vm.historyAmounts.push(thinnings);
				vm.historyAmounts.push(communicationUserConnections);
				
	   		 }, function errorCallback(response) {
	   			  console.log('error superadministration.controller#getHistoryAmountsBetweenDates');
	   		 });
    	}
	} 
})();