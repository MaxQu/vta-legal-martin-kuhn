package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class UtilServiceTest extends AbstractServiceTest {

    @Test
    public void shouldCompareList() throws BusinessException {
        final List<String> list1 = new ArrayList<>();
        list1.add("");

        final List<String> list2 = new ArrayList<>();

        final boolean changed1 = utilService.hasStringListChanged(list1, list2);
        Assert.assertFalse(changed1);

        final List<String> list3 = new ArrayList<>();
        list3.add("TestEmail");
        list3.add("Hallo123");

        final List<String> list4 = new ArrayList<>();
        list4.add("TestEmail");

        final boolean changed2 = utilService.hasStringListChanged(list3, list4);
        Assert.assertTrue(changed2);
    }

    @Test
    public void shouldTestHasAgentNumberPropertyChanged() throws BusinessException {

        final List<AgentNumberContainer> agentNumberContainers = new ArrayList<>();
        final AgentNumberContainer agentNumberContainer1 = new AgentNumberContainer();
        agentNumberContainer1.setCustomerNumber("123456");
        final AgentNumberContainer agentNumberContainer2 = new AgentNumberContainer();
        agentNumberContainer2.setCustomerNumber("443343");
        agentNumberContainers.add(agentNumberContainer1);
        agentNumberContainers.add(agentNumberContainer2);

        final List<AgentNumberContainer> mergedContactImportedAgentNumberContainers = new ArrayList<>();
        final AgentNumberContainer agentNumberContainerA = new AgentNumberContainer();
        agentNumberContainerA.setCustomerNumber("123456");
        mergedContactImportedAgentNumberContainers.add(agentNumberContainerA);

        final boolean hasAgentNumberPropertyChanged = utilService.hasAgentNumberPropertyChanged(agentNumberContainers,
            mergedContactImportedAgentNumberContainers);
        Assert.assertFalse(hasAgentNumberPropertyChanged);
    }

}
