package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class ProjectDataSheetAuthority {

    private String authorityName;
    private String authorityStreet;
    private String authorityNumber;
    private Integer authorityZip;
    private String authorityLocation;
    private String authorityProvinceType;
    private String authorityApprovalPerson;

}
