(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.factory('timeregistrationService', timeregistrationService);
        
    timeregistrationService.$inject = ['$http', 'api_config'];
    
    function timeregistrationService($http, api_config) {
		var service = {
			findTimeRegistrations: findTimeRegistrations,
			getTimeRegistrationTimeRangeSum: getTimeRegistrationTimeRangeSum,
			createTimeRegistration: createTimeRegistration,
			updateTimeRegistration: updateTimeRegistration,
			deleteTimeRegistration: deleteTimeRegistration
		};
		
		return service;
		
		////////////
		
		function findTimeRegistrations(projectId, start, end) {
		return $http.get(api_config.BASE_URL + '/timeregistrations/timeregistration/' + projectId + '/' + start + '/' + end + '/');
		}
		
		function getTimeRegistrationTimeRangeSum(projectId, start, end) {
			return $http.get(api_config.BASE_URL + '/timeregistrations/timeregistration/' + projectId + '/' + start + '/' + end + '/timerangesum');
			}
		
		function createTimeRegistration(userId, timeRegistration) {
			return $http.post(api_config.BASE_URL + '/timeregistrations/timeregistration/' + userId, timeRegistration);
		}
		
		function updateTimeRegistration(userId, timeRegistration) {
			return $http.put(api_config.BASE_URL + '/timeregistrations/timeregistration/' + userId, timeRegistration);
		}
		
		function deleteTimeRegistration(id) {
			return $http.delete(api_config.BASE_URL + '/timeregistrations/timeregistration/' + id);
		}
    }
})();