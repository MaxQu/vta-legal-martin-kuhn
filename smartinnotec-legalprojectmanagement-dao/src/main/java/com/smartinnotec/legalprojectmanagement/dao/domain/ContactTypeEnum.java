package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ContactTypeEnum {

    POTENTIAL_BUYER("POTENTIAL_BUYER", true),
    SUPPLIER("SUPPLIER", true),
    CUSTOMER("CUSTOMER", true),
    PURCHASER("PURCHASER", true),
    URBAN_ANLAGE("URBAN_ANLAGE", true),
    INDUSTRY("INDUSTRY", true),
    ENGINEER_OFFICE("ENGINEER_OFFICE", true),
    OTHER("OTHER", true),
    LAUB_FROSCH("LAUB_FROSCH", true),
    NEWS_LETTER("NEWS_LETTER", true),
    USER_CONTACT("USER_CONTACT", true);

    private String value;
    private boolean active;

    private ContactTypeEnum(final String value, final boolean active) {
        this.value = value;
        this.active = active;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
