package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChangedTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface ContactChangedService {

    List<ContactChanged> findPagedContactsChanged(final Integer page, final Boolean committed) throws BusinessException;

    Integer getAmountOfContactsChanged(final Tenant tenant) throws BusinessException;

    Integer getAmountOfContactsChangedByCommitted(final Tenant tenant, final Boolean committed) throws BusinessException;

    ContactChanged findById(final String id) throws BusinessException;

    List<ContactChanged> getAllContactsChanged() throws BusinessException;

    List<ContactChanged> getContactsChangedByCommitted(final boolean committed) throws BusinessException;

    ContactChanged convertFromContactToContactChanged(final Contact contact, final ContactChangedTypeEnum contactChangedType)
            throws BusinessException;

    ContactChanged create(final ContactChanged contactChanged, final User user) throws BusinessException;

    ContactChanged update(final ContactChanged contactChanged, final User user) throws BusinessException;

    void delete(final String id) throws BusinessException;

    void deleteAll() throws BusinessException;

    void deleteAllCommittedContactsChanged() throws BusinessException;

}
