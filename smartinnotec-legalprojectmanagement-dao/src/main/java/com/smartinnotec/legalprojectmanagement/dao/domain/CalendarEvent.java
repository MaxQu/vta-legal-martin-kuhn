package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "CalendarEvent")
public @Data class CalendarEvent {

    @Id
    private String id;
    @TextIndexed
    private String title;
    @TextIndexed
    private String location;
    private Long startsAt;
    private Long endsAt;
    private boolean createContact;
    private CalendarEventTypeEnum calendarEventTyp;
    @DBRef
    private Project project;
    @DBRef
    private Tenant tenant;
    private Boolean serialDate;
    private Long serialDateNumber;
    private Boolean confirmed;
    private CalendarEventSerialDateTypeEnum calendarEventSerialDateType;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime serialEndDate;
    @TextIndexed
    private String information;
    private HistoryTypeEnum archivedHistoryType;
    private Boolean archived;
    @Transient
    private ContactAddressWithProducts contactAddressWithProducts; // address of weiterer Standort
    @Transient
    private Contact contact;

    public CalendarEvent() {
        confirmed = false;
        archived = false;
    }

    public CalendarEvent(final CalendarEventBuilder calendarEventBuilder) {
        this.id = calendarEventBuilder.id;
        this.title = calendarEventBuilder.title;
        this.createContact = calendarEventBuilder.createContact;
        this.location = calendarEventBuilder.location;
        this.startsAt = calendarEventBuilder.startsAt;
        this.endsAt = calendarEventBuilder.endsAt;
        this.calendarEventTyp = calendarEventBuilder.calendarEventTyp;
        this.project = calendarEventBuilder.project;
        this.tenant = calendarEventBuilder.tenant;
        this.serialDate = calendarEventBuilder.serialDate;
        this.serialDateNumber = calendarEventBuilder.serialDateNumber;
        this.calendarEventSerialDateType = calendarEventBuilder.calendarEventSerialDateType;
        this.serialEndDate = calendarEventBuilder.serialEndDate;
        this.confirmed = calendarEventBuilder.confirmed;
        this.information = calendarEventBuilder.information;
        this.archivedHistoryType = calendarEventBuilder.archivedHistoryType;
        this.archived = calendarEventBuilder.archived;
    }

    public CalendarEvent copy(final CalendarEvent calendarEvent) {
        final CalendarEvent copiedCalendarEvent = new CalendarEvent();
        copiedCalendarEvent.setCreateContact(createContact);
        copiedCalendarEvent.setTitle(calendarEvent.getTitle());
        copiedCalendarEvent.setLocation(calendarEvent.getLocation());
        copiedCalendarEvent.setStartsAt(calendarEvent.getStartsAt());
        copiedCalendarEvent.setEndsAt(calendarEvent.getEndsAt());
        copiedCalendarEvent.setCalendarEventTyp(calendarEvent.getCalendarEventTyp());
        copiedCalendarEvent.setProject(calendarEvent.getProject());
        copiedCalendarEvent.setTenant(calendarEvent.getTenant());
        copiedCalendarEvent.setSerialDate(calendarEvent.getSerialDate());
        copiedCalendarEvent.setSerialDateNumber(calendarEvent.getSerialDateNumber());
        copiedCalendarEvent.setCalendarEventSerialDateType(calendarEvent.getCalendarEventSerialDateType());
        copiedCalendarEvent.setSerialEndDate(calendarEvent.getSerialEndDate());
        copiedCalendarEvent.setConfirmed(calendarEvent.getConfirmed());
        copiedCalendarEvent.setInformation(calendarEvent.getInformation());
        copiedCalendarEvent.setArchived(calendarEvent.getArchived());
        copiedCalendarEvent.setArchivedHistoryType(calendarEvent.getArchivedHistoryType());
        return copiedCalendarEvent;
    }

    public static class CalendarEventBuilder {

        private String id;
        private boolean createContact;
        private String title;
        private String location;
        private Long startsAt;
        private Long endsAt;
        private CalendarEventTypeEnum calendarEventTyp;
        private Project project;
        private Tenant tenant;
        private Boolean serialDate;
        private Long serialDateNumber;
        private CalendarEventSerialDateTypeEnum calendarEventSerialDateType;
        private DateTime serialEndDate;
        private Boolean confirmed;
        private String information;
        private Boolean archived;
        private HistoryTypeEnum archivedHistoryType;

        public CalendarEventBuilder setId(final String id) {
            this.id = id;
            return this;
        }

        public CalendarEventBuilder setCreateContact(final boolean createContact) {
            this.createContact = createContact;
            return this;
        }

        public CalendarEventBuilder setTile(final String title) {
            this.title = title;
            return this;
        }

        public CalendarEventBuilder setLocation(final String location) {
            this.location = location;
            return this;
        }

        public CalendarEventBuilder setStartsAt(final Long startsAt) {
            this.startsAt = startsAt;
            return this;
        }

        public CalendarEventBuilder setEndsAt(final Long endsAt) {
            this.endsAt = endsAt;
            return this;
        }

        public CalendarEventBuilder setCalendarEventTyp(final CalendarEventTypeEnum calendarEventTyp) {
            this.calendarEventTyp = calendarEventTyp;
            return this;
        }

        public CalendarEventBuilder setProject(final Project project) {
            this.project = project;
            return this;
        }

        public CalendarEventBuilder setTenant(final Tenant tenant) {
            this.tenant = tenant;
            return this;
        }

        public CalendarEventBuilder setSerialDate(final Boolean serialDate) {
            this.serialDate = serialDate;
            return this;
        }

        public CalendarEventBuilder setSerialDateNumber(final Long serialDateNumber) {
            this.serialDateNumber = serialDateNumber;
            return this;
        }

        public CalendarEventBuilder setCalendarEventSerialDateType(final CalendarEventSerialDateTypeEnum calendarEventSerialDateType) {
            this.calendarEventSerialDateType = calendarEventSerialDateType;
            return this;
        }

        public CalendarEventBuilder setSerialEndDate(final DateTime serialEndDate) {
            this.serialEndDate = serialEndDate;
            return this;
        }

        public CalendarEventBuilder setTitle(final String title) {
            this.title = title;
            return this;
        }

        public CalendarEventBuilder setConfirmed(final Boolean confirmed) {
            this.confirmed = confirmed;
            return this;
        }

        public CalendarEventBuilder setInformation(final String information) {
            this.information = information;
            return this;
        }

        public CalendarEventBuilder setArchived(final Boolean archived) {
            this.archived = archived;
            return this;
        }

        public CalendarEventBuilder setArchivedHistoryType(final HistoryTypeEnum archivedHistoryType) {
            this.archivedHistoryType = archivedHistoryType;
            return this;
        }

        public CalendarEvent build() {
            return new CalendarEvent(this);
        }
    }
}
