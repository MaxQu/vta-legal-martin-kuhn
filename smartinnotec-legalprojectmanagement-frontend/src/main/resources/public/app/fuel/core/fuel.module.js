(function() {
    'use strict';

	console.log('fuel.module called');

    angular
    	.module('legalprojectmanagement.fuel', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
