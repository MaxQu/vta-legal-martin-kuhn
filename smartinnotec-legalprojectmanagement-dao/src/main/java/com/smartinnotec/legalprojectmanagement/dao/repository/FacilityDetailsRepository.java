package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.FacilityDetails;

public interface FacilityDetailsRepository extends MongoRepository<FacilityDetails, String> {

    List<FacilityDetails> findByCalendarEventId(final String calendarEventId);

    List<FacilityDetails> findByContactId(final String contactId);
    
    FacilityDetails findFirst1ByCalendarEventIdOrderByCreationDateDesc(final String calendarEventId);

    List<FacilityDetails> findByContactIdAndCalendarEventStartDateBetween(final String contactId, final DateTime start, final DateTime end);

    List<FacilityDetails> findByCalendarEventStartDateBetween(final DateTime start, final DateTime end);
    
    List<FacilityDetails> findByCalendarEventIdAndCalendarEventStartDateBetween(final String calendarEventId, final DateTime start, final DateTime end);
    
    List<FacilityDetails> findByUserIdAndCalendarEventStartDateBetween(final String userId, final DateTime start, final DateTime end);
        
    List<FacilityDetails> findByUserIdAndContactIdAndCalendarEventStartDateBetween(final String userId, final String contactId, final DateTime start, final DateTime end);

}
