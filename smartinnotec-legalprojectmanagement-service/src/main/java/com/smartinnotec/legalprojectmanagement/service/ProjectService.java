package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface ProjectService {

	Integer count() throws BusinessException;
	
    Project findProject(final String id) throws BusinessException;

    List<Project> findAllProjectsByTenant(final Tenant tenant) throws BusinessException;

    List<Project> findAllGeneralAvailableProjects() throws BusinessException;

    List<Project> findProjectByTerm(final String searchTerm) throws BusinessException;

    Project create(final Project project) throws BusinessException;

    Project update(final Project project) throws BusinessException;

    Integer getProjectPageSize() throws BusinessException;

    boolean checkIfProjectExists(final String id) throws BusinessException;

    void deleteProject(final String projectId, final String userId, final String username) throws BusinessException;

    boolean renameFolder(final String projectId, final String username) throws BusinessException;

}
