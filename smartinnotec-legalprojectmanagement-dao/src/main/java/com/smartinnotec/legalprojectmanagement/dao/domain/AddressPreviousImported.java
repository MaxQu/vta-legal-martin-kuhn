package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;

import lombok.Data;

@Document(collection = "AddressPreviousImported")
public @Data class AddressPreviousImported {

    @JsonView(RestServiceResponseView.AddressPublic.class)
    @Id
    private String id;
    private String flfId;
    private boolean flfIdChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String additionalInformation;
    private boolean additionalInformationChanged;
    @TextIndexed
    private List<CustomerNumberContainer> customerNumberContainers;
    private boolean customerNumberContainersChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String street;
    private boolean streetChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String intSign;
    private boolean intSignChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String postalCode;
    private boolean postalCodeChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    @TextIndexed
    private String region;
    private boolean regionChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private ProvinceTypeEnum provinceType;
    private boolean provinceTypeChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private CountryTypeEnum country;
    private boolean countryChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String telephone;
    private boolean telephoneChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String email;
    private boolean emailChanged;
    @JsonView(RestServiceResponseView.AddressPublic.class)
    private String information;
    private boolean informationChanged;

    public void addCustomerNumberContainer(final CustomerNumberContainer customerNumberContainer) {
        if (customerNumberContainers == null) {
            customerNumberContainers = new ArrayList<>();
        }
        customerNumberContainers.add(customerNumberContainer);
    }
}
