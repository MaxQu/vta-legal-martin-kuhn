package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ContactChangedTypeEnum {

    CONTACT_CREATED("CONTACT_CREATED"),
    CONTACT_UPDATED("CONTACT_UPDATED"),
    CONTACT_DELETED("CONTACT_DELETED");

    private String value;

    private ContactChangedTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
