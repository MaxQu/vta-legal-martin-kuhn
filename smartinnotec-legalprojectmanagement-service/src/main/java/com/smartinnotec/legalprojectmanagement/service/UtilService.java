package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;

public interface UtilService {

    String escapeString(final String searchString) throws BusinessException;

    boolean hasStringPropertyChanged(final String property1, final String property2) throws BusinessException;

    boolean hasStringListChanged(final List<String> list1, final List<String> list2) throws BusinessException;

    boolean hasCustomerNumberPropertyChanged(final List<CustomerNumberContainer> customerNumberContainers,
            final List<CustomerNumberContainer> mergedContactImportedCustomerNumberContainers)
            throws BusinessException;

    boolean hasAgentNumberPropertyChanged(final List<AgentNumberContainer> agentNumberContainers,
            final List<AgentNumberContainer> mergedContactImportedAgentNumberContainers)
            throws BusinessException;

    boolean overlapTimeRange(final DateTime date1, final DateTime date2, final DateTime start, final DateTime end) throws BusinessException;
}
