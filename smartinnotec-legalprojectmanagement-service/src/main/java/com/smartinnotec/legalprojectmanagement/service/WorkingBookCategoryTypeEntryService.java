package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEntry;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;

public interface WorkingBookCategoryTypeEntryService {

    List<WorkingBookCategoryTypeEntry> findByWorkingBookCategoryTypeEnumAndTenant(final String projectId,
            final WorkingBookCategoryTypeEnum workingBookCategoryType, final Tenant tenant) throws BusinessException;

    WorkingBookCategoryTypeEntry createWorkingBookCategoryTypeEntry(final WorkingBookCategoryTypeEntry workingBookCategoryTypeEntry)
            throws BusinessException;

    WorkingBookCategoryTypeEntry updateWorkingBookCategoryTypeEntry(final WorkingBookCategoryTypeEntry workingBookCategoryTypeEntry)
            throws BusinessException;

    void deleteWorkingBookCategoryType(final String workingBookCategoryTypeId) throws BusinessException;

}
