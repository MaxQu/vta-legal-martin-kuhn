package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.util.Assert;

import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorization;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationStateEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationUserContainer;

public class UserAuthorizationUserContainerDAOTest extends AbstractDAOTest {

    @Test
    public void shouldCreateUserAuthorizationUserContainer() {
        final UserAuthorizationUserContainer userAuthorizationUserContainer = new UserAuthorizationUserContainer();
        userAuthorizationUserContainer.setUserId("1234");

        final List<UserAuthorization> userAuthorizations = new ArrayList<>();

        // Project read
        final UserAuthorization userAuthorizationProjectRead = new UserAuthorization();
        userAuthorizationProjectRead.setUserAuthorizationType(UserAuthorizationTypeEnum.PROJECT);
        userAuthorizationProjectRead.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationProjectRead.setState(true);
        userAuthorizationProjectRead.setOrder(1);
        userAuthorizations.add(userAuthorizationProjectRead);

        // Project change
        final UserAuthorization userAuthorizationProjectChange = new UserAuthorization();
        userAuthorizationProjectChange.setUserAuthorizationType(UserAuthorizationTypeEnum.PROJECT);
        userAuthorizationProjectChange.setUserAuthorizationState(UserAuthorizationStateEnum.CHANGE);
        userAuthorizationProjectChange.setState(true);
        userAuthorizationProjectChange.setOrder(2);
        userAuthorizations.add(userAuthorizationProjectChange);

        // Project delete
        final UserAuthorization userAuthorizationProjectDelete = new UserAuthorization();
        userAuthorizationProjectDelete.setUserAuthorizationType(UserAuthorizationTypeEnum.PROJECT);
        userAuthorizationProjectDelete.setUserAuthorizationState(UserAuthorizationStateEnum.DELETE);
        userAuthorizationProjectDelete.setState(false);
        userAuthorizationProjectDelete.setOrder(3);
        userAuthorizations.add(userAuthorizationProjectDelete);

        // Calendar read
        final UserAuthorization userAuthorizationCalendarRead = new UserAuthorization();
        userAuthorizationCalendarRead.setUserAuthorizationType(UserAuthorizationTypeEnum.CALENDAR);
        userAuthorizationCalendarRead.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationCalendarRead.setState(true);
        userAuthorizationCalendarRead.setOrder(4);
        userAuthorizations.add(userAuthorizationCalendarRead);

        // Calendar change
        final UserAuthorization userAuthorizationCalendarChange = new UserAuthorization();
        userAuthorizationCalendarChange.setUserAuthorizationType(UserAuthorizationTypeEnum.CALENDAR);
        userAuthorizationCalendarChange.setUserAuthorizationState(UserAuthorizationStateEnum.CHANGE);
        userAuthorizationCalendarChange.setState(true);
        userAuthorizationCalendarChange.setOrder(5);
        userAuthorizations.add(userAuthorizationCalendarChange);

        // Calendar delete
        final UserAuthorization userAuthorizationCalendarDelete = new UserAuthorization();
        userAuthorizationCalendarDelete.setUserAuthorizationType(UserAuthorizationTypeEnum.CALENDAR);
        userAuthorizationCalendarDelete.setUserAuthorizationState(UserAuthorizationStateEnum.DELETE);
        userAuthorizationCalendarDelete.setState(true);
        userAuthorizationCalendarDelete.setOrder(6);
        userAuthorizations.add(userAuthorizationCalendarDelete);

        // Contact read
        final UserAuthorization userAuthorizationContactRead = new UserAuthorization();
        userAuthorizationContactRead.setUserAuthorizationType(UserAuthorizationTypeEnum.CONTACT);
        userAuthorizationContactRead.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationContactRead.setState(true);
        userAuthorizationContactRead.setOrder(7);
        userAuthorizations.add(userAuthorizationContactRead);

        // Contact change
        final UserAuthorization userAuthorizationContactChange = new UserAuthorization();
        userAuthorizationContactChange.setUserAuthorizationType(UserAuthorizationTypeEnum.CONTACT);
        userAuthorizationContactChange.setUserAuthorizationState(UserAuthorizationStateEnum.CHANGE);
        userAuthorizationContactChange.setState(true);
        userAuthorizationContactChange.setOrder(8);
        userAuthorizations.add(userAuthorizationContactChange);

        // Contact delete
        final UserAuthorization userAuthorizationContactDelete = new UserAuthorization();
        userAuthorizationContactDelete.setUserAuthorizationType(UserAuthorizationTypeEnum.CONTACT);
        userAuthorizationContactDelete.setUserAuthorizationState(UserAuthorizationStateEnum.DELETE);
        userAuthorizationContactDelete.setState(false);
        userAuthorizationContactDelete.setOrder(9);
        userAuthorizations.add(userAuthorizationContactDelete);

        // working book read
        final UserAuthorization userAuthorizationWorkingBookRead = new UserAuthorization();
        userAuthorizationWorkingBookRead.setUserAuthorizationType(UserAuthorizationTypeEnum.WORKING_BOOK);
        userAuthorizationWorkingBookRead.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationWorkingBookRead.setState(true);
        userAuthorizationWorkingBookRead.setOrder(10);
        userAuthorizations.add(userAuthorizationWorkingBookRead);

        // working book change
        final UserAuthorization userAuthorizationWorkingBookChange = new UserAuthorization();
        userAuthorizationWorkingBookChange.setUserAuthorizationType(UserAuthorizationTypeEnum.WORKING_BOOK);
        userAuthorizationWorkingBookChange.setUserAuthorizationState(UserAuthorizationStateEnum.CHANGE);
        userAuthorizationWorkingBookChange.setState(true);
        userAuthorizationWorkingBookChange.setOrder(11);
        userAuthorizations.add(userAuthorizationWorkingBookChange);

        // working book delete
        final UserAuthorization userAuthorizationWorkingBookDelete = new UserAuthorization();
        userAuthorizationWorkingBookDelete.setUserAuthorizationType(UserAuthorizationTypeEnum.WORKING_BOOK);
        userAuthorizationWorkingBookDelete.setUserAuthorizationState(UserAuthorizationStateEnum.DELETE);
        userAuthorizationWorkingBookDelete.setState(false);
        userAuthorizationWorkingBookDelete.setOrder(12);
        userAuthorizations.add(userAuthorizationWorkingBookDelete);

        // communication read
        final UserAuthorization userAuthorizationCommunicationRead = new UserAuthorization();
        userAuthorizationCommunicationRead.setUserAuthorizationType(UserAuthorizationTypeEnum.COMMUNICATION);
        userAuthorizationCommunicationRead.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationCommunicationRead.setState(true);
        userAuthorizationCommunicationRead.setOrder(13);
        userAuthorizations.add(userAuthorizationCommunicationRead);

        // communication change
        final UserAuthorization userAuthorizationCommunicationChange = new UserAuthorization();
        userAuthorizationCommunicationChange.setUserAuthorizationType(UserAuthorizationTypeEnum.COMMUNICATION);
        userAuthorizationCommunicationChange.setUserAuthorizationState(UserAuthorizationStateEnum.CHANGE);
        userAuthorizationCommunicationChange.setState(true);
        userAuthorizationCommunicationChange.setOrder(14);
        userAuthorizations.add(userAuthorizationCommunicationChange);

        // communication delete
        final UserAuthorization userAuthorizationCommunicationDelete = new UserAuthorization();
        userAuthorizationCommunicationDelete.setUserAuthorizationType(UserAuthorizationTypeEnum.COMMUNICATION);
        userAuthorizationCommunicationDelete.setUserAuthorizationState(UserAuthorizationStateEnum.DELETE);
        userAuthorizationCommunicationDelete.setState(false);
        userAuthorizationCommunicationDelete.setOrder(15);
        userAuthorizations.add(userAuthorizationCommunicationDelete);

        // history read
        final UserAuthorization userAuthorizationHistoryRead = new UserAuthorization();
        userAuthorizationHistoryRead.setUserAuthorizationType(UserAuthorizationTypeEnum.HISTORY);
        userAuthorizationHistoryRead.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationHistoryRead.setState(true);
        userAuthorizationHistoryRead.setOrder(16);
        userAuthorizations.add(userAuthorizationHistoryRead);

        // history change
        final UserAuthorization userAuthorizationHistoryChange = new UserAuthorization();
        userAuthorizationHistoryChange.setUserAuthorizationType(UserAuthorizationTypeEnum.HISTORY);
        userAuthorizationHistoryChange.setUserAuthorizationState(UserAuthorizationStateEnum.CHANGE);
        userAuthorizationHistoryChange.setState(true);
        userAuthorizationHistoryChange.setOrder(17);
        userAuthorizations.add(userAuthorizationHistoryChange);

        // history delete
        final UserAuthorization userAuthorizationHistoryDelete = new UserAuthorization();
        userAuthorizationHistoryDelete.setUserAuthorizationType(UserAuthorizationTypeEnum.HISTORY);
        userAuthorizationHistoryDelete.setUserAuthorizationState(UserAuthorizationStateEnum.DELETE);
        userAuthorizationHistoryDelete.setState(false);
        userAuthorizationHistoryDelete.setOrder(18);
        userAuthorizations.add(userAuthorizationHistoryDelete);

        // admin read
        final UserAuthorization userAuthorizationAdministrationRead = new UserAuthorization();
        userAuthorizationAdministrationRead.setUserAuthorizationType(UserAuthorizationTypeEnum.ADMINISTRATION);
        userAuthorizationAdministrationRead.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationAdministrationRead.setState(true);
        userAuthorizationAdministrationRead.setOrder(19);
        userAuthorizations.add(userAuthorizationAdministrationRead);

        // admin change
        final UserAuthorization userAuthorizationAdministrationChange = new UserAuthorization();
        userAuthorizationAdministrationChange.setUserAuthorizationType(UserAuthorizationTypeEnum.ADMINISTRATION);
        userAuthorizationAdministrationChange.setUserAuthorizationState(UserAuthorizationStateEnum.CHANGE);
        userAuthorizationAdministrationChange.setState(true);
        userAuthorizationAdministrationChange.setOrder(20);
        userAuthorizations.add(userAuthorizationAdministrationChange);

        // admin delete
        final UserAuthorization userAuthorizationAdministrationDelete = new UserAuthorization();
        userAuthorizationAdministrationDelete.setUserAuthorizationType(UserAuthorizationTypeEnum.ADMINISTRATION);
        userAuthorizationAdministrationDelete.setUserAuthorizationState(UserAuthorizationStateEnum.DELETE);
        userAuthorizationAdministrationDelete.setState(false);
        userAuthorizationAdministrationDelete.setOrder(21);
        userAuthorizations.add(userAuthorizationAdministrationDelete);

        // timeRegistration read
        final UserAuthorization userAuthorizationTimeRegistrationRead = new UserAuthorization();
        userAuthorizationTimeRegistrationRead.setUserAuthorizationType(UserAuthorizationTypeEnum.TIME_REGISTRATION);
        userAuthorizationTimeRegistrationRead.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationTimeRegistrationRead.setState(true);
        userAuthorizationTimeRegistrationRead.setOrder(22);
        userAuthorizations.add(userAuthorizationTimeRegistrationRead);

        // timeRegistration change
        final UserAuthorization userAuthorizationTimeRegistrationChange = new UserAuthorization();
        userAuthorizationTimeRegistrationChange.setUserAuthorizationType(UserAuthorizationTypeEnum.TIME_REGISTRATION);
        userAuthorizationTimeRegistrationChange.setUserAuthorizationState(UserAuthorizationStateEnum.CHANGE);
        userAuthorizationTimeRegistrationChange.setState(true);
        userAuthorizationTimeRegistrationChange.setOrder(23);
        userAuthorizations.add(userAuthorizationTimeRegistrationChange);

        // timeRegistration delete
        final UserAuthorization userAuthorizationTimeRegistrationDelete = new UserAuthorization();
        userAuthorizationTimeRegistrationDelete.setUserAuthorizationType(UserAuthorizationTypeEnum.TIME_REGISTRATION);
        userAuthorizationTimeRegistrationDelete.setUserAuthorizationState(UserAuthorizationStateEnum.DELETE);
        userAuthorizationTimeRegistrationDelete.setState(false);
        userAuthorizationTimeRegistrationDelete.setOrder(24);
        userAuthorizations.add(userAuthorizationTimeRegistrationDelete);

        userAuthorizationUserContainer.setUserAuthorizations(userAuthorizations);

        final UserAuthorizationUserContainer createdUserAuthorizationUserContainer = userAuthorizationUserContainerRepository
            .insert(userAuthorizationUserContainer);
        Assert.notNull(createdUserAuthorizationUserContainer);
    }

}
