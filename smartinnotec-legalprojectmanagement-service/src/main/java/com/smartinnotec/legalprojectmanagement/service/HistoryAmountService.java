package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryAmount;

public interface HistoryAmountService {

	HistoryAmount create(final HistoryAmount historyAmount) throws BusinessException;
	
	List<HistoryAmount> getHistoryAmountsBetweenDates(final DateTime start, final DateTime end) throws BusinessException;
	
}
