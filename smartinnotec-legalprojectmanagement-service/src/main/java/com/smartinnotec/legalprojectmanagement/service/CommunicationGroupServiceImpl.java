package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroup;
import com.smartinnotec.legalprojectmanagement.dao.repository.CommunicationGroupRepository;

@Service("messageGroupService")
public class CommunicationGroupServiceImpl extends AbstractService implements CommunicationGroupService {

    @Autowired
    private CommunicationGroupRepository messageGroupRepository;

    public CommunicationGroupServiceImpl() {
    }

    @Override
    public CommunicationGroup createCommunicationGroup(final CommunicationGroup messageGroup) throws BusinessException {
        BusinessAssert.notNull(messageGroup, "messageGroup is mandatory in MessageGroupServiceImpl#createMessageGroup", "400");
        logger.info("create messageGroup in MessageGroupServiceImpl#createMessageGroup");
        final CommunicationGroup createdMessageGroup = messageGroupRepository.insert(messageGroup);
        logger.debug("created messageGroup with id '{}' in MessageGroupServiceImpl#createMessageGroup", createdMessageGroup.getId());
        return createdMessageGroup;
    }

    @Override
    public CommunicationGroup findCommunicationGroup(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in MessageGroupServiceImpl#findMessageGroup", "400");
        BusinessAssert.isId(id, "id must be an id in MessageGroupServiceImpl#findMessageGroup", "400");
        logger.info("find messageGroup by id '{}' in MessageGroupServiceImpl#findMessageGroup", id);
        return messageGroupRepository.findOne(id);
    }

    @Override
    public CommunicationGroup updateCommunicationGroup(final CommunicationGroup messageGroup) throws BusinessException {
        BusinessAssert.notNull(messageGroup, "messageGroup is mandatory in MessageGroupServiceImpl#updateMessageGroup", "400");
        logger.info("update messageGroup with id '{}' in MessageGroupServiceImpl#updateMessageGroup", messageGroup.getId());
        return messageGroupRepository.save(messageGroup);
    }

    @Override
    public void deleteCommunicationGroup(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in MessageGroupServiceImpl#deleteMessageGroup", "400");
        BusinessAssert.isId(id, "id must be an id in MessageGroupServiceImpl#deleteMessageGroup", "400");
        logger.info("delete messageGroup of id '{}' in MessageGroupServiceImpl#deleteMessageGroup", id);
        messageGroupRepository.delete(id);
    }

    @Override
    public String toString() {
        return "[MessageGroupServiceImpl]";
    }
}
