package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactImportedRepository;

@Service("contactImportedService")
public class ContactImportedServiceImpl extends AbstractService implements ContactImportedService {

    @Autowired
    private ContactImportedRepository contactImportedRepository;
    @Autowired
    private AddressImportedService addressImportedService;

    @Override
    public List<ContactImported> findAll() throws BusinessException {
        logger.info("find all contactImported in ContactImportedServiceImpl#findAll");
        return contactImportedRepository.findAll();
    }

    @Override
    public List<ContactImported> findPagedContactsImported(final Integer page) throws BusinessException {
        logger.info("find paged contacts imported in ContactImportedServiceImpl#findPagedContactsImported");
        BusinessAssert.notNull(page, "page is mandatory in ContactImportedServiceImpl#findPagedContactsImported");
        final Direction sortDirection = Sort.Direction.ASC;
        final String sortingType = "institution";
        final Pageable pageable = new PageRequest(page, this.getContactImportedSize(), new Sort(sortDirection, sortingType));
        final List<ContactImported> pagedContactImported = contactImportedRepository.findAll(pageable).getContent();
        return pagedContactImported;
    }

    @Override
    public ContactImported findByCustomerNumberContainersIn(final CustomerNumberContainer customerNumberContainer)
            throws BusinessException {
        BusinessAssert.notNull(customerNumberContainer,
            "customerNumber is mandatory in ContactImportedServiceImpl#findByCustomerNumberContainersIn", "400");
        logger.info("find contact by customerNumber '{}' in ContactImportedServiceImpl#findByCustomerNumberContainersIn",
            customerNumberContainer);
        return contactImportedRepository.findByCustomerNumberContainersIn(customerNumberContainer);
    }

    @Override
    public ContactImported findById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ContactImportedServiceImpl#findById", "400");
        BusinessAssert.isId(id, "id must be an id in ContactImportedServiceImpl#findById", "400");
        logger.info("find contactImported by id '{}' in ContactImportedServiceImpl#findById", id);
        return contactImportedRepository.findOne(id);
    }

    @Override
    public Integer count() throws BusinessException {
        logger.info("count contactImported in ContactImportedServiceImpl#count");
        final Long amount = contactImportedRepository.count();
        return amount.intValue();
    }

    @Override
    public boolean hasContactImportedChanged(final ContactImported contactImported) throws BusinessException {
        BusinessAssert.notNull(contactImported, "contactImported is mandatory in ContactImportedServiceImpl#hasContactImportedChanged",
            "400");
        logger.info("has contactImported changed in ContactImportedServiceImpl#hasContactImportedChanged");
        if (contactImported.isInstitutionChanged()) {
            return true;
        }
        if (contactImported.isAdditionalNameInformationChanged()) {
            return true;
        }
        if (contactImported.isContactPersonChanged()) {
            return true;
        }
        if (contactImported.isShortCodeChanged()) {
            return true;
        }
        if (contactImported.isCustomerNumberContainersChanged()) {
            return true;
        }
        if (contactImported.isAgentNumberContainersChanged()) {
            return true;
        }
        if (contactImported.isEmailsChanged()) {
            return true;
        }
        if (contactImported.isTelephonesChanged()) {
            return true;
        }
        if (contactImported.isInformationChanged()) {
            return true;
        }

        final AddressImported address = contactImported.getAddress();
        final boolean hasAddressImportedChanged = addressImportedService.hasAddressImportedChanged(address);
        if (hasAddressImportedChanged) {
            return true;
        }

        if (contactImported.getAddressesWithProducts() != null && !contactImported.getAddressesWithProducts().isEmpty()) {
            for (final ContactImportedAddressImported contactAddressWithProducts : contactImported.getAddressesWithProducts()) {
                final AddressImported addressImportedOfContactAddressWithProducts = contactAddressWithProducts.getAddress();
                if (addressImportedOfContactAddressWithProducts != null) {
                    final boolean hasAddressImportedOfContactAddressWithProductsChanged = addressImportedService
                        .hasAddressImportedChanged(addressImportedOfContactAddressWithProducts);
                    if (hasAddressImportedOfContactAddressWithProductsChanged) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public ContactImported create(final ContactImported contactImported) throws BusinessException {
        BusinessAssert.notNull(contactImported, "contactImported is mandatory in ContactImportedServiceImpl#create");
        logger.info("create contactImported in ContactImportedServiceImpl#create");
        final ContactImported createdContactImported = contactImportedRepository.insert(contactImported);
        return createdContactImported;
    }

    @Override
    public ContactImported update(final ContactImported contactImported) throws BusinessException {
        BusinessAssert.notNull(contactImported, "contactImported is mandatory in ContactImportedServiceImpl#update");
        logger.info("update contactImported with id '{}' in ContactImportedServiceImpl#update", contactImported.getId());
        final ContactImported updatedContactImported = contactImportedRepository.save(contactImported);
        return updatedContactImported;
    }

    @Override
    public void delete(final ContactImported contactImported) throws BusinessException {
        BusinessAssert.notNull(contactImported, "contactImported is mandatory in ContactImportedServiceImpl#delete");
        logger.info("delete contactImported with id '{}' in ContactImportedServiceImpl#delete", contactImported.getId());
        contactImportedRepository.delete(contactImported);
    }

    @Override
    public Integer getAmountOfContactsImported(final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in ContactImportedServiceImpl#getAmountOfContactsImported");
        logger.info("get amount of contacts imported in ContactImportedServiceImpl#getAmountOfContactsImported");
        final Long amountOfProducts = contactImportedRepository.count();
        return amountOfProducts.intValue();
    }

    @Override
    public void deleteAllContactsImported() throws BusinessException {
        logger.info("delete all contacsImported in ContactImportedServiceImpl#deleteAllContactsImported");
        contactImportedRepository.deleteAll();
    }
}
