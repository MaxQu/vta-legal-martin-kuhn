package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.service.container.ReportingContainer;

public interface ReportingService {

    List<ReportingContainer> findByUserId(final String userId) throws BusinessException;
}
