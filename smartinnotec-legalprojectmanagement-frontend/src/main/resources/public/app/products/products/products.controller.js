(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.controller('ProductsController', ProductsController);
    
    ProductsController.$inject = ['$scope', '$timeout', '$window', '$location', '$anchorScroll', 'currentUser', 'amountOfProducts', 'userAuthorizationUserContainer', 'productTypes', 'clickedProduct', 'unityTypes', 'productAggregateConditionTypes', 'productId', 'roles', 'usersOfProject', 'currentThinningPolymerDateTimeValue', 'productsService', 'yesNoDeleteProductModalService', 'changeProductPropertiesModalService', 'createReceipeModalService', 'editReceipeModalService', 'historyOfReceipeModalService', 'receipeService', 'editReceipeToMixtureModalService', 'createThinningModalService', 'editThinningModalService', 'historyOfThinningModalService', 'dateUtilityService', 'printingPDFService', 'contactsService', 'uploadService', 'thinningService', 'utilService', 'documentInformationModalService', 'documentFileService', 'projectTemplateService'];
       
    function ProductsController($scope, $timeout, $window, $location, $anchorScroll, currentUser, amountOfProducts, userAuthorizationUserContainer, productTypes, clickedProduct, unityTypes, productAggregateConditionTypes, productId, roles, usersOfProject, currentThinningPolymerDateTimeValue, productsService, yesNoDeleteProductModalService, changeProductPropertiesModalService, createReceipeModalService, editReceipeModalService, historyOfReceipeModalService, receipeService, editReceipeToMixtureModalService, createThinningModalService, editThinningModalService, historyOfThinningModalService, dateUtilityService, printingPDFService, contactsService, uploadService, thinningService, utilService, documentInformationModalService, documentFileService, projectTemplateService) {
	    $scope.vm = this;   
    	var vm = this;
    	
    	vm.products = [];
    	vm.clickedProduct = clickedProduct != null ? clickedProduct.data : null;
    	vm.currentUser = currentUser;
    	vm.userAuthorizationUserContainer = vm.currentUser.userAuthorizationUserContainer;
    	vm.productTypes = productTypes != null ? productTypes.data : null;
    	vm.amountOfProducts = amountOfProducts != null ? amountOfProducts.data : 0;
    	vm.unityTypes = unityTypes.data;
    	vm.productId = productId;
    	vm.productAggregateConditionTypes = productAggregateConditionTypes.data;
    	vm.roles = roles.data;
    	vm.usersOfProject = usersOfProject.data;
    	vm.showProductCreationTrigger = false;
    	vm.orderType = 'name';
    	vm.uploadFiles = false;
    	vm.searchProductTerm = null;
    	vm.searchProductOverDocumentsTerm = null;
    	vm.loadingProducts = false;
    	vm.reloadProducts = false;
    	vm.page = 0;
    	vm.files = [];
    	vm.successfulUploadedFiles = 0;
    	vm.failedUploadedFiles = 0;
    	vm.percentageResultIs100 = false;
    	vm.searchReceipesTrigger = false;
    	vm.product = {
    		number: '',
    		name: '',
			names: [],
			description: '',
			chemicalDescription: '',
			density: '',
			productFillingWeights: [],
			color: '#007ab9', 
			developmentDate: null,
			types: [],
			position: '',
			mixtureInstructions: null,
			contactsProduct: [],
			creationDate: null,
			creationUserId: ''
			};
    	vm.receipe = {
    		productId: '',
    		date: new Date(),
    		receipeProducts: [],
    		information: '',
    		receipeAttachments: [],
    		receipeType: null
    	};
    	vm.receipeToSearch = {
        		productId: '',
        		date: new Date(),
        		receipeProducts: [],
        		information: '',
        		receipeAttachments: [],
        		receipeType: null
        	};
    	vm.thinning = {
        		productId: '',
        		date: new Date(),
        		ironContent: null,
        		targetContent: null,
        		density: null,
        		amountLiter: null,
        		amount: null,
        		iron: null,
        		water: null,
        		polymer: false,
        		ascorbinSaeure: false,
        		polymerSubtract: 1000,
        		polymerPromille: currentThinningPolymerDateTimeValue.data.value,
        		ascorbinSaeureSubtract: 2700,
        		waterAmountCalculated: null,
        		informationTop: null,
        		information: null,
        		labelLeft1: 'Fe- Gehalt',
				labelLeft2: 'Fe- Gehalt',
				labelRight1: 'Eisenlösung'
        	};
    	vm.polymerPromille = currentThinningPolymerDateTimeValue.data.value;

    	vm.loadingPagedProducts = loadingPagedProducts;
    	vm.printContainer = printContainer;
    	vm.createProductNotifier = createProductNotifier;
    	vm.removeSearchResults = removeSearchResults;
    	vm.deleteProduct = deleteProduct;
    	vm.createReceipe = createReceipe;
    	vm.editAndDeleteReceipe = editAndDeleteReceipe;
    	vm.editReceipeToMixture = editReceipeToMixture;
    	vm.editAndDeleteMixture = editAndDeleteMixture;
    	vm.historyOfReceipe = historyOfReceipe;
    	vm.createThinning = createThinning;
    	vm.editAndDeleteThinning = editAndDeleteThinning;
    	vm.historyOfThinning = historyOfThinning;
    	vm.gotoTop = gotoTop;
    	vm.productClicked = productClicked;
    	vm.addReceipeProduct = addReceipeProduct;
    	vm.removeReceipeProduct = removeReceipeProduct;
    	vm.getProductName = getProductName;
    	vm.getProductsByTerm = getProductsByTerm;
    	vm.productSelected = productSelected;
    	vm.checkIfPercentageResultIs100 = checkIfPercentageResultIs100;
    	vm.searchProductsWithSameReceipe = searchProductsWithSameReceipe;
    	vm.showChangeProductPropertiesModalService = showChangeProductPropertiesModalService;
    	vm.showYesNoDeleteProductModal = showYesNoDeleteProductModal;
    	vm.showTemplateModal = showTemplateModal;
    	vm.createDocumentUrl = createDocumentUrl;
    	vm.updateDocumentFile = updateDocumentFile;
    	vm.hideAddressWithProducts = hideAddressWithProducts;
    	vm.searchProducts = searchProducts;
    	vm.searchProductOverDocumentFiles = searchProductOverDocumentFiles;
    	vm.showDocumentInformationModal = showDocumentInformationModal;
    	vm.receipeSearchDisabled = receipeSearchDisabled;
    	loadProducts(vm.page);
    	
    	//////////////
    	
    	$scope.$watch('vm.orderType', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			removeSearchResults();
    		} 
		});
    	
    	$scope.$watch('vm.files', function () {
    		if(vm.products != null && vm.files != null && vm.files.length > 0) {
	    		for(var i = 0; i < vm.products.length; i++) {
	    			uploadService.uploadFiles(vm.files, currentUser.id, vm.products[i].id, '', 'PRODUCT', successCallback, errorCallback, progressCallback);
	    		}
    		}
        });
    	
    	function successCallback(response) {
    		setFileUpdateTimeout();
    		vm.successfulUploadedFiles += 1;
    		for(var i = 0; i < vm.products.length; i++) {
    			if(vm.products[i].id == response.data.projectProductId) {
    				vm.products[i].uploadedDocumentFileSuccessful = true;
    				setUploadedDocumentFileSuccessfulTimeout(vm.products[i]);
    				break;
    			}
    		}
    	}
    	
    	function errorCallback(response) {
    		vm.failedUploadedFiles += 1;
    	}
    	
    	function progressCallback(log) {
    		vm.uploadFiles = true;
    		console.log('progressCallback: ' + log);
    	}
    	
    	function setTimeout() {
      		$timeout(function() {
      			vm.noLastReceipeExists = false;
  			   }, 2000); 
      	}
    	
    	function setFileUpdateTimeout() {
      		$timeout(function() {
      			vm.uploadFiles = false;
  			   }, 2500); 
      	}
    	
    	function hideAddressWithProducts(addressWithProducts) {
			var address = addressWithProducts.address;
			if(address == null) {
				return false;
			}
			if(address.userIdBlackList != null && address.userIdBlackList.indexOf(vm.currentUser.id) !== -1) {
				return true;
			}
			if(address.rolesBlackList != null && address.rolesBlackList.indexOf(vm.currentUser.projectUserConnectionRole) !== -1) {
				return true;
			}
			return false;
		}
    	
    	function setUploadedDocumentFileSuccessfulTimeout(product) {
      		$timeout(function() {
      			product.uploadedDocumentFileSuccessful = false;
  			   }, 8500); 
      	}
    	
    	function productClicked(product) {
    		return product.id;
    	}
    	
    	function searchProductOverDocumentFiles() {
    		if(vm.searchProductOverDocumentsTerm != null && vm.searchProductOverDocumentsTerm.length > 3) {
    			productsService.findProductsByDocumentFileSearchTerm(vm.searchProductOverDocumentsTerm).then(function(response) {
    				vm.products = response.data;
    				for(var i = 0; i < vm.products.length; i++) {
    					vm.products[i].uploadDocumentFile = true;
    					setContactOrUser(vm.products[i].documentFiles);
    				}
					vm.loadingProducts = false;
		    		vm.page = 0;
     	   		 }, function errorCallback(response) {
     	   			 console.log('error products.controller#searchProductOverDocumentFiles');
     	   		 });
    		}
    	}
    	
    	function searchProducts() {
			if(vm.searchProductTerm != null && vm.searchProductTerm.length > 2) {
				var searchTerm = utilService.prepareSearchTerm(vm.searchProductTerm);
				vm.loadingProducts = true;
				vm.orderType = 'name';
				productsService.findProductsByTerm(searchTerm).then(function(response) {
					vm.products = response.data;
					vm.loadingProducts = false;
		    		vm.page = 0;
	  	   		 }, function errorCallback(response) {
	  	   			 console.log('error products.controller#searchProductTerm');
	  	   		 });
			}
		}
    	
    	function removeSearchResults() { 
    		vm.searchProductTerm = null;
    		vm.searchProductOverDocumentsTerm = null;
    		vm.clickedProduct = null;
    		vm.reloadProducts = false;
    		vm.searchReceipesTrigger = false;
    		resetReceipeToSearch();
    		vm.products = [];
    		vm.page = 0;
    		loadProducts(vm.page);
    	}
    	
    	function getAmountOfProducts() {
    		productsService.getAmountOfProducts().then(function(response) {
    			vm.amountOfProducts = response.data;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error products.controller#getAmountOfProducts');
  	   		 });
    	}
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function loadingPagedProducts() {
    		if(vm.loadingProducts == false && vm.searchProductOverDocumentsTerm == null && vm.searchProductTerm == null && vm.searchReceipesTrigger == false) {
    			vm.loadingProducts = true;
	    		vm.page = vm.page + 1;
	    		loadProducts(vm.page);
	    		vm.loadingProducts = false;
    		}
    	}
    	
    	function loadProducts(page) {
    		if(vm.clickedProduct != null) {
    			if(vm.products == null) {
    				vm.products = [];
    			}
    			for(var i = 0; i < vm.products.length; i++) {
    				if(vm.products[i].id == vm.clickedProduct.id) {
    					return;
    				}
    			}
    			vm.products.push(vm.clickedProduct); 
    		} else if(vm.amountOfProducts > vm.products.length) {
    			if(vm.products == null) {
    				vm.products = [];
    			}
	    		productsService.getPagedProductsOfTenant(page, vm.orderType).then(function(response) {
	    			var loadedProducts = response.data;
	    			if(loadedProducts != null && loadedProducts.length > 0) {
	    				vm.products.push.apply(vm.products, loadedProducts);
	    				for(var k = 0; k < loadedProducts.length; k++) {
	    					if(loadedProducts[k].id == vm.productId) {
	    						loadedProducts[k].selected = true;
	    						break;
	    					}
	    				}
		    			console.log("push products:" + vm.products.length + ', ' + loadedProducts.length);
	    			}
	  	   		 }, function errorCallback(response) {
	  	   			 console.log('error products.controller#loadProducts');
	  	   		 });
    		}
    	}
    	
    	function createProductNotifier() {
    		getAmountOfProducts();
    		vm.reloadProducts = true;
    	}
    	
    	function deleteProduct(product) {
    		productsService.deleteProduct(product.id).then(function(response) {
    			removeProduct(product);
    			getAmountOfProducts();
    			vm.reloadProducts = true;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error products.controller#deleteProduct');
  	   		 });
    	}
    	
    	function removeProduct(product) {
    		for(var i = 0; i < vm.products.length; i++) {
    			if(vm.products[i].id == product.id) {
    				vm.products.splice(i, 1);
    				break;
    			}
    		}
    	}
    	
    	function createReceipe(product, receipeType) {
    		receipeService.findAllReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var allReceipesOfProduct = response.data;
    			createReceipeModalService.showCreateReceipeModal(vm.currentUser, product, vm.products, vm.receipe, allReceipesOfProduct, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#createReceipe');
 	   		 });
    	}
    	
    	function editAndDeleteReceipe(product, receipeType) {
    		receipeService.findAllReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var receipesOfProduct = response.data;
    			var start = receipesOfProduct != null && receipesOfProduct.length > 0 ? receipesOfProduct[receipesOfProduct.length-1].date : new Date();
    			var end = dateUtilityService.formatDateToString(new Date());
    			editReceipeModalService.showEditReceipeModal(vm.currentUser, product, vm.products, receipesOfProduct, receipeType, start, end, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#editAndDeleteReceipe');
 	   		 });
    	}
    	
    	function editReceipeToMixture(product, receipeType, $event) {  
    		// get last standard receipe of product
    		$event.stopPropagation();
    		receipeService.findLastReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var lastReceipeOfProduct = response.data;
    			if(lastReceipeOfProduct == '') {
    				vm.noLastReceipeExists = true;
    				setTimeout();
    			} else {
    				lastReceipeOfProduct.date = new Date();
    				editReceipeToMixtureModalService.showEditReceipeToMixtureModal(vm.currentUser, product, vm.products, lastReceipeOfProduct, vm.userAuthorizationUserContainer, vm.unityTypes);
    			}
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#editReceipeToMixture');
 	   		 });
    	}
    	
    	function createThinning(product, $event) { 
    		thinningService.findAllThinningsOfProduct(product.id).then(function(response) {
    			var allThinningsOfProduct = response.data;
    			createThinningModalService.showCreateThinningModal(vm.currentUser, product, vm.products, vm.thinning, allThinningsOfProduct, vm.userAuthorizationUserContainer, vm.polymerPromille);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#createReceipe');
 	   		 });
    	}
    	
    	function editAndDeleteThinning(product) { 
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		thinningService.findThinningsOfProductInRange(product.id, startDateString, endDateString).then(function(response) {
    			var thinningsOfProduct = response.data;
    			editThinningModalService.showEditThinningModal(vm.currentUser, product, vm.products, thinningsOfProduct, start, end, vm.userAuthorizationUserContainer, vm.polymerPromille);
    		}, function errorCallback(response) {
	   			 console.log('error products.controller#editAndDeleteThinning');
	   		 });
    	}
    	
    	function historyOfThinning(product) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		thinningService.findThinningsOfProductInRange(product.id, startDateString, endDateString).then(function(response) {
    			var thinningsOfProduct = response.data;
    			historyOfThinningModalService.showHistoryOfThinningModal(vm.currentUser, product, thinningsOfProduct, start, end, vm.polymerPromille);
    		}, function errorCallback(response) {
	   			 console.log('error products.controller#historyOfThinning');
	   		 });
    	}
    	
    	function historyOfReceipe(product, receipeType) {
    		if(receipeType == 'STANDARD_MIXTURE') {
	    		receipeService.findAllReceipesOfProductOfType(product.id, receipeType).then(function(response) {
	    			var receipesOfProduct = response.data;
	    			var start = receipesOfProduct != null && receipesOfProduct.length > 0 ? receipesOfProduct[receipesOfProduct.length-1].date : new Date();
	    			var end = dateUtilityService.formatDateToString(new Date());
	    			historyOfReceipeModalService.showHistoryOfReceipeModal(vm.currentUser, receipeType, product, receipesOfProduct, start, end);
	 	   		 }, function errorCallback(response) {
	 	   			 console.log('error products.controller#historyOfReceipe');
	 	   		 });
    		} else if(receipeType == 'CHANGED_MIXTURE') {
    			var end = new Date();
        		var start = dateUtilityService.oneMonthBack(end);
        		var startDateString = dateUtilityService.formatDateToString(start);
        		var endDateString = dateUtilityService.formatDateToString(end);
        		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
        			var receipesOfProduct = response.data;
        			historyOfReceipeModalService.showHistoryOfReceipeModal(vm.currentUser, receipeType, product, receipesOfProduct, start, end);
     	   		 }, function errorCallback(response) {
     	   			 console.log('error products.controller#historyOfReceipe');
     	   		 });
    		}
    	}
    	
    	function editAndDeleteMixture(product, receipeType, $event) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
    			var receipesOfProduct = response.data;
    			editReceipeModalService.showEditReceipeModal(vm.currentUser, product, vm.products, receipesOfProduct, receipeType, start, end, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#editAndDeleteMixture');
 	   		 });
    	}
    	
    	function showChangeProductPropertiesModalService(product) {
    		changeProductPropertiesModalService.showChangeProductPropertiesModal(product, vm.products, vm.productTypes, vm.currentUser, vm.productAggregateConditionTypes, vm.roles, vm.usersOfProject);
    	}
    	
    	function showYesNoDeleteProductModal(product) {
    		contactsService.getContactsReferencedToProduct(product.id).then(function(response) {
    			var contactsReferencedToProduct = response.data;
    			yesNoDeleteProductModalService.showYesNoModal(product, vm, contactsReferencedToProduct);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#showYesNoDeleteProductModal');
 	   		 });
    	}
    	
    	function setContactOrUser(pagedDocumentFiles) {
    		for(var i = 0; i < pagedDocumentFiles.length; i++) {
    			for(var j = 0; j < pagedDocumentFiles[i].documentFileVersions.length; j++) {
    				if(pagedDocumentFiles[i].documentFileVersions[j].responsibleUser != null) {
    					pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = pagedDocumentFiles[i].documentFileVersions[j].responsibleUser;
        			} else if(pagedDocumentFiles[i].documentFileVersions[j].responsibleContact != null) {
        				pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = pagedDocumentFiles[i].documentFileVersions[j].responsibleContact;
        			} else if(pagedDocumentFiles[i].documentFileVersions[j].responsibleText != null) {
        				pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact  = pagedDocumentFiles[i].documentFileVersions[j].responsibleText;
        			}
    			}
    		}
    		return pagedDocumentFiles;
    	}
    	
    	function addReceipeProduct() { 
    		var receipeProduct = {}; 
    		vm.receipeToSearch.receipeProducts.push(receipeProduct);
    	}
    	
    	function resetReceipeToSearch() {
    		vm.receipeToSearch = {
            		productId: '',
            		date: new Date(),
            		receipeProducts: [],
            		information: '',
            		receipeAttachments: [],
            		receipeType: null
            	};
    	}
    	
    	function removeReceipeProduct(index) {
    		vm.receipeToSearch.receipeProducts.splice(index, 1);
    		for(var i = 0; i < vm.receipeToSearch.receipeProducts.length; i++) {
    			vm.receipeToSearch.receipeProducts[i].selectedReceipeProduct = getProductName(vm.receipeToSearch.receipeProducts[i]);
    		}
    		vm.checkIfPercentageResultIs100(vm.receipeToSearch.receipeProducts);
    	}
    	
    	function getProductName(receipeProduct) { 
			if(receipeProduct == null || receipeProduct.product == null) { 
				return '';
			}
			return receipeProduct.product.name;
		}
    	
    	function getProductsByTerm(term) {
			return productsService.findProductsByTerm(term).then(function(response) {
	    		return response.data;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error products.controller#getProductsByTerm'); 
  	   		 });
		}
		
		function productSelected(selectedReceipeProduct, index) {
			var addedProduct = false;
    		for(var i = 0; i < vm.receipeToSearch.receipeProducts.length; i++) {
    			if(Object.keys(vm.receipeToSearch.receipeProducts[i]).length === 0) {
    				var receipeProduct = {};
    				receipeProduct.product = selectedReceipeProduct;
    				receipeProduct.percentage = null;
    				vm.receipeToSearch.receipeProducts[i] = receipeProduct;
    				addedProduct = true;
    				break;
    			}
    		}
    		if(addedProduct == false) {
    			vm.receipeToSearch.receipeProducts[index].product = selectedReceipeProduct;
    		}
    	}
		
		function checkIfPercentageResultIs100(receipeProducts) {
    		if(receipeProducts == null) { 
    			receipeProducts.percentageResult = 0;
    			vm.percentageResultIs100 = false;
    			return;
    		} 
    		var result = 0;
    		for(var i = 0; i < receipeProducts.length; i++) {
    			if(receipeProducts[i].percentage != null) {
    				result += receipeProducts[i].percentage;
    				receipeProducts[i].errorPercentage = false;
    			}
    		}
    		var resultInteger = parseFloat(result).toFixed(2);
    		if(resultInteger == parseFloat(100).toFixed(2)) {
    			receipeProducts.percentageResult = resultInteger;
    			vm.percentageResultIs100 = true;
    			return;
    		}
    		receipeProducts.percentageResult = resultInteger;
    		vm.percentageResultIs100 = false;
    	}
		
		function searchProductsWithSameReceipe() {
			productsService.getProductsWithSameReceipe(vm.receipeToSearch).then(function(response) { 
				vm.products = response.data;
			}, function errorCallback(response) {
 	   			  console.log('error products.controller.js#searchProductsWithSameReceipe');
 	   		 });
		}
    	
    	function gotoTop() {
    		 $location.hash('top'); 
    	     $anchorScroll();
    	}
    	
    	function showTemplateModal(url, documentFile, documentFileVersion) {
    		projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
    	}
    	
    	function updateDocumentFile(documentFile, updateDocumentFileResponsibility) {
    		documentFileService.updateDocumentFile(documentFile, documentFile.projectProductId).then(function(response) {
    			console.log('updated products in search.controller#updateDocumentFile');
  	   		 }, function errorCallback(response) {
  	   			  console.log('error products.controller.js#updateDocumentFile');
  	   		 });
    	} 
    	
    	function createDocumentUrl(user, projectId, documentFile, documentFileVersion) {
    		if(documentFile == null || documentFileVersion == null) {
    			return '';
    		}
    		var version = documentFileVersion === undefined ? '_Version1' : documentFileVersion.version;
    		return 'api/filedownloads/filedownload/' + user.id + '/' + documentFile.id + '/' + version + '/';
    	}
    	
    	function showDocumentInformationModal(documentFile, userAuthorizationUserContainer) {
    		documentInformationModalService.showDocumentInformationModal(documentFile, currentUser, userAuthorizationUserContainer, vm.type, vm, false);
    	}
    	
    	function receipeSearchDisabled() {
    		if(vm.receipeToSearch.receipeProducts.length == 0) {
    			return true;
    		}
    		for(var i = 0; i < vm.receipeToSearch.receipeProducts.length; i++) {
    			if(vm.receipeToSearch.receipeProducts[i].percentage == null || vm.receipeToSearch.receipeProducts[i].product == null) {
    				return true;
    			}
    		}
    		return false;
    	}
	} 
})();