package com.smartinnotec.legalprojectmanagement.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteConcern;
import com.smartinnotec.legalprojectmanagement.dao.repository.RepositoryPackage;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Configuration
@EnableMongoRepositories(basePackageClasses = RepositoryPackage.class)
@EnableConfigurationProperties
@ConfigurationProperties(locations = "classpath:application.properties", ignoreUnknownFields = true, prefix = "legalprojectmanagement.mongodb")
@EqualsAndHashCode(callSuper = false)
public @Data class SmartinnotecSpringMongoDBConfig extends AbstractMongoConfiguration {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private String databaseName;
    private String databaseAddress;
    private int databasePort;
    private String username;
    private String password;

    @Override
    @Bean
    public MongoClient mongo() throws Exception {

        String mongoDBUri = null;
        if (username != null && !username.isEmpty() && password != null && !password.isEmpty()) {
            mongoDBUri = "mongodb://" + username + ":" + password + "@" + databaseAddress + ":" + databasePort + "/" + databaseName;
        } else {
            mongoDBUri = "mongodb://" + databaseAddress + ":" + databasePort + "/" + databaseName;
        }

        final MongoClientURI mongoClientURI = new MongoClientURI(mongoDBUri);
        final MongoClient client = new MongoClient(mongoClientURI);
        client.setWriteConcern(WriteConcern.SAFE);
        return client;
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory(), mongoConverter());
    }

    @Bean
    public MongoDbFactory mongoDbFactory() throws Exception {
        return new SimpleMongoDbFactory(mongo(), getDatabaseName());
    }

    @Bean
    public MappingMongoConverter mongoConverter() throws Exception {
        final MongoMappingContext mappingContext = new MongoMappingContext();
        final DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
        final MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver, mappingContext);
        mongoConverter.setCustomConversions(customConversions());
        return mongoConverter;
    }

    @Override
    public String toString() {
        return "[SmartinnotecSpringMongoDBConfig]";
    }
}
