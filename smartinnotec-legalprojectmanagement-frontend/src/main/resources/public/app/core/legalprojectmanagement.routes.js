(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement')
    	.config(configure)
    	.run(['$state', function ($state) {}]);
    
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];
    
    function configure($stateProvider, $urlRouterProvider) {

		console.log("configure");

    	$urlRouterProvider.otherwise('/signin');
    	
    	$stateProvider
			.state(getAuthState());
    	
		////////////
			
		function getAuthState() {
			var state = {
				name: 'auth',
    			abstract: true,
    			views: {
    				'header': {
    					controller: 'NavController',
    					controllerAs: 'vm',
    					templateUrl: 'app/nav/nav.html'
    				},
    				'content': {
    					templateUrl: 'app/templates/content.html'
    				},
    				'footer': {
    					templateUrl: 'app/templates/footer.html'
    				}
    			},
				resolve: {
					authUserService: 'authUserService',	
					userService: 'userService',
					currentUser: function (authUserService) {
						return authUserService.getCurrentUser();
					}
				}
    		};
    		return state;
    	}
	}
})();
