package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.Data;

public @Data class ContactProductAddressAndOrderConstruction {

    @DBRef
    private Address address;
    private String orderConstruction;

}
