(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('noTemplateModalService', noTemplateModalService);
        
    noTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function noTemplateModalService($modal, $stateParams) {
		var service = {
			showNoTemplateModal: showNoTemplateModal
		};		
		return service;
		
		////////////
				
		function showNoTemplateModal(url, documentName, ending, version) {
			 var noTemplateModal = $modal.open({
				controller: NoTemplateModalController,
			    controllerAs: 'vm',
			    size: 'md',
			    windowClass: "modal fade in",
			    resolve: {
			    	url: function() {
			    		return url;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/noTemplateModal/noTemplate.modal.html'
			});
			return noTemplateModal;
			
			function NoTemplateModalController($modalInstance, $scope, url, documentName, ending, version) {
		    	var vm = this; 
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;	
		    	
		    	vm.closeNoTemplateModal = closeNoTemplateModal;
		    	
		    	////////////
		    	
		    	function closeNoTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();