package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class CommunicationAttachedDocumentFile {

    private String projectId;
    private String documentFileId;
    private String fileName;
    private String url;
    private String version;
    private boolean isImage;

}
