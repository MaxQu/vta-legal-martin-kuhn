package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "ThinningPolymerDateTimeValue")
public @Data class ThinningPolymerDateTimeValue implements Comparable<ThinningPolymerDateTimeValue> {

    @Id
    private String id;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime dateTime;
    private String value;

    @Override
    public int compareTo(final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue) {
        return this.dateTime.compareTo(thinningPolymerDateTimeValue.getDateTime());
    }
}
