package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.index.TextIndexed;

import lombok.Data;

public @Data class DeliveryNumberContainer {

    @TextIndexed
    private String contactTenant;
    @TextIndexed
    private String deliveryNumber;

    public String toString() {
        return "[" + contactTenant + " " + deliveryNumber + "]";
    }
}
