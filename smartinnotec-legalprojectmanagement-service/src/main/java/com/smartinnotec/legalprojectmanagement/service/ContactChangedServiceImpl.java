package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChangedAddressChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChangedTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactChangedRepository;

@Service("contactChangedService")
public class ContactChangedServiceImpl extends AbstractService implements ContactChangedService {

    @Autowired
    private ContactChangedRepository contactChangedRepository;
    @Autowired
    private AddressChangedService addressChangedService;
    @Autowired
    private HistoryService historyService;

    @Override
    public List<ContactChanged> findPagedContactsChanged(final Integer page, final Boolean committed) throws BusinessException {
        logger.info("find paged contacts changed in ContactImportedServiceImpl#findPagedContactsImported");
        BusinessAssert.notNull(page, "page is mandatory in ContactImportedServiceImpl#findPagedContactsImported");
        final Direction sortDirection = Sort.Direction.ASC;
        final String sortingType = "institution";
        final Pageable pageable = new PageRequest(page, this.getContactToExportSize(), new Sort(sortDirection, sortingType));
        final List<ContactChanged> pagedContactsChanged = contactChangedRepository.findPagedContactsChangedByCommitted(committed, pageable);
        return pagedContactsChanged;
    }

    @Override
    public Integer getAmountOfContactsChanged(final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in ContactChangedServiceImpl#getAmountOfContactsChanged", "400");
        logger.info("get amount of contacts changed of tenant with id '{}' in ContactChangedServiceImpl#getAmountOfContactsChanged",
            tenant.getId());
        final Long amount = contactChangedRepository.count();
        return amount.intValue();
    }

    @Override
    public Integer getAmountOfContactsChangedByCommitted(final Tenant tenant, final Boolean committed) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in ContactChangedServiceImpl#getAmountOfContactsChangedByCommitted", "400");
        logger.info(
            "get amount of contacts changed of tenant with id '{}' in ContactChangedServiceImpl#getAmountOfContactsChangedByCommitted",
            tenant.getId());
        return contactChangedRepository.countByTenantAndCommitted(tenant, committed);
    }

    @Override
    public ContactChanged findById(final String id) throws BusinessException {
        logger.info("find contact changed by id '{}' in ContactChangedServiceImpl#getAllContactsChanged", id);
        return contactChangedRepository.findOne(id);
    }

    @Override
    public List<ContactChanged> getAllContactsChanged() throws BusinessException {
        logger.info("get all contacts changed in ContactChangedServiceImpl#getAllContactsChanged");
        final List<ContactChanged> contactsChanged = contactChangedRepository.findAll();
        return contactsChanged;
    }

    @Override
    public List<ContactChanged> getContactsChangedByCommitted(final boolean committed) throws BusinessException {
        logger.info("get contacts changed by committed '{}' in ContactChangedServiceImpl#getContactsChangedByCommitted", committed);
        final List<ContactChanged> contactsChanged = contactChangedRepository.findContactsChangedByCommitted(committed);
        return contactsChanged;
    }

    @Override
    public ContactChanged convertFromContactToContactChanged(final Contact contact, final ContactChangedTypeEnum contactChangedType)
            throws BusinessException {
        logger.info("convert from contact with id '{}' to contactChanged in ContactChangedServiceImpl#convertFromContactToContactChanged",
            contact.getId());

        final ContactChanged contactChanged = new ContactChanged();
        contactChanged.setNewContact(contact.isNewContact());
        contactChanged.setContactId(contact.getId());
        contactChanged.setCommitted(false);  
        contactChanged.setContactChangedType(contactChangedType);
        contactChanged.setInstitution(contact.getInstitution());
        contactChanged.setInstitutionChanged(contact.isInstitutionChanged());
        contactChanged.setAdditionalNameInformation(contact.getAdditionalNameInformation());
        contactChanged.setAdditionalNameInformationChanged(contact.isAdditionalNameInformationChanged());
        contactChanged.setContactPerson(contact.getContactPerson());
        contactChanged.setContactPersonChanged(contact.isContactPersonChanged());
        contactChanged.setShortCode(contact.getShortCode());
        contactChanged.setShortCodeChanged(contact.isShortCodeChanged());
        final List<CustomerNumberContainer> copiedCustomerNumberContainers = new ArrayList<>();
        final List<CustomerNumberContainer> customerNumberContainers = contact.getCustomerNumberContainers();
        if (customerNumberContainers != null) {
            for (final CustomerNumberContainer customerNumberContainer : customerNumberContainers) {
                final CustomerNumberContainer copiedCustomerNumberContainer = customerNumberContainer.deepCopy();
                copiedCustomerNumberContainers.add(copiedCustomerNumberContainer);
            }
        }
        contactChanged.setCustomerNumberContainers(copiedCustomerNumberContainers);
        contactChanged.setCustomerNumberContainersChanged(contact.isCustomerNumberContainersChanged());
        contactChanged.setTelephones(contact.getTelephones());
        contactChanged.setTelephonesChanged(contact.isTelephonesChanged());
        contactChanged.setEmails(contact.getEmails());
        contactChanged.setEmailsChanged(contact.isEmailsChanged());

        contactChanged.setInformation(contact.getInformation());
        contactChanged.setInformationChanged(contact.isInformationChanged());

        final List<AgentNumberContainer> copiedAgentNumberContainers = new ArrayList<>();
        final List<AgentNumberContainer> agentNumberContainers = contact.getAgentNumberContainers();
        if (agentNumberContainers != null) {
            for (final AgentNumberContainer agentNumberContainer : agentNumberContainers) {
                final AgentNumberContainer copiedAgentNumberContainer = agentNumberContainer.deepCopy();
                copiedAgentNumberContainers.add(copiedAgentNumberContainer);
            }
        }
        contactChanged.setAgentNumberContainers(copiedAgentNumberContainers);
        contactChanged.setAgentNumberContainersChanged(contact.isAgentNumberContainersChanged());

        final Address address = contact.getAddress();

        final AddressChanged addressChanged = addressChangedService.convertFromAddressToAddressChanged(address);
        contactChanged.setAddress(addressChanged);

        final List<ContactChangedAddressChanged> copiedContactsChangedAddressesChanged = new ArrayList<>();
        final List<ContactAddressWithProducts> addressesWithProducts = contact.getAddressesWithProducts();
        if (addressesWithProducts != null) {
            for (final ContactAddressWithProducts contactImportedAddressImported : addressesWithProducts) {
                final ContactChangedAddressChanged contactChangedAddressChanged = new ContactChangedAddressChanged();
                final Address addressImportedOfContactImportedAddressImported = contactImportedAddressImported.getAddress();
                final AddressChanged addressChangedOfAddressWithProduct = addressChangedService
                    .convertFromAddressToAddressChanged(addressImportedOfContactImportedAddressImported);
                contactChangedAddressChanged.setAddress(addressChangedOfAddressWithProduct);
                copiedContactsChangedAddressesChanged.add(contactChangedAddressChanged);
            }
        }
        contactChanged.setAddressesWithProducts(copiedContactsChangedAddressesChanged);
        contactChanged.setTenant(contact.getTenant());
        contactChanged.setContactImportedAddressesImportedChanged(contact.isAddressesWithProductsChanged());
        contactChanged.setCustomerSince(contact.getCustomerSince());

        return contactChanged;
    }

    @Override
    public ContactChanged create(final ContactChanged contactChanged, final User user) throws BusinessException {
        BusinessAssert.notNull(contactChanged, "contactChanged is mandatory in ContactChangedServiceImpl#create", "400");
        logger.info("create contactChanged in ContactChangedServiceImpl#create");

        final ContactChanged existingContactChanged = contactChangedRepository.findContactChangedByContactId(contactChanged.getContactId());
        if (existingContactChanged != null) {
            final AddressChanged createdAddressChanged = existingContactChanged.getAddress();
            if (createdAddressChanged != null) {
                addressChangedService.delete(createdAddressChanged);
            }
            if (existingContactChanged.getAddressesWithProducts() != null && !existingContactChanged.getAddressesWithProducts().isEmpty()) {
                for (final ContactChangedAddressChanged contactChangedAddressChanged : existingContactChanged.getAddressesWithProducts()) {
                    final AddressChanged addressChanged = contactChangedAddressChanged.getAddress();
                    if (addressChanged != null) {
                        addressChangedService.delete(addressChanged);
                    }
                }
            }
            contactChangedRepository.delete(existingContactChanged);
        }

        final AddressChanged createdAddressChanged = addressChangedService.create(contactChanged.getAddress());
        contactChanged.setAddress(createdAddressChanged);
        if (contactChanged.getAddressesWithProducts() != null && !contactChanged.getAddressesWithProducts().isEmpty()) {
            for (final ContactChangedAddressChanged contactChangedAddressChanged : contactChanged.getAddressesWithProducts()) {
                final AddressChanged addressChanged = contactChangedAddressChanged.getAddress();
                final AddressChanged addressChangedOfContactChangedAddressChanged = addressChangedService.create(addressChanged);
                contactChangedAddressChanged.setAddress(addressChangedOfContactChangedAddressChanged);
            }
        }
        final ContactChanged createdContactChanged = contactChangedRepository.insert(contactChanged);

        final String message = createdContactChanged.getInstitution();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.CREATED_CONTACT_CHANGED)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ContactChangedServiceImpl#create", createdHistory.getId());

        return createdContactChanged;
    }

    @Override
    public ContactChanged update(final ContactChanged contactChanged, final User user) throws BusinessException {
        BusinessAssert.notNull(contactChanged, "contactChanged is mandatory in ContactChangedServiceImpl#update", "400");
        logger.info("update contactChanged with id '{}' in ContactChangedServiceImpl#update", contactChanged.getId());
        final ContactChanged updatedContactChanged = contactChangedRepository.save(contactChanged);

        final String message = updatedContactChanged.getInstitution();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.UPDATED_CONTACT_CHANGED)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ContactChangedServiceImpl#update", createdHistory.getId());

        return updatedContactChanged;
    }

    @Override
    public void delete(final String id) throws BusinessException {
        logger.info("delete contact changed with id '{}' in ContactChangedServiceImpl#delete", id);
        final ContactChanged contactChanged = contactChangedRepository.findOne(id);
        if (contactChanged != null) {
            final AddressChanged addressChanged = contactChanged.getAddress();
            if (addressChanged != null) {
                addressChangedService.delete(addressChanged);
            }
            if (contactChanged.getAddressesWithProducts() != null && !contactChanged.getAddressesWithProducts().isEmpty()) {
                for (final ContactChangedAddressChanged contactChangedAddressChanged : contactChanged.getAddressesWithProducts()) {
                    final AddressChanged addressWithProductsChanged = contactChangedAddressChanged.getAddress();
                    if (addressWithProductsChanged != null) {
                        addressChangedService.delete(addressWithProductsChanged);
                    }
                }
            }

            contactChangedRepository.delete(contactChanged);
        }
    }

    @Override
    public void deleteAllCommittedContactsChanged() throws BusinessException {
        logger.info("delete all committed contact changed in ContactChangedServiceImpl#deleteAllCommittedContactsChanged");
        final List<ContactChanged> contactsChanged = contactChangedRepository.findContactsChangedByCommitted(true);
        for (final ContactChanged contactChanged : contactsChanged) {
            logger.info("deleted contactChanged with id '{}' in ContactChangedServiceImpl#deleteAllCommittedContactsChanged",
                contactChanged.getId());
            this.delete(contactChanged.getId());
        }
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all contacts changed in ContactChangedServiceImpl#deleteAll");
        contactChangedRepository.deleteAll();
    }

    public String toString() {
        return "[ContactChangedServiceImpl]";
    }
}
