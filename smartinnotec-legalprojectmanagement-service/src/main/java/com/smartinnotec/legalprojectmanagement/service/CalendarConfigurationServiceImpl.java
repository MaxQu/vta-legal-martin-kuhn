package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarConfiguration;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarConfigurationTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.repository.CalendarConfigurationRepository;

@Service("calendarConfigurationService")
public class CalendarConfigurationServiceImpl extends AbstractService implements CalendarConfigurationService {

    @Autowired
    private CalendarConfigurationRepository calendarConfigurationRepository;

    public CalendarConfigurationServiceImpl() {
    }

    @Override
    public String getCalendarConfigurationHeader() throws BusinessException {
        logger.info("get calendar configuration header string in CalendarConfigurationServiceImpl#getCalendarConfigurationHeader");
        final List<CalendarConfiguration> calendarConfigurationsHeader = calendarConfigurationRepository
            .findByCalendarConfigurationTypeOrderByOrderNumberAsc(CalendarConfigurationTypeEnum.HEADER);

        final StringBuilder calendarHeader = new StringBuilder();
        for (final CalendarConfiguration calendarConfiguration : calendarConfigurationsHeader) {
            calendarHeader.append(calendarConfiguration.getKey()).append(":").append(calendarConfiguration.getValue())
                .append(System.getProperty("line.separator"));
        }
        return calendarHeader.toString();
    }

    @Override
    public String getCalendarConfigurationMessage(final String projectName, final String name, final String title, final String location,
            final Long start, final Long end) throws BusinessException {
        logger.info("get calendar configuration message string in CalendarConfigurationServiceImpl#getCalendarConfigurationHeader");
        final List<CalendarConfiguration> calendarConfigurationsHeader = calendarConfigurationRepository
            .findByCalendarConfigurationTypeOrderByOrderNumberAsc(CalendarConfigurationTypeEnum.MESSAGE);

        final StringBuilder calendarHeader = new StringBuilder();
        final String created = dateTimeFormatterStandard1.print(new DateTime(System.currentTimeMillis()));
        for (final CalendarConfiguration calendarConfiguration : calendarConfigurationsHeader) {
            switch (calendarConfiguration.getKey()) {
            case "CREATED":
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(created)
                    .append(System.getProperty("line.separator"));
                break;
            case "DESCRIPTION":
                final String description = projectName + " " + name + " " + title;
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(description)
                    .append(System.getProperty("line.separator"));
                break;
            case "UID":
                final String uid = UUID.randomUUID() + "";
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(uid).append(System.getProperty("line.separator"));
                break;
            case "DTEND;TZID=\"W. Europe Standard Time\"":
                final DateTime endDateTime = new DateTime(end).withSecondOfMinute(0);
                final String endFormatted = dateTimeFormatterStandard2.print(endDateTime);
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(endFormatted)
                    .append(System.getProperty("line.separator"));
                break;
            case "DTSTAMP":
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(created)
                    .append(System.getProperty("line.separator"));
                break;
            case "DTSTART;TZID=\"W. Europe Standard Time\"":
                final DateTime startDateTime = new DateTime(start).withSecondOfMinute(0);
                final String dtend_tzid1 = dateTimeFormatterStandard2.print(new DateTime(startDateTime));
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(dtend_tzid1)
                    .append(System.getProperty("line.separator"));
                break;
            case "LAST-MODIFIED":
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(created)
                    .append(System.getProperty("line.separator"));
                break;
            case "LOCATION":
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(location)
                    .append(System.getProperty("line.separator"));
                break;
            case "SUMMARY;LANGUAGE=de-at":
                final String summary = projectName + " " + title;
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(summary)
                    .append(System.getProperty("line.separator"));
                break;
            default:
                calendarHeader.append(calendarConfiguration.getKey()).append(":").append(calendarConfiguration.getValue())
                    .append(System.getProperty("line.separator"));
            }
        }
        return calendarHeader.toString();
    }

    @Override
    public String getCalendarConfigurationHeaderEnd() throws BusinessException {
        logger.info("get calendar configuration header end string in CalendarConfigurationServiceImpl#getCalendarConfigurationHeader");
        final List<CalendarConfiguration> calendarConfigurationsHeader = calendarConfigurationRepository
            .findByCalendarConfigurationTypeOrderByOrderNumberAsc(CalendarConfigurationTypeEnum.HEADER_END);

        final StringBuilder calendarHeader = new StringBuilder();
        for (final CalendarConfiguration calendarConfiguration : calendarConfigurationsHeader) {
            calendarHeader.append(calendarConfiguration.getKey()).append(":").append(calendarConfiguration.getValue());
        }
        return calendarHeader.toString();
    }
}
