package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactProduct;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactProductAddressAndOrderConstruction;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;

public class ProductTest extends AbstractDAOTest {

    @Test
    public void shouldFindProductsWithAddressId() {
        final List<Product> products = productRepository.findAll();
        final List<String> addressIds = new ArrayList<>();
        final List<Product> foundedProductLocationAndOrderConstructions = new ArrayList<>();
        addressIds.add("59f746d0c2dc8211734e5ab9"); // 53
        // addressIds.add("5b47500dc2dc0371165522be"); // 0
        addressIds.add("5b486803c2dc0371165522eb"); // 1044
        // addressIds.add("5b83f5dbc2dc01e6d220933b"); // 0
        addressIds.add("5b843830c2dc01e6d22093a4"); // 1046
        addressIds.add("5b9f9164c2dc01e6d2209d74"); // 1045
        for (final Product product : products) {
            final List<ContactProduct> contactsProduct = product.getContactsProduct();
            if (contactsProduct != null) {
                for (final ContactProduct contactProduct : contactsProduct) {
                    final List<ContactProductAddressAndOrderConstruction> contactProductAddressesAndOrderConstruction = contactProduct
                        .getProductLocationAndOrderConstructions();
                    if (contactProductAddressesAndOrderConstruction != null) {
                        for (final ContactProductAddressAndOrderConstruction contactProductAddressAndOrderConstruction : contactProductAddressesAndOrderConstruction) {
                            final Address contactProductAddress = contactProductAddressAndOrderConstruction.getAddress();
                            if (addressIds.contains(contactProductAddress.getId())) {
                                foundedProductLocationAndOrderConstructions.add(product);
                            }
                        }
                    }

                }
            }
        }
        Assert.assertEquals(1066, foundedProductLocationAndOrderConstructions.size());
    }
}
