package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "ContactProduct")
public @Data class ContactProduct {

    @DBRef
    private Contact contact;
    private List<ContactProductAddressAndOrderConstruction> productLocationAndOrderConstructions;
}
