(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.error')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	$stateProvider
	    	.state(getErrorState());
    	
    	////////////
			    	
		function getErrorState() {
			var state = {
				name: 'error',
				params: {
					'error' : 'undefinied',
					'lastState' : 'undefinied'
				},
				url: '/error',
				views: {
					'header': {},
					'content': {
						controller: 'ErrorController',
						controllerAs: 'vm',
						templateUrl: 'app/error/error.html'
					},
					'footer': {}
				}
			};	
			return state;
		}
	}
})();