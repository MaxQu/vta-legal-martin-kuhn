(function() {
	'use strict';

	angular
		.module('legalprojectmanagement.user')
		.factory('changePasswordModalService', changePasswordModalService);

	changePasswordModalService.$inject = ['$modal', 'authUserService'];

	function changePasswordModalService($modal) {
		var service = {
			showChangePasswordModal: showChangePasswordModal
		};

		return service;

		////////////

		function showChangePasswordModal() {
			var changePasswordModal = $modal.open({
				controller: ChangePasswordModalController,
				controllerAs: 'vm',
				size: 'sm',
				templateUrl: '/app/user/profile/change-password/change-password-modal.html',
				windowClass: ''
			});
			return changePasswordModal;

			function ChangePasswordModalController($scope, $state, $modalInstance, authUserService) {
				$scope.vm = this;
				var vm = this;

				vm.changePassword = changePassword;

				////////////

				function changePassword(oldPassword, newPassword) {
					var changePasswordParams = {
						"oldPassword": oldPassword,
						"newPassword": newPassword
					};

					authUserService.changePassword(changePasswordParams).then(function () {
						$modalInstance.close();
						$state.go('home.signin');
					}).catch(function (response) {
						vm.changePasswordError = response.data;
					});
				}
			}
		}
	}
})();