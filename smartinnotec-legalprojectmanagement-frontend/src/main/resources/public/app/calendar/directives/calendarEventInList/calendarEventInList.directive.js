(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar') 
    	.directive('calendarEventInListDirective', calendarEventInListDirective);
    
    calendarEventInListDirective.$inject = [];
    
	function calendarEventInListDirective() {
		var directive = {
			restrict: 'E',
			scope: {
				calendarEvents: '='
			},
			templateUrl: 'app/calendar/directives/calendarEventInList/calendarEventInList.html',
			link: function($scope) {

			}
		};
		return directive;
		
		////////////
	}
})();
