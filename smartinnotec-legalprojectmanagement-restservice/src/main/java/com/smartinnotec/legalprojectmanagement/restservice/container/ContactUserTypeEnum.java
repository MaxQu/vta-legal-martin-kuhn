package com.smartinnotec.legalprojectmanagement.restservice.container;

public enum ContactUserTypeEnum {

    CONTACT("CONTACT"),
    USER("USER");

    private String value;

    private ContactUserTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
