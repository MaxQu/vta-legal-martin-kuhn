package com.smartinnotec.legalprojectmanagement.service;

import java.io.File;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.TimeRegistration;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;
import com.smartinnotec.legalprojectmanagement.dao.repository.ProjectRepository;

@Service("projectService")
public class ProjectServiceImpl extends AbstractService implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private DocumentFileService documentFileService;
    @Autowired
    private UserService userService;
    @Autowired
    private CalendarEventService calendarEventService;
    @Autowired
    private WorkingBookService workingBookService;
    @Autowired
    private CommunicationUserConnectionService communicationUserConnectionService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private TimeRegistrationService timeRegistrationService;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;

    @Override
	public Integer count() throws BusinessException {
    	logger.info("count projects in ProjectServiceImpl#count");
    	final Long projects = projectRepository.count();
		return projects.intValue();
	}
    
    @Override
    public Project findProject(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ProjectServiceImpl#findProject", "400");
        BusinessAssert.isId(id, "id must be an id in ProjectServiceImpl#findProject", "400");
        logger.info("find project with id '{}' in ProjectServiceImpl#findProject", id);
        return projectRepository.findOne(id);
    }

    @Override
    public List<Project> findAllProjectsByTenant(final Tenant tenant) throws BusinessException {
        logger.info("find all projects by tenant with id '{}' in ProjectServiceImpl#findAllProjectsByTenant", tenant.getId());
        return projectRepository.findByTenant(tenant);
    }

    @Override
    public List<Project> findAllGeneralAvailableProjects() throws BusinessException {
        logger.info("get all projects which are general available in ProjectServiceImpl#findAllGeneralAvailableProjects");
        final List<Project> projects = projectRepository.findByGeneralAvailable(true);
        return projects;
    }

    @Override
    public List<Project> findProjectByTerm(final String searchTerm) throws BusinessException {
        BusinessAssert.notNull(searchTerm, "searchTerm is mandatory in ProjectServiceImpl#findProjectByTerm", "400");
        logger.info("find project by term '{}' in ProjectServiceImpl#findProjectByTerm", searchTerm);
        return projectRepository.findProjectBySearchString(searchTerm);
    }

    @Override
    public Project create(final Project project) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in ProjectServiceImpl#create", "400");
        logger.info("create project with name '{}' in  ProjectServiceImpl#create", project.getName());
        final Project createdProject = projectRepository.insert(project);
        logger.debug("created project with id '{}' in  ProjectServiceImpl#create", createdProject.getId());
        return createdProject;
    }

    @Override
    public Project update(final Project project) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in ProjectServiceImpl#update", "400");
        logger.info("save project with id '{}' in ProjectServiceImpl#update", project.getId());
        return projectRepository.save(project);
    }

    @Override
    public Integer getProjectPageSize() {
        return super.getProjectPageSize();
    }

    @Override
    public boolean checkIfProjectExists(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ProjectServiceImpl#checkIfProjectExists", "400");
        BusinessAssert.isId(id, "id must be an id in ProjectServiceImpl#checkIfProjectExists", "400");
        logger.info("check if project with id '{}' exists in ProjectServiceImpl#checkIfProjectExist", id);
        return projectRepository.countById(id) > 0;
    }

    @Override
    public void deleteProject(final String projectId, final String userId, final String username) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in ProjectServiceImpl#deleteProject", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in ProjectServiceImpl#deleteProject", "400");
        logger.info("delete project with id '{}' in ProjectServiceImpl#deleteProject", projectId);
        final Project project = projectRepository.findOne(projectId);

        final List<DocumentFile> documentFiles = documentFileService.findByProjectId(projectId);
        if (documentFiles != null && documentFiles.size() > 0) {
            logger.info(
                "project with id '{}' containing '{}' document files and therefore can not be deleted in ProjectServiceImpl#deleteProject",
                project.getId(), documentFiles.size());
            throw new BusinessException("REFERENCE_TO_DOCUMENTFILES", "400");
        }

        final List<CalendarEvent> calendarEvents = calendarEventService.findByProject(project);
        if (calendarEvents != null && calendarEvents.size() > 0) {
            logger.info(
                "project with id '{}' has '{}' references to calendarEvents and therefore can not be deleted in ProjectServiceImpl#deleteProject",
                project.getId(), calendarEvents.size());
            throw new BusinessException("REFERENCE_TO_CALENDAREVENTS", "400");
        }

        final List<WorkingBook> workingBooks = workingBookService.findByProject(project);
        if (workingBooks != null && workingBooks.size() > 0) {
            logger.info(
                "project with id '{}' has '{}' references to workingBooks and therefore can not be deleted in ProjectServiceImpl#deleteProject",
                project.getId(), workingBooks.size());
            throw new BusinessException("REFERENCE_TO_WORKINGBOOKENTRIES", "400");
        }

        final List<CommunicationUserConnection> communicationUserConnections = communicationUserConnectionService
            .findCommunicationUserConnectionByProject(project);
        if (communicationUserConnections != null && communicationUserConnections.size() > 0) {
            logger.info(
                "project with id '{}' has '{}' references to communicationUserConnections and therefore can not be deleted in ProjectServiceImpl#deleteProject",
                project.getId(), communicationUserConnections.size());
            throw new BusinessException("REFERENCE_TO_COMMUNICATIONUSERCONNECTIONS", "400");
        }

        final List<TimeRegistration> timeRegistrations = timeRegistrationService.findTimeRegistrationByProject(project);
        if (timeRegistrations != null && timeRegistrations.size() > 0) {
            logger.info(
                "project with id '{}' has '{}' references to timeRegistrations and therefore can not be deleted in ProjectServiceImpl#deleteProject",
                project.getId(), communicationUserConnections.size());
            throw new BusinessException("REFERENCE_TO_TIMEREGISTRATIONS", "400");
        }

        final User user = userService.findUser(userId);

        // delete projectUserConnection
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByProjectAndActive(project, true);
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            projectUserConnectionService.delete(projectUserConnection.getId(), user);
        }
        renameFolder(projectId, username);
        // delete project
        projectRepository.delete(projectId);
        logger.debug("deleted project with id '{}' in ProjectServiceImpl#deleteProject", projectId);

        final String message = project.getName();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DELETED_PROJECT)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ProjectServiceImpl#deleteProject", createdHistory.getId());
    }

    @Override
    public boolean renameFolder(final String projectId, final String username) throws BusinessException {
        // add deleted, date and username to folder deleted in database
        final Project project = this.findProject(projectId);
        final String projectName = project.getName();

        final DateTime deletionDate = new DateTime(System.currentTimeMillis());
        final String deletePostFix = "#Del#" + username + "#" + dateTimeFormatter2.print(deletionDate);
        final File currentFolder = new File(uploadPath + File.separator + projectName);
        final File deletedFolder = new File(uploadPath + File.separator + projectName + deletePostFix);
        if (currentFolder.isDirectory()) {
            currentFolder.renameTo(deletedFolder);
            logger.info("folder '{}' renamed to '{}' in ProjectServiceImpl#renameFolder", currentFolder.getAbsolutePath(),
                deletedFolder.getAbsolutePath());
        } else {
            logger.error("path '{}' is not a folder in ProjectServiceImpl#renameFolder", currentFolder.getAbsolutePath());
        }
        return true;
    }

    @Override
    public String toString() {
        return "[ProjectServiceImpl]";
    }
}
