package com.smartinnotec.legalprojectmanagement.restservice;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.*;
import com.smartinnotec.legalprojectmanagement.service.FuelService;
import com.smartinnotec.legalprojectmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FuelRestService extends AbstractRestService {

    @Autowired
    private FuelService fuelService;

    @Autowired
    private UserService userService;

    private void checkBusinessRulesForFuel(Fuel fuel) throws BusinessException {
        BusinessAssert.notNull(fuel, "fuel is mandatory", BAD_REQUEST_CODE);
    }

    @RequestMapping(value = "/fuel", method = {RequestMethod.POST}, consumes = "application/json;charset=utf-8")
    public Fuel save(final @RequestBody Fuel fuel) throws BusinessException {

        checkBusinessRulesForFuel(fuel);

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        // set tenant of created fuel
        final Tenant tenant = userService.getTenantOfUser(userId);
        fuel.setTenant(tenant);
        fuel.setUser(user);
        Fuel createdFuel = fuelService.save(fuel);

        logger.debug("created contact with id '{}' in FuelRestService#create", createdFuel.getId());
        return createdFuel;
    }

    @RequestMapping(value = "/fuel/{fuelId}", method = RequestMethod.DELETE)
    public void deleteFuel(final @PathVariable(value = "fuelId") String fuelId) throws BusinessException {
        BusinessAssert.notNull(fuelId, "fuelId is mandatory in FuelRestService#deleteFuel", BAD_REQUEST_CODE);
        BusinessAssert.isId(fuelId, "fuelId must be an id in FuelRestService#deleteFuel", BAD_REQUEST_CODE);
        logger.info(getInvokedRestServiceUserInformation() + ": delete fuel with id '{}' in FuelRestService#deleteFuel",
                fuelId);

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        fuelService.deleteFuel(user, fuelId);

    }


    @RequestMapping(value = "/fuel", method = {RequestMethod.GET})
    public List<Fuel> findAll() throws BusinessException {
        final Tenant tenant = userService.getTenantOfUser(super.getLoggedInUserId());
        return fuelService.findAll(tenant);
    }

    @RequestMapping(value = "/fuel/page/{page}", method = {RequestMethod.GET}, produces = "application/json")
    public List<Fuel> findPagedFuels(final @PathVariable(value = "page") Integer page)
            throws BusinessException {
        BusinessAssert.notNull(page, "page is mandatory in ContactRestService#findPagedContacts", BAD_REQUEST_CODE);
        final String userId = super.getLoggedInUserId();
        final Tenant tenant = userService.getTenantOfUser(userId);
        return fuelService.findPagedFuelsByTenant(tenant, page);
    }

    @RequestMapping(value = "/fuel/amountofpages", method = {RequestMethod.GET})
    public Integer calculateAmountOfPages() throws BusinessException {
        final Tenant tenant = userService.getTenantOfUser(super.getLoggedInUserId());
        return fuelService.calculateAmountOfPages(tenant);
    }

    @RequestMapping(value = "/fuel/fuelpagesize", method = {RequestMethod.GET}, produces = "application/json")
    public Integer getContactPageSize() {
        return fuelService.getFuelPageSize();
    }

}
