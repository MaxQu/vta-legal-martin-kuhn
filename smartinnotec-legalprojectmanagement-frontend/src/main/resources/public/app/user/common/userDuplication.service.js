(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.factory('userDuplicationService', userDuplicationService);
        
    userDuplicationService.$inject = ['$http', 'api_config'];
    
    function userDuplicationService($http, api_config) {
		var service = {
			duplicateUser: duplicateUser
		};
		
		return service;
		
		////////////
		
		function duplicateUser(userIdFrom, userIdTo) {
			return $http.post(api_config.BASE_URL + '/userduplications/userduplication/' + userIdFrom + '/' + userIdTo);
		}
    }
})();