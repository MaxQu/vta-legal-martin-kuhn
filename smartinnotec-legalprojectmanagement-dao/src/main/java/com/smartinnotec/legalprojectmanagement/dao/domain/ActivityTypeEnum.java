package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ActivityTypeEnum {

    ROUTINE_VISITATION("ROUTINE_VISITATION", false),
    POLYMER("POLYMER", true),
    FAELLUNGS_MITTEL("FAELLUNGS_MITTEL", true),
    OTHER_CHEMICALS("OTHER_CHEMICALS", true),
    TECHNIQUE("TECHNIQUE", true),
    SERVICE("SERVICE", false),
    OTHERS("OTHERS", true);

    private String value;
    private boolean active;

    private ActivityTypeEnum(final String value, final boolean active) {
        this.value = value;
        this.active = active;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
