(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.directive('communicationCreateFormDirective', communicationCreateFormDirective);
    
    communicationCreateFormDirective.$inject = ['$timeout', 'communicationService', 'communicationAttachmentFilesModalService', 'dateUtilityService'];
    
	function communicationCreateFormDirective($timeout, communicationService, communicationAttachmentFilesModalService, dateUtilityService) {
		var directive = {
			restrict: 'E',
			scope: {
				invoker: '=',
				confirmation: '=',
				selectedProject: '=',
				executionDate: '=',
				selectedCommunicationGroups: '=',
				selectedUsers: '=',
				currentUser: '=',
				communicationUserConnectionsOfUserAndPage: '=',
				message: '=',
				messageEdit: '=',
				communicationUserConnectionOfUserAndPageEdit: '='
			},
			templateUrl: 'app/communication/directives/communicationCreateFormDirective/communicationCreateForm.html',
			link: function($scope) {
				$scope.message = null;
				$scope.editMessageMode = false;
				$scope.confirmation = $scope.confirmation;
				$scope.message = null;
				$scope.executionDate = $scope.executionDate;
				
				$scope.openExecutionDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.executionDatePicker = true;
		        };
				
				$scope.sendMessage = function() {
		        	if($scope.messageEdit) {
		        		$scope.editMode();
		        	} else {
			            $scope.createMode();
		        	}
				};
				
				$scope.editMode = function() {
					$scope.communicationUserConnectionOfUserAndPageEdit.executionDate = $scope.executionDate;
					$scope.communicationUserConnectionOfUserAndPageEdit.confirmationNeeded = $scope.confirmation;
					$scope.communicationUserConnectionOfUserAndPageEdit.communication.message = $scope.message;
					Date.prototype.toJSON = function () { return this.toLocaleString(); };
					communicationService.updateCommunicationMessage($scope.communicationUserConnectionOfUserAndPageEdit.communication.id, $scope.communicationUserConnectionOfUserAndPageEdit).then(function successCallback(response) {
						$scope.messageEdit = false;
						$scope.communicationUserConnectionOfUserAndPageEdit = response.data;
						$scope.unsetCommunicationCreateForm();
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error communicationCreateForm.directive#editMode');
		  	   		 });
				};
				
				$scope.createMode = function() {
		            if($scope.message != null && $scope.message.length > 0 && $scope.usersOrGroupsSelected() === false) {
		            	$scope.files = null;
		            	$scope.noMessageError = false;
		            	$scope.noUserSelectedError = false;
		            	
		            	Date.prototype.toJSON = function () { return this.toLocaleString(); };
		            	$scope.stompClient.send("/app/api/communications/communication/" + $scope.currentUser.id, {}, JSON.stringify({ 'id': $scope.currentUser.id,'name': $scope.currentUser.title + ' ' + $scope.currentUser.firstname + ' ' + $scope.currentUser.surname, 'message': $scope.message, 'associatedToGroup': $scope.selectedCommunicationGroups.length > 0, 'communicationGroupIds': $scope.selectedCommunicationGroups,'usersSelected': $scope.selectedUsers,'project': $scope.selectedProject ,'confirmation': $scope.confirmation,'relevantForWorkingBook': false,'elapsedDate': '', 'executionDate': $scope.executionDate, 'selectedDocumentFiles': $scope.selectedDocumentFiles}));
		            	$scope.unsetCommunicationCreateForm();
		            } else if($scope.message == null || $scope.message.length < 2) {
		            	$scope.noMessageError = true;
		            	$scope.noUserSelectedError = false;
		            } else if($scope.usersOrGroupsSelected() === true) {
		            	$scope.noUserSelectedError = true;
		            	$scope.noMessageError = false;
		            	console.log('please select user to communicate for chat.controller.js');
		            }
				};
				
				$scope.unsetCommunicationCreateForm = function() {
					$scope.confirmation = false;
					$scope.message = null;
					$scope.executionDate = null;
					$scope.selectedDocumentFiles = [];
				};
				
				$scope.usersOrGroupsSelected = function() {
					return ($scope.selectedCommunicationGroup == null && $scope.selectedUsers == null) || ($scope.selectedCommunicationGroups.length === 0 && $scope.selectedUsers.length === 0);
				};
				
				$scope.connect = function() {
		            var socket = new SockJS('/api/chat');
		            $scope.stompClient = Stomp.over(socket);            
		            $scope.stompClient.connect({}, function(frame) {
		                console.log('webSocket Connected in communicationCreateForm.directive.js: ' + frame);
	                	$scope.stompClient.subscribe('/topic/communication/' + $scope.currentUser.id, function(message) {
	                		var parsedMessage = JSON.parse(message.body);
	                		$scope.communicationUserConnectionsOfUserAndPage.push(parsedMessage);
	                		$scope.$apply();
	                		$scope.invoker.scrollToBottom();
	                    });
		            });
		        };
				
				$scope.addAttachmentFiles = function() {
					communicationAttachmentFilesModalService.showCommunicationAttachmentFilesModal($scope);
				};
				
				$scope.attachFiles = function(selectedDocumentFiles) {
		    		$scope.selectedDocumentFiles = selectedDocumentFiles;
		    	};
				
				$scope.$on('$destroy', function() { // invoked if page is leaved 
		        	if($scope.stompClient != null) {
		        		$scope.stompClient.unsubscribe();
		        		$scope.stompClient.disconnect(function() {
		                	console.log("Disconnected communicationCreateForm.directive#destroy");
		                });
		            } else {
		            	console.log("vm.stompClient is null and cannot be disconnected communicationCreateForm.directive#destroy");
		            }
		    	});
				$scope.connect();
			 }
		};
		return directive;
		
		////////////
	}
})();