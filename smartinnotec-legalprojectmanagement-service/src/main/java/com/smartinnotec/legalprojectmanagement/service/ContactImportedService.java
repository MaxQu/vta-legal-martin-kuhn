package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface ContactImportedService {

    List<ContactImported> findAll() throws BusinessException;

    List<ContactImported> findPagedContactsImported(final Integer page) throws BusinessException;

    ContactImported findByCustomerNumberContainersIn(final CustomerNumberContainer customerNumberContainer) throws BusinessException;

    ContactImported findById(final String id) throws BusinessException;

    Integer count() throws BusinessException;

    boolean hasContactImportedChanged(final ContactImported contactImported) throws BusinessException;

    ContactImported create(final ContactImported contactImported) throws BusinessException;

    ContactImported update(final ContactImported contactImported) throws BusinessException;

    void delete(final ContactImported contactImported) throws BusinessException;

    Integer getAmountOfContactsImported(final Tenant tenant) throws BusinessException;

    void deleteAllContactsImported() throws BusinessException;

}
