package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.repository.AddressRepository;

@Service("addressService")
public class AddressServiceImpl extends AbstractService implements AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private GeocodeService geocodeService;



    @Override
    public Address create(final Address address) throws BusinessException {
        BusinessAssert.notNull(address, "address is mandatory in AddressServiceImpl#create", "400");
        logger.info("create address in AddressServiceImpl#create");
        final Address createdAddress = addressRepository.insert(address);
        return createdAddress;
    }

    @Override
    public Address update(final Address address) throws BusinessException {
        BusinessAssert.notNull(address, "address is mandatory in AddressServiceImpl#update", "400");
        logger.info("update address with id '{}' in AddressServiceImpl#create", address.getId());
        return addressRepository.save(address);
    }

    @Override
    public Address findById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in AddressServiceImpl#findById", "400");
        BusinessAssert.isId(id, "id is mandatory in AddressServiceImpl#findById", "400");
        logger.info("find address with id '{}' in AddressServiceImpl#findById", id);
        return addressRepository.findOne(id);
    }

    @Override
    public List<Address> findAll() throws BusinessException {
        logger.info("find all addresses in AddressServiceImpl#findAll");
        return addressRepository.findAll();
    }
    
    @Override
	public List<Address> findAddressesBySearchString(final String searchString) throws BusinessException {
    	BusinessAssert.notNull(searchString, "searchString is mandatory in AddressServiceImpl#findAddressesBySearchString", "400");
    	logger.info("find addesses by searchString '{}' in AddressServiceImpl#findAddressesBySearchString", searchString);
		return addressRepository.findAddressesBySearchString(searchString);
	}

    @Override
    public void delete(final Address address) throws BusinessException {
        BusinessAssert.notNull(address, "address is mandatory in AddressServiceImpl#delete", "400");
        logger.info("delete address with id '{}' in AddressServiceImpl#delete", address.getId());
        addressRepository.delete(address.getId());
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all addresses in AddressServiceImpl#deleteAll");
        addressRepository.deleteAll();
    }

    @Override
    public void performGeocodingWhenRequired() {

        List<Address> addresses =  addressRepository.findAddressesWithoutGeolocation();
        logger.info(String.format("geocode addresses without geolocation (%d)", addresses.size()));

        for (Address address: addresses) {
            logger.info(String.format("geocode address with ID %s", address.getId()));
            geocodeService.geocodeAddress(address);
            addressRepository.save(address);
        }
    }

    @Override
    public String toString() {
        return "[AddressServiceImpl]";
    }
}
