(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();