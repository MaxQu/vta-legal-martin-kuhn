package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface UserRepository extends MongoRepository<User, String> {

    User findUserByUsername(final String username);

    List<User> findUsersByUsername(final String name);

    @Query("{$or : [{'username' : ?0}, {'email' : ?0}], $and : [{'active' : ?1}]}")
    User findUserByUsernameOrEmailAndActive(final String usernameOrEmail, final boolean active);

    User findUserByEmail(final String email);

    @Query("{$or : [{'username' : ?0}, {'email' : ?1}]}")
    User checkIfUserExistByUsernameOrEmail(final String username, final String email);

    User getUserByUsername(final String username);

    // http://docs.mongodb.org/manual/reference/operator/query/text/#op._S_text
    // @Query("{$or : [{'firstname' : ?0}, {'surname' : ?0}, {'email' : ?0}, {'username' : ?0} ] }")
    @Query("{$text : { $search : ?0 } }")
    List<User> findUserBySearchString(final String searchString);

    List<User> findByTenant(final Tenant tenant);

    List<User> findByProjectUserConnectionRoleIn(final List<ProjectUserConnectionRoleEnum> projectUserConnectionRoles);
}
