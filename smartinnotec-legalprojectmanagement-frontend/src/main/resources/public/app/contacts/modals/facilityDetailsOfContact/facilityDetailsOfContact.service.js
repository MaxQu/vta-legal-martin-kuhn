(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('facilityDetailsOfContactModalService', facilityDetailsOfContactModalService);
        
    facilityDetailsOfContactModalService.$inject = ['$modal', '$stateParams', 'dateUtilityService', 'facilityDetailsService', 'contactFacilityDetailCreateEditModalService', 'yesNoFacilityDetailsContactModalService'];
    
    function facilityDetailsOfContactModalService($modal, $stateParams, dateUtilityService, facilityDetailsService, contactFacilityDetailCreateEditModalService, yesNoFacilityDetailsContactModalService) {
		var service = {
			showFacilityDetailsOfContactModal: showFacilityDetailsOfContactModal
		};		
		return service;
		
		////////////
				
		function showFacilityDetailsOfContactModal(contact, facilityDetails, start, end, activityTypes, facilityDetailsTypes, userAuthorizationUserContainer) {
			 var facilityDetailsOfContactModal = $modal.open({
				controller: FacilityDetailsOfContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {   
			    	contact: function() {
			    		return contact; 
			    	}, 
			    	facilityDetails: function() {
			    		return facilityDetails;
			    	}, 
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	}, 
			    	activityTypes: function() {
			    		return activityTypes;
			    	}, 
			    	facilityDetailsTypes: function() {
			    		return facilityDetailsTypes;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/facilityDetailsOfContact/facilityDetailsOfContact.modal.html'
			});
			return facilityDetailsOfContactModal;
			
			function FacilityDetailsOfContactModalController($modalInstance, $scope, contact, facilityDetails, start, end, activityTypes, facilityDetailsTypes, userAuthorizationUserContainer) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.facilityDetails = facilityDetails;
		    	vm.filterStartDatePicker = false;
		    	vm.filterEndDatePicker = false;
		    	vm.filterStartDate = start;
		    	vm.filterEndDate = end;
		    	vm.activityTypes = activityTypes;
		    	vm.facilityDetailsTypes = facilityDetailsTypes;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	
		    	vm.showContactFacilityDetailCreateEditModal = showContactFacilityDetailCreateEditModal;
		    	vm.removeFacilityDetail = removeFacilityDetail;
		    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
		    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
		    	vm.formatStartEndDateTime = formatStartEndDateTime;
		    	vm.showRemoveFacilityDetail = showRemoveFacilityDetail;
		    	vm.closeFacilityDetailsOfContactModal = closeFacilityDetailsOfContactModal;
		    	
		    	////////////

		    	$scope.$watch('vm.filterStartDate', function(newValue, oldValue) {
		    		if(oldValue != newValue && newValue != null) {
		    			loadFacilityDetailsOfContact();
		    		}
				});
		    	
		    	$scope.$watch('vm.filterEndDate', function(newValue, oldValue) {
		    		if(oldValue != newValue && newValue != null) {
		    			loadFacilityDetailsOfContact();
		    		}
				});
		    	
		    	function loadFacilityDetailsOfContact() {
		    		var startDateString = dateUtilityService.formatDateToString(vm.filterStartDate);
		    		var endDateString = dateUtilityService.formatDateToString(vm.filterEndDate);
		    		facilityDetailsService.getFacilityDetailsOfContactIdInRange(vm.contact.id, startDateString, endDateString).then(function(response) {
		    			vm.facilityDetails = response.data;
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error facilityDetailsOfContact.service#loadFacilityDetailsOfContact');
		  	   		 });
		    	}
		    	
		    	function openFilterStartDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterStartDatePicker = true;
		        }
		    	
		    	function openFilterEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterEndDatePicker = true;
		        }
		    	
		    	function showContactFacilityDetailCreateEditModal(facilityDetail) {
		    		contactFacilityDetailCreateEditModalService.showContactFacilityDetailCreateEditModal(vm.contact, facilityDetail, vm.activityTypes, vm.facilityDetailsTypes);
		    	}
		    	
		    	function removeFacilityDetail(facilityDetail) {
		    		facilityDetailsService.deleteFacilityDetail(facilityDetail).then(function(response) {
		    			for(var i = 0; i < vm.facilityDetails.length; i++) {
		    				if(vm.facilityDetails[i].id === facilityDetail.id) {
		    					vm.facilityDetails.splice(i, 1);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error facilityDetailsOfContact.service#removeFacilityDetail');
		  	   		 });
		    	}
		    	
		    	function formatStartEndDateTime(start, end) {
		    		var startDate = new Date(start);
		    		var formattedStartDate = dateUtilityService.formatDateToDateTimeString(startDate);
		    		var endDate = new Date(end);
		    		var formattedEndDate = dateUtilityService.formatDateToDateTimeString(endDate);
		    		return dateUtilityService.formatStartEndDateTime(formattedStartDate, formattedEndDate);
		    	}
		    	
		    	function showRemoveFacilityDetail(facilityDetails) {
		    		yesNoFacilityDetailsContactModalService.showYesNoModal(vm, vm.contact, facilityDetails);
		    	}
		    	
		    	function closeFacilityDetailsOfContactModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();