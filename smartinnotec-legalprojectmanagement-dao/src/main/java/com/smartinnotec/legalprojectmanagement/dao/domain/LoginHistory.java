package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "LoginHistory")
public @Data class LoginHistory {

    @Id
    private String id;
    @DBRef
    private User user;
    private LoginHistoryTypeEnum loginHistoryType;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime loginDateTime;

}
