package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Receipe;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeTypeEnum;

public interface ReceipeRepository extends MongoRepository<Receipe, String> {

    List<Receipe> findByReceipeAttachmentsNotNull();

    List<Receipe> findByProductIdAndReceipeType(final String productId, final ReceipeTypeEnum receipeType);

    List<Receipe> findByReceipeType(final ReceipeTypeEnum receipeType);

    List<Receipe> findByProductIdAndReceipeTypeAndDateBetween(final String productId, final ReceipeTypeEnum receipeType,
            final DateTime start, final DateTime end);

    Receipe findByProductIdAndReceipeTypeOrderByDateDesc(final String productId, final ReceipeTypeEnum receipeType);

    Long countByProductIdAndReceipeType(final String productId, final ReceipeTypeEnum receipeType);

    Long countByReceipeType(final ReceipeTypeEnum receipeType);

}
