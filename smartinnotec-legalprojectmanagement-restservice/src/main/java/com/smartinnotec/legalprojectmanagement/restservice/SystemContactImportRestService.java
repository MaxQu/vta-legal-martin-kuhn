package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressPreviousImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactPreviousImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactPreviousImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.restservice.container.ContactAndContactImportedContainer;
import com.smartinnotec.legalprojectmanagement.restservice.container.SystemImportContainer;
import com.smartinnotec.legalprojectmanagement.service.AddressPreviousImportedService;
import com.smartinnotec.legalprojectmanagement.service.ContactImportedService;
import com.smartinnotec.legalprojectmanagement.service.ContactPreviousImportedService;
import com.smartinnotec.legalprojectmanagement.service.ContactService;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.SystemImportContactMerge;
import com.smartinnotec.legalprojectmanagement.service.SystemImportMergeScheduleService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class SystemContactImportRestService extends AbstractRestService {

    @Autowired
    private UserService userService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private ContactImportedService contactImportedService;
    @Autowired
    private SystemImportMergeScheduleService systemImportMergeScheduleService;
    @Autowired
    private ContactPreviousImportedService contactPreviousImportedService;
    @Autowired
    private AddressPreviousImportedService addressPreviousImportedService;
    @Autowired
    private SystemImportContactMerge systemImportContactMerge;
    @Autowired
    private HistoryService historyService;

    @RequestMapping(value = "/systemimports/systemimport/ofstandardpath", method = { RequestMethod.PUT })
    public void mergeContactsWithAddressesAndSaveOfStandardPath() throws BusinessException {
        logger.info(
            "merge contacts with addresses of standard path and save in SystemContactImportRestService#mergeContactsWithAddressesAndSaveOfStandardPath");

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final String message = "";
        final History history = new History.HistoryBuilder()
            .setHistoryType(HistoryTypeEnum.MERGE_CONTACTS_WITH_ADDRESSES_OF_STANDARD_PATH_AND_SAVE)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in SystemContactImportRestService#mergeContactsWithAddressesAndSaveOfStandardPath",
            createdHistory.getId());
    }

    @RequestMapping(value = "/systemimports/systemimport", method = { RequestMethod.PUT })
    public void mergeContactsWithAddressesAndSave(final @RequestBody SystemImportContainer systemImportContainer) throws BusinessException {
        BusinessAssert.notNull(systemImportContainer,
            "systemImportContainer is mandatory in SystemContactImportRestService#mergeContactsWithAddressesAndSave");
        logger.info("merge contacts with addresses and save in SystemContactImportRestService#mergeContactsWithAddressesAndSave");
        systemImportMergeScheduleService.mergeContactsWithAddressesAndSave(systemImportContainer.getPathAddress(),
                systemImportContainer.getPathContact());
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final String message = systemImportContainer.getPathAddress().substring(systemImportContainer.getPathAddress().lastIndexOf("/") + 1)
            + ", " + systemImportContainer.getPathContact().substring(systemImportContainer.getPathContact().lastIndexOf("/") + 1);
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.MERGE_CONTACTS_WITH_ADDRESSES_AND_SAVE)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in SystemContactImportRestService#mergeContactsWithAddressesAndSave",
            createdHistory.getId());
    }

    @RequestMapping(value = "/systemimports/systemimport/contactimported", method = { RequestMethod.GET })
    public List<ContactAndContactImportedContainer> getContactsImported() throws BusinessException {
        logger.info("get contact imoorted in SystemContactImportRestService#getContactsImported");

        final List<ContactImported> contactsImported = contactImportedService.findAll();
        final List<ContactAndContactImportedContainer> contactAndContactImportedContainers = createContactsAndContactImportedContainers(
            contactsImported);
        return contactAndContactImportedContainers;
    }

    @RequestMapping(value = "/systemimports/systemimport/confirm", method = { RequestMethod.PUT })
    public Contact confirmContactImported(final @RequestBody ContactAndContactImportedContainer contactAndContactImportedContainer)
            throws BusinessException {
        logger.info("confirm contactImported in SystemContactImportRestService#confirmContactImported");
        BusinessAssert.notNull(contactAndContactImportedContainer,
            "contactAndContactImportedContainer is mandatory in SystemContactImportRestService#confirmContactImported");

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        boolean newContact = false;
        if (contactAndContactImportedContainer.getContact() == null) {
            newContact = true;
        }
        // merge contact (contactImported into contact)
        if (newContact) {
            final Contact contact = new Contact();
            final Address address = new Address();
            contact.setAddress(address);
            contactAndContactImportedContainer.setContact(contact);
        }
        final Contact contactMerged = systemImportContactMerge.mergeContact(contactAndContactImportedContainer.getContact(),
            contactAndContactImportedContainer.getContactImported());
        contactMerged.setImportConfirmed(true);
        contactMerged.setImported(true);
        contactMerged.setImportationDate(new DateTime(System.currentTimeMillis()));
        logger.info("contact with id '{}' merged in SystemContactImportRestService#confirmContactImported", contactMerged.getId());
        // create or update merged contact
        final boolean changed = contactImportedService.hasContactImportedChanged(contactAndContactImportedContainer.getContactImported());
        if (newContact) {
            final Contact createdContact = contactService.create(contactMerged, user, changed);
            logger.info("contact with id '{}' created in SystemContactImportRestService#confirmContactImported", createdContact.getId());
        } else {
            final Contact updatedContact = contactService.update(contactMerged, user, false);
            logger.info("contact with id '{}' updated in SystemContactImportRestService#confirmContactImported", updatedContact.getId());
        }
        // write contactImported to contactPreviousImported table
        final ContactPreviousImported contactPreviousImported = contactPreviousImportedService
            .convertFromContactImportedToContactPreviousImported(contactAndContactImportedContainer.getContactImported());
        final AddressPreviousImported createdAddressPreviousImported = addressPreviousImportedService
            .create(contactPreviousImported.getAddressPreviousImported());
        contactPreviousImported.setAddressPreviousImported(createdAddressPreviousImported);
        if (contactPreviousImported.getContactPreviousImportedAddressesImported() != null) {
            for (final ContactPreviousImportedAddressImported contactPreviousImportedAddressImported : contactPreviousImported
                .getContactPreviousImportedAddressesImported()) {
                final AddressPreviousImported addressPreviousImported = contactPreviousImportedAddressImported.getAddressPreviousImported();
                final AddressPreviousImported createdAddressPreviousImportedForContactPreviousImportedAddressImported = addressPreviousImportedService
                    .create(addressPreviousImported);
                contactPreviousImportedAddressImported
                    .setAddressPreviousImported(createdAddressPreviousImportedForContactPreviousImportedAddressImported);
            }
        }
        logger.info("create addressPreviousImported in SystemContactImportRestService#confirmContactImported");

        final ContactPreviousImported createdContactPreviousImported = contactPreviousImportedService.create(contactPreviousImported);
        logger.info("created contactPrevioudImported with id '{}' in SystemContactImportRestService#confirmContactImported",
            createdContactPreviousImported.getId());
        // remove contactImported
        contactImportedService.delete(contactAndContactImportedContainer.getContactImported());

        final String message = createdContactPreviousImported.getInstitution();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.CONTACT_IMPORTED_CONFIRMED)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in SystemContactImportRestService#confirmContactImported", createdHistory.getId());

        return contactMerged;
    }

    @RequestMapping(value = "/systemimports/systemimport/{page}", method = { RequestMethod.GET }, produces = "application/json")
    public @ResponseBody List<ContactAndContactImportedContainer> getPagedContactsImported(final @PathVariable("page") Integer page)
            throws BusinessException {
        BusinessAssert.notNull(page, "page is mandatory in SystemContactImportRestService#getPagedContactsImported", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": get paged contacts imported starting of page '{}' in SystemContactImportRestService#getPagedContactsImported", page);
        final List<ContactImported> contactsImported = contactImportedService.findPagedContactsImported(page);
        final List<ContactAndContactImportedContainer> contactAndContactImportedContainers = createContactsAndContactImportedContainers(
            contactsImported);
        return contactAndContactImportedContainers;
    }

    private List<ContactAndContactImportedContainer> createContactsAndContactImportedContainers(
            final List<ContactImported> contactsImported)
            throws BusinessException {
        logger.info(
            "create contactsAndContactImportedContainers in SystemContactImportRestService#createContactsAndContactImportedContainers");
        final List<ContactAndContactImportedContainer> contactAndContactImportedContainers = new ArrayList<>();
        for (final ContactImported contactImported : contactsImported) {
            final List<CustomerNumberContainer> customerNumberContainers = contactImported.getCustomerNumberContainers();
            final CustomerNumberContainer customerNumberContainer = customerNumberContainers.get(0);
            final Contact contact = contactService.findByCustomerNumberContainersIn(customerNumberContainer);
            final ContactAndContactImportedContainer contactAndContactImportedContainer = new ContactAndContactImportedContainer();
            contactAndContactImportedContainer.setContact(contact);
            contactAndContactImportedContainer.setContactImported(contactImported);
            contactAndContactImportedContainers.add(contactAndContactImportedContainer);
        }
        return contactAndContactImportedContainers;
    }

    @Override
    public String toString() {
        return "[SystemContactImportRestService]";
    }
}
