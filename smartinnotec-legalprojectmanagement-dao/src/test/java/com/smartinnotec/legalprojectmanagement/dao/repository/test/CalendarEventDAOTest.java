package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public class CalendarEventDAOTest extends AbstractDAOTest {

    @Test
    public void shouldGetCalendarEventWhereItIsLongerThanOneDay() {
        calendarEventRepository.deleteAll();

        final User user = userRepository.findOne("5589929b887dc1fdb501cdba");

        final DateTime start = dateFormatter.parseDateTime("20.12.2016 08:03");
        final DateTime end = dateFormatter.parseDateTime("24.12.2016 11:32");

        final DateTime searchStart = dateFormatter.parseDateTime("22.12.2016 06:03");
        final DateTime searchEnd = dateFormatter.parseDateTime("23.12.2016 16:32");

        final CalendarEvent calendarEvent = new CalendarEvent();
        calendarEvent.setStartsAt(start.getMillis());
        calendarEvent.setEndsAt(end.getMillis());
        calendarEvent.setTenant(user.getTenant());
        final CalendarEvent createdCalendarEvent = calendarEventRepository.insert(calendarEvent);
        Assert.assertNotNull(createdCalendarEvent);

        final List<CalendarEvent> calendarEvents = calendarEventRepository.findByTenantAndArchivedAndStartsAtLessThanAndEndsAtGreaterThan(
            user.getTenant(), false, searchStart.getMillis(), searchEnd.getMillis());
        Assert.assertNotNull(calendarEvents);
        Assert.assertEquals(1, calendarEvents.size());
    }
}
