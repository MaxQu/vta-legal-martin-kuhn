package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarConfiguration;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarConfigurationTypeEnum;

public class CalendarConfigurationDAOTest extends AbstractDAOTest {

    @Test
    public void shouldCreateCalendarConfiguration() {
        final CalendarConfiguration calendarConfiguration1 = new CalendarConfiguration();
        calendarConfiguration1.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration1.setKey("BEGIN");
        calendarConfiguration1.setValue("VCALENDAR");
        calendarConfiguration1.setOrderNumber(1);
        final CalendarConfiguration createdCalendarConfiguration1 = calendarConfigurationRepository.insert(calendarConfiguration1);
        Assert.assertNotNull(createdCalendarConfiguration1);

        final CalendarConfiguration calendarConfiguration2 = new CalendarConfiguration();
        calendarConfiguration2.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration2.setKey("PRODID");
        calendarConfiguration2.setValue("-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN");
        calendarConfiguration2.setOrderNumber(2);
        final CalendarConfiguration createdCalendarConfiguration2 = calendarConfigurationRepository.insert(calendarConfiguration2);
        Assert.assertNotNull(createdCalendarConfiguration2);

        final CalendarConfiguration calendarConfiguration3 = new CalendarConfiguration();
        calendarConfiguration3.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration3.setKey("VERSION");
        calendarConfiguration3.setValue("2.0");
        calendarConfiguration3.setOrderNumber(3);
        final CalendarConfiguration createdCalendarConfiguration3 = calendarConfigurationRepository.insert(calendarConfiguration3);
        Assert.assertNotNull(createdCalendarConfiguration3);

        final CalendarConfiguration calendarConfiguration4 = new CalendarConfiguration();
        calendarConfiguration4.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration4.setKey("METHOD");
        calendarConfiguration4.setValue("PUBLISH");
        calendarConfiguration4.setOrderNumber(4);
        final CalendarConfiguration createdCalendarConfiguration4 = calendarConfigurationRepository.insert(calendarConfiguration4);
        Assert.assertNotNull(createdCalendarConfiguration4);

        final CalendarConfiguration calendarConfiguration5 = new CalendarConfiguration();
        calendarConfiguration5.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration5.setKey("X-CALSTART");
        calendarConfiguration5.setValue("20170110T100000Z");
        calendarConfiguration5.setOrderNumber(5);
        final CalendarConfiguration createdCalendarConfiguration5 = calendarConfigurationRepository.insert(calendarConfiguration5);
        Assert.assertNotNull(createdCalendarConfiguration5);

        final CalendarConfiguration calendarConfiguration6 = new CalendarConfiguration();
        calendarConfiguration6.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration6.setKey("X-CALEND");
        calendarConfiguration6.setValue("20170110T103000Z");
        calendarConfiguration6.setOrderNumber(6);
        final CalendarConfiguration createdCalendarConfiguration6 = calendarConfigurationRepository.insert(calendarConfiguration6);
        Assert.assertNotNull(createdCalendarConfiguration6);

        final CalendarConfiguration calendarConfiguration7 = new CalendarConfiguration();
        calendarConfiguration7.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration7.setKey("X-CLIPSTART");
        calendarConfiguration7.setValue("20170108T230000Z");
        calendarConfiguration7.setOrderNumber(7);
        final CalendarConfiguration createdCalendarConfiguration7 = calendarConfigurationRepository.insert(calendarConfiguration7);
        Assert.assertNotNull(createdCalendarConfiguration7);

        final CalendarConfiguration calendarConfiguration8 = new CalendarConfiguration();
        calendarConfiguration8.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration8.setKey("X-CLIPEND");
        calendarConfiguration8.setValue("20170111T230000Z");
        calendarConfiguration8.setOrderNumber(8);
        final CalendarConfiguration createdCalendarConfiguration8 = calendarConfigurationRepository.insert(calendarConfiguration8);
        Assert.assertNotNull(createdCalendarConfiguration8);

        final CalendarConfiguration calendarConfiguration9 = new CalendarConfiguration();
        calendarConfiguration9.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration9.setKey("X-WR-RELCALID");
        calendarConfiguration9.setValue("{0000002E-1627-1AF9-4B2F-4DC09F390C65}");
        calendarConfiguration9.setOrderNumber(9);
        final CalendarConfiguration createdCalendarConfiguration9 = calendarConfigurationRepository.insert(calendarConfiguration9);
        Assert.assertNotNull(createdCalendarConfiguration9);

        final CalendarConfiguration calendarConfiguration10 = new CalendarConfiguration();
        calendarConfiguration10.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration10.setKey("X-WR-CALNAME");
        calendarConfiguration10.setValue("LegalProjectManagement");
        calendarConfiguration10.setOrderNumber(10);
        final CalendarConfiguration createdCalendarConfiguration10 = calendarConfigurationRepository.insert(calendarConfiguration10);
        Assert.assertNotNull(createdCalendarConfiguration10);

        final CalendarConfiguration calendarConfiguration11 = new CalendarConfiguration();
        calendarConfiguration11.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration11.setKey("X-PRIMARY-CALENDAR");
        calendarConfiguration11.setValue("TRUE");
        calendarConfiguration11.setOrderNumber(11);
        final CalendarConfiguration createdCalendarConfiguration11 = calendarConfigurationRepository.insert(calendarConfiguration11);
        Assert.assertNotNull(createdCalendarConfiguration11);

        final CalendarConfiguration calendarConfiguration12 = new CalendarConfiguration();
        calendarConfiguration12.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration12.setKey("X-OWNER;CN=\"LPM\"");
        calendarConfiguration12.setValue("mailto:office@ihl.com");
        calendarConfiguration12.setOrderNumber(12);
        final CalendarConfiguration createdCalendarConfiguration12 = calendarConfigurationRepository.insert(calendarConfiguration12);
        Assert.assertNotNull(createdCalendarConfiguration12);

        final CalendarConfiguration calendarConfiguration14 = new CalendarConfiguration();
        calendarConfiguration14.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration14.setKey("X-MS-OLK-WKHRSTART;TZID=\"W. Europe Standard Time\"");
        calendarConfiguration14.setValue("073000");
        calendarConfiguration14.setOrderNumber(13);
        final CalendarConfiguration createdCalendarConfiguration14 = calendarConfigurationRepository.insert(calendarConfiguration14);
        Assert.assertNotNull(createdCalendarConfiguration14);

        final CalendarConfiguration calendarConfiguration15 = new CalendarConfiguration();
        calendarConfiguration15.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration15.setKey("X-MS-OLK-WKHREND;TZID=\"W. Europe Standard Time\"");
        calendarConfiguration15.setValue("170000");
        calendarConfiguration15.setOrderNumber(14);
        final CalendarConfiguration createdCalendarConfiguration15 = calendarConfigurationRepository.insert(calendarConfiguration15);
        Assert.assertNotNull(createdCalendarConfiguration15);

        final CalendarConfiguration calendarConfiguration16 = new CalendarConfiguration();
        calendarConfiguration16.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration16.setKey("X-MS-OLK-WKHRDAYS");
        calendarConfiguration16.setValue("MO,TU,WE,TH,FR");
        calendarConfiguration16.setOrderNumber(15);
        final CalendarConfiguration createdCalendarConfiguration16 = calendarConfigurationRepository.insert(calendarConfiguration16);
        Assert.assertNotNull(createdCalendarConfiguration16);

        final CalendarConfiguration calendarConfiguration16a = new CalendarConfiguration();
        calendarConfiguration16a.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration16a.setKey("BEGIN");
        calendarConfiguration16a.setValue("VTIMEZONE");
        calendarConfiguration16a.setOrderNumber(16);
        final CalendarConfiguration createdCalendarConfiguration16a = calendarConfigurationRepository.insert(calendarConfiguration16a);
        Assert.assertNotNull(createdCalendarConfiguration16a);

        final CalendarConfiguration calendarConfiguration17 = new CalendarConfiguration();
        calendarConfiguration17.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration17.setKey("TZID");
        calendarConfiguration17.setValue("W. Europe Standard Time");
        calendarConfiguration17.setOrderNumber(17);
        final CalendarConfiguration createdCalendarConfiguration17 = calendarConfigurationRepository.insert(calendarConfiguration17);
        Assert.assertNotNull(createdCalendarConfiguration17);

        final CalendarConfiguration calendarConfiguration18 = new CalendarConfiguration();
        calendarConfiguration18.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration18.setKey("BEGIN");
        calendarConfiguration18.setValue("STANDARD");
        calendarConfiguration18.setOrderNumber(18);
        final CalendarConfiguration createdCalendarConfiguration18 = calendarConfigurationRepository.insert(calendarConfiguration18);
        Assert.assertNotNull(createdCalendarConfiguration18);

        final CalendarConfiguration calendarConfiguration19 = new CalendarConfiguration();
        calendarConfiguration19.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration19.setKey("DTSTART");
        calendarConfiguration19.setValue("16011028T030000");
        calendarConfiguration19.setOrderNumber(19);
        final CalendarConfiguration createdCalendarConfiguration19 = calendarConfigurationRepository.insert(calendarConfiguration19);
        Assert.assertNotNull(createdCalendarConfiguration19);

        final CalendarConfiguration calendarConfiguration20 = new CalendarConfiguration();
        calendarConfiguration20.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration20.setKey("RRULE");
        calendarConfiguration20.setValue("FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10");
        calendarConfiguration20.setOrderNumber(20);
        final CalendarConfiguration createdCalendarConfiguration20 = calendarConfigurationRepository.insert(calendarConfiguration20);
        Assert.assertNotNull(createdCalendarConfiguration20);

        final CalendarConfiguration calendarConfiguration21 = new CalendarConfiguration();
        calendarConfiguration21.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration21.setKey("TZOFFSETFROM");
        calendarConfiguration21.setValue("+0200");
        calendarConfiguration21.setOrderNumber(21);
        final CalendarConfiguration createdCalendarConfiguration21 = calendarConfigurationRepository.insert(calendarConfiguration21);
        Assert.assertNotNull(createdCalendarConfiguration21);

        final CalendarConfiguration calendarConfiguration22 = new CalendarConfiguration();
        calendarConfiguration22.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration22.setKey("TZOFFSETTO");
        calendarConfiguration22.setValue("+0000");
        calendarConfiguration22.setOrderNumber(22);
        final CalendarConfiguration createdCalendarConfiguration22 = calendarConfigurationRepository.insert(calendarConfiguration22);
        Assert.assertNotNull(createdCalendarConfiguration22);

        final CalendarConfiguration calendarConfiguration23 = new CalendarConfiguration();
        calendarConfiguration23.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration23.setKey("END");
        calendarConfiguration23.setValue("STANDARD");
        calendarConfiguration23.setOrderNumber(23);
        final CalendarConfiguration createdCalendarConfiguration23 = calendarConfigurationRepository.insert(calendarConfiguration23);
        Assert.assertNotNull(createdCalendarConfiguration23);

        final CalendarConfiguration calendarConfiguration24 = new CalendarConfiguration();
        calendarConfiguration24.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration24.setKey("BEGIN");
        calendarConfiguration24.setValue("DAYLIGHT");
        calendarConfiguration24.setOrderNumber(24);
        final CalendarConfiguration createdCalendarConfiguration24 = calendarConfigurationRepository.insert(calendarConfiguration24);
        Assert.assertNotNull(createdCalendarConfiguration24);

        final CalendarConfiguration calendarConfiguration25 = new CalendarConfiguration();
        calendarConfiguration25.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration25.setKey("DTSTART");
        calendarConfiguration25.setValue("16010325T020000");
        calendarConfiguration25.setOrderNumber(25);
        final CalendarConfiguration createdCalendarConfiguration25 = calendarConfigurationRepository.insert(calendarConfiguration25);
        Assert.assertNotNull(createdCalendarConfiguration25);

        final CalendarConfiguration calendarConfiguration26 = new CalendarConfiguration();
        calendarConfiguration26.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration26.setKey("RRULE");
        calendarConfiguration26.setValue("FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3");
        calendarConfiguration26.setOrderNumber(26);
        final CalendarConfiguration createdCalendarConfiguration26 = calendarConfigurationRepository.insert(calendarConfiguration26);
        Assert.assertNotNull(createdCalendarConfiguration26);

        final CalendarConfiguration calendarConfiguration27 = new CalendarConfiguration();
        calendarConfiguration27.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration27.setKey("TZOFFSETFROM");
        calendarConfiguration27.setValue("+0100");
        calendarConfiguration27.setOrderNumber(27);
        final CalendarConfiguration createdCalendarConfiguration27 = calendarConfigurationRepository.insert(calendarConfiguration27);
        Assert.assertNotNull(createdCalendarConfiguration27);

        final CalendarConfiguration calendarConfiguration28 = new CalendarConfiguration();
        calendarConfiguration28.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration28.setKey("TZOFFSETTO");
        calendarConfiguration28.setValue("+0100");
        calendarConfiguration28.setOrderNumber(28);
        final CalendarConfiguration createdCalendarConfiguration28 = calendarConfigurationRepository.insert(calendarConfiguration28);
        Assert.assertNotNull(createdCalendarConfiguration28);

        final CalendarConfiguration calendarConfiguration29 = new CalendarConfiguration();
        calendarConfiguration29.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration29.setKey("END");
        calendarConfiguration29.setValue("DAYLIGHT");
        calendarConfiguration29.setOrderNumber(29);
        final CalendarConfiguration createdCalendarConfiguration29 = calendarConfigurationRepository.insert(calendarConfiguration29);
        Assert.assertNotNull(createdCalendarConfiguration29);

        final CalendarConfiguration calendarConfiguration30 = new CalendarConfiguration();
        calendarConfiguration30.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER);
        calendarConfiguration30.setKey("END");
        calendarConfiguration30.setValue("VTIMEZONE");
        calendarConfiguration30.setOrderNumber(30);
        final CalendarConfiguration createdCalendarConfiguration30 = calendarConfigurationRepository.insert(calendarConfiguration30);
        Assert.assertNotNull(createdCalendarConfiguration30);

        final CalendarConfiguration calendarConfiguration31 = new CalendarConfiguration();
        calendarConfiguration31.setCalendarConfigurationType(CalendarConfigurationTypeEnum.HEADER_END);
        calendarConfiguration31.setKey("END");
        calendarConfiguration31.setValue("VCALENDAR");
        calendarConfiguration31.setOrderNumber(300);
        final CalendarConfiguration createdCalendarConfiguration31 = calendarConfigurationRepository.insert(calendarConfiguration31);
        Assert.assertNotNull(createdCalendarConfiguration31);

        /**************************/

        final CalendarConfiguration calendarConfiguration32 = new CalendarConfiguration();
        calendarConfiguration32.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration32.setKey("BEGIN");
        calendarConfiguration32.setValue("VEVENT");
        calendarConfiguration32.setOrderNumber(32);
        final CalendarConfiguration createdCalendarConfiguration32 = calendarConfigurationRepository.insert(calendarConfiguration32);
        Assert.assertNotNull(createdCalendarConfiguration32);

        final CalendarConfiguration calendarConfiguration33 = new CalendarConfiguration();
        calendarConfiguration33.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration33.setKey("CLASS");
        calendarConfiguration33.setValue("PUBLIC");
        calendarConfiguration33.setOrderNumber(33);
        final CalendarConfiguration createdCalendarConfiguration33 = calendarConfigurationRepository.insert(calendarConfiguration33);
        Assert.assertNotNull(createdCalendarConfiguration33);

        final CalendarConfiguration calendarConfiguration34 = new CalendarConfiguration();
        calendarConfiguration34.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration34.setKey("CREATED");
        calendarConfiguration34.setValue("");
        calendarConfiguration34.setOrderNumber(34);
        final CalendarConfiguration createdCalendarConfiguration34 = calendarConfigurationRepository.insert(calendarConfiguration34);
        Assert.assertNotNull(createdCalendarConfiguration34);

        final CalendarConfiguration calendarConfiguration35 = new CalendarConfiguration();
        calendarConfiguration35.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration35.setKey("DESCRIPTION");
        calendarConfiguration35.setValue("");
        calendarConfiguration35.setOrderNumber(35);
        final CalendarConfiguration createdCalendarConfiguration35 = calendarConfigurationRepository.insert(calendarConfiguration35);
        Assert.assertNotNull(createdCalendarConfiguration35);

        final CalendarConfiguration calendarConfiguration36 = new CalendarConfiguration();
        calendarConfiguration36.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration36.setKey("DTEND;TZID=\"W. Europe Standard Time\"");
        calendarConfiguration36.setValue("");
        calendarConfiguration36.setOrderNumber(36);
        final CalendarConfiguration createdCalendarConfiguration36 = calendarConfigurationRepository.insert(calendarConfiguration36);
        Assert.assertNotNull(createdCalendarConfiguration36);

        final CalendarConfiguration calendarConfiguration37 = new CalendarConfiguration();
        calendarConfiguration37.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration37.setKey("DTSTAMP");
        calendarConfiguration37.setValue("");
        calendarConfiguration37.setOrderNumber(37);
        final CalendarConfiguration createdCalendarConfiguration37 = calendarConfigurationRepository.insert(calendarConfiguration37);
        Assert.assertNotNull(createdCalendarConfiguration37);

        final CalendarConfiguration calendarConfiguration38 = new CalendarConfiguration();
        calendarConfiguration38.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration38.setKey("DTSTART;TZID=\"W. Europe Standard Time\"");
        calendarConfiguration38.setValue("");
        calendarConfiguration38.setOrderNumber(38);
        final CalendarConfiguration createdCalendarConfiguration38 = calendarConfigurationRepository.insert(calendarConfiguration38);
        Assert.assertNotNull(createdCalendarConfiguration38);

        final CalendarConfiguration calendarConfiguration39 = new CalendarConfiguration();
        calendarConfiguration39.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration39.setKey("LAST-MODIFIED");
        calendarConfiguration39.setValue("");
        calendarConfiguration39.setOrderNumber(39);
        final CalendarConfiguration createdCalendarConfiguration39 = calendarConfigurationRepository.insert(calendarConfiguration39);
        Assert.assertNotNull(createdCalendarConfiguration39);

        final CalendarConfiguration calendarConfiguration40 = new CalendarConfiguration();
        calendarConfiguration40.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration40.setKey("LOCATION");
        calendarConfiguration40.setValue("");
        calendarConfiguration40.setOrderNumber(40);
        final CalendarConfiguration createdCalendarConfiguration40 = calendarConfigurationRepository.insert(calendarConfiguration40);
        Assert.assertNotNull(createdCalendarConfiguration40);

        final CalendarConfiguration calendarConfiguration41 = new CalendarConfiguration();
        calendarConfiguration41.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration41.setKey("PRIORITY");
        calendarConfiguration41.setValue("5");
        calendarConfiguration41.setOrderNumber(41);
        final CalendarConfiguration createdCalendarConfiguration41 = calendarConfigurationRepository.insert(calendarConfiguration41);
        Assert.assertNotNull(createdCalendarConfiguration41);

        final CalendarConfiguration calendarConfiguration42 = new CalendarConfiguration();
        calendarConfiguration42.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration42.setKey("SEQUENCE");
        calendarConfiguration42.setValue("0");
        calendarConfiguration42.setOrderNumber(42);
        final CalendarConfiguration createdCalendarConfiguration42 = calendarConfigurationRepository.insert(calendarConfiguration42);
        Assert.assertNotNull(createdCalendarConfiguration42);

        final CalendarConfiguration calendarConfiguration43 = new CalendarConfiguration();
        calendarConfiguration43.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration43.setKey("SUMMARY;LANGUAGE=de-at");
        calendarConfiguration43.setValue("");
        calendarConfiguration43.setOrderNumber(43);
        final CalendarConfiguration createdCalendarConfiguration43 = calendarConfigurationRepository.insert(calendarConfiguration43);
        Assert.assertNotNull(createdCalendarConfiguration43);

        final CalendarConfiguration calendarConfiguration44 = new CalendarConfiguration();
        calendarConfiguration44.setCalendarConfigurationType(CalendarConfigurationTypeEnum.MESSAGE);
        calendarConfiguration44.setKey("END");
        calendarConfiguration44.setValue("VEVENT");
        calendarConfiguration44.setOrderNumber(44);
        final CalendarConfiguration createdCalendarConfiguration44 = calendarConfigurationRepository.insert(calendarConfiguration44);
        Assert.assertNotNull(createdCalendarConfiguration44);
    }
}
