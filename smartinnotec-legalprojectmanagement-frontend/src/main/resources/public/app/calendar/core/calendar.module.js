(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();