(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.factory('userService', userService);
        
    userService.$inject = ['$http', 'authTokenService', 'authService', 'authUserService', 'api_config', 'token_config'];
    
    function userService($http, authTokenService, authService, authUserService, api_config, token_config) {
		var service = {
			logout: logout,
			findAllUsers: findAllUsers,
			updateUser: updateUser,
			deleteUser: deleteUser, 
			updatePassword: updatePassword,
			getUserById: getUserById,
			getHighestRoleOfUser: getHighestRoleOfUser,
			findUsersByTerm: findUsersByTerm,
			canUserSeeConfidentialDocumentFiles: canUserSeeConfidentialDocumentFiles,
			getUserAuthorizationUserContainerOfUser: getUserAuthorizationUserContainerOfUser,
			createUserAuthorizationUserContainer: createUserAuthorizationUserContainer,
			updateUserAuthorizationUserContainerOfUser: updateUserAuthorizationUserContainerOfUser,
			deleteUserAuthorizationUserContainerOfUser: deleteUserAuthorizationUserContainerOfUser
		};
		
		return service;
		
		////////////
		
		function logout() {
			authUserService.setCurrentUser(false);
			authTokenService.deleteToken();
		}
		
		function updateUser(updateUser) {
			return $http.put(api_config.BASE_URL + '/users/' + updateUser.id, updateUser);
		}
		
		function deleteUser(user) {
			return $http.delete(api_config.BASE_URL + '/users/' + user.id);
		}
		
		function updatePassword(userId, password) {
			return $http.put(api_config.BASE_URL + '/users/' + userId + '/password', password);
		}
		
		function findAllUsers() {
			return $http.get(api_config.BASE_URL + '/users');
		}
		
		function getUserById(userId) {
			return $http.get(api_config.BASE_URL + '/users/' + userId);
		}
		
		function getHighestRoleOfUser() {
			return $http.get(api_config.BASE_URL + '/users/highestrole').then(function successCallback(response) {
    			return response.data;
    		}, function errorCallback(response) {
    			console.log('error in user.service.js#getHighestRoleOfUser');
    		});
		}
		
		function findUsersByTerm(term) {
			return $http.get(api_config.BASE_URL + '/users/' + term + '/search');
		}
		
		function canUserSeeConfidentialDocumentFiles(userId) {
			return $http.get(api_config.BASE_URL + '/user/confidentialdocumentfiles/' + userId);
		}
		
		function getUserAuthorizationUserContainerOfUser(userId) {
			return $http.get(api_config.BASE_URL + '/users/userauthorizationusercontainer/' + userId);
		}
		
		function createUserAuthorizationUserContainer(userId, projectUserConnectionRole) {
			return $http.post(api_config.BASE_URL + '/users/userauthorizationusercontainer/' + userId + '/' + projectUserConnectionRole);
		}
		
		function updateUserAuthorizationUserContainerOfUser(userAuthorizationUserContainer) {
			return $http.put(api_config.BASE_URL + '/users/userauthorizationusercontainer', userAuthorizationUserContainer);
		}
		
		function deleteUserAuthorizationUserContainerOfUser(userId, id) {
			return $http.delete(api_config.BASE_URL + '/users/userauthorizationusercontainer/' + userId + '/' + id);
		}
    }
})();