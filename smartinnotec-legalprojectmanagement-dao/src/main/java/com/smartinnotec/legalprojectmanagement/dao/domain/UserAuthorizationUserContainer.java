package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;

import lombok.Data;

@Document(collection = "UserAuthorizationUserContainer")
public @Data class UserAuthorizationUserContainer {

    @Id
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String id;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String userId;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private List<UserAuthorization> userAuthorizations;

    public UserAuthorizationUserContainer deepCopy() {
        final UserAuthorizationUserContainer copiedUserAuthorizationUserContainer = new UserAuthorizationUserContainer();
        copiedUserAuthorizationUserContainer.setUserId(userId);
        final List<UserAuthorization> copiedUserAuthorizations = new ArrayList<>();
        for (final UserAuthorization userAuthorization : userAuthorizations) {
            final UserAuthorization copiedUserAuthorization = userAuthorization.deepCopy();
            copiedUserAuthorizations.add(copiedUserAuthorization);
        }
        copiedUserAuthorizationUserContainer.setUserAuthorizations(copiedUserAuthorizations);
        return copiedUserAuthorizationUserContainer;
    }
}
