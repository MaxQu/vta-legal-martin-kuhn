package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.SearchHistory;

public interface SearchHistoryRepository extends MongoRepository<SearchHistory, String> {

    Page<SearchHistory> findPagedSearchHistoryByUserIdOrderBySearchDateDesc(final String userId, final Pageable pageable);

    SearchHistory findByUserIdAndSearchString(final String userId, final String searchString);

}
