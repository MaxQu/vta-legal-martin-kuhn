package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;

@Component
public class ContactComparation {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UtilService utilService;

    public boolean contactChanged(final Contact existingContact, final Contact changedContact) throws BusinessException {
        BusinessAssert.notNull(changedContact, "changedContact is mandatory in ContactComparation#contactChanged");
        logger.info(
            "contact changed detected with existing contact with id '{}' and changed contact with id '{}' in ContactComparation#contactChanged",
            (existingContact != null ? existingContact.getId() : null), changedContact.getId());

        // if imported contact is new
        if (existingContact == null) {
            changedContact.setNewContact(true);
            return true;
        }

        boolean contactChanged = false;

        if (utilService.hasStringPropertyChanged(existingContact.getInstitution(), changedContact.getInstitution())) {
            changedContact.setInstitutionChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingContact.getAdditionalNameInformation(),
            changedContact.getAdditionalNameInformation())) {
            changedContact.setAdditionalNameInformationChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingContact.getContactPerson(), changedContact.getContactPerson())) {
            changedContact.setContactPersonChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingContact.getShortCode(), changedContact.getShortCode())) {
            changedContact.setShortCodeChanged(true);
            contactChanged = true;
        }
        if (utilService.hasCustomerNumberPropertyChanged(existingContact.getCustomerNumberContainers(),
            changedContact.getCustomerNumberContainers())) {
            changedContact.setCustomerNumberContainersChanged(true);
            contactChanged = true;
        }
        if (utilService.hasAgentNumberPropertyChanged(existingContact.getAgentNumberContainers(),
            changedContact.getAgentNumberContainers())) {
            changedContact.setAgentNumberContainersChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringListChanged(existingContact.getTelephones(), changedContact.getTelephones())) {
            changedContact.setTelephonesChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringListChanged(existingContact.getEmails(), changedContact.getEmails())) {
            changedContact.setEmailsChanged(true);
            contactChanged = true;
        }

        final Address existingAddress = existingContact.getAddress();
        final Address changedAddress = changedContact.getAddress();

        final boolean addressChanged = compareAddresses(existingAddress, changedAddress);
        contactChanged = !contactChanged ? addressChanged : contactChanged;

        // contactAddressWithProducts
        final List<ContactAddressWithProducts> existingContactAddressesWithProducts = existingContact.getAddressesWithProducts();
        final List<ContactAddressWithProducts> contactAddressesWithProducts = changedContact.getAddressesWithProducts();

        if (contactAddressesWithProductsHasDifferentSize(existingContactAddressesWithProducts, contactAddressesWithProducts)) {
            changedContact.setAddressesWithProductsChanged(true);
            contactChanged = true;
        }
        if (contactAddressesWithProductsHasNotSameFlfId(existingContactAddressesWithProducts, contactAddressesWithProducts)) {
            changedContact.setAddressesWithProductsChanged(true);
            contactChanged = true;
        }
        if (existingContactAddressesWithProducts != null && contactAddressesWithProducts != null) {
            if (existingContactAddressesWithProducts.size() > contactAddressesWithProducts.size()) {
                for (final ContactAddressWithProducts existingContactAddressWithProducts : existingContactAddressesWithProducts) {
                    final ContactAddressWithProducts foundedContactAddressWithProducts = getContactAddressWithProducts(
                        existingContactAddressWithProducts, contactAddressesWithProducts);
                    if (foundedContactAddressWithProducts == null) {
                        return true;
                    }
                    final Address existingAddressOfContactAddressWithProducts = existingContactAddressWithProducts.getAddress();
                    final Address addressOfContactAddressWithProducts = foundedContactAddressWithProducts.getAddress();
                    final boolean contactImportedChanged = compareAddresses(existingAddressOfContactAddressWithProducts,
                        addressOfContactAddressWithProducts);
                    if (contactImportedChanged) {
                        return true;
                    }
                }
            } else if (existingContactAddressesWithProducts.size() < contactAddressesWithProducts.size()
                || existingContactAddressesWithProducts.size() == contactAddressesWithProducts.size()) {
                for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
                    final ContactAddressWithProducts foundedContactAddressWithProducts = getContactAddressWithProducts(
                        contactAddressWithProducts, existingContactAddressesWithProducts);
                    if (foundedContactAddressWithProducts == null) {
                        return true;
                    }
                    final Address addressOfContactAddressWithProducts = contactAddressWithProducts.getAddress();
                    final Address addressOfFoundedContactAddressWithProducts = foundedContactAddressWithProducts.getAddress();
                    final boolean contactImportedChanged = compareAddresses(addressOfContactAddressWithProducts,
                        addressOfFoundedContactAddressWithProducts);
                    if (contactImportedChanged) {
                        return true;
                    }
                }
            }
        }
        return contactChanged;
    }

    // contactImportedAddressImported -> equal with ContactAddressWithProducts
    private ContactAddressWithProducts getContactAddressWithProducts(final ContactAddressWithProducts contactAddressesWithProducts,
            final List<ContactAddressWithProducts> contactsImportedAddressesImported) {
        logger.info("get contact in ContactComparation#getContactAddressWithProducts");
        for (final ContactAddressWithProducts contactImportedAddressImported : contactsImportedAddressesImported) {
            if (contactImportedAddressImported.getAddress().getFlfId() != null
                && contactAddressesWithProducts.getAddress().getFlfId() != null
                && contactImportedAddressImported.getAddress().getFlfId().equals(contactAddressesWithProducts.getAddress().getFlfId())) {
                return contactImportedAddressImported;
            }
        }
        return null;
    }

    private boolean contactAddressesWithProductsHasDifferentSize(final List<ContactAddressWithProducts> existingContactsAddressWithProducts,
            final List<ContactAddressWithProducts> contactAddressWithProducts) {
        if (((existingContactsAddressWithProducts == null || existingContactsAddressWithProducts.isEmpty())
            && (contactAddressWithProducts != null && !contactAddressWithProducts.isEmpty()))
            || ((existingContactsAddressWithProducts != null && !existingContactsAddressWithProducts.isEmpty())
                && (contactAddressWithProducts == null || contactAddressWithProducts.isEmpty()))
            || (existingContactsAddressWithProducts != null && contactAddressWithProducts != null
                && existingContactsAddressWithProducts.size() != contactAddressWithProducts.size())) {
            return true;
        }
        return false;
    }

    private boolean contactAddressesWithProductsHasNotSameFlfId(final List<ContactAddressWithProducts> existingContactAddressesWithProducts,
            final List<ContactAddressWithProducts> contactAddressesWithProducts) {
        if (existingContactAddressesWithProducts != null) {
            for (final ContactAddressWithProducts existingContactAddressWithProducts : existingContactAddressesWithProducts) {
                final String flfId = existingContactAddressWithProducts.getAddress().getFlfId();
                boolean exists = false;
                for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
                    final String importedFlfId = contactAddressWithProducts.getAddress().getFlfId();
                    if (flfId != null && importedFlfId != null && flfId.equals(importedFlfId)) {
                        exists = true;
                    }
                }
                if (exists == false) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    private boolean compareAddresses(final Address existingAddress, final Address changedAddress) throws BusinessException {
        logger.info("compare addresses in ContactComparation#compareAddresses");
        if (existingAddress == null && changedAddress == null) {
            return false;
        }

        boolean contactChanged = false;
        if (utilService.hasStringPropertyChanged(existingAddress.getFlfId(), changedAddress.getFlfId())) {
            changedAddress.setFlfIdChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingAddress.getAdditionalInformation(), changedAddress.getAdditionalInformation())) {
            changedAddress.setAdditionalInformationChanged(true);
            contactChanged = true;
        }
        if (utilService.hasCustomerNumberPropertyChanged(existingAddress.getCustomerNumberContainers(),
            changedAddress.getCustomerNumberContainers())) {
            changedAddress.setCustomerNumberContainersChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingAddress.getStreet(), changedAddress.getStreet())) {
            changedAddress.setStreetChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingAddress.getIntSign(), changedAddress.getIntSign())) {
            changedAddress.setIntSignChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingAddress.getPostalCode(), changedAddress.getPostalCode())) {
            changedAddress.setPostalCodeChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingAddress.getRegion(), changedAddress.getRegion())) {
            changedAddress.setRegionChanged(true);
            contactChanged = true;
        }
        if ((existingAddress.getProvinceType() == null && changedAddress.getProvinceType() != null)
            || (existingAddress.getProvinceType() != null && changedAddress.getProvinceType() == null)
            || (existingAddress.getProvinceType() != null && changedAddress.getProvinceType() != null && utilService
                .hasStringPropertyChanged(existingAddress.getProvinceType().toString(), changedAddress.getProvinceType().toString()))) {
            changedAddress.setProvinceTypeChanged(true);
            contactChanged = true;
        }
        if ((existingAddress.getCountry() != null && changedAddress.getCountry() == null)
            || (existingAddress.getCountry() == null && changedAddress.getCountry() != null)
            || (existingAddress.getCountry() != null && changedAddress.getCountry() != null
                && utilService.hasStringPropertyChanged(existingAddress.getCountry().toString(), changedAddress.getCountry().toString()))) {
            changedAddress.setCountryChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingAddress.getTelephone(), changedAddress.getTelephone())) {
            changedAddress.setTelephoneChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingAddress.getEmail(), changedAddress.getEmail())) {
            changedAddress.setEmailChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(existingAddress.getInformation(), changedAddress.getInformation())) {
            changedAddress.setInformationChanged(true);
            contactChanged = true;
        }
        return contactChanged;
    }
}
