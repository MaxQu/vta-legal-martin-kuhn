package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "CalendarEventScheduleDays")
public @Data class CalendarEventScheduleDays {

    @Id
    private String id;
    private Integer days;
    @DBRef
    private Tenant tenant;
}
