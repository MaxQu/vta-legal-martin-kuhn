(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('projectTemplateService', projectTemplateService);
        
    projectTemplateService.$inject = ['$http', 'api_config', 'pdfTemplateModalService', 'imgTemplateModalService', 'textTemplateModalService', 'htmlTemplateModalService', 'noTemplateModalService'];
    
    function projectTemplateService($http, api_config, pdfTemplateModalService, imgTemplateModalService, textTemplateModalService, htmlTemplateModalService, noTemplateModalService) {
    	var imgEndings = ['png', 'PNG', 'jpg', 'JPG', 'JPEG', 'jpeg'];
		var service = {
			showTemplateModal: showTemplateModal
		};
		return service;
		
		////////////
		
		function showTemplateModal(url, documentFile, documentFileVersion) {
    		if(documentFileVersion == null) {
    			return;
    		}
    		if(documentFile.ending.indexOf('pdf') > -1 || documentFile.ending.indexOf('PDF') > -1 || documentFile.ending.indexOf('docx') > -1) {
    			pdfTemplateModalService.showPdfTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			} else if(containsImgEnding(documentFile.ending)) {
				imgTemplateModalService.showImgTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			} else if(documentFile.ending.indexOf('msg') > -1 || documentFile.ending.indexOf('txt') > -1) {
				textTemplateModalService.showTextTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			} else if(documentFile.ending.indexOf('html') > -1) {
				htmlTemplateModalService.showHtmlTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			} else {
				noTemplateModalService.showNoTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			}
    	}
		
		function containsImgEnding(ending) {
    		for(var i = 0; i < imgEndings.length; i++) {
    			if(imgEndings[i] === ending) {
    				return true;
    			}
    		}
    		return false;
    	}
    }
})();