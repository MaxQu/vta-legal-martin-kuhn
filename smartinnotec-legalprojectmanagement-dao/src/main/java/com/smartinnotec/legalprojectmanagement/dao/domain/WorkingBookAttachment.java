package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class WorkingBookAttachment {

    private String filepath;
    private String filename;
    private String originalFilename;
    private boolean isImage;

}
