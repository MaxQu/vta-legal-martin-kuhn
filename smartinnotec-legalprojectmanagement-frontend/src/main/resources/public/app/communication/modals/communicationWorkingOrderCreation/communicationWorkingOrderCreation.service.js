(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationWorkingOrderCreationModalService', communicationWorkingOrderCreationModalService);
        
    communicationWorkingOrderCreationModalService.$inject = ['$modal', '$stateParams', 'projectUserConnectionService', 'communicationService', 'dateUtilityService', 'communicationAttachmentFilesModalService', 'uploadService'];
    
    function communicationWorkingOrderCreationModalService($modal, $stateParams, projectUserConnectionService, communicationService, dateUtilityService, communicationAttachmentFilesModalService, uploadService) {
		var service = {
			showCommunicationWorkingOrderCreationModal: showCommunicationWorkingOrderCreationModal
		};		
		return service;
		
		////////////
				
		function showCommunicationWorkingOrderCreationModal(invoker, currentUser, mode, selectedFilterProject, projects, usersOfProject, communicationUserConnection) {
			 var communicationGroupCreationModal = $modal.open({
				controller: CommunicationWorkingOrderCreationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	mode: function() {
			    		return mode;
			    	},
			    	selectedFilterProject: function() {
			    		return selectedFilterProject;
			    	},
			    	projects: function() {
			    		return projects;
			    	}, 
			    	usersOfProject: function() {
			    		return usersOfProject;
			    	}, 
			    	communicationUserConnection: function() {
			    		return communicationUserConnection;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/communicationWorkingOrderCreation/communicationWorkingOrderCreation.modal.html'
			});
			return communicationGroupCreationModal;
			
			function CommunicationWorkingOrderCreationModalController($modalInstance, $scope, invoker, currentUser, mode, selectedFilterProject, projects, usersOfProject, communicationUserConnection) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.currentUser = currentUser;
		    	vm.name = vm.currentUser.title + " " + vm.currentUser.firstname + " " + vm.currentUser.surname;
		    	vm.mode = mode;
		    	vm.communicationUserConnection = communicationUserConnection;
		    	vm.selectedFilterProject = vm.mode == 'EDIT' ? communicationUserConnection.project : selectedFilterProject;
		    	vm.projects = projects;
		    	vm.usersOfProject = usersOfProject;
		    	vm.executionDateDatePicker = false;
		    	vm.uploadFiles = false;
		    	vm.message = vm.mode == 'EDIT' ? vm.communicationUserConnection.communication.title + ' ' + vm.communicationUserConnection.communication.message : '';
		    	vm.message = prepareMessage(vm.message);
		    	vm.communicationWorkingOrder = {
		    		id: vm.currentUser.id,
		    		name: vm.name,
		    		message: vm.mode == 'EDIT' ? vm.message : '',
		    		associatedToGroup: null,
		    		project: vm.selectedFilterProject,
		    		communicationGroupIds: null,
		    		usersSelected: vm.mode == 'EDIT' ? [vm.communicationUserConnection.userReceivedMessage] : [],
		    		confirmation: true,
		    		elapsedDate: null,
		    		executionDate: vm.mode == 'EDIT' ? dateUtilityService.convertToDateOrUndefined(vm.communicationUserConnection.executionDate) : null,
		    		relevantForWorkingBook: false,
		    		selectedDocumentFiles: vm.mode == 'EDIT' ? prepareSelectedDocumentFiles(communicationUserConnection.communication.attachedFileUrls) : [],
		    		communicationAttachments: vm.mode == 'EDIT' ? communicationUserConnection.communication.communicationAttachments : []
		    	};
		    	
		    	vm.openEndDatePicker = openEndDatePicker;
		    	vm.addUser = addUser;
		    	vm.addProject = addProject;
		    	vm.removeProject = removeProject;
		    	vm.userStillSelected = userStillSelected;
		    	vm.removeUser = removeUser;
		    	vm.addAttachmentFiles = addAttachmentFiles;
		    	vm.attachFiles = attachFiles;
		    	vm.removeAttachment = removeAttachment;
		    	vm.removeAttachmentDocumentFile = removeAttachmentDocumentFile;
		    	vm.createCommunicationWorkingOrder = createCommunicationWorkingOrder;
		    	vm.closeCommunicationWorkingOrderCreationModal = closeCommunicationWorkingOrderCreationModal;
		    	
		    	////////////
		    	
		    	function prepareSelectedDocumentFiles(attachedFileUrls) {
		    		var selectedDocumentFiles = [];
		    		if(attachedFileUrls == null) {
		    			return selectedDocumentFiles;
		    		}
		    		for(var i = 0; i < attachedFileUrls.length; i++) {
		    			var selectedDocumentFile = {};
		    			var selectedDocumentFileContainer = {};
		    			selectedDocumentFileContainer.projectId = attachedFileUrls[i].projectId;
		    			selectedDocumentFileContainer.id = attachedFileUrls[i].documentFileId;
		    			selectedDocumentFileContainer.fileName = attachedFileUrls[i].fileName;
		    			selectedDocumentFileContainer.url = attachedFileUrls[i].url;
		    			selectedDocumentFile.selectedDocumentFile = selectedDocumentFileContainer;
		    			
		    			var documentFileVersion = {};
		    			documentFileVersion.version = attachedFileUrls[i].version; 
		    			selectedDocumentFile.documentFileVersion = documentFileVersion;
		    			selectedDocumentFiles.push(selectedDocumentFile);
		    		}
		    		return selectedDocumentFiles;
		    	}
		    	
		    	function prepareMessage(message) {
		    		while(message.indexOf('<br />') > -1) {
		    			message = message.replace('<br />', '\n');
		    	      }
		    		return message;
		    	}
		    	
		    	function addProject(project) {
		    		projectUserConnectionService.findProjectUserConnectionByProject(project.id).then(function successCallback(response) {
		    			removeProjectSelection();
		    			vm.communicationWorkingOrder.usersSelected = [];
		    			vm.usersOfProject = response.data;
		    			project.selected = true;
			    		vm.selectedFilterProject = project;
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error communication.controller#createNewWorkingOrderEntry');
		   	   		 });
		    	}
		    	
		    	function addAttachmentFiles() {
		    		communicationAttachmentFilesModalService.showCommunicationAttachmentFilesModal(vm);
		    	}
		    	
		    	function attachFiles(selectedDocumentFiles) {
		    		for(var i = 0; i < selectedDocumentFiles.length; i++) {
		    			vm.communicationWorkingOrder.selectedDocumentFiles.push(selectedDocumentFiles[i]);
		    		}
		    	}
		    	
		    	$scope.uploadExternalFiles = function(files, errFiles) {
		      		uploadService.uploadGeneralFiles('communication', files, $scope.successCallback, $scope.progressCallback);
		    	};
		    	
		    	$scope.progressCallback = function(response) {
		    		vm.uploadFiles = true;
		    		console.log("file upload progress in communicationWorkingOrderCreation.service#progressCallback");
		    	};
		    	
		    	$scope.successCallback = function(response) {
		    		var fileUploadResponseContainer = response.data;
		    		$scope.initializeAttachments();
		    		vm.communicationWorkingOrder.communicationAttachments.push(fileUploadResponseContainer);
		    		vm.uploadFiles = false;
		    		console.log("file upload success in communicationWorkingOrderCreation.service#successCallback");
		    	};
		    	
		    	$scope.initializeAttachments = function() {
		    		if(vm.communicationWorkingOrder.communicationAttachments == null || typeof vm.communicationWorkingOrder.communicationAttachments == 'undefined') {
		    			vm.communicationWorkingOrder.communicationAttachments = [];
		    		}
		    	};
		    	
		    	function removeAttachment(attachment) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.communicationAttachments.length; i++) {
		    			if(vm.communicationWorkingOrder.communicationAttachments[i].filename == attachment.filename) {
		    				vm.communicationWorkingOrder.communicationAttachments.splice(i, 1);
		    			}
		    		}
		    	}
		    	
		    	function removeAttachmentDocumentFile(selectedDocumentFile) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.selectedDocumentFiles.length; i++) {
		    			if(vm.communicationWorkingOrder.selectedDocumentFiles[i].fileName == selectedDocumentFile.fileName) {
		    				vm.communicationWorkingOrder.selectedDocumentFiles.splice(i, 1);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function addUser(user) { 
		    		if(userStillAdded(user) === false) {
		    			vm.noUsersSelected = false;
		    			vm.communicationWorkingOrder.usersSelected.push(user);
		    		}
		    	}
		    	
		    	function userStillAdded(user) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.usersSelected.length; i++) {
		    			if(vm.communicationWorkingOrder.usersSelected[i].id == user.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function removeProject(project) {
		    		vm.communicationWorkingOrder.project = null;
		    		vm.communicationWorkingOrder.usersSelected = [];
		    		vm.usersOfProject = [];
		    		project.selected = false;
		    		vm.selectedFilterProject = null;
		    	}
		    	
		    	function userStillSelected(user) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.usersSelected.length; i++) {
		    			if(user != null && vm.communicationWorkingOrder.usersSelected[i].id == user.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function removeUser(user) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.usersSelected.length; i++) {
		    			if(vm.communicationWorkingOrder.usersSelected[i].id == user.id) {
		    				vm.communicationWorkingOrder.usersSelected.splice(i, 1);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function removeProjectSelection() {
		    		for(var i = 0; i < vm.projects.length; i++) {
		    			vm.projects[i].selected = false;
		    		}
		    	}
		    	
		    	function openEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.executionDateDatePicker = true;
	            }
		    	
		    	function createCommunicationWorkingOrder() {
		    		if(vm.communicationWorkingOrder.usersSelected.length === 0) {
		    			vm.noUsersSelected = true;
		    			return;
		    		}
		    		if(vm.selectedFilterProject == null) {
		    			return;
		    		}
		    		vm.communicationWorkingOrder.project = vm.selectedFilterProject;
		    		var userIds = [];
		    		for(var i = 0; i < vm.communicationWorkingOrder.usersSelected.length; i++) {
		    			userIds.push(vm.communicationWorkingOrder.usersSelected[i].id);
		    		}
		    		vm.communicationWorkingOrder.usersSelected = userIds;
		    		
		    		if(vm.mode == 'CREATE') {
			    		communicationService.createCommunicationUserConnectionMessages(vm.currentUser.id, vm.communicationWorkingOrder).then(function successCallback(response) {
			    			var communicationUserConnections = response.data;
			    			invoker.addCommunicationUserConnectionToSentAndOpenOrders(communicationUserConnections);
			   	   		 }, function errorCallback(response) {
			   	   			  console.log('error communicationWorkingOrderCreation.service#createCommunicationWorkingOrder');
			   	   		 });
		    		} else if(vm.mode == 'EDIT') {
		    			vm.communicationWorkingOrder.communicationUserConnectionId = vm.communicationUserConnection.id;
		    			communicationService.updateCommunicationUserConnectionMessages(vm.communicationWorkingOrder).then(function successCallback(response) {
		    				var communicationUserConnections = response.data;
			    			invoker.updateCommunicationUserConnectionToSentAndOpenOrders(communicationUserConnections);
			   	   		 }, function errorCallback(response) {
			   	   			  console.log('error communicationWorkingOrderCreation.service#updateCommunicationWorkingOrder');
			   	   		 });
		    		}
		    		
		    		closeCommunicationWorkingOrderCreationModal();
		    	}
		    	
		    	function closeCommunicationWorkingOrderCreationModal() {
					$modalInstance.dismiss('cancel');
					}
			} 
		}
    }
})();
