package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum DocumentFileStateEnum {

    BLOCKED("BLOCKED"),
    NOT_BLOCKED("NOT_BLOCKED");

    private String value;

    private DocumentFileStateEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
