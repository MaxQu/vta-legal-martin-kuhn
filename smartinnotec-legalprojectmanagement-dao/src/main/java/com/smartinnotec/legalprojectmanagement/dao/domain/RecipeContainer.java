package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class RecipeContainer {

    private String date;
    private String productName;
    private String recipeProducts;
    private String information;
    private Integer amountInKilo;
    private String recipeType;
    private String creationDate;
}
