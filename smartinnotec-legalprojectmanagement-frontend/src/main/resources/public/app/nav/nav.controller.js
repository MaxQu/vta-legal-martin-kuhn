(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.nav')
    	.controller('NavController', NavController);
    
    NavController.$inject = ['$scope', '$state', 'currentUser'];
    
    function NavController($scope, $state, currentUser) {
	    $scope.vm = this;
    	var vm = this;
    	vm.state = $state;
    	
    	vm.currentUser = currentUser;
    }
})();
