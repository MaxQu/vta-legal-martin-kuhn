(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.superadministration', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();