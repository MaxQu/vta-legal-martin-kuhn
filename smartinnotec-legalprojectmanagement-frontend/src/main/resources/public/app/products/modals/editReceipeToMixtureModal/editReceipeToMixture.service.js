(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('editReceipeToMixtureModalService', editReceipeToMixtureModalService);
        
    editReceipeToMixtureModalService.$inject = ['$modal', '$stateParams', 'productsService', 'receipeService'];
    
    function editReceipeToMixtureModalService($modal, $stateParams, productsService, receipeService) {
		var service = {
			showEditReceipeToMixtureModal: showEditReceipeToMixtureModal
		};		
		return service;
		
		////////////
				
		function showEditReceipeToMixtureModal(currentUser, product, products, lastReceipeOfProduct, userAuthorizationUserContainer, unityTypes) {
			 var editReceipeToMixtureModal = $modal.open({
				controller: EditReceipeToMixtureModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	lastReceipeOfProduct: function() {
			    		return lastReceipeOfProduct;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	unityTypes: function() {
			    		return unityTypes;
			    	}
			    }, 
				templateUrl: 'app/products/modals/editReceipeToMixtureModal/editReceipeToMixture.modal.html'
			});
			return editReceipeToMixtureModal;
			
			function EditReceipeToMixtureModalController($modalInstance, $scope, currentUser, product, products, lastReceipeOfProduct, userAuthorizationUserContainer, unityTypes) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.lastReceipeOfProduct = lastReceipeOfProduct;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.unityTypes = unityTypes;
		    	
		    	vm.storeEditReceipeToMixture = storeEditReceipeToMixture;
		    	vm.closeEditReceipeToMixtureModal = closeEditReceipeToMixtureModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	function storeEditReceipeToMixture() {
		    		vm.lastReceipeOfProduct.receipeType = 'CHANGED_MIXTURE';
		    		vm.lastReceipeOfProduct.id = null;
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		receipeService.createReceipe(vm.lastReceipeOfProduct).then(function(response) {
		    			updateProduct(vm.product.id); 
		    			vm.closeEditReceipeToMixtureModal();
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error editReceipeToMixture.service#storeEditReceipeToMixture'); 
		  	   		 });
		    	}
		    	
		    	function updateProduct(productId) {
		    		productsService.findProduct(productId).then(function(response) {
		    			var product = response.data;
		    			for(var i = 0; i < vm.products.length; i++) {
		    				if(vm.products[i].id == productId) {
		    					var selected = vm.products[i].selected;
		    					vm.products[i] = product;
		    					vm.products[i].selected = selected;
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error editReceipeToMixture.service#updateProduct'); 
		  	   		 });
		    	}
		    	
		    	function closeEditReceipeToMixtureModal() {
					$modalInstance.dismiss('cancel');  
				}
			} 
		}
    }
})();