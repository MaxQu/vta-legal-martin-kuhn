package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.UserRepository;

@Service("userService")
public class UserServiceImpl extends AbstractService implements UserService {

    @Autowired
    protected UserRepository userRepository;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;

    @Override
    public List<User> findAllUsers() throws BusinessException {
        logger.debug("get all users in UserServiceImpl#getAllUsers");
        return userRepository.findAll();
    }

    @Override
    public List<User> findAllUsersByTenant(final Tenant tenant) throws BusinessException {
        if (tenant == null) {
            return new ArrayList<>();
        }
        logger.info("find all users by tenant with id '{}' in UserServiceImpl#findAllUsersByTenant", tenant.getId());
        return userRepository.findByTenant(tenant);
    }

    @Override
    public Tenant getTenantOfUser(final String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in UserServiceImpl#getTenantOfUser", "400");
        BusinessAssert.isId(userId, "userId must be an id in UserServiceImpl#getTenantOfUser", "400");
        logger.info("get tenant of user with id '{}' in UserServiceImpl#getTenantOfUser", userId);
        final User user = this.findUser(userId);
        return user.getTenant();
    }

    @Override
    public User findUser(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in UserServiceImpl#get", "400");
        logger.debug("get user in UserServiceImpl#get");
        return userRepository.findOne(id);
    }

    @Override
    public List<User> findUserBySearchString(final String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in UserServiceImpl#findUserBySearchString", "400");
        logger.debug("search user by search string '{}' in UserServiceImpl#findUserBySearchString", searchString);
        return userRepository.findUserBySearchString(searchString);
    }

    @Override
    public User createUser(final User user) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in UserServiceImpl#createUser");
        logger.debug("create user in UserServiceImpl#createUser");
        return userRepository.insert(user);
    }

    @Override
    public User updateUser(final User user) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in UserServiceImpl#updateUser", "400");
        logger.debug("update user with id '{}' in UserServiceImpl#updateUser", user.getId());
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in UserServiceImpl#deleteUser", "400");
        BusinessAssert.isId(id, "id must be an id in UserServiceImpl#deleteUser", "400");
        logger.debug("delete user with id '{}' in UserServiceImpl#deleteUser", id);
        final User user = this.findUser(id);
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);
        if (projectUserConnections == null || projectUserConnections.isEmpty()) {
            userRepository.delete(id);
        } else {
            throw new BusinessException("PROJECT_USER_CONNECTIONS_EXISTS", "400");
        }
    }

    @Override
    public void deleteAllUsers() throws BusinessException {
        logger.debug("delete all users in UserServiceImpl#delete");
        userRepository.deleteAll();
    }

    @Override
    public User findUserByUsername(final String username) throws BusinessException {
        BusinessAssert.notNull(username, "username is mandatory in UserServiceImpl#findUserByUsername", "400");
        logger.debug("find user by username '{}' in UserServiceImpl#findUserByUsername", username);
        final User user = userRepository.findUserByUsername(username);
        BusinessAssert.notNull(user, "user with username '" + username + "' not found in UserServiceImpl#findUserByUsername");
        logger.debug("found user by username '{}' with id '{}' in UserServiceImpl#findUserByUsername", username, user.getId());
        return user;
    }

    @Override
    public User findUserByUsernameOrEmail(final String usernameOrEmail) throws BusinessException {
        BusinessAssert.notNull(usernameOrEmail, "usernameOrEmail is mandatory in UserServiceImpl#findUserByUsernameOrEmail", "400");
        logger.debug("find user by username or email '{}' in UserServiceImpl#findUserByUsernameOrEmail", usernameOrEmail);
        final User user = userRepository.findUserByUsernameOrEmailAndActive(usernameOrEmail, true);
        BusinessAssert.notNull(user,
            "user with username or email '" + usernameOrEmail + "' not found in UserServiceImpl#findUserByUsernameOrEmail");
        logger.debug("found user by username or email '{}' with id '{}' in UserServiceImpl#findUserByUsernameOrEmail", usernameOrEmail,
            user.getId());
        return user;
    }

    @Override
    public User findUserByEmail(final String emailAddress) throws BusinessException {
        BusinessAssert.notNull(emailAddress, "emailAddress is mandatory in UserServiceImpl#findUserByEmail");
        logger.debug("find user by email '{}' in UserServiceImpl#findUserByEmail", emailAddress);
        final User user = userRepository.findUserByEmail(emailAddress);
        BusinessAssert.notNull(user, "user with email '" + emailAddress + "' not found in UserServiceImpl#findUserByEmail", "400");
        logger.debug("found user by email '{}' with id '{}' in UserServiceImpl#findUserByEmail", emailAddress, user.getId());
        return user;
    }

    @Override
    public User checkIfUserExistByUsername(final String username) throws BusinessException {
        BusinessAssert.notNull(username, "username is mandatory in UserServiceImpl#checkIfUserExistByUsernameOrEmail", "400");
        logger.debug("check if user exists by username '{}' in UserServiceImpl#checkIfUserExistByUsername", username);
        final User user = userRepository.getUserByUsername(username);
        return user;
    }

    @Override
    public void removeSuperAdmin(final List<User> users) throws BusinessException {
        users.removeIf(s -> s.getUsername().equalsIgnoreCase("superadmin"));
    }

    @Override
    public List<User> findUsersByProjectUserConnectionRoles(final List<ProjectUserConnectionRoleEnum> projectUserConnectionRoles)
            throws BusinessException {
        BusinessAssert.notNull(projectUserConnectionRoles,
            "projectUserConnectionRoles is mandatory in UserServiceImpl#findUsersByProjectUserConnectionRoles", "400");
        logger.debug("find users by projectUserConnectionRoles '{}' in UserServiceImpl#findUsersByProjectUserConnectionRoles",
            projectUserConnectionRoles);
        return userRepository.findByProjectUserConnectionRoleIn(projectUserConnectionRoles);
    }

    @Override
    public String toString() {
        return "[UserServiceImpl]";
    }
}
