(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.factory('createOrUpdateTimeRegistrationModalService', createOrUpdateTimeRegistrationModalService);
        
    createOrUpdateTimeRegistrationModalService.$inject = ['$modal', '$stateParams'];
    
    function createOrUpdateTimeRegistrationModalService($modal, $stateParams) {
		var service = {
			showCreateOrUpdateTimeRegistrationModal: showCreateOrUpdateTimeRegistrationModal
		};		
		return service;
		
		////////////
				
		function showCreateOrUpdateTimeRegistrationModal(type, projects, selectedProject, timeRegistration, invoker) {
			 var createOrUpdateTimeRegistrationModal = $modal.open({
				controller: CreateOrUpdateTimeRegistrationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	type: function() {
			    		return type;
			    	},
			    	projects: function() {
			    		return projects;
			    	},
			    	selectedProject: function() {
			    		return selectedProject;
			    	},
			    	timeRegistration: function() {
			    		return timeRegistration;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/timeregistration/modals/createOrUpdateTimeRegistration/createOrUpdateTimeRegistration.modal.html'
			});
			return createOrUpdateTimeRegistrationModal;
			
			function CreateOrUpdateTimeRegistrationModalController($modalInstance, $scope, type, projects, selectedProject, timeRegistration, invoker) {
		    	var vm = this; 
		    	vm.type = type;
		    	vm.projects = projects;
		    	vm.selectedProject = selectedProject;
		    	vm.timeRegistration = timeRegistration;
		    	vm.invoker = invoker;
		    	
		    	vm.closeCreateOrUpdateTimeRegistrationModal = closeCreateOrUpdateTimeRegistrationModal;
		    	
		    	////////////
		    	
		    	function closeCreateOrUpdateTimeRegistrationModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();