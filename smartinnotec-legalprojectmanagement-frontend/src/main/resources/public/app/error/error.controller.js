(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.error')
    	.controller('ErrorController', ErrorController);
    
    ErrorController.$inject = ['$scope', '$stateParams', '$window'];
    
    function ErrorController($scope, $stateParams, $window) {
	    $scope.vm = this;
    	var vm = this;
    	
    	vm.error = $stateParams.error;
    	vm.lastState = $stateParams.lastState;
    	vm.goBack = goBack;
    	
    	function goBack(){
    		$window.history.back();
    	}
    }
})();