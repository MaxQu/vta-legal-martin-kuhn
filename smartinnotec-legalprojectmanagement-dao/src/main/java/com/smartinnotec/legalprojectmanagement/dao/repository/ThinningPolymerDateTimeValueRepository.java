package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.ThinningPolymerDateTimeValue;

public interface ThinningPolymerDateTimeValueRepository extends MongoRepository<ThinningPolymerDateTimeValue, String> {

}
