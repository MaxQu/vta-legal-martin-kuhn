package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.service.container.AddressContainer;
import com.smartinnotec.legalprojectmanagement.service.container.ContactContainer;

public interface SystemImportMergeService {

    List<ContactImported> mergeContactsWithAddresses(final List<ContactContainer> contactContainers,
            final List<AddressContainer> addressContainers)
            throws BusinessException;
}
