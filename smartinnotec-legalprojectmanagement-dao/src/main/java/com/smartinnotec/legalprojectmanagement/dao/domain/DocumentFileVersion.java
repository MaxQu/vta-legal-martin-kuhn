package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

public @Data class DocumentFileVersion {

    private String filePathName;
    private String userUploadedId;
    private String version;
    @TextIndexed
    private List<LabelsPredefined> labels;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime uploadTime;
    private boolean active;
    private String userIdDeactivated;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime deactivationDateTime;
    @DBRef
    private User responsibleUser;
    @DBRef
    private Contact responsibleContact;
    @TextIndexed
    private String responsibleText;
    @TextIndexed
    private List<String> responsibilityStrings;
    private Long size;

    @Transient
    private List<LabelsPredefinedContainer> labelsPredefinedContainers;

    public DocumentFileVersion() {
        active = true;
    }

    public void addLabelsPredefinedContainer(final LabelsPredefinedContainer labelsPredefinedContainer) {
        if (labelsPredefinedContainers == null) {
            labelsPredefinedContainers = new ArrayList<>();
        }
        labelsPredefinedContainers.add(labelsPredefinedContainer);
    }

    public void addResponsibilityString(final String responsibilityString) {
        if (responsibilityStrings == null) {
            responsibilityStrings = new ArrayList<>();
        }
        responsibilityStrings.add(responsibilityString);
    }

    public List<String> getRelevantResponsibilityStrings() {
        final List<String> relevantResponsibleStrings = new ArrayList<>();
        if (this.getResponsibleUser() == null && this.getResponsibleContact() == null) {
            return relevantResponsibleStrings;
        } else if (this.getResponsibleUser() != null) {
            relevantResponsibleStrings.add(this.getResponsibleUser().getFirstname());
            relevantResponsibleStrings.add(this.getResponsibleUser().getSurname());
        } else if (this.getResponsibleContact() != null) {
            relevantResponsibleStrings.add(this.getResponsibleContact().getInstitution());
        }
        return relevantResponsibleStrings;
    }
}
