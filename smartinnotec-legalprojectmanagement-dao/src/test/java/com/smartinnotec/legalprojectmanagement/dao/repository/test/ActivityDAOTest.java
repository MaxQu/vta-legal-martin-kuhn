package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.ActivityDocumentFile;

public class ActivityDAOTest extends AbstractDAOTest {

    @Test
    public void shouldFindActivitiesOfActivityDocumentFiles() {

        final Activity activity = new Activity();
        final List<ActivityDocumentFile> activityDocumentFiles = new ArrayList<>();
        final ActivityDocumentFile activityDocumentFile = new ActivityDocumentFile();
        activityDocumentFile.setOriginalFileName("auszug.png");
        activityDocumentFile.setFileName("20190220_auszug.png");
        activityDocumentFile.setImage(false);
        activityDocumentFile.setVersion("1.0.0");
        activityDocumentFiles.add(activityDocumentFile);
        activity.setActivityDocumentFiles(activityDocumentFiles);
        final Activity createdActivity = activityRepository.insert(activity);
        Assert.assertNotNull(createdActivity);

        final List<Activity> foundedActivities = activityRepositoryDAO.findActivities("ausz*");
        Assert.assertNotNull(foundedActivities);
        Assert.assertEquals(1, foundedActivities.size());
    }
}
