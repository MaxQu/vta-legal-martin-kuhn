package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;

public interface ActivityRepository extends MongoRepository<Activity, String> {

    List<Activity> findByCalendarEventId(final String calendarEventId);

    Activity findFirst1ByCalendarEventIdOrderByCreationDateDesc(final String calendarEventId);

    List<Activity> findByContactId(final String contactId);

    List<Activity> findByCalendarEventStartDateBetween(final DateTime start, final DateTime end);

    List<Activity> findByCalendarEventIdAndCalendarEventStartDateBetween(final String calendarEventId, final DateTime start,
            final DateTime end);

    List<Activity> findByUserIdAndCalendarEventStartDateBetween(final String userId, final DateTime start, final DateTime end);

    List<Activity> findByContactIdAndCalendarEventStartDateBetween(final String contactId, final DateTime start, final DateTime end);

    List<Activity> findByUserIdAndContactIdAndCalendarEventStartDateBetween(final String userId, final String contactId,
            final DateTime start, final DateTime end);

    @Query("{$text : { $search : ?0 } }")
    List<Activity> findActivitiesBySearchString(final String searchString);

}
