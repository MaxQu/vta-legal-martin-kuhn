(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('calendarEventScheduleDaysService', calendarEventScheduleDaysService);
        
    calendarEventScheduleDaysService.$inject = ['$http', 'api_config'];
    
    function calendarEventScheduleDaysService($http, api_config) {
		var service = {
				findCalendarEventScheduleDaysByTenant: findCalendarEventScheduleDaysByTenant,
				create: create,
				update: update,
				deleteCalendarEventScheduleDays: deleteCalendarEventScheduleDays
		};
		
		return service;
		
		////////////
		
		function findCalendarEventScheduleDaysByTenant(userId) {
			return $http.get(api_config.BASE_URL + '/calendareventscheduledays/calendareventscheduleday/' + userId);
		}
		
		function create(calendarEventScheduleDays) {
			return $http.post(api_config.BASE_URL + '/calendareventscheduledays/calendareventscheduleday', calendarEventScheduleDays);
		}
		
		function update(calendarEventScheduleDays) {
			return $http.put(api_config.BASE_URL + '/calendareventscheduledays/calendareventscheduleday', calendarEventScheduleDays);
		}
		
		function deleteCalendarEventScheduleDays(id) {
			return $http.delete(api_config.BASE_URL + '/calendareventscheduledays/calendareventscheduleday/' + id);
		}
		
    }
})();
