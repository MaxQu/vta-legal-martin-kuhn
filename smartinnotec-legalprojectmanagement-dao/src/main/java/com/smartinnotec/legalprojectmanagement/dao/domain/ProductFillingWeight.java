package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class ProductFillingWeight {

    private String fillingWeight;
    private String information;

}
