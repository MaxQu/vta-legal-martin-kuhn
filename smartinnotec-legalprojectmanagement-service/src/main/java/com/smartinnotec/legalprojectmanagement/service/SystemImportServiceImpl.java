package com.smartinnotec.legalprojectmanagement.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.service.AbstractService;
import com.smartinnotec.legalprojectmanagement.service.container.AddressContainer;
import com.smartinnotec.legalprojectmanagement.service.container.ContactContainer;

@Service("systemImportService")
public class SystemImportServiceImpl extends AbstractService implements SystemImportService {

    private static final int ADDRESS_CSV_LENGTH;
    private static final int CONTACT_CSV_LENGTH;

    static {
        ADDRESS_CSV_LENGTH = 10;
        CONTACT_CSV_LENGTH = 15;
    }

    @Override
    public List<ContactContainer> importContactCSV(final String path) throws BusinessException {
        logger.info("import address from csv file in SystemImportServiceImpl#importContactCSV");
        List<ContactContainer> inputList = new ArrayList<ContactContainer>();
        try {
            final File inputF = new File(path);
            final InputStream inputFS = new FileInputStream(inputF);
            final BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
            final java.util.function.Function<String, ContactContainer> createdMapToContactContainer = mapToContactContainer;
            if (br.readLine().split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)").length != CONTACT_CSV_LENGTH) {
                br.close();
                throw new BusinessException("CONTACT_FILE_NOT_READABLE", "400");
            }
            inputList = br.lines().map(createdMapToContactContainer).collect(Collectors.toList());
            br.close();
        } catch (IOException e) {
            throw new BusinessException("CONTACT_FILE_NOT_FOUND", "400");
        }
        logger.info("imported '{}' contacts in SystemImportServiceImpl#importContactCSV", inputList.size());
        return inputList;
    }

    private java.util.function.Function<String, ContactContainer> mapToContactContainer = (line) -> {
        String[] p = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

        if (p.length == CONTACT_CSV_LENGTH - 1) { // if Vertreter is not available
            p = Arrays.copyOf(p, p.length + 1);
            p[p.length - 1] = "";
        } else if (p.length != CONTACT_CSV_LENGTH) {
            return null;
        }
        final ContactContainer contactContainer = new ContactContainer();
        contactContainer.setTenant(cutString(p[0]));
        contactContainer.setAccountNumber(cutString(p[1]));
        contactContainer.setShortCode(cutString(p[2]));
        contactContainer.setName1(cutString(p[3]));
        contactContainer.setName2(cutString(p[4]));
        contactContainer.setContactPerson(cutString(p[5]));
        contactContainer.setStreet(cutString(p[6]));
        contactContainer.setIntSign(cutString(p[7]));
        contactContainer.setPostalCode(cutString(p[8]));
        contactContainer.setRegion(cutString(p[9]));
        contactContainer.setTelephone(cutString(p[10]));
        contactContainer.setMobile(cutString(p[11]));
        contactContainer.setEmail(cutString(p[12]));
        contactContainer.setCustomerSince(cutString(p[13]));
        contactContainer.setAgent(cutString(p[14]));
        return contactContainer;
    };

    @Override
    public List<AddressContainer> importAddressCSV(final String path) throws BusinessException {
        logger.info("import contacts from csv file in SystemImportServiceImpl#importAddressCSV");
        List<AddressContainer> inputList = new ArrayList<AddressContainer>();
        try {
            final File inputF = new File(path);
            final InputStream inputFS = new FileInputStream(inputF);
            final BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
            if (br.readLine().split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)").length != ADDRESS_CSV_LENGTH) {
                br.close();
                throw new BusinessException("ADDRESS_FILE_NOT_READABLE", "400");
            }
            final java.util.function.Function<String, AddressContainer> createdḾapToAddressContainer = mapToAddressContainer;
            inputList = br.lines().map(createdḾapToAddressContainer).collect(Collectors.toList());
            br.close();
        } catch (IOException e) {
            throw new BusinessException("ADDRESS_FILE_NOT_FOUND", "400");
        }
        logger.info("imported '{}' addresses in SystemImportServiceImpl#importAddressCSV", inputList.size());
        return inputList;
    }

    private java.util.function.Function<String, AddressContainer> mapToAddressContainer = (line) -> {
        final String[] p = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
        if (p.length != ADDRESS_CSV_LENGTH) {
            return null;
        }
        final AddressContainer addressContainer = new AddressContainer();
        addressContainer.setTenant(cutString(p[0]));
        addressContainer.setAccountNumber(cutString(p[1]));
        addressContainer.setFlfId(cutString(p[2]));
        addressContainer.setName(cutString(p[3]));
        addressContainer.setContactPerson(cutString(p[4]));
        addressContainer.setStreet(cutString(p[5]));
        addressContainer.setPostalCode(cutString(p[6]));
        addressContainer.setRegion(cutString(p[7]));
        addressContainer.setTelephone(cutString(p[8]));
        addressContainer.setEmail(cutString(p[9]));
        return addressContainer;
    };

    private String cutString(String text) {
        if (text == null) {
            return null;
        }
        text = text.startsWith("\"") ? text.substring(1) : text;
        text = text.endsWith("\"") ? text.substring(0, text.length() - 1) : text;
        return text;
    }

    @Override
    public String toString() {
        return "[SystemImportServiceImpl]";
    }
}
