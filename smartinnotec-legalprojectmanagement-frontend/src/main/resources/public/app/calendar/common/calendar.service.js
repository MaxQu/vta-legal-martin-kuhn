(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarService', calendarService);
        
    calendarService.$inject = ['$http', 'api_config'];
    
    function calendarService($http, api_config) {
		var service = {
			findCalendarEventById: findCalendarEventById,
			findAllCalendarEvents: findAllCalendarEvents,
			findAllCalendarEventsOfNextWeeks: findAllCalendarEventsOfNextWeeks,
			findCalendarEventOfCriteria: findCalendarEventOfCriteria,
			getAllPreparedCalendarEvents: getAllPreparedCalendarEvents,
			getPreparedCalendarEvents: getPreparedCalendarEvents,
			getPreparedFilteredCalendarEvents: getPreparedFilteredCalendarEvents,
			create: create,
			update: update,
			deleteCalendarEvent: deleteCalendarEvent, 
			getICSLinkOfUser: getICSLinkOfUser
		};
		
		return service;
		
		////////////
		
		function findCalendarEventById(id) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/' + id);
		}
		
		function findAllCalendarEvents() {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent');
		}
		
		function findAllCalendarEventsOfNextWeeks(amountOfWeeks) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/weeks/' + amountOfWeeks);
		}
		
		function findCalendarEventOfCriteria(calendarEventSearchCriteriaContainer) {
			return $http.put(api_config.BASE_URL + '/calendarevents/calendarevent/criteria', calendarEventSearchCriteriaContainer);
		}
		
		function getAllPreparedCalendarEvents() {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/prepared');
		}
		
		function getPreparedCalendarEvents(calendarViewType, day, month, year) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/' + calendarViewType.toUpperCase() + '/' + day + '/' + month + '/' + year);
		}
		
		function getPreparedFilteredCalendarEvents(userId, calendarViewType, searchString, day, month, year) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/' + userId + '/' + calendarViewType.toUpperCase() + '/' + searchString + '/' + day + '/' + month + '/' + year);
		}
		
		function create(calendarEvent, serialDates, calendarEventSerialDateType, serialEndDate) {
			return $http.post(api_config.BASE_URL + '/calendarevents/calendarevent/' + serialDates + '/' + calendarEventSerialDateType + '/' + serialEndDate + '/', calendarEvent);
		}
		
		function update(calendarEvent) {
			return $http.put(api_config.BASE_URL + '/calendarevents/calendarevent', calendarEvent);
		}
		
		function deleteCalendarEvent(calendarEventId, deleteSerialDates) {
			return $http.delete(api_config.BASE_URL + '/calendarevents/calendarevent/' + calendarEventId + '/delete/' + deleteSerialDates);
		}
		
		function getICSLinkOfUser(userId) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/icslink/' + userId);
		}
    }
})();