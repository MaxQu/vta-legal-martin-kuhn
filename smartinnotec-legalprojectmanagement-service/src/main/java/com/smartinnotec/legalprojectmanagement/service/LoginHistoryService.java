package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.LoginHistory;
import com.smartinnotec.legalprojectmanagement.dao.domain.LoginHistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface LoginHistoryService {

    void setCurrentAndLastLoginHistory(final String userId) throws BusinessException;

    LoginHistory create(final LoginHistory loginHistory) throws BusinessException;

    LoginHistory update(final LoginHistory loginHistory) throws BusinessException;

    LoginHistory findByLoginHistoryTypeAndUser(final LoginHistoryTypeEnum loginHistoryType, final User user) throws BusinessException;

}
