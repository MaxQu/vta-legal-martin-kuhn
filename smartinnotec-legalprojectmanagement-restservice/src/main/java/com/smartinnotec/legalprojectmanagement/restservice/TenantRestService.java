package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.service.TenantService;

@RestController
public class TenantRestService extends AbstractRestService {

    @Autowired(required = true)
    private TenantService tenantService;

    public TenantRestService() {
    }

    @RequestMapping(value = "/tenants/tenant/all", method = { RequestMethod.GET })
    public List<Tenant> findAll() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": find all tenants in TenantRestService#findAll");
        return tenantService.findAll();
    }

    @RequestMapping(value = "/tenants/tenant", method = { RequestMethod.POST }, consumes = "application/json;charset=utf-8")
    public Tenant create(final @RequestBody Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in TenantRestService#create", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": creation of tenant with name '{}' in TenantRestService#create",
            tenant.getName());
        final Tenant createdTenant = tenantService.create(tenant);
        logger.debug("created tenant with id '{}' in TenantRestService#create", createdTenant.getId());
        return createdTenant;
    }

    @RequestMapping(value = "/tenants/tenant/{id}", method = RequestMethod.DELETE)
    public void deleteTenant(final @PathVariable(value = "id") String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in TenantRestService#deleteTenant", "400");
        BusinessAssert.isId(id, "projectId must be an id in TenantRestService#deleteTenant", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": delete tenant with id '{}' in TenantRestService#deleteTenant", id);
        tenantService.delete(id);
    }

    @Override
    public String toString() {
        return "[TenantRestService]";
    }
}
