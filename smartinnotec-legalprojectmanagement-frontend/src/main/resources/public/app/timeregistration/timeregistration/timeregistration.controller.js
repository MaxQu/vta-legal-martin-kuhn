(function() {
    'use strict';
    
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.controller('TimeregistrationController', TimeregistrationController);
    
    TimeregistrationController.$inject = ['$scope', '$timeout', 'moment', 'currentUser', 'projects', 'defaultProject', 'timeregistrationService', 'dateUtilityService', 'createOrUpdateTimeRegistrationModalService', 'yesNoTimeRegistrationModalService', 'printTimeRegistrationModalService', 'printingPDFService'];
    
    function TimeregistrationController($scope, $timeout, moment, currentUser, projects, defaultProject, timeregistrationService, dateUtilityService, createOrUpdateTimeRegistrationModalService, yesNoTimeRegistrationModalService, printTimeRegistrationModalService, printingPDFService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	vm.currentUser = currentUser;
    	vm.defaultProject = defaultProject.data;
    	vm.projects = projects.data;
    	vm.selectedProject = vm.projects.length == 1 ? vm.projects[0] : (vm.defaultProject != null ? vm.defaultProject : null);
    	vm.timeRegistration = setTimeRegistrationObject();
    	vm.todayDate = new Date(); 
    	vm.filterStartDate = moment();
    	vm.filterEndDate = moment().toDate();
    	vm.filterStartDate = vm.filterStartDate.add(-1, 'month').toDate();
    	vm.timeRegistrations = null;
    	vm.filterStartDatePicker = false;
    	vm.filterEndDatePicker = false;
    	vm.timeRegistrationTimeRangeSum = null;
    	
    	vm.printContainer = printContainer;
    	vm.findTimeRegistrationsInTimeRange = findTimeRegistrationsInTimeRange;
    	vm.getTimeRegistrationTimeRangeSum = getTimeRegistrationTimeRangeSum;
    	vm.createTimeRegistration = createTimeRegistration;
    	vm.deleteTimeRegistration = deleteTimeRegistration;
    	vm.addSelectedProject = addSelectedProject;
    	vm.updateTimeRegistration = updateTimeRegistration;
    	vm.removeSelectedProject = removeSelectedProject;
    	vm.showCreateOrUpdateTimeRegistrationModal = showCreateOrUpdateTimeRegistrationModal;
    	vm.showDeleteTimeRegistrationYesNoModal = showDeleteTimeRegistrationYesNoModal;
    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
    	vm.showPrintTimeRegistrations = showPrintTimeRegistrations;
    	vm.formatStartEndDateTime = formatStartEndDateTime;
    	findTimeRegistrationsInTimeRange();
    	
    	////////////
    	
    	function openFilterStartDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterStartDatePicker = true;
        }
    	
    	function openFilterEndDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterEndDatePicker = true;
        }
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function setTimeRegistrationObject() {
    		var timeRegistration = {
    	    		id: null,
    	    		dateTimeFrom: new Date(),
    	    		dateTimeUntil: new Date(),
    	    		user: vm.currentUser,
    	    		text: '',
    	    		project: vm.selectedProject,
    	    		tenant: null
    	    	};
    		return timeRegistration;
    	}
    	
    	function addSelectedProject(project) {
    		removeAllProjectSelections(vm.projects);
    		project.selected = true;
    		vm.selectedProject = project;
    	}
    	
    	function removeSelectedProject(project) {
    		vm.selectedProject = null;
    		removeAllProjectSelections(vm.projects);
    	}

    	function removeAllProjectSelections(projects) {
    		for(var i = 0; i < projects.length; i++) {
    			projects[i].selected = false;
    		}
    	}
    	
    	function findTimeRegistrationsInTimeRange() {
    		if(vm.selectedProject == null) {
    			return;
    		}
    		var start = dateUtilityService.formatDateToString(vm.filterStartDate);
    		var end = dateUtilityService.formatDateToString(vm.filterEndDate);    
    		timeregistrationService.findTimeRegistrations(vm.selectedProject.id, start, end).then(function (response) {
    			vm.timeRegistrations = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findTimeRegistrationsInTimeRange: ' + data);
			});
    		getTimeRegistrationTimeRangeSum();
    	}
    	
    	function getTimeRegistrationTimeRangeSum() {
    		var start = dateUtilityService.formatDateToString(vm.filterStartDate);
    		var end = dateUtilityService.formatDateToString(vm.filterEndDate);  
    		timeregistrationService.getTimeRegistrationTimeRangeSum(vm.selectedProject.id, start, end).then(function (response) {
    			vm.timeRegistrationTimeRangeSum = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#getTimeRegistrationTimeRangeSum: ' + data);
			});
    	}
    	
    	function createTimeRegistration(timeRegistration, startDate, startTime, endDate, endTime) {
    		timeRegistration.dateTimeFrom = dateUtilityService.joinDateObjectsToDateTimeObject(startDate, startTime);
    		timeRegistration.dateTimeUntil = dateUtilityService.joinDateObjectsToDateTimeObject(endDate, endTime);
    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
    		timeregistrationService.createTimeRegistration(vm.currentUser.id, timeRegistration).then(function (response) {
    			if(vm.timeRegistrations != null) {
    				vm.timeRegistrations.push(response.data);
    				getTimeRegistrationTimeRangeSum();
    			}
    			vm.timeRegistration = setTimeRegistrationObject();
    		}).catch(function (data) {
				console.log('error communication.controller#findTimeRegistrationsInTimeRange: ' + data);
			});
    	}
    	
    	function updateTimeRegistration(timeRegistration, startDate, startTime, endDate, endTime) {
    		timeRegistration.dateTimeFrom = dateUtilityService.joinDateObjectsToDateTimeObject(startDate, startTime);
    		timeRegistration.dateTimeUntil = dateUtilityService.joinDateObjectsToDateTimeObject(endDate, endTime);
    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
    		timeregistrationService.updateTimeRegistration(vm.currentUser.id, timeRegistration).then(function (response) {
    			vm.timeRegistration = setTimeRegistrationObject();
    			for(var i = 0; i < vm.timeRegistrations.length; i++) {
    				if(vm.timeRegistrations[i].id == timeRegistration.id) {
    					vm.timeRegistrations[i] = response.data;
    					break;
    				}
    			}
    			getTimeRegistrationTimeRangeSum();
    		}).catch(function (data) {
				console.log('error communication.controller#updateTimeRegistration: ' + data);
			});
    	}
    	
    	function deleteTimeRegistration(timeRegistration) {
    		timeregistrationService.deleteTimeRegistration(timeRegistration.id).then(function (response) {
    			for(var i = 0; i < vm.timeRegistrations.length; i++) {
    				if(vm.timeRegistrations[i].id == timeRegistration.id) {
    					vm.timeRegistrations.splice(i, 1);
    					break;
    				}
    			}
    			getTimeRegistrationTimeRangeSum();
    		}).catch(function (data) {
				console.log('error communication.controller#deleteTimeRegistration: ' + data);
			});
    	}
    	
    	function showCreateOrUpdateTimeRegistrationModal(type, timeRegistration) {
    		createOrUpdateTimeRegistrationModalService.showCreateOrUpdateTimeRegistrationModal(type, vm.projects, vm.selectedProject, timeRegistration, vm);
    	}
    	
    	function showDeleteTimeRegistrationYesNoModal(timeRegistration) {
    		yesNoTimeRegistrationModalService.showYesNoModal(vm, timeRegistration);
    	}
    	
    	function showPrintTimeRegistrations() {
    		printTimeRegistrationModalService.showPrintTimeRegistrationModal(vm.timeRegistrations);
    	}
    	
    	function formatStartEndDateTime(start, end) {
    		return dateUtilityService.formatStartEndDateTime(start, end);
    	}
	} 
})();