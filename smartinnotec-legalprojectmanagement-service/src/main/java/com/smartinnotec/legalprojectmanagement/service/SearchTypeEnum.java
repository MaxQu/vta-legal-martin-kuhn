package com.smartinnotec.legalprojectmanagement.service;

public enum SearchTypeEnum {

    PRODUCT("PRODUCT"),
    PROJECT("PROJECT");

    private String value;

    private SearchTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
