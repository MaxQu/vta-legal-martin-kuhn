package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum SexEnum {

    MALE("MALE"),
    FEMALE("FEMALE");

    private String value;

    private SexEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
