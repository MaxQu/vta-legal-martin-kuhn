package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Communication;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface CommunicationUserConnectionService {

    List<CommunicationUserConnection> findCommunicationUserConnectionByProject(final Project project) throws BusinessException;

    CommunicationUserConnection findCommunicationUserConnection(final String id) throws BusinessException;

    CommunicationUserConnection createCommunicationUserConnection(final CommunicationUserConnection communicationUserConnection,
            final Tenant tenant) throws BusinessException;

    CommunicationUserConnection updateCommunicationUserConnection(final CommunicationUserConnection communicationUserConnection)
            throws BusinessException;

    CommunicationUserConnection archiveCommunicationUserConnection(final CommunicationUserConnection communicationUserConnection)
            throws BusinessException;

    void deleteCommunicationUserConnection(final String id) throws BusinessException;

    boolean isCreatedOrReceivedUserNotSet(final CommunicationUserConnection communicationUserConnection);

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived(
            final User userCreatedMessage, final User userReceivedMessage, final Tenant tenant, final boolean archived)
            throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageNotAndUserReceivedMessageAndTenantAndArchived(
            final User userCreatedMessage, final User userReceivedMessage, final Tenant tenant, final boolean archived)
            throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessage(final User userReceivedMessage)
            throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndArchived(final User userReceivedMessage,
            final boolean archived, final Integer page) throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionsByCommunication(final Communication communication)
            throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(
            final User userReceivedMessage, final Boolean confirmationNeeded, final Boolean read, final Boolean executed)
            throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(
            final User userCreatedMessage, final Boolean confirmationNeeded, final Boolean read, final Boolean executed)
            throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessage(final User userCreatedMessage)
            throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndExecutionDateNotNullAndExecuted(
            final User userReceivedMessage, final Boolean executed) throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndExecutionDateNotNullAndExecuted(
            final User userCreatedMessage, final Boolean executed) throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween(final User userCreatedMessage,
            final DateTime start, final DateTime end) throws BusinessException;

    List<CommunicationUserConnection> findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween(final User userReceivedMessage,
            final DateTime start, final DateTime end) throws BusinessException;
    
    Integer count() throws BusinessException;
}
