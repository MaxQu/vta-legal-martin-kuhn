package com.smartinnotec.legalprojectmanagement.restservice.container;

import lombok.Data;

public @Data class SystemImportContainer {

    private String pathAddress;
    private String pathContact;

}
