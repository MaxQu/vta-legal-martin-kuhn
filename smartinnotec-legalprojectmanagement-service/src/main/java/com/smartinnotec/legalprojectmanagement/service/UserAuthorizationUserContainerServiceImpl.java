package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorization;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationStateEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationUserContainer;
import com.smartinnotec.legalprojectmanagement.dao.repository.UserAuthorizationRoleAccessRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.UserAuthorizationUserContainerRepository;

@Service("userAuthorizationUserContainerService")
public class UserAuthorizationUserContainerServiceImpl extends AbstractService implements UserAuthorizationUserContainerService {

    @Autowired
    private UserAuthorizationUserContainerRepository userAuthorizationUserContainerRepository;
    @Autowired
    private UserAuthorizationRoleAccessRepository userAuthorizationRoleAccessRepository;

    @Override
    public UserAuthorizationUserContainer findById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in UserAuthorizationUserContainerServiceImpl#findById", "400");
        BusinessAssert.isId(id, "id must be an id in UserAuthorizationUserContainerServiceImpl#findById", "400");
        logger.info("find userAuthorizationUserContainer by id '{}' in UserAuthorizationUserContainerServiceImpl#findById", id);
        return userAuthorizationUserContainerRepository.findOne(id);
    }

    @Override
    public boolean isUserAuthorized(final User user, final UserAuthorizationTypeEnum userAuthorizationType,
            final UserAuthorizationStateEnum userAuthorizationState) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in UserAuthorizationUserContainerServiceImpl#isUserAuthorized", "400");
        BusinessAssert.notNull(userAuthorizationType,
            "userAuthorizationType is mandatory in UserAuthorizationUserContainerServiceImpl#isUserAuthorized", "400");
        BusinessAssert.notNull(userAuthorizationState,
            "userAuthorizationState is mandatory in UserAuthorizationUserContainerServiceImpl#isUserAuthorized", "400");
        logger.info(
            "is user with id '{}' authorized for userAuthorizationType '{}' and userAuthorizationState '{}' in UserAuthorizationUserContainerServiceImpl#isUserAuthorized",
            user.getId(), userAuthorizationType, userAuthorizationState);
        final UserAuthorizationUserContainer userAuthorizationUserContainer = user.getUserAuthorizationUserContainer();

        if (userAuthorizationUserContainer == null || userAuthorizationUserContainer.getUserAuthorizations() == null
            || userAuthorizationUserContainer.getUserAuthorizations().isEmpty()) {
            return false;
        }

        for (final UserAuthorization userAuthorization : userAuthorizationUserContainer.getUserAuthorizations()) {
            if (userAuthorization.getUserAuthorizationType() == userAuthorizationType
                && userAuthorization.getUserAuthorizationState() == userAuthorizationState) {
                return userAuthorization.getState();
            }
        }
        return false;
    }

    @Override
    public UserAuthorizationUserContainer findUserAuthorizationUserContainerByUserId(final String userId) throws BusinessException {
        BusinessAssert.notNull(userId,
            "userId is mandatory in UserAuthorizationUserContainerServiceImpl#findUserAuthorizationUserContainerByUserId", "400");
        BusinessAssert.isId(userId,
            "userId must be an id in UserAuthorizationUserContainerServiceImpl#findUserAuthorizationUserContainerByUserId", "400");
        logger.info(
            "find userAuthorizationUserContainer for user with id '{}' in UserAuthorizationUserContainerServiceImpl#findUserAuthorizationUserContainerByUserId",
            "400");
        return userAuthorizationUserContainerRepository.findByUserId(userId);
    }

    @Override
    public UserAuthorizationUserContainer create(final String userId, final ProjectUserConnectionRoleEnum projectUserConnectionRole)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in UserAuthorizationUserContainerServiceImpl#create", "400");
        BusinessAssert.isId(userId, "userId must be an id in UserAuthorizationUserContainerServiceImpl#create", "400");
        BusinessAssert.notNull(projectUserConnectionRole,
            "projectUserConnectionRole is mandatory in UserAuthorizationUserContainerServiceImpl#create", "400");
        logger.info(
            "create userAuthorizationUserContainer of projectUserConnectionRole '{}' in UserAuthorizationUserContainerServiceImpl#create",
            projectUserConnectionRole);
        final UserAuthorizationUserContainer userAuthorizationUserContainer = new UserAuthorizationUserContainer();
        userAuthorizationUserContainer.setUserId(userId);

        switch (projectUserConnectionRole) {
        case ADMIN:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForAdmin());
            break;
        case GENERAL_MANAGER:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForGeneralManager());
            break;
        case PROJECT_MANAGER:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForProjectManager());
            break;
        case LABOR:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForLabor());
            break;
        case PROJECT_WORKER:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForProjectWorker());
            break;
        case PRODUCTION:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForProduction());
            break;
        case MARKETING:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForMarketing());
            break;
        case CUSTOMER_SERVICE:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForCustomerCare());
            break;
        case EXTERNAL_WORKER:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForExternalWorker());
            break;
        case GUEST:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForGuest());
            break;
        case CONTACT:
            userAuthorizationUserContainer.setUserAuthorizations(createUserAuthorizationForContact());
            break;
        }
        return userAuthorizationUserContainerRepository.insert(userAuthorizationUserContainer);
    }

    @Override
    public UserAuthorizationUserContainer create(final UserAuthorizationUserContainer userAuthorizationUserContainer)
            throws BusinessException {
        BusinessAssert.notNull(userAuthorizationUserContainer,
            "userAuthorizationUserContainer is mandatory in UserAuthorizationUserContainerServiceImpl#create", "400");
        logger.info("create userAuthorizationUserContainer in UserAuthorizationUserContainerServiceImpl#create");
        return userAuthorizationUserContainerRepository.insert(userAuthorizationUserContainer);
    }

    @Override
    public UserAuthorizationUserContainer update(final UserAuthorizationUserContainer userAuthorizationUserContainer)
            throws BusinessException {
        BusinessAssert.notNull(userAuthorizationUserContainer,
            "userAuthorizationUserContainer is mandatory in UserAuthorizationUserContainerServiceImpl#update", "400");
        logger.info("update userAuthorizationUserContainer with id '{}' in UserAuthorizationUserContainerServiceImpl#update",
            userAuthorizationUserContainer.getId());
        return userAuthorizationUserContainerRepository.save(userAuthorizationUserContainer);
    }

    @Override
    public void delete(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in UserAuthorizationUserContainerServiceImpl#delete", "400");
        BusinessAssert.isId(id, "id is mandatory in UserAuthorizationUserContainerServiceImpl#delete", "400");
        logger.info("delete userAuthorizationUserContainer with id '{}' in UserAuthorizationUserContainerServiceImpl#delete", id);
        userAuthorizationUserContainerRepository.delete(id);
    }

    @Override
    public void deleteOfUserId(final String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in UserAuthorizationUserContainerServiceImpl#deleteOfUserId", "400");
        BusinessAssert.isId(userId, "userId is mandatory in UserAuthorizationUserContainerServiceImpl#deleteOfUserId", "400");
        logger.info(
            "delete userAuthorizationUserContainer for user with id '{}' in UserAuthorizationUserContainerServiceImpl#deleteOfUserId",
            userId);
        final UserAuthorizationUserContainer userAuthorizationUserContainer = userAuthorizationUserContainerRepository.findByUserId(userId);
        if (userAuthorizationUserContainer != null) {
            this.delete(userAuthorizationUserContainer.getId());
        }
    }

    private List<UserAuthorization> createUserAuthorizationForAdmin() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
        userAuthorizations.add(createUserAuthorization(41,
        		getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        		UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
        userAuthorizations.add(createUserAuthorization(42,
        		getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.ADMIN, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        		UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForGeneralManager() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();

        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.CONTACT),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.HISTORY),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.RECEIPE),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.MIXTURE),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.THINNING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING));
        userAuthorizations
            .add(
                createUserAuthorization(
                    32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING));
        userAuthorizations
            .add(
                createUserAuthorization(
                    33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.ACTIVITY),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GENERAL_MANAGER, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForProjectManager() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.CONTACT),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.HISTORY),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.RECEIPE),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.MIXTURE),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.THINNING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING));
        userAuthorizations
            .add(
                createUserAuthorization(
                    32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING));
        userAuthorizations
            .add(
                createUserAuthorization(
                    33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.ACTIVITY),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_MANAGER, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForLabor() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.LABOR, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForProjectWorker() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PROJECT_WORKER, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForProduction() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.PRODUCTION, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForMarketing() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
        userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
        userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.MARKETING, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForCustomerCare() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.PROJECT),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.PRODUCT),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.CALENDAR),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations
            .add(
                createUserAuthorization(
                    8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations
            .add(
                createUserAuthorization(
                    9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.CONTACT),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.HISTORY),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.RECEIPE),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.MIXTURE),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.THINNING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING));
        userAuthorizations
            .add(
                createUserAuthorization(
                    32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING));
        userAuthorizations
            .add(
                createUserAuthorization(
                    33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.ACTIVITY),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForExternalWorker() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.CONTACT),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations
            .add(
                createUserAuthorization(
                    12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.HISTORY),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.RECEIPE),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.MIXTURE),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations
            .add(
                createUserAuthorization(
                    30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.THINNING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING));
        userAuthorizations
            .add(
                createUserAuthorization(
                    32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING));
        userAuthorizations
            .add(
                createUserAuthorization(
                    33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.ACTIVITY),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations
            .add(
                createUserAuthorization(
                    36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER,
                        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.EXTERNAL_WORKER, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForGuest() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.GUEST, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private List<UserAuthorization> createUserAuthorizationForContact() {
        final List<UserAuthorization> userAuthorizations = new ArrayList<>();
        userAuthorizations.add(createUserAuthorization(1, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(2, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PROJECT));
        userAuthorizations.add(createUserAuthorization(3, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PROJECT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PROJECT));

        userAuthorizations.add(createUserAuthorization(4, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(5, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.PRODUCT));
        userAuthorizations.add(createUserAuthorization(6, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.PRODUCT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.PRODUCT));

        userAuthorizations.add(createUserAuthorization(7, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(8, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CALENDAR));
        userAuthorizations.add(createUserAuthorization(9, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CALENDAR), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CALENDAR));

        userAuthorizations.add(createUserAuthorization(10, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(11, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.CONTACT));
        userAuthorizations.add(createUserAuthorization(12, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.CONTACT), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.CONTACT));

        userAuthorizations
            .add(
                createUserAuthorization(13,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(14,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.CHANGE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.COMMUNICATION));
        userAuthorizations
            .add(
                createUserAuthorization(15,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.DELETE,
                        UserAuthorizationTypeEnum.COMMUNICATION),
                    UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.COMMUNICATION));

        userAuthorizations.add(createUserAuthorization(16,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(17,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.TIME_REGISTRATION));
        userAuthorizations.add(createUserAuthorization(18,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.TIME_REGISTRATION),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.TIME_REGISTRATION));

        userAuthorizations.add(createUserAuthorization(19, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(20, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.HISTORY));
        userAuthorizations.add(createUserAuthorization(21, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.HISTORY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.HISTORY));

        userAuthorizations
            .add(
                createUserAuthorization(22,
                    getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.READ,
                        UserAuthorizationTypeEnum.ADMINISTRATION),
                    UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(23,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.CHANGE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ADMINISTRATION));
        userAuthorizations
            .add(createUserAuthorization(24,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.DELETE,
                    UserAuthorizationTypeEnum.ADMINISTRATION),
                UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ADMINISTRATION));

        userAuthorizations.add(createUserAuthorization(25, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(26, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.RECEIPE));
        userAuthorizations.add(createUserAuthorization(27, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.RECEIPE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.RECEIPE));

        userAuthorizations.add(createUserAuthorization(28, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(29, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.MIXTURE));
        userAuthorizations.add(createUserAuthorization(30, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.MIXTURE), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.MIXTURE));

        userAuthorizations.add(createUserAuthorization(31, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(32, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.THINNING));
        userAuthorizations.add(createUserAuthorization(33, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.THINNING), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.THINNING));

        userAuthorizations.add(createUserAuthorization(34, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.READ,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(35, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.ACTIVITY));
        userAuthorizations.add(createUserAuthorization(36, getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT,
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.ACTIVITY), UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.ACTIVITY));

        userAuthorizations
            .add(createUserAuthorization(37,
                getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.READ,
                    UserAuthorizationTypeEnum.FACILITY_DETAILS),
                UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(38,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.CHANGE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        userAuthorizations.add(createUserAuthorization(39,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.DELETE,
                UserAuthorizationTypeEnum.FACILITY_DETAILS),
            UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.FACILITY_DETAILS));
        
        userAuthorizations
        .add(createUserAuthorization(40,
            getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.READ,
                UserAuthorizationTypeEnum.REPORTING),
            UserAuthorizationStateEnum.READ, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(41,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.CHANGE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.CHANGE, UserAuthorizationTypeEnum.REPORTING));
    userAuthorizations.add(createUserAuthorization(42,
        getUserAuthorizationRoleAccessState(ProjectUserConnectionRoleEnum.CONTACT, UserAuthorizationStateEnum.DELETE,
            UserAuthorizationTypeEnum.REPORTING),
        UserAuthorizationStateEnum.DELETE, UserAuthorizationTypeEnum.REPORTING));

        return userAuthorizations;
    }

    private boolean getUserAuthorizationRoleAccessState(final ProjectUserConnectionRoleEnum projectUserConnectionRole,
            final UserAuthorizationStateEnum userAuthorizationState, final UserAuthorizationTypeEnum userAuthorizationType) {
        final Boolean state = userAuthorizationRoleAccessRepository
            .findByProjectUserConnectionRoleAndUserAuthorizationStateAndUserAuthorizationType(projectUserConnectionRole,
                userAuthorizationState, userAuthorizationType)
            .getAccess();
        return state;
    }

    private UserAuthorization createUserAuthorization(final Integer order, final Boolean state,
            final UserAuthorizationStateEnum userAuthorizationState, final UserAuthorizationTypeEnum userAuthorizationType) {
        final UserAuthorization userAuthorization = new UserAuthorization();
        userAuthorization.setOrder(order);
        userAuthorization.setState(state);
        userAuthorization.setUserAuthorizationState(userAuthorizationState);
        userAuthorization.setUserAuthorizationType(userAuthorizationType);
        return userAuthorization;
    }
}
