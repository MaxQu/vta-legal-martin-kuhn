(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.search')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getSearchState());
    	 
    	////////////
			    	
    	function getSearchState() {
    		var state = {
    			name: 'auth.search',
				url: '/search/:userId',
				templateUrl: 'app/search/search/search.html',
				controller: 'SearchController',
				controllerAs: 'vm',
				resolve: {
					searchService: 'searchService',
					labelsPredefinedService: 'labelsPredefinedService',
					userService: 'userService',
					optionsService: 'optionsService',
					thinningPolymerDateTimeValueService: 'thinningPolymerDateTimeValueService',
					searchHistory: function getSearchHistory($stateParams, searchService) {
						return searchService.getSearchHistory($stateParams.userId);
					},
					labelsPredefined: function getAllLabelsPredefined($stateParams, labelsPredefinedService) {
						return labelsPredefinedService.getAllLabelsPredefined($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					unityTypes: function(optionsService) {
						return optionsService.getUnityTypes();
					},
					currentThinningPolymerDateTimeValue: function findThinningPolymerDateTimeValueOfDate(thinningPolymerDateTimeValueService) {
						var date = new Date();
						var dateString = date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear();
 						return thinningPolymerDateTimeValueService.findThinningPolymerDateTimeValueOfDate(dateString);
					},
					titles: function getAllTitles(optionsService) {
						return optionsService.getAllTitles();
					},
					provinceTypes: function getProvinceTypes(optionsService) {
						return optionsService.getProvinceTypes();
					},
					contactTypes: function(optionsService) {
						return optionsService.getContactTypes();
					},
					countryTypes: function getCountryTypes(optionsService) {
						return optionsService.getCountryTypes();
					},
					roles: function(optionsService) {
						return optionsService.getRoles();
					},
					usersOfProject: function findProjectUserConnectionByProject(userService) {
						return userService.findAllUsers();
					}
				}
    		};
    		return state;
    	}
	}
})();