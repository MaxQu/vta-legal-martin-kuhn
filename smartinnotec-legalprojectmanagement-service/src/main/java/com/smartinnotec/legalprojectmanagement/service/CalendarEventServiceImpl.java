package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventSerialDateTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.FacilityDetails;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.CalendarEventRepository;
import com.smartinnotec.legalprojectmanagement.service.container.CalendarEventPreparedContainer;

@Service("calendarEventService")
public class CalendarEventServiceImpl extends AbstractService implements CalendarEventService {

    private static final String PROJECT_START;
    private static final String PROJECT_END;

    static {
        PROJECT_START = "Projektstart";
        PROJECT_END = "Projektende";
    }

    @Autowired
    private CalendarEventRepository calendarEventRepository;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private FacilityDetailsService facilityDetailsService;
   
    @Override
    public Integer count() throws BusinessException {
        logger.info("count calendarEvents in CalendarEventServiceImpl#count");
        final Long calendarEvents = calendarEventRepository.count();
        return calendarEvents.intValue();
    }

    @Override
    public List<CalendarEvent> findByProject(final Project project) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in CalendarEventServiceImpl#findByProject", "400");
        logger.info("find calendarEvents by project with id '{}' in CalendarEventServiceImpl#findByProject", project.getId());
        return calendarEventRepository.findByProjectAndArchived(project, false);
    }

    @Override
    public CalendarEvent findById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in CalendarEventServiceImpl#findAllCalendarEventsByTenant", "400");
        BusinessAssert.isId(id, "id must be an id CalendarEventServiceImpl#findAllCalendarEventsByTenant", "400");
        logger.info("find calendarEvent by id '{}' in CalendarEventServiceImpl#findAll", id);
        return calendarEventRepository.findOne(id);
    }

    @Override
    public List<CalendarEvent> findBySerialDateNumber(final Long serialDateNumber) throws BusinessException {
        BusinessAssert.notNull(serialDateNumber, "serialDateNumber is mandatory in CalendarEventServiceImpl#findBySerialDateNumber", "400");
        logger.info("find all calendarEvents with serialDateNumber '{}' in CalendarEventServiceImpl#findBySerialDateNumber",
            serialDateNumber);
        return calendarEventRepository.findBySerialDateNumberAndArchived(serialDateNumber, false);
    }

    @Override
    public List<CalendarEvent> findCalendarEventsBySearchString(final String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in CalendarEventServiceImpl#findCalendarEventsBySearchString",
            "400");
        logger.info("find calendarEvents of searchString '{}' in CalendarEventServiceImpl#findCalendarEventsBySearchString", searchString);
        return calendarEventRepository.findCalendarEventBySearchString(searchString);
    }

    @Override
    public List<CalendarEvent> findAll() throws BusinessException {
        logger.info("find all calendarEvents in CalendarEventServiceImpl#findAll");
        return calendarEventRepository.findAll();
    }

    @Override
    public List<CalendarEvent> findAllCalendarEventsByTenant(final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in CalendarEventServiceImpl#findAllCalendarEventsByTenant", "400");
        logger.info("find all calendarEvents in CalendarEventServiceImpl#findAll");
        return calendarEventRepository.findByTenant(tenant);
    }

    @Override
    public CalendarEvent findByProjectAndLocation(final Project project, final String location) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in CalendarEventServiceImpl#findByProjectAndStartsAtAndLocation", "400");
        logger.info("find calendarEvents by project with id '{}' in CalendarEventServiceImpl#findByProjectAndStartsAtAndLocation",
            project.getId());
        return calendarEventRepository.findByProjectAndLocationAndArchived(project, location, false);
    }

    @Override
    public CalendarEvent findByLocationAndProject(final String location, final Project project) throws BusinessException {
        BusinessAssert.notNull(location, "location is mandatory in CalendarEventServiceImpl#findByTitleAndLocationAndProject", "400");
        BusinessAssert.notNull(project, "project is mandatory in CalendarEventServiceImpl#findByTitleAndLocationAndProject", "400");
        logger.info(
            "find calendarEvent by title '{}' and location '{}' and project with id '{}' in CalendarEventServiceImpl#findByTitleAndLocationAndProject",
            location, project.getId());
        return calendarEventRepository.findByLocationAndProjectAndArchived(location, project, false);
    }

    @Override
    public List<CalendarEventPreparedContainer> prepareCalendarEvents(final List<CalendarEvent> calendarEvents,
            final boolean addCalendarEventUserConnections)
            throws BusinessException {
        if (calendarEvents == null || calendarEvents.isEmpty()) {
            return new ArrayList<>();
        }
        logger.info("prepare '{}' calendar events in CalendarEventServiceImpl#prepareCalendarEvents", calendarEvents.size());
        final List<CalendarEventPreparedContainer> calendarEventPreparedContainers = new ArrayList<>();
        for (final CalendarEvent calendarEvent : calendarEvents) {
            final CalendarEventPreparedContainer calendarEventPreparedContainer = prepareCalendarEvent(calendarEvent,
                addCalendarEventUserConnections);
            calendarEventPreparedContainers.add(calendarEventPreparedContainer);
        }
        return calendarEventPreparedContainers;
    }

    @Override
    public List<CalendarEventPreparedContainer> getPreparedCalendarEvents(final Tenant tenant,
            final CalendarEventViewTypeEnum calendarViewType, final Long start, final Long end)
            throws BusinessException {
        BusinessAssert.notNull(calendarViewType, "calendarViewType is mandatory in CalendarEventServiceImpl#getPreparedCalendarEvents",
            "400");
        BusinessAssert.notNull(start, "start is mandatory in CalendarEventServiceImpl#getPreparedCalendarEvents", "400");
        BusinessAssert.notNull(end, "end is mandatory in CalendarEventServiceImpl#getPreparedCalendarEvents", "400");
        logger.info(
            "get calendar events for calendar view type '{}' from start '{}' to end '{}' in CalendarEventServiceImpl#getPreparedCalendarEvents",
            calendarViewType, start, end);
        final List<CalendarEvent> uniqueCalendarEvents = getCalendarEvents(tenant, calendarViewType, start, end, false);
        return this.prepareCalendarEvents(uniqueCalendarEvents, false);
    }

    @Override
    public List<CalendarEvent> getCalendarEvents(final Tenant tenant, final CalendarEventViewTypeEnum calendarViewType, final Long start,
            final Long end, final boolean archived)
            throws BusinessException {
        BusinessAssert.notNull(calendarViewType, "calendarViewType is mandatory in CalendarEventServiceImpl#getCalendarEvents", "400");
        BusinessAssert.notNull(start, "start is mandatory in CalendarEventServiceImpl#getCalendarEvents", "400");
        BusinessAssert.notNull(end, "end is mandatory in CalendarEventServiceImpl#getCalendarEvents", "400");
        logger.info(
            "get calendar events for calendar view type '{}' from start '{}' to end '{}' in CalendarEventServiceImpl#getCalendarEvents",
            calendarViewType, start, end);

        final List<CalendarEvent> calendarEventsStartBetween = calendarEventRepository.findByTenantAndArchivedAndStartsAtBetween(tenant,
            archived, start, end);
        final List<CalendarEvent> calendarEventsEndBetween = calendarEventRepository.findByTenantAndArchivedAndEndsAtBetween(tenant,
            archived, start, end);
        final List<CalendarEvent> calendarEventsStartLowerThanAndEndGreaterThan = calendarEventRepository
            .findByTenantAndArchivedAndStartsAtLessThanAndEndsAtGreaterThan(tenant, false, start, end);

        final List<CalendarEvent> calendarEvents = new ArrayList<>();
        calendarEvents.addAll(calendarEventsStartBetween);
        calendarEvents.addAll(calendarEventsEndBetween);
        calendarEvents.addAll(calendarEventsStartLowerThanAndEndGreaterThan);

        logger.debug("'{}' calendar events found in CalendarEventServiceImpl#getCalendarEvents", calendarEvents.size());

        final List<CalendarEvent> uniqueCalendarEvents = getUniqueCalendarEvents(calendarEvents);
        return uniqueCalendarEvents;
    }

    @Override
    public List<CalendarEvent> getCalendarEventsInRange(final List<CalendarEvent> calendarEvents, final Long start, final Long end)
            throws BusinessException {
        BusinessAssert.notNull(calendarEvents, "calendarEvents is mandatory in CalendarEventServiceImpl#getCalendarEventsInRange", "400");
        BusinessAssert.notNull(start, "start is mandatory in CalendarEventServiceImpl#getCalendarEventsInRange", "400");
        BusinessAssert.notNull(end, "end is mandatory in CalendarEventServiceImpl#getCalendarEventsInRange", "400");
        logger.info("get calendar events in range from start '{}' to end '{}' in CalendarEventServiceImpl#getCalendarEventsInRange", start,
            end);
        final List<CalendarEvent> calendarEventsInRange = calendarEvents.stream()
            .filter(s -> s.getStartsAt() >= start && s.getEndsAt() <= end).collect(Collectors.toList());
        return calendarEventsInRange;
    }

    @Override
    public List<CalendarEventPreparedContainer> getCalendarEvents(final Tenant tenant, final Long start, final Long end,
            final boolean archived, final boolean addCalendarEventUserConnections)
            throws BusinessException {
        BusinessAssert.notNull(start, "start is mandatory in CalendarEventServiceImpl#getCalendarEvents", "400");
        BusinessAssert.notNull(end, "end is mandatory in CalendarEventServiceImpl#getCalendarEvents", "400");
        logger.info("get calendar events of tenant with id '{}', start '{}' and end '{}' in CalendarEventServiceImpl#getCalendarEvents",
            tenant != null ? tenant.getId() : null, start, end);
        final List<CalendarEvent> calendarEventsStartBetween = calendarEventRepository.findByTenantAndArchivedAndStartsAtBetween(tenant,
            archived, start, end);
        final List<CalendarEvent> calendarEventsEndBetween = calendarEventRepository.findByTenantAndArchivedAndEndsAtBetween(tenant,
            archived, start, end);
        final List<CalendarEvent> calendarEventsStartLowerThanAndEndGreaterThan = calendarEventRepository
            .findByTenantAndArchivedAndStartsAtLessThanAndEndsAtGreaterThan(tenant, archived, start, end);

        final List<CalendarEvent> calendarEvents = new ArrayList<>();
        calendarEvents.addAll(calendarEventsStartBetween);
        calendarEvents.addAll(calendarEventsEndBetween);
        calendarEvents.addAll(calendarEventsStartLowerThanAndEndGreaterThan);

        logger.debug("'{}' calendar events found in CalendarEventServiceImpl#getCalendarEvents", calendarEvents.size());

        final List<CalendarEvent> uniqueCalendarEvents = getUniqueCalendarEvents(calendarEvents);
        return this.prepareCalendarEvents(uniqueCalendarEvents, addCalendarEventUserConnections);
    }

    @Override
    public CalendarEventPreparedContainer prepareCalendarEvent(final CalendarEvent calendarEvent,
            final boolean addCalendarEventUserConnections)
            throws BusinessException {
        logger.info("prepare calendarEvent in CalendarEventServiceImpl#prepareCalendarEvent");
        final String color = calendarEvent.getProject() != null ? calendarEvent.getProject().getColor() : "#ccc";
        final String startTime = calendarEvent.getStartsAt() != null ? timeFormatter.print(calendarEvent.getStartsAt()) : " offen";
        final String endTime = calendarEvent.getEndsAt() != null ? timeFormatter.print(calendarEvent.getEndsAt()) : " offen";

        final ContactAddressWithProducts contactAddressWithProducts = calendarEvent.getContactAddressWithProducts();

        final StringBuilder contactAddressWithProductsStringBuilder = new StringBuilder();
        if (contactAddressWithProducts != null && contactAddressWithProducts.getAddress() != null) {
            contactAddressWithProductsStringBuilder.append(contactAddressWithProducts.getAddress().getAdditionalInformation());
            if (contactAddressWithProducts.getAddress().getStreet() != null
                && contactAddressWithProducts.getAddress().getStreet().length() > 0) {
                contactAddressWithProductsStringBuilder.append(" ").append(contactAddressWithProducts.getAddress().getStreet());
            }
            if (contactAddressWithProducts.getAddress().getPostalCode() != null
                && contactAddressWithProducts.getAddress().getPostalCode().length() > 0) {
                contactAddressWithProductsStringBuilder.append(" ").append(contactAddressWithProducts.getAddress().getPostalCode());
            }
            if (contactAddressWithProducts.getAddress().getRegion() != null
                && contactAddressWithProducts.getAddress().getRegion().length() > 0) {
                contactAddressWithProductsStringBuilder.append(" ").append(contactAddressWithProducts.getAddress().getRegion());
            }
        }
        final String timeRange = startTime + "-" + endTime;
        List<CalendarEventUserConnection> calendarEventUserConnections = null;
        if (addCalendarEventUserConnections) {
            calendarEventUserConnections = null;
        }
        final DateTime today = new DateTime().withTimeAtStartOfDay();
        final Integer remainingDays = Days.daysBetween(today, new DateTime(calendarEvent.getStartsAt()).withTimeAtStartOfDay()).getDays();
        final CalendarEventPreparedContainer calendarEventPreparedContainer = new CalendarEventPreparedContainer.CalendarEventPreparedContainerBuilder()
            .setId(calendarEvent.getId()).setTitle(calendarEvent.getTitle()).setLocation(calendarEvent.getLocation())
            .setCreateContact(calendarEvent.isCreateContact()).setTimeRange(timeRange)
            .setContactAddressWithProductsText(contactAddressWithProductsStringBuilder.toString()).setContact(calendarEvent.getContact())
            .setStartsAt(calendarEvent.getStartsAt()).setEndsAt(calendarEvent.getEndsAt()).setColor(color, color).setDraggable(true)
            .setResizable(true).setCssClass(getCssClasses(calendarEvent))
            .setProjectId(calendarEvent.getProject() != null ? calendarEvent.getProject().getId() : null)
            .setProjectName(calendarEvent.getProject() != null ? calendarEvent.getProject().getName() : null)
            .setSerialDate(calendarEvent.getSerialDate()).setSerialDateNumber(calendarEvent.getSerialDateNumber())
            .setCalendarEventSerialDateType(calendarEvent.getCalendarEventSerialDateType()).setConfirmed(calendarEvent.getConfirmed())
            .setArchivedHistoryType(calendarEvent.getArchivedHistoryType()).setArchived(calendarEvent.getArchived())
            .setSerialEndDate(calendarEvent.getSerialEndDate()).setInformation(calendarEvent.getInformation())
            .setCalendarEventTyp(calendarEvent.getCalendarEventTyp()).setRemainingDays(remainingDays)
            .setCalendarEventUserConnections(calendarEventUserConnections).build();
        return calendarEventPreparedContainer;
    }

    private String getCssClasses(final CalendarEvent calendarEvent) {
        StringBuilder cssClasses = new StringBuilder();
        cssClasses.append("");
        if (calendarEvent.getSerialDate() != null && calendarEvent.getSerialDate() == true) {
            cssClasses.append("serialDate");
        }
        cssClasses.append(" ").append(calendarEvent.getCalendarEventTyp());
        if (calendarEvent.getCalendarEventTyp() == CalendarEventTypeEnum.OUTSIDE_WORK
            || calendarEvent.getCalendarEventTyp() == CalendarEventTypeEnum.FIX_DATE
            || calendarEvent.getCalendarEventTyp() == CalendarEventTypeEnum.FRAME_DATE
            || calendarEvent.getCalendarEventTyp() == CalendarEventTypeEnum.MEETING) {
            if (calendarEvent.getConfirmed() != null && calendarEvent.getConfirmed() == true) {
                cssClasses.append(" ").append("CALENDAR_EVENT_CONFIRMED");
            } else {
                cssClasses.append(" ").append("CALENDAR_EVENT_NOT_CONFIRMED");
            }
        }
        return cssClasses.toString();
    }

    @Override
    public List<CalendarEvent> create(final CalendarEvent calendarEvent, final User user, final Boolean serialDates,
            final DateTime serialEndDate, final CalendarEventSerialDateTypeEnum calendarEventSerialDateType)
            throws BusinessException {
        BusinessAssert.notNull(calendarEvent, "calendarEvent is mandatory in CalendarEventServiceImpl#create", "400");
        BusinessAssert.notNull(user, "user is mandatory in CalendarEventServiceImpl#create", "400");
        logger.info("create calendarEvent in CalendarEventServiceImpl#create");
        calendarEvent.setTenant(user.getTenant());
        if (calendarEvent.getCalendarEventTyp() == null) {
            calendarEvent.setCalendarEventTyp(CalendarEventTypeEnum.MEETING);
        }

        final List<CalendarEvent> calendarEvents = new ArrayList<>();
        if (serialDates == true) {
            final Long serialDateNumber = System.currentTimeMillis();
            calendarEvent.setSerialDate(true);
            calendarEvent.setSerialDateNumber(serialDateNumber);
            final CalendarEvent createdCalendarEvent = calendarEventRepository.insert(calendarEvent);
            calendarEvents.add(createdCalendarEvent);
            DateTime startDate = new DateTime(createdCalendarEvent.getStartsAt());
            DateTime endDate = new DateTime(createdCalendarEvent.getEndsAt());
            while ((startDate = this.calculateNextSerialDateTime(calendarEventSerialDateType, startDate)).isBefore(serialEndDate)) {
                endDate = this.calculateNextSerialDateTime(calendarEventSerialDateType, endDate);
                final CalendarEvent newCalendarEvent = calendarEvent.copy(calendarEvent);
                newCalendarEvent.setStartsAt(startDate.getMillis());
                newCalendarEvent.setEndsAt(endDate.getMillis());
                newCalendarEvent.setSerialDate(true);
                newCalendarEvent.setSerialDateNumber(serialDateNumber);
                final CalendarEvent createdSerialDateCalendarEvent = calendarEventRepository.insert(newCalendarEvent);
                calendarEvents.add(createdSerialDateCalendarEvent);
                logger.info("created serialDate calendarEvent with id '{}' in CalendarEventServiceImpl#create",
                    createdSerialDateCalendarEvent.getId());
                if (calendarEventSerialDateType == CalendarEventSerialDateTypeEnum.NO_SERIAL_DATE) {
                    break;
                }
            }
        } else {
            final CalendarEvent createdCalendarEvent = calendarEventRepository.insert(calendarEvent);
            calendarEvents.add(createdCalendarEvent);
            logger.info("created unique calendarEvent with id '{}' in CalendarEventServiceImpl#create", createdCalendarEvent.getId());
        }

        final String serialEndDateString = dateFormatter.print(serialEndDate);

        final String message = calendarEvents.get(0).getTitle() + " "
            + (serialDates == true ? "SerienTermin bis " + serialEndDateString : "");
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.CREATED_CALENDAR_EVENT)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in calendarEventRestService#create", createdHistory.getId());

        return calendarEvents;
    }

    @Override
    public void createCalendarEventsForProject(final Project project, final User user) throws BusinessException {
        if (project.getStart() != null) {
            final CalendarEvent calendarEventProjectStart = new CalendarEvent.CalendarEventBuilder().setTile(project.getName())
                .setStartsAt(project.getStart().getMillis()).setEndsAt(project.getStart().getMillis()).setLocation(PROJECT_START)
                .setCalendarEventTyp(CalendarEventTypeEnum.MEETING).setProject(project).setTenant(user.getTenant()).setArchived(false)
                .build();
            final List<CalendarEvent> createdCalendarEvents = this.create(calendarEventProjectStart, user, false, null, null);
            logger.info("create calendar event for project start with id '{}' in CalendarEventServiceImpl#createCalendarEventsForProject",
                createdCalendarEvents.get(0).getId());
        }
        if (project.getEnd() != null) {
            final CalendarEvent calendarEventProjectStart = new CalendarEvent.CalendarEventBuilder().setTile(project.getName())
                .setStartsAt(project.getEnd().getMillis()).setEndsAt(project.getEnd().getMillis()).setLocation(PROJECT_END)
                .setCalendarEventTyp(CalendarEventTypeEnum.MEETING).setProject(project).setTenant(user.getTenant()).setArchived(false)
                .build();
            final List<CalendarEvent> createdCalendarEvents = this.create(calendarEventProjectStart, user, false, null, null);
            logger.info("create calendar event for project start with id '{}' in CalendarEventServiceImpl#createCalendarEventsForProject",
                createdCalendarEvents.get(0).getId());
        }
    }

    @Override
    public List<CalendarEvent> update(CalendarEvent calendarEvent, final User user) throws BusinessException {
        BusinessAssert.notNull(calendarEvent, "calendarEvent is mandatory in CalendarEventServiceImpl#update", "400");
        logger.info("update calendarEvent with id '{}' in CalendarEventServiceImpl#update", calendarEvent.getId());

        final List<CalendarEvent> createdOrUpdatedCalendarEvents = new ArrayList<>();
        // search calendarEvent with lowest startsAt and endsAt
        if (calendarEvent.getSerialDate() != null && calendarEvent.getSerialDate()) {
            final Long serialDateNumber = calendarEvent.getSerialDateNumber();
            List<CalendarEvent> calendarEvents = new ArrayList<>();
            if (serialDateNumber == null) {
                calendarEvent.setSerialDateNumber(System.currentTimeMillis());
                final CalendarEvent changedCalendarEventToSerialEvent = calendarEventRepository.save(calendarEvent);
                logger.info("changed calendarEvent to servial event with id '{}' in CalendarEventServiceImpl#update",
                    changedCalendarEventToSerialEvent.getId());
                calendarEvents.add(calendarEvent);
            } else {
                calendarEvents = calendarEventRepository.findBySerialDateNumberAndArchived(serialDateNumber, false);
            }
            Collections.sort(calendarEvents, new Comparator<CalendarEvent>() {

                public int compare(final CalendarEvent c1, final CalendarEvent c2) {
                    return c1.getStartsAt().compareTo(c2.getStartsAt());
                }
            });
            final CalendarEvent calendarEventWithLowestStart = calendarEvents.get(0);

            final DateTime lowestStart = new DateTime(calendarEventWithLowestStart.getStartsAt());
            final DateTime lowestEnd = new DateTime(calendarEventWithLowestStart.getEndsAt());
            final DateTime calendarStart = new DateTime(calendarEvent.getStartsAt());
            final DateTime calendarEnd = new DateTime(calendarEvent.getEndsAt());
            final int daysBetweenStart = Days.daysBetween(lowestStart, calendarStart).getDays();
            final int daysBetweenEnd = Days.daysBetween(lowestEnd, calendarEnd).getDays();

            final int daysBetweenWeek = Days.daysBetween(lowestStart, calendarStart).getDays() % 7;

            calendarEvent.setStartsAt(calendarStart.minusDays(daysBetweenStart - daysBetweenWeek).getMillis());
            calendarEvent.setEndsAt(calendarEnd.minusDays(daysBetweenEnd - daysBetweenWeek).getMillis());
            final boolean deleteForRecreation = true;
            final boolean deleteSerialDates = true;
            this.delete(user, calendarEvent, deleteSerialDates, deleteForRecreation);
            final List<CalendarEvent> createdCalendarEvents = this.create(calendarEvent, user, calendarEvent.getSerialDate(),
                calendarEvent.getSerialEndDate(), calendarEvent.getCalendarEventSerialDateType());
            createdOrUpdatedCalendarEvents.addAll(createdCalendarEvents);
        } else {
            // if calendarEvent is shifted than create calendarEvent and calendarEventUserConnections with old
            // properties
            final CalendarEvent previousCalendarEvent = findById(calendarEvent.getId());
            createPreviousCalendarEventAndCalendarEventUserConnectionsBeforeShifted(user, previousCalendarEvent, calendarEvent);
            calendarEvent.setInformation("");
            final CalendarEvent updatedCalendarEvent = calendarEventRepository.save(calendarEvent);
            createdOrUpdatedCalendarEvents.add(updatedCalendarEvent);
        }
        return createdOrUpdatedCalendarEvents;
    }

    public void createPreviousCalendarEventAndCalendarEventUserConnectionsBeforeShifted(final User user,
            final CalendarEvent previousCalendarEvent, final CalendarEvent calendarEvent)
            throws BusinessException {
        // if calendarEvent of type outside work is shifted than create calendarEvent and calendarEventUserConnections
        // with old properties
        if ((calendarEvent.getConfirmed() == null || !calendarEvent.getConfirmed())
            && calendarEvent.getCalendarEventTyp() == CalendarEventTypeEnum.OUTSIDE_WORK
            && isCalendarEventShifted(previousCalendarEvent, calendarEvent)) {
            // CalendarEvent
            final CalendarEvent newPreviousCalendarEvent = previousCalendarEvent.copy(previousCalendarEvent);
            newPreviousCalendarEvent.setId(null);
            newPreviousCalendarEvent.setInformation(calendarEvent.getInformation());
            newPreviousCalendarEvent.setArchived(true);
            newPreviousCalendarEvent.setArchivedHistoryType(HistoryTypeEnum.UPDATED_CALENDAR_EVENT);
            final CalendarEvent createdNewPreviousCalendarEvent = calendarEventRepository.insert(newPreviousCalendarEvent);
            logger.info(
                "created new previous calendarEvent with id '{}' in CalendarEventServiceImpl#createPreviousCalendarEventBeforeShifted",
                createdNewPreviousCalendarEvent.getId());

            
        }
    }

    public boolean isCalendarEventShifted(final CalendarEvent previousCalendarEvent, final CalendarEvent currentCalendarEvent) {
        logger.info("is calendarEvent shifted in CalendarEventServiceImpl#isCalendarEventShifted");
        if (previousCalendarEvent.getStartsAt().equals(currentCalendarEvent.getStartsAt())
            && previousCalendarEvent.getEndsAt().equals(currentCalendarEvent.getEndsAt())) {
            return false;
        }
        return true;
    }

    @Override
    public void deactivate(final User user, final CalendarEvent calendarEvent, final Boolean deleteSerialDates,
            final Boolean deleteForRecreation, final HistoryTypeEnum activationHistoryType, final String information)
            throws BusinessException {
        BusinessAssert.notNull(calendarEvent, "calendarEventId is mandatory in CalendarEventServiceImpl#deactivate", "400");
        BusinessAssert.notNull(deleteSerialDates, "deleteSerialDates must be an id in CalendarEventServiceImpl#deactivate", "400");
        logger.info("deactivate calendarEvent with id '{}' in CalendarEventServiceImpl#deactivate", calendarEvent.getId());

        if (deleteSerialDates) {
            final Long serialDateNumber = calendarEvent.getSerialDateNumber();
            final List<CalendarEvent> serialDateCalendarEvents = this.findBySerialDateNumber(serialDateNumber);
            
        } else {
           
        }
    }

    @Override
    public void delete(final User user, final CalendarEvent calendarEvent, final Boolean deleteSerialDates,
            final Boolean deleteForRecreation)
            throws BusinessException {
        BusinessAssert.notNull(calendarEvent, "calendarEventId is mandatory in CalendarEventServiceImpl#delete", "400");
        BusinessAssert.notNull(deleteSerialDates, "deleteSerialDates must be an id in CalendarEventServiceImpl#delete", "400");
        logger.info("delete calendarEvent with id '{}' in CalendarEventServiceImpl#create", calendarEvent.getId());

        final List<Activity> activities = activityService.getActivitiesByCalendarEventId(calendarEvent.getId());
        final List<FacilityDetails> facilityDetails = facilityDetailsService.getFacilityDetailsByCalendarEventId(calendarEvent.getId());

        if (activities != null && !activities.isEmpty()) {
            for (final Activity activity : activities) {
                activityService.delete(activity.getId());
                logger.info("activity with id '{}' deleted in CalendarEventServiceImpl#delete", activity.getId());
            }
        }
        if (facilityDetails != null && !facilityDetails.isEmpty()) {
            for (final FacilityDetails facilityDetail : facilityDetails) {
                facilityDetailsService.delete(facilityDetail.getId());
                logger.info("facilityDetail with id '{}' deleted in CalendarEventServiceImpl#delete", facilityDetail.getId());
            }
        }

        
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all calendarEvents in CalendarEventServiceImpl#deleteAll");
        calendarEventRepository.deleteAll();
    }

    @Override
    public DateTime calculateNextSerialDateTime(final CalendarEventSerialDateTypeEnum calendarEventSerialDateType, DateTime date)
            throws BusinessException {
        BusinessAssert.notNull(calendarEventSerialDateType,
            "calendarEventSerialDateType is mandatory in CalendarEventServiceImpl#calculateNextSerialDateTime", "400");
        BusinessAssert.notNull(date, "date is mandatory in CalendarEventServiceImpl#calculateNextSerialDateTime", "400");
        logger.info(
            "calculate next serialDateTime for calendarEventSerialDateType '{}' and date {}' in CalendarEventServiceImpl#calculateNextSerialDateTime",
            calendarEventSerialDateType, date);
        switch (calendarEventSerialDateType) {
        case EVERY_WEEK:
            date = date.plusWeeks(1);
            break;
        case EVERY_SECOND_WEEK:
            date = date.plusWeeks(2);
            break;
        case EVERY_THIRD_WEEK:
            date = date.plusWeeks(3);
            break;
        case EVERY_FOURTH_WEEK:
            date = date.plusWeeks(4);
            break;
        case EVERY_FIFTH_WEEK:
            date = date.plusWeeks(5);
            break;
        case EVERY_SIXTH_WEEK:
            date = date.plusWeeks(6);
            break;
        case EVERY_SEVENTH_WEEK:
            date = date.plusWeeks(7);
            break;
        case EVERY_EIGHT_WEEK:
            date = date.plusWeeks(8);
            break;
        case EVERY_NINTH_WEEK:
            date = date.plusWeeks(9);
            break;
        case EVERY_TENTH_WEEK:
            date = date.plusWeeks(10);
            break;
        case EVERY_ELEVENTH_WEEK:
            date = date.plusWeeks(11);
            break;
        case EVERY_TWELFETH_WEEK:
            date = date.plusWeeks(12);
            break;
        default:
            break;
        }
        return date;
    }

    @Override
    public List<CalendarEvent> findByCalendarEventTypAndConfirmed(final CalendarEventTypeEnum calendarEventType, final boolean confirmed)
            throws BusinessException {
        BusinessAssert.notNull(calendarEventType,
            "calendarEventType is mandatory in CalendarEventServiceImpl#findByCalendarEventTypAndConfirmed", "400");
        logger.info(
            "find calendarEvents by calendarEvent of type '{}' and confirmed '{}' in CalendarEventServiceImpl#findByCalendarEventTypAndConfirmed",
            calendarEventType, confirmed);
        return calendarEventRepository.findByCalendarEventTypAndConfirmedAndArchived(calendarEventType, confirmed, false);
    }

    private List<CalendarEvent> getUniqueCalendarEvents(final List<CalendarEvent> calendarEvents) {
        final List<CalendarEvent> uniqueCalendarEvents = new ArrayList<>();
        for (final CalendarEvent calendarEvent : calendarEvents) {
            if (!this.contains(uniqueCalendarEvents, calendarEvent)) {
                uniqueCalendarEvents.add(calendarEvent);
            }
        }
        return uniqueCalendarEvents;
    }

    private boolean contains(final List<CalendarEvent> calendarEvents, final CalendarEvent calendarEvent) {
        for (final CalendarEvent stillAddedCalendarEvent : calendarEvents) {
            if (stillAddedCalendarEvent.getId().equals(calendarEvent.getId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "[CalendarEventServiceImpl]";
    }
}
