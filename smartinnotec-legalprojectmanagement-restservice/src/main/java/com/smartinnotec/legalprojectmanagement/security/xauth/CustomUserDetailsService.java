package com.smartinnotec.legalprojectmanagement.security.xauth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserRoleEnum;
import com.smartinnotec.legalprojectmanagement.service.UserService;

public class CustomUserDetailsService implements UserDetailsService {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private UserService userService;

    public CustomUserDetailsService(final UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        try {
            final User user = userService.findUserByUsernameOrEmail(username);
            logger.debug("load user with name '{}' by username or email in CustomUserDetailsService#loadUserByUsername", username);
            return new SimpleUserDetails(user.getId(), user.getUsername(), user.getPassword(), user, UserRoleEnum.USER.toString());
        } catch (BusinessException | UsernameNotFoundException e) {
            throw new UsernameNotFoundException("Fehler beim einloggen - " + username + " nicht gefunden");
        }
    }

    @Override
    public String toString() {
        return "[CustomUserDetailsService]";
    }
}
