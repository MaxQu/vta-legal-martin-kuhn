package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Receipe;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface ProductService {

	Integer count() throws BusinessException;
	
    Boolean checkUniqueProductNumber(final User user, final String productNumber, final String type, final String productIdToUpdate)
            throws BusinessException;

    Boolean checkUniqueProductName(final User user, final String productName, final String type, final String productIdToUpdate)
            throws BusinessException;

    Product findProduct(final String id) throws BusinessException;

    List<Product> findAllProductsByTenant(final Tenant tenant) throws BusinessException;

    List<Product> findProductsFacade(final String searchTerm) throws BusinessException;

    List<Product> findProductByTerm(final String searchTerm) throws BusinessException;

    List<Product> findProductsByRegex(final String searchTerm) throws BusinessException;

    List<Product> findPagedProductsByTenant(final Tenant tenant, final Integer page, final String sortingType) throws BusinessException;

    List<Product> findProductsOfReceipes(final List<Receipe> receipes) throws BusinessException;

    Integer getAmountOfProducts(final Tenant tenant) throws BusinessException;

    boolean checkIfProductExists(final String id) throws BusinessException; // check if product exist

    Product create(final Product product) throws BusinessException;

    Product update(final Product product) throws BusinessException;

    boolean renameFolder(final String productId, final String username) throws BusinessException;

    boolean productContainedInProducts(final Product product, final List<Product> products) throws BusinessException;

    void deleteProduct(final String productId, final String userId, final String username) throws BusinessException;

    void deleteAll() throws BusinessException;

}
