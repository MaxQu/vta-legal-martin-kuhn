package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.LabelsPredefined;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface LabelsPredefinedRepository extends MongoRepository<LabelsPredefined, String> {

    List<LabelsPredefined> findByTenant(final Tenant tenant);

    Set<LabelsPredefined> findLabelsPredefinedByNameStartsWith(final String searchString);

}
