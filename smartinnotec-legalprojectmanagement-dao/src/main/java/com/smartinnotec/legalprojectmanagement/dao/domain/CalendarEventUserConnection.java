package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "CalendarEventUserConnection")
public @Data class CalendarEventUserConnection implements Comparable<CalendarEventUserConnection> {

    @Id
    private String id;
    @DBRef
    private User user;
    @DBRef
    private Contact contact;
    private ContactAddressWithProducts contactAddressWithProducts; // address of weiterer Standort
    private List<ContactPerson> contactPersons;
    @DBRef
    private CalendarEvent calendarEvent;
    private boolean emailActive;
    private HistoryTypeEnum archivedHistoryType;
    private Boolean archived;

    public CalendarEventUserConnection() {
        this.archived = false;
    }

    public CalendarEventUserConnection copy() {
        final CalendarEventUserConnection copiedCalendarEventUserConnection = new CalendarEventUserConnection();
        copiedCalendarEventUserConnection.setUser(user);
        copiedCalendarEventUserConnection.setContact(contact);
        copiedCalendarEventUserConnection.setContactPersons(contactPersons);
        copiedCalendarEventUserConnection.setCalendarEvent(calendarEvent);
        copiedCalendarEventUserConnection.setEmailActive(emailActive);
        copiedCalendarEventUserConnection.setArchivedHistoryType(archivedHistoryType);
        copiedCalendarEventUserConnection.setArchived(archived);

        if (contactAddressWithProducts != null) {
            final ContactAddressWithProducts contactAddressWithProducts = new ContactAddressWithProducts();
            contactAddressWithProducts
                .setAddress(contactAddressWithProducts.getAddress() != null ? contactAddressWithProducts.getAddress().deepCopy() : null);
            contactAddressWithProducts.setProductIds(contactAddressWithProducts.getProductIds());
            copiedCalendarEventUserConnection.setContactAddressWithProducts(contactAddressWithProducts);
        }

        return copiedCalendarEventUserConnection;
    }

    @Override
    public int compareTo(final CalendarEventUserConnection calendarEventUserConnection) {
        return this.calendarEvent.getStartsAt().compareTo(calendarEventUserConnection.getCalendarEvent().getStartsAt());
    }
}
