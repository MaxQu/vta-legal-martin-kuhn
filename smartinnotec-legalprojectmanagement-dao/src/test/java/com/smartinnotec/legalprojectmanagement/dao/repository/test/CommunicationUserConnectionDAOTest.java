package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public class CommunicationUserConnectionDAOTest extends AbstractDAOTest {

    @Test
    public void shouldFindCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted() {

        final User user = new User();
        user.setFirstname("Max");
        user.setSurname("Mustermann");
        final User userCreated = userRepository.insert(user);
        Assert.assertNotNull(userCreated);

        final CommunicationUserConnection communicationUserConnectionWithNoExecutionDate = new CommunicationUserConnection();
        communicationUserConnectionWithNoExecutionDate.setUserReceivedMessage(user);
        communicationUserConnectionWithNoExecutionDate.setConfirmationNeeded(true);
        communicationUserConnectionWithNoExecutionDate.setRead(false);
        final CommunicationUserConnection createdCommunicationUserConnectionWithNoExecutionDate = communicationUserConnectionRepository
            .insert(communicationUserConnectionWithNoExecutionDate);
        Assert.assertNotNull(createdCommunicationUserConnectionWithNoExecutionDate);

        final CommunicationUserConnection communicationUserConnection = new CommunicationUserConnection();
        communicationUserConnection.setUserReceivedMessage(user);
        communicationUserConnection.setConfirmationNeeded(true);
        communicationUserConnection.setRead(false);
        communicationUserConnection.setExecutionDate(new DateTime(System.currentTimeMillis()));
        communicationUserConnection.setExecuted(false);
        final CommunicationUserConnection createdCommunicationUserConnection = communicationUserConnectionRepository
            .insert(communicationUserConnection);
        Assert.assertNotNull(createdCommunicationUserConnection);

        final List<CommunicationUserConnection> communicationUserConnections = communicationUserConnectionRepository
            .findCommunicationUserConnectionByUserReceivedMessageAndConfirmationNeededAndReadAndExecutionDateNotNullAndExecuted(user,
                isConfirmationNeeded(true), isRead(false), isExecuted(false));
        Assert.assertNotNull(communicationUserConnections);
        Assert.assertEquals(1, communicationUserConnections.size());
    }

    private boolean isConfirmationNeeded(final boolean confirmationNeeded) {
        return confirmationNeeded;
    }

    private boolean isRead(final boolean read) {
        return read;
    }

    private boolean isExecuted(final boolean executed) {
        return executed;
    }
}
