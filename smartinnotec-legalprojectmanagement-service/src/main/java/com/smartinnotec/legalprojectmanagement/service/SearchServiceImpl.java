package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Communication;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactPerson;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.Folder;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.TimeRegistration;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;
import com.smartinnotec.legalprojectmanagement.service.container.BreadCrumbContainer;
import com.smartinnotec.legalprojectmanagement.service.container.SearchResult;

@Service("searchService")
public class SearchServiceImpl extends AbstractService implements SearchService {

    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProductService productService;
    @Autowired
    private DocumentFileService documentFileService;
    @Autowired
    private FolderService folderService;
    @Autowired
    private CalendarEventService calendarEventService;
    
    @Autowired
    private ContactService contactService;
    @Autowired
    private WorkingBookService workingBookService;
    @Autowired
    private TimeRegistrationService timeRegistrationService;
    @Autowired
    private CommunicationUserConnectionService communicationUserConnectionService;

    @Override
    public List<SearchResult> searchInProducts(String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInProducts", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInProducts", "400");
        logger.info("search in products for searchString '{}' in SearchServiceImpl#searchInProducts", searchString);
        final List<Product> products = productService.findProductsFacade(searchString);
        final List<SearchResult> searchResults = new ArrayList<>();
        for (final Product product : products) {
            final SearchResult searchResult = new SearchResult();
            searchResult.setApplicationCategorie(ApplicationCategorieEnum.PRODUCTS);
            searchResult.setSearchType(SearchTypeEnum.PRODUCT);
            searchResult.setProduct(product);
            searchResults.add(searchResult);
        }
        logger.debug("found '{}' products in SearchServiceImpl#searchInFolders", searchResults.size());
        return searchResults;
    }

    @Override
    public List<SearchResult> searchDocumentFilesInProducts(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchDocumentFilesInProducts", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchDocumentFilesInProducts", "400");
        logger.info("search documentFiles in products for searchString '{}' in SearchServiceImpl#searchDocumentFilesInProducts",
            searchString);
        final List<SearchResult> searchResults = new ArrayList<>();

        final List<DocumentFile> documentFiles = documentFileService.findBySearchString(searchString, user);

        for (final DocumentFile documentFile : documentFiles) {
            final String projectProductId = documentFile.getProjectProductId();
            final Product product = productService.findProduct(projectProductId);
            if (documentFile.getUserIdBlackList().contains(user.getId())) {
                continue;
            }
            // only search files in product
            if (product == null) {
                continue;
            }
            final BreadCrumbContainer breadCrumbContainer = folderService.findBreadCrumbOfFolder(documentFile.getFolderId());
            final List<String> additionalInformation = new ArrayList<>();
            if (breadCrumbContainer.getBreadCrumbStringOfFolders() != null
                && !breadCrumbContainer.getBreadCrumbStringOfFolders().isEmpty()) {
                additionalInformation.addAll(breadCrumbContainer.getBreadCrumbStringOfFolders());
            } else {
                additionalInformation.add(product.getName());
            }
            final SearchResult searchResult = new SearchResult();
            searchResult.setApplicationCategorie(ApplicationCategorieEnum.DOCUMENTS);
            searchResult.setDocumentFile(documentFile);
            searchResult.setAdditionalInformation(additionalInformation);
            searchResult.setSearchType(SearchTypeEnum.PRODUCT);
            searchResults.add(searchResult);
        }
        return searchResults;
    }

    @Override
    public List<SearchResult> searchInDocumentFiles(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInDocuments", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInDocumentFiles", "400");
        logger.info("search in documentFiles for searchString '{}' in SearchServiceImpl#searchInDocuments", searchString);
        final List<DocumentFile> documentFiles = documentFileService.findDocumentFilesBySearchString(user, searchString);

        final List<SearchResult> searchResults = new ArrayList<>();
        for (final DocumentFile documentFile : documentFiles) {
            // id document is black listed for user
            if (documentFile.getUserIdBlackList().contains(user.getId())) {
                continue;
            }
            // document is confident and user has not right role to see it
            final Project project = projectService.findProject(documentFile.getProjectProductId());
            final ProjectUserConnection projectUserConnection = projectUserConnectionService.findProjectUserConnectionByUserAndProject(user,
                project);
            final boolean canUserSeeConfidentialDocumentFiles = projectUserConnectionService
                .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(projectUserConnection.getProjectUserConnectionRole());
            if (documentFile.isConfident() && !canUserSeeConfidentialDocumentFiles) {
                continue;
            }
            final BreadCrumbContainer breadCrumbContainer = folderService.findBreadCrumbOfFolder(documentFile.getFolderId());
            final List<String> additionalInformation = new ArrayList<>();
            if (breadCrumbContainer.getBreadCrumbStringOfFolders() != null
                && !breadCrumbContainer.getBreadCrumbStringOfFolders().isEmpty()) {
                additionalInformation.addAll(breadCrumbContainer.getBreadCrumbStringOfFolders());
            } else {
                additionalInformation.add(project.getName());
            }

            final SearchResult searchResult = new SearchResult();
            searchResult.setApplicationCategorie(ApplicationCategorieEnum.DOCUMENTS);
            searchResult.setAdditionalInformation(additionalInformation);
            searchResult.setDocumentFile(documentFile);
            searchResult.setSearchType(SearchTypeEnum.PROJECT);
            searchResults.add(searchResult);
        }
        logger.debug("found '{}' documentFiles in SearchServiceImpl#searchInDocuments", searchResults.size());
        return searchResults;
    }

    @Override
    public List<SearchResult> searchInProjects(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInProjects", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInProjects", "400");
        logger.info("search in projects for searchString '{}' in SearchServiceImpl#searchInProjects", searchString);

        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);

        final List<SearchResult> searchResults = new ArrayList<>();
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            final Project project = projectUserConnection.getProject();
            final List<ProjectUserConnection> projectUserConnectionsOfProject = projectUserConnectionService.findByProjectAndActive(project,
                true);
            if (project.getName().toLowerCase().contains(searchString.toLowerCase())
                || (project.getDescription() != null && project.getDescription().toLowerCase().contains(searchString.toLowerCase()))
                || isSearchStringFoundInMemberOfProject(searchString, projectUserConnectionsOfProject)) {
                final SearchResult searchResult = new SearchResult();
                searchResult.setApplicationCategorie(ApplicationCategorieEnum.PROJECTS);
                searchResult.setProjectUserConnection(projectUserConnection);
                searchResults.add(searchResult);
            }
        }
        logger.debug("found '{}' projects in SearchServiceImpl#searchInProjects", searchResults.size());
        return searchResults;
    }

    @Override
    public List<SearchResult> searchInProjectFolders(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInProjectFolders", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInProjectFolders", "400");
        logger.info("search in folder for searchString '{}' in SearchServiceImpl#searchInProjectFolders", searchString);
        final List<Folder> folders = folderService.findProjectFoldersBySearchString(user, searchString);
        if (folders == null || folders.isEmpty()) {
            return new ArrayList<>();
        }
        final List<SearchResult> searchResults = new ArrayList<>();
        final Project project = projectService.findProject(folders.get(0).getProjectId());
        final ProjectUserConnection projectUserConnection = projectUserConnectionService.findProjectUserConnectionByUserAndProject(user,
            project);
        for (final Folder folder : folders) {
            final BreadCrumbContainer breadCrumbContainer = folderService.findBreadCrumbOfFolder(folder.getId());
            final SearchResult searchResult = new SearchResult();
            searchResult.setApplicationCategorie(ApplicationCategorieEnum.FOLDERS);
            searchResult.setAdditionalInformation(breadCrumbContainer.getBreadCrumbStringOfFolders());
            searchResult.setProjectUserConnection(projectUserConnection);
            searchResult.setFolder(folder);
            searchResult.setSearchType(SearchTypeEnum.PROJECT);
            searchResults.add(searchResult);
        }
        logger.debug("found '{}' folders in SearchServiceImpl#searchInProjectFolders", searchResults.size());
        return searchResults;
    }

    @Override
    public List<SearchResult> searchInProductFolders(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInProductFolders", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInProductFolders", "400");
        logger.info("search in folder for searchString '{}' in SearchServiceImpl#searchInProductFolders", searchString);
        final List<Folder> folders = folderService.findProductFoldersBySearchString(user, searchString);
        if (folders == null || folders.isEmpty()) {
            return new ArrayList<>();
        }
        final List<SearchResult> searchResults = new ArrayList<>();
        for (final Folder folder : folders) {
            final String projectProductId = folder.getProjectId();
            final Product product = productService.findProduct(projectProductId);
            if (product != null) {
                final BreadCrumbContainer breadCrumbContainer = folderService.findBreadCrumbOfFolder(folder.getId());
                final SearchResult searchResult = new SearchResult();
                searchResult.setApplicationCategorie(ApplicationCategorieEnum.PRODUCT_FOLDERS);
                searchResult.setAdditionalInformation(breadCrumbContainer.getBreadCrumbStringOfFolders());
                searchResult.setProduct(product);
                searchResult.setFolder(folder);
                searchResult.setSearchType(SearchTypeEnum.PRODUCT);
                searchResults.add(searchResult);
            }
        }
        logger.debug("found '{}' folders in SearchServiceImpl#searchInProductFolders", searchResults.size());
        return searchResults;
    }

    @Override
    public List<SearchResult> searchInCalendar(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInCalendar", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInCalendar", "400");
        logger.info("search in calendar for searchString '{}' in SearchServiceImpl#searchInCalendar", searchString);

        final List<SearchResult> searchResults = new ArrayList<>();

        final ProjectUserConnectionRoleEnum projectUserConnectionRole = user.getProjectUserConnectionRole();
        if (projectUserConnectionRole == ProjectUserConnectionRoleEnum.ADMIN) {
            final List<CalendarEvent> allCalendarEvents = calendarEventService.findCalendarEventsBySearchString(searchString);
            for (final CalendarEvent calendarEvent : allCalendarEvents) {
                
            }
            return searchResults;
        }

        
        logger.debug("found '{}' calendarEvents in SearchServiceImpl#searchInCalendar", searchResults.size());
        return searchResults;
    }

    @Override
    public List<SearchResult> searchInContacts(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInContacts", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInContacts", "400");
        logger.info("search in contacts for searchString '{}' in SearchServiceImpl#searchInContacts", searchString);
        final List<Contact> contacts = contactService.findContactBySearchString(user, searchString);

        final List<SearchResult> searchResults = new ArrayList<>();
        for (final Contact contact : contacts) {
            final SearchResult searchResult = new SearchResult();
            searchResult.setApplicationCategorie(ApplicationCategorieEnum.CONTACTS);
            searchResult.setContact(contact);
            searchResults.add(searchResult);
        }
        logger.debug("found '{}' contacts in SearchServiceImpl#searchInContacts", searchResults.size());
        return searchResults;
    }

    @Override
    public List<SearchResult> searchInWorkingBook(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInWorkingBook", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInWorkingBook", "400");
        logger.info("search in workingBooks for searchString '{}' in SearchServiceImpl#searchInWorkingBook", searchString);

        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);

        final List<SearchResult> searchResults = new ArrayList<>();
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            final Project project = projectUserConnection.getProject();
            final List<WorkingBook> workingBooks = workingBookService.findWorkingBookByProjectAndTenantAndSearchString(project,
                user.getTenant(), searchString);
            for (final WorkingBook workingBook : workingBooks) {
                final SearchResult searchResult = new SearchResult();
                searchResult.setApplicationCategorie(ApplicationCategorieEnum.WORKINGBOOK);
                searchResult.setWorkingBook(workingBook);
                searchResults.add(searchResult);
            }
        }
        logger.debug("found '{}' workingBook in SearchServiceImpl#searchInWorkingBook", searchResults.size());
        return searchResults;
    }

    @Override
    public List<SearchResult> searchInCommunication(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInCommunication", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInCommunication", "400");
        logger.info("search in communication for searchString '{}' in SearchServiceImpl#searchInCommunication", searchString);

        final List<CommunicationUserConnection> communicationUserConnectionsByUserReceivedMessage = communicationUserConnectionService
            .findCommunicationUserConnectionByUserCreatedMessage(user);
        communicationUserConnectionsByUserReceivedMessage.removeIf(s -> communicationUserConnectionService.isCreatedOrReceivedUserNotSet(s)
            || s.getUserCreatedMessage().getId().equals(s.getUserReceivedMessage().getId()));
        final List<CommunicationUserConnection> communicationUserConnectionsByUserCreatedMessage = communicationUserConnectionService
            .findCommunicationUserConnectionByUserReceivedMessage(user);
        communicationUserConnectionsByUserCreatedMessage.removeIf(s -> communicationUserConnectionService.isCreatedOrReceivedUserNotSet(s)
            || s.getUserCreatedMessage().getId().equals(s.getUserReceivedMessage().getId()));

        final List<SearchResult> searchResults = new ArrayList<>();
        for (final CommunicationUserConnection communicationUserConnection : communicationUserConnectionsByUserCreatedMessage) {
            final Communication communication = communicationUserConnection.getCommunication();
            final String title = communication.getTitle();
            final String message = communication.getMessage();

            if ((title != null && title.toLowerCase().contains(searchString.toLowerCase()))
                || message.toLowerCase().contains(searchString.toLowerCase())) {
                final SearchResult searchResult = new SearchResult();
                searchResult.setApplicationCategorie(ApplicationCategorieEnum.COMMUNICATION);
                searchResult.setCommunicationUserConnectionCreated(communicationUserConnection);
                searchResults.add(searchResult);
            }
        }
        for (final CommunicationUserConnection communicationUserConnection : communicationUserConnectionsByUserReceivedMessage) {
            final Communication communication = communicationUserConnection.getCommunication();
            final String title = communication.getTitle();
            final String message = communication.getMessage();

            if ((title != null && title.toLowerCase().contains(searchString.toLowerCase()))
                || message.toLowerCase().contains(searchString.toLowerCase())) {
                final SearchResult searchResult = new SearchResult();
                searchResult.setApplicationCategorie(ApplicationCategorieEnum.COMMUNICATION);
                searchResult.setCommunicationUserConnectionReceived(communicationUserConnection);
                searchResults.add(searchResult);
            }
        }

        return searchResults;
    }

    @Override
    public List<SearchResult> searchInTimeRegistration(final String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in SearchServiceImpl#searchInTimeRegistration", "400");
        BusinessAssert.notNull(user, "user is mandatory in SearchServiceImpl#searchInTimeRegistration", "400");
        logger.info("search in timeRegistration for searchString '{}' in SearchServiceImpl#searchInTimeRegistration", searchString);

        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);

        final List<SearchResult> searchResults = new ArrayList<>();
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            final Project project = projectUserConnection.getProject();
            final List<TimeRegistration> timeRegistrations = timeRegistrationService
                .findTimeRegistrationsBySearchStringAndProjectAndUser(searchString, project, user);
            for (final TimeRegistration timeRegistration : timeRegistrations) {
                if (timeRegistration.getText().toLowerCase().contains(searchString.toLowerCase())) {
                    final SearchResult searchResult = new SearchResult();
                    searchResult.setApplicationCategorie(ApplicationCategorieEnum.TIME_REGISTRATION);
                    searchResult.setTimeRegistration(timeRegistration);
                    searchResults.add(searchResult);
                }
            }
        }
        logger.debug("found '{}' timeRegistration in SearchServiceImpl#searchInTimeRegistration", searchResults.size());
        return searchResults;
    }

    @Override
    public String prepareSearchTerm(final String searchTerm) {
        return searchTerm.replaceAll("-slash-", "/");
    }

    @Override
    public boolean combineSearchStringWithAnd(final String searchString) {
        final String[] partsOfSearchString = searchString.split(" ");
        if (partsOfSearchString.length > 1) {
            for (int i = 1; i < partsOfSearchString.length; i++) {
                if (!partsOfSearchString[i].startsWith("+")) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isSearchStringFoundInMemberOfProject(final String searchString,
            final List<ProjectUserConnection> projectUserConnections) {
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            final User user = projectUserConnection.getUser();
            final Contact contact = projectUserConnection.getContact();
            if (user != null) {
                final String userName = user.getFirstname() + " " + user.getSurname();
                if (userName.toLowerCase().contains(searchString.toLowerCase())) {
                    return true;
                }
            } else if (contact != null) {
                final String propertyStringsOfContact = getPropertyStringsOfContact(contact);
                if (propertyStringsOfContact.toLowerCase().contains(searchString.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isSearchStringFoundInMembersOfCalendarEvent(final String searchString,
            final List<CalendarEventUserConnection> calendarEventUserConnectionsOfEvent) {
        for (final CalendarEventUserConnection calendarEventUserConnection : calendarEventUserConnectionsOfEvent) {
            final User user = calendarEventUserConnection.getUser();
            final Contact contact = calendarEventUserConnection.getContact();
            if (user != null) {
                final String userName = user.getFirstname() + " " + user.getSurname();
                if (userName.toLowerCase().contains(searchString.toLowerCase())) {
                    return true;
                }
            } else if (contact != null) {
                final String propertyStringsOfContact = getPropertyStringsOfContact(contact);
                if (propertyStringsOfContact.toLowerCase().contains(searchString.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    private String getPropertyStringsOfContact(final Contact contact) {
        final StringBuffer sb = new StringBuffer();
        sb.append(contact.getInstitution()).append(" ").append(contact.getAddress().getPostalCode()).append(" ")
            .append(contact.getAddress().getStreet()).append(" ").append(contact.getAddress().getRegion()).append(" ")
            .append(contact.getAddress().getProvinceType()).append(contact.getHomepage()).append(" ");
        if (contact.getEmails() != null) {
            for (final String email : contact.getEmails()) {
                sb.append(email).append(" ");
            }
        }
        if (contact.getTelephones() != null) {
            for (final String telephone : contact.getTelephones()) {
                sb.append(telephone).append(" ");
            }
        }
        if (contact.getContactPersons() != null) {
            for (final ContactPerson contactPerson : contact.getContactPersons()) {
                sb.append(contactPerson.getFirstname()).append(" ").append(contactPerson.getSurname()).append(" ");
            }
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return "[SearchServiceImpl]";
    }
}
