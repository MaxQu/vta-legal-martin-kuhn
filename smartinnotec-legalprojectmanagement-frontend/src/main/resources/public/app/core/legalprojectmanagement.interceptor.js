(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement')
    	.config(configure);
    			
    configure.$inject = ['$httpProvider', 'authTokenServiceProvider', 'token_config'];
    
	function configure($httpProvider, authTokenServiceProvider, token_config) {
		$httpProvider.interceptors.push(legalProjectManagementInterceptor);
		
		////////////
		
		function legalProjectManagementInterceptor($q) {
			var interceptor = {
					request: request,
					response: response,
					responseError: responseError
			};
			
			return interceptor;
			
			////////////
			
			function request(config) {
				var token = authTokenServiceProvider.$get().getToken();
				if (token) {
					config.headers[token_config.AUTH_TOKEN_HEADER] = token;
				}
				return config;
			}
			
			function response(response) {
				return response;
			}
			
			function responseError(response) {
				switch(response.status) {
				case 401:
					authTokenServiceProvider.$get().deleteToken();	
					break;
				}
				return $q.reject(response);
			}
		}
	}
})();