package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class ReceipeAttachment {

    private String documentFileId;
    private String filepath;
    private String filename;
    private String ending;
    private String version;
    private String originalFilename;
    private boolean isImage;

}
