(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.reporting', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();