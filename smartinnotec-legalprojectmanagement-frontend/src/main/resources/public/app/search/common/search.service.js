(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.search')
    	.factory('searchService', searchService);
        
    searchService.$inject = ['$http', 'api_config'];
    
    function searchService($http, api_config) {
		var service = {
			search: search,
			getSearchHistory: getSearchHistory
		};
		
		return service;
		
		////////////
		
		function search(searchString, categories, lastSearchResultLength) {
			return $http.put(api_config.BASE_URL + '/search/' + searchString + '/' + lastSearchResultLength, categories);
		}
		
		function getSearchHistory(userId) {
			return $http.get(api_config.BASE_URL + '/search/searchhistory/' + userId);
		}
    }
})();