(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.controller('UserProfileController', UserProfileController);
    
    UserProfileController.$inject = ['$scope', '$timeout', 'user', 'authUserService', 'userService', 'changePasswordModalService', 'Upload', 'currentUser', 'communicationModes'];
    
    function UserProfileController($scope, $timeout, user, authUserService, userService, changePasswordModalService, Upload, currentUser, communicationModes) {
	    $scope.vm = this;
		var vm = this;
		
		vm.Upload = Upload;
		vm.currentUser = user.data;
		vm.communicationModes = communicationModes.data;
		vm.currentUser.anotherEmails = (vm.currentUser.anotherEmails == null || typeof vm.currentUser.anotherEmails == 'undefined') ? [] : vm.currentUser.anotherEmails;
		vm.currentUser.anotherTelephones = (vm.currentUser.anotherTelephones == null || typeof vm.currentUser.anotherTelephones == 'undefined') ? [] : vm.currentUser.anotherTelephones;
		vm.updateUser = updateUser;
		vm.uploadProfileImage = uploadProfileImage;
	    vm.showChangePasswordModalService = showChangePasswordModalService;
		
		////////////
		
		vm.submitColor = function(type) {
			updateUser(type);
	    };
	    
		function updateUser(type) {
			userService.updateUser(vm.currentUser).success(function(data) {
				vm.currentUser = data;
				setUpdateSuccessType(type);
				setUpdateUserTimeout();
				return vm.currentUser;
			}).error(function(data) {
				console.log('error in user-profile.controller.js#updateUser: ' + data);
			});
		}
		
		function setUpdateUserTimeout() {
      		$timeout(function() {
      			vm.nameSuccess = false;
      			vm.countrySuccess = false;
      			vm.locationSuccess = false;
      			vm.streetSuccess = false;
      			vm.telephoneSuccess = false;
      			vm.anotherTelephoneSuccess = false;
      			vm.emailSuccess = false;
      			vm.anotherEmailSuccess = false;
      			vm.birthdaySuccess = false;
      			vm.colorSuccess = false;
      			vm.workingOrderViewSuccess = false;
      			vm.searchHistoryAmountSuccess = false;
      			vm.contactPageSizeSuccess = false;
  			   }, 2000); 
      	}
		
		function setUpdateSuccessType(type) {
			switch(type) {
			case 'name':
				vm.nameSuccess = true;
				break;
			case 'country':
				vm.countrySuccess = true;
				break;
			case 'location':
				vm.locationSuccess = true;
				break;
			case 'street':
				vm.streetSuccess = true;
				break;
			case 'telephone':
				vm.telephoneSuccess = true;
				break;
			case 'anotherTelephone':
				vm.anotherTelephoneSuccess = true;
				break;
			case 'email': 
				vm.emailSuccess = true;
				break;
			case 'anotherEmail':
				vm.anotherEmailSuccess = true;
				break;
			case 'birthday':
				vm.birthdaySuccess = true;
				break;
			case 'color':
				vm.colorSuccess = true;
				break;
			case 'workingOrderView': 
				vm.workingOrderViewSuccess = true;
				break;
			case 'searchHistoryAmount':
				vm.searchHistoryAmountSuccess = true;
				break;
			case 'contactPageSize':
				vm.contactPageSizeSuccess = true;
				break;
			}
		}
		
		function uploadProfileImage(files, errFiles) {
        	vm.files = files;
        	vm.errFiles = errFiles;
            angular.forEach(files, function(file) {
                file.upload = vm.Upload.upload({
                    url: '/api/users/profile/upload/' + vm.currentUser.id,
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        file.result = response.data;
                        vm.currentUser.profileImagePath = response.data.profileImagePath;
                        $scope.$apply();
                    });
                }, function (response) {
                    if (response.status > 0) {
                        vm.errorMsg = response.status + ': ' + response.data;
                    }
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            });
    	}
		
		function showChangePasswordModalService() {
			changePasswordModalService.showChangePasswordModal();
		}
    }
})();