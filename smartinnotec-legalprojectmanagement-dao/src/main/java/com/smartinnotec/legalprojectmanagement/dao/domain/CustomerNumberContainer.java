package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.index.TextIndexed;

import lombok.Data;

public @Data class CustomerNumberContainer {

    @TextIndexed
    private String contactTenant;
    @TextIndexed
    private String customerNumber;

    public CustomerNumberContainer deepCopy() {
        final CustomerNumberContainer copiedCustomerNumberContainer = new CustomerNumberContainer();
        copiedCustomerNumberContainer.setContactTenant(this.getContactTenant());
        copiedCustomerNumberContainer.setCustomerNumber(this.getCustomerNumber());
        return copiedCustomerNumberContainer;
    }

    public String toString() {
        return "[contactTenant: " + contactTenant + ", customerNumber:" + customerNumber + "]";
    }
}
