(function() {
    'use strict';
    
    angular
		.module('legalprojectmanagement.project')
		.directive('documentFileIconDispatch', function () {
			return {
			      restrict: 'AE',
			      link: function (scope, element, attrs) {
			        element.bind('load', function () {
			          console.log('image loaded');
			        }).bind('error', function () {
			          element.attr('src', 'assets/img/default.PNG');
			        }); 
			      }
			};
	});	
})();