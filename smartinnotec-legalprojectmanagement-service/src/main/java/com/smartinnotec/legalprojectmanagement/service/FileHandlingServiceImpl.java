package com.smartinnotec.legalprojectmanagement.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;

@Service("fileHandlingService")
public class FileHandlingServiceImpl extends AbstractService implements FileHandlingService {

    public FileHandlingServiceImpl() {
    }

    @Override
    public String storeFile(final String dirName, final String filename, final MultipartFile multipartFile) throws BusinessException {
        BusinessAssert.notNull(dirName, "dirName is mandatory in FileHandlingServiceImpl#storeFile");
        BusinessAssert.notNull(filename, "folderName is mandatory in FileHandlingServiceImpl#storeFile");
        logger.info("store file at directory with name '{}' in file with name '{}' in FileHandlingServiceImpl#storeFile", dirName,
            filename);
        try {
            final File folder = new File(dirName);
            final File file = new File(dirName + File.separator + filename);
            if (!folder.exists()) {
                final boolean dirCreated = folder.mkdirs();
                logger.info("directory was created = '{}' in FileHandlingServiceImpl#storeFile", dirCreated);
            }
            final boolean newFileCreated = file.createNewFile();
            logger.info("new file created is '{}' in FileHandlingServiceImpl#storeFile", newFileCreated);

            final byte[] bytes = multipartFile.getBytes();
            final BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
            stream.write(bytes);
            stream.close();

            logger.debug("stored file at directory with name '{}' in file with name '{}' in FileHandlingServiceImpl#storeFile", dirName,
                filename);

            return file.getAbsolutePath();
        } catch (Exception e) {
            logger.error("Exception '{}' occured in FileHandlingServiceImpl#storeFile", e.getMessage());
            throw new BusinessException(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "[FileHandlingServiceImpl]";
    }
}
