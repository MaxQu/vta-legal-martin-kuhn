package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.repository.AddressChangedRepository;

@Service("addressChangedService")
public class AddressChangedServiceImpl extends AbstractService implements AddressChangedService {

    @Autowired
    private AddressChangedRepository addressChangedRepository;

    @Override
    public AddressChanged convertFromAddressToAddressChanged(final Address address) throws BusinessException {
        logger.info("convert address to addressChanged in AddressChangedServiceImpl#convertFromAddressToAddressChanged");

        if (address == null) {
            return null;
        }

        final AddressChanged addressChanged = new AddressChanged();
        addressChanged.setFlfId(address.getFlfId());
        addressChanged.setFlfIdChanged(address.isFlfIdChanged());
        addressChanged.setAdditionalInformation(address.getAdditionalInformation());
        addressChanged.setAdditionalInformationChanged(address.isAdditionalInformationChanged());

        final List<CustomerNumberContainer> customerNumberContainers = new ArrayList<>();
        if (addressChanged.getCustomerNumberContainers() != null) {
            for (final CustomerNumberContainer customerNumberContainer : addressChanged.getCustomerNumberContainers()) {
                final CustomerNumberContainer copiedCustomerNumberContainer = customerNumberContainer.deepCopy();
                customerNumberContainers.add(copiedCustomerNumberContainer);
            }
        }
        addressChanged.setCustomerNumberContainers(customerNumberContainers);
        addressChanged.setCustomerNumberContainersChanged(address.isCustomerNumberContainersChanged());
        addressChanged.setStreet(address.getStreet());
        addressChanged.setStreetChanged(address.isStreetChanged());
        addressChanged.setIntSign(address.getIntSign());
        addressChanged.setIntSignChanged(address.isIntSignChanged());
        addressChanged.setPostalCode(address.getPostalCode());
        addressChanged.setPostalCodeChanged(address.isPostalCodeChanged());
        addressChanged.setRegion(address.getRegion());
        addressChanged.setRegionChanged(address.isRegionChanged());
        addressChanged.setProvinceType(address.getProvinceType());
        addressChanged.setProvinceTypeChanged(address.isProvinceTypeChanged());
        addressChanged
            .setCountry(address.getCountry() != null ? CountryTypeEnum.valueOf(address.getCountry()) : CountryTypeEnum.EMPTY_COUNTRY);
        addressChanged.setCountryChanged(address.isCountryChanged());
        addressChanged.setTelephone(address.getTelephone());
        addressChanged.setTelephoneChanged(address.isTelephoneChanged());
        addressChanged.setEmail(address.getEmail());
        addressChanged.setEmailChanged(address.isEmailChanged());
        addressChanged.setInformation(address.getInformation());
        addressChanged.setInformationChanged(address.isInformationChanged());

        return addressChanged;
    }

    @Override
    public AddressChanged create(final AddressChanged addressChanged) throws BusinessException {
        BusinessAssert.notNull(addressChanged, "addressChanged is mandatory in AddressChangedServiceImpl#create", "400");
        logger.info("create addressChanged in AddressChangedServiceImpl#create");
        final AddressChanged createdAddressChanged = addressChangedRepository.insert(addressChanged);
        return createdAddressChanged;
    }

    @Override
    public AddressChanged update(final AddressChanged addressChanged) throws BusinessException {
        BusinessAssert.notNull(addressChanged, "addressChanged is mandatory in AddressChangedServiceImpl#update", "400");
        logger.info("update addressChanged with id '{}' in AddressChangedServiceImpl#create", addressChanged.getId());
        return addressChangedRepository.save(addressChanged);
    }

    @Override
    public void delete(final AddressChanged addressChanged) throws BusinessException {
        BusinessAssert.notNull(addressChanged, "addressChanged is mandatory in AddressChangedServiceImpl#delete", "400");
        logger.info("delete addressChanged with id '{}' in AddressChangedServiceImpl#delete", addressChanged.getId());
        addressChangedRepository.delete(addressChanged.getId());
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all addresses in AddressChangedServiceImpl#deleteAll");
        addressChangedRepository.deleteAll();
    }

    @Override
    public String toString() {
        return "[AddressChangedServiceImpl]";
    }
}
