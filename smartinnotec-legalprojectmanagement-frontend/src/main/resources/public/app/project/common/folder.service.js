(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('folderService', folderService);
        
    folderService.$inject = ['$http', 'api_config'];
    
    function folderService($http, api_config) {
		var service = {
			create: create,
			update: update,
			findFolders: findFolders,
			findAllFoldersOfProject: findAllFoldersOfProject,
			findBreadCrumbOfFolder: findBreadCrumbOfFolder,
			findFoldersByBySearchStringAndProjectIdAndFolderId: findFoldersByBySearchStringAndProjectIdAndFolderId,
			deleteFolder: deleteFolder
		};
		return service;
		
		////////////
		
		function create(folder, documentFileParentType) {
			return $http.post(api_config.BASE_URL + '/folders/folder/' + documentFileParentType, folder);
		}
		
		function update(folder, documentFileParentType) {
			return $http.put(api_config.BASE_URL + '/folders/folder/' + documentFileParentType, folder);
		}
		
		function findFolders(projectId, superFolderId) {
			return $http.get(api_config.BASE_URL + '/folders/folder/' + projectId, {
				params: {
					superfolderid: superFolderId
				} 
			});
		}
		
		function findBreadCrumbOfFolder(folderId) {
			return $http.get(api_config.BASE_URL + '/folders/folder/breadcrumb/', {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function findAllFoldersOfProject(projectId) {
			return $http.get(api_config.BASE_URL + '/folders/folder/project/' + projectId);
		}
		
		function findFoldersByBySearchStringAndProjectIdAndFolderId(documentFileAndFolderSearchString, projectId, folderId) {
			return $http.get(api_config.BASE_URL + '/folders/folder/search/project/' + projectId + '/' + documentFileAndFolderSearchString, {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function deleteFolder(folderId, documentFileParentType) {
			return $http.delete(api_config.BASE_URL + '/folders/folder/' + folderId + '/' + documentFileParentType);
		}
    }
})();