package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "HistoryAmount")
public @Data class HistoryAmount {

	@Id
	private String id;
	@JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
	private DateTime date;
	private Integer contacts;
	private Integer activities;
	private Integer facilityDetails;
	private Integer documents;
	private Integer products;
	private Integer projects;
	private Integer calendarEvents;
	private Integer receipes;
	private Integer mixtures;
	private Integer thinnings;
	private Integer communicationUserConnections;
}
