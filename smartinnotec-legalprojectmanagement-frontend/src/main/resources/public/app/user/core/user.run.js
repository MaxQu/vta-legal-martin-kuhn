(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.run(runBlock);
    
    runBlock.$inject = ['editableOptions', 'editableThemes'];
    
    function runBlock(editableOptions, editableThemes) {
    	editableThemes.bs3.inputClass = 'input-sm';
    	editableThemes.bs3.buttonsClass = 'btn-sm';
		editableOptions.theme = 'bs3';
    }
})();