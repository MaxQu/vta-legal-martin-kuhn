package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Title;
import com.smartinnotec.legalprojectmanagement.dao.repository.TitleRepository;

@Service("titleService")
public class TitleServiceImpl extends AbstractService implements TitleService {

    @Autowired
    private TitleRepository titleRepository;

    public TitleServiceImpl() {
    }

    @Override
    public List<Title> findAllTitles() throws BusinessException {
        logger.info("find all titles in TitleServiceImpl#findAllTitles");
        return titleRepository.findAll();
    }

    @Override
    public String toString() {
        return "[TitleServiceImpl]";
    }
}
