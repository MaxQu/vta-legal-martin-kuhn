package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum CountryTypeEnum {

    EMPTY_COUNTRY("EMPTY_COUNTRY"),
    AUSTRIA("AUSTRIA"),
    GERMANY("GERMANY"),
    SWITZERLAND("SWITZERLAND"),

    UKRAINE("UKRAINE"),
    MACEDONIA("MACEDONIA"),
    ESTLAND("ESTLAND"),
    TUNIS("TUNIS"),
    GREECE("GREECE"),
    USA("USA"),
    CANADA("CANADA"),
    PORTUGAL("PORTUGAL"),
    YUGOSLAWIA("YUGOSLAWIA"),
    KOREA("KOREA"),
    CHINA("CHINA"),
    LICHTENSTEIN("LICHTENSTEIN"),
    BELGIUM("BELGIUM"),
    BOSNIA_HERZEGOVINA("BOSNIA_HERZEGOVINA"),
    BULGARIA("BULGARIA"),
    DENMARK("DENMARK"),
    EGYPT("EGYPT"),
    FINNLAND("FINNLAND"),
    IRELAND("IRELAND"),
    FRANCE("FRANCE"),
    GREAT_BRITAN("GREAT_BRITAN"),
    ISRAEL("ISRAEL"),
    ITALY("ITALY"),
    CROATIA("CROATIA"),
    NETHERLANDS("NETHERLANDS"),
    NORWAY("NORWAY"),
    POLAND("POLAND"),
    QUATAR("QUATAR"),
    ROMANIA("ROMANIA"),
    RUSSIA("RUSSIA"),
    SWEDEN("SWEDEN"),
    SERBIA("SERBIA"),
    SLOVAKIA("SLOVAKIA"),
    SLOVENIA("SLOVENIA"),
    SPAIN("SPAIN"),
    CZECH_REPUBLIC("CZECH_REPUBLIC"),
    TURKEY("TURKEY"),
    HUNGARY("HUNGARY"),
    SOUTH_AFRICA("SOUTH_AFRICA");

    private String value;

    private CountryTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
