package com.smartinnotec.legalprojectmanagement.restservice.container;

import lombok.Data;

public @Data class ProjectLogoNameContainer {

    private String logoName;

}