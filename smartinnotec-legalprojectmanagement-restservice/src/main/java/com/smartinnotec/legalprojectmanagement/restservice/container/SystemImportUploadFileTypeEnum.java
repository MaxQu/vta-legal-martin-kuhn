package com.smartinnotec.legalprojectmanagement.restservice.container;

public enum SystemImportUploadFileTypeEnum {

    CONTACT_FILE("CONTACT_FILE"),
    ADDRESS_FILE("ADDRESS_FILE");

    private String value;

    private SystemImportUploadFileTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
