(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('changeProductPropertiesModalService', changeProductPropertiesModalService);
        
    changeProductPropertiesModalService.$inject = ['$modal', '$stateParams'];
    
    function changeProductPropertiesModalService($modal, $stateParams) {
		var service = {
			showChangeProductPropertiesModal: showChangeProductPropertiesModal
		};		
		return service;
		
		////////////
				
		function showChangeProductPropertiesModal(product, products, productTypes, currentUser, productAggregateConditionTypes, roles, usersOfProject) {
			 var changeProductPropertiesModal = $modal.open({
				controller: ChangeProductPropertiesModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	productTypes: function() {
			    		return productTypes;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	productAggregateConditionTypes: function() {
			    		return productAggregateConditionTypes;
			    	},
			    	roles: function() {
			    		return roles;
			    	},
			    	usersOfProject: function() {
			    		return usersOfProject;
			    	}
			    }, 
				templateUrl: 'app/products/modals/changeProductProperties/changeProductProperties.modal.html'
			});
			return changeProductPropertiesModal;
			
			function ChangeProductPropertiesModalController($modalInstance, $scope, product, products, productTypes, currentUser, productAggregateConditionTypes, roles, usersOfProject) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.products = products;
		    	vm.productTypes = productTypes;
		    	vm.currentUser = currentUser;
		    	vm.productAggregateConditionTypes = productAggregateConditionTypes;
		    	vm.roles = roles;
		    	vm.usersOfProject = usersOfProject;
		    	
		    	vm.closeChangeProductPropertiesModal = closeChangeProductPropertiesModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	function closeChangeProductPropertiesModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();