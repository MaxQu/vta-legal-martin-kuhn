package com.smartinnotec.legalprojectmanagement.security.xauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class XAuthTokenConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private UserDetailsService detailsService;
    @Autowired
    private XAuthTokenFilter xAuthTokenFilter;

    public XAuthTokenConfigurer() {
    }

    @Override
    public void configure(final HttpSecurity http) throws Exception {
        xAuthTokenFilter.setDetailsService(detailsService);
        http.addFilterBefore(xAuthTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }

    public UserDetailsService getDetailsService() {
        return detailsService;
    }

    public void setDetailsService(final UserDetailsService detailsService) {
        this.detailsService = detailsService;
    }

    @Override
    public String toString() {
        return "[XAuthTokenConfigurer]";
    }

}
