package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

public @Data class ProjectDataSheetChange {

    private String changeContent;
    private String chanceResponsibleAgency;
    private String changeNumber;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime changeDate;

}
