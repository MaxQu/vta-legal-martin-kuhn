(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.directive('createProjectDirective', createProjectDirective);
    
    createProjectDirective.$inject = ['$timeout', 'Upload', 'projectsService', 'projectUserConnectionService', 'dateUtilityService', 'calendarService', 'calendarEventUserConnectionService', 'projectsOfTenantModalService'];
    
	function createProjectDirective($timeout, Upload, projectsService, projectUserConnectionService, dateUtilityService, calendarService, calendarEventUserConnectionService, projectsOfTenantModalService) {
		var directive = {
			restrict: 'E',
			scope: {
				project: "=",
				projectUserConnections: "=",
				currentUser: "=",
				typeOfChange: "=",
				invoker: "=",
				provinceTypes: "=",
				projectDataSheet: "="
			},
			templateUrl: 'app/projects/directives/createProjectDirective/createProject.html',
			link: function($scope) {
				var createProjectClicked = false;
				$scope.datasheet = false;
				$scope.projectEndDateBeforeStartDateError = false;
				$scope.deletedProjectDataSheet = false;
				$scope.project.start = dateUtilityService.convertToDateOrUndefined($scope.project.start);
				$scope.project.end = dateUtilityService.convertToDateOrUndefined($scope.project.end);
				$scope.createCalendarEntry = false;
				setProjectDataSheetApprovalOrdersDate();
				setProjectDataSheetChangesDate();
				$scope.showAllProjectsOfTenant = showAllProjectsOfTenant;
				
				function showAllProjectsOfTenant() {
					projectsService.getAllProjectsOfTenant().then(function successCallback(response) {
						projectsOfTenantModalService.showProjectsOfTenantModal(response.data);
		    		}, function errorCallback(response) {
		    			console.log('error createProject.directive#showAllProjectsOfTenant');
			   		});
				}
				
				function setProjectDataSheetApprovalOrdersDate() {
					if($scope.projectDataSheet == null || $scope.projectDataSheet.projectDataSheetApprovalOrders == null) {
						return;
					}
					for(var i = 0; i < $scope.projectDataSheet.projectDataSheetApprovalOrders.length; i++) {
						$scope.projectDataSheet.projectDataSheetApprovalOrders[i].approvalOrderDate = dateUtilityService.convertToDateOrUndefined($scope.projectDataSheet.projectDataSheetApprovalOrders[i].approvalOrderDate);
					}
				}
				
				function setProjectDataSheetChangesDate() {
					if($scope.projectDataSheet == null || $scope.projectDataSheet.projectDataSheetChanges == null) {
						return;
					}
					for(var i = 0; i < $scope.projectDataSheet.projectDataSheetChanges.length; i++) {
						$scope.projectDataSheet.projectDataSheetChanges[i].changeDate = dateUtilityService.convertToDateOrUndefined($scope.projectDataSheet.projectDataSheetChanges[i].changeDate);
					}
				}
				
				$scope.openProjectStartDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.projectStartDatePicker = true;
				};
				
				$scope.openProjectEndDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.projectEndDatePicker = true;
				};
				
				$scope.openApprovalOrderDatePicker = function($event, projectDataSheetApprovalOrder) {
					$event.preventDefault();
					$event.stopPropagation();
					projectDataSheetApprovalOrder.approvalOrderDatePicker = true;
				};
				
				$scope.openChangeDatePicker = function($event, projectDataSheetChange) {
					$event.preventDefault();
					$event.stopPropagation();
					projectDataSheetChange.changeDatePicker = true;
				};
				
				$scope.removeProjectLogo = function() {
					$scope.project.logoName = 'standardLogo.png';
				};
				
				$scope.createOrChangeProject = function() {
					var projectEndBeforeProjectStart = dateUtilityService.isDateBeforeOrEqualOtherDate($scope.project.start, $scope.project.end);
					if(projectEndBeforeProjectStart && $scope.project.start instanceof Date && $scope.project.end instanceof Date) {
						$scope.projectEndDateBeforeStartDateError = true;
						return;
					}
					$scope.projectEndDateBeforeStartDateError = false;
					if($scope.typeOfChange == 'CREATE') {
						$scope.createProject(); 
					} else if($scope.typeOfChange == 'CHANGE') {
						$scope.changeProject();
					}
				};
				
				$scope.uploadProjectLogo = function(logoFiles, logoErrFiles) {
					var files = logoFiles;
		            angular.forEach(files, function(file) {
		                file.upload = Upload.upload({
		                    url: '/api/projects/project/projectlogo/upload/' + $scope.currentUser.id,
		                    data: {file: file}
		                });
		                file.upload.then(function (response) {
		                    $timeout(function () {
		                        file.result = response.data;
		                        $scope.project.logoName = response.data.logoName;
		                        $scope.$apply();
		                    });
		                }, function (response) {
		                    if (response.status > 0) {
		                    	$scope.errorMsg = response.status + ': ' + response.data;
		                    }
		                }, function (evt) {
		                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
		                });
		            });
				};
				
				$scope.deleteProjectDataSheet = function() {
					projectsService.deleteProjectDataSheet($scope.project.id).then(function successCallback(response) {
						$scope.initProjectDataSheet(false);
						$scope.deletedProjectDataSheet = true;
						$scope.setProjectDataSheetDeletedTimeout();
		    			console.log('datasheet deleted in createProject.directive.js');
		    		}, function errorCallback(response) {
		    			console.log('error createProject.directive#deleteProjectDataSheet');
			   		});
				};
				
				$scope.createProject = function() {
		    		if($scope.project.name === '') {
		    			return;
		    		}
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		$scope.project.creationUserId = $scope.currentUser.id;
		    		projectsService.createProject($scope.project, false).then(function successCallback(response) {
		    			$scope.project.name = '';
		    			$scope.project.description = '';
		    			$scope.project.start = '';
		    			$scope.project.logoName = '';
		    			$scope.project.end = '';
		    			//$scope.createCalendarEntry = false;
		    			$scope.creationUserId = '';
		    			
		    			if($scope.datasheet === true) {
							Date.prototype.toJSON = function () { return this.toLocaleString(); };
							$scope.projectDataSheet.projectId = response.data.id;
				    		projectsService.createProjectDataSheet($scope.projectDataSheet).then(function(response) {
				    			$scope.initProjectDataSheet(false);
				    			console.log('datasheet created in createProject.directive.js');
				    		}, function errorCallback(response) {
				    			console.log('error createProject.directive#createProjectDataSheet');
					   		});
		    			}
		    			
		    			var createdProject = response.data;
		    			var projectUserConnection = {};
		    			projectUserConnection.user = $scope.currentUser;
		    			projectUserConnection.project = createdProject;
		    			projectUserConnection.projectUserConnectionRole = 'PROJECT_MANAGER';
		    			projectUserConnectionService.createProjectUserConnection(projectUserConnection).then(function successCallback(response) {
		    				var projectUserConnection = response.data;
		    				projectUserConnection.stillCreated = true;
		    				$scope.projectUserConnections.splice(0, 0, projectUserConnection);
		    				
		    				if($scope.createCalendarEntry) {
		    					if(createdProject.start != null && typeof createdProject.start != 'undefined') {
		    						var startDateTime = dateUtilityService.convertToDateOrUndefined(createdProject.start);
		    						startDateTime.setHours(8,0,0,0);
		    						var endDateTime = dateUtilityService.convertToDateOrUndefined(createdProject.start);
		    						endDateTime.setHours(9,0,0,0);
		    						
		    						var calendarEventStart = {};
		    						calendarEventStart.title = createdProject.name;
		    						calendarEventStart.startsAt = startDateTime.getTime();
		    						calendarEventStart.endsAt = endDateTime.getTime();
		    						calendarEventStart.location = "Projektstart";
		    						calendarEventStart.calendarEventTyp = 'MEETING';
		    						calendarEventStart.project = createdProject;
		    						calendarEventStart.tenant = $scope.currentUser.tenant;
		    						calendarEventStart.serialDate = false;

			    					calendarService.create(calendarEventStart, false, 'NO_SERIAL_DATE', '01.01.2017').then(function (response) {
			    						var createdCalendarEvents = response.data;
			    						var calendarUserConnections = [];
			    						var calendarUserConnection = {};
			    						calendarUserConnection.user = projectUserConnection.user;
			    						calendarUserConnection.calendarEvent = createdCalendarEvents[0];
			    						calendarUserConnection.emailActive = true;
			    						calendarUserConnections.push(calendarUserConnection);
			    						
			    						calendarEventUserConnectionService.create(createdCalendarEvents[0].id, calendarUserConnections, 0).then(function (response) {
		    			    				console.log('created calendarEventUserConnection in createProject.directive.js');
		    			    			}).catch(function (data) {
		    								console.log('error createNewCalendarEvent.service#create#calendarEventUserConnection: ' + data);
		    							});
			    		    		}).catch(function (data) {
			    						console.log('error createNewCalendarEvent.service#createCalendarEvent: ' + data);
			    					});
		    					}
		    					if(createdProject.end != null && typeof createdProject.end != 'undefined') {
		    						var startDateTimeEnd = dateUtilityService.convertToDateOrUndefined(createdProject.end);
		    						startDateTimeEnd.setHours(8,0,0,0);
		    						var endDateTimeEnd = dateUtilityService.convertToDateOrUndefined(createdProject.end);
		    						endDateTimeEnd.setHours(9,0,0,0);
		    						
		    						var calendarEventEnd = {};
		    						calendarEventEnd.title = createdProject.name;
		    						calendarEventEnd.location = "Projektende";
		    						calendarEventEnd.startsAt = startDateTimeEnd.getTime();
		    						calendarEventEnd.endsAt = endDateTimeEnd.getTime();
		    						calendarEventEnd.calendarEventTyp = 'MEETING';
		    						calendarEventEnd.project = createdProject;
		    						calendarEventEnd.tenant = $scope.currentUser.tenant;
		    						calendarEventEnd.serialDate = false;

			    					calendarService.create(calendarEventEnd, false, 'NO_SERIAL_DATE', '01.01.2017').then(function (response) {
			    						var createdCalendarEvents = response.data;

			    						var calendarUserConnections = [];
			    						var calendarUserConnection = {};
			    						calendarUserConnection.user = projectUserConnection.user;
			    						calendarUserConnection.calendarEvent = createdCalendarEvents[0];
			    						calendarUserConnection.emailActive = true;
			    						calendarUserConnections.push(calendarUserConnection);
			    						
			    						calendarEventUserConnectionService.create(createdCalendarEvents[0].id, calendarUserConnections, 0).then(function (response) {
		    			    				console.log('created calendarEventUserConnection in createProject.directive.js');
		    			    			}).catch(function (data) {
		    								console.log('error createNewCalendarEvent.service#create#calendarEventUserConnection: ' + data);
		    							});
			    		    		}).catch(function (data) {
			    						console.log('error createNewCalendarEvent.service#createCalendarEvent: ' + data);
			    					});
		    					}
		    				}
			   	   		 }, function errorCallback(response) {
			   	   			  console.log('error createProject.directive#createProjectUserConnection');
			   	   		 });
		    			$scope.createProjectClicked = true;
		    			$scope.setProjectCreatedTimeout();
			   		 }, function errorCallback(response) {
			   			  console.log('error createProject.directive#createProject');
			   		 });
		    	};
		    	
		    	$scope.changeProject = function() {
					Date.prototype.toJSON = function () { return this.toLocaleString(); };
					projectsService.updateProject($scope.project).then(function successCallback(response) {
						if($scope.datasheet === true && $scope.projectDataSheet !== '') {
							Date.prototype.toJSON = function () { return this.toLocaleString(); };
							$scope.projectDataSheet.projectId = response.data.id;
				    		projectsService.updateProjectDataSheet($scope.projectDataSheet).then(function successCallback(response) {
				    			$scope.invoker.closeChangeProjectPropertiesModal();
				    			console.log('datasheet updated in createProject.directive.js');
				    		}, function errorCallback(response) {
				    			console.log('error createProject.directive#updateProjectDataSheet');
					   		});
		    			} else {
		    				$scope.invoker.closeChangeProjectPropertiesModal();
		    			}
					}, function errorCallback(response) {
			   			  console.log('error createProject.directive#changeProject');
			   		});
				};
				
				$scope.initProjectDataSheet = function(createNewObject) {
					if(createNewObject === true) {
						$scope.projectDataSheet = {};
					}
					if($scope.projectDataSheet === null || $scope.projectDataSheet === '') {
						return;
					}
		    		$scope.projectDataSheet.operatorName = '';
		    		$scope.projectDataSheet.operatorStreet = '';
		    		$scope.projectDataSheet.operatorNumber = 0;
		    		$scope.projectDataSheet.operatorZip = 0;
		    		$scope.projectDataSheet.operatorLocation = '';
		    		$scope.projectDataSheet.operatorProvinceType = null;

		    		$scope.projectDataSheet.locationStreet = '';
		    		$scope.projectDataSheet.locationNumber = 0;
		    		$scope.projectDataSheet.locationZip = 0;
		    		$scope.projectDataSheet.locationLocation = '';
		    		$scope.projectDataSheet.locationProvinceType = null;
		    		$scope.projectDataSheet.locationRegion = '';
		    		$scope.projectDataSheet.locationEstateNumber = '';
		    		$scope.projectDataSheet.locationArchiveNumber = '';
		    		$scope.projectDataSheet.locationCastralCommunity = '';
		    		
		    		$scope.projectDataSheet.identificationFacilities = [{
		    			identificationFacilityName: '',
		    			identificationFacilityPower: '',
		    			identificationFacilityType: ''
		    		}];
		    		
		    		$scope.projectDataSheet.projectDataSheetApprovalOrders = [{
				    	approvalOrderAgency: '',
					    approvalOrderNumber: '',
					    approvalOrderDate: null
				    }];

		    		$scope.projectDataSheet.projectDataSheetAuthorities = [{
		    			authorityName: '',
		    			authorityStreet: '',
		    			authorityNumber: 0,
		    			authorityZip: 0,
		    			authorityLocation: '',
		    			authorityProvinceType: null,
		    			authorityApprovalPerson: ''
		    		}];
		    		
		    		$scope.projectDataSheet.projectDataSheetChanges = [{ 
				    	changeContent: '',
					    chanceResponsibleAgency: '',
					    changeNumber: '',
					    changeDate: null
				    }];
		    	};
		    	
		    	$scope.addProjectDataSheetApprovalOrder = function() {
					var projectDataSheetApprovalOrder = {
						approvalOrderAgency: '',
					    approvalOrderNumber: '',
					    approvalOrderDate: null
						};
					if($scope.projectDataSheet == null || $scope.projectDataSheet === '') {
						$scope.initProjectDataSheet(true); 
					}
					if($scope.projectDataSheet.projectDataSheetApprovalOrders == null) {
						$scope.projectDataSheet.projectDataSheetApprovalOrders = [];
					}
					$scope.projectDataSheet.projectDataSheetApprovalOrders.push(projectDataSheetApprovalOrder);
				};
				
				$scope.addIdentificationFacility = function() {
					var projectDataSheetIdentificationFacility = {
				    	identificationFacilityName: '',
					    identificationFacilityPower: '',
					    identificationFacilityType: ''
				    };
					if($scope.projectDataSheet == null || $scope.projectDataSheet === '') {
						$scope.initProjectDataSheet(true); 
					}
					if($scope.projectDataSheet.projectDataSheetIdentificationFacilities == null) {
						$scope.projectDataSheet.projectDataSheetIdentificationFacilities = [];
					}
					$scope.projectDataSheet.projectDataSheetIdentificationFacilities.push(projectDataSheetIdentificationFacility);
				};
				
				$scope.addProjectDataSheetChange = function() {
					var projectDataSheetChange = {
						changeContent: '',
						chanceResponsibleAgency: '',
						changeNumber: '',
						changeDate: null
						};
					if($scope.projectDataSheet == null || $scope.projectDataSheet === '') {
						$scope.initProjectDataSheet(true);
					}
					if($scope.projectDataSheet.projectDataSheetChanges == null) {
						$scope.projectDataSheet.projectDataSheetChanges = [];
					}
					$scope.projectDataSheet.projectDataSheetChanges.push(projectDataSheetChange);
				};
				
				$scope.addProjectDataSheetAuthority = function() {
					var projectDataSheetAuthority = {
						authorityName: '',
				    	authorityStreet: '',
				    	authorityNumber: 0,
				    	authorityZip: 1234,
				    	authorityLocation: '',
				    	authorityProvinceType: '', 
				    	authorityApprovalPerson: ''
						};
					if($scope.projectDataSheet == null || $scope.projectDataSheet === '') {
						$scope.initProjectDataSheet(true);
					}
					if($scope.projectDataSheet.projectDataSheetAuthorities == null) {
						$scope.projectDataSheet.projectDataSheetAuthorities = [];
					}
					$scope.projectDataSheet.projectDataSheetAuthorities.push(projectDataSheetAuthority);
				};
				
				$scope.removeProjectDataSheetApprovalOrder = function(index) {
					if($scope.projectDataSheet.projectDataSheetApprovalOrders.length > 1) {
						$scope.projectDataSheet.projectDataSheetApprovalOrders.splice(index, 1);
					}
				};
				
				$scope.removeProjectDataSheetChange = function(index) {
					if($scope.projectDataSheet.projectDataSheetChanges.length > 1) {
						$scope.projectDataSheet.projectDataSheetChanges.splice(index, 1);
					}
				};
				
				$scope.removeIdentificationFacility = function(index) {
					if($scope.projectDataSheet.projectDataSheetIdentificationFacilities.length > 1) {
						$scope.projectDataSheet.projectDataSheetIdentificationFacilities.splice(index, 1);
					}
				};
				
				$scope.removeProjectDataSheetAuthority = function(index) {
					if($scope.projectDataSheet.projectDataSheetAuthorities.length > 1) {
						$scope.projectDataSheet.projectDataSheetAuthorities.splice(index, 1);
					}
				};
				
				$scope.changeProjectDataSheet = function() {
				};
				
				$scope.setProjectCreatedTimeout = function() {
		      		$timeout(function() {
		      			$scope.createProjectClicked = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.setProjectDataSheetDeletedTimeout = function() {
		      		$timeout(function() {
		      			$scope.deletedProjectDataSheet = false;
		  			   }, 2000); 
		      	};
			 }
		};
		return directive;
		
		////////////
	}
})();
