package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileVersion;

import lombok.Data;

public @Data class CommunicationSelectedDocumentFile {

    private DocumentFile selectedDocumentFile;
    private DocumentFileVersion documentFileVersion;
}
