package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;

public class WorkingBookDAOTest extends AbstractDAOTest {

    @Test
    public void shouldGetLastWorkingBookEntry() {

        final Project project = new Project();
        project.setName("TestProject");
        final Project createdProject = projectRepository.insert(project);
        Assert.assertNotNull(createdProject);

        final Tenant tenant = new Tenant();
        tenant.setName("TestTenant1");
        final Tenant createdTenant = tenantRepository.insert(tenant);
        Assert.assertNotNull(createdTenant);

        final WorkingBook wb1 = new WorkingBook();
        wb1.setWorkingText("wb- Eintrag 1");
        wb1.setProject(createdProject);
        wb1.setTenant(createdTenant);
        wb1.setCreationDateTime(dateTimeFormatter.parseDateTime("01.03.2017 11:12"));
        wb1.setCategoryType(WorkingBookCategoryTypeEnum.TAEGLICH);
        final WorkingBook createdWb1 = workingBookRepository.insert(wb1);
        Assert.assertNotNull(createdWb1);

        final WorkingBook wb2 = new WorkingBook();
        wb2.setWorkingText("wb- Eintrag 2");
        wb2.setProject(createdProject);
        wb2.setTenant(createdTenant);
        wb2.setCreationDateTime(dateTimeFormatter.parseDateTime("07.03.2017 11:12"));
        wb2.setCategoryType(WorkingBookCategoryTypeEnum.TAEGLICH);
        final WorkingBook createdWb2 = workingBookRepository.insert(wb2);
        Assert.assertNotNull(createdWb2);

        final WorkingBook wb3 = new WorkingBook();
        wb3.setWorkingText("wb- Eintrag 1");
        wb3.setProject(createdProject);
        wb3.setTenant(createdTenant);
        wb3.setCreationDateTime(dateTimeFormatter.parseDateTime("04.03.2017 11:12"));
        wb3.setCategoryType(WorkingBookCategoryTypeEnum.TAEGLICH);
        final WorkingBook createdWb3 = workingBookRepository.insert(wb3);
        Assert.assertNotNull(createdWb3);

        final WorkingBook foundedWorkingBook = workingBookRepository.findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc(
            createdProject, createdTenant, WorkingBookCategoryTypeEnum.TAEGLICH);
        Assert.assertNotNull(foundedWorkingBook);
        Assert.assertEquals(createdWb2.getId(), foundedWorkingBook.getId());
        Assert.assertEquals("wb- Eintrag 2", foundedWorkingBook.getWorkingText());
    }
}
