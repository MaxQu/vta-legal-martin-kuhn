// Karma configuration
// Generated on Fri Jul 24 2015 23:27:33 GMT+0200 (Mitteleuropäische Sommerzeit)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
	files: [
		'src/main/resources/public/assets/bower_components/angular/angular.min.js',
        'src/main/resources/public/assets/bower_components/angular-i18n/angular-locale_de-at.js',
		'src/main/resources/public/assets/bower_components/angular-mocks/angular-mocks.js',
		'src/main/resources/public/assets/bower_components/angular-ui-router/release/angular-ui-router.js',
		'src/main/resources/public/assets/bower_components/angular-bootstrap-show-errors/src/showErrors.js',
		'src/main/resources/public/assets/bower_components/angular-http-auth/src/http-auth-interceptor.js',
		'src/main/resources/public/assets/bower_components/angular-translate/angular-translate.js',
		'src/main/resources/public/assets/bower_components/jquery/dist/jquery.min.js',
		'src/main/resources/public/assets/bower_components/**/*.min.js',
		'src/main/resources/public/constants/*.js',
		'src/main/resources/public/app/**/*.module.js',
		'src/main/resources/public/app/**/*.service.js',
		'src/main/resources/public/app/**/*.directive.js',
		'src/main/resources/public/app/**/*.controller.js',
		'src/main/resources/public/app/**/*.spec.js',
		'src/main/resources/public/app/**/*.js',
		'src/main/resources/public/app/**/*.html'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'src/main/resources/public/app/**/*.html': 'ng-html2js'
    },

	ngHtml2JsPreprocessor: {
		moduleName: 'templates',
		stripPrefix: 'src/main/resources/public/app/',
		prependPrefix: 'app/'
	},


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    //browsers: ['Chrome', 'Firefox', 'PhantomJS'],
    browsers: ['PhantomJS'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    },

    plugins : ['karma-jasmine', 'karma-phantomjs-launcher', 'karma-ng-html2js-preprocessor']
  });
};
