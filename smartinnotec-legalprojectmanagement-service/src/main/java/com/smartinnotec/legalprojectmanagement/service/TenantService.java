package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface TenantService {

    Tenant create(final Tenant tenant) throws BusinessException;

    List<Tenant> findAll() throws BusinessException;

    Tenant findTenantByName(final String tenantName) throws BusinessException;

    Tenant findTenantById(final String id) throws BusinessException;

    void delete(final String id) throws BusinessException;

    void deleteAll() throws BusinessException;

}
