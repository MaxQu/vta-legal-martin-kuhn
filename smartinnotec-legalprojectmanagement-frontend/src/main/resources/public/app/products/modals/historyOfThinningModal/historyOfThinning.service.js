(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('historyOfThinningModalService', historyOfThinningModalService);
        
    historyOfThinningModalService.$inject = ['$modal', '$stateParams', 'productsService', 'documentFileService', 'projectTemplateService', 'receipeService', 'dateUtilityService', 'printingPDFService', 'productOverviewModalService', 'thinningService', 'printThinningModalService'];
    
    function historyOfThinningModalService($modal, $stateParams, productsService, documentFileService, projectTemplateService, receipeService, dateUtilityService, printingPDFService, productOverviewModalService, thinningService, printThinningModalService) {
		var service = {
			showHistoryOfThinningModal: showHistoryOfThinningModal
		};		
		return service;
		
		////////////
				
		function showHistoryOfThinningModal(currentUser, product, thinningsOfProduct, start, end, polymerPromille) { 
			 var historyOfThinningModal = $modal.open({
				controller: HistoryOfThinnigModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	thinningsOfProduct: function() {
			    		return thinningsOfProduct;
			    	},
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	}, 
			    	polymerPromille: function() {
			    		return polymerPromille;
			    	}
			    }, 
				templateUrl: 'app/products/modals/historyOfThinningModal/historyOfThinning.modal.html'
			});
			return historyOfThinningModal;
			
			function HistoryOfThinnigModalController($modalInstance, $scope, currentUser, product, thinningsOfProduct, start, end, polymerPromille) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.thinningsOfProduct = thinningsOfProduct;
		    	vm.thinningDateFrom = start;
		    	vm.thinningDateUntil = end;
		    	vm.searchThinningTerm = null;
		    	vm.searchThinningDateFromDatePicker = false;
		    	vm.searchThinningDateUntilDatePicker = false;
		    	vm.polymerPromille = polymerPromille;
		    	
		    	vm.printContainer = printContainer;
		    	vm.openSearchThinningFromDatePicker = openSearchThinningFromDatePicker;
		    	vm.openSearchThinningUntilDatePicker = openSearchThinningUntilDatePicker;
		    	vm.findThinningsOfProductOfTypeInLastMonth = findThinningsOfProductOfTypeInLastMonth;
		    	vm.showPrintThinningModal = showPrintThinningModal;
		    	vm.closeHistoryOfThinningModal = closeHistoryOfThinningModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    
		    	$scope.$watch('vm.thinningDateFrom', function (newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.thinningDateFrom, vm.thinningDateUntil);
			    		if(vm.thinningDateFrom != null && vm.thinningDateUntil != null && checkDateRange == false) {
			    			searchThinningsInTimeRange();
			    		}
		    		}
		        });
		    	
		    	$scope.$watch('vm.thinningDateUntil', function (newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.thinningDateFrom, vm.thinningDateUntil);
			    		if(vm.thinningDateFrom != null && vm.thinningDateUntil != null && checkDateRange == false) {
			    			searchThinningsInTimeRange();
			    		}
		    		}
		        });
		    	
		    	function searchThinningsInTimeRange() {
					var start = dateUtilityService.formatDateToString(vm.thinningDateFrom);
					var end = dateUtilityService.formatDateToString(vm.thinningDateUntil);
					thinningService.findThinningsOfProductInRange(vm.product.id, start, end).then(function(response) {
						vm.thinningsOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfThinning.service#searchThinningsInTimeRange');
		 	   		 });
				}
		    	
		    	function printContainer(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	}
		    	
		    	function findThinningsOfProductOfTypeInLastMonth() {
					vm.thinningDateFrom = dateUtilityService.oneMonthBack(new Date());
			    	vm.thinningDateUntil = new Date();
			    	var startDateString = dateUtilityService.formatDateToString(vm.thinningDateFrom);
		    		var endDateString = dateUtilityService.formatDateToString(vm.thinningDateUntil);
			    	vm.searchThinningTerm = null;
			    	thinningService.findThinningsOfProductInRange(vm.product.id, startDateString, endDateString).then(function(response) {
						vm.thinningsOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfThinning.controller#findThinningsOfProductOfTypeInLastMonth');
		 	   		 }); 
				}
		    	
		    	function openSearchThinningFromDatePicker($event) { 
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchThinningDateFromDatePicker = true; 
				}
		    	
		    	function openSearchThinningUntilDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchThinningDateUntilDatePicker = true; 
				}
		    	
		    	function showPrintThinningModal(thinningOfProduct) {
		    		printThinningModalService.showPrintThinningModal(vm.product, thinningOfProduct, vm.currentUser, vm.polymerPromille);
		    	}
		    	
		    	function closeHistoryOfThinningModal() {
					$modalInstance.dismiss('cancel');  
				}
			} 
		}
    }
})();