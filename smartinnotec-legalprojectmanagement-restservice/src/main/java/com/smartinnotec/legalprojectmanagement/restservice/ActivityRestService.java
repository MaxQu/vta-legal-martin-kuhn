package com.smartinnotec.legalprojectmanagement.restservice;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;

@CrossOrigin(origins = {
    "http://portal.lehr.co.at:14200", "http://localhost:4200", "http://vta.smartinnotec.com:4200", "http://vta.smartinnotec.com:4211",
    "http://192.168.186.136:4200", "http://vta-adm.intranet.vta-europe.com:4200", "https://adm.vta.cc",
    "http://89.144.209.71:4200" }, allowedHeaders = { "Content-Type", "x-auth-token" })
@RestController
public class ActivityRestService extends AbstractRestService {

	@RequestMapping(value = "/activities/activity/{contactid}/amount", method = { RequestMethod.GET })
    public Integer getActivitiesAmountOfContact(final @PathVariable("contactid") String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId is mandatory in ActivityRestService#getActivitiesAmountOfContact", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ActivityRestService#getActivitiesAmountOfContact", "400");
        logger.info("get amount of activities of contact with id '{}' in ActivityRestService#getActivitiesAmountOfContact", contactId);
        return 4;
    }
	
	@RequestMapping(value = "/activities/activity/{contactid}/last", method = { RequestMethod.GET })
    public String getLastActivityOfContact(final @PathVariable("contactid") String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId is mandatory in ActivityRestService#getLastActivityOfContact", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ActivityRestService#getLastActivityOfContact", "400");
        logger.info("get last activities of contact with id '{}' in ActivityRestService#getLastActivityOfContact", contactId);
        return "12.08.2020";
    }
    
}