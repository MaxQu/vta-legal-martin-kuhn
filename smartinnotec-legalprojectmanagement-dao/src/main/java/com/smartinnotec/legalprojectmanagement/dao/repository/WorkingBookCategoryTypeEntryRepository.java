package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEntry;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;

public interface WorkingBookCategoryTypeEntryRepository extends MongoRepository<WorkingBookCategoryTypeEntry, String> {

    List<WorkingBookCategoryTypeEntry> findByProjectIdAndWorkingBookCategoryTypeAndTenant(final String projectId,
            final WorkingBookCategoryTypeEnum workingBookCategoryTypeEnum, final Tenant tenant);

}
