package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "DocumentFile")
public @Data class DocumentFile {

    @Id
    private String id;
    private String subFilePath;
    @TextIndexed
    private String fileName;
    @TextIndexed
    private String ending;
    private String projectProductId;
    private String folderId;
    @TextIndexed
    private List<DocumentFileVersion> documentFileVersions;
    private Integer contactReferenceCounter;
    private Integer receipeReferenceCounter;
    private boolean active;
    private List<String> responsibleUsers;
    private List<String> userIdWhiteList; // if userId added user can only see this documentFile
    private List<String> userIdBlackList; // if userId added user cannot see this documentFile
    private List<ProjectUserConnectionRoleEnum> rolesBlackList;
    private boolean confident;
    private String information;

    public DocumentFile() {
        active = true;
        contactReferenceCounter = 0;
        receipeReferenceCounter = 0;
        userIdBlackList = new ArrayList<>();
        userIdBlackList = new ArrayList<>();
    }

    public void addDocumentFileVersion(final DocumentFileVersion documentFileVersion) {
        if (this.documentFileVersions == null) {
            this.documentFileVersions = new ArrayList<>();
        }
        this.documentFileVersions.add(documentFileVersion);
    }

    public void addUserIdBlackList(final String userId) {
        if (this.userIdBlackList == null) {
            this.userIdBlackList = new ArrayList<>();
        }
        this.userIdBlackList.add(userId);
    }

    public void addUserIdWhiteList(final String userId) {
        if (this.userIdWhiteList == null) {
            this.userIdWhiteList = new ArrayList<>();
        }
        this.userIdWhiteList.add(userId);
    }
}
