package com.smartinnotec.legalprojectmanagement.service;

import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;

@Service("utilService")
public class UtilServiceImpl extends AbstractService implements UtilService {

    @Override
    public String escapeString(String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in UtilServiceImpl#escapeString");
        logger.info("escape '{}' in UtilServiceImpl#escapeString", searchString);
        searchString = searchString.replace("+", "\\+");
        searchString = searchString.replace("-", "\\-");
        return searchString;
    }

    @Override
    public boolean hasStringPropertyChanged(String property1, String property2) throws BusinessException {
        if (property1 != null) {
            property1 = property1.trim();
        }
        if (property2 != null) {
            property2 = property2.trim();
        }
        if (property1 == null && property2 == null) {
            return false;
        } else if ((property2 != null && property1 == null && property2.isEmpty())
            || (property1 != null && property1.isEmpty() && property2 == null)) {
            return false;
        } else if ((property1 == null && property2 != null) || (property1 != null && property2 == null) || (!property1.equals(property2))) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasStringListChanged(final List<String> list1, final List<String> list2) throws BusinessException {
        if (list1 != null && !list1.isEmpty()) {
            for (final Iterator<String> iterator = list1.iterator(); iterator.hasNext();) {
                final String item = iterator.next();
                if (item.isEmpty()) {
                    iterator.remove();
                }
            }
        }
        if (list2 != null && !list2.isEmpty()) {
            for (final Iterator<String> iterator = list2.iterator(); iterator.hasNext();) {
                final String item = iterator.next();
                if (item.isEmpty()) {
                    iterator.remove();
                }
            }
        }

        if ((list1 == null || list1.isEmpty()) && (list2 == null || list2.isEmpty())) {
            return false;
        }
        if ((list1 == null && list2 != null && !list2.isEmpty()) || (list2 == null && list1 != null && !list1.isEmpty())) {
            return true;
        }
        if (list1 != null && list2 != null && list1.size() != list2.size()) {
            return true;
        }
        return !list1.equals(list2);
    }

    @Override
    public boolean hasCustomerNumberPropertyChanged(final List<CustomerNumberContainer> customerNumberContainers,
            final List<CustomerNumberContainer> mergedContactImportedCustomerNumberContainers)
            throws BusinessException {
        if (customerNumberContainers == null && mergedContactImportedCustomerNumberContainers == null) {
            return false;
        }
        if ((customerNumberContainers == null && mergedContactImportedCustomerNumberContainers != null) || (customerNumberContainers != null
            && !customerNumberContainers.isEmpty() && mergedContactImportedCustomerNumberContainers == null)) {
            return true;
        }
        if ((customerNumberContainers == null && mergedContactImportedCustomerNumberContainers != null
            && mergedContactImportedCustomerNumberContainers.isEmpty())
            || (mergedContactImportedCustomerNumberContainers == null && customerNumberContainers != null
                && customerNumberContainers.isEmpty())) {
            return false;
        }
        if (customerNumberContainers.isEmpty() && mergedContactImportedCustomerNumberContainers.isEmpty()) {
            return false;
        }
        if (customerNumberContainers.size() != mergedContactImportedCustomerNumberContainers.size()) {
            return true;
        }
        for (final CustomerNumberContainer mergedContactImportedCustomerNumberContainer : mergedContactImportedCustomerNumberContainers) {
            boolean equal = false;
            for (final CustomerNumberContainer customerNumberContainer : customerNumberContainers) {
                if (customerNumberContainer.getCustomerNumber().equals(mergedContactImportedCustomerNumberContainer.getCustomerNumber())
                    && customerNumberContainer.getContactTenant().equals(mergedContactImportedCustomerNumberContainer.getContactTenant())) {
                    equal = true;
                }
            }
            if (!equal) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasAgentNumberPropertyChanged(final List<AgentNumberContainer> agentNumberContainers,
            final List<AgentNumberContainer> mergedContactImportedAgentNumberContainers)
            throws BusinessException {
        if (agentNumberContainers == null && mergedContactImportedAgentNumberContainers == null) {
            return false;
        }
        if ((agentNumberContainers == null && mergedContactImportedAgentNumberContainers != null)
            || (agentNumberContainers != null && !agentNumberContainers.isEmpty() && mergedContactImportedAgentNumberContainers == null)) {
            return true;
        }
        if ((agentNumberContainers == null && mergedContactImportedAgentNumberContainers != null
            && mergedContactImportedAgentNumberContainers.isEmpty())
            || (mergedContactImportedAgentNumberContainers == null && agentNumberContainers != null && agentNumberContainers.isEmpty())) {
            return false;
        }
        if (agentNumberContainers.isEmpty() && mergedContactImportedAgentNumberContainers.isEmpty()) {
            return false;
        }
        if ((agentNumberContainers == null || agentNumberContainers.isEmpty())
            && (mergedContactImportedAgentNumberContainers != null && !mergedContactImportedAgentNumberContainers.isEmpty())) {
            return true;
        }
        if (agentNumberContainers != null && agentNumberContainers.size() > 0 && mergedContactImportedAgentNumberContainers != null
            && mergedContactImportedAgentNumberContainers.size() > 0) {
            if (agentNumberContainers.get(0).getCustomerNumber()
                .equals(mergedContactImportedAgentNumberContainers.get(0).getCustomerNumber())) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean overlapTimeRange(final DateTime date1, final DateTime date2, final DateTime start, final DateTime end)
            throws BusinessException {
        if (date2.isBefore(date1) || end.isBefore(start)) {
            return false;
        }
        final Interval interval1 = new Interval(start, end);
        if (date2.equals(start)) {
            return false;
        }
        if (interval1.contains(date1)) {
            return true;
        }
        if (interval1.contains(date2)) {
            return true;
        }
        if (date1.equals(end)) {
            return false;
        }
        final Interval interval2 = new Interval(date1, date2);
        if (interval2.contains(start)) {
            return true;
        }
        if (interval2.contains(end)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "[UtilServiceImpl]";
    }
}
