package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum LoginHistoryTypeEnum {

    CURRENT_LOGIN("CURRENT_LOGIN"),
    LAST_LOGIN("LAST_LOGIN");

    private String value;

    private LoginHistoryTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
