package com.smartinnotec.legalprojectmanagement.security.xauth;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

// currently only for testing -> not needed productive for now

@Aspect
@Configuration
public class ErrorAspectRestService {

    public ErrorAspectRestService() {
    }

    // if http://localhost:8080/api/healthcheck is invoked, this method is also invoked before

    public void logServiceAccess() {
        System.out.println("logServiceAccess Before: ");
    }
}
