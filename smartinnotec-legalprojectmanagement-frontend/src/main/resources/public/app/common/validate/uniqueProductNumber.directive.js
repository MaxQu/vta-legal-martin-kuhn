(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('uniqueProductNumber', uniqueProductNumber); 
    
    uniqueProductNumber.$inject = ['validateService'];
    
	function uniqueProductNumber(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////
		
		function link(scope, element, attrs, ngModel) {
			if(attrs.uniqueProductNumber == 'CREATE') {
				ngModel.$parsers.unshift(parser);
				ngModel.$formatters.unshift(formatter);
			}
			
			////////////
			
			function parser(value) { 
				if(value.length > 1) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProductNumber(value, attrs.uniqueProductNumber).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 1) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProductNumber(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();