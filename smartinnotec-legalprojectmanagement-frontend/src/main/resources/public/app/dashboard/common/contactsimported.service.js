(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactsImportedService', contactsImportedService);
        
    contactsImportedService.$inject = ['$http', 'api_config'];
    
    function contactsImportedService($http, api_config) {
		var service = {
			getAmountOfContactsImported: getAmountOfContactsImported,
			deleteContactImported: deleteContactImported,
			deleteAllContactsImported: deleteAllContactsImported
		};
		
		return service;
		
		////////////
		
		function getAmountOfContactsImported() {
			return $http.get(api_config.BASE_URL + '/contactsimported/contactimported/amount');
		}
		
		function deleteContactImported(id) {
			return $http.delete(api_config.BASE_URL + '/contactsimported/contactimported/' + id + '/delete');
		}
		
		function deleteAllContactsImported() {
			return $http.delete(api_config.BASE_URL + '/contactsimported/contactimported/delete/all');
		}
    }
})();
