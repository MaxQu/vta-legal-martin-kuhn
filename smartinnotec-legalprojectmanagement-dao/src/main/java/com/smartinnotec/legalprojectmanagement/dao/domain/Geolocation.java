package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Geolocation {
  private double lat;
  private double lng;
}
