package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.web.multipart.MultipartFile;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.ActivityDocumentFile;

public interface ActivityService {

    Integer count() throws BusinessException;

    Activity create(final Activity activity) throws BusinessException;

    Activity update(final Activity activity) throws BusinessException;

    Activity findById(final String id) throws BusinessException;

    List<Activity> getActivitiesByCalendarEventId(final String calendarEventId) throws BusinessException;

    Activity getLastActivityOfCalendarEventId(final String calendarEventId) throws BusinessException;

    List<Activity> getByContactId(final String contactId) throws BusinessException;

    List<Activity> getByContactIdInRange(final String calendarEventId, final DateTime start, final DateTime end) throws BusinessException;

    List<Activity> findByCalendarEventStartDateBetween(final DateTime start, final DateTime end) throws BusinessException;

    List<Activity> findActivitiesBySearchString(final String searchString) throws BusinessException;

    List<Activity> findByCalendarEventIdAndCalendarEventStartDateBetween(final String calendarEventId, final DateTime start,
            final DateTime end)
            throws BusinessException;

    List<Activity> findByUserIdAndCalendarEventStartDateBetween(final String userId, final DateTime start, final DateTime end)
            throws BusinessException;

    List<Activity> findByUserIdAndContactIdAndCalendarEventStartDateBetween(final String userId, final String contactId,
            final DateTime start, final DateTime end)
            throws BusinessException;

    void delete(final String id) throws BusinessException;

    ActivityDocumentFile uploadActivityFile(final MultipartFile multipartFile) throws BusinessException;

    void downloadActivityFile(final HttpServletResponse response, final String filename, final String ending) throws BusinessException;

}
