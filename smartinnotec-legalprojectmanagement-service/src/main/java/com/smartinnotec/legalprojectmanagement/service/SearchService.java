package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.container.SearchResult;

public interface SearchService {

    List<SearchResult> searchInProjects(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchDocumentFilesInProducts(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInDocumentFiles(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInProjectFolders(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInProductFolders(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInProducts(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInCalendar(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInContacts(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInWorkingBook(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInCommunication(final String searchString, final User user) throws BusinessException;

    List<SearchResult> searchInTimeRegistration(final String searchString, final User user) throws BusinessException;

    String prepareSearchTerm(final String searchTerm);

    boolean combineSearchStringWithAnd(final String searchString);

}
