(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('roleOfAssignedUserIsBlackListedModalService', roleOfAssignedUserIsBlackListedModalService);
        
    roleOfAssignedUserIsBlackListedModalService.$inject = ['$modal', '$stateParams'];
    
    function roleOfAssignedUserIsBlackListedModalService($modal, $stateParams) {
		var service = {
			showRoleOfAssignedUserIsBlackListedModal: showRoleOfAssignedUserIsBlackListedModal
		};		
		return service;
		
		////////////
				
		function showRoleOfAssignedUserIsBlackListedModal(documentFile, user) {
			 var roleOfAssignedUserIsBlackListedModal = $modal.open({
				controller: RoleOfAssignedUserIsBlackListedModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	documentFile: function() {
			    		return documentFile;
			    	},
			    	user: function() {
			    		return user;
			    	}
			    }, 
				templateUrl: 'app/project/modals/roleOfAssignedUserIsBlackListedModal/roleOfAssignedUserIsBlackListed.modal.html'
			});
			return roleOfAssignedUserIsBlackListedModal;
			
			function RoleOfAssignedUserIsBlackListedModalController($modalInstance, $scope, documentFile, user) {
		    	var vm = this; 
		    	vm.documentFile = documentFile;
		    	vm.user = user;
		    	
		    	vm.closeRoleOfAssignedUserIsBlackListedModal = closeRoleOfAssignedUserIsBlackListedModal;
		    	
		    	////////////
		    	
		    	function closeRoleOfAssignedUserIsBlackListedModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();