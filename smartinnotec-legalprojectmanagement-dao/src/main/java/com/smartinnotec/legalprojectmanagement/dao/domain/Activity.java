package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "Activity")
public @Data class Activity implements Comparable<Activity> {

    @Id
    private String id;
    private String calendarEventId;
    @Transient
    private CalendarEvent calendarEvent;
    private String contactId;
    @Transient
    private Contact contact;
    private String userId;
    @Transient
    private User user;
    private List<ActivityTypeEnum> activityTypes;
    @TextIndexed
    private String additionalInformation;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime creationDate;
    @DBRef
    private Tenant tenant;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime calendarEventStartDate; // need for search in range
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime calendarEventEndDate; // need for search in range
    @TextIndexed
    private List<ActivityDocumentFile> activityDocumentFiles;

    @Override
    public int compareTo(final Activity activity) {
        return this.getCalendarEventStartDate().compareTo(activity.getCalendarEventStartDate());
    }
}
