package com.smartinnotec.legalprojectmanagement.core.system.exception;

public interface ErrorCodeAwareAble {

    String getErrorCode();
}
