package com.smartinnotec.legalprojectmanagement.service;

public enum ApplicationCategorieEnum {

    PROJECTS("PROJECTS"),
    PRODUCTS("PRODUCTS"),
    DOCUMENTS("DOCUMENTS"),
    FOLDERS("FOLDERS"),
    PRODUCT_FOLDERS("PRODUCT_FOLDERS"),
    CALENDAR("CALENDAR"),
    CONTACTS("CONTACTS"),
    WORKINGBOOK("WORKINGBOOK"),
    COMMUNICATION("COMMUNICATION"),
    TIME_REGISTRATION("TIME_REGISTRATION");

    private String value;

    private ApplicationCategorieEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
