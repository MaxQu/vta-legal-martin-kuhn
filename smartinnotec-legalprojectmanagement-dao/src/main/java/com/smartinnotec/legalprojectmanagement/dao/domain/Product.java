package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "Product")
public @Data class Product {

    @Id
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private String id;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private Boolean inactive;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private DateTime inactiveDate;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private String inactiveMessage;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private String number; // Artikelnummer
    @TextIndexed(weight = 2)
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private String name; // Artikelbezeichnung
    @TextIndexed(weight = 3)
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private List<String> names; // Artikelbezeichnungen
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private String description;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private String chemicalDescription; // chemische Beschreibung
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private ProductAggregateConditionTypeEnum productAggregateConditionType;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private Float density; // Dichte des Produkts
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private List<ProductFillingWeight> productFillingWeights; // Fuellgewicht diverser Gebinde
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private String color;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime developmentDate;
    @TextIndexed
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private List<ProductTypeEnum> types; // Art
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private List<ContactProduct> contactsProduct; // Hersteller, Lieferanten
    @JsonView(RestServiceResponseView.ProductPublic.class)
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime creationDate;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private Long amountOfReceipes;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private Long amountOfMixtures;
    @JsonView(RestServiceResponseView.ProductPublic.class)
    private Long amountOfThinnings;
    private String creationUserId;
    @DBRef
    private Tenant tenant;
    @TextScore
    private Float score;
    @Transient
    private List<DocumentFile> documentFiles; // for searching products over documentFiles

    public Product() {
        this.inactive = false;
    }

    public void addDocumentFile(final DocumentFile documentFile) {
        if (documentFiles == null) {
            documentFiles = new ArrayList<>();
        }
        documentFiles.add(documentFile);
    }
}
