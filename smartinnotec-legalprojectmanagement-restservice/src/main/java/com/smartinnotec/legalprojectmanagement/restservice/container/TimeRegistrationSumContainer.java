package com.smartinnotec.legalprojectmanagement.restservice.container;

import lombok.Data;

public @Data class TimeRegistrationSumContainer {

    private String timeRegistrationTimeRangeSum;

}
