(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.timeregistration')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getTimeregistrationState());
    	 
    	////////////
			    	
    	function getTimeregistrationState() {
    		var state = {
    			name: 'auth.timeregistration',
				url: '/timeregistration/:userId',
				templateUrl: 'app/timeregistration/timeregistration/timeregistration.html',
				controller: 'TimeregistrationController',
				controllerAs: 'vm',
				resolve: {
					projectsService: 'projectsService',
					projects: function findProjectsOfUser($stateParams, projectsService) {
						return projectsService.findProjectsOfUser($stateParams.userId);
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					}
				}
    		};
    		return state;
    	}
	}
})();