package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;

public interface AddressService {

    Address create(final Address address) throws BusinessException;

    Address update(final Address address) throws BusinessException;

    Address findById(final String id) throws BusinessException;

    List<Address> findAll() throws BusinessException;

    List<Address> findAddressesBySearchString(final String searchString) throws BusinessException;

    void delete(final Address address) throws BusinessException;

    void deleteAll() throws BusinessException;

    void performGeocodingWhenRequired();

}
