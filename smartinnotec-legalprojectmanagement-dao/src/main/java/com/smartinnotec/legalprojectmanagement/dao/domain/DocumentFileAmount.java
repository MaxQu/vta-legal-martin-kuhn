package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class DocumentFileAmount {

    private Long amountOfDocumentFiles;

}
