(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.factory('printTimeRegistrationModalService', printTimeRegistrationModalService);
        
    printTimeRegistrationModalService.$inject = ['$modal', '$stateParams'];
    
    function printTimeRegistrationModalService($modal, $stateParams) {
		var service = {
			showPrintTimeRegistrationModal: showPrintTimeRegistrationModal
		};		
		return service;
		
		////////////
				
		function showPrintTimeRegistrationModal(timeRegistrations) {
			 var printTimeRegistrationModal = $modal.open({
				controller: PrintTimeRegistrationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	timeRegistrations: function() {
			    		return timeRegistrations;
			    	}
			    }, 
				templateUrl: 'app/timeregistration/modals/printTimeRegistration/printTimeRegistration.modal.html'
			});
			return printTimeRegistrationModal;
			
			function PrintTimeRegistrationModalController($modalInstance, $scope, timeRegistrations) {
		    	var vm = this; 
		    	vm.timeRegistrations = timeRegistrations;
		    	vm.today = new Date();
		    	
		    	vm.closePrintTimeRegistrationModal = closePrintTimeRegistrationModal;
		    	
		    	////////////
		    	
		    	function closePrintTimeRegistrationModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();