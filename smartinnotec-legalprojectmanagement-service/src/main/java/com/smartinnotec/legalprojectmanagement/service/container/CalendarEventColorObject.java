package com.smartinnotec.legalprojectmanagement.service.container;

import lombok.Data;

public @Data class CalendarEventColorObject {

    private String primary;
    private String secondary;

}
