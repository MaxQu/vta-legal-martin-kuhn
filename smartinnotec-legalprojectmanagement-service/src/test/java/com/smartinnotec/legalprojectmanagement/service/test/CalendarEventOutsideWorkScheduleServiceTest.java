package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventSerialDateTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;
import com.smartinnotec.legalprojectmanagement.service.container.CalendarEventOutsideWorkContainer;

public class CalendarEventOutsideWorkScheduleServiceTest extends AbstractServiceTest {

    @Test
    public void shouldSearchAllStringsInSearchString() throws BusinessException {
        final User user = new User();
        user.setFirstname("TestFirstname1");
        user.setSurname("TestSurname1");
        final User createdUser = userService.createUser(user);
        Assert.assertNotNull(createdUser);

        final CalendarEvent calendarEvent1 = new CalendarEvent();
        calendarEvent1.setTitle("calendarEvent1");
        calendarEvent1.setLocation("calendarEventLocation1");
        calendarEvent1.setStartsAt(formatter.parseDateTime("02.05.2018 10:00").getMillis());
        calendarEvent1.setEndsAt(formatter.parseDateTime("02.05.2018 12:00").getMillis());
        final List<CalendarEvent> createdCalendarEvents1 = calendarEventService.create(calendarEvent1, createdUser, false, null,
            CalendarEventSerialDateTypeEnum.NO_SERIAL_DATE);
        Assert.assertNotNull(createdCalendarEvents1);

        final CalendarEventUserConnection calendarEventUserConnection1 = new CalendarEventUserConnection();
        calendarEventUserConnection1.setCalendarEvent(calendarEvent1);
        calendarEventUserConnection1.setUser(createdUser);
    }
}
