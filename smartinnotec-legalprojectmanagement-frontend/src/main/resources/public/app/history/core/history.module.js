(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.history', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();