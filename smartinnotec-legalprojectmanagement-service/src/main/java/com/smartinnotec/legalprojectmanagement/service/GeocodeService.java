package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.dao.domain.*;

public interface GeocodeService {
  void geocodeAddress(Address address);

  Address reverseGeocode(Geolocation geolocation);
}
