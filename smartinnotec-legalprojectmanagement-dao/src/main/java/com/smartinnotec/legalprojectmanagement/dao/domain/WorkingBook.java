package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "WorkingBook")
public @Data class WorkingBook {

    @Id
    private String id;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime dateTimeFrom;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime dateTimeUntil;
    @TextIndexed
    private WorkingBookCategoryTypeEnum categoryType;
    @TextIndexed
    private List<WorkingBookCategoryTypeEntry> workingBookCategoryTypeEntries;
    @DBRef
    private User user;
    @DBRef
    private Project project;
    @DBRef
    private Tenant tenant;
    @TextIndexed
    private String workingText;
    private List<WorkingBookAttachment> attachments;
    @TextIndexed
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime creationDateTime;

}
