(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.contacts')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getContactsState());
    	 
    	////////////
			    	
    	function getContactsState() {
    		var state = {
    			name: 'auth.contacts',
				url: '/contacts/:userId',
				templateUrl: 'app/contacts/contacts/contacts.html',
				controller: 'ContactsController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					optionsService: 'optionsService',
					contactsService: 'contactsService',
					countryTypes: function getCountryTypes(optionsService) {
						return optionsService.getCountryTypes();
					},
					titles: function getAllTitles(optionsService) {
						return optionsService.getAllTitles();
					},
					provinceTypes: function getProvinceTypes(optionsService) {
						return optionsService.getProvinceTypes();
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					contactTypes: function($stateParams, optionsService) {
						return optionsService.getContactTypes();
					},
					contactPageSize: function getContactPageSize(contactsService) {
						return contactsService.getContactPageSize();
					},
					amountOfPages: function calculateAmountOfPages(contactsService) {
						return contactsService.calculateAmountOfPages(); 
					},
					amountOfContacts: function($stateParams, contactsService) {
						return contactsService.countByTenantAndActive($stateParams.userId);
					},
					usersOfProject: function findProjectUserConnectionByProject(userService) {
						return userService.findAllUsers();
					},
					roles: function(optionsService) {
						return optionsService.getRoles();
					}
				}
    		};
    		return state;
    	}
	}
})();