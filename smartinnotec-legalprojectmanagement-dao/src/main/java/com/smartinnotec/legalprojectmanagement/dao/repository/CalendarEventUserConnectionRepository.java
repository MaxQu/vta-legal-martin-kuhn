package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface CalendarEventUserConnectionRepository extends MongoRepository<CalendarEventUserConnection, String> {

    List<CalendarEventUserConnection> findByCalendarEventAndArchived(final CalendarEvent calendarEvent, final boolean archived);

    List<CalendarEventUserConnection> findByUser(final User user);

    List<CalendarEventUserConnection> findByUserAndArchived(final User user, final boolean archived);

    List<CalendarEventUserConnection> findByContactAndArchived(final Contact contact, final boolean archived);

    CalendarEventUserConnection findByUserAndCalendarEventAndArchived(final User user, final CalendarEvent calendarEvent,
            final boolean archived);

}
