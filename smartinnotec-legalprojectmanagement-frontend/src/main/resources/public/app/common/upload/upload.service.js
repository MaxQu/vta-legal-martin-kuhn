(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('uploadService', uploadService);
    
	uploadService.$inject = ['$http', '$q', 'api_config', 'Upload', '$timeout'];
	
	function uploadService($http, $q, api_config, Upload, $timeout) {
		var service = {	
			uploadFiles: uploadFiles,
			uploadGeneralFiles: uploadGeneralFiles,
			uploadNewDocumentVersion: uploadNewDocumentVersion,
			uploadSystemImportFile: uploadSystemImportFile 
		};
		
		return service;
		
		////////////
		
		function uploadFiles(files, userId, projectId, superFolderId, documentFileParentType, successCallback, errorCallback, progressCallback) {
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	              var file = files[i];
	              if (!file.$error) {
	                Upload.upload({
	                    url: 'api/fileuploads/fileupload/' + userId + '/' + projectId + '/' + documentFileParentType,
	                    data: {
	                      superfolderid: superFolderId,
	                      file: file  
	                    }
	                }).then(function (resp) {
	                	if (successCallback) {
	                        successCallback(resp);
	                    }
	                }, function (resp) {
	                    if(errorCallback) {
	                    	errorCallback(resp);
	                    }
	                }, function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    var log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n';
	                    if (progressCallback) {
	                        progressCallback(log);
	                    }
	                });
	              }
	            }
	        }
	    }
		
		function uploadGeneralFiles(type, files, successCallback, progressCallback) {
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	              var file = files[i];
	              if (!file.$error) {
	                Upload.upload({
	                    url: 'api/fileuploads/fileupload/' + type,
	                    data: {
	                      file: file  
	                    }
	                }).then(function (resp) {
	                	if (successCallback) {
	                        successCallback(resp);
	                    } 
	                }, null, function (evt) { 
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    var log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n';
	                    if (progressCallback) {
	                        progressCallback(log);
	                    }
	                });
	              }
	            }
	        }
	    }
		
		function uploadNewDocumentVersion(file, userId, projectId, documentFileId, successCallback, progressCallback) {
			if (!file.$error) {
                Upload.upload({
                    url: 'api/fileuploads/fileupload/documentversion/' + userId + '/' + projectId + '/' + documentFileId,
                    data: {
                      file: file  
                    }
                }).then(function (resp) {
                	if (successCallback) {
                        successCallback(resp);
                    }
                }, null, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    var log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n';
                    if (progressCallback) {
                        progressCallback(log);
                    }
                });
             }
		}	
		
		function uploadSystemImportFile(files, userId, systemImportUploadFileType, successCallback, errorCallback, progressCallback) {
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	              var file = files[i];
	              if (!file.$error) {
	                Upload.upload({
	                    url: 'api/fileuploads/fileupload/' + userId + '/systemimport/' + systemImportUploadFileType,
	                    data: {
	                      file: file  
	                    }
	                }).then(function (resp) {
	                	if (successCallback) {
	                        successCallback(resp);
	                    }
	                }, function (resp) {
	                    if(errorCallback) {
	                    	errorCallback(resp);
	                    }
	                }, function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    var log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n';
	                    if (progressCallback) {
	                        progressCallback(log);
	                    }
	                });
	              }
	            }
	        }
	    }
	}
})();
