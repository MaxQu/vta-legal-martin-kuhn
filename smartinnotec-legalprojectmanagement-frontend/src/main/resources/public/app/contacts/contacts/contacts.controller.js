(function() {
    'use strict';
       
    angular
    	.module('legalprojectmanagement.contacts')
    	.controller('ContactsController', ContactsController);
     
    ContactsController.$inject = ['$scope', 'currentUser', 'countryTypes', 'titles', 'provinceTypes', 'contactTypes', 'amountOfContacts', 'contactPageSize', 'amountOfPages', 'userAuthorizationUserContainer', 'roles', 'usersOfProject', 'projectsOfContactModalService', 'contactsService', 'projectUserConnectionService', 'projectsAssignedToContactModalService', 'editContactModalService', 'yesNoContactModalService', 'projectsService', 'documentFileService', 'projectTemplateService', 'printingPDFService', 'productsService', 'facilityDetailsOfContactModalService', 'facilityDetailsService', 'contactActivityModalService', 'activityService', 'dateUtilityService', 'calendarEventUserConnectionService', 'userService', 'usersAssignedToContactModalService', 'optionsService', 'deleteImportedContactsModalService', 'yesNoDeactivateContactModalService'];
     
    function ContactsController($scope, currentUser, countryTypes, titles, provinceTypes, contactTypes, amountOfContacts, contactPageSize, amountOfPages, userAuthorizationUserContainer, roles, usersOfProject, projectsOfContactModalService, contactsService, projectUserConnectionService, projectsAssignedToContactModalService, editContactModalService, yesNoContactModalService, projectsService, documentFileService, projectTemplateService, printingPDFService, productsService, facilityDetailsOfContactModalService, facilityDetailsService, contactActivityModalService, activityService, dateUtilityService, calendarEventUserConnectionService, userService, usersAssignedToContactModalService, optionsService, deleteImportedContactsModalService, yesNoDeactivateContactModalService) {
	    $scope.vm = this;   
    	var vm = this;
    	
    	vm.currentUser = currentUser; 
    	vm.countryTypes = countryTypes.data; 
    	vm.titles = titles.data;
    	vm.amountOfContacts = amountOfContacts.data;
    	vm.provinceTypes = provinceTypes.data;
    	vm.contactTypes = contactTypes.data;
    	vm.contactPageSize = contactPageSize.data;
    	vm.amountOfPages = amountOfPages.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.roles = roles.data;
    	vm.usersOfProject = usersOfProject.data;
    	vm.showContactForm = false; 
    	vm.orderType = 'institution';
    	vm.currentPage = 0;
    	vm.searchContactsTerm = null;
    	vm.searchTrigger = false;
    	vm.loadingContacts = false;
    	vm.searchingContacts = false;
    	vm.importContactsFromExcel = false;
    	vm.selectedTenantForExcelImport = 'EMPTY_COUNTRY';
    	vm.contacts = [];
    	vm.activityTypes = [];
    	vm.facilityDetailsTypes = [];
    	vm.files = [];
    	vm.selectedCountryImport = vm.countryTypes[0];
    	
    	vm.printContainer = printContainer;
    	vm.createContact = createContact;
    	vm.updateContact = updateContact;
    	vm.deleteContact = deleteContact;
    	vm.deactivateContact = deactivateContact;
    	vm.pageChange = pageChange;
    	vm.arrayRang = arrayRang;
    	vm.isCurrentPage = isCurrentPage;
    	vm.findContactBySearchString = findContactBySearchString;
    	vm.removeSearchResults = removeSearchResults;
    	vm.showProductsOfContact = showProductsOfContact;
    	vm.showUsersAssignedToContactModal = showUsersAssignedToContactModal;
    	vm.showProjectsAssignedToContactModal = showProjectsAssignedToContactModal;
    	vm.showEditContactModal = showEditContactModal;
    	vm.showDeleteContactModal = showDeleteContactModal;
    	vm.showContactActivityModal = showContactActivityModal;
    	vm.showFacilityDetailsModal = showFacilityDetailsModal;
    	vm.createDocumentUrl = createDocumentUrl;
    	vm.hideAddressWithProducts = hideAddressWithProducts;
    	vm.searchContacts = searchContacts;
    	vm.showTemplateModal = showTemplateModal;
    	vm.showAccountAddress = showAccountAddress;
    	vm.successCallback = successCallback;
    	vm.errorCallback = errorCallback;
    	vm.progressCallback = progressCallback;
    	vm.showDeleteAllImportedContacts = showDeleteAllImportedContacts;
    	vm.showDeactivateContactModal = showDeactivateContactModal;
    	vm.contactTypesCheck = contactTypesCheck;
    	vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    	createContactVariable();
    	pageChange(0);
    	loadActivityTypes();
    	loadFacilityDetailsTypes();
    	
    	////////////
    	
    	$scope.$watch('vm.orderType', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			pageChange(0);
    		} 
		});
    	
    	$scope.$watch('vm.files', function () {
    		if(vm.files != null && vm.files.length > 0) {
    			contactsService.importContacts(vm.files[0], vm.selectedCountryImport, vm.successCallback, vm.errorCallback, vm.progressCallback);
    		}
        });
    	
    	function successCallback(response) {
    		pageChange(0);
    		vm.importContactsFromExcel = false;
    	}
    	
    	function errorCallback(response) {
    		console.log('error importing excel in contacts.controller#errorCallback: ' + response.data);
    	}

    	function progressCallback(response) {
    		vm.importContactsFromExcel = true;
    	}
    	
    	function showDeleteAllImportedContacts() {
    		contactsService.getContactImportationDateGroup().then(function(response) {
    			var contactImportationDateGroups = response.data;
    			deleteImportedContactsModalService.showDeleteImportedContactsModal(vm, contactImportationDateGroups);
			}).catch(function(data) {
				console.log('error in contacts.controller.js#showDeleteAllImportedContacts: ' + data);
			});
    	}
    	
    	function showAccountAddress(accountAddress) {
    		if(accountAddress == null) {
    			return false;
    		}
    		if((accountAddress.street == null || accountAddress.street.length == 0) && 
    		   (accountAddress.postalCode == null || accountAddress.postalCode.length == 0) &&
    		   (accountAddress.region == null || accountAddress.region.length == 0) &&
    		   (accountAddress.provinceType == null || accountAddress.provinceType.length == 0) &&
    		   (accountAddress.country == null || accountAddress.country.length == 0) &&
    		   (accountAddress.email == null || accountAddress.email.length == 0)) {
    			return false;
    		}
    		return true;
    	}
    	
    	function searchContacts() {
			if(vm.searchContactsTerm != null && vm.searchContactsTerm.length > 3) {
				vm.searchingContacts = true;
				findContactBySearchString(vm.searchContactsTerm);
				vm.searchTrigger = true;
				vm.searchingContacts = false;
			}
		}
    	
    	function hideAddressWithProducts(addressWithProducts) {
			var address = addressWithProducts.address;
			if(address == null) {
				return false;
			}
			if(address.userIdBlackList != null && address.userIdBlackList.indexOf(vm.currentUser.id) !== -1) {
				return true;
			}
			if(address.rolesBlackList != null && address.rolesBlackList.indexOf(vm.currentUser.projectUserConnectionRole) !== -1) {
				return true;
			}
			return false;
		}
    	
    	function removeSearchResults() {
    		vm.searchContactsTerm = null;
    		vm.searchTrigger = false;
    		pageChange(0);
    	}
    	
    	function loadAmountOfContacts() {
    		contactsService.countByTenantAndActive(vm.currentUser.id).then(function(response) {
    			vm.amountOfContacts = response.data;
			}).catch(function (data) {
				console.log('error in contacts.controller.js#loadAmountOfContacts: ' + data);
			});
    	}
    	
    	function createContactVariable() {
    		vm.contact = {
    			institution: '',
    			contactTenant: 'EMPTY_COUNTRY',
    			contactPerson: '',
    			customerNumberContainers: [],
    			deliveryNumberContainers: [],
    			contactTypes: [],
    			address: {
					street: '',
					postalCode: '',
					region: '',
					provinceType: 'EMPTY_PROVINCE',
					country: 'EMPTY_COUNTRY'
					},
				accountAddress: null,
				addressesWithProducts: [],
				emails: [''],
				telephones: [''],
				homepage: '',
				products: [],
				contactPersons: [],
				contactAttachments: []
	    		};
    	}
    	
    	function findContactBySearchString(searchString) {
    		vm.loadingContacts = true;
    		contactsService.findContactBySearchString(searchString).then(function (response) {
    			vm.contacts = response.data;
    			vm.loadingContacts = false;
			}).catch(function (data) {
				console.log('error in contacts.controller.js#findContactBySearchString: ' + data);
			});
    	}
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}

    	function createContact(contact) {
    		vm.createContactClicked = true;
    		contactsService.createContact(contact).then(function (response) {
    			var createdContact = response.data;
    			createdContact.stillCreated = true;
    			createContactVariable();
    			loadAmountOfContacts();
    			vm.contacts.splice(0, 0, createdContact);
			}).catch(function (data) {
				console.log('error in contacts.controller.js#createContact: ' + data);
			});
    	}
    	
    	function updateContact(contact) {
    		contact.importConfirmed = true;
    		contactsService.updateContact(contact).then(function(response) {
    			updateContactFromView(response.data);
			}).catch(function (data) {
				console.log('error in contacts.controller.js#updateContact: ' + data);
			});
    	}
    	
    	function deleteContact(contact) {
    		contactsService.deleteContact(contact.id).then(function (response) {
    			deleteContactFromView(contact);
    			loadAmountOfContacts();
			}).catch(function (data) {
				console.log('error in contacts.controller.js#deleteContact: ' + data);
			});
    	}
    	
    	function updateContactFromView(contact) {
    		for(var i = 0; i < vm.contacts.length; i++) {
    			if(vm.contacts[i].id == contact.id) {
    				vm.contacts[i] = contact;
    				break;
    			}
    		}
    	}
    	
    	function deleteContactFromView(contact) {
    		for(var i = 0; i < vm.contacts.length; i++) {
    			if(vm.contacts[i].id == contact.id) {
    				vm.contacts.splice(i, 1);
    				break;
    			}
    		}
    	}
    	
    	function showEditContactModal(contact) {
    		editContactModalService.showEditContactModal(contact, vm.titles, vm.provinceTypes, vm.contactTypes, vm.countryTypes, vm, vm.currentUser, vm.roles, vm.usersOfProject);
    	}
    	
    	function showDeleteContactModal(contact) {
    		productsService.getProductsReferencedToContact(contact.id).then(function(response) {
    			var productsReferencedToContact = response.data;
    			calendarEventUserConnectionService.findCalendarEventUserConnectionsByContact(contact.id).then(function(response) {
    				var calendarEventUserConnections = response.data;
    				yesNoContactModalService.showYesNoModal(vm, contact, productsReferencedToContact, calendarEventUserConnections);
    			 }, function errorCallback(response) {
    	   			  console.log('error contacts.controller.js#showDeleteContactModal#findCalendarEventUserConnectionsByContact');
     	   		 });
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#showDeleteContactModal#showDeleteContactModal');
  	   		 });
    	}
    	
    	function showDeactivateContactModal(contact, state) {
    		yesNoDeactivateContactModalService.showYesNoModal(vm, contact, state);
    	}
    	
    	function deactivateContact(contact, state) {
    		contactsService.deactivateContact(contact.id, state).then(function (response) {
    			if(state == false) {
    				deleteContactFromView(contact);
    			} else {
    				contact.active = true;
    			}
    			loadAmountOfContacts();
			}).catch(function (data) {
				console.log('error in contacts.controller.js#deactivateContact: ' + data);
			});
    	}
    	
    	function showProductsOfContact(contact) {
    		contactsService.findContactById(contact.id).then(function(response) {
    			var contactWithProducts = response.data;
    			projectsOfContactModalService.showProductsOfContactModal(contactWithProducts, vm.currentUser, vm.userAuthorizationUserContainer);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#showProductsOfContact');
  	   		 });
    	}
    	
    	function showProjectsAssignedToContactModal(contact) {
    		projectsService.findProjectsOfUser(currentUser.id).then(function (projectsResponse) {
    			var projects = projectsResponse.data;
    			projectUserConnectionService.findProjectUserConnectionsByContactAndProject(contact.id).then(function (projectUserConnectionResponse) {
        			var projectUserConnections = projectUserConnectionResponse.data;
        			projectsAssignedToContactModalService.showProjectsAssignedToContactModal(contact, projects, projectUserConnections);
    			}).catch(function (data) {
    				console.log('error in contacts.controller.js#findProjectUserConnectionsByContact: ' + data);
    			});
			}).catch(function (data) {
				console.log('error in contacts.controller.js#showProjectsAssignedToContactModal: ' + data);
			});
    	}
    	
    	function showUsersAssignedToContactModal(contact) {
    		userService.findAllUsers().then(function(projectsResponse) {
    			var users = projectsResponse.data;
    			projectUserConnectionService.findProjectUserConnectionsByContactAndUser(contact.id).then(function (projectUserConnectionResponse) {
        			var projectUserConnections = projectUserConnectionResponse.data;
        			usersAssignedToContactModalService.showUsersAssignedToContactModal(contact, users, projectUserConnections);
    			}).catch(function (data) {
    				console.log('error in contacts.controller.js#findProjectsOfUser: ' + data);
    			});
			}).catch(function (data) {
				console.log('error in contacts.controller.js#showProjectsAssignedToContactModal: ' + data);
			});
    	}
    	
    	function showContactActivityModal(contact) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		activityService.getActivitiesOfContactIdInRange(contact.id, startDateString, endDateString).then(function(response) {
    			var activities = response.data;
    			contactActivityModalService.showContactActivityModal(contact, activities, start, end, vm.activityTypes, vm.userAuthorizationUserContainer);
    		}, function errorCallback(response) {
	   			  console.log('error contacts.controller.js#showContactActivityModal');
	   		 });
    	}
    	
    	function showFacilityDetailsModal(contact) {  
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		facilityDetailsService.getFacilityDetailsOfContactIdInRange(contact.id, startDateString, endDateString).then(function(response) {
    			var facilityDetails = response.data;
    			facilityDetailsOfContactModalService.showFacilityDetailsOfContactModal(contact, facilityDetails, start, end, vm.activityTypes, vm.facilityDetailsTypes, vm.userAuthorizationUserContainer);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#showFacilityDetailsModal');
  	   		 });
    	}
    	
    	function pageChange(page) {
    		contactsService.calculateAmountOfPages().then(function(response) {
    			vm.amountOfPages = response.data;
    			vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    			if(page < 0 || (page > 0 && page > vm.amountOfPagesArray.length-1 )) {
        			return;
        		}
        		vm.loadingContacts = true;
        		contactsService.findPagedContacts(page, vm.orderType).then(function(response) {
        			vm.contacts = response.data;
        			vm.currentPage = page;
        			vm.loadingContacts = false;
        			loadAmountOfContacts();
      	   		 }, function errorCallback(response) {
      	   			  console.log('error contacts.controller.js#pageChange');
      	   		 });
			}).catch(function (data) {
				console.log('error in contacts.controller.js#pageChange: ' + data);
			});
    	}
    	
    	function isCurrentPage(indexHistory) {
    		if(indexHistory == (vm.currentPage+1)) {
    			return true;
    		}
    		return false; 
    	}
    	
    	function arrayRang(min, max, step) {
    	    step = step || 1;
    	    var input = [];
    	    for (var i = min; i <= max; i += step) {
    	        input.push(i);
    	    }
    	    return input;
    	}
    	
    	function createDocumentUrl(user, product, receipeAttachment) {	
    		if(receipeAttachment == null) {
    			return;
    		}
    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
    		return 'api/filedownloads/filedownload/' + currentUser.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
    	}
    	
    	function showTemplateModal(url, receipeAttachment) {
    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
    			var documentFile = response.data;
    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
  	   		 }, function errorCallback(response) {
  	   			 console.log('error contacts.controller#showTemplateModal'); 
  	   		 });
    	}
    	
    	function loadActivityTypes() {     
    		optionsService.getActivityTypes('all').then(function(response) {
    			vm.activityTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#loadActivityTypes');
  	   		 });
    	}
    	
    	function loadFacilityDetailsTypes() {
    		optionsService.getFacilityDetailsTypes().then(function(response) {
    			vm.facilityDetailsTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#loadFacilityDetailsTypes');
  	   		 });
    	}
    	
    	function contactTypesCheck(contact) {
    		if(contact.contactTypes == null || contact.contactTypes.length == 0) {
    			return true;
    		}
    		for(var i = 0; i < contact.contactTypes.length; i++) {
    			if(contact.contactTypes[i] == 'POTENTIAL_BUYER') {
    				return false;
    			}
    		}
    		return true;
    	}
	} 
})();