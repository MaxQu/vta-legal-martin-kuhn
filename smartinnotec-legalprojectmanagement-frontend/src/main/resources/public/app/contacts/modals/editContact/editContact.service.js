(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('editContactModalService', editContactModalService);
        
    editContactModalService.$inject = ['$modal', '$stateParams'];
    
    function editContactModalService($modal, $stateParams) {
		var service = {
			showEditContactModal: showEditContactModal
		};		
		return service;
		
		////////////
				
		function showEditContactModal(contact, titles, provinceTypes, contactTypes, countryTypes, invoker, currentUser, roles, usersOfProject) {
			 var editContactModal = $modal.open({
				controller: EditContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	contact: function() {
			    		return contact;
			    	},
			    	titles: function() {
			    		return titles;
			    	},
			    	provinceTypes: function() {
			    		return provinceTypes;
			    	},
			    	contactTypes: function() {
			    		return contactTypes;
			    	},
			    	countryTypes: function() {
			    		return countryTypes;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	roles: function() {
			    		return roles;
			    	},
			    	usersOfProject: function() {
			    		return usersOfProject;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/editContact/editContact.modal.html'
			});
			return editContactModal;
			
			function EditContactModalController($modalInstance, $scope, contact, titles, provinceTypes, contactTypes, countryTypes, invoker, currentUser, roles, usersOfProject) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.titles = titles;
		    	vm.provinceTypes = provinceTypes;
		    	vm.contactTypes = contactTypes;
		    	vm.countryTypes = countryTypes;
		    	vm.invoker = invoker;
		    	vm.currentUser = currentUser;
		    	vm.roles = roles;
				vm.usersOfProject = usersOfProject;
		    	
		    	vm.closeEditContactModal = closeEditContactModal;
		    	
		    	////////////
		    	
		    	function closeEditContactModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();