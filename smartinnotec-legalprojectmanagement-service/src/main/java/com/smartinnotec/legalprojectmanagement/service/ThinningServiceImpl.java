package com.smartinnotec.legalprojectmanagement.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Thinning;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ThinningRepository;

@Service("thinningService")
public class ThinningServiceImpl extends AbstractService implements ThinningService {

    @Autowired
    private ThinningRepository thinningRepository;

    @Override
	public Integer count() throws BusinessException {
    	logger.info("count thinnings in ThinningServiceImpl#count");
    	final Long thinnings = thinningRepository.count();
		return thinnings.intValue();
	}
    
    @Override
    public Thinning findThinningById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ThinningServiceImpl#findThinningById", "400");
        BusinessAssert.isId(id, "id must be an id in ThinningServiceImpl#findThinningById", "400");
        logger.info("find thinning by id '{}' in ThinningServiceImpl#findThinningById", id);
        return thinningRepository.findOne(id);
    }

    @Override
    public List<Thinning> findByProductId(final String productId) throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ThinningServiceImpl#findByProductId", "400");
        BusinessAssert.isId(productId, "productId must be an id in ThinningServiceImpl#findByProductId", "400");
        logger.info("find thinnings by product id '{}' in ThinningServiceImpl#findByProductId", productId);
        return thinningRepository.findByProductId(productId);
    }

    @Override
    public List<Thinning> findByProductIdAndDateBetween(final String productId, final DateTime start, final DateTime end)
            throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ThinningServiceImpl#findByProductIdAndDateBetween", "400");
        BusinessAssert.isId(productId, "productId must be an id in ThinningServiceImpl#findByProductIdAndDateBetween", "400");
        logger.info("find thinnings by product id '{}' and date between '{}' and '{}' in ThinningServiceImpl#findByProductIdAndDateBetween",
            productId, start, end);
        return thinningRepository.findByProductIdAndDateBetween(productId, start, end);
    }

    @Override
    public Thinning findByProductIdOrderByDateDesc(final User user, final String productId) throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ThinningServiceImpl#findByProductIdOrderByDateDesc", "400");
        BusinessAssert.isId(productId, "productId must be an id in ThinningServiceImpl#findByProductIdOrderByDateDesc", "400");
        logger.info("find last thinning by productId '{}' in ThinningServiceImpl#findByProductIdOrderByDateDesc", productId);
        final Thinning thinning = thinningRepository.findByProductIdOrderByDateDesc(productId);
        return thinning;
    }

    @Override
    public Long countByProductId(final String productId) throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ThinningServiceImpl#countByProductId", "400");
        BusinessAssert.isId(productId, "productId must be an id in ThinningServiceImpl#countByProductId", "400");
        logger.info("count thinning by product with id '{}' in ThinningServiceImpl#countByProductId", productId);
        return thinningRepository.countByProductId(productId);
    }

    @Override
    public Thinning create(final Thinning thinning) throws BusinessException {
        BusinessAssert.notNull(thinning, "thinning is mandatory in ThinningServiceImpl#create", "400");
        logger.info("create thinning in ThinningServiceImpl#create");
        return thinningRepository.insert(thinning);
    }

    @Override
    public Thinning update(final Thinning thinning) throws BusinessException {
        BusinessAssert.notNull(thinning, "thinning is mandatory in ThinningServiceImpl#update", "400");
        BusinessAssert.notNull(thinning.getId(), "thinningId is mandatory in ThinningServiceImpl#update", "400");
        BusinessAssert.isId(thinning.getId(), "thinningId must be and id in ThinningServiceImpl#update", "400");
        logger.info("update thinning with id '{}' in ThinningServiceImpl#update", thinning.getId());
        return thinningRepository.save(thinning);
    }

    @Override
    public void delete(final String thinningId) throws BusinessException {
        BusinessAssert.notNull(thinningId, "thinningId is mandatory in ThinningServiceImpl#delete", "400");
        BusinessAssert.isId(thinningId, "thinningId must be and id in ThinningServiceImpl#delete", "400");
        logger.info("delete thinning with id '{}' in ThinningServiceImpl#delete", thinningId);
        thinningRepository.delete(thinningId);
    }

    @Override
    public List<Thinning> getThinningsInReverseOrder(final List<Thinning> thinnings) throws BusinessException {
        logger.info("get thinnings in reverse order in ThinningServiceImpl#ThinningServiceImpl");
        if (thinnings != null && !thinnings.isEmpty()) {
            Comparator<Thinning> cmp = Collections.reverseOrder();
            Collections.sort(thinnings, cmp);
        }
        return thinnings;
    }
}
