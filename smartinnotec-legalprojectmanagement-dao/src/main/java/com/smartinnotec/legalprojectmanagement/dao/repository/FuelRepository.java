package com.smartinnotec.legalprojectmanagement.dao.repository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Fuel;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FuelRepository extends MongoRepository<Fuel, String> {

    List<Fuel> findAllByTenant(final Tenant tenant, final Sort sort);

    List<Fuel> findPagedByTenant(final Tenant tenant, final Pageable pageable);

    Integer countByTenant(final Tenant tenant);
}
