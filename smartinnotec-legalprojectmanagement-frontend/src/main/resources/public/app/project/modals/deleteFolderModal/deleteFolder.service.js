(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('deleteFolderModalService', deleteFolderModalService);
        
    deleteFolderModalService.$inject = ['$modal', '$stateParams'];
    
    function deleteFolderModalService($modal, $stateParams) {
		var service = {
			showDeleteFolderModal: showDeleteFolderModal
		};		
		return service;
		
		////////////
				
		function showDeleteFolderModal(invoker, folder) {
			 var deleteFolderModal = $modal.open({
				controller: DeleteFolderModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	folder: function() {
			    		return folder;
			    	}
			    }, 
				templateUrl: 'app/project/modals/deleteFolderModal/deleteFolder.modal.html'
			});
			return deleteFolderModal;
			
			function DeleteFolderModalController($modalInstance, $scope, invoker, folder) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.folder = folder;
		    	vm.errorCodeType = null;
		    	
		    	vm.deleteFolder = deleteFolder;
		    	vm.closeDeleteFolderModal = closeDeleteFolderModal;
		    	
		    	////////////
		    	
		    	function deleteFolder() {
		    		vm.invoker.deleteFolder(vm.folder, function(response) {
		    			if(response.errorCodeType != null) {
		    				vm.errorCodeType = response.errorCodeType;
		    			} else {
		    				closeDeleteFolderModal();
		    			}
		    		});
		    	}
		    	
		    	function closeDeleteFolderModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
