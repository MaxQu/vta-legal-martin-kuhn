(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactActivityModalService', contactActivityModalService);
        
    contactActivityModalService.$inject = ['$modal', '$stateParams', 'dateUtilityService', 'activityService', 'contactActivityCreateEditModalService', 'yesNoActivityContactModalService'];
    
    function contactActivityModalService($modal, $stateParams, dateUtilityService, activityService, contactActivityCreateEditModalService, yesNoActivityContactModalService) {
		var service = {
			showContactActivityModal: showContactActivityModal
		};		
		return service;
		
		////////////
				
		function showContactActivityModal(contact, activities, start, end, activityTypes, userAuthorizationUserContainer) {
			 var contactActivityModal = $modal.open({
				controller: ContactActivityModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {   
			    	contact: function() {
			    		return contact; 
			    	}, 
			    	activities: function() {
			    		return activities;
			    	}, 
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	},
			    	activityTypes: function() {
			    		return activityTypes;
			    	}, 
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/contactActivity/contactActivity.modal.html'
			});
			return contactActivityModal;
			
			function ContactActivityModalController($modalInstance, $scope, contact, activities, start, end, activityTypes, userAuthorizationUserContainer) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.activities = activities;
		    	vm.filterStartDatePicker = false;
		    	vm.filterEndDatePicker = false;
		    	vm.filterStartDate = start;
		    	vm.filterEndDate = end;
		    	vm.activityTypes = activityTypes;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	
		    	vm.formatStartEndDateTime = formatStartEndDateTime;
		    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
		    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
		    	vm.showContactActivityCreateEditModal = showContactActivityCreateEditModal;
		    	vm.removeActivity = removeActivity;
		    	vm.showRemoveActivity = showRemoveActivity;
		    	vm.closeContactActivityModal = closeContactActivityModal;
		    	
		    	////////////

		    	$scope.$watch('vm.filterStartDate', function(newValue, oldValue) {
		    		if(oldValue != newValue && newValue != null) {
		    			loadActivitiesOfContact();
		    		}
				});
		    	
		    	$scope.$watch('vm.filterEndDate', function(newValue, oldValue) {
		    		if(oldValue != newValue && newValue != null) {
		    			loadActivitiesOfContact();
		    		}
				});
		    	
		    	function loadActivitiesOfContact() {
		    		var startDateString = dateUtilityService.formatDateToString(vm.filterStartDate);
		    		var endDateString = dateUtilityService.formatDateToString(vm.filterEndDate);
		    		activityService.getActivitiesOfContactIdInRange(vm.contact.id, startDateString, endDateString).then(function(response) {
		    			vm.activities = response.data;
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error contactActivitiy.service#loadActivitiesOfContact');
		  	   		 });
		    	}
		    	
		    	function openFilterStartDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterStartDatePicker = true;
		        }
		    	
		    	function openFilterEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterEndDatePicker = true;
		        }
		    	
		    	function showContactActivityCreateEditModal(activity) {
		    		contactActivityCreateEditModalService.showContactActivityCreateEditModal(vm.contact, activity, vm.activityTypes);
		    	}
		    	
		    	function removeActivity(activity) {
		    		activityService.deleteActivity(activity).then(function(response) {
		    			for(var i = 0; i < vm.activities.length; i++) {
		    				if(vm.activities[i].id === activity.id) {
		    					vm.activities.splice(i, 1);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error contactActivitiy.service#removeActivity');
		  	   		 });
		    	}
		    	
		    	function formatStartEndDateTime(start, end) {
		    		var startDate = new Date(start);
		    		var formattedStartDate = dateUtilityService.formatDateToDateTimeString(startDate);
		    		var endDate = new Date(end);
		    		var formattedEndDate = dateUtilityService.formatDateToDateTimeString(endDate);
		    		return dateUtilityService.formatStartEndDateTime(formattedStartDate, formattedEndDate);
		    	}
		    	
		    	function showRemoveActivity(activity) {
		    		yesNoActivityContactModalService.showYesNoModal(vm, vm.contact, activity);
		    	}
		    	
		    	function closeContactActivityModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();