(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('asUniqueEmail', asUniqueEmail);
    
    asUniqueEmail.$inject = ['validateService'];
    
	function asUniqueEmail(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////
		
		function link(scope, element, attrs, ngModel) {
			ngModel.$parsers.unshift(parser);
			ngModel.$formatters.unshift(formatter);
			
			////////////
			
			function parser(value) {
				if(value.length > 3) {
					validateService.checkUniqueEmail(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 3) {
					validateService.checkUniqueEmail(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();