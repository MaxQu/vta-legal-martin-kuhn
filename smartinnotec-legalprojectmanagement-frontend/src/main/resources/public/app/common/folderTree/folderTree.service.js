(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('folderTreeService', folderTreeService);
    
	folderTreeService.$inject = ['$http', '$q', 'api_config'];
	
	function folderTreeService($http, $q, api_config) {
		var service = {		
			createTree: createTree
		};
		
		return service;
		
		////////////
	
		function createTree(options) {
			var children, e, id, o, pid, temp, _i, _len, _ref;     
	         id =  "id";
	         pid =  "superFolderId";
	         children =  "children";
	         temp = {};
	         o = [];
	         _ref = options.q;
	         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	           e = _ref[_i];
	           e[children] = [];
	           temp[e.id] = e;
	           if (temp[e.superFolderId] != null) { 
	             e.path =  temp[e.superFolderId].path + '/' + e.name;  
	             temp[e.superFolderId].children.push(e);                          
	           }
	           else { // no parent
	             e.path = e.name;
	             o.push(e);     
	           }
	         }
	         return o;
		}
	}
})();