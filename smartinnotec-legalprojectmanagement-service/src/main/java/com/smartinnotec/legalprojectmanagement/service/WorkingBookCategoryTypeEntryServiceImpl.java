package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEntry;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.repository.WorkingBookCategoryTypeEntryRepository;

@Service("workingBookCategoryTypeEntryService")
public class WorkingBookCategoryTypeEntryServiceImpl extends AbstractService implements WorkingBookCategoryTypeEntryService {

    @Autowired
    private WorkingBookCategoryTypeEntryRepository workingBookCategoryTypeEntryRepository;

    @Override
    public List<WorkingBookCategoryTypeEntry> findByWorkingBookCategoryTypeEnumAndTenant(final String projectId,
            final WorkingBookCategoryTypeEnum workingBookCategoryType, final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(workingBookCategoryType,
            "workingBookCategoryType is mandatory in WorkingBookCategoryTypeEntryServiceImpl#findByWorkingBookCategoryTypeEnumAndTenant",
            "400");
        BusinessAssert.notNull(tenant,
            "tenant is mandatory in WorkingBookCategoryTypeEntryServiceImpl#findByWorkingBookCategoryTypeEnumAndTenant", "400");
        BusinessAssert.notNull(projectId,
            "projectId is mandatory in WorkingBookCategoryTypeEntryServiceImpl#findByWorkingBookCategoryTypeEnumAndTenant", "400");
        BusinessAssert.isId(projectId,
            "projectId must be an id in WorkingBookCategoryTypeEntryServiceImpl#findByWorkingBookCategoryTypeEnumAndTenant", "400");
        logger.info(
            "find by workingBookCategoryType '{}' and tenant with id '{}' in WorkingBookCategoryTypeEntryServiceImpl#findByWorkingBookCategoryTypeEnumAndTenant",
            workingBookCategoryType, tenant.getId());
        return workingBookCategoryTypeEntryRepository.findByProjectIdAndWorkingBookCategoryTypeAndTenant(projectId, workingBookCategoryType,
            tenant);
    }

    @Override
    public WorkingBookCategoryTypeEntry createWorkingBookCategoryTypeEntry(final WorkingBookCategoryTypeEntry workingBookCategoryTypeEntry)
            throws BusinessException {
        BusinessAssert.notNull(workingBookCategoryTypeEntry,
            "workingBookCategoryTypeEntry is mandatory in WorkingBookCategoryTypeEntryServiceImpl#createWorkingBookCategoryTypeEntry",
            "400");
        logger.info("create workingBookCategoryTypeEntry in WorkingBookCategoryTypeEntryServiceImpl#createWorkingBookCategoryTypeEntry");
        return workingBookCategoryTypeEntryRepository.insert(workingBookCategoryTypeEntry);
    }

    @Override
    public WorkingBookCategoryTypeEntry updateWorkingBookCategoryTypeEntry(final WorkingBookCategoryTypeEntry workingBookCategoryTypeEntry)
            throws BusinessException {
        BusinessAssert.notNull(workingBookCategoryTypeEntry,
            "workingBookCategoryTypeEntry is mandatory in WorkingBookCategoryTypeEntryServiceImpl#updateWorkingBookCategoryTypeEntry",
            "400");
        logger.info(
            "update workingBookCategoryTypeEntry with id '{}' in WorkingBookCategoryTypeEntryServiceImpl#updateWorkingBookCategoryTypeEntry",
            workingBookCategoryTypeEntry.getId());
        return workingBookCategoryTypeEntryRepository.save(workingBookCategoryTypeEntry);
    }

    @Override
    public void deleteWorkingBookCategoryType(final String workingBookCategoryTypeId) throws BusinessException {
        BusinessAssert.notNull(workingBookCategoryTypeId,
            "workingBookCategoryTypeId is mandatory in WorkingBookCategoryTypeEntryServiceImpl#deleteWorkingBookCategoryType", "400");
        logger.info("delete workingBookCategoryType with id '{}' in WorkingBookCategoryTypeEntryServiceImpl#deleteWorkingBookCategoryType",
            workingBookCategoryTypeId);
        workingBookCategoryTypeEntryRepository.delete(workingBookCategoryTypeId);
    }
}
