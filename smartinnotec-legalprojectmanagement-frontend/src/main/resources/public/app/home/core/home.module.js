(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.home', [
    	    'legalprojectmanagement.common',
    	    'legalprojectmanagement.constants',
    	    'ui.router', 
    	    'ui.bootstrap', 
    	    'ui.bootstrap.showErrors'
    	]);
})();