(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user', [
		    'pascalprecht.translate',
    	    'legalprojectmanagement.common',
    	    'legalprojectmanagement.constants',
    	    'ui.router', 
    	    'ui.bootstrap',
    	    'ui.validate'
        ]);  
})();