(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('historyOfReceipeModalService', historyOfReceipeModalService);
        
    historyOfReceipeModalService.$inject = ['$modal', '$stateParams', 'productsService', 'documentFileService', 'projectTemplateService', 'receipeService', 'dateUtilityService', 'printingPDFService', 'productOverviewModalService', 'printModalService'];
    
    function historyOfReceipeModalService($modal, $stateParams, productsService, documentFileService, projectTemplateService, receipeService, dateUtilityService, printingPDFService, productOverviewModalService, printModalService) {
		var service = {
			showHistoryOfReceipeModal: showHistoryOfReceipeModal
		};		
		return service;
		
		////////////
				
		function showHistoryOfReceipeModal(currentUser, receipeType, product, receipesOfProduct, start, end) { 
			 var historyOfReceipeModal = $modal.open({
				controller: HistoryOfReceipeModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	receipeType: function() {
			    		return receipeType;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	receipesOfProduct: function() {
			    		return receipesOfProduct;
			    	},
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	}
			    }, 
				templateUrl: 'app/products/modals/historyOfReceipeModal/historyOfReceipe.modal.html'
			});
			return historyOfReceipeModal;
			
			function HistoryOfReceipeModalController($modalInstance, $scope, currentUser, receipeType, product, receipesOfProduct, start, end) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.receipeType = receipeType;
		    	vm.product = product;
		    	vm.receipesOfProduct = receipesOfProduct;
		    	vm.searchReceipeDateFromDatePicker = false;
		    	vm.searchReceipeDateUntilDatePicker = false;
		    	vm.receipeDateFrom = start;
		    	vm.receipeDateUntil = end;
		    	
		    	vm.printContainer = printContainer;
		    	vm.openSearchReceipeFromDatePicker = openSearchReceipeFromDatePicker;
		    	vm.openSearchReceipeUntilDatePicker = openSearchReceipeUntilDatePicker;
		    	vm.findReceipesOfProductOfTypeInLastMonth = findReceipesOfProductOfTypeInLastMonth;
		    	vm.createDocumentUrl = createDocumentUrl;
		    	vm.showTemplateModal = showTemplateModal;
		    	vm.showProductOverviewModalService = showProductOverviewModalService;
		    	vm.closeHistoryOfReceipeModal = closeHistoryOfReceipeModal;
		    	vm.showPrintModal = showPrintModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	$scope.$watch('vm.receipeDateFrom', function (newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.receipeDateFrom, vm.receipeDateUntil);
			    		if(vm.receipeDateFrom != null && vm.receipeDateUntil != null && checkDateRange == false) {
			    			searchReceipesInTimeRange();
			    		}
		    		}
		        });
		    	
		    	$scope.$watch('vm.receipeDateUntil', function (newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.receipeDateFrom, vm.receipeDateUntil);
			    		if(vm.receipeDateFrom != null && vm.receipeDateUntil != null && checkDateRange == false) {
			    			searchReceipesInTimeRange();
			    		}
		    		}
		        });
		    	
		    	function printContainer(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	}
		    	
		    	function openSearchReceipeFromDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchReceipeDateFromDatePicker = true; 
				}
		    	
		    	function openSearchReceipeUntilDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchReceipeDateUntilDatePicker = true; 
				}
				
				function searchReceipesInTimeRange() {
					var start = vm.receipeDateFrom instanceof Date ? dateUtilityService.formatDateToString(vm.receipeDateFrom) : vm.receipeDateFrom;
					var end = vm.receipeDateUntil instanceof Date ? dateUtilityService.formatDateToString(vm.receipeDateUntil) : vm.receipeDateUntil;
					receipeService.findReceipesOfProductOfTypeInRange(vm.product.id, vm.receipeType, start, end).then(function(response) {
						vm.receipesOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfReceipe.service#searchReceipesInTimeRange');
		 	   		 });
				}
				
				function findReceipesOfProductOfTypeInLastMonth() {
					vm.receipeDateFrom = dateUtilityService.oneMonthBack(new Date());
			    	vm.receipeDateUntil = new Date();
			    	var startDateString = dateUtilityService.formatDateToString(vm.receipeDateFrom);
		    		var endDateString = dateUtilityService.formatDateToString(vm.receipeDateUntil);
			    	vm.searchReceipeTerm = null;
					receipeService.findReceipesOfProductOfTypeInRange(vm.product.id, vm.receipeType, startDateString, endDateString).then(function(response) {
						vm.receipesOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfReceipe.controller#findAllReceipesOfProductOfType');
		 	   		 });
				}
		    	
		    	function createDocumentUrl(user, product, receipeAttachment) {		    	
		    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
		    		return 'api/filedownloads/filedownload/' + user.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
		    	}
		    	
		    	function showTemplateModal(url, receipeAttachment) {
		    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
		    			var documentFile = response.data;
		    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
		    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#getProductsByTerm'); 
		  	   		 });
		    	}
		    	
		    	function showProductOverviewModalService(product) {
		    		productsService.findProduct(product.id).then(function(response) {
		    			productOverviewModalService.showProductOverviewModal(response.data, vm);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfReceipe.service#showProductOverviewModalService');
		 	   		 });
		    	}
		    	
		    	function showPrintModal(receipeOfProduct) {
		    		printModalService.showPrintModal(vm.product, receipeOfProduct, vm.receipeType, vm.currentUser);
		    	}
		    	
		    	function closeHistoryOfReceipeModal() {
					$modalInstance.dismiss('cancel');  
				}
			} 
		}
    }
})();