(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.controller('ProjectController', ProjectController);
     
    ProjectController.$inject = ['$scope', '$timeout', 'currentUser', 'type', 'projectOrProduct', 'userAuthorizationUserContainer', 'projectUserConnectionsOfProject', 'projectTemplateService', 'uploadService', 'projectsService', 'documentFileService', 'yesNoModalService', 'contactsService', 'userService', 'contactAndUserService', 'folderService', 'createFolderModalService', 'amountOfDocumentFilesWithUniqueName', 'amountOfPages', 'documentFilePageSize', 'pagedDocumentFiles', 'folders', 'folderId', 'usersOfProject', 'allLabelsPredefined', 'canSeeConfidentialDocumentFiles', 'deleteFolderModalService', 'shiftDocumentFileModalService', 'documentInformationModalService', 'roles', 'roleOfAssignedUserIsBlackListedModalService'];
    
    function ProjectController($scope, $timeout, currentUser, type, projectOrProduct, userAuthorizationUserContainer, projectUserConnectionsOfProject, projectTemplateService, uploadService, projectsService, documentFileService, yesNoModalService, contactsService, userService, contactAndUserService, folderService, createFolderModalService, amountOfDocumentFilesWithUniqueName, amountOfPages, documentFilePageSize, pagedDocumentFiles, folders, folderId, usersOfProject, allLabelsPredefined, canSeeConfidentialDocumentFiles, deleteFolderModalService, shiftDocumentFileModalService, documentInformationModalService, roles, roleOfAssignedUserIsBlackListedModalService) {
	    $scope.vm = this;  
    	var vm = this;
    	
    	vm.type = type;
    	vm.amountOfDocumentFilesWithUniqueName = amountOfDocumentFilesWithUniqueName.data;
    	vm.amountOfDocumentFilesInFolderWithUniqueName = null;
    	vm.pagedDocumentFiles = setContactOrUser(pagedDocumentFiles.data);
    	vm.amountOfPages = amountOfPages.data;
    	vm.projectUserConnectionsOfProject = projectUserConnectionsOfProject != null ? projectUserConnectionsOfProject.data : null;
    	vm.folders = folders.data;
    	vm.roles = roles.data;
    	vm.folderId = folderId;
    	vm.documentFilePageSize = documentFilePageSize.data;
    	vm.usersOfProject = usersOfProject != null ? usersOfProject.data : null;
    	vm.allLabelsPredefined = allLabelsPredefined.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.canSeeConfidentialDocumentFiles = canSeeConfidentialDocumentFiles.data;
    	vm.projectOrProduct = projectOrProduct.data;
    	vm.currentUser = currentUser;
    	vm.orderType = 'fileName';
    	vm.currentPage = 0;
    	vm.selectedContact = null;
    	vm.labelOrder = 'labelsPredefinedType';
    	vm.uploadFiles = false;
    	vm.successfulUploadedFiles = 0;
    	vm.failedUploadedFiles = 0;
    	vm.documentFileAndFolderSearchString = '';
    	vm.superFolderId = '';
    	vm.assignedDocumentFiles = false;
    	vm.confidentDocumentFiles = false;
    	vm.breadCrumbFolderContainer = null;
    	vm.referenceToCommunicationError = false;
    	vm.referenceToRecipeError = false;
    	vm.referenceToContactError = false;

    	vm.successCallback = successCallback;
    	vm.errorCallback = errorCallback;
    	vm.progressCallback = progressCallback;
    	vm.updateDocumentFile = updateDocumentFile;
    	vm.updateDocumentFileLabels = updateDocumentFileLabels;
    	vm.labelInputChange = labelInputChange;
    	vm.removeLabel = removeLabel;
    	vm.addLabel = addLabel;
    	vm.arrayRang = arrayRang;
    	vm.pageChange = pageChange;
    	vm.deleteDocumentFileVersion = deleteDocumentFileVersion;
    	vm.deleteAllDocumentVersions = deleteAllDocumentVersions;
    	vm.createDocumentUrl = createDocumentUrl;
    	vm.showTemplateModal = showTemplateModal;
    	vm.showYesNoDeleteModal = showYesNoDeleteModal;
    	vm.setContactOrUser = setContactOrUser;
    	vm.findContactAndUserBySearchString = findContactAndUserBySearchString;
    	vm.prepareForUpdateDocumentFile = prepareForUpdateDocumentFile;
    	vm.isCurrentPage = isCurrentPage;
    	vm.uploadNewDocumentVersion = uploadNewDocumentVersion;
    	vm.isLabelSelected = isLabelSelected;
    	vm.isBlackListed = isBlackListed;
    	vm.isRoleBlackListed = isRoleBlackListed;
    	vm.addBlackListId = addBlackListId;
    	vm.addBlackListRole = addBlackListRole;
    	vm.removeBlackListId = removeBlackListId;
    	vm.removeBlackListRole = removeBlackListRole;
    	vm.isWhiteListed = isWhiteListed;
    	vm.addWhiteListId = addWhiteListId;
    	vm.removeWhiteListId = removeWhiteListId;
    	vm.isCurrentUserWhiteListed = isCurrentUserWhiteListed;
    	vm.searchForLabel = searchForLabel;
    	vm.removeSearchString = removeSearchString;
    	vm.changeOrderType = changeOrderType;
    	vm.isWhiteListedExclusion = isWhiteListedExclusion;
    	vm.isBlackListedExclusion = isBlackListedExclusion;
    	vm.findAssignedDocumentFiles = findAssignedDocumentFiles;
    	vm.showCreateProjectFolderModal = showCreateProjectFolderModal;
    	vm.showUpdateProjectFolderModal = showUpdateProjectFolderModal;
    	vm.createProjectFolder = createProjectFolder;
    	vm.updateProjectFolder = updateProjectFolder;
    	vm.selectFolder = selectFolder;
    	vm.selectBreadCrumbFolder = selectBreadCrumbFolder;
    	vm.showDeleteFolderModal = showDeleteFolderModal;
    	vm.deleteFolder = deleteFolder;
    	vm.showShiftModal = showShiftModal;
    	vm.searchDocumentFiles = searchDocumentFiles;
    	vm.setDocumentFileVersion = setDocumentFileVersion;
    	vm.removeDocumentFileOfPagedDocumentFiles = removeDocumentFileOfPagedDocumentFiles;
    	vm.showDocumentInformationModal = showDocumentInformationModal;
    	vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    	var folder = {};
    	folder.id = vm.folderId;
    	findBreadCrumbFolder(folder);
    	
    	////////////
    	
    	$scope.$watch('vm.files', function () {
    		unsetStillUploadedFlag();
    		uploadService.uploadFiles(vm.files, currentUser.id, vm.projectOrProduct.id, vm.superFolderId, vm.type, vm.successCallback, vm.errorCallback, vm.progressCallback);
        });
    	
        $scope.$watch('file', function () {
            if (vm.file != null) {
                vm.files = [vm.file]; 
            }
        });
        
        function searchDocumentFiles() {
    		findDocumentFilesBySearchString();
    		findFoldersBySearchString();
        }
        
        function createFolderObject(project, name, description, superFolderId, active) {
        	var folder = {};
        	folder.name = name;
        	folder.description = description;
        	folder.created = new Date();
        	folder.projectId = project.id;
        	folder.superFolderId = superFolderId;
        	folder.creationUserId = vm.currentUser.id;
        	folder.active = active;
        	folder.confident = false;
        	return folder;
        }
        
        function removeSearchString() {
        	vm.documentFileAndFolderSearchString = '';
        	findDocumentFilesBySearchString();
        }
        
        function searchForLabel(label) {
        	vm.documentFileAndFolderSearchString = label;
        } 
        
        function findDocumentFilesBySearchString() {
        	if(vm.documentFileAndFolderSearchString != null && vm.documentFileAndFolderSearchString.length >= 3) {
        		documentFileService.findDocumentFilesByBySearchStringAndProjectIdAndFolderId(vm.documentFileAndFolderSearchString, vm.projectOrProduct.id, vm.superFolderId).then(function successCallback(response) {
        			vm.pagedDocumentFiles = setContactOrUser(response.data);
      	   		 }, function errorCallback(response) {
      	   			  console.log('error project.controller.js#findDocumentFilesBySearchString');
      	   		 });
        	} else if(vm.documentFileAndFolderSearchString == null || vm.documentFileAndFolderSearchString.length === 0) {
        		pageChange(0);
        	}
        }
        
        function setDocumentFileVersion(documentFile) {
        	if(documentFile.documentFileVersion != null) {
        		documentFile.documentFileVersion = documentFile.selectedDocumentFileVersion;
        	} else {
        		documentFile.documentFileVersion = documentFile.documentFileVersions[documentFile.documentFileVersions.length-1];
        		documentFile.selectedDocumentFileVersion = documentFile.documentFileVersions[documentFile.documentFileVersions.length-1];
        	}
        }
        
        function findFoldersBySearchString() {
        	if(vm.documentFileAndFolderSearchString != null && vm.documentFileAndFolderSearchString.length >= 3) {
        		folderService.findFoldersByBySearchStringAndProjectIdAndFolderId(vm.documentFileAndFolderSearchString, vm.projectOrProduct.id, vm.superFolderId).then(function(response) {
        			vm.folders = response.data;
      	   		 }, function errorCallback(response) {
      	   			  console.log('error project.controller.js#findFoldersBySearchString');
      	   		 });
        	} else {
        		folderService.findFolders(vm.projectOrProduct.id, vm.superFolderId).then(function(response) {
        			vm.folders = response.data;
     	   		 }, function errorCallback(response) {
     	   			  console.log('error project.controller.js#findFoldersBySearchString');
     	   		 });
        	}
        }
        
        function isLabelSelected(labelPredefined, documentFileVersion) {
        	if(documentFileVersion == null || documentFileVersion.labels == null) {
        		return false;
        	}
        	for(var i = 0; i < documentFileVersion.labels.length; i++) {
        		if(documentFileVersion.labels[i].name == labelPredefined.name) {
        			return true;
        		}
        	}
        	return false;
        }
        
        function uploadNewDocumentVersion(document, currentDocumentFile) {
        	unsetStillUploadedFlag();
        	uploadService.uploadNewDocumentVersion(document, currentUser.id, vm.projectOrProduct.id, currentDocumentFile.id, vm.successCallback, vm.progressCallback);
        }
        
        function setResponsibleUserOrContactAddedTimeout(documentFile) {
      		$timeout(function() {
      			documentFile.labelUpdated = false;
      			documentFile.setContactOrUserSuccess = false;
  			   }, 2000); 
      	}
    	
        function removeDocumentFileOfPagedDocumentFiles(documentFile) {
        	for(var i = 0; i < vm.pagedDocumentFiles.length; i++) {
        		var pagedDocumentFile = vm.pagedDocumentFiles[i];
        		if(pagedDocumentFile.id === documentFile.id) {
        			vm.pagedDocumentFiles.splice(i, 1);
        		}
        	}
        }
        
    	function successCallback(response) {
    		var documentFile = response.data;
    		removeDocumentFileOfPagedDocumentFiles(documentFile);
    		documentFile.stillUploaded = true;
    		vm.pagedDocumentFiles.splice(0, 0, documentFile); // add file
    		setContactOrUser([documentFile]);
    		calculateAmountOfPages();
    		getAmountOfFileDocuments();
    		getAmountOfFileDocumentsInFolder();
    		vm.successfulUploadedFiles += 1; 
    		setFileUpdateTimeout();
    		setSuccessUploadFilesTimeout();
    	}
    	
    	function errorCallback(response) {
    		vm.failedUploadedFiles += 1;
    		setFailedUploadFilesTimeout();
    		setFileUpdateTimeout();
    		console.log('upload error in project.controller: statusText: ' + response.statusText + ', response- object: ' + response + ', message: ' + response.message + ', data: ' + response.data);
    	}
    	
    	function progressCallback(log) {
    		vm.uploadFiles = true;
    		console.log('upload log in project.controller: ' + log);
    	}
    	
    	function setFileUpdateTimeout() {
      		$timeout(function() {
      			vm.uploadFiles = false;
  			}, 2500); 
      	}
    	
    	function setDocumentFileDeleteTimeout() {
    		$timeout(function() {
    			vm.referenceToCommunicationError = false;
            	vm.referenceToRecipeError = false;
            	vm.referenceToContactError = false;
  			 }, 2500);
    	}
    	
    	function setSuccessUploadFilesTimeout() {
      		$timeout(function() {
      			vm.successfulUploadedFiles = 0;
  			   }, 10000); 
      	}
    	
    	function setFailedUploadFilesTimeout() {
      		$timeout(function() {
      			vm.failedUploadedFiles = 0;
  			   }, 10000); 
      	}
    	
    	function calculateAmountOfPages() {
    		documentFileService.calculateAmountOfPages(vm.projectOrProduct.id, vm.type, vm.superFolderId).then(function successCallback(response) {
				vm.amountOfPages = response.data;
				vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#calculateAmountOfPages');
  	   		 });
    	}
    	
    	function getAmountOfFileDocuments() {
    		documentFileService.countByProjectIdGroupByOriginalFileName(vm.projectOrProduct.id, vm.confidentDocumentFiles, vm.type).then(function successCallback(response) {
    			vm.amountOfDocumentFilesWithUniqueName = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#getAmountOfFileDocuments');
  	   		 });
    	}
    	
    	function getAmountOfFileDocumentsInFolder() {
    		documentFileService.countByProjectIdAndFolderIdGroupByOriginalFileName(vm.projectOrProduct.id, vm.superFolderId, vm.confidentDocumentFiles, vm.type).then(function(response) {
    			vm.amountOfDocumentFilesInFolderWithUniqueName = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#getAmountOfFileDocumentsInFolder');
  	   		 });
    	}
    	
    	function unsetStillUploadedFlag() {
    		if(vm.pagedDocumentFiles  == null) {
    			return;
    		}
    		for(var i = 0; i < vm.pagedDocumentFiles.length; i++) {
    			vm.pagedDocumentFiles[i].stillUploaded = false;
    		}
    	}
    	
    	function addLabel(documentFile, foundedLabelPredefinedSelected, documentFileVersion) {
    		var stillExists = labelStillExist(documentFileVersion, foundedLabelPredefinedSelected);
    		if(stillExists) {
    			return;
    		}
    		if(documentFileVersion.labels == null) {
    			documentFileVersion.labels = [];
    		}
    		if(foundedLabelPredefinedSelected != null && typeof foundedLabelPredefinedSelected != 'undefined') {
    			documentFileVersion.labels.push(foundedLabelPredefinedSelected);
    		}
    		updateDocumentFileLabels(documentFile);
    	}
    	
    	function labelStillExist(documentFileVersion, newLabel) {
    		if(documentFileVersion.labels == null || typeof documentFileVersion.labels == 'undefined') {
    			return false;
    		}
    		for(var i = 0; i < documentFileVersion.labels.length; i++) {
    			if(documentFileVersion.labels[i].name == newLabel.name) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function updateDocumentFile(documentFile, updateDocumentFileResponsibility) {
    		if(documentFile.fileName.length < 3) {
    			documentFile.nameLengthError = true;
    			return;
    		}
    		documentFile.nameLengthError = false;
    		documentFileService.updateDocumentFile(documentFile, vm.projectOrProduct.id).then(function successCallback(response) {
    			documentFile.foundedLabelsPredefined = null;
    			var updatedDocumentFile = response.data;
    			if(updateDocumentFileResponsibility === true) {
    				setResponsibleUserOrContactOrPlainText(updatedDocumentFile);
    			}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller.js#updateDocumentFile');
  	   		 });
    	} 
    	
    	function updateDocumentFileLabels(documentFile) {
    		documentFileService.updateDocumentFileLabels(documentFile, vm.projectOrProduct.id).then(function successCallback(response) {
    			documentFile.labelUpdated = true;
    			setResponsibleUserOrContactAddedTimeout(documentFile);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller.js#updateDocumentFileLabels');
  	   		 });
    	}
    	
    	function getDocumentFileVersion(documentFile, documentFileVersion) {
    		for(var i = 0; i < documentFile.documentFileVersions.length; i++) {
    			var documentFileVersionOfIndex = documentFile.documentFileVersions[i];
    			if(documentFileVersionOfIndex.version === documentFileVersion.version) {
    				return documentFileVersionOfIndex;
    			}
    		}
    		return null;
    	}
    	
    	function labelInputChange(data, documentFile) {
    		if(data.length === 0) {
    			documentFile.foundedLabelsPredefined = [];
    			return;
    		}
    		documentFileService.searchPredefinedLabels(data).then(function successCallback(response) {
    			if(response.data.length > 0) {
    				var foundedLabelsPredefined = response.data;
    				documentFile.foundedLabelsPredefined = foundedLabelsPredefined;
    			}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#labelInputChange');
  	   		 });
    	}
    	
    	function removeLabel(documentFile, documentFileVersion, label) {
    		var keepGoing = true;
			angular.forEach(documentFileVersion.labels, function(value, key) {
				if(keepGoing) {
  				  if(value.name == label.name) { 
  					  documentFileVersion.labels.splice(key, 1);
  					  keepGoing = false;
  				  }
				}
			});
    		updateDocumentFileLabels(documentFile);
    	}
    	
    	function arrayRang(min, max, step) {
    	    step = step || 1;
    	    var input = [];
    	    for (var i = min; i <= max; i += step) {
    	        input.push(i);
    	    }
    	    return input;
    	}
    	
    	function changeOrderType(orderType) { 
    		vm.orderType = orderType;
    		pageChange(0);
    	}
    	
    	function pageChange(page) {
    		vm.pagedDocumentFiles = null;
    		if(page < 0 || page > vm.amountOfPagesArray.length-1) {
    			return;
    		}
    		if(vm.assignedDocumentFiles === false) {
    			findPagedDocumentFiles(page);
    		} else {
    			findAssignedDocumentFiles(page);
    		}
    		getAmountOfFileDocuments();
    	}
    	
    	function findPagedDocumentFiles(page) {
    		documentFileService.findPagedDocumentFilesByProjectIdAndFolderId(vm.projectOrProduct.id, vm.superFolderId, page, vm.orderType, vm.confidentDocumentFiles, vm.type).then(function successCallback(response) {
    			vm.pagedDocumentFiles = setContactOrUser(response.data);
    			vm.currentPage = page;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller.js#findPagedDocumentFiles');
  	   		 });
    	}
    	
    	function findAssignedDocumentFiles(page) {
    		if(vm.assignedDocumentFiles === false) {
    			findPagedDocumentFiles(page);
    			getAmountOfFileDocuments();
    		} else {
    			documentFileService.findAssignedDocumentFilesByProjectId(vm.projectOrProduct.id, vm.orderType).then(function successCallback(response) {
        			vm.pagedDocumentFiles = setContactOrUser(response.data);
        			vm.currentPage = page;
        			vm.amountOfDocumentFilesWithUniqueName = vm.pagedDocumentFiles.length;
      	   		 }, function errorCallback(response) {
      	   			  console.log('error project.controller.js#findPagedAndAssignedDocumentFiles');
      	   		 });
    		}
    	}

    	function showYesNoDeleteModal(documentFile, documentFileVersion, type) {
    		yesNoModalService.showYesNoModal(documentFile, documentFileVersion, vm, type);
    	}
    	
    	function deleteDocumentFileVersion(documentFile, documentFileVersion) {
    		if(documentFileVersion == null) {
    			return;
    		}
    		for(var i = 0; i < documentFile.documentFileVersions.length; i++) {
				if(documentFile.documentFileVersions[i].version == documentFileVersion.version) {
					documentFile.documentFileVersions[i].active = false;
					documentFile.documentFileVersions[i].userIdDeactivated = currentUser.id;
					break;
				}
    		}
    		documentFileService.updateDocumentFile(documentFile, vm.projectOrProduct.id).then(function successCallback(response) {
    			var updatedDocumentFile = response.data;
    			for(var i = 0; i < updatedDocumentFile.documentFileVersions.length; i++) {
    				if(updatedDocumentFile.documentFileVersions[i].active === true) {
    					documentFileVersion = updatedDocumentFile.documentFileVersions[i];
    					break;
    				}
        		}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#deleteDocumentVersion');
  	   		 });
    	}
    	
    	function deleteAllDocumentVersions(documentFile) {
    		documentFile.active = false;
    		documentFileService.deleteDocumentFile(documentFile.id, vm.projectOrProduct.id).then(function(response) {
    			for(var i = 0; i < vm.pagedDocumentFiles.length; i++) {
    				if(vm.pagedDocumentFiles[i].id == documentFile.id) {
    					vm.pagedDocumentFiles.splice(i, 1);
    					getAmountOfFileDocuments();
    				}
    			}
  	   		 }, function errorCallback(response) {
  	   			 console.log('error project.controller#deleteAllDocumentVersions');
  	   			 if(response.data.message === 'REFERENCE_TO_COMMUNICATION') {
  	   				 vm.referenceToCommunicationError = true;
  	   			 }
  	   			 if(response.data.message === 'REFERENCE_TO_RECIPE') {
  	   				 vm.referenceToRecipeError = true;
	   			 }
	  	   		 if(response.data.message === 'REFERENCE_TO_CONTACT') {
	  	   			 vm.referenceToContactError = true;
	  	   		 }
	  	   		setDocumentFileDeleteTimeout();
  	   		 });
    	}
    	
    	function createDocumentUrl(user, project, documentFile, documentFileVersion) {
    		if(documentFileVersion == null) {
    			return '';
    		}
    		var version = documentFileVersion === undefined ? '_Version1' : documentFileVersion.version;
    		return 'api/filedownloads/filedownload/' + user.id + '/' + documentFile.id + '/' + version + '/';
    	}
    	
    	function showTemplateModal(url, documentFile, documentFileVersion) {
    		projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
    	}
    	
    	function findContactAndUserBySearchString(searchString) {
    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function (response) {
    			return response.data;
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findContactBySearchString: ' + data);
			});
    	}
    	
    	function setResponsibleUserOrContactOrPlainText(updatedDocumentFile) {
    		for(var i = 0; i < vm.pagedDocumentFiles.length; i++) {
				if(vm.pagedDocumentFiles[i].id == updatedDocumentFile.id) {
					vm.pagedDocumentFiles[i] = updatedDocumentFile;
					for(var j = 0; j < vm.pagedDocumentFiles[i].documentFileVersions.length; j++) {
						vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleUser != null ? vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleUser : (vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleContact != null ? vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleContact : vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleText);
					}
					vm.pagedDocumentFiles[i].setContactOrUserSuccess = true;
					setResponsibleUserOrContactAddedTimeout(vm.pagedDocumentFiles[i]);
					break;
				}
			}
    	}
    	
    	function setContactOrUser(pagedDocumentFiles) {
    		for(var i = 0; i < pagedDocumentFiles.length; i++) {
    			for(var j = 0; j < pagedDocumentFiles[i].documentFileVersions.length; j++) {
    				if(pagedDocumentFiles[i].documentFileVersions[j].responsibleUser != null) {
    					pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = pagedDocumentFiles[i].documentFileVersions[j].responsibleUser;
        			} else if(pagedDocumentFiles[i].documentFileVersions[j].responsibleContact != null) {
        				pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = pagedDocumentFiles[i].documentFileVersions[j].responsibleContact;
        			} else if(pagedDocumentFiles[i].documentFileVersions[j].responsibleText != null) {
        				pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact  = pagedDocumentFiles[i].documentFileVersions[j].responsibleText;
        			}
    			}
    		}
    		return pagedDocumentFiles;
    	}
    	
    	function prepareForUpdateDocumentFile(documentFile, documentFileVersion) {
    		if(documentFileVersion.responsibleUserOrContact == null || typeof documentFileVersion.responsibleUserOrContact == 'undefined') {
    			documentFile.setContactOrUserError = true;
    			documentFile.setContactOrUserSuccess = false;
    			return;
    		}
    		documentFile.setContactOrUserError = false;
    		if(documentFileVersion.responsibleUserOrContact.contactUserType == 'USER') {
    			documentFileVersion.responsibleUser = documentFileVersion.responsibleUserOrContact.user;
    			documentFileVersion.responsibleContact = null;
    			documentFileVersion.responsibleText = null;
    		} else if(documentFileVersion.responsibleUserOrContact.contactUserType == 'CONTACT') {
    			documentFileVersion.responsibleContact = documentFileVersion.responsibleUserOrContact.contact;
    			documentFileVersion.responsibleUser = null;
    			documentFileVersion.responsibleText = null;
    		} else if(documentFileVersion.responsibleUserOrContact.length > 0) {
    			documentFileVersion.responsibleContact = null;
    			documentFileVersion.responsibleUser = null;
    			documentFileVersion.responsibleText = documentFileVersion.responsibleUserOrContact;
    		} 
    		vm.updateDocumentFile(documentFile, true);
    		documentFile.setContactOrUserSuccess = true;
    		setResponsibleUserOrContactAddedTimeout(documentFile);
    	}
    	
    	function isCurrentPage(indexProject) {
    		if(indexProject == (vm.currentPage+1)) {
    			return true;
    		}
    		return false; 
    	}
    	
    	function isBlackListed(user, documentFile) {
    		if(documentFile.userIdBlackList == null || user == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdBlackList.length; i++) {
    			if(documentFile.userIdBlackList[i] == user.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function isRoleBlackListed(role, documentFile) {
    		if(documentFile.rolesBlackList == null || role == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.rolesBlackList.length; i++) {
    			if(documentFile.rolesBlackList[i] == role) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function addBlackListId(documentFile, user, userAuthorizationUserContainer) {
    		if(vm.type == 'PROJECT' && userAuthorizationUserContainer.userAuthorizations[1].state == false) {  // project not changable
    			return;
    		}
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		if(documentFile.userIdBlackList == null) {
    			documentFile.userIdBlackList = [];
    		} 
    		var addTrigger = true;
    		for(var i = 0; i < documentFile.userIdBlackList.length; i++) {
    			if(documentFile.userIdBlackList[i] == user.id) {
    				addTrigger = false;
    			}
    		}
    		if(addTrigger) {
    			documentFile.userIdBlackList.push(user.id);
    			updateDocumentFile(documentFile, false);
    		}
    	}
    	
    	function addBlackListRole(documentFile, role, userAuthorizationUserContainer) {
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		if(documentFile.rolesBlackList == null) {
    			documentFile.rolesBlackList = [];
    		} 
    		var addTrigger = true;
    		for(var i = 0; i < documentFile.rolesBlackList.length; i++) {
    			if(documentFile.rolesBlackList[i] == role) {
    				addTrigger = false;
    			}
    		}
    		if(addTrigger) {
    			documentFile.rolesBlackList.push(role);
    			updateDocumentFile(documentFile, false);
    		}
    	}
    	
    	function removeBlackListId(documentFile, user, userAuthorizationUserContainer) {
    		if(vm.type == 'PROJECT' && userAuthorizationUserContainer.userAuthorizations[1].state == false) {  // project not changable
    			return;
    		}
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		for(var i = 0; i < documentFile.userIdBlackList.length; i++) {
    			if(documentFile.userIdBlackList[i] == user.id) {
    				documentFile.userIdBlackList.splice(i, 1);
    				break;
    			}
    		}
    		updateDocumentFile(documentFile, false);
    	}
    	
    	function removeBlackListRole(documentFile, role, userAuthorizationUserContainer) {
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		for(var i = 0; i < documentFile.rolesBlackList.length; i++) {
    			if(documentFile.rolesBlackList[i] == role) {
    				documentFile.rolesBlackList.splice(i, 1);
    				break;
    			}
    		}
    		updateDocumentFile(documentFile, false);
    	}
    	
    	function isWhiteListed(user, documentFile) {
    		if(documentFile.userIdWhiteList == null || user == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdWhiteList.length; i++) {
    			if(documentFile.userIdWhiteList[i] == user.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function addWhiteListId(documentFile, user, userAuthorizationUserContainer) {
    		if(vm.type == 'PROJECT' && userAuthorizationUserContainer.userAuthorizations[1].state == false) {  // project not changable
    			return;
    		}
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		// is user role of user is black listed than show info
    		if(documentFile.rolesBlackList != null) {
	    		for(var i = 0; i < documentFile.rolesBlackList.length; i++) {
	    			if(documentFile.rolesBlackList[i] == user.projectUserConnectionRole) {
	    				roleOfAssignedUserIsBlackListedModalService.showRoleOfAssignedUserIsBlackListedModal(documentFile, user);
	    				return;
	    			}
	    		}
    		}
    		
    		if(documentFile.userIdWhiteList == null) {
    			documentFile.userIdWhiteList = [];
    		} 
    		var addTrigger = true;
    		for(var j = 0; j < documentFile.userIdWhiteList.length; j++) {
    			if(documentFile.userIdWhiteList[j] == user.id) {
    				addTrigger = false;
    			}
    		}
    		if(addTrigger) {
    			documentFile.userIdWhiteList.push(user.id);
    			updateDocumentFile(documentFile, false);
    		}
    	}
    	
    	function removeWhiteListId(documentFile, user, userAuthorizationUserContainer) {
    		if(vm.type == 'PROJECT' && userAuthorizationUserContainer.userAuthorizations[1].state == false) {  // project not changable
    			return;
    		}
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		for(var i = 0; i < documentFile.userIdWhiteList.length; i++) {
    			if(documentFile.userIdWhiteList[i] == user.id) {
    				documentFile.userIdWhiteList.splice(i, 1);
    				break;
    			}
    		}
    		updateDocumentFile(documentFile, false);
    	}
    	
    	function isCurrentUserWhiteListed(documentFile) {
    		if(documentFile.userIdWhiteList == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdWhiteList.length; i++) {
    			if(documentFile.userIdWhiteList[i] == vm.currentUser.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function isWhiteListedExclusion(documentFile, user) {
    		if(documentFile.userIdWhiteList == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdWhiteList.length; i++) {
    			if(documentFile.userIdWhiteList[i] == user.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function isBlackListedExclusion(documentFile, user) {
    		if(documentFile.userIdBlackList == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdBlackList.length; i++) {
    			if(documentFile.userIdBlackList[i] == user.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function showCreateProjectFolderModal(project) {
    		var folder = createFolderObject(vm.projectOrProduct, '', '', vm.superFolderId, true);
    		createFolderModalService.showCreateFolderModal(vm, folder, 'CREATE'); 
    	}
    	
    	function showUpdateProjectFolderModal(folder) {
    		createFolderModalService.showCreateFolderModal(vm, folder, 'UPDATE'); 
    	}
    	
    	function createProjectFolder(folder, callback) {
    		if(vm.superFolderId != '') {
    			folder.superFolderId = vm.superFolderId;
    		}
    		folderService.create(folder, vm.type).then(function(response) {
    			vm.folders.push(response.data);
    			callback(response.data);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error project.controller.js#createProjectFolder');
 	   			  callback(response.data);
 	   		 });
    	}
    	
    	function updateProjectFolder(folder, callback) {
    		folderService.update(folder, vm.type).then(function(response) {
    			for(var i = 0; i < vm.folders.length; i++) {
    				if(vm.folders[i].id == folder.id) {
    					vm.folders[i] = folder;
    					break;
    				}
    			}
    			callback(response.data);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error project.controller.js#updateProjectFolder');
 	   			  callback(response.data);
 	   		 });
    	}
    	
    	function selectFolder(folder) {
    		vm.superFolderId = folder.id;
    		findFolders(folder);
    		findBreadCrumbFolder(folder);
    		findPagedDocumentFiles(0);
    		calculateAmountOfPages();
    		getAmountOfFileDocumentsInFolder();
    	}
    	
    	function selectBreadCrumbFolder(folder) {
    		vm.superFolderId = folder == null ? '' : folder.id;
    		findFolders(folder);
    		findBreadCrumbFolder(folder);
    		findPagedDocumentFiles(0);
    		calculateAmountOfPages();
    		getAmountOfFileDocumentsInFolder();
    	}
    	
    	function findFolders(folder) {
    		var folderId = folder == null ? '' : folder.id;
    		folderService.findFolders(vm.projectOrProduct.id, folderId).then(function(response) {
    			vm.folders = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error project.controller.js#findFolders');
 	   		 });
    	}
    	
    	function findBreadCrumbFolder(folder) {
    		var folderId = folder == null ? '' : folder.id;
    		folderService.findBreadCrumbOfFolder(folderId).then(function(response) {
    			vm.breadCrumbFolderContainer = response.data;
	   		 }, function errorCallback(response) {
	   			  console.log('error project.controller.js#findBreadCrumbOfFolder');
	   		 });
    	}
    	
    	function showDeleteFolderModal(folder) {
    		deleteFolderModalService.showDeleteFolderModal(vm, folder);
    	}
    	
    	function deleteFolder(folder, callback) {
    		folderService.deleteFolder(folder.id, vm.type).then(function(response) {
    			for(var i = 0; i < vm.folders.length; i++) {
    				if(vm.folders[i].id == folder.id) {
    					vm.folders.splice(i, 1);
    					break;
    				}
    			}
    			callback(response.data);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error project.controller.js#deleteFolder');
 	   			callback(response.data);
 	   		 });
    	}
    	
    	function showShiftModal(documentFile) {
    		folderService.findAllFoldersOfProject(vm.projectOrProduct.id).then(function(response) {
    			shiftDocumentFileModalService.showShiftDocumentFileModal(vm, response.data, documentFile);
	   		 }, function errorCallback(response) {
	   			  console.log('error project.controller.js#findBreadCrumbOfFolder');
	   		 });
    	}
    	
    	function showDocumentInformationModal(documentFile, userAuthorizationUserContainer) {
    		documentInformationModalService.showDocumentInformationModal(documentFile, currentUser, userAuthorizationUserContainer, vm.type, vm, false);
    	}
	} 
})();
