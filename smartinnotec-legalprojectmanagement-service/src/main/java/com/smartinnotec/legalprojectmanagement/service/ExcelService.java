package com.smartinnotec.legalprojectmanagement.service;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public abstract class ExcelService {

    protected DateTimeFormatter dateFormatter;
    protected DateTimeFormatter dateTimeFormatter;
    protected DateTimeFormatter timeFormatter;

    public ExcelService() {
        dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        dateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        timeFormatter = DateTimeFormat.forPattern("HH:mm");
    }

    protected HSSFFont getHeaderFont(final HSSFWorkbook workbook) {
        final HSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short)10);
        font.setFontName("Arial");
        font.setColor(IndexedColors.BLACK.getIndex());
        font.setBold(true);
        font.setItalic(false);
        return font;
    }

    protected HSSFCellStyle getStyle(final HSSFWorkbook workbook) {
        final HSSFFont font = workbook.createFont();
        font.setColor(HSSFColor.BLACK.index);
        final HSSFCellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(HSSFColor.WHITE.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFont(font);
        return style;
    }
}
