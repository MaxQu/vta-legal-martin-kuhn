(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.controller('CommunicationController', CommunicationController);
    
    CommunicationController.$inject = ['$scope', '$timeout', 'moment', 'currentUser', 'communicationGroupUserConnections', 'communicationUserConnectionsOfUserAndPage', 'allUsers', 'sentAndOpenOrders', 'receivedAndOpenOrders', 'projects', 'defaultProject', 'userAuthorizationUserContainer', 'communicationService', 'communicationGroupCreationModalService', 'messageReceiverOverviewModalService', 'dateUtilityService', 'communicationWorkingOrderCreationModalService', 'projectUserConnectionService', 'yesNoCommunicationModalService', 'printMessageOrdersModalService', 'optionsService', 'imgTemplateModalService', 'printingPDFService'];
    
    function CommunicationController($scope, $timeout, moment, currentUser, communicationGroupUserConnections, communicationUserConnectionsOfUserAndPage, allUsers, sentAndOpenOrders, receivedAndOpenOrders, projects, defaultProject, userAuthorizationUserContainer, communicationService, communicationGroupCreationModalService, messageReceiverOverviewModalService, dateUtilityService, communicationWorkingOrderCreationModalService, projectUserConnectionService, yesNoCommunicationModalService, printMessageOrdersModalService, optionsService, imgTemplateModalService, printingPDFService) {
	    $scope.vm = this;   
    	var vm = this;
    	
    	vm.teaserMessageLength = 150;
    	vm.todoView = currentUser.communicationMode == 'TODO' ? true : false;
    	vm.currentUser = currentUser;
    	vm.allUsers = allUsers.data;
    	vm.communicationGroupUserConnections = communicationGroupUserConnections.data;
    	vm.communicationUserConnectionsOfUserAndPage = communicationUserConnectionsOfUserAndPage.data;
    	vm.sentAndOpenOrders = sentAndOpenOrders.data;
    	vm.receivedAndOpenOrders = receivedAndOpenOrders.data;
    	vm.projects = projects.data;
    	vm.defaultProject = defaultProject.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.selectedProject = vm.projects.length == 1 ? vm.projects[0] : (vm.defaultProject != null ? vm.defaultProject : null);
    	vm.selectedUsers = [];
    	vm.selectedCommunicationGroups = [];
    	vm.message = '';
    	vm.messageEdit = false;
    	vm.communicationUserConnectionOfUserAndPageEdit = null;
    	vm.todayDate = new Date(); 
    	vm.filterStartDate = moment();
    	vm.filterEndDate = moment().toDate();
    	vm.filterStartDate = vm.filterStartDate.add(-1, 'month').toDate();
    	vm.filterStartDatePicker = false;
    	vm.filterEndDatePicker = false;
    	vm.projectUserConnectionsOfProject = findProjectUserConnectionsOfProject();
    	vm.sentOrderType = '-sortDate';
    	vm.receivedOrderType = '-sortDate';
    	vm.communicationUserConnectionOfUserAndPage = null;
    	vm.confirmation = false;
    	vm.executionDate = null;
    	vm.executedSearchTrigger = false;
    	
    	vm.printContainer = printContainer;
    	vm.executionDateAfterToday = executionDateAfterToday;
    	vm.selectedUser = selectedUser;
    	vm.showMessageReceiverOverview = showMessageReceiverOverview;
    	vm.communicationConfirmation = communicationConfirmation;
    	vm.communicationOrderExecuted = communicationOrderExecuted;
    	vm.editCommunicationMessage = editCommunicationMessage;
    	vm.archiveCommunicationUserConnection = archiveCommunicationUserConnection;
    	vm.showCommunicationGroupCreationModal = showCommunicationGroupCreationModal;
    	vm.scrollToBottom = scrollToBottom;
    	vm.addSelectedProject = addSelectedProject;
    	vm.removeSelectedProject = removeSelectedProject;
    	vm.addCommunicationUserConnectionToSentAndOpenOrders = addCommunicationUserConnectionToSentAndOpenOrders;
    	vm.updateCommunicationUserConnectionToSentAndOpenOrders = updateCommunicationUserConnectionToSentAndOpenOrders;
    	vm.showYesNoArchiveCommunicationUserConnection = showYesNoArchiveCommunicationUserConnection;
    	vm.getTeasingMessage = getTeasingMessage;
    	vm.findOpenSentAndReceivedWorkingOrders = findOpenSentAndReceivedWorkingOrders;
    	vm.findWorkingOrdersInTimeRange = findWorkingOrdersInTimeRange;
    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
    	vm.showPrintMessageOrders = showPrintMessageOrders;
    	vm.showImagePreview = showImagePreview;
    	vm.createNewWorkingOrderEntry = createNewWorkingOrderEntry;
    	vm.editWorkingOrder = editWorkingOrder;
    	vm.showDocumentFilePreview = showDocumentFilePreview;
    	vm.executedSearchFilter = executedSearchFilter;
    	scrollToBottom();
    	setSelectionOfSelectedProject(vm.selectedProject);
    	
    	////////////
    	
    	$scope.$watch('vm.selectedProject', function(newValue, oldValue) {
    		if(oldValue != newValue && newValue != null) {
    			findProjectUserConnectionsOfProject();
    		}
		});
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function openFilterStartDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterStartDatePicker = true;
        }
    	
    	function openFilterEndDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterEndDatePicker = true;
        }
    	
    	function getTeasingMessage(message) {
    		var teasingMessage = message.substr(0, vm.teaserMessageLength);
    		return teasingMessage;
    	}
    	
    	function findProjectUserConnectionsOfProject() {
    		if(vm.selectedProject == null) {
    			return;
    		}
    		projectUserConnectionService.findProjectUserConnectionByProject(vm.selectedProject.id).then(function successCallback(response) {
    			vm.projectUserConnectionsOfProject = response.data;
   	   		 }, function errorCallback(response) {
   	   			  console.log('error communication.controller#findProjectUserConnectionsOfProject');
   	   		 });
    	}
    	
    	function executedSearchFilter() {
    		return function( item ) {
    		    if(item.executionDate != null && item.executed === vm.executedSearchTrigger) {
    		    	return true;
    		    } else if(vm.executedSearchTrigger === true) {
    		    	return true;
    		    } else {
    		    	return false;
    		    }
    		  };
    	}
    	
    	function findOpenSentAndReceivedWorkingOrders() {
    		vm.executedSearchTrigger = false;
    		communicationService.findSentAndOpenOrders(vm.currentUser.id, false).then(function (response) {
    			vm.sentAndOpenOrders = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findOpenSentAndReceivedWorkingOrders: ' + data);
			});
			communicationService.findReceivedAndOpenOrders(vm.currentUser.id, false).then(function (response) {
    			vm.receivedAndOpenOrders = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findOpenSentAndReceivedWorkingOrders: ' + data);
			});
    	}
    	
    	function findWorkingOrdersInTimeRange() { 
    		vm.executedSearchTrigger = true;
    		var start = dateUtilityService.formatDateToString(vm.filterStartDate);
    		var end = dateUtilityService.formatDateToString(vm.filterEndDate);    		
    		communicationService.findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween(vm.currentUser.id, start, end).then(function (response) {
    			vm.sentAndOpenOrders = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findWorkingOrdersByProjectAndTenantAndTimeRange: ' + data);
			});
    		communicationService.findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween(vm.currentUser.id, start, end).then(function (response) {
    			vm.receivedAndOpenOrders = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findWorkingOrdersReceivedByProjectAndTenantAndTimeRange: ' + data);
			});
    	}
    	
    	function scrollToBottom() {
          	$timeout(function(){
  	        	var communicationHistoryContainer = document.getElementById("communicationHistoryContainer");
  	        	communicationHistoryContainer.scrollTop = communicationHistoryContainer.scrollHeight;
          	});
          }
    	
    	function selectedUser(user) {
    		if(user.selected === true) {
    			vm.selectedUsers.push(user.id);
    		} else if(user.selected === false) {
    			for(var i = 0; i < vm.selectedUsers.length; i++) {
    				if(vm.selectedUsers[i] == user.id) {
    					vm.selectedUsers.splice(i, 1);
    					break;
    				}
    			}
    		}
    	}
    	
    	function executionDateAfterToday(executionDateString) {
    		if(executionDateString == null) {
    			return false;
    		}
    		var executionDate = executionDateString instanceof Date ? executionDateString : dateUtilityService.convertToDateOrUndefined(executionDateString);
    		if(vm.todayDate.getTime() > executionDate.getTime()) {
    			return true;
    		}
    		return false;
    	}
    	
    	function showMessageReceiverOverview(communicationUserConnectionOfUserAndPage, confirmationNeeded) {
    		var messageId = communicationUserConnectionOfUserAndPage.communication.id;
    		communicationService.getMessageReceiverOverview(messageId).then(function successCallback(response) {
    			var communicationUserConnections = response.data;
    			messageReceiverOverviewModalService.showMessageReceiverOverviewModal(currentUser, communicationUserConnections, confirmationNeeded);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error communication.controller#showMessageReceiverOverview');
  	   		 });
    	}
    	
    	function communicationConfirmation(communicationUserConnectionOfUserAndPage) {
    		communicationService.updateCommunicationUserConnection(communicationUserConnectionOfUserAndPage).then(function successCallback(response) {
  	   		 }, function errorCallback(response) {
  	   			  console.log('error communication.controller#communicationConfirmation');
  	   		 });
    	}
    	
    	function communicationOrderExecuted(communicationUserConnectionOfUserAndPage) {
    		if(false && communicationUserConnectionOfUserAndPage.executed) {
    			communicationUserConnectionOfUserAndPage.executed = false;
    			vm.communicationUserConnectionOfUserAndPage = communicationUserConnectionOfUserAndPage;
    		} else {
	    		communicationService.updateCommunicationUserConnection(communicationUserConnectionOfUserAndPage).then(function successCallback(response) {
	  	   		 }, function errorCallback(response) {
	  	   			  console.log('error communication.controller#communicationOrderExecuted');
	  	   		 });
    		}
    	}
    	
    	function editCommunicationMessage(communicationUserConnectionOfUserAndPage) {
    		if(vm.messageEdit === false) {
    			var message = (typeof communicationUserConnectionOfUserAndPage.communication.title != 'undefined' && communicationUserConnectionOfUserAndPage.communication.title !== '') ? communicationUserConnectionOfUserAndPage.communication.title + '<br />' + communicationUserConnectionOfUserAndPage.communication.message : '' + communicationUserConnectionOfUserAndPage.communication.message;
    			var messageFormatted = message.replace(/<br \/>/g, '\n');
    			vm.message = messageFormatted;
    			vm.messageEdit = true;
    			vm.confirmation = communicationUserConnectionOfUserAndPage.confirmationNeeded;
    			vm.communicationUserConnectionOfUserAndPageEdit = communicationUserConnectionOfUserAndPage;
    			vm.executionDate = communicationUserConnectionOfUserAndPage.executionDate;
    			communicationUserConnectionOfUserAndPage.messageEdit = true;
    		} else {
    			vm.message = '';
    			vm.messageEdit = false;
    			communicationUserConnectionOfUserAndPage.messageEdit = false;
    		}
    	}
    	
    	function archiveCommunicationUserConnection(communicationUserConnectionOfUserAndPage) {
    		if(communicationUserConnectionOfUserAndPage.executed === false && communicationUserConnectionOfUserAndPage.executionDate != null) {
    			communicationUserConnectionOfUserAndPage.archivationNotPossible = true;
    			return;
    		}
    		communicationUserConnectionOfUserAndPage.archivationNotPossible = false;
    		communicationUserConnectionOfUserAndPage.archived = true;
    		communicationService.updateCommunicationUserConnection(communicationUserConnectionOfUserAndPage).then(function successCallback(response) {
    			removeCommunicationUserConnection(communicationUserConnectionOfUserAndPage);
    			removeSentAndOpenOrders(communicationUserConnectionOfUserAndPage);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error communication.controller#archiveCommunicationUserConnection');
  	   		 });
    	}
    	
    	function removeSentAndOpenOrders(communicationUserConnection) {
    		for(var i = 0; i < vm.sentAndOpenOrders.length; i++) {
    			if(vm.sentAndOpenOrders[i].id == communicationUserConnection.id) {
    				vm.sentAndOpenOrders.splice(i, 1);
    			}
    		}
    	}
    	
    	function removeCommunicationUserConnection(communicationUserConnection) {
    		for(var i = 0; i < vm.communicationUserConnectionsOfUserAndPage.length; i++) {
    			if(vm.communicationUserConnectionsOfUserAndPage[i].id == communicationUserConnection.id) {
    				vm.communicationUserConnectionsOfUserAndPage.splice(i, 1);
    				break;
    			}
    		}
    	}
    	
    	function addSelectedProject(project) {
    		vm.selectedProject = project;
    		removeAllProjectSelections(vm.projects);
    		vm.selectedProject.selected = true;
    	}
    	
    	function setSelectionOfSelectedProject(project) {
    		if(project == null) {
    			return;
    		}
    		for(var i = 0; i < vm.projects.length; i++) {
    			if(vm.projects[i].id == project.id) {
    				vm.projects[i].selected = true;
    				break;
    			}
    		}
    	}
    	
    	function removeSelectedProject(project) {
    		vm.selectedProject = null;
    		removeAllProjectSelections(vm.projects);
    	}
    	
    	function removeAllProjectSelections(projects) {
    		for(var i = 0; i < projects.length; i++) {
    			projects[i].selected = false;
    		}
    	}
    	
    	function showCommunicationGroupCreationModal(mode) {
    		communicationGroupCreationModalService.showCommunicationGroupCreationModal(mode, currentUser, null, null, vm.communicationGroupUserConnections);
    	}
    	
    	function createNewWorkingOrderEntry() {
    		if(vm.selectedProject != null) {
	    		projectUserConnectionService.findProjectUserConnectionByProject(vm.selectedProject.id).then(function successCallback(response) {
	    			var usersOfProject = response.data;
	    			communicationWorkingOrderCreationModalService.showCommunicationWorkingOrderCreationModal(vm, currentUser, 'CREATE', vm.selectedProject, vm.projects, usersOfProject, null);
	   	   		 }, function errorCallback(response) {
	   	   			  console.log('error communication.controller#createNewWorkingOrderEntry');
	   	   		 });
    		} else {
    			communicationWorkingOrderCreationModalService.showCommunicationWorkingOrderCreationModal(vm, currentUser, 'CREATE', vm.selectedProject, vm.projects, null, null);
    		}
    	}
    	
    	function addCommunicationUserConnectionToSentAndOpenOrders(communicationUserConnections) {
    		for(var i = 0; i < communicationUserConnections.length; i++) {
    			vm.sentAndOpenOrders.push(communicationUserConnections[i]);
    		}
    	}
    	
    	function updateCommunicationUserConnectionToSentAndOpenOrders(communicationUserConnections) {
    		communicationService.findSentAndOpenOrders(vm.currentUser.id, false).then(function successCallback(response) {
    			vm.sentAndOpenOrders = response.data;
    		}, function errorCallback(response) {
   	   			  console.log('error communication.controller#updateCommunicationUserConnectionToSentAndOpenOrders');
   	   		});
    	}
    	
    	function showYesNoArchiveCommunicationUserConnection(communicationUserConnection) {
    		yesNoCommunicationModalService.showYesNoModal(vm, communicationUserConnection);
    	}
    	
    	function editWorkingOrder(sentAndOpenOrder) {
    		var projectId = null;
    		if(vm.selectedProject != null) {
    			projectId = vm.selectedProject.id;
    		} else {
    			projectId = sentAndOpenOrder.project.id;
    		}
    		projectUserConnectionService.findProjectUserConnectionByProject(projectId).then(function successCallback(response) {
    			var usersOfProject = response.data;
    			communicationWorkingOrderCreationModalService.showCommunicationWorkingOrderCreationModal(vm, currentUser, 'EDIT', vm.selectedProject, vm.projects, usersOfProject, sentAndOpenOrder);
    		}, function errorCallback(response) {
   	   			  console.log('error communication.controller#editWorkingOrder');
   	   		});
    	}
    	
    	function showPrintMessageOrders(mode) {
    		if(mode == 'SENT') {
    			printMessageOrdersModalService.showPrintMessageOrdersModal(vm.sentAndOpenOrders, mode, currentUser);
    		} else if(mode == 'RECEIVED') {
    			printMessageOrdersModalService.showPrintMessageOrdersModal(vm.receivedAndOpenOrders, mode, currentUser);
    		}
    	}
    	
    	function showImagePreview(attachment, subUrl, workingBookEntryId, filename) {
    		var url = subUrl + workingBookEntryId + '/' + filename;
    		imgTemplateModalService.showImgTemplateModal(url, attachment.originalFilename, '--', '--');
    	}
    	
    	function showDocumentFilePreview(attachedFileUrl) {
    		imgTemplateModalService.showImgTemplateModal(attachedFileUrl.url, attachedFileUrl.fileName, '--', attachedFileUrl.version);
    	}
	} 
})();