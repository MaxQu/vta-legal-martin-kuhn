package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;

@Component
public class SystemImportContactMerge {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public Contact mergeContact(final Contact contact, final ContactImported mergedContactImported) throws BusinessException {
        BusinessAssert.notNull(contact, "contact is mandatory in SystemImportContactMerge#mergeContact", "400");
        BusinessAssert.notNull(mergedContactImported, "mergedContactImported is mandatory in SystemImportContactMerge#mergeContact", "400");
        logger.info("merge contact in SystemImportContactMerge#mergeContact");
        contact.setNewContact(mergedContactImported.isNewContact());
        if (mergedContactImported.isInstitutionChanged() || mergedContactImported.isNewContact()) {
            contact.setInstitution(mergedContactImported.getInstitution());
            contact.setInstitutionChanged(true);
        }
        if (mergedContactImported.isAdditionalNameInformationChanged() || mergedContactImported.isNewContact()) {
            contact.setAdditionalNameInformation(mergedContactImported.getAdditionalNameInformation());
            contact.setAdditionalNameInformationChanged(true);
        }
        if (mergedContactImported.isContactPersonChanged() || mergedContactImported.isNewContact()) {
            contact.setContactPerson(mergedContactImported.getContactPerson());
            contact.setContactPersonChanged(true);
        }
        if (mergedContactImported.isShortCodeChanged() || mergedContactImported.isNewContact()) {
            contact.setShortCode(mergedContactImported.getShortCode());
            contact.setShortCodeChanged(true);
        }
        if (mergedContactImported.isCustomerNumberContainersChanged() || mergedContactImported.isNewContact()) {
            final List<CustomerNumberContainer> copiedCustomerNumberContainers = new ArrayList<>();
            for (final CustomerNumberContainer customerNumberContainer : mergedContactImported.getCustomerNumberContainers()) {
                final CustomerNumberContainer copiedCustomerNumberContainer = customerNumberContainer.deepCopy();
                copiedCustomerNumberContainers.add(copiedCustomerNumberContainer);
            }
            contact.setCustomerNumberContainers(copiedCustomerNumberContainers);
            contact.setCustomerNumberContainersChanged(true);
        }
        if (mergedContactImported.isAgentNumberContainersChanged() || mergedContactImported.isNewContact()) {
            final List<AgentNumberContainer> copiedAgentNumberContainers = new ArrayList<>();
            if ((mergedContactImported.getAgentNumberContainers() == null || mergedContactImported.getAgentNumberContainers().isEmpty())
                && (contact.getAgentNumberContainers() != null & !contact.getAgentNumberContainers().isEmpty())) {
                contact.setAgentNumberContainers(copiedAgentNumberContainers);
                contact.setAgentNumberContainersChanged(true);
            } else {
                if (mergedContactImported.getAgentNumberContainers() != null && mergedContactImported.getAgentNumberContainers().size() > 0
                    && contact.getAgentNumberContainers() != null && contact.getAgentNumberContainers().size() > 0
                    && !mergedContactImported.getAgentNumberContainers().get(0).getCustomerNumber()
                        .equals(contact.getAgentNumberContainers().get(0).getCustomerNumber())) {
                    final AgentNumberContainer agentNumberContainer = mergedContactImported.getAgentNumberContainers().get(0).deepCopy();
                    copiedAgentNumberContainers.add(agentNumberContainer);
                    if (contact.getAgentNumberContainers().size() > 1) {
                        for (int i = 1; i < contact.getAgentNumberContainers().size(); i++) {
                            final AgentNumberContainer copiedAgentNumberContainer = contact.getAgentNumberContainers().get(i).deepCopy();
                            copiedAgentNumberContainers.add(copiedAgentNumberContainer);
                        }
                    }
                    contact.setAgentNumberContainers(copiedAgentNumberContainers);
                    contact.setAgentNumberContainersChanged(true);
                } else if (mergedContactImported.getAgentNumberContainers() != null
                    && !mergedContactImported.getAgentNumberContainers().isEmpty()
                    && (contact.getAgentNumberContainers() == null || contact.getAgentNumberContainers().isEmpty())) {
                    for (final AgentNumberContainer agentNumberContainer : mergedContactImported.getAgentNumberContainers()) {
                        final AgentNumberContainer copiedAgentNumberContainer = agentNumberContainer.deepCopy();
                        copiedAgentNumberContainers.add(copiedAgentNumberContainer);
                    }
                    contact.setAgentNumberContainers(copiedAgentNumberContainers);
                    contact.setAgentNumberContainersChanged(true);
                }
            }
        }
        if (mergedContactImported.isTelephonesChanged() || mergedContactImported.isNewContact()) {
            contact.setTelephones(mergedContactImported.getTelephones());
            contact.setTelephonesChanged(true);
        }
        if (mergedContactImported.isEmailsChanged() || mergedContactImported.isNewContact()) {
            contact.setEmails(mergedContactImported.getEmails());
            contact.setEmailsChanged(true);
        }
        if (mergedContactImported.isInformationChanged() || mergedContactImported.isNewContact()) {
            contact.setInformation(mergedContactImported.getInformation());
            contact.setInformationChanged(true);
        }

        // emails not imported

        // telephones not imported

        // Address
        final AddressImported addressImported = mergedContactImported.getAddress();

        setAddress(mergedContactImported.isNewContact(), contact.getAddress(), addressImported);

        // ContactImportedAddressImported
        if (mergedContactImported.isContactImportedAddressesImportedChanged() || mergedContactImported.isNewContact()) {
            final List<ContactImportedAddressImported> contactsImportedAddressesImported = mergedContactImported.getAddressesWithProducts();
            List<ContactAddressWithProducts> contactAddressesWithProducts = contact.getAddressesWithProducts();

            if ((contactAddressesWithProducts != null && contactsImportedAddressesImported == null)
                || (contactAddressesWithProducts == null && contactsImportedAddressesImported == null)) {
                contact.setAddressesWithProducts(new ArrayList<>());
            } else if (contactAddressesWithProducts != null
                && (contactsImportedAddressesImported != null
                    && contactAddressesWithProducts.size() > contactsImportedAddressesImported.size())
                || (contactAddressesWithProducts != null && contactsImportedAddressesImported == null)) {
                for (final Iterator<ContactAddressWithProducts> iterator = contactAddressesWithProducts.iterator(); iterator.hasNext();) {
                    final ContactAddressWithProducts contactAddressWithProducts = iterator.next();
                    final ContactImportedAddressImported contactImportedAddressImported = getContactImportedAddressImported(
                        contactAddressWithProducts, contactsImportedAddressesImported);
                    if (contactImportedAddressImported == null) {
                        iterator.remove(); // delete
                    } else {
                        setAddressWithProducts(mergedContactImported.isNewContact(), contactAddressWithProducts,
                            contactImportedAddressImported);
                    }
                    contact.setAddressesWithProductsChanged(true);
                }
            } else if ((contactAddressesWithProducts == null && contactsImportedAddressesImported != null)
                || (contactAddressesWithProducts != null && contactsImportedAddressesImported != null
                    && contactAddressesWithProducts.size() < contactsImportedAddressesImported.size())
                || contactAddressesWithProducts.size() == contactsImportedAddressesImported.size()) {
                for (int i = 0; i < contactsImportedAddressesImported.size(); i++) {
                    final ContactImportedAddressImported contactsImportedAddressImported = contactsImportedAddressesImported.get(i);
                    final ContactAddressWithProducts contactAddressWithProducts = getContactAddressWithProducts(
                        contactsImportedAddressImported, contactAddressesWithProducts, i);
                    if (contactAddressWithProducts == null) {
                        final ContactAddressWithProducts createdContactAddressWithProducts = createContactAddressWithProducts(
                            mergedContactImported.isNewContact(), contactsImportedAddressImported);
                        if (contact.getAddressesWithProducts() == null) {
                            contact.setAddressesWithProducts(new ArrayList<>());
                        }
                        setAddress(true, createdContactAddressWithProducts.getAddress(), contactsImportedAddressImported.getAddress());
                        contact.getAddressesWithProducts().add(createdContactAddressWithProducts); // create
                        contact.setAddressesWithProductsChanged(true);
                    } else {
                        setAddressWithProducts(mergedContactImported.isNewContact(), contactAddressWithProducts,
                            contactsImportedAddressImported);
                        contact.setAddressesWithProductsChanged(true);
                    }
                }
            }
        }
        contact.setCustomerSince(mergedContactImported.getCustomerSince());
        contact.setTenant(mergedContactImported.getTenant());
        return contact;
    }

    private void setAddressWithProducts(final boolean newContact, final ContactAddressWithProducts contactAddressWithProducts,
            final ContactImportedAddressImported contactsImportedAddressImported) {
        setAddress(newContact, contactAddressWithProducts.getAddress(), contactsImportedAddressImported.getAddress());
    }

    private Address setAddress(final boolean newContactOrAddress, final Address address, final AddressImported addressImported) {
        if (address == null || addressImported == null) {
            return null;
        }
        if (addressImported.isFlfIdChanged() || newContactOrAddress) {
            address.setFlfId(addressImported.getFlfId());
            address.setFlfIdChanged(true);
        }
        if (addressImported.isAdditionalInformationChanged() || newContactOrAddress) {
            address.setAdditionalInformation(addressImported.getAdditionalInformation());
            address.setAdditionalInformationChanged(true);
        }
        if (addressImported.isCustomerNumberContainersChanged() || newContactOrAddress) {
            final List<CustomerNumberContainer> copiedCustomerNumberContainers = new ArrayList<>();
            if (addressImported.getCustomerNumberContainers() != null) {
                for (final CustomerNumberContainer customerNumberContainer : addressImported.getCustomerNumberContainers()) {
                    final CustomerNumberContainer copiedCustomerNumberContainer = customerNumberContainer.deepCopy();
                    copiedCustomerNumberContainers.add(copiedCustomerNumberContainer);
                }
            }
            address.setCustomerNumberContainers(copiedCustomerNumberContainers);
            address.setCustomerNumberContainersChanged(true);
        }
        if (addressImported.isStreetChanged() || newContactOrAddress) {
            address.setStreet(addressImported.getStreet());
            address.setStreetChanged(true);
        }
        if (addressImported.isIntSignChanged() || newContactOrAddress) {
            address.setIntSign(addressImported.getIntSign());
            address.setIntSignChanged(true);
        }
        if (addressImported.isPostalCodeChanged() || newContactOrAddress) {
            address.setPostalCode(addressImported.getPostalCode());
            address.setPostalCodeChanged(true);
        }
        if (addressImported.isProvinceTypeChanged() || newContactOrAddress) {
            address.setProvinceType(addressImported.getProvinceType());
            address.setProvinceTypeChanged(true);
        }
        if (addressImported.isRegionChanged() || newContactOrAddress) {
            address.setRegion(addressImported.getRegion());
            address.setRegionChanged(true);
        }
        if (addressImported.isCountryChanged() || newContactOrAddress) {
            address.setCountry(addressImported.getCountry() != null ? addressImported.getCountry().toString() : null);
            address.setCountryChanged(true);
        }
        if (addressImported.isTelephoneChanged() || newContactOrAddress) {
            address.setTelephone(addressImported.getTelephone());
            address.setTelephoneChanged(true);
        }
        if (addressImported.isEmailChanged() || newContactOrAddress) {
            address.setEmail(addressImported.getEmail());
            address.setEmailChanged(true);
        }
        if (addressImported.isInformationChanged() || newContactOrAddress) {
            address.setInformation(addressImported.getInformation());
            address.setInformationChanged(true);
        }
        return address;
    }

    private ContactAddressWithProducts createContactAddressWithProducts(final boolean newContact,
            final ContactImportedAddressImported contactsImportedAddressImported) {
        final ContactAddressWithProducts contactAddressWithProducts = new ContactAddressWithProducts();
        final Address address = new Address();
        setAddress(newContact, address, contactsImportedAddressImported.getAddress());
        contactAddressWithProducts.setAddress(address);
        return contactAddressWithProducts;
    }

    // imported contactImportedAddressImported -> equal with ContactAddressWithProducts without products
    private ContactImportedAddressImported getContactImportedAddressImported(final ContactAddressWithProducts contactAddressWithProducts,
            final List<ContactImportedAddressImported> contactsImportedAddressesImported) {
        logger.info("get contactImported in SystemImportContactMerge#getContactImportedAddressImported");
        if (contactsImportedAddressesImported != null) {
            for (final ContactImportedAddressImported contactImportedAddressImported : contactsImportedAddressesImported) {
                if (contactImportedAddressImported.getAddress().getFlfId().equals(contactAddressWithProducts.getAddress().getFlfId())) {
                    return contactImportedAddressImported;
                }
            }
        }
        return null;
    }

    private ContactAddressWithProducts getContactAddressWithProducts(final ContactImportedAddressImported contactImportedAddressImported,
            final List<ContactAddressWithProducts> contactAddressesWithProducts, final int index) {
        logger.info("get contactAddressWithProducts in SystemImportContactMerge#getContactAddressWithProducts");
        if (contactAddressesWithProducts != null) {
            for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
                if (contactImportedAddressImported.getAddress().getFlfId().equals(contactAddressWithProducts.getAddress().getFlfId())) {
                    return contactAddressWithProducts;
                }
            }
        }
        return null;
    }
}
