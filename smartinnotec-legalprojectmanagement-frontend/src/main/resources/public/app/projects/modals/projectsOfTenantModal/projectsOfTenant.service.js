(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectsOfTenantModalService', projectsOfTenantModalService);
        
    projectsOfTenantModalService.$inject = ['$modal', '$stateParams'];
    
    function projectsOfTenantModalService($modal, $stateParams) {
		var service = {
			showProjectsOfTenantModal: showProjectsOfTenantModal
		};		
		return service;
		
		////////////
				
		function showProjectsOfTenantModal(projects) {
			 var projectsOfTenantModal = $modal.open({
				controller: ProjectsOfTenantModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	projects: function() {
			    		return projects;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/projectsOfTenantModal/projectsOfTenant.modal.html'
			});
			return projectsOfTenantModal;
			
			function ProjectsOfTenantModalController($modalInstance, $scope, projects) {
		    	var vm = this; 
		    	vm.projects = projects;

		    	vm.closeProjectsOfTenantModal = closeProjectsOfTenantModal;
		    	
		    	////////////
		    	
		    	function closeProjectsOfTenantModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();