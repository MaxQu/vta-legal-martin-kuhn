(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('printMessageOrdersModalService', printMessageOrdersModalService);
        
    printMessageOrdersModalService.$inject = ['$modal', '$stateParams', 'dateUtilityService'];
    
    function printMessageOrdersModalService($modal, $stateParams, dateUtilityService) {
		var service = {
			showPrintMessageOrdersModal: showPrintMessageOrdersModal
		};		
		return service;
		
		////////////
				
		function showPrintMessageOrdersModal(workingOrders, mode, currentUser) {
			 var printMessageOrdersModal = $modal.open({
				controller: PrintMessageOrdersModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    size: 'lg',
			    resolve: {
			    	workingOrders: function() {
			    		return workingOrders;
			    	},
			    	mode: function() {
			    		return mode;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/printMessageOrders/printMessageOrders.modal.html'
			});
			return printMessageOrdersModal;
			
			function PrintMessageOrdersModalController($modalInstance, $scope, workingOrders, mode, currentUser) {
		    	var vm = this; 
		    	vm.today = dateUtilityService.formatDateToDateTimeString(new Date());
		    	vm.workingOrders = workingOrders;
		    	vm.mode = mode;
		    	vm.currentUser = currentUser;
		    	vm.orderType = 'executionDate';
		    	
		    	vm.closePrintMessageOrdersModal = closePrintMessageOrdersModal;
		    	
		    	function closePrintMessageOrdersModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
