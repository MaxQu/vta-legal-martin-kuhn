package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Thinning;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface ThinningService {
	
	Integer count() throws BusinessException;

    Thinning findThinningById(final String id) throws BusinessException;

    List<Thinning> findByProductId(final String productId) throws BusinessException;

    List<Thinning> findByProductIdAndDateBetween(final String productId, final DateTime start, final DateTime end) throws BusinessException;

    Thinning findByProductIdOrderByDateDesc(final User user, final String productId) throws BusinessException;

    Long countByProductId(final String productId) throws BusinessException;

    Thinning create(final Thinning thinning) throws BusinessException;

    Thinning update(final Thinning thinning) throws BusinessException;

    void delete(final String thinningId) throws BusinessException;

    List<Thinning> getThinningsInReverseOrder(final List<Thinning> thinnings) throws BusinessException;

}
