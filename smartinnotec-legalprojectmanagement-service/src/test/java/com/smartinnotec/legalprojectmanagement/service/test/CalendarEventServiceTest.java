package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class CalendarEventServiceTest extends AbstractServiceTest {

    @Test
    public void shouldDeactivateCalendarEvent() throws BusinessException {
        final User user1 = new User();
        user1.setFirstname("test1");
        user1.setSurname("user1");
        final User createdUser1 = userService.createUser(user1);
        Assert.assertNotNull(createdUser1);

        final User user2 = new User();
        user2.setFirstname("test2");
        user2.setSurname("user2");
        final User createdUser2 = userService.createUser(user2);
        Assert.assertNotNull(createdUser2);

        // CalendarEvent
        final CalendarEvent calendarEvent1 = new CalendarEvent();
        calendarEvent1.setTitle("CalendarEvent1");
        calendarEvent1.setLocation("CalendarEvent1 Location");
        calendarEvent1.setStartsAt(formatter.parseMillis("18.06.2018 08:00"));
        calendarEvent1.setEndsAt(formatter.parseMillis("18.06.2018 10:00"));
        calendarEvent1.setCalendarEventTyp(CalendarEventTypeEnum.OUTSIDE_WORK);
        final List<CalendarEvent> createdCalendarEvents = calendarEventService.create(calendarEvent1, createdUser1, false, null, null);
        Assert.assertNotNull(createdCalendarEvents);
        Assert.assertEquals(1, createdCalendarEvents.size());

        // CalendarEventUserConnections
        final CalendarEventUserConnection calendarEventUserConnection1 = new CalendarEventUserConnection();
        calendarEventUserConnection1.setUser(createdUser1);
        calendarEventUserConnection1.setCalendarEvent(calendarEvent1);
    }

    @Test
    public void shouldShiftCalendarEvent() throws BusinessException {
        final User user1 = new User();
        user1.setFirstname("test1");
        user1.setSurname("user1");
        final User createdUser1 = userService.createUser(user1);
        Assert.assertNotNull(createdUser1);

        final User user2 = new User();
        user2.setFirstname("test1");
        user2.setSurname("user1");
        final User createdUser2 = userService.createUser(user2);
        Assert.assertNotNull(createdUser2);

        final CalendarEvent calendarEvent1 = new CalendarEvent();
        calendarEvent1.setTitle("CalendarEvent1");
        calendarEvent1.setLocation("CalendarEvent1 Location");
        calendarEvent1.setStartsAt(formatter.parseMillis("18.06.2018 08:00"));
        calendarEvent1.setEndsAt(formatter.parseMillis("18.06.2018 11:00"));
        calendarEvent1.setCalendarEventTyp(CalendarEventTypeEnum.OUTSIDE_WORK);

        // CalendarEvent
        final List<CalendarEvent> createdCalendarEvents = calendarEventService.create(calendarEvent1, createdUser1, false, null, null);
        Assert.assertNotNull(createdCalendarEvents);
        Assert.assertEquals(1, createdCalendarEvents.size());

        // CalendarEventUserConnections
        final CalendarEventUserConnection calendarEventUserConnection1 = new CalendarEventUserConnection();
        calendarEventUserConnection1.setUser(user1);
        calendarEventUserConnection1.setCalendarEvent(calendarEvent1);
    }
}
