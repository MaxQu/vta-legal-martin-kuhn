(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoContactModalService', yesNoContactModalService);
        
    yesNoContactModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoContactModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, contact, productsReferencedToContact, calendarEventUserConnections) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contact: function() {
			    		return contact;
			    	},
			    	productsReferencedToContact: function() {
			    		return productsReferencedToContact;
			    	}, 
			    	calendarEventUserConnections: function() {
			    		return calendarEventUserConnections;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, contact, productsReferencedToContact, calendarEventUserConnections) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contact = contact;
		    	vm.productsReferencedToContact = productsReferencedToContact;
		    	vm.calendarEventUserConnections = calendarEventUserConnections;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.deleteContact(vm.contact);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();