package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.Receipe;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeProduct;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ReceipeRepository;

@Service("receipeService")
public class ReceipeServiceImpl extends AbstractService implements ReceipeService {

    @Autowired
    private ReceipeRepository receipeRepository;
    @Autowired
    private DocumentFileService documentFileService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;

    @Override
    public Integer countByReceipeType(final ReceipeTypeEnum receipeType) throws BusinessException {
        logger.info("count receipes by receipeType '{}' in ReceipeServiceImpl#countByReceipeType", receipeType);
        final Long receipes = receipeRepository.countByReceipeType(receipeType);
        return receipes.intValue();
    }

    @Override
    public Receipe create(final Receipe receipe) throws BusinessException {
        BusinessAssert.notNull(receipe, "receipe is mandatory in ReceipeServiceImpl#create", "400");
        logger.info("create receipe in ReceipeServiceImpl#create");
        return receipeRepository.insert(receipe);
    }

    @Override
    public Receipe update(final Receipe receipe) throws BusinessException {
        BusinessAssert.notNull(receipe, "receipe is mandatory in ReceipeServiceImpl#update", "400");
        logger.info("update receipe with id '{}' in ReceipeServiceImpl#update", receipe.getId());
        return receipeRepository.save(receipe);
    }

    @Override
    public void delete(final String receipeId) throws BusinessException {
        BusinessAssert.notNull(receipeId, "receipeId is mandatory in ReceipeServiceImpl#delete", "400");
        BusinessAssert.isId(receipeId, "receipeId is mandatory in ReceipeServiceImpl#delete", "400");
        logger.info("delete receipe with id '{}' in ReceipeServiceImpl#delete", receipeId);
        receipeRepository.delete(receipeId);
    }

    @Override
    public List<ReceipeAttachment> getAddedReceipeAttachments(final List<ReceipeAttachment> currentReceipeAttachments,
            final List<ReceipeAttachment> newReceipeAttachments) {
        logger.info("get added receipeAttachments in ReceipeServiceImpl#getAddedReceipeAttachments");
        final List<ReceipeAttachment> addedReceipeAttachments = new ArrayList<>();
        if ((currentReceipeAttachments == null || currentReceipeAttachments.isEmpty()) && newReceipeAttachments != null
            && !newReceipeAttachments.isEmpty()) {
            return newReceipeAttachments;
        } else if (currentReceipeAttachments != null && !currentReceipeAttachments.isEmpty()
            && (newReceipeAttachments == null || newReceipeAttachments.isEmpty())) {
            return addedReceipeAttachments;
        } else if ((currentReceipeAttachments == null || currentReceipeAttachments.isEmpty())
            && (newReceipeAttachments == null || newReceipeAttachments.isEmpty())) {
            return addedReceipeAttachments;
        }
        for (final ReceipeAttachment newReceipeAttachment : newReceipeAttachments) {
            if (!currentReceipeAttachments.contains(newReceipeAttachment)) {
                addedReceipeAttachments.add(newReceipeAttachment);
            }
        }
        return addedReceipeAttachments;
    }

    @Override
    public List<ReceipeAttachment> getRemovedReceipeAttachments(final List<ReceipeAttachment> currentReceipeAttachments,
            final List<ReceipeAttachment> newReceipeAttachments) {
        logger.info("get removed receipeAttachments in ReceipeServiceImpl#getRemovedReceipeAttachments");
        final List<ReceipeAttachment> removedReceipeAttachments = new ArrayList<>();
        if ((currentReceipeAttachments != null && !currentReceipeAttachments.isEmpty())
            && (newReceipeAttachments == null || newReceipeAttachments.isEmpty())) {
            return currentReceipeAttachments;
        } else if ((currentReceipeAttachments == null || currentReceipeAttachments.isEmpty())
            && (newReceipeAttachments != null && !newReceipeAttachments.isEmpty())) {
            return removedReceipeAttachments;
        } else if ((currentReceipeAttachments == null || currentReceipeAttachments.isEmpty())
            && (newReceipeAttachments == null || newReceipeAttachments.isEmpty())) {
            return removedReceipeAttachments;
        }
        for (final ReceipeAttachment currentReceipeAttachment : currentReceipeAttachments) {
            if (!newReceipeAttachments.contains(currentReceipeAttachment)) {
                removedReceipeAttachments.add(currentReceipeAttachment);
            }
        }
        return removedReceipeAttachments;
    }

    @Override
    public Receipe findReceipeById(final String receipeId) throws BusinessException {
        BusinessAssert.notNull(receipeId, "receipeId is mandatory in ReceipeServiceImpl#findReceipeById", "400");
        BusinessAssert.isId(receipeId, "receipeId is mandatory in ReceipeServiceImpl#findReceipeById", "400");
        logger.info("find receipe with id '{}' in ReceipeServiceImpl#findReceipeById", receipeId);
        final Receipe receipe = receipeRepository.findOne(receipeId);
        return receipe;
    }

    @Override
    public List<Receipe> findByProductIdAndReceipeType(final User user, final String productId, final ReceipeTypeEnum receipeType)
            throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ReceipeServiceImpl#findByProductIdAndReceipeType", "400");
        BusinessAssert.isId(productId, "productId must be an id in ReceipeServiceImpl#findByProductIdAndReceipeType", "400");
        BusinessAssert.notNull(receipeType, "receipeType is mandatory in ReceipeServiceImpl#findByProductIdAndReceipeType", "400");
        logger.info("find receipes by product with id '{}' and receiveType '{}' in ReceipeServiceImpl#findByProductIdAndReceipeType",
            productId, receipeType);
        final List<Receipe> receipes = receipeRepository.findByProductIdAndReceipeType(productId, receipeType);
        final boolean canSeeConfidentDocumentFiles = projectUserConnectionService
            .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());
        if (receipes != null) {
            for (final Receipe receipe : receipes) {
                removeAttachmentsConcerningPermissions(receipe, user, canSeeConfidentDocumentFiles);
            }
        }
        return receipes;
    }

    @Override
    public List<Receipe> findByProductIdAndReceipeTypeAndDateBetween(final User user, final String productId,
            final ReceipeTypeEnum receipeType, final DateTime start, final DateTime end)
            throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ReceipeServiceImpl#findByProductIdAndReceipeTypeAndDateBetween",
            "400");
        BusinessAssert.isId(productId, "productId must be an id in ReceipeServiceImpl#findByProductIdAndReceipeTypeAndDateBetween", "400");
        BusinessAssert.notNull(receipeType, "receipeType is mandatory in ReceipeServiceImpl#findByProductIdAndReceipeTypeAndDateBetween",
            "400");
        BusinessAssert.notNull(start, "start is mandatory in ReceipeServiceImpl#findByProductIdAndReceipeTypeAndDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in ReceipeServiceImpl#findByProductIdAndReceipeTypeAndDateBetween", "400");
        logger.info(
            "find receipes of product with id '{}', receipeType '{}' and between '{}' and '{}' in ReceipeServiceImpl#findByProductIdAndReceipeTypeAndDateBetween",
            productId, receipeType, start, end);
        final List<Receipe> receipes = receipeRepository.findByProductIdAndReceipeTypeAndDateBetween(productId, receipeType, start, end);
        final boolean canSeeConfidentDocumentFiles = projectUserConnectionService
            .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());
        for (final Receipe receipe : receipes) {
            removeAttachmentsConcerningPermissions(receipe, user, canSeeConfidentDocumentFiles);
        }
        return receipes;
    }

    @Override
    public List<Receipe> findSameReceipes(final Receipe receipeToSearch) throws BusinessException {
        BusinessAssert.notNull(receipeToSearch, "receipeToSearch is mandatory in ReceipeServiceImpl#findSameReceipes", "400");
        logger.info("find same receipe in ReceipeServiceImpl#findSameReceipes");
        final List<Receipe> receipes = receipeRepository.findAll();

        final List<Receipe> foundedReceipes = new ArrayList<>();
        for (final Receipe receipe : receipes) {
            if (receipe.getReceipeProducts().size() != receipeToSearch.getReceipeProducts().size()) {
                continue;
            }
            final List<ReceipeProduct> receipeProducts = receipe.getReceipeProducts();
            final List<ReceipeProduct> receipeProductsToSearch = receipeToSearch.getReceipeProducts();

            boolean receipeIsEqual = true;
            for (final ReceipeProduct receipeProduct : receipeProducts) {
                boolean receipeProductFound = false;
                for (final ReceipeProduct receipeProductToSearch : receipeProductsToSearch) {
                    if (receipeProduct.getProduct().getId().equals(receipeProductToSearch.getProduct().getId())
                        && receipeProduct.getPercentage().floatValue() == receipeProductToSearch.getPercentage().floatValue()) {
                        receipeProductFound = true;
                    }
                }
                if (receipeProductFound == false) {
                    receipeIsEqual = false;
                    break;
                }
            }

            if (receipeIsEqual) {
                foundedReceipes.add(receipe);
            }
        }
        return foundedReceipes;
    }

    @Override
    public List<Receipe> findByReceipeType(final ReceipeTypeEnum receipeType) throws BusinessException {
        BusinessAssert.notNull(receipeType, "receipeType is mandatory in ReceipeServiceImpl#findByReceipeType", "400");
        logger.info("find receipes by receipeType '{}' in ReceipeServiceImpl#findByReceipeType", receipeType);
        return receipeRepository.findByReceipeType(receipeType);
    }

    @Override
    public List<Receipe> findByReceipeAttachmentsNotNull() throws BusinessException {
        logger.info("find recipes where attachments not null in ReceipeServiceImpl#findByReceipeAttachmentsNotNull");
        return receipeRepository.findByReceipeAttachmentsNotNull();
    }

    private void removeAttachmentsConcerningPermissions(final Receipe receipe, final User user, final boolean canSeeConfidentDocumentFiles)
            throws BusinessException {
        if (receipe == null) {
            return;
        }
        final List<ReceipeAttachment> receipeAttachments = receipe.getReceipeAttachments();
        if (receipeAttachments != null && !receipeAttachments.isEmpty()) {
            for (final Iterator<ReceipeAttachment> iterator = receipeAttachments.iterator(); iterator.hasNext();) {
                final ReceipeAttachment receipeAttachment = iterator.next();
                final String documentFileId = receipeAttachment.getDocumentFileId();
                final DocumentFile documentFile = documentFileService.findById(documentFileId);
                if (documentFile == null) {
                    continue;
                }
                final String projectProductId = documentFile.getProjectProductId();
                // can not see confident documentFiles
                if (canSeeConfidentDocumentFiles == false && documentFile.isConfident()) {
                    iterator.remove();
                    continue;
                }
                // if user is black listed for documentFile
                if (documentFile.getUserIdBlackList().contains(user.getId())) {
                    iterator.remove();
                    continue;
                }
                if (productService.checkIfProductExists(projectProductId)) {
                    // if role is black listed
                    if (documentFile.getRolesBlackList() != null
                        && documentFile.getRolesBlackList().contains(user.getProjectUserConnectionRole())) {
                        iterator.remove();
                        continue;
                    }
                }
            }
        }
    }

    @Override
    public Receipe findByProductIdAndReceipeTypeOrderByDateDesc(final User user, final String productId, final ReceipeTypeEnum receipeType)
            throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ReceipeServiceImpl#findByProductIdAndReceipeTypeOrderByDateDesc",
            "400");
        BusinessAssert.isId(productId, "productId must be an id in ReceipeServiceImpl#findByProductIdAndReceipeTypeOrderByDateDesc", "400");
        BusinessAssert.notNull(receipeType, "receipeType is mandatory in ReceipeServiceImpl#findByProductIdAndReceipeTypeOrderByDateDesc",
            "400");
        logger.info(
            "find last receipe by productId '{}' and receipeType '{}' in ReceipeServiceImpl#findByProductIdAndReceipeTypeOrderByDateDesc",
            productId, receipeType);
        final Receipe receipe = receipeRepository.findByProductIdAndReceipeTypeOrderByDateDesc(productId, ReceipeTypeEnum.STANDARD_MIXTURE);
        final boolean canSeeConfidentDocumentFiles = projectUserConnectionService
            .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());
        removeAttachmentsConcerningPermissions(receipe, user, canSeeConfidentDocumentFiles);
        return receipe;
    }

    @Override
    public Long countByProductIdAndReceipeType(final String productId, final ReceipeTypeEnum receipeType) throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ReceipeServiceImpl#countByProductIdAndReceipeType", "400");
        BusinessAssert.isId(productId, "productId must be an id in ReceipeServiceImpl#countByProductIdAndReceipeType", "400");
        BusinessAssert.notNull(receipeType, "receipeType is mandatory in ReceipeServiceImpl#countByProductIdAndReceipeType", "400");
        logger.info("count by productId '{}' and receipeType '{}' in ReceipeServiceImpl#countByProductIdAndReceipeType", productId,
            receipeType);
        return receipeRepository.countByProductIdAndReceipeType(productId, receipeType);
    }

    @Override
    public List<Receipe> getReceipesInReverseOrder(final List<Receipe> receipes) throws BusinessException {
        logger.info("get receipes in reverse order in ReceipeServiceImpl#getReceipesInReverseOrder");
        if (receipes != null && !receipes.isEmpty()) {
            Comparator<Receipe> cmp = Collections.reverseOrder();
            Collections.sort(receipes, cmp);
        }
        return receipes;
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all receipes in ReceipeServiceImpl#deleteAll");
        receipeRepository.deleteAll();
    }

    @Override
    public String toString() {
        return "[ReceipeServiceImpl]";
    }
}
