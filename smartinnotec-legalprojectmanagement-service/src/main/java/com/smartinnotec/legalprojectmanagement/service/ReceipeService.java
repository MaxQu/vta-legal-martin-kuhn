package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Receipe;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface ReceipeService {

    Integer countByReceipeType(ReceipeTypeEnum receipeType) throws BusinessException;

    Receipe create(final Receipe receipe) throws BusinessException;

    Receipe update(final Receipe receipe) throws BusinessException;

    void delete(final String receipeId) throws BusinessException;

    List<ReceipeAttachment> getRemovedReceipeAttachments(final List<ReceipeAttachment> currentReceipeAttachments,
            final List<ReceipeAttachment> newReceipeAttachments);

    List<ReceipeAttachment> getAddedReceipeAttachments(final List<ReceipeAttachment> currentReceipeAttachments,
            final List<ReceipeAttachment> newReceipeAttachments);

    Receipe findReceipeById(final String receipeId) throws BusinessException;

    List<Receipe> findByProductIdAndReceipeType(final User user, final String productId, final ReceipeTypeEnum receipeType)
            throws BusinessException;

    List<Receipe> findSameReceipes(final Receipe receipeToSearch) throws BusinessException;

    List<Receipe> findByProductIdAndReceipeTypeAndDateBetween(final User user, final String productId, final ReceipeTypeEnum receipeType,
            final DateTime start, final DateTime end)
            throws BusinessException;

    List<Receipe> findByReceipeType(final ReceipeTypeEnum receipeType) throws BusinessException;

    Receipe findByProductIdAndReceipeTypeOrderByDateDesc(final User user, final String productId, final ReceipeTypeEnum receipeType)
            throws BusinessException;

    List<Receipe> findByReceipeAttachmentsNotNull() throws BusinessException;

    Long countByProductIdAndReceipeType(final String productId, final ReceipeTypeEnum receipeType) throws BusinessException;

    List<Receipe> getReceipesInReverseOrder(final List<Receipe> receipes) throws BusinessException;

    void deleteAll() throws BusinessException;
}
