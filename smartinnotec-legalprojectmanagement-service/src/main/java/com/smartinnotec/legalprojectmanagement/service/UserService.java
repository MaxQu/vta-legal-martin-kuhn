package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface UserService {

    User findUser(final String id) throws BusinessException;

    Tenant getTenantOfUser(final String userId) throws BusinessException;

    List<User> findAllUsers() throws BusinessException;

    List<User> findAllUsersByTenant(final Tenant tenant) throws BusinessException;

    List<User> findUserBySearchString(final String searchString) throws BusinessException;

    User createUser(final User user) throws BusinessException;

    User updateUser(final User user) throws BusinessException;

    void deleteUser(final String id) throws BusinessException;

    void deleteAllUsers() throws BusinessException;

    User findUserByUsername(final String username) throws BusinessException;

    User findUserByEmail(final String emailAddress) throws BusinessException;

    User findUserByUsernameOrEmail(final String usernameOrEmail) throws BusinessException;

    User checkIfUserExistByUsername(final String username) throws BusinessException;

    void removeSuperAdmin(final List<User> users) throws BusinessException;

    List<User> findUsersByProjectUserConnectionRoles(final List<ProjectUserConnectionRoleEnum> projectUserConnectionRoles)
            throws BusinessException;

}
