package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroup;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroupUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface CommunicationGroupUserConnectionRepository extends MongoRepository<CommunicationGroupUserConnection, String> {

    List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionByUser(final User user);

    List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionByCommunicationGroup(final CommunicationGroup messageGroup);

    List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionsByUser(final User user);

}
