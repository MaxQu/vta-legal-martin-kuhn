(function() {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .config(configure);

    configure.$inject = ['$stateProvider'];

    function configure($stateProvider) {

        $stateProvider
            .state(getFuelState());

        ////////////

        function getFuelState() {
            var state = {
                name: 'auth.fuel',
                url: '/fuel/:userId',
                templateUrl: 'app/fuel/fuel/fuel.html',
                controller: 'FuelController',
                controllerAs: 'vm',
                resolve: {
                    fuelService: 'fuelService',
                    fuelList: function (fuelService) {
                        return fuelService.findPagedFuels(0);
                    },
                    amountOfPages: function (fuelService) {
                        return fuelService.calculateAmountOfPages();
                    },
                    fuelPageSize: function (fuelService) {
                        return fuelService.getFuelPageSize();
                    }
                }
            };
            return state;
        }
    }
})();
