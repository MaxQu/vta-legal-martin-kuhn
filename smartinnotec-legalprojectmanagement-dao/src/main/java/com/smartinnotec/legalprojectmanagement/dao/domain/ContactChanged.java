package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "ContactChanged")
public @Data class ContactChanged {

    @Id
    private String id;
    private boolean newContact;
    private String contactId;
    private String institution;
    private boolean institutionChanged;
    @TextIndexed
    private String additionalNameInformation;
    private boolean additionalNameInformationChanged;
    @TextIndexed
    private String contactPerson;
    private boolean contactPersonChanged;
    private String shortCode;
    private boolean shortCodeChanged;
    @TextIndexed
    private List<CustomerNumberContainer> customerNumberContainers; // Kundennummern
    private boolean customerNumberContainersChanged;
    @TextIndexed
    private List<AgentNumberContainer> agentNumberContainers; // Vertreternummern
    private boolean agentNumberContainersChanged;
    private String information;
    private boolean informationChanged;
    @TextIndexed
    private List<String> emails;
    private boolean emailsChanged;
    @TextIndexed
    private List<String> telephones;
    private boolean telephonesChanged;
    @DBRef
    protected AddressChanged address;
    @TextIndexed
    private List<ContactChangedAddressChanged> addressesWithProducts;
    private boolean contactImportedAddressesImportedChanged;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime customerSince;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime importDateTime;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime imported;
    @DBRef
    private Tenant tenant;
    private boolean committed;
    private ContactChangedTypeEnum contactChangedType;

    public ContactChanged() {
        this.newContact = false;
        this.committed = false;
    }

    public void addEmails(final String email) {
        if (emails == null) {
            emails = new ArrayList<>();
        }
        emails.add(email);
    }

    public void addTelephones(final String telephone) {
        if (telephones == null) {
            telephones = new ArrayList<>();
        }
        telephones.add(telephone);
    }

    public void addCustomerNumberContainer(final CustomerNumberContainer customerNumberContainer) {
        if (customerNumberContainers == null) {
            customerNumberContainers = new ArrayList<>();
        }
        customerNumberContainers.add(customerNumberContainer);
    }

    public void addAgentNumberContainer(final AgentNumberContainer agentNumberContainer) {
        if (agentNumberContainers == null) {
            agentNumberContainers = new ArrayList<>();
        }
        agentNumberContainers.add(agentNumberContainer);
    }

    public void addAddressesWithProducts(final ContactChangedAddressChanged contactChangedAddressChanged) {
        if (this.addressesWithProducts == null) {
            this.addressesWithProducts = new ArrayList<>();
        }
        this.addressesWithProducts.add(contactChangedAddressChanged);
    }
}
