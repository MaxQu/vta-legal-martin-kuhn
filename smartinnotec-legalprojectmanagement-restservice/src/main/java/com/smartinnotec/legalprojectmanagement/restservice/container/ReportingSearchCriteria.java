package com.smartinnotec.legalprojectmanagement.restservice.container;

import lombok.Data;

public @Data class ReportingSearchCriteria {

	private String userId;
	private String calendarEventId;
	private String contactId;
	
	public boolean isOnlyUserIdSet() {
		return userId != null && calendarEventId == null && contactId == null;
	}
	
	public boolean isOnlyCalendarEventIdSet() {
		return userId == null && calendarEventId != null && contactId == null;
	}
	
	public boolean isOnlyContactIdSet() {
		return userId == null && calendarEventId == null && contactId != null;
	}
	
	public boolean isContactIdAndUserIdSet() {
		return userId != null && calendarEventId == null && contactId != null;
	}
}
