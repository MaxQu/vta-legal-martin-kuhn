(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactsPreviousImportedService', contactsPreviousImportedService);
        
    contactsPreviousImportedService.$inject = ['$http', 'api_config'];
    
    function contactsPreviousImportedService($http, api_config) {
		var service = {
			getAmountOfContactsPreviousImported: getAmountOfContactsPreviousImported
		};
		
		return service;
		
		////////////
		
		function getAmountOfContactsPreviousImported() {
			return $http.get(api_config.BASE_URL + '/contactspreviousimported/contactpreviousimported/amount');
		}
    }
})();
