package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ProvinceTypeEnum {

    EMPTY_PROVINCE("EMPTY_PROVINCE", null),
    BURGENLAND("BURGENLAND", CountryTypeEnum.AUSTRIA),
    KAERNTEN("KAERNTEN", CountryTypeEnum.AUSTRIA),
    NIEDEROESTERREICH("NIEDEROESTERREICH", CountryTypeEnum.AUSTRIA),
    OBEROESTERREICH("OBEROESTERREICH", CountryTypeEnum.AUSTRIA),
    SALZBURG("SALZBURG", CountryTypeEnum.AUSTRIA),
    STEIERMARK("STEIERMARK", CountryTypeEnum.AUSTRIA),
    TIROL("TIROL", CountryTypeEnum.AUSTRIA),
    VORARLBERG("VORARLBERG", CountryTypeEnum.AUSTRIA),
    WIEN("WIEN", CountryTypeEnum.AUSTRIA),

    BADEN_WUERTTEMBERG("BADEN_WUERTTEMBERG", CountryTypeEnum.GERMANY),
    BAYERN("BAYERN", CountryTypeEnum.GERMANY),
    BERLIN("BERLIN", CountryTypeEnum.GERMANY),
    BRANDENBURG("BRANDENBURG", CountryTypeEnum.GERMANY),
    BREMEN("BREMEN", CountryTypeEnum.GERMANY),
    HAMBURG("HAMBURG", CountryTypeEnum.GERMANY),
    HESSEN("HESSEN", CountryTypeEnum.GERMANY),
    MECKLENBURG_VORPOMMEN("MECKLENBURG_VORPOMMEN", CountryTypeEnum.GERMANY),
    NIEDERSACHSEN("NIEDERSACHSEN", CountryTypeEnum.GERMANY),
    NORDREIN_WESTFALEN("NORDREIN_WESTFALEN", CountryTypeEnum.GERMANY),
    RHEINLAND_PFALZ("RHEINLAND_PFALZ", CountryTypeEnum.GERMANY),
    SAALLAND("SAALLAND", CountryTypeEnum.GERMANY),
    SACHSEN("SACHSEN", CountryTypeEnum.GERMANY),
    SACHSEN_ANHALT("SACHSEN_ANHALT", CountryTypeEnum.GERMANY),
    SCHLESWIG_HOLSTEIN("SCHLESWIG_HOLSTEIN", CountryTypeEnum.GERMANY),
    THUERINGEN("THUERINGEN", CountryTypeEnum.GERMANY),

    AARGAU("AARGAU", CountryTypeEnum.SWITZERLAND),
    APPENZELL_AUSSERRHODEN("APPENZELL_AUSSERRHODEN", CountryTypeEnum.SWITZERLAND),
    APPENZELL_INNERRHODEN("APPENZELL_INNERRHODEN", CountryTypeEnum.SWITZERLAND),
    BASEL_LANDSCHAFT("BASEL_LANDSCHAFT", CountryTypeEnum.SWITZERLAND),
    BASEL_STADT("BASEL_STADT", CountryTypeEnum.SWITZERLAND),
    BERN("BERN", CountryTypeEnum.SWITZERLAND),
    FREIBURG("FREIBURG", CountryTypeEnum.SWITZERLAND),
    GENF("GENF", CountryTypeEnum.SWITZERLAND),
    GLARUS("GLARUS", CountryTypeEnum.SWITZERLAND),
    GRAUBUENDEN("GRAUBUENDEN", CountryTypeEnum.SWITZERLAND),
    JURA("JURA", CountryTypeEnum.SWITZERLAND),
    LUZERN("LUZERN", CountryTypeEnum.SWITZERLAND),
    NEUENBURG("NEUENBURG", CountryTypeEnum.SWITZERLAND),
    NIDWALDEN("NIDWALDEN", CountryTypeEnum.SWITZERLAND),
    OBWALDEN("OBWALDEN", CountryTypeEnum.SWITZERLAND),
    SCHAFFHAUSEN("SCHAFFHAUSEN", CountryTypeEnum.SWITZERLAND),
    SCHWYZ("SCHWYZ", CountryTypeEnum.SWITZERLAND),
    SOLOTHURN("SOLOTHURN", CountryTypeEnum.SWITZERLAND),
    ST_GALLEN("ST_GALLEN", CountryTypeEnum.SWITZERLAND),
    TESSIN("TESSIN", CountryTypeEnum.SWITZERLAND),
    THURGAU("THURGAU", CountryTypeEnum.SWITZERLAND),
    URI("URI", CountryTypeEnum.SWITZERLAND),
    WAADT("WAADT", CountryTypeEnum.SWITZERLAND),
    WALLIS("WALLIS", CountryTypeEnum.SWITZERLAND),
    ZUERICH("ZUERICH", CountryTypeEnum.SWITZERLAND),
    ZUG("ZUG", CountryTypeEnum.SWITZERLAND);

    private String value;
    private CountryTypeEnum countryType;

    private ProvinceTypeEnum(final String value, final CountryTypeEnum countryType) {
        this.value = value;
        this.countryType = countryType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public CountryTypeEnum getCountryType() {
        return countryType;
    }

    public void setCountryType(CountryTypeEnum countryType) {
        this.countryType = countryType;
    }
}
