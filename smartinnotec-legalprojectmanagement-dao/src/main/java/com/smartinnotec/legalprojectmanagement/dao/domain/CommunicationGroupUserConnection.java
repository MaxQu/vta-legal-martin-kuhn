package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "CommunicationGroupUserConnection")
public @Data class CommunicationGroupUserConnection {

    @Id
    private String id;
    @DBRef
    private User user;
    @DBRef
    private CommunicationGroup communicationGroup;
    @DBRef
    private Tenant tenant;

}
