package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class LabelsPredefinedContainer {

    private String name;
    private boolean selected;

}
