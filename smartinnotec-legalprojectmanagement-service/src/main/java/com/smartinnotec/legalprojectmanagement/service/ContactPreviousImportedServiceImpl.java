package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressPreviousImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactPreviousImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactPreviousImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactPreviousImportedRepository;

@Service("contactPreviousImportedService")
public class ContactPreviousImportedServiceImpl extends AbstractService implements ContactPreviousImportedService {

    @Autowired
    private ContactPreviousImportedRepository contactPreviousImportedRepository;
    @Autowired
    private AddressPreviousImportedService addressPreviousImportedService;

    @Override
    public ContactPreviousImported convertFromContactImportedToContactPreviousImported(final ContactImported contactImported)
            throws BusinessException {
        logger.info(
            "convert from contactImported with id '{}' to contactPreviousImported in ContactPreviousImportedServiceImpl#convertFromContactImportedToContactPreviousImported",
            contactImported.getId());

        final ContactPreviousImported contactPreviousImported = new ContactPreviousImported();

        contactPreviousImported.setInstitution(contactImported.getInstitution());
        contactPreviousImported.setInstitutionChanged(contactImported.isInstitutionChanged());
        contactPreviousImported.setAdditionalNameInformation(contactImported.getAdditionalNameInformation());
        contactPreviousImported.setAdditionalNameInformationChanged(contactImported.isAdditionalNameInformationChanged());
        contactPreviousImported.setContactPerson(contactImported.getContactPerson());
        contactPreviousImported.setContactPersonChanged(contactImported.isContactPersonChanged());
        contactPreviousImported.setShortCode(contactImported.getShortCode());
        contactPreviousImported.setShortCodeChanged(contactImported.isShortCodeChanged());
        final List<CustomerNumberContainer> copiedCustomerNumberContainers = new ArrayList<>();
        final List<CustomerNumberContainer> customerNumberContainers = contactImported.getCustomerNumberContainers();
        for (final CustomerNumberContainer customerNumberContainer : customerNumberContainers) {
            final CustomerNumberContainer copiedCustomerNumberContainer = customerNumberContainer.deepCopy();
            copiedCustomerNumberContainers.add(copiedCustomerNumberContainer);
        }
        contactPreviousImported.setCustomerNumberContainers(copiedCustomerNumberContainers);
        contactPreviousImported.setCustomerNumberContainersChanged(contactImported.isCustomerNumberContainersChanged());
        contactPreviousImported.setTelephones(contactImported.getTelephones());
        contactPreviousImported.setTelephonesChanged(contactImported.isTelephonesChanged());
        contactPreviousImported.setEmails(contactImported.getEmails());
        contactPreviousImported.setEmailsChanged(contactImported.isEmailsChanged());

        contactPreviousImported.setInformation(contactImported.getInformation());
        contactPreviousImported.setInformationChanged(contactImported.isInformationChanged());

        final List<AgentNumberContainer> copiedAgentNumberContainers = new ArrayList<>();
        final List<AgentNumberContainer> agentNumberContainers = contactImported.getAgentNumberContainers();
        for (final AgentNumberContainer agentNumberContainer : agentNumberContainers) {
            final AgentNumberContainer copiedAgentNumberContainer = agentNumberContainer.deepCopy();
            copiedAgentNumberContainers.add(copiedAgentNumberContainer);
        }
        contactPreviousImported.setAgentNumberContainers(copiedAgentNumberContainers);
        contactPreviousImported.setAgentNumberContainersChanged(contactImported.isAgentNumberContainersChanged());

        final AddressImported addressImported = contactImported.getAddress();

        final AddressPreviousImported addressPreviousImported = addressPreviousImportedService
            .convertFromAddressImportedToAddressPreviousImported(addressImported);
        contactPreviousImported.setAddressPreviousImported(addressPreviousImported);

        final List<ContactPreviousImportedAddressImported> copiedContactsPreviousImportedAddressesImported = new ArrayList<>();
        final List<ContactImportedAddressImported> contactsImportedAddressesImported = contactImported.getAddressesWithProducts();
        if (contactsImportedAddressesImported != null) {
            for (final ContactImportedAddressImported contactImportedAddressImported : contactsImportedAddressesImported) {
                final ContactPreviousImportedAddressImported contactPreviousImportedAddressImported = new ContactPreviousImportedAddressImported();
                final AddressImported addressImportedOfContactImportedAddressImported = contactImportedAddressImported.getAddress();
                final AddressPreviousImported addressPreviousImportedOfContactImportedAddressImported = addressPreviousImportedService
                    .convertFromAddressImportedToAddressPreviousImported(addressImportedOfContactImportedAddressImported);
                contactPreviousImportedAddressImported.setAddressPreviousImported(addressPreviousImportedOfContactImportedAddressImported);
                copiedContactsPreviousImportedAddressesImported.add(contactPreviousImportedAddressImported);
            }
        }
        contactPreviousImported.setContactPreviousImportedAddressesImported(copiedContactsPreviousImportedAddressesImported);
        contactPreviousImported.setTenant(contactImported.getTenant());
        contactPreviousImported.setContactImportedAddressesImportedChanged(contactImported.isContactImportedAddressesImportedChanged());
        contactPreviousImported.setCustomerSince(contactImported.getCustomerSince());
        contactPreviousImported.setImportDateTime(contactImported.getImportDateTime());
        contactPreviousImported.setImported(contactImported.getImported());

        return contactPreviousImported;
    }

    @Override
    public Integer count() throws BusinessException {
        logger.info("count contactPreviousImported in ContactPreviousImportedServiceImpl#count");
        final Long amount = contactPreviousImportedRepository.count();
        return amount.intValue();
    }

    @Override
    public ContactPreviousImported create(final ContactPreviousImported contactPreviousImported) throws BusinessException {
        BusinessAssert.notNull(contactPreviousImported,
            "contactPreviousImported is mandatory in ContactPreviousImportedServiceImpl#create");
        logger.info("create contactPreviousImported in ContactPreviousImportedServiceImpl#create");
        final ContactPreviousImported createdContactPreviousImported = contactPreviousImportedRepository.insert(contactPreviousImported);
        return createdContactPreviousImported;
    }

    @Override
    public ContactPreviousImported update(final ContactPreviousImported contactPreviousImported) throws BusinessException {
        BusinessAssert.notNull(contactPreviousImported,
            "contactPreviousImported is mandatory in ContactPreviousImportedServiceImpl#update");
        logger.info("update contactPreviousImported with id '{}' in ContactPreviousImportedServiceImpl#update",
            contactPreviousImported.getId());
        final ContactPreviousImported updatedContactPreviousImported = contactPreviousImportedRepository.save(contactPreviousImported);
        return updatedContactPreviousImported;
    }

    @Override
    public void delete(final ContactPreviousImported contactPreviousImported) throws BusinessException {
        BusinessAssert.notNull(contactPreviousImported,
            "contactPreviousImported is mandatory in ContactPreviousImportedServiceImpl#delete");
        logger.info("delete contactPreviousImported with id '{}' in ContactPreviousImportedServiceImpl#delete",
            contactPreviousImported.getId());
        contactPreviousImportedRepository.delete(contactPreviousImported);
    }
}
