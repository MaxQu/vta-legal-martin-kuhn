package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventScheduleDays;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.CalendarEventScheduleDaysService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class CalendarEventScheduleDaysRestService extends AbstractRestService {

    @Autowired
    private UserService userService;
    @Autowired
    private CalendarEventScheduleDaysService calendarEventScheduleDaysService;

    @RequestMapping(value = "/calendareventscheduledays/calendareventscheduleday/{userid}", method = {
        RequestMethod.GET }, produces = "application/json")
    public List<CalendarEventScheduleDays> findCalendarEventScheduleDaysByTenant(final @PathVariable(value = "userid") String userId)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in CalendarEventScheduleDaysRestService#findCalendarEventById", "400");
        BusinessAssert.isId(userId, "userId must be an id in CalendarEventScheduleDaysRestService#findCalendarEventById", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ":find all calendarEventScheduleDay by user with id '{}' in CalendarEventScheduleDaysRestService#findCalendarEventById",
            userId);
        final User user = userService.findUser(userId);
        return calendarEventScheduleDaysService.findAllByTenant(user);
    }

    @RequestMapping(value = "/calendareventscheduledays/calendareventscheduleday", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody CalendarEventScheduleDays update(@RequestBody
    final CalendarEventScheduleDays calendarEventScheduleDays) throws BusinessException {
        BusinessAssert.notNull(calendarEventScheduleDays,
            "calendarEventScheduleDays is mandatory in CalendarEventScheduleDaysRestService#update", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ":update calendarEventScheduleDays with id '{}' in CalendarEventScheduleDaysRestService#update",
            calendarEventScheduleDays.getId());
        return calendarEventScheduleDaysService.update(calendarEventScheduleDays);
    }

    @RequestMapping(value = "/calendareventscheduledays/calendareventscheduleday", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody CalendarEventScheduleDays create(@RequestBody
    final CalendarEventScheduleDays calendarEventScheduleDays) throws BusinessException {
        BusinessAssert.notNull(calendarEventScheduleDays,
            "calendarEventScheduleDays is mandatory in CalendarEventScheduleDaysRestService#create", "400");
        logger.info(
            getInvokedRestServiceUserInformation() + ":create calendarEventScheduleDays in CalendarEventScheduleDaysRestService#create");
        return calendarEventScheduleDaysService.create(calendarEventScheduleDays);
    }

    @RequestMapping(value = "/calendareventscheduledays/calendareventscheduleday/{calendareventscheduledaysid}", method = RequestMethod.DELETE)
    public void delete(final @PathVariable(value = "calendareventscheduledaysid") String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in CalendarEventScheduleDaysRestService#delete", "400");
        BusinessAssert.notNull(id, "id is mandatory in CalendarEventScheduleDaysRestService#delete", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ":delete calendarEventScheduleDays with id '{}' in CalendarEventScheduleDaysRestService#delete", id);
        calendarEventScheduleDaysService.delete(id);
    }

    @Override
    public String toString() {
        return "[CalendarEventScheduleDaysRestService]";
    }
}
