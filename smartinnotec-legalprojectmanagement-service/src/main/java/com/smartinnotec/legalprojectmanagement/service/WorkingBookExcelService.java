package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;

public interface WorkingBookExcelService {

    HSSFWorkbook createExcelFile(final String additionalText, final Project project, final List<WorkingBook> workingBookEntries)
            throws BusinessException;

}
