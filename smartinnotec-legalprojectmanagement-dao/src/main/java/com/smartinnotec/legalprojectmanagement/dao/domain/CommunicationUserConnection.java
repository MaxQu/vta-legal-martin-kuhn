package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "CommunicationUserConnection")
public @Data class CommunicationUserConnection implements Comparable<CommunicationUserConnection> {

    @Id
    private String id;
    @DBRef
    private User userCreatedMessage;
    @DBRef
    private User userReceivedMessage;
    @DBRef
    private Project project;
    @DBRef
    private Communication communication;
    private Boolean confirmationNeeded;
    private Boolean read;
    private Boolean finished;
    private boolean archived;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime sortDate;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime executionDate;
    private Boolean executed;
    private boolean relevantForWorkingBook;
    @DBRef
    private Tenant tenant;

    public CommunicationUserConnection() {
        executed = false;
    }

    @Override
    public int compareTo(final CommunicationUserConnection communicationUserConnection) {
        return this.executionDate.compareTo(communicationUserConnection.executionDate);
    }
}
