package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.service.UtilService;

@Component
public class SystemImportContactImportedComparation {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UtilService utilService;

    public boolean contactImportedChanged(final ContactImported existingContactImported, final ContactImported contactImported)
            throws BusinessException {
        BusinessAssert.notNull(contactImported,
            "contactImported is mandatory in SystemImportContactImportedComparation#contactImportedChanged");
        logger.info("imported contact changed detected in SystemImportContactImportedComparation#contactImportedChanged");

        // if imported contact is new
        if (existingContactImported == null && contactImported != null) {
            return true;
        } else if (existingContactImported == null && contactImported == null) {
            return false;
        } else if (existingContactImported != null && contactImported == null) {
            return false;
        }

        if (utilService.hasStringPropertyChanged(existingContactImported.getInstitution(), contactImported.getInstitution())) {
            return true;
        }
        if (utilService.hasStringPropertyChanged(existingContactImported.getAdditionalNameInformation(),
            contactImported.getAdditionalNameInformation())) {
            return true;
        }
        if (utilService.hasStringPropertyChanged(existingContactImported.getContactPerson(), contactImported.getContactPerson())) {
            return true;
        }
        if (utilService.hasStringPropertyChanged(existingContactImported.getShortCode(), contactImported.getShortCode())) {
            return true;
        }
        if (utilService.hasCustomerNumberPropertyChanged(existingContactImported.getCustomerNumberContainers(),
            contactImported.getCustomerNumberContainers())) {
            return true;
        }
        if (utilService.hasAgentNumberPropertyChanged(existingContactImported.getAgentNumberContainers(),
            contactImported.getAgentNumberContainers())) {
            return true;
        }
        if (utilService.hasStringListChanged(existingContactImported.getTelephones(), contactImported.getTelephones())) {
            return true;
        }
        if (utilService.hasStringListChanged(existingContactImported.getEmails(), contactImported.getEmails())) {
            return true;
        }

        final AddressImported existingAddressImported = existingContactImported.getAddress();
        final AddressImported addressImported = contactImported.getAddress();

        final boolean addressImportedChanged = compareAddressesImported(existingAddressImported, addressImported);
        if (addressImportedChanged) {
            return true;
        }

        // contactAddressWithProducts
        final List<ContactImportedAddressImported> existingContactsImportedAddressesImported = existingContactImported
            .getAddressesWithProducts();
        final List<ContactImportedAddressImported> contactsImportedAddressesImported = contactImported.getAddressesWithProducts();

        if (contactAddressesWithProductsHasDifferentSize(existingContactsImportedAddressesImported, contactsImportedAddressesImported)) {
            return true;
        }
        if (contactAddressesWithProductsHasNotSameFlfId(existingContactsImportedAddressesImported, contactsImportedAddressesImported)) {
            return true;
        }
        if (existingContactsImportedAddressesImported != null && contactsImportedAddressesImported != null) {
            if (existingContactsImportedAddressesImported.size() > contactsImportedAddressesImported.size()) {
                for (final ContactImportedAddressImported contactAddressWithProducts : existingContactsImportedAddressesImported) {
                    final ContactImportedAddressImported contactImportedAddressImported = getContactImportedAddressImported(
                        contactAddressWithProducts, contactsImportedAddressesImported);
                    if (contactImportedAddressImported == null) {
                        return true;
                    }
                    final AddressImported addressOfContactAddressWithProducts = contactAddressWithProducts.getAddress();
                    final AddressImported addressOfContactImportedAddressImported = contactImportedAddressImported.getAddress();
                    final boolean contactImportedChanged = compareAddressesImported(addressOfContactAddressWithProducts,
                        addressOfContactImportedAddressImported);
                    if (contactImportedChanged) {
                        return true;
                    }
                }
            } else if (existingContactsImportedAddressesImported.size() < contactsImportedAddressesImported.size()
                || existingContactsImportedAddressesImported.size() == contactsImportedAddressesImported.size()) {
                for (final ContactImportedAddressImported contactsImportedAddressImported : contactsImportedAddressesImported) {
                    final ContactImportedAddressImported contactAddressWithProducts = getContactAddressWithProducts(
                        contactsImportedAddressImported, existingContactsImportedAddressesImported);
                    if (contactAddressWithProducts == null) {
                        return true;
                    }
                    final AddressImported addressOfContactAddressWithProducts = contactAddressWithProducts.getAddress();
                    final AddressImported addressOfContactImportedAddressImported = contactsImportedAddressImported.getAddress();
                    final boolean contactImportedChanged = compareAddressesImported(addressOfContactAddressWithProducts,
                        addressOfContactImportedAddressImported);
                    if (contactImportedChanged) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // imported contactImportedAddressImported -> equal with ContactAddressWithProducts without products
    private ContactImportedAddressImported getContactImportedAddressImported(
            final ContactImportedAddressImported contactAddressWithProducts,
            final List<ContactImportedAddressImported> contactsImportedAddressesImported) {
        logger.info("get contactImported in SystemImportContactImportedComparation#getContactImportedAddressImported");
        for (final ContactImportedAddressImported contactImportedAddressImported : contactsImportedAddressesImported) {
            if (contactImportedAddressImported.getAddress().getFlfId().equals(contactAddressWithProducts.getAddress().getFlfId())) {
                return contactImportedAddressImported;
            }
        }
        return null;
    }

    private ContactImportedAddressImported getContactAddressWithProducts(
            final ContactImportedAddressImported contactImportedAddressImported,
            final List<ContactImportedAddressImported> contactAddressesWithProducts) {
        logger.info("get contactAddressWithProducts in SystemImportContactImportedComparation#getContactAddressWithProducts");
        for (final ContactImportedAddressImported contactAddressWithProducts : contactAddressesWithProducts) {
            if (contactImportedAddressImported.getAddress().getFlfId().equals(contactAddressWithProducts.getAddress().getFlfId())) {
                return contactAddressWithProducts;
            }
        }
        return null;
    }

    private boolean contactAddressesWithProductsHasNotSameFlfId(final List<ContactImportedAddressImported> contactAddressesWithProducts,
            final List<ContactImportedAddressImported> contactsImportedAddressesImported) {
        if (contactAddressesWithProducts != null) {
            for (final ContactImportedAddressImported contactAddressWithProducts : contactAddressesWithProducts) {
                final String flfId = contactAddressWithProducts.getAddress().getFlfId();
                boolean exists = false;
                for (final ContactImportedAddressImported contactImportedAddressImported : contactsImportedAddressesImported) {
                    final String importedFlfId = contactImportedAddressImported.getAddress().getFlfId();
                    if (flfId != null && importedFlfId != null && flfId.equals(importedFlfId)) {
                        exists = true;
                    }
                }
                if (exists == false) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    private boolean contactAddressesWithProductsHasDifferentSize(
            final List<ContactImportedAddressImported> existingContactsImportedAddressesImported,
            final List<ContactImportedAddressImported> contactsImportedAddressesImported) {
        if (((existingContactsImportedAddressesImported == null || existingContactsImportedAddressesImported.isEmpty())
            && (contactsImportedAddressesImported != null && !contactsImportedAddressesImported.isEmpty()))
            || ((existingContactsImportedAddressesImported != null && !existingContactsImportedAddressesImported.isEmpty())
                && (contactsImportedAddressesImported == null || contactsImportedAddressesImported.isEmpty()))
            || (existingContactsImportedAddressesImported != null && contactsImportedAddressesImported != null
                && existingContactsImportedAddressesImported.size() != contactsImportedAddressesImported.size())) {
            return true;
        }
        return false;
    }

    private boolean compareAddressesImported(final AddressImported address, final AddressImported addressImported)
            throws BusinessException {
        logger.info("compare addresses in SystemImportContactComparation#compareAddressesImported");
        if (address == null && addressImported == null) {
            return false;
        }

        boolean contactChanged = false;
        if (utilService.hasStringPropertyChanged(address.getFlfId(), addressImported.getFlfId())) {
            addressImported.setFlfIdChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getAdditionalInformation(), addressImported.getAdditionalInformation())) {
            addressImported.setAdditionalInformationChanged(true);
            contactChanged = true;
        }
        if (utilService.hasCustomerNumberPropertyChanged(address.getCustomerNumberContainers(),
            addressImported.getCustomerNumberContainers())) {
            addressImported.setCustomerNumberContainersChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getStreet(), addressImported.getStreet())) {
            addressImported.setStreetChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getIntSign(), addressImported.getIntSign())) {
            addressImported.setIntSignChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getPostalCode(), addressImported.getPostalCode())) {
            addressImported.setPostalCodeChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getRegion(), addressImported.getRegion())) {
            addressImported.setRegionChanged(true);
            contactChanged = true;
        }
        if ((address.getProvinceType() == null && addressImported.getProvinceType() != null)
            || (address.getProvinceType() != null && addressImported.getProvinceType() == null)
            || (address.getProvinceType() != null && addressImported.getProvinceType() != null && utilService
                .hasStringPropertyChanged(address.getProvinceType().toString(), addressImported.getProvinceType().toString()))) {
            addressImported.setProvinceTypeChanged(true);
            contactChanged = true;
        }
        if ((address.getCountry() != null && addressImported.getCountry() == null)
            || (address.getCountry() == null && addressImported.getCountry() != null)
            || (address.getCountry() != null && addressImported.getCountry() != null
                && utilService.hasStringPropertyChanged(address.getCountry().toString(), addressImported.getCountry().toString()))) {
            addressImported.setCountryChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getTelephone(), addressImported.getTelephone())) {
            addressImported.setTelephoneChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getEmail(), addressImported.getEmail())) {
            addressImported.setEmailChanged(true);
            contactChanged = true;
        }
        if (utilService.hasStringPropertyChanged(address.getInformation(), addressImported.getInformation())) {
            addressImported.setInformationChanged(true);
            contactChanged = true;
        }
        return contactChanged;
    }

    @Override
    public String toString() {
        return "[SystemImportContactImportedComparation]";
    }

}
