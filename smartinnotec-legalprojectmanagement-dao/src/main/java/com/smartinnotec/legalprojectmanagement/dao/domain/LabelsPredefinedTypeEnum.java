package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum LabelsPredefinedTypeEnum {

    CONTENT("CONTENT"),
    DESCRIPTIVE("DESCRIPTIVE"),
    PRODUCT_DESCRIPTIVE("PRODUCT_DESCRIPTIVE");

    private String value;

    private LabelsPredefinedTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
