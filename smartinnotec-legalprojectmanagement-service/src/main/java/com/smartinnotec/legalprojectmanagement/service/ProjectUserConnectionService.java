package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface ProjectUserConnectionService {

    ProjectUserConnection findProjectUserConnection(final String id) throws BusinessException;

    ProjectUserConnection findProjectUserConnectionByUserAndProject(final User user, final Project project) throws BusinessException;

    List<ProjectUserConnection> findByUserAndActive(final User user, final boolean active) throws BusinessException;

    List<ProjectUserConnection> findByProjectAndActive(final Project project, final boolean active) throws BusinessException;

    ProjectUserConnection findByProjectAndContactAndActive(final Project project, final Contact contact, final boolean active)
            throws BusinessException;

    ProjectUserConnection findByUserAndContactAndActive(final User user, final Contact contact, final boolean active)
            throws BusinessException;

    List<ProjectUserConnection> findProjectUserConnectionsOfContactAndProject(final String contactId) throws BusinessException;

    List<ProjectUserConnection> findProjectUserConnectionsOfContactAndUser(final String contactId) throws BusinessException;

    List<ProjectUserConnection> findProjectUserConnectionsOfUserByProjectSearchString(final String userId, final String searchTerm)
            throws BusinessException;

    List<ProjectUserConnection> findProjectUserConnectionsByUserAndContactIsNotNull(final User user) throws BusinessException;

    ProjectUserConnection create(final ProjectUserConnection projectUserConnection) throws BusinessException;

    ProjectUserConnection update(final ProjectUserConnection projectUserConnection) throws BusinessException;

    void delete(final String projectUserConnectionId, final User user) throws BusinessException;

    boolean canProjectUserConnectionRoleSeeConfidentialDocumentFiles(final ProjectUserConnectionRoleEnum projectUserConnectionRole)
            throws BusinessException;

}
