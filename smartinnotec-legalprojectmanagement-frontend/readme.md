
To start a webserver which is able to deliver the client and "proxy" the API requests to the server

`ws --rewrite "/api/(.*) -> http://localhost:8088/api/$1" --spa index.html --port 4444  --directory ./src/main/resources/public --static.maxage -1` 
