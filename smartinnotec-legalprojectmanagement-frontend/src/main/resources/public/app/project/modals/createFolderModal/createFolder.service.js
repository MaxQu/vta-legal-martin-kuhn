(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('createFolderModalService', createFolderModalService);
        
    createFolderModalService.$inject = ['$modal', '$stateParams', '$timeout'];
    
    function createFolderModalService($modal, $stateParams, $timeout) {
		var service = {
			showCreateFolderModal: showCreateFolderModal
		};		
		return service;
		
		////////////
				
		function showCreateFolderModal(invoker, folder, type) {
			 var createFolderModal = $modal.open({
				controller: CreateFolderModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	folder: function() {
			    		return folder;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/project/modals/createFolderModal/createFolder.modal.html'
			});
			return createFolderModal;
			
			function CreateFolderModalController($modalInstance, $scope, invoker, folder, type) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.folder = folder;
		    	vm.type = type;
		    	vm.folderWithNameStillExistsError = false;
		    	
		    	vm.createOrUpdateFolder = createOrUpdateFolder;
		    	vm.closeCreateFolderModal = closeCreateFolderModal;
		    	
		    	////////////
		    	
		    	function setTimeout() {
		      		$timeout(function() {
		      			vm.folderWithNameStillExistsError = false;
		  			   }, 4000); 
		      	}
		    	
		    	function createOrUpdateFolder() {
		    		if(vm.type == 'CREATE') {
		    			vm.invoker.createProjectFolder(vm.folder, function(response) {
		    				if(response.error == 'FOLDER_WITH_NAME_STILL_EXISTS') {
		    					vm.folderWithNameStillExistsError = true;
		    					setTimeout();
		    				} else {
		    					closeCreateFolderModal();
		    				}
		    			});
		    		} else if(vm.type == 'UPDATE') {
		    			vm.invoker.updateProjectFolder(vm.folder, function(response) {
		    				if(response.error == 'FOLDER_WITH_NAME_STILL_EXISTS') {
		    					vm.folderWithNameStillExistsError = true;
		    					setTimeout();
		    				} else {
		    					closeCreateFolderModal();
		    				}
		    			});
		    		}
		    	}
		    	
		    	function closeCreateFolderModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
