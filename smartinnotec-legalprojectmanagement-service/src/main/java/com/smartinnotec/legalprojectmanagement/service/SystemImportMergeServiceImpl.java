package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.service.AbstractService;
import com.smartinnotec.legalprojectmanagement.service.container.AddressContainer;
import com.smartinnotec.legalprojectmanagement.service.container.ContactContainer;

@Service("systemImportMergeService")
public class SystemImportMergeServiceImpl extends AbstractService implements SystemImportMergeService {

    protected final DateTimeFormatter dateFormatter;

    public SystemImportMergeServiceImpl() {
        dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    }

    @Override
    public List<ContactImported> mergeContactsWithAddresses(List<ContactContainer> contactContainers,
            List<AddressContainer> addressContainers)
            throws BusinessException {
        logger.info("merge contacts with addresses in SystemImportMergeServiceImpl#mergeContactsWithAddresses");

        final List<ContactImported> contactsImported = new ArrayList<>();

        for (final ContactContainer contactContainer : contactContainers) {
            if (contactContainer == null) {
                continue;
            }
            final ContactImported contactImported = new ContactImported();
            contactImported.setInstitution(contactContainer.getName1());
            contactImported.setAdditionalNameInformation(contactContainer.getName2());
            contactImported.setContactPerson(contactContainer.getContactPerson());
            contactImported.setShortCode(contactContainer.getShortCode());
            final CustomerNumberContainer customerNumberContainer = new CustomerNumberContainer();
            customerNumberContainer
                .setContactTenant(contactContainer.getTenant() != null ? contactContainer.getTenant().toUpperCase() : null);
            customerNumberContainer.setCustomerNumber(contactContainer.getAccountNumber());
            contactImported.addCustomerNumberContainer(customerNumberContainer);
            final AgentNumberContainer agentNumberContainer = new AgentNumberContainer();
            agentNumberContainer.setCustomerNumber(contactContainer.getAgent());
            contactImported.addAgentNumberContainer(agentNumberContainer);
            if (contactContainer.getTelephone().length() > 0) {
                contactImported.addTelephones(contactContainer.getTelephone());
            }
            if (contactContainer.getMobile().length() > 0) {
                contactImported.addTelephones(contactContainer.getMobile());
            }
            if (contactContainer.getEmail().length() > 0) {
                contactImported.addEmails(contactContainer.getEmail());
            }

            final AddressImported addressImported = new AddressImported();
            addressImported.setStreet(contactContainer.getStreet());
            addressImported.setPostalCode(contactContainer.getPostalCode());
            addressImported.setIntSign(contactContainer.getIntSign());
            addressImported.setRegion(contactContainer.getRegion());
            contactImported.setAddress(addressImported);

            final DateTime customerSince = contactContainer.getCustomerSince().trim().length() < 10 ? null
                : dateFormatter.parseDateTime(contactContainer.getCustomerSince());
            contactImported.setCustomerSince(customerSince);
            contactImported.setImported(new DateTime(System.currentTimeMillis()));

            final List<AddressContainer> foundedAddressContainers = getAddressContainersOfContact(contactContainer.getTenant(),
                contactContainer.getAccountNumber(), addressContainers);
            for (final AddressContainer addressContainer : foundedAddressContainers) {
                final ContactImportedAddressImported contactImportedAddressImported = new ContactImportedAddressImported();
                final AddressImported contactAddressImported = new AddressImported();
                final CustomerNumberContainer addressCustomerNumberContainer = new CustomerNumberContainer();
                addressCustomerNumberContainer
                    .setContactTenant(addressContainer.getTenant() != null ? addressContainer.getTenant().toUpperCase() : null);
                addressCustomerNumberContainer.setCustomerNumber(addressContainer.getAccountNumber());
                contactAddressImported.addCustomerNumberContainer(addressCustomerNumberContainer);
                contactAddressImported.setFlfId(addressContainer.getFlfId());
                contactAddressImported.setAdditionalInformation(addressContainer.getName());
                contactAddressImported.setStreet(addressContainer.getStreet());
                contactAddressImported.setPostalCode(addressContainer.getPostalCode());
                contactAddressImported.setRegion(addressContainer.getRegion());
                contactAddressImported.setTelephone(addressContainer.getTelephone());
                contactAddressImported.setEmail(addressContainer.getEmail());
                contactAddressImported.setInformation(addressContainer.getContactPerson());
                contactAddressImported.setInformation(contactAddressImported.getInformation());
                contactImportedAddressImported.setAddress(contactAddressImported);
                contactImported.addAddressesWithProducts(contactImportedAddressImported);
            }
            contactsImported.add(contactImported);
        }
        logger.info("merged '{}' contacts with addresses in SystemImportMergeServiceImpl#mergeContactsWithAddresses",
            contactsImported.size());
        return contactsImported;
    }

    private List<AddressContainer> getAddressContainersOfContact(final String tenant, final String accountNumber,
            final List<AddressContainer> addressContainers) {
        logger.info(
            "get addressContainers of tenant '{}' and accountNumber '{}' in SystemImportMergeServiceImpl#getAddressContainersOfContact",
            tenant, accountNumber);
        final List<AddressContainer> foundedAddressContainers = addressContainers.stream()
            .filter(s -> s != null && s.getAccountNumber().equals(accountNumber) && s.getTenant().equals(tenant))
            .collect(Collectors.toList());
        logger.info("founded '{}' addressContainers of accountNumber '{}' in SystemImportMergeServiceImpl#getAddressContainersOfContact",
            (foundedAddressContainers != null ? foundedAddressContainers.size() : 0), accountNumber);
        return foundedAddressContainers;
    }
}
