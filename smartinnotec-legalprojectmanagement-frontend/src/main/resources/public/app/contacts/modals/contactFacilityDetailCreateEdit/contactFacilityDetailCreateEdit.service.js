(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactFacilityDetailCreateEditModalService', contactFacilityDetailCreateEditModalService);
        
    contactFacilityDetailCreateEditModalService.$inject = ['$modal', '$timeout', '$stateParams', 'facilityDetailsService'];
    
    function contactFacilityDetailCreateEditModalService($modal, $timeout, $stateParams, facilityDetailsService) {
		var service = {
			showContactFacilityDetailCreateEditModal: showContactFacilityDetailCreateEditModal
		};		
		return service;
		
		////////////
				
		function showContactFacilityDetailCreateEditModal(contact, facilityDetail, activityTypes, facilityDetailsTypes) {
			 var contactFacilityDetailCreateEditModal = $modal.open({
				controller: ContactFacilityDetailCreateEditModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {   
			    	contact: function() {
			    		return contact; 
			    	}, 
			    	facilityDetail: function() {
			    		return facilityDetail;
			    	}, 
			    	activityTypes: function() {
			    		return activityTypes;
			    	},
			    	facilityDetailsTypes: function() {
			    		return facilityDetailsTypes;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/contactFacilityDetailCreateEdit/contactFacilityDetailCreateEdit.modal.html'
			});
			return contactFacilityDetailCreateEditModal;
			
			function ContactFacilityDetailCreateEditModalController($modalInstance, $scope, contact, facilityDetail, activityTypes, facilityDetailsTypes) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.facilityDetail = facilityDetail;
		    	vm.activityTypes = activityTypes;
		    	vm.facilityDetailsTypes = facilityDetailsTypes;
		    	vm.facilityDetailsUpdatedError = false;   
		    	
		    	vm.addOrRemoveActivityType = addOrRemoveActivityType;
		    	vm.addOrRemoveFacilityDetailsType = addOrRemoveFacilityDetailsType;
		    	vm.updateFacilityDetail = updateFacilityDetail;
		    	vm.isTypeSelected = isTypeSelected;
		    	vm.isActivityTypeSelected = isActivityTypeSelected;
		    	vm.isFacilityDetailTypeSelected = isFacilityDetailTypeSelected;
		    	vm.isFacilityTypeSelected = isFacilityTypeSelected;
		    	vm.closeContactFacilityDetailCreateEditModal = closeContactFacilityDetailCreateEditModal;
		    	
		    	////////////

		    	function setTimeout() {
		      		$timeout(function() {
		      			vm.facilityDetailsUpdatedError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function updateFacilityDetail() {
		    		facilityDetailsService.update(vm.facilityDetail).then(function (response) {
		    			closeContactFacilityDetailCreateEditModal();
					}).catch(function (data) {
						vm.facilityDetailsUpdatedError = true;
						setTimeout();
						console.log('error in contactFacilityDetailCreateEdit.service#updateFacilityDetail: ' + data);
					});  
		    	}
		    	
		    	function addOrRemoveActivityType(activityType) {
		    		if(activityType == null) {
		    			return;
		    		}
		    		var removed = false;
		    		for(var i = 0; i < vm.facilityDetail.activityTypes.length; i++) {
		    			if(vm.facilityDetail.activityTypes[i] === activityType) {
		    				vm.facilityDetail.activityTypes.splice(i, 1);
		    				removed = true;
		    				break;
		    			}
		    		}
		    		if(removed === false) {
		    			vm.facilityDetail.activityTypes.push(activityType);
		    		}
		    	}
		    	
		    	function addOrRemoveFacilityDetailsType(facilityDetailsType) {
		    		if(facilityDetailsType == null) {
		    			return;
		    		}
		    		var removed = false;
		    		for(var i = 0; i < vm.facilityDetail.facilityDetailsTypes.length; i++) {
		    			if(vm.facilityDetail.facilityDetailsTypes[i] === facilityDetailsType) {
		    				vm.facilityDetail.facilityDetailsTypes.splice(i, 1);
		    				if(facilityDetailsType === 'VORREINIGUNG') {
		    					for(var j = 0; j < vm.facilityDetail.facilityDetailsTypes.length; j++) {
		    		    			if(vm.facilityDetail.facilityDetailsTypes[j] === 'SEDIMENTATIONS_BECKEN' || vm.facilityDetail.facilityDetailsTypes[j] === 'FLOTATIONS_BECKEN') {
		    		    				vm.facilityDetail.facilityDetailsTypes.splice(j, 1);
		    		    			}
		    					}
		    				}
		    				removed = true;
		    				break;
		    			}
		    		}
		    		if(removed === false) {
		    			vm.facilityDetail.facilityDetailsTypes.push(facilityDetailsType);
		    		}
		    	}
		    	
		    	function isActivityTypeSelected(activityType) {
		    		for(var i = 0; i < vm.facilityDetail.activityTypes.length; i++) {   
		    			if(vm.facilityDetail.activityTypes[i] === activityType) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function isFacilityDetailTypeSelected(facilityDetailsType) {
		    		for(var i = 0; i < vm.facilityDetail.facilityDetailsTypes.length; i++) {   
		    			if(vm.facilityDetail.facilityDetailsTypes[i] === facilityDetailsType) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function isTypeSelected(type) {
		    		for(var i = 0; i < vm.facilityDetail.activityTypes.length; i++) {   
		    			if(vm.facilityDetail.activityTypes[i] === type) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function isFacilityTypeSelected(type) {
		    		for(var i = 0; i < vm.facilityDetail.facilityDetailsTypes.length; i++) {   
		    			if(vm.facilityDetail.facilityDetailsTypes[i] === type) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function closeContactFacilityDetailCreateEditModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();