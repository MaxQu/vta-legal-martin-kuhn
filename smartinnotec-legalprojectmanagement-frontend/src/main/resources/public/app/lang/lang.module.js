(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.lang', [
    		'pascalprecht.translate'
    	]);
})();