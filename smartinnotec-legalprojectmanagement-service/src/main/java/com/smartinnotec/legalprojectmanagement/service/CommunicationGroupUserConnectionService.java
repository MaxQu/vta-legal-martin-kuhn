package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroup;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroupUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface CommunicationGroupUserConnectionService {

    CommunicationGroupUserConnection createCommunicationGroupUserConnection(final User user, final CommunicationGroup communicationGroup,
            final Tenant tenant) throws BusinessException;

    List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionByUser(final User user) throws BusinessException;

    List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionByCommunicationGroup(
            final CommunicationGroup communicationGroup) throws BusinessException;

    List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionsByUser(final User user) throws BusinessException;

    void deleteCommunicationGroupUserConnections(final List<CommunicationGroupUserConnection> communicationGroupUserConnections)
            throws BusinessException;
}
