package com.smartinnotec.legalprojectmanagement.security.xauth;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.smartinnotec.legalprojectmanagement.restservice.ErrorAdviceHolder;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(locations = "classpath:application.properties", ignoreUnknownFields = true, prefix = "legalprojectmanagement.login")
public class XAuthTokenFilter extends GenericFilterBean {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private UserDetailsService detailsService;
    private final TokenUtils tokenUtils = new TokenUtils();
    private String xAuthTokenHeaderName; // configured in application.properties

    public XAuthTokenFilter() {
    }

    public UserDetailsService getDetailsService() {
        return detailsService;
    }

    public void setDetailsService(final UserDetailsService detailsService) {
        this.detailsService = detailsService;
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        try {
            final HttpServletRequest httpServletRequest = (HttpServletRequest)request;
            final String authToken = httpServletRequest.getHeader(this.xAuthTokenHeaderName);

            logger.debug("filter request for token '{}' in XAuthTokenFilter#doFilter", authToken);

            if (StringUtils.hasText(authToken)) {
                final String username = this.tokenUtils.getUserNameFromToken(authToken);

                final UserDetails details = this.detailsService.loadUserByUsername(username);

                final boolean isTokenValid = this.tokenUtils.validateToken(authToken, details);
                logger.debug("token is valid = '{}' in XAuthTokenFilter#doFilter for user with user name '{}'", isTokenValid, username);
                if (isTokenValid) {
                    final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(details,
                        details.getPassword(), details.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(token);
                } else {
                    logger.debug("token '{}' is not valid for user '{}' in AuthTokenFilter", authToken, username);
                    // throw new Exception("token is not valid in XAuthTokenFilter#doFilter");
                }
            }
            filterChain.doFilter(request, response);
        } catch (Exception ex) {

            // @ExceptionHandler is not available at filter-therefore own json-response is created and sent to client
            ((HttpServletResponse)response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);

            final ErrorAdviceHolder errorAdviceHolder = new ErrorAdviceHolder();
            errorAdviceHolder.setTimestamp(new Timestamp(System.currentTimeMillis()));
            errorAdviceHolder.setError(ex.getStackTrace()[0].toString());
            errorAdviceHolder.setMessage(ex.getMessage());
            errorAdviceHolder.setPath("XAuthTokenFilter#doFilter");
            errorAdviceHolder.setStatus("Error");
            errorAdviceHolder.setException(ex.getClass().getName());

            final ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String errorAdviceHolderJson = ow.writeValueAsString(errorAdviceHolder);

            ((HttpServletResponse)response).getWriter().write(errorAdviceHolderJson);
            ((HttpServletResponse)response).getWriter().flush();
            ((HttpServletResponse)response).getWriter().close();
        }
    }

    public String getxAuthTokenHeaderName() {
        return xAuthTokenHeaderName;
    }

    public void setxAuthTokenHeaderName(final String xAuthTokenHeaderName) {
        this.xAuthTokenHeaderName = xAuthTokenHeaderName;
    }

    @Override
    public String toString() {
        return "[XAuthTokenFilter: xAuthTokenHeaderName: " + xAuthTokenHeaderName + "]";
    }
}
