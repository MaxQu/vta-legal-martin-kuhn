package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.SearchHistory;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.SearchHistoryRepository;

@Service("searchHistoryService")
public class SearchHistoryServiceImpl extends AbstractService implements SearchHistoryService {

    @Autowired
    private SearchHistoryRepository searchHistoryRepository;
    @Autowired
    private UserService userService;

    @Override
    public SearchHistory createOrUpdateSearchHistory(final SearchHistory searchHistory) throws BusinessException {
        BusinessAssert.notNull(searchHistory, "searchHistory is mandatory in SearchHistoryServiceImpl#createSearchHistory");
        logger.info("create searchHistory in SearchHistoryServiceImpl#createSearchHistory");

        final SearchHistory existingSearchHistory = searchHistoryRepository.findByUserIdAndSearchString(searchHistory.getUserId(),
            searchHistory.getSearchString());
        if (existingSearchHistory != null) {
            existingSearchHistory.setSearchDate(new DateTime(System.currentTimeMillis()));
            return searchHistoryRepository.save(existingSearchHistory);
        } else {
            return searchHistoryRepository.insert(searchHistory);
        }
    }

    @Override
    public List<SearchHistory> findPagedSearchHistoryOrderBySearchDateDesc(final String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in SearchHistoryServiceImpl#createSearchHistory", "400");
        BusinessAssert.isId(userId, "userId must be an id in SearchHistoryServiceImpl#createSearchHistory", "400");
        logger.info(
            "find paged searchHistory order by searchDate desc in SearchHistoryServiceImpl#findPagedSearchHistoryOrderBySearchDateDesc");
        final User user = userService.findUser(userId);
        final Integer searchHistoryAmount = user.getSearchHistoryAmount() == null ? 5 : user.getSearchHistoryAmount();
        final Pageable pageable = new PageRequest(0, searchHistoryAmount, new Sort(Sort.Direction.DESC, "searchDate"));
        final Page<SearchHistory> pageableSearchHistories = searchHistoryRepository
            .findPagedSearchHistoryByUserIdOrderBySearchDateDesc(userId, pageable);
        final List<SearchHistory> searchHistoryList = new ArrayList<>();
        for (final SearchHistory searchHistory : pageableSearchHistories) {
            searchHistoryList.add(searchHistory);
        }
        return searchHistoryList;
    }
}
