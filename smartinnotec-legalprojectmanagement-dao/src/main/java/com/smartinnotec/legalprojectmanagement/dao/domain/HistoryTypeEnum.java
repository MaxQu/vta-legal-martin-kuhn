package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum HistoryTypeEnum {

    LOGGED_IN("LOGGED_IN"),
    SIGNED_UP("SIGNED_UP"),
    MISREMEMBER_PASSWORD("MISREMEMBER_PASSWORD"),
    CREATED_USER("CREATED_USER"),
    UPDATED_USER("UPDATED_USER"),
    DELETED_USER("DELETED_USER"),
    CREATED_FOLDER("CREATED_FOLDER"),
    UPDATED_FOLDER("UPDATED_FOLDER"),
    DELETED_FOLDER("DELETED_FOLDER"),
    UPDATED_USER_CHANGED_PASSWORD("UPDATED_USER_CHANGED_PASSWORD"),
    CREATED_USER_AUTHORIZATION_USER_CONTAINER("CREATED_USER_AUTHORIZATION_USER_CONTAINER"),
    UPDATED_USER_AUTHORIZATION_USER_CONTAINER("UPDATED_USER_AUTHORIZATION_USER_CONTAINER"),
    DELETED_USER_AUTHORIZATION_USER_CONTAINER("DELETED_USER_AUTHORIZATION_USER_CONTAINER"),
    NEW_FILE_UPLOADED("NEW_FILE_UPLOADED"),
    NEW_VERSION_UPLOADED("NEW_VERSION_UPLOADED"),
    CREATED_NEW_CONTACT("CREATED_NEW_CONTACT"),
    UPDATED_CONTACT("UPDATED_CONTACT"),
    UPDATED_CONTACT_IN_ADM("UPDATED_CONTACT_IN_ADM"),
    CONTACT_IMPORTED_CONFIRMED("CONTACT_IMPORTED_CONFIRMED"),
    MERGE_CONTACTS_WITH_ADDRESSES_OF_STANDARD_PATH_AND_SAVE("MERGE_CONTACTS_WITH_ADDRESSES_OF_STANDARD_PATH_AND_SAVE"),
    MERGE_CONTACTS_WITH_ADDRESSES_AND_SAVE("MERGE_CONTACTS_WITH_ADDRESSES_AND_SAVE"),
    CREATED_CONTACT_CHANGED("CREATED_CONTACT_CHANGED"),
    UPDATED_CONTACT_CHANGED("UPDATED_CONTACT_CHANGED"),
    COMMITED_CONTACT_CHANGED("COMMITED_CONTACT_CHANGED"),
    DELETE_COMMITTED_CONTACTS_CHANGED("DELETE_COMMITTED_CONTACTS_CHANGED"),
    GET_CREATED_CHANGED_DELETED_CONTACTS_CHANGED("GET_CREATED_CHANGED_DELETED_CONTACTS_CHANGED"),
    GET_CREATED_CHANGED_DELETED_ADDRESSES_CHANGED("GET_CREATED_CHANGED_DELETED_ADDRESSES_CHANGED"),
    DELETED_CONTACT_CHANGED("DELETED_CONTACT_CHANGED"),
    DELETED_CONTACT_IMPORTED("DELETED_CONTACT_IMPORTED"),
    DELETED_ALL_CONTACTS_IMPORTED("DELETED_ALL_CONTACTS_IMPORTED"),
    SEARCHED_CONTACT("SEARCHED_CONTACT"),
    ADDED_PRODUCT_TO_CONTACT("ADDED_PRODUCT_TO_CONTACT"),
    REMOVED_PRODUCT_TO_CONTACT("REMOVED_PRODUCT_TO_CONTACT"),
    IMPORTED_CONTACT("IMPORTED_CONTACT"),
    DELETED_IMPORTED_CONTACT("DELETED_IMPORTED_CONTACT"),
    DELETED_CONTACT("DELETED_CONTACT"),
    DEACTIVATE_CONTACT("DEACTIVATE_CONTACT"),
    CREATED_PROJECT("CREATED_PROJECT"),
    DELETED_PROJECT("DELETED_PROJECT"),
    UPDATED_PROJECT("UPDATED_PROJECT"),
    SEARCHED_PROJECT("SEARCHED_PROJECT"),
    CREATED_PRODUCT("CREATED_PRODUCT"),
    DELETED_PRODUCT("DELETED_PRODUCT"),
    UPDATED_PRODUCT("UPDATED_PRODUCT"),
    SEARCHED_PRODUCT("SEARCHED_PRODUCT"),
    CREATED_RECEIPE("CREATED_RECEIPE"),
    DELETED_RECEIPE("DELETED_RECEIPE"),
    UPDATED_RECEIPE("UPDATED_RECEIPE"),
    CREATED_THINNING("CREATED_THINNING"),
    DELETED_THINNING("DELETED_THINNING"),
    UPDATED_THINNING("UPDATED_THINNING"),
    CREATED_MIXTURE("CREATED_MIXTURE"),
    DELETED_MIXTURE("DELETED_MIXTURE"),
    UPDATED_MIXTURE("UPDATED_MIXTURE"),
    ADD_USER_OR_CONTACT_TO_PROJECT("ADD_USER_OR_CONTACT_TO_PROJECT"),
    UPDATED_DOCUMENT_FILE("UPDATED_DOCUMENT_FILE"),
    DELETED_DOCUMENT_FILE("DELETED_DOCUMENT_FILE"),
    SEARCHED_DOCUMENT_FILE("SEARCHED_DOCUMENT_FILE"),
    ADDED_NEW_LABEL("ADDED_NEW_LABEL"),
    DELETED_NEW_LABEL("DELETED_NEW_LABEL"),
    CREATED_CALENDAR_EVENT("CREATED_CALENDAR_EVENT"),
    UPDATED_CALENDAR_EVENT("UPDATED_CALENDAR_EVENT"),
    DELETED_CALENDAR_EVENT("DELETED_CALENDAR_EVENT"),
    DEACTIVATED_CALENDAR_EVENT("DEACTIVATED_CALENDAR_EVENT"),
    CREATE_WORKING_BOOK_ENTRY("CREATE_WORKING_BOOK_ENTRY"),
    UPDATED_WORKING_BOOK_ENTRY("UPDATED_WORKING_BOOK_ENTRY"),
    DELETED_WORKING_BOOK_ENTRY("DELETE_WORKING_BOOK_ENTRY"),
    CREATED_COMMUNICATION_ENTRY("CREATED_COMMUNICATION_ENTRY"),
    UPDATED_COMMUNICATION_ENTRY("UPDATED_COMMUNICATION_ENTRY"),
    DELETED_COMMUNICATION_ENTRY("DELETED_COMMUNICATION_ENTRY"),
    CREATED_TIMEREGISTRATION_ENTRY("CREATED_TIMEREGISTRATION_ENTRY"),
    UPDATED_TIMEREGISTRATION_ENTRY("UPDATED_TIMEREGISTRATION_ENTRY"),
    DELETED_TIMEREGISTRATION_ENTRY("DELETED_TIMEREGISTRATION_ENTRY"),
    DUPLICATE_USER("DUPLICATE_USER"),
    CREATED_ACTIVITY("CREATED_ACTIVITY"),
    UPDATED_ACTIVITY("UPDATED_ACTIVITY"),
    DELETED_ACTIVITY("DELETED_ACTIVITY"),
    READ_ACTIVITIES_OVER_REPORTING("READ_ACTIVITIES_OVER_REPORTING"),
    CREATED_FACILITY_DETAIL("CREATED_FACILITY_DETAIL"),
    UPDATED_FACILITY_DETAIL("UPDATED_FACILITY_DETAIL"),
    DELETED_FACILITY_DETAIL("DELETED_FACILITY_DETAIL"),
    READ_FACILITY_DETAILS_OVER_REPORTING("READ_FACILITY_DETAILS_OVER_REPORTING"),
    GENERAL_SEARCH("GENERAL_SEARCH");

    private String value;

    private HistoryTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
