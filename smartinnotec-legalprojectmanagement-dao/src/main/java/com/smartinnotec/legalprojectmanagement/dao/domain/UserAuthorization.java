package com.smartinnotec.legalprojectmanagement.dao.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;

import lombok.Data;

public @Data class UserAuthorization implements Comparable<UserAuthorization> {

    @JsonView(RestServiceResponseView.UserPublic.class)
    private Integer order;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private Boolean state;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private UserAuthorizationTypeEnum userAuthorizationType;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private UserAuthorizationStateEnum userAuthorizationState;

    @Override
    public int compareTo(UserAuthorization o) {
        if (this.order == null || o.getOrder() == null) {
            return 0;
        }
        return this.order.compareTo(o.getOrder());
    }

    public UserAuthorization deepCopy() {
        final UserAuthorization copiedUserAuthorization = new UserAuthorization();
        copiedUserAuthorization.setOrder(this.order);
        copiedUserAuthorization.setState(this.state);
        copiedUserAuthorization.setUserAuthorizationType(this.userAuthorizationType);
        copiedUserAuthorization.setUserAuthorizationState(this.userAuthorizationState);
        return copiedUserAuthorization;
    }

}
