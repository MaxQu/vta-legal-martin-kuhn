package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileVersion;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class DocumentFileServiceTest extends AbstractServiceTest {

    @Test
    public void shouldGetDocumentFileVersionOfLastVersion() throws BusinessException {
        final DocumentFile documentFile = new DocumentFile();
        final List<DocumentFileVersion> documentFileVersions = new ArrayList<>();

        final DocumentFileVersion documentFileVersion1 = new DocumentFileVersion();
        documentFileVersion1.setVersion("_Version1");
        documentFileVersion1.setActive(true);
        final DocumentFileVersion documentFileVersion2 = new DocumentFileVersion();
        documentFileVersion2.setVersion("_Version2");
        documentFileVersion2.setActive(true);
        final DocumentFileVersion documentFileVersion3 = new DocumentFileVersion();
        documentFileVersion3.setVersion("_Version3");
        documentFileVersion3.setActive(false);

        documentFileVersions.add(documentFileVersion1);
        documentFileVersions.add(documentFileVersion2);
        documentFileVersions.add(documentFileVersion3);

        documentFile.setDocumentFileVersions(documentFileVersions);

        final DocumentFileVersion documentFileVersion = documentFileService.getDocumentFileVersionOfLastVersion(documentFile);
        Assert.assertNotNull(documentFileVersion);
    }
}
