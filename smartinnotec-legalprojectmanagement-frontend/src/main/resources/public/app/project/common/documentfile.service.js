(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('documentFileService', documentFileService);
        
    documentFileService.$inject = ['$http', 'api_config'];
    
    function documentFileService($http, api_config) { 
		var service = {
				searchPredefinedLabels: searchPredefinedLabels,
				findDocumentFileById: findDocumentFileById,
				getVersionObjectOfDocumentFile: getVersionObjectOfDocumentFile,
				countByProjectIdGroupByOriginalFileName: countByProjectIdGroupByOriginalFileName,
				countByProjectIdAndFolderIdGroupByOriginalFileName: countByProjectIdAndFolderIdGroupByOriginalFileName,
				findPagedDocumentFilesByProjectIdAndFolderId: findPagedDocumentFilesByProjectIdAndFolderId,
				findAssignedDocumentFilesByProjectId: findAssignedDocumentFilesByProjectId,
				findDocumentFilesBySearchString: findDocumentFilesBySearchString,
				findDocumentFilesBySearchStringOverAllProjectsAndProducts: findDocumentFilesBySearchStringOverAllProjectsAndProducts,
				findDocumentFilesByBySearchStringAndProjectIdAndFolderId: findDocumentFilesByBySearchStringAndProjectIdAndFolderId,
				calculateAmountOfPages: calculateAmountOfPages,
				updateDocumentFile: updateDocumentFile,
				updateDocumentFileLabels: updateDocumentFileLabels,
				deleteDocumentFile: deleteDocumentFile,
				getDocumentFilePageSize: getDocumentFilePageSize
		};
		return service;
		
		////////////
		
		function searchPredefinedLabels(searchString) {
			return $http.get(api_config.BASE_URL + '/documentfiles/labels/search/' + searchString);
		}
		
		function findDocumentFileById(id) {
			return $http.get(api_config.BASE_URL + '/documentfiles/' + id);
		}
		
		function getVersionObjectOfDocumentFile(documentFile, version) {
    		for(var i = 0; i < documentFile.documentFileVersions.length; i++) {
    			if(documentFile.documentFileVersions[i].version == version) {
    				return documentFile.documentFileVersions[i];
    			}
    		}
    		return null;
    	}
		
		function countByProjectIdGroupByOriginalFileName(projectId, onlyConfident, documentFileParentType) {
			return $http.get(api_config.BASE_URL + '/documentfiles/count/' + projectId + '/' + onlyConfident + '/' + documentFileParentType);
		}
		
		function countByProjectIdAndFolderIdGroupByOriginalFileName(projectId, folderId, onlyConfident, documentFileParentType) {
			return $http.get(api_config.BASE_URL + '/documentfiles/folder/count/' + projectId + '/' + onlyConfident + '/' + documentFileParentType, {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function findPagedDocumentFilesByProjectIdAndFolderId(projectId, folderId, page, sort, onlyConfident, documentFileParentType) { 
			return $http.get(api_config.BASE_URL + '/documentfiles/paged/' + projectId + '/' + page + '/' + sort + '/' + onlyConfident + '/' + documentFileParentType, {
				params: {
					folderid: folderId
				} 
			});
		}
		 
		function findAssignedDocumentFilesByProjectId(projectId, sort) {
			return $http.get(api_config.BASE_URL + '/documentfiles/' + projectId + '/' + sort + '/assigned');
		}
		
		function findDocumentFilesBySearchString(searchString) { 
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/' + searchString);
		}
		
		function findDocumentFilesBySearchStringOverAllProjectsAndProducts(searchString) { 
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/' + searchString + '/all');
		}
		
		function findDocumentFilesByBySearchStringAndProjectIdAndFolderId(searchString, projectId, folderId) { 
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/' + searchString + '/' + projectId, {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function calculateAmountOfPages(projectId, documentFileParentType, folderId) {
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/' + projectId + '/' + documentFileParentType + '/amountofpages', {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function updateDocumentFile(documentFile, projectId) {
			return $http.put(api_config.BASE_URL + '/documentfiles/documentfile/' + projectId, documentFile);
		}
		
		function updateDocumentFileLabels(documentFile, projectId) {
			return $http.put(api_config.BASE_URL + '/documentfiles/documentfile/labels/' + projectId, documentFile);
		}
		
		function deleteDocumentFile(documentFileId, projectId) {
			return $http.delete(api_config.BASE_URL + '/documentfiles/documentfile/' + projectId + '/' + documentFileId); 
		}
		
		function getDocumentFilePageSize() {
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/documentpagesize');
		}
    }
})();