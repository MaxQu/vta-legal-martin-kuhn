package com.smartinnotec.legalprojectmanagement.serializer;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class DateToDateDeserializer extends JsonDeserializer<DateTime> {

    private final DateTimeFormatter dateFormatterDateAndTime = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private final DateTimeFormatter dateFormatterDateAndTime1 = DateTimeFormat.forPattern("dd.MM.yyyy', 'HH:mm:ss");
    private final DateTimeFormatter dateFormatterDateAndTime2 = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
    private final DateTimeFormatter dateFormatterDate = DateTimeFormat.forPattern("dd.MM.yyyy");

    @Override
    public DateTime deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        final String date = jsonparser.getText();
        return deserializeString(date);
    }

    public DateTime deserializeString(final String date) {
        if (date.length() == 10) {
            return dateFormatterDate.parseDateTime(date);
        } else if (date.contains(",")) {
            return dateFormatterDateAndTime1.parseDateTime(date);
        } else if (date.length() == 16) {
            return dateFormatterDateAndTime2.parseDateTime(date);
        } else if (date.length() == 0) {
            return null;
        }

        return dateFormatterDateAndTime.parseDateTime(date);
    }
}
