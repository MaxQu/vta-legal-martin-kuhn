(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.factory('authTokenService', authTokenService);
    
    authTokenService.$inject = ['localStorageService', 'token_config'];
    
    function authTokenService(localStorageService, token_config) {
		var service = {
			saveToken: saveToken,
			deleteToken: deleteToken,
			getToken: getToken
		};
		
		return service;
		
		////////////
		
		function saveToken(token) {
			return localStorageService.set(token_config.AUTH_TOKEN_KEY, token);
		}
		
		function deleteToken() {
			return localStorageService.remove(token_config.AUTH_TOKEN_KEY);
		}	
		
		function getToken(){
			return localStorageService.get(token_config.AUTH_TOKEN_KEY);
		}
	}
})();