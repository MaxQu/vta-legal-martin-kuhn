package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ThinningPolymerDateTimeValue;

public interface ThinningPolymerDateTimeValueService {

    List<ThinningPolymerDateTimeValue> findAll() throws BusinessException;

    ThinningPolymerDateTimeValue getThinningPolymerDateTimeValueByDate(final DateTime dateTime) throws BusinessException;

    ThinningPolymerDateTimeValue create(final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue) throws BusinessException;

    ThinningPolymerDateTimeValue update(final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue) throws BusinessException;

    void delete(final String thinningPolymerDateTimeValueId) throws BusinessException;

}
