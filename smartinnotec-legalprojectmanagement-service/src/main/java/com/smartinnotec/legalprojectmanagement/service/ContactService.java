package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactImportationDateGroup;

public interface ContactService {

    Contact create(final Contact contact, final User user, final boolean createContactChanged) throws BusinessException;

    Contact update(final Contact contact, final User user, final boolean createContactChanged) throws BusinessException;

    List<Contact> findAllTenantContacts(final Tenant tenant) throws BusinessException;

    List<Contact> findAll() throws BusinessException;

    List<Contact> findAllImportedContacts(final Tenant tenant, final boolean imported) throws BusinessException;

    List<Contact> findByContactAttachmentsNotNull() throws BusinessException;

    List<Contact> findByTenantAndImportedAndImportConfirmedAndImportationDate(final Tenant tenant, final boolean imported,
            final boolean importConfirmed, final DateTime importationDate)
            throws BusinessException;

    boolean importContacts(final User user, final CountryTypeEnum selectedCountryType, final byte[] bytes) throws BusinessException;

    List<Contact> findContactBySearchString(final User user, final String searchString) throws BusinessException;

    boolean isContactASupplier(final Contact contact) throws BusinessException;

    List<Contact> findContactsByExistingContactsAnSearchString(final User user, String searchString, final List<Contact> contacts)
            throws BusinessException;

    List<Contact> findContactsOfUserBySearchString(final User user, final String searchString) throws BusinessException;

    List<Contact> findPagedContactsByTenant(final User user, final Tenant tenant, final Integer page, final String sortingType)
            throws BusinessException;

    List<Contact> findAllContactsOfContactType(final ContactTypeEnum contactType) throws BusinessException;

    List<Contact> findAllSupplierContacts(final Tenant tenant);

    List<Contact> findAllContactsAssignedOverAgentNumbers(final User user) throws BusinessException;

    Contact findByCustomerNumberContainersIn(final CustomerNumberContainer customerNumberContainer) throws BusinessException;

    Contact setProductsToContact(final Contact contact) throws BusinessException;

    Contact findContactById(final String contactId) throws BusinessException;

    Contact findContactByIdWithoutProducts(String contactId) throws BusinessException;

    List<Contact> findAllContactsOfUserWithoutProducts(final User user) throws BusinessException;

    Integer calculateAmountOfPages(final User user, final Tenant tenant) throws BusinessException;

    Integer getContactPageSize() throws BusinessException;

    Integer countByTenantAndActive(final Tenant tenant, final boolean active) throws BusinessException;

    Integer countByTenantAndActiveAndDeliveryNumberContainersIsNull(final Tenant tenant, final boolean active) throws BusinessException;

    Integer countByActive(final boolean active) throws BusinessException;

    List<ContactAttachment> getAddedContactAttachments(final List<ContactAttachment> currentContactAttachments,
            final List<ContactAttachment> newContactAttachments);

    List<ContactAttachment> getRemovedContactAttachments(final List<ContactAttachment> currentContactAttachments,
            final List<ContactAttachment> newContactAttachments);

    Contact resetAllChangedFlags(final Contact contact);

    List<Contact> findUniqueContactsByAddress(final String searchString) throws BusinessException;

    List<Contact> findUniqueContacts(final List<Contact> contacts, final List<Contact> contactsFoundedOverAddresses)
            throws BusinessException;

    List<Contact> findContactsByAddress(final Address address) throws BusinessException;

    List<ContactImportationDateGroup> getContactImportationDateGroups() throws BusinessException;

    void deleteContact(final String contactId, final User user) throws BusinessException;

    void deleteAll() throws BusinessException;

    void deleteAllCustomerContacts(final CountryTypeEnum countryType) throws BusinessException;

}
