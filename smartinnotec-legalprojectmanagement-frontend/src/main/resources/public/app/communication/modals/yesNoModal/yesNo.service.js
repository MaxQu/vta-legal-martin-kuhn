(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoCommunicationModalService', yesNoCommunicationModalService);
        
    yesNoCommunicationModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoCommunicationModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, communicationUserConnection) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	communicationUserConnection: function() {
			    		return communicationUserConnection;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, communicationUserConnection) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.communicationUserConnection = communicationUserConnection;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.archiveCommunicationUserConnection(vm.communicationUserConnection);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();