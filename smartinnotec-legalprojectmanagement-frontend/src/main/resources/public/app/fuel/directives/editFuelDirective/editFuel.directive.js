(function () {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .directive('editFuelDirective', editFuelDirective);

    editFuelDirective.$inject = ['$timeout', 'dateUtilityService', 'utilService', 'fuelService', 'optionsService'];

    function editFuelDirective($timeout, dateUtilityService, utilService, fuelService, optionsService) {
        var directive = {
            restrict: 'E',
            scope: {
                fuel: '=',
                locations: '=',
                articles: '=',
                invoker: '=',
                modalInvoker: '=',
                type: '=',
                currentUser: '=',
                roles: '=',
                disabled: '='
            },
            templateUrl: 'app/fuel/directives/editFuelDirective/editFuel.html',
            link: function ($scope) {

                $scope.selected = undefined;
                $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

                $scope.suppliers = [];

                fuelService.findSupplierContacts().then(result => {
                    console.log('findSupplierContacts', result);
                    $scope.suppliers = result.data;
                })

                $scope.deliveryDateDatePickerOpen = false;
                if (!$scope.fuel) {
                    $scope.fuel = {};
                }
                if (!$scope.fuel.deliveryDate) {
                    $scope.fuel.deliveryDate = new Date();
                }

                $scope.openDeliveryDateDatePicker = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.deliveryDateDatePickerOpen = true;
                };

                $scope.saveFuel = function () {
                    $scope.invoker.saveFuel($scope.fuel).then(function() {
                        $scope.modalInvoker.closeEditFuelModal();
                    });
                };

            }
        };
        return directive;

        ////////////
    }
})();
