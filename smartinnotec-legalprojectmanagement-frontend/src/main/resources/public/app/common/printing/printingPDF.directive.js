(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('printingPDFService', printingPDFService);
    
	printingPDFService.$inject = ['$http', 'api_config'];
	
	function printingPDFService($http, api_config) {
		var service = {			
				printSchedule: printSchedule 
		};
		
		return service;
		
		////////////
    	
		function printSchedule(container, name) {
    		var printElement = document.getElementById(container);
    		makePDF(printElement, name);
    	}
		
    	function makePDF(container, pdfName) {
    	    var quotes = container;
    	    html2canvas(quotes, {
    	        onrendered: function(canvas) {

    	        var pdf = new jsPDF('p', 'pt', 'letter');

    	        var loop = Math.ceil(quotes.clientHeight/980);
    	        for (var i = 0; i <= loop; i++) {
    	            var srcImg  = canvas;
    	            var sX      = 0;
    	            var sY      = 980*i; // start 980 pixels down for every new page
    	            var sWidth  = 900;
    	            var sHeight = 980;
    	            var dX      = 0;
    	            var dY      = 0;
    	            var dWidth  = 900;
    	            var dHeight = 980;

    	            window.onePageCanvas = document.createElement("canvas");
    	            onePageCanvas.setAttribute('width', 900);
    	            onePageCanvas.setAttribute('height', 980);
    	            var ctx = onePageCanvas.getContext('2d');
    	            ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
    	            var canvasDataURL = onePageCanvas.toDataURL("image/png", 0.92);

    	            var width         = onePageCanvas.width;
    	            var height        = onePageCanvas.clientHeight;

    	            var pageHeight = pdf.internal.pageSize.height;
    	            
    	            if (i > 0) {
    	                pdf.addPage(612, pageHeight); //8.5" x 11" in pts (in*72)
    	            }
    	            pdf.setPage(i+1);
    	            pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width*0.62), (height*0.62));

    	        }
    	        var today = new Date();
        	    pdf.save(pdfName + '_' + today.getFullYear() + (today.getMonth()+1) + today.getDate());
    	    }
    	  });
    	}
	}
})();


