package com.smartinnotec.legalprojectmanagement.core.util;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.system.exception.ErrorCodesEnum;

public abstract class BusinessAssert {

    private final static String EMAIL_PATTERN;
    private final static Pattern PATTERN;

    static {
        EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        PATTERN = Pattern.compile(EMAIL_PATTERN);
    }

    public static final void isTrue(final boolean expression, final String message, final String errorCode) throws BusinessException {
        if (!expression) {
            throw new BusinessException(message, errorCode);
        }
    }

    public static final void isNotAllowed(final String message, final String errorCode) throws BusinessException {
        throw new BusinessException(message, errorCode);
    }

    /**
     * Assert a boolean expression, throwing <code>BusinessException</code> if the test result is <code>true</code>.
     * 
     * <pre class="code">
     * Assert.isFalse(i &lt; 0, &quot;The value must be less than zero&quot;);
     * </pre>
     * 
     * @param expression a boolean expression
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if expression is <code>true</code>
     */
    public static final void isFalse(final boolean expression, final String message, final String errorCode) throws BusinessException {
        if (expression) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * Assert a boolean expression, throwing <code>BusinessException</code> if the test result is <code>false</code>.
     * 
     * <pre class="code">
     * Assert.isTrue(i &gt; 0);
     * </pre>
     * 
     * @param expression a boolean expression
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if expression is <code>false</code>
     */
    public static final void isTrue(final boolean expression) throws BusinessException {
        isTrue(expression, "[Assertion failed] - this expression must be true", null);
    }

    /**
     * Assert that an object is <code>null</code> .
     * 
     * <pre class="code">
     * Assert.isNull(value, &quot;The value must be null&quot;);
     * </pre>
     * 
     * @param object the object to check
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object is not <code>null</code>
     */
    public static final void isNull(final Object object, final String message, final String errorCode) throws BusinessException {
        if (object != null) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * check if both objects (1 and 2) are null - if one is not null -> BusinessException
     * 
     * @param object1
     * @param object2
     * @param message
     * @param errorCode
     * @throws BusinessException
     */
    public static final void bothNull(final Object object1, final Object object2, final String message, final String errorCode)
            throws BusinessException {
        if (object1 != null || object2 != null) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * check if object string is a valid guid
     * 
     * @param object
     * @param message
     * @param errorCode
     * @throws BusinessException
     */
    public static final void isGUID(final String object, final String message, final String errorCode) throws BusinessException {
        final String guidRegEx = "[0-9a-fA-F]{8}(?:-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12}";
        if (!Pattern.matches(guidRegEx, object)) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * check if parameter is an id
     * 
     * @param object
     * @param message
     * @param errorCode
     * @throws BusinessException
     */
    public static final void isId(final String object, final String message, final String errorCode) throws BusinessException {
        final String regex = "[0-9a-f]{24}";
        if (!Pattern.matches(regex, object)) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * Assert that an object is <code>null</code> .
     * 
     * <pre class="code">
     * Assert.isNull(value);
     * </pre>
     * 
     * @param object the object to check
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object is not <code>null</code>
     */
    public static final void isNull(final Object object) throws BusinessException {
        isNull(object, "[Assertion failed] - the object argument must be null", null);
    }

    /**
     * Assert that an object is not <code>null</code> .
     * 
     * <pre class="code">
     * Assert.notNull(clazz, &quot;The class must not be null&quot;);
     * </pre>
     * 
     * @param object the object to check
     * @param message the exception message to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object is <code>null</code>
     */
    public static final void notNull(final Object object, final String message) throws BusinessException {
        if (object == null) {
            throw new BusinessException(message);
        }
    }

    /**
     * Assert that an object is not <code>null</code> .
     * 
     * <pre class="code">
     * Assert.notNull(clazz, &quot;The class must not be null&quot;);
     * </pre>
     * 
     * @param object the object to check
     * @param message the exception message to use if the assertion fails
     * @param errorCode the error code of the exception
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object is <code>null</code>
     */
    public static final void notNull(final Object object, final String message, final String errorCode) throws BusinessException {
        if (object == null) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * Assert that an object is not <code>null</code> .
     * 
     * <pre class="code">
     * Assert.notNull(clazz);
     * </pre>
     * 
     * @param object the object to check
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object is <code>null</code>
     */
    public static final void notNull(final Object object) throws BusinessException {
        notNull(object, "[Assertion failed] - this argument is required; it must not be null");
    }

    /**
     * Assert that the given String is not empty; that is, it must not be <code>null</code> and not the empty String.
     * 
     * <pre class="code">
     * Assert.hasLength(name, &quot;Name must not be empty&quot;);
     * </pre>
     * 
     * @param text the String to check
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @see StringUtils#hasLength
     */
    public static final void hasLength(final String text, final String message, final String errorCode) throws BusinessException {
        if (!StringUtils.hasLength(text)) {
            throw new BusinessException(message, errorCode);
        }
    }

    public static final void hasMinLength(final String text, final int length, final String message, final String errorCode)
            throws BusinessException {
        if (text.length() < length)
            throw new BusinessException(message, errorCode);
    }

    /**
     * Assert that the given String is not empty; that is, it must not be <code>null</code> and not the empty String.
     * 
     * <pre class="code">
     * Assert.hasLength(name);
     * </pre>
     * 
     * @param text the String to check
     * @throws BusinessException the result of the task
     * @see StringUtils#hasLength
     */
    public static final void hasLength(final String text) throws BusinessException {
        hasLength(text, "[Assertion failed] - this String argument must have length; it must not be null or empty", null);
    }

    /**
     * Assert that the given String has valid text content; that is, it must not be <code>null</code> and must contain
     * at least one non-whitespace character.
     * 
     * <pre class="code">
     * Assert.hasText(name, &quot;'name' must not be empty&quot;);
     * </pre>
     * 
     * @param text the String to check
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @see StringUtils#hasText
     */
    public static final void hasText(final String text, final String message, final String errorCode) throws BusinessException {
        if (!StringUtils.hasText(text)) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * Assert that the given String has valid text content; that is, it must not be <code>null</code> and must contain
     * at least one non-whitespace character.
     * 
     * <pre class="code">
     * Assert.hasText(name, &quot;'name' must not be empty&quot;);
     * </pre>
     * 
     * @param text the String to check
     * @param message the exception message to use if the assertion fails
     * @throws BusinessException the result of the task
     * @see StringUtils#hasText
     */
    public static final void hasText(final String text, final String message) throws BusinessException {
        if (!StringUtils.hasText(text)) {
            throw new BusinessException(message);
        }
    }

    /**
     * Assert that the given String has valid text content; that is, it must not be <code>null</code> and must contain
     * at least one non-whitespace character.
     * 
     * <pre class="code">
     * Assert.hasText(name, &quot;'name' must not be empty&quot;);
     * </pre>
     * 
     * @param text the String to check
     * @throws BusinessException the result of the task
     * @see StringUtils#hasText
     */
    public static final void hasText(final String text) throws BusinessException {
        hasText(text, "[Assertion failed] - this String argument must have text; it must not be null, empty, or blank", null);
    }

    public static final void isStartTimeAfterEndTime(final boolean startTimeAfterEndTime, final String message, final String errorCode,
            final ErrorCodesEnum errorCodeType) throws BusinessException {
        if (startTimeAfterEndTime) {
            throw new BusinessException(message, errorCode, errorCodeType.toString());
        }
    }

    public static final void isStartDateTimeAfterOrEqualWithCurrentTime(final boolean startDateTimeAfterOrEqualWithCurrentTime,
            final String message, final String errorCode, final ErrorCodesEnum errorCodeType) throws BusinessException {
        if (startDateTimeAfterOrEqualWithCurrentTime) {
            throw new BusinessException(message, errorCode, errorCodeType.toString());
        }
    }

    /**
     * Assert that the given text does not contain the given substring.
     * 
     * <pre class="code">
     * Assert.doesNotContain(name, &quot;rod&quot;, &quot;Name must not contain 'rod'&quot;);
     * </pre>
     * 
     * @param textToSearch the text to search
     * @param substring the substring to find within the text
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     */
    public static final void doesNotContain(final String textToSearch, final String substring, final String message, final String errorCode)
            throws BusinessException {
        if (StringUtils.hasLength(textToSearch) && StringUtils.hasLength(substring) && textToSearch.indexOf(substring) != -1) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * Assert that the given text does not contain the given substring.
     * 
     * <pre class="code">
     * Assert.doesNotContain(name, &quot;rod&quot;);
     * </pre>
     * 
     * @param textToSearch the text to search
     * @param substring the substring to find within the text
     * @throws BusinessException the result of the task
     */
    public static final void doesNotContain(final String textToSearch, final String substring) throws BusinessException {
        doesNotContain(textToSearch, substring,
            "[Assertion failed] - this String argument must not contain the substring [" + substring + "]", null);
    }

    /**
     * Assert that an array has elements; that is, it must not be <code>null</code> and must have at least one element.
     * 
     * <pre class="code">
     * Assert.notEmpty(array, &quot;The array must have elements&quot;);
     * </pre>
     * 
     * @param array the array to check
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object array is <code>null</code> or has no elements
     */
    public static final void notEmpty(final Object[] array, final String message, final String errorCode) throws BusinessException {
        if (ObjectUtils.isEmpty(array)) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * Assert that an array has elements; that is, it must not be <code>null</code> and must have at least one element.
     * 
     * <pre class="code">
     * Assert.notEmpty(array);
     * </pre>
     * 
     * @param array the array to check
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object array is <code>null</code> or has no elements
     */
    public static final void notEmpty(final Object[] array) throws BusinessException {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element", null);
    }

    /**
     * Assert that an array has no null elements. Note: Does not complain if the array is empty!
     * 
     * <pre class="code">
     * Assert.noNullElements(array, &quot;The array must have non-null elements&quot;);
     * </pre>
     * 
     * @param array the array to check
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object array contains a <code>null</code> element
     */
    public static final void noNullElements(final Object[] array, final String message, final String errorCode) throws BusinessException {
        if (array != null) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] == null) {
                    throw new BusinessException(message, errorCode);
                }
            }
        }
    }

    /**
     * Assert that an array has no null elements. Note: Does not complain if the array is empty!
     * 
     * <pre class="code">
     * Assert.noNullElements(array);
     * </pre>
     * 
     * @param array the array to check
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object array contains a <code>null</code> element
     */
    public static final void noNullElements(final Object[] array) throws BusinessException {
        noNullElements(array, "[Assertion failed] - this array must not contain any null elements", null);
    }

    /**
     * Assert that a collection has elements; that is, it must not be <code>null</code> and must have at least one
     * element.
     * 
     * <pre class="code">
     * Assert.notEmpty(collection, &quot;Collection must have elements&quot;);
     * </pre>
     * 
     * @param collection the collection to check
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the collection is <code>null</code> or has no elements
     */
    public static final void notEmpty(final Collection<?> collection, final String message, final String errorCode)
            throws BusinessException {
        if (CollectionUtils.isEmpty(collection)) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * Assert that a collection has elements; that is, it must not be <code>null</code> and must have at least one
     * element.
     * 
     * <pre class="code">
     * Assert.notEmpty(collection, &quot;Collection must have elements&quot;);
     * </pre>
     * 
     * @param collection the collection to check
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the collection is <code>null</code> or has no elements
     */
    public static final void notEmpty(final Collection<?> collection) throws BusinessException {
        notEmpty(collection, "[Assertion failed] - this collection must not be empty: it must contain at least 1 element", null);
    }

    /**
     * Assert that a Map has entries; that is, it must not be <code>null</code> and must have at least one entry.
     * 
     * <pre class="code">
     * Assert.notEmpty(map, &quot;Map must have entries&quot;);
     * </pre>
     * 
     * @param map the map to check
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the map is <code>null</code> or has no entries
     */
    public static final void notEmpty(final Map<?, ?> map, final String message, final String errorCode) throws BusinessException {
        if (CollectionUtils.isEmpty(map)) {
            throw new BusinessException(message, errorCode);
        }
    }

    /**
     * Assert that a Map has entries; that is, it must not be <code>null</code> and must have at least one entry.
     * 
     * <pre class="code">
     * Assert.notEmpty(map);
     * </pre>
     * 
     * @param map the map to check
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the map is <code>null</code> or has no entries
     */
    public static final void notEmpty(final Map<?, ?> map) throws BusinessException {
        notEmpty(map, "[Assertion failed] - this map must not be empty; it must contain at least one entry", null);
    }

    /**
     * Assert that the provided object is an instance of the provided class.
     * 
     * <pre class="code">
     * Assert.instanceOf(Foo.class, foo);
     * </pre>
     * 
     * @param clazz the required class
     * @param obj the object to check
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object is not an instance of clazz
     * @see Class#isInstance
     */
    public static final void isInstanceOf(final Class<?> clazz, final Object obj) throws BusinessException {
        isInstanceOf(clazz, obj, "", null);
    }

    /**
     * Assert that the provided object is an instance of the provided class.
     * 
     * <pre class="code">
     * Assert.instanceOf(Foo.class, foo);
     * </pre>
     * 
     * @param type the type to check against
     * @param obj the object to check
     * @param message a message which will be prepended to the message produced by the function itself, and which may be
     *            used to provide context. It should normally end in a ": " or ". " so that the function generate
     *            message looks ok when prepended to it.
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the object is not an instance of clazz
     * @see Class#isInstance
     */
    public static final void isInstanceOf(final Class<?> type, final Object obj, final String message, final String errorCode)
            throws BusinessException {
        notNull(type, "Type to check against must not be null", errorCode);
        if (!type.isInstance(obj)) {
            throw new BusinessException(
                message + "Object of class [" + (obj != null ? obj.getClass().getName() : "null") + "] must be an instance of " + type);
        }
    }

    /**
     * Assert that <code>superType.isAssignableFrom(subType)</code> is <code>true</code>.
     * 
     * <pre class="code">
     * Assert.isAssignable(Number.class, myClass);
     * </pre>
     * 
     * @param superType the super type to check
     * @param subType the sub type to check
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the classes are not assignable
     */
    public static final void isAssignable(final Class<?> superType, final Class<?> subType) throws BusinessException {
        isAssignable(superType, subType, "", null);
    }

    /**
     * Assert that <code>superType.isAssignableFrom(subType)</code> is <code>true</code>.
     * 
     * <pre class="code">
     * Assert.isAssignable(Number.class, myClass);
     * </pre>
     * 
     * @param superType the super type to check against
     * @param subType the sub type to check
     * @param message a message which will be prepended to the message produced by the function itself, and which may be
     *            used to provide context. It should normally end in a ": " or ". " so that the function generate
     *            message looks ok when prepended to it.
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws BusinessException the result of the task
     * @throws BusinessException the result of the task if the classes are not assignable
     */
    public static final void isAssignable(final Class<?> superType, final Class<?> subType, final String message, final String errorCode)
            throws BusinessException {
        notNull(superType, "Type to check against must not be null", errorCode);
        if (subType == null || !superType.isAssignableFrom(subType)) {
            throw new BusinessException(message + subType + " is not assignable to " + superType);
        }
    }

    public static final void isEqual(final int expected, final int current, final String message) throws BusinessException {
        if (expected != current) {
            throw new BusinessException(message + ", expected: " + expected + " but was: " + current);
        }
    }

    /**
     * Assert a boolean expression, throwing <code>IllegalStateException</code> if the test result is <code>false</code>
     * . Call isTrue if you wish to throw BusinessException on an assertion failure.
     * 
     * <pre class="code">
     * Assert.state(id == null, &quot;The id property must not already be initialized&quot;);
     * </pre>
     * 
     * @param expression a boolean expression
     * @param message the exception message to use if the assertion fails
     * @param errorCode the exception errorCode to use if the assertion fails
     * @throws IllegalStateException if expression is <code>false</code>
     */
    public static final void state(final boolean expression, final String message, final String errorCode) {
        if (!expression) {
            throw new IllegalStateException(message);
        }
    }

    /**
     * Assert a boolean expression, throwing {@link IllegalStateException} if the test result is <code>false</code>.
     * <p>
     * Call {@link #isTrue(boolean)} if you wish to throw {@link BusinessException} on an assertion failure.
     * 
     * <pre class="code">
     * Assert.state(id == null);
     * </pre>
     * 
     * @param expression a boolean expression
     * @throws IllegalStateException if the supplied expression is <code>false</code>
     */
    public static final void state(final boolean expression) {
        state(expression, "[Assertion failed] - this state invariant must be true", null);
    }

    // ////////////////////////////////////////////////////////////////////

    public static final boolean checkEmail(final String email, final String message, final String errorCode) throws BusinessException {
        final Matcher matcher = PATTERN.matcher(email);
        final boolean emailChecked = matcher.matches();
        if (!emailChecked) {
            throw new BusinessException(message, errorCode);
        }
        return emailChecked;
    }

}
