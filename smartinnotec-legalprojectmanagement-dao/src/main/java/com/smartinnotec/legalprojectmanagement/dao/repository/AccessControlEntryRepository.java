package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.AccessControlEntry;

public interface AccessControlEntryRepository extends MongoRepository<AccessControlEntry, String> {

    List<AccessControlEntry> findByEventDateTimeBetween(final DateTime start, final DateTime end);

}
