(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products') 
    	.directive('createProductsDirective', createProductsDirective);
    
    createProductsDirective.$inject = ['$timeout', 'Upload', 'productsService', 'productsOfTenantModalService', 'dateUtilityService', 'contactsService'];
    
	function createProductsDirective($timeout, Upload, productsService, productsOfTenantModalService, dateUtilityService, contactsService) {
		var directive = {
			restrict: 'E',
			scope: {
				product: '=',
				products: '=',
				productTypes: '=',
				typeOfChange: '=',
				currentUser: '=',
				productAggregateConditionTypeEnums: '=',
				invoker: '=',
				roles: '=',
				usersOfProject: '='
			},
			templateUrl: 'app/products/directives/createProductDirective/createProduct.html',
			link: function($scope) {
				
				$scope.product.inactiveDate = $scope.product.inactiveDate != null ? dateUtilityService.convertToDateOrUndefined($scope.product.inactiveDate) : null;
				$scope.product.developmentDate = $scope.product.developmentDate != null ? dateUtilityService.convertToDateOrUndefined($scope.product.developmentDate) : null;
				$scope.selectedContact = null;
				$scope.showAllAddressesWithProducts = $scope.typeOfChange == 'CREATE' ? true : false;
				$scope.previousProductFillingWeights = $scope.product.productFillingWeights;
				
				$scope.$watch('product.productAggregateConditionType', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.productFillingWeights();
		    		} 
				});
				
				$scope.addFirstProductName = function() {
					if($scope.product.names.length == 0) {
						$scope.product.names.push('');
					}
				};
				$scope.addFirstProductName();
				
				$scope.openProductInactiveDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.productInactiveDateDatePicker = true;
				};
				
				$scope.openProductDevelopmentDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.productDevelopmentDatePicker = true;
				};
				
				$scope.createOrChangeProduct = function() {
					if($scope.typeOfChange == 'CREATE') {
						$scope.createProduct(); 
					} else if($scope.typeOfChange == 'CHANGE') {
						$scope.changeProduct();
					}
				};
				
				$scope.productFillingWeights = function() {
					if($scope.product.productAggregateConditionType == 'LIQUID' && ($scope.typeOfChange == 'CREATE' || $scope.product.productFillingWeights.length == 3)) {
						$scope.product.productFillingWeights = [
							{fillingWeight: 'IBC (1.000 Liter)', information: ''},
							{fillingWeight: 'Fass (220 Liter)', information: ''},
							{fillingWeight: 'Fass (120 Liter)', information: ''},
							{fillingWeight: 'Kanister (25 Liter)', information: ''} 
							];
					} else if($scope.product.productAggregateConditionType == 'LIQUID' && $scope.product.productFillingWeights.length == 4) {
						$scope.product.productFillingWeights = $scope.previousProductFillingWeights;
					} else if($scope.product.productAggregateConditionType == 'HARD') {
						$scope.product.productFillingWeights = [
							{fillingWeight: 'Big-Bag (250 Kg)', information: ''},
							{fillingWeight: 'Big-Bag (750 Kg)', information: ''},
							{fillingWeight: 'Big-Bag (1000 Kg)', information: ''}
							]; 
					}
				};
				
				$scope.createProduct = function() {
		    		if($scope.product.name === '') {
		    			return;
		    		}
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		$scope.product.creationUserId = $scope.currentUser.id;
		    		$scope.product.orderNumber = $scope.products.length;
		    		productsService.createProduct($scope.product, false).then(function(response) {
		    			var createdProduct = response.data;
		    			createdProduct.stillCreated = true;
		    			$scope.products.splice(0, 0, createdProduct);
		    			$scope.resetProduct();
		    			$scope.productCreatedSuccess = true;
		    			$scope.invoker.createProductNotifier();
		    			setTimeout();
		    		}, function errorCallback(response) {
			   			  console.log('error createProduct.directive#createProduct');
			   		});
				};
				
				$scope.resetProduct = function() {
					$scope.product.number = '';
	    			$scope.product.name = '';
	    			$scope.product.names = [];
	    			$scope.product.names.push('');
	    			$scope.product.description = '';
	    			$scope.product.chemicalDescription = '';
	    			$scope.product.density = '';
	    			$scope.product.productAggregateConditionType = null;
	    			for(var i = 0; i < $scope.product.productFillingWeights.length; i++) {
	    				$scope.product.productFillingWeights[i].information = ''; 
	    			}
	    			$scope.product.productFillingWeights = [];
	    			$scope.product.color = '#007ab9';
	    			$scope.product.developmentDate = null;
	    			$scope.product.types = [];
	    			$scope.product.position = '';
	    			$scope.product.mixtureInstructions = null;
	    			$scope.product.contactsProduct = [];
	    			$scope.product.creationUserId = '';
	    			$scope.product.creationDate = null;
				};
				
				$scope.changeProduct = function() {
					Date.prototype.toJSON = function () { return this.toLocaleString(); };
					productsService.updateProduct($scope.product, false).then(function(response) {
						$scope.updateProduct(response.data);
						$scope.invoker.closeChangeProductPropertiesModal();
					}, function errorCallback(response) {
						  if(response.data.error == 'NO_UNIQUE_NAME') {
							  $scope.noUniqueNameError = true;
						  } else if(response.data.error == 'NO_UNIQUE_NUMBER') {
							  $scope.noUniqueNumberError = true;
						  }
						  setTimeout();
			   			  console.log('error createProduct.directive#changeProduct');
			   		});	
				};
				
				$scope.updateProduct = function(product) {
					for(var i = 0; i < $scope.products.length; i++) {
						if($scope.products[i].id == product.id) {
							$scope.products[i] = product;
							$scope.products[i].selected = true;
							break;
						}
					}
				};
				
				$scope.showAllProductsOfTenant = function() {
					productsService.getAllProductsOfTenant().then(function(response) {
						productsOfTenantModalService.showProductsOfTenantModal(response.data);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createProduct.directive#showAllProductsOfTenant');
		  	   		 });
				};
				
				$scope.isProductTypeSelected = function(product, productType) {
					for(var i = 0; i < product.types.length; i++) { 
						if(product.types[i] == productType) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addProductType = function(product, productType) {
					var productTypeStillExists = false;
					for(var i = 0; i < product.types.length; i++) {
						if(product.types[i] == productType) {
							productTypeStillExists = true;
							break;
						}
					}
					if(!productTypeStillExists) {
						product.types.push(productType);
					}
				};
				
				$scope.removeProductType = function(product, productType) {
					for(var i = 0; i < product.types.length; i++) {
						if(product.types[i] == productType) {
							product.types.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.getContactsByTerm = function(term) {
					return contactsService.findContactBySearchString(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createProduct.directive#getContactsByTerm'); 
		  	   		 });
				};
				
				$scope.addContact = function(contact) {
					if(contact == null) {
						return;
					}
					var contactStillAdded = false;
					for(var i = 0; i < $scope.product.contactsProduct.length; i++) {
						if($scope.product.contactsProduct[i].contact.id == contact.id) {
							contactStillAdded = true;
						}
					}
					if(!contactStillAdded) {
						var contactProduct = {};
						contactProduct.contact = contact;
						$scope.product.contactsProduct.push(contactProduct);
						$scope.selectedContact = null;
					} else {
						$scope.contactStillExists = true;
						setTimeout();
					}
				};
				
				$scope.removeContact = function(contactProduct) {
					for(var i = 0; i < $scope.product.contactsProduct.length; i++) {
						if($scope.product.contactsProduct[i].contact.id === contactProduct.contact.id) {
							$scope.product.contactsProduct.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.addOrRemoveAddressLocation = function(contactProduct, address, selectAddressLocation, orderConstruction) {
					if(selectAddressLocation == true) {
						$scope.addAddressLocation(contactProduct, address, orderConstruction);
					} else if(selectAddressLocation == false) {
						$scope.removeAddressLocation(contactProduct, address, orderConstruction);
					}
				};
				
				$scope.addAddressLocation = function(contactProduct, addressLocation, orderConstruction) {
					var productLocationAndOrderConstruction = {};
					productLocationAndOrderConstruction.address = addressLocation;
					productLocationAndOrderConstruction.orderConstruction = orderConstruction;
					if(contactProduct.productLocationAndOrderConstructions == null) {
						contactProduct.productLocationAndOrderConstructions = [];
					}
					contactProduct.productLocationAndOrderConstructions.push(productLocationAndOrderConstruction);
				};
				
				$scope.removeAddressLocation = function(contactProduct, addressLocation, orderConstruction) {
					for(var i = 0; i < contactProduct.productLocationAndOrderConstructions.length; i++) {
						if(contactProduct.productLocationAndOrderConstructions[i].address.id == addressLocation.id || $scope.addressEqualsOverProperties(contactProduct.productLocationAndOrderConstructions[i], addressLocation)) {
							contactProduct.productLocationAndOrderConstructions.splice(i, 1);
						}
					}
				};
				
				$scope.addOrderConstruction = function(contactProduct, addressWithProducts, orderConstruction) {
					if(contactProduct.productLocationAndOrderConstructions == null) {
						addressWithProducts.notSelectedError = true;
						setAddressWithProductsTimeout(addressWithProducts);
						return;
					}
					var addressStillSelected = false;
					for(var j = 0; j < contactProduct.productLocationAndOrderConstructions.length; j++) {
						if(contactProduct.productLocationAndOrderConstructions[j].address != null && addressWithProducts != null && contactProduct.productLocationAndOrderConstructions[j].address.id == addressWithProducts.id) {
							addressStillSelected = true;
						}
					}
					if(addressStillSelected == false) {
						addressWithProducts.notSelectedError = true;
						setAddressWithProductsTimeout(addressWithProducts);
						return;
					}
					
					for(var i = 0; i < contactProduct.productLocationAndOrderConstructions.length; i++) {
						if(contactProduct.productLocationAndOrderConstructions[i].address.id == addressWithProducts.id) {
							contactProduct.productLocationAndOrderConstructions[i].orderConstruction = orderConstruction;
						}
					}
				};
				
				$scope.isAddressWithProductsSelected = function(contactProduct, address) {
					if(contactProduct.productLocationAndOrderConstructions == null) {
						return false;
					}
					for(var i = 0; i < contactProduct.productLocationAndOrderConstructions.length; i++) {
						if((contactProduct.productLocationAndOrderConstructions[i].address != null && 
						   address != null && 
						   contactProduct.productLocationAndOrderConstructions[i].address.id == address.id) || $scope.addressEqualsOverProperties(contactProduct.productLocationAndOrderConstructions[i], address)) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addressEqualsOverProperties = function(productLocationAndOrderConstruction, address) {
					if(productLocationAndOrderConstruction.address == null || address == null) {
						return false;
					}
					var contactProductAddress = productLocationAndOrderConstruction.address;
					
					if(contactProductAddress.street === address.street &&
					   contactProductAddress.region === address.region &&
					   contactProductAddress.postalCode === address.postalCode &&
					   contactProductAddress.provinceType === address.provinceType &&
					   contactProductAddress.country === address.country) {
						return true;
					}
					return false;
				};
				
				$scope.getOrderConstruction = function(contactProduct, addressLocation) {
					if(contactProduct.productLocationAndOrderConstructions == null) {
						return '';
					}
					for(var i = 0; i < contactProduct.productLocationAndOrderConstructions.length; i++) {
						if((contactProduct.productLocationAndOrderConstructions[i].address != null && addressLocation != null && contactProduct.productLocationAndOrderConstructions[i].address.id == addressLocation.id)|| 
							$scope.addressEqualsOverProperties(contactProduct.productLocationAndOrderConstructions[i], addressLocation)) {
							return contactProduct.productLocationAndOrderConstructions[i].orderConstruction;
						}
					}
					return '';
				};
				
				$scope.addName = function() {
					$scope.product.names.push('');
				};
				
				$scope.removeName = function(index) {
					$scope.product.names.splice(index, 1);
				};
				
				function setAddressWithProductsTimeout(addressWithProducts) {
		      		$timeout(function() {
		      			addressWithProducts.notSelectedError = false;
		  			   }, 2000); 
		      	}
				
				function setTimeout() {
		      		$timeout(function() {
		      			$scope.contactStillExists = false;
		      			$scope.noUniqueNameError = false;
		      			$scope.noUniqueNumberError = false;
		      			$scope.productCreatedSuccess = false;
		  			   }, 2000); 
		      	}
				
				$scope.hideAddressWithProducts = function(addressWithProducts) {
					var address = addressWithProducts.address;
					if(address == null) {
						return false;
					}
					if(address.userIdBlackList != null && address.userIdBlackList.indexOf($scope.currentUser.id) !== -1) {
						return true;
					}
					if(address.rolesBlackList != null && address.rolesBlackList.indexOf($scope.currentUser.projectUserConnectionRole) !== -1) {
						return true;
					}
					return false;
				};
				
				$scope.isRoleBlackListed = function(role, address) {
					if(address == null || address.rolesBlackList == null) {
						return false;
					}
					for(var i = 0; i < address.rolesBlackList.length; i++) {
						if(address.rolesBlackList[i] == role) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addBlackListRole = function(address, role) {
					if(address.rolesBlackList == null) {
						address.rolesBlackList = [];
					}
					address.rolesBlackList.push(role);
				};
				
				$scope.removeBlackListRole = function(address, role) {
					for(var i = 0; i < address.rolesBlackList.length; i++) {
						if(address.rolesBlackList[i] == role) {
							address.rolesBlackList.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.isBlackListed = function(user, address) {
					if(address == null || address.userIdBlackList == null) {
						return false;
					}
					for(var i = 0; i < address.userIdBlackList.length; i++) {
						if(address.userIdBlackList[i] == user.id) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addBlackListId = function(address, user) {
					if(address.userIdBlackList == null) {
						address.userIdBlackList = [];
					}
					address.userIdBlackList.push(user.id);
				};
				
				$scope.removeBlackListId = function(address, user) {
					for(var i = 0; i < address.userIdBlackList.length; i++) {
						if(address.userIdBlackList[i] == user.id) {
							address.userIdBlackList.splice(i, 1);
							break;
						}
					}
				};
			}
		};
		return directive;
		
		////////////
	}
})();
