(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('productOverviewModalService', productOverviewModalService);
        
    productOverviewModalService.$inject = ['$modal', '$stateParams'];
    
    function productOverviewModalService($modal, $stateParams) {
		var service = {
			showProductOverviewModal: showProductOverviewModal
		};		
		return service;
		
		////////////
				
		function showProductOverviewModal(product, invoker) {
			 var productOverviewModal = $modal.open({
				controller: ProductOverviewModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/products/modals/productOverviewModal/productOverview.modal.html'
			});
			return productOverviewModal;
			
			function ProductOverviewModalController($modalInstance, $scope, product, invoker) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.invoker = invoker; 
		    	
		    	vm.closeProductOverviewModal = closeProductOverviewModal;  
		    	
		    	////////////
		    	
		    	function closeProductOverviewModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();