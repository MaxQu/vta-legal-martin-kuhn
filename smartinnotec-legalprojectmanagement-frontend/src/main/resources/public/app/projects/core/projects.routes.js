(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.projects')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getProjectState());
    	 
    	////////////
			    	
    	function getProjectState() {
    		var state = {
    			name: 'auth.projects',
				url: '/projects/:userId',
				templateUrl: 'app/projects/projects/projects.html',
				controller: 'ProjectsController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					optionsService: 'optionsService',
					projectsService: 'projectsService',
					projectUserConnectionService: 'projectUserConnectionService',
					projectUserConnectionRoles: function(optionsService) {
						return optionsService.getRoles();
					},
					provinceTypes: function(optionsService) {
						return optionsService.getProvinceTypes();
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					}
				}
    		};
    		return state;
    	}
	}
})();