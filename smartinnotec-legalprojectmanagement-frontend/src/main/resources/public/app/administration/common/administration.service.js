(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('administrationService', administrationService);
        
    administrationService.$inject = ['$http', 'api_config'];
    
    function administrationService($http, api_config) {
		var service = {
			sendTestEmail: sendTestEmail
		};
		
		return service;
		
		////////////
		
		function sendTestEmail(receiver) {
			return $http.get(api_config.BASE_URL + '/testemails/testemail/' + receiver + '/');
		}
    }
})();
