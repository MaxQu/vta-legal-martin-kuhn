package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.TimeRegistration;

public interface TimeRegistrationRepository extends MongoRepository<TimeRegistration, String> {

    List<TimeRegistration> findByProject(final Project project);

    List<TimeRegistration> findByProjectAndDateTimeFromBetween(final Project project, final DateTime start, final DateTime end);

    List<TimeRegistration> findByProjectAndDateTimeUntilBetween(final Project project, final DateTime start, final DateTime end);

    @Query("{$text : { $search : ?0 } }")
    List<TimeRegistration> findBySearchString(final String searchString);

}
