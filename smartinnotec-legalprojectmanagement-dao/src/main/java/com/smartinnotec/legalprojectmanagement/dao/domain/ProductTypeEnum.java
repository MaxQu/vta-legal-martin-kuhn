package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ProductTypeEnum {

    LAB_CHEMICAL("LAB_CHEMICAL"),
    RAW_MATERIAL("RAW_MATERIAL"),
    TRADING_GOODS("TRADING_GOODS"),
    BLEND("BLEND");

    private String value;

    private ProductTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
