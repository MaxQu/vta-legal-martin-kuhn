package com.smartinnotec.legalprojectmanagement.service.schedule;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;

public interface HistoryAmountScheduleService {

    void createHistoryAmount() throws BusinessException;

}
