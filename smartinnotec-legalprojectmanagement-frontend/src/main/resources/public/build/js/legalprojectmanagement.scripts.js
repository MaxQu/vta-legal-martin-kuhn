(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.constants', [])
    	.constant('api_config', api_config())
    	.constant('token_config', token_config());
    
    function api_config() {
    	var constant = {
			BASE_URL: '/api'
    	};
    	return constant;
    }
    
    function token_config() {
    	var constant = {
			AUTH_TOKEN_KEY: 'legalProjectManagement-AuthToken',
			AUTH_TOKEN_HEADER: 'X-AUTH-TOKEN'
    	};
    	return constant;
    }  
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.common', [
    		'legalprojectmanagement.constants',
    		'ui.bootstrap.showErrors',
		    'LocalStorageModule',
		    'http-auth-interceptor'
    	]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement', [
            'legalprojectmanagement.home',
            'legalprojectmanagement.search',    
			'legalprojectmanagement.constants',
			'legalprojectmanagement.common',
			'legalprojectmanagement.error',
			'legalprojectmanagement.lang',
			'legalprojectmanagement.error',
			'legalprojectmanagement.user',
			'legalprojectmanagement.nav',
			'legalprojectmanagement.dashboard',
			'legalprojectmanagement.projects',
			'legalprojectmanagement.project',
			'legalprojectmanagement.administration',
			'legalprojectmanagement.superadministration',
			'legalprojectmanagement.contacts',
			'legalprojectmanagement.calendar',
			'legalprojectmanagement.history',
			'legalprojectmanagement.communication',
			'legalprojectmanagement.fuel',
			'legalprojectmanagement.timeregistration',
			'legalprojectmanagement.accesscontrol',
			'legalprojectmanagement.products',
			'legalprojectmanagement.reporting',
			'ui.router',
			'ngSanitize',
    	    'ui.bootstrap', 
    	    'ui.bootstrap.showErrors',
    	    'xeditable',
    	    'ui.bootstrap.datetimepicker',
    	    'angularjs-dropdown-multiselect',
    	    'ngFileUpload',
    	    'ui.sortable',
    	    'angular-smilies',
    	    'pdf',
    	    'ngToast',
    	    'ngContextMenu',
    	    'chart.js',
    	    'mwl.calendar',
    	    'infinite-scroll',
    	    'ui.toggle'
    	]);
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.error', [
    	    'ui.router'
    	]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.history', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.home', [
    	    'legalprojectmanagement.common',
    	    'legalprojectmanagement.constants',
    	    'ui.router', 
    	    'ui.bootstrap', 
    	    'ui.bootstrap.showErrors'
    	]);
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.lang', [
    		'pascalprecht.translate'
    	]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.nav', [
    	    'ui.router'
        ]);
})();
(function() {
    'use strict';

	console.log('fuel.module called');

    angular
    	.module('legalprojectmanagement.fuel', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.search', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.superadministration', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user', [
		    'pascalprecht.translate',
    	    'legalprojectmanagement.common',
    	    'legalprojectmanagement.constants',
    	    'ui.router', 
    	    'ui.bootstrap',
    	    'ui.validate'
        ]);  
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.accesscontrol', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.reporting', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();
(function() {
    'use strict';
        
    angular
    	.module('legalprojectmanagement.accesscontrol')
    	.controller('AccessControlController', AccessControlController);
        
    AccessControlController.$inject = ['$scope', 'accessControls', 'projects', 'defaultProject', 'dateUtilityService', 'accessControlService', 'printAccessControlModalService'];
         
    function AccessControlController($scope, accessControls, projects, defaultProjects, dateUtilityService, accessControlService, printAccessControlModalService) {
	    $scope.vm = this; 
    	var vm = this;
    	                   
    	vm.projects = projects.data;  
    	vm.defaultProjects = defaultProjects.data;
    	vm.selectedProject = vm.projects.length == 1 ? vm.projects[0] : (vm.defaultProject != null ? vm.defaultProject : null);
    	vm.accessControls = accessControls.data;
    	vm.filterStartDatePicker = false;
    	vm.filterEndDatePicker = false;
    	vm.filterStartDate = new Date();
    	vm.filterEndDate = new Date();
    	vm.activatedLiveAccessControl = false;
    	
    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
    	vm.getAccessControlsInRange = getAccessControlsInRange;
    	vm.showPrintAccessControlModalService = showPrintAccessControlModalService;
    	vm.activateLiveAccessControls = activateLiveAccessControls;
    	
    	////////////
    	
    	function openFilterStartDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterStartDatePicker = true;
        }
    	
    	function openFilterEndDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterEndDatePicker = true;
        }
    	
    	function getAccessControlsInRange() {
    		if(vm.filterStartDate == null || vm.filterEndDate == null) {
    			return;
    		}
    		var start = dateUtilityService.formatDateToString(vm.filterStartDate);
    		var end = dateUtilityService.formatDateToString(vm.filterEndDate);
    		accessControlService.getAccessControlsInRange(start, end).then(function (response) {
    			vm.accessControls = response.data;
			}).catch(function (data) {
				console.log('error in accesscontrol.controller#getAccessControlsInRange: ' + data);
			});
    	}
    
    	function showPrintAccessControlModalService() {
    		printAccessControlModalService.showPrintAccessControlModal(vm.accessControls);
    	}
    	
    	$scope.connect = function() {
            var socket = new SockJS('/api/accesscontrol');
            $scope.stompClient = Stomp.over(socket);            
            $scope.stompClient.connect({}, function(frame) {
            	$scope.stompClient.subscribe('/topic/accesscontrol/projectId', function(message) {
            		var parsedMessage = JSON.parse(message.body);
            		vm.accessControls.push(parsedMessage);
            		$scope.$apply();
                });
            });
        };
		
        $scope.unconnection = function() {
        	if($scope.stompClient != null) {
        		$scope.stompClient.unsubscribe();
        		$scope.stompClient.disconnect(function() {
                	console.log("Disconnected stomClient in accesscontrol.controller#destroy");
                });
            } else {
            	console.log("vm.stompClient is null and cannot be disconnected in accesscontrol.controller#destroy");
            } 
        };
        
		$scope.$on('$destroy', function() {  // invoked if page is leaved 
			$scope.unconnection();
    	});
		
		function activateLiveAccessControls() {
			if(vm.activatedLiveAccessControl === true) {
				$scope.unconnection();
			} else {
				$scope.connect();
			}
			vm.activatedLiveAccessControl = !vm.activatedLiveAccessControl;
		}
	} 
})();
(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.accesscontrol')
    	.factory('accessControlService', accessControlService);
        
    accessControlService.$inject = ['$http', 'api_config'];
    
    function accessControlService($http, api_config) {
		var service = {
			startListening: startListening,
			stopListening: stopListening,
			getAllAccessControls: getAllAccessControls,
			getAccessControlsInRange: getAccessControlsInRange
		};
		
		return service;
		 
		////////////
		
		function startListening() {
			return $http.get(api_config.BASE_URL + '/accesscontrols/accesscontrol/start');
		}
		
		function stopListening() {
			return $http.get(api_config.BASE_URL + '/accesscontrols/accesscontrol/stop');
		}
		
		function getAllAccessControls() {
			return $http.get(api_config.BASE_URL + '/accesscontrols/accesscontrol');
		}
		
		function getAccessControlsInRange(start, end) {
			return $http.get(api_config.BASE_URL + '/accesscontrols/accesscontrol/' + start + '/' + end + '/');
		}
    }
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.accesscontrol')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getAccessControlState());
    	 
    	////////////
			    	
    	function getAccessControlState() {
    		var state = {
    			name: 'auth.accesscontrol',
				url: '/accesscontrol/:userId',
				templateUrl: 'app/accesscontrol/accesscontrol/accesscontrol.html',
				controller: 'AccessControlController',
				controllerAs: 'vm',
				resolve: {
					accessControlService: 'accessControlService',
					projectsService: 'projectsService',
					accessControls: function(accessControlService) {
						return accessControlService.getAllAccessControls();
					},
					projects: function findProjectsOfUser($stateParams, projectsService) {
						return projectsService.findProjectsOfUser($stateParams.userId);
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.accesscontrol')
    	.factory('printAccessControlModalService', printAccessControlModalService);
        
    printAccessControlModalService.$inject = ['$modal', '$stateParams'];
    
    function printAccessControlModalService($modal, $stateParams) {
		var service = {
			showPrintAccessControlModal: showPrintAccessControlModal
		};		
		return service;
		
		////////////
				
		function showPrintAccessControlModal(accessControls) {
			 var printAccessControlModal = $modal.open({
				controller: PrintAccessControlModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	accessControls: function() { 
			    		return accessControls;
			    	}
			    }, 
				templateUrl: 'app/accesscontrol/modals/printAccessControl/printAccessControl.modal.html'
			});
			return printAccessControlModal;
			
			function PrintAccessControlModalController($modalInstance, $scope, accessControls) {
		    	var vm = this;
		    	vm.today = new Date();
		    	vm.accessControls = accessControls;
		    	
		    	vm.closePrintAccessControlModal = closePrintAccessControlModal;
		    	
		    	////////
		    	
		    	function closePrintAccessControlModal() {  
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() { 
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.controller('AdministrationController', AdministrationController);
    
    AdministrationController.$inject = ['$scope', '$timeout', 'currentUser', 'countryTypes', 'allLabelsPredefined', 'labelsPredefinedService', 'accessControlService', 'userRoles', 'allUsers', 'projectUserConnectionRoles', 'userAuthorizationUserContainer', 'calendarEventScheduleDays', 'projects', 'defaultProject', 'userService', 'calendarEventScheduleService', 'historyService', 'yesNoAdministrationModalService', 'calendarEventScheduleDaysService', 'userDuplicationService', 'authUserService', 'administrationService', 'dateUtilityService', 'optionsService', 'systemImportService', 'uploadService', 'contactsImportedService', 'contactsService', 'contactsImportedYesNoModalService', 'contactsYesNoModalService'];
       
    function AdministrationController($scope, $timeout, currentUser, countryTypes, allLabelsPredefined, labelsPredefinedService, accessControlService, userRoles, allUsers, projectUserConnectionRoles, userAuthorizationUserContainer, calendarEventScheduleDays, projects, defaultProject, userService, calendarEventScheduleService, historyService, yesNoAdministrationModalService, calendarEventScheduleDaysService, userDuplicationService, authUserService, administrationService, dateUtilityService, optionsService, systemImportService, uploadService, contactsImportedService, contactsService, contactsImportedYesNoModalService, contactsYesNoModalService) {
	    $scope.vm = this; 
    	var vm = this;
    	  
    	vm.allLabelsPredefined = allLabelsPredefined.data;
    	vm.userRoles = userRoles.data; 
    	vm.countryTypes = countryTypes.data;
    	vm.currentUser = currentUser;
    	vm.allUsers = allUsers.data;
    	vm.projectUserConnectionRoles = projectUserConnectionRoles.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.calendarEventScheduleDays = calendarEventScheduleDays.data;
    	vm.projects = projects.data;
    	vm.defaultProject = defaultProject.data;
    	vm.selectedProject = vm.projects.length == 1 ? vm.projects[0] : (vm.defaultProject != null ? vm.defaultProject : null);
    	vm.targetProject = null;
    	vm.newLabel = null;
    	vm.labelStillExistsError = false;
    	vm.foundedUsers = null;
    	vm.sendCalendarEventRememberingMessageTrigger = false;  
    	vm.userAuthenticationUserContainerTrigger = false;
    	vm.sendHistoryMessageTrigger = false; 
    	vm.sendCalendarEventOutsideWorkRememberingMessageTrigger = false;
    	vm.labelsPredefinedType = null;
    	vm.labelsPredefinedTypeError = false;
    	vm.calendarEventScheduleDayUpdated = false;
    	vm.orderType = 'labelsPredefinedType';
    	vm.duplicateUserFrom = null;
    	vm.duplicateUserTo = null;
    	vm.duplicateUserFromSelectionError = false;
    	vm.duplicateUserToSelectionError = false;
    	vm.userDuplicationSuccess = false;
    	vm.userDuplicationError = false;
    	vm.duplicationSecurity = false;
    	vm.userRegistered = false;
    	vm.testEmailSent = false;
    	vm.mergeContactsWithAddressesAndSaveTrigger = false;
    	vm.mergeContactsWithAddressesAndSaveOfStandardPathTrigger = false;
    	vm.pathAddress = '';
    	vm.pathContact = '';
    	vm.systemImportNoPathAddressError = false; 
    	vm.systemImportNoPathContactError = false;
    	vm.generalSystemImportError = false;
    	vm.systemImportContactFileNotFoundError = false;
    	vm.systemImportAddressFileNotFoundError = false;
    	vm.mergeContactsWithAddressesAndSaveSuccess = false;
    	vm.systemImportContactFileNotReadableError = false;
    	vm.systemImportAddressFileNotReadableError = false;
    	vm.contactFiles = null;
    	vm.addressFiles = null;
    	vm.pathContactSet = false;
    	vm.pathAddressSet = false;
    	vm.contactsImportedDeleted = false;
    	vm.contactsDeleted = false;
    	vm.amountOfContactsImported = null;
    	vm.amountOfContacts = null;
    	vm.amountOfCustomerContacts = null;
    	vm.selectedCountry = null;

    	vm.addNewLabel = addNewLabel;
    	vm.labelStillExists = labelStillExists;
    	vm.setLabelCreatedTimeout = setLabelCreatedTimeout;
    	vm.deleteLabel = deleteLabel;
    	vm.searchUsersByTerm = searchUsersByTerm;
    	vm.sendCalendarEventRememberingMessage = sendCalendarEventRememberingMessage;
    	vm.updateUser = updateUser;
    	vm.deleteUser = deleteUser;
    	vm.sendHistoryMessages = sendHistoryMessages;
    	vm.updatePassword = updatePassword;
    	vm.saveUserAuthorizationUserContainer = saveUserAuthorizationUserContainer;
    	vm.updateUserAuthorizationUserContainer = updateUserAuthorizationUserContainer;
    	vm.deleteUserAuthorizationUserContainer = deleteUserAuthorizationUserContainer;
    	vm.showYesNoAdministrationModalService = showYesNoAdministrationModalService;
    	vm.addCalendarEventScheduleDay = addCalendarEventScheduleDay;
    	vm.addCalendarEventScheduleDay = addCalendarEventScheduleDay;
    	vm.updateCalendarEventScheduleDay = updateCalendarEventScheduleDay;
    	vm.removeCalendarEventScheduleDay = removeCalendarEventScheduleDay;
    	vm.addSelectedProject = addSelectedProject;
    	vm.removeSelectedProject = removeSelectedProject;
    	vm.sendCalendarEventOutsideWorkRememberingMessages = sendCalendarEventOutsideWorkRememberingMessages;
    	vm.addTargetProject = addTargetProject;
    	vm.removeTargetProject = removeTargetProject;
    	vm.duplicateUser = duplicateUser;
    	vm.startListening = startListening;
    	vm.stopListening = stopListening;
    	vm.addAgentNumberContainer = addAgentNumberContainer;
    	vm.removeAgentNumberContainer = removeAgentNumberContainer;
    	vm.showDeleteAllContactsImported = showDeleteAllContactsImported;
    	vm.deleteAllContactsImported = deleteAllContactsImported;
    	vm.showDeleteAllCustomerContacts = showDeleteAllCustomerContacts;
    	vm.deleteAllCustomerContacts = deleteAllCustomerContacts;
    	vm.signup = signup;
    	vm.sendTestEmail = sendTestEmail;
    	vm.mergeContactsWithAddressesAndSave = mergeContactsWithAddressesAndSave;
    	vm.mergeContactsWithAddressesAndSaveOfStandardPath = mergeContactsWithAddressesAndSaveOfStandardPath;
    	getAmountOfContacts();
    	getAmountOfCustomerContacts();
    	getAmountOfContactsImported();
    	
    	////////////
    	
    	$scope.$watch('vm.contactFiles', function () {
    		uploadService.uploadSystemImportFile(vm.contactFiles, vm.currentUser.id, 'CONTACT_FILE', successCallback, errorCallback, progressCallback);
        });
    	
    	$scope.$watch('vm.addressFiles', function () {
    		uploadService.uploadSystemImportFile(vm.addressFiles, vm.currentUser.id, 'ADDRESS_FILE', successCallback, errorCallback, progressCallback);
        });
    	
    	function successCallback(response) {
    		if(response.data.pathContact != null) {
    			vm.pathContact = response.data.pathContact;
    			vm.pathContactSet = true;
    		}
    		if(response.data.pathAddress != null) {
    			vm.pathAddress = response.data.pathAddress;
    			vm.pathAddressSet = true;
    		}
    		systemImportTimeout();
    	}
    	
    	function errorCallback(response) {
    		console.log('errorCallback: ' + response);
    	}
    	
    	function progressCallback(response) {
    		console.log('progressCallback: ' + response);
    	}
    	
    	function getAmountOfContacts() {
    		contactsService.countByTenantAndActive(vm.currentUser.id).then(function(response) {
				vm.amountOfContacts = response.data;
	   		 }, function errorCallback(response) {
	   			  console.log('error administration.controller#getAmountOfContacts');
	   		 });
    	}
    	
    	function getAmountOfCustomerContacts() {
    		contactsService.countCustomerContactsByTenantAndActive(vm.currentUser.id).then(function(response) {
				vm.amountOfCustomerContacts = response.data;
	   		 }, function errorCallback(response) {
	   			  console.log('error administration.controller#getAmountsOfCustomerContacts');
	   		 });
    	}
    	
    	function getAmountOfContactsImported() {
			contactsImportedService.getAmountOfContactsImported().then(function(response) {
				vm.amountOfContactsImported = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#amountOfContactsImported');
 	   		 });
		}
    	
    	function showDeleteAllContactsImported() {
    		contactsImportedYesNoModalService.showContactsImportedYesNoModal(vm);
    	}
    	
    	function deleteAllContactsImported() {
    		contactsImportedService.deleteAllContactsImported().then(function(response) {
    			vm.contactsImportedDeleted = true;
    			systemImportTimeout();
    			getAmountOfContactsImported();
    			getAmountOfCustomerContacts();
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#deleteAllContactsImported');
 	   		 });
    	}
    	
    	function showDeleteAllCustomerContacts() {
    		contactsYesNoModalService.showContactsYesNoModal(vm, vm.amountOfCustomerContacts, vm.selectedCountry);
    	}
    	
    	function deleteAllCustomerContacts() {
    		contactsService.deleteAllCustomerContacts(vm.selectedCountry).then(function(response) {
    			vm.contactsDeleted = true;
    			systemImportTimeout();
    			getAmountOfContacts();
    			getAmountOfCustomerContacts();
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#deleteAllContacts');
 	   		 });
    	}
    	
    	function setSendCalendarEventRememberingMessage() {
    		$timeout(function() {
    			vm.sendCalendarEventRememberingMessageTrigger = false;
    			vm.sendHistoryMessageTrigger = false;
    			vm.sendCalendarEventOutsideWorkRememberingMessageTrigger = false;
			   }, 2000); 
    	}
    	
    	function setLabelCreatedTimeout() {
    		$timeout(function() {
				vm.createLabelClicked = false;
			   }, 2000); 
    	}
    	
    	function systemImportTimeout() {
    		$timeout(function() {
    			vm.systemImportNoPathAddressError = false; 
    	    	vm.systemImportNoPathContactError = false;
    	    	vm.generalSystemImportError = false;
    	    	vm.systemImportContactFileNotFoundError = false;
    	    	vm.systemImportAddressFileNotFoundError = false;
    	    	vm.pathContactSet = false;
    	    	vm.pathAddressSet = false;
    	    	vm.contactsImportedDeleted = false;
    	    	vm.contactsDeleted = false;
    	    	vm.mergeContactsWithAddressesAndSaveSuccess = false;
    	    	vm.systemImportContactFileNotReadableError = false;
    	    	vm.systemImportAddressFileNotReadableError = false;
    	    	vm.mergeContactsWithAddressesAndSaveOfStandardPathTrigger = false;
    	    	vm.systemImportContactFileOfStandardPathNotFoundError = false;
			   }, 3000); 
    	}
    	
    	function setUserDeletedTimeout(user) {
    		$timeout(function() {
    			user.userDeleted = false;
			   }, 2000); 
    	}
    	
    	function setUserRegisteredTimeout() {
    		$timeout(function() {
    			vm.userRegistered = false;
			   }, 2000); 
    	}
    	
    	function testEmailSentTimeout() {
    		$timeout(function() {
    			vm.testEmailSent = false;
			   }, 2000); 
    	}
    	
    	function setCalendarEventScheduleDayUpdated(calendarEventScheduleDay) {
    		$timeout(function() {
    			calendarEventScheduleDay.calendarEventScheduleDayUpdated = false;
			   }, 2000); 
    	}
    	
    	function setUserAuthenticationUserContainerTimeout() {
    		$timeout(function() {
				vm.userAuthenticationUserContainerTrigger = false;
			   }, 2000); 
    	}
    	
    	function setDuplicationTimeout() {
    		$timeout(function() {
    			vm.userDuplicationSuccess = false;
    	    	vm.userDuplicationError = false;
			   }, 6000); 
    	}
    	
    	function setAccessControlTimeout() {
    		$timeout(function() {
    			vm.accessControlStarted = false;
    			vm.accessControlStopped = false;
			   }, 2000); 
    	}
    	
    	function addCalendarEventScheduleDay() {
    		var calendarEventScheduleDay = {};
    		calendarEventScheduleDay.days = 1;
    		calendarEventScheduleDay.tenant = currentUser.tenant;
    		calendarEventScheduleDaysService.create(calendarEventScheduleDay).then(function(response) {
    			vm.calendarEventScheduleDays.push(response.data);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#addCalendarEventScheduleDay');
  	   		 });
    	}
    	
    	function updateCalendarEventScheduleDay(calendarEventScheduleDay) {
    		calendarEventScheduleDaysService.update(calendarEventScheduleDay).then(function(response) {
    			calendarEventScheduleDay.calendarEventScheduleDayUpdated = true;
    			setCalendarEventScheduleDayUpdated(calendarEventScheduleDay);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#updateCalendarEventScheduleDay');
  	   		 });
    	}
    	
    	function removeCalendarEventScheduleDay(calendarEventScheduleDay) {
    		calendarEventScheduleDaysService.deleteCalendarEventScheduleDays(calendarEventScheduleDay.id).then(function(response) {
    			for(var i = 0; i < vm.calendarEventScheduleDays.length; i++) {
    				if(vm.calendarEventScheduleDays[i].id == calendarEventScheduleDay.id) {
    					vm.calendarEventScheduleDays.splice(i, 1);
    				}
    			}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#removeCalendarEventScheduleDay');
  	   		 });
    	}
    	
    	function sendHistoryMessages() {
    		historyService.sendHistoryMessages();
    		vm.sendHistoryMessageTrigger = true;
    		setSendCalendarEventRememberingMessage();
    	}
    	
    	function sendCalendarEventRememberingMessage() {
    		calendarEventScheduleService.sendCalendarEventRememberingMessage().then(function(response) {
    			vm.sendCalendarEventRememberingMessageTrigger = true;
    			setSendCalendarEventRememberingMessage();
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#sendCalendarEventRememberingMessage');
  	   		 });
    	}
    	
    	function sendCalendarEventOutsideWorkRememberingMessages() {
    		calendarEventScheduleService.sendCalendarEventOutsideWorkRememberingMessage().then(function(response) {
    			vm.sendCalendarEventOutsideWorkRememberingMessageTrigger = true;
    			setSendCalendarEventRememberingMessage();
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#sendCalendarEventOutsideWorkRememberingMessages');
  	   		 });
    	}
    	
    	function updateUser(user, type) {
			userService.updateUser(user).success(function(data) {
				setUpdateModeType(user, type, 'SUCCESS');
				setUpdateUserTimeout(user);
			}).error(function(data) { 
				setUpdateModeType(user, type, 'ERROR');
				setUpdateUserTimeout(user);
				console.log('error in administration.controller.js#updateUser: ' + data);
			});
		}
    	
    	function showYesNoAdministrationModalService(user) {
    		yesNoAdministrationModalService.showYesNoModal(vm, user);
    	}
    	
    	function deleteUser(user) {
    		userService.deleteUser(user).success(function(data) {
    			user.userDeleted = true;
    			user.userDeletionError = false;
    			setUserDeletedTimeout(user);
    			removeUser(user);
			}).error(function(data) {
				user.userDeletionError = true;
				user.userDeletionErrorMessage = data.message;
			});
		}
    	
    	function removeUser(user) {
    		for(var i = 0; i < vm.foundedUsers.length; i++) {
    			if(vm.foundedUsers[i].id == user.id) {
    				vm.foundedUsers.splice(i, 1);
    				break;
    			}
    		}
    	}
    	
    	function updatePassword(user, type) {
			userService.updatePassword(user.id, user.password).success(function(data) {
				setUpdateModeType(user, type, 'SUCCESS');
				setUpdateUserTimeout(user);
			}).error(function(data) {
				setUpdateModeType(user, type, 'ERROR');
				setUpdateUserTimeout(user);
				console.log('error in administration.controller.js#updatePassword: ' + data);
			});
		}
    	
    	function setUpdateUserTimeout(user) {
      		$timeout(function() {
      			user.nameSuccess = null;
      			user.countrySuccess = null;
      			user.locationSuccess = null;
      			user.streetSuccess = null;
      			user.telephoneSuccess = null;
      			user.emailSuccess = null;
      			user.birthdaySuccess = null;
      			user.emailActiveSuccess = null;
      			user.userActiveSuccess = null;
      			user.userRoleSuccess = null;
      			user.usernameSuccess = null;
      			user.passwordSuccess = null;
      			user.roleSuccess = null;
      			
      			user.nameError = null;
      			user.countryError = null;
      			user.locationError = null;
      			user.streetError = null;
      			user.telephoneError = null;
      			user.emailError = null;
      			user.birthdayError = null;
      			user.emailActiveError = null;
      			user.userRoleError = null;
      			user.usernameError = null;
      			user.passwordError = null;
      			user.roleError = null;
      			user.agentNumberContainersSuccess = null;
      			user.agentNumberContainersError = null;
      			user.allContactsAvailableSuccess = null;
      			user.allContactsAvailableError = null;
  			   }, 2000); 
      	}
    	
    	function setUpdateModeType(user, type, mode) {
			switch(type) {
			case 'name':
				if(mode == 'SUCCESS') {
					user.nameSuccess = true;
				} else if(mode == 'ERROR') {
					user.nameError = true;
				}
				break;
			case 'country':
				if(mode == 'SUCCESS') {
					user.countrySuccess = true;
				} else if(mode == 'ERROR') {
					user.countryError = true;
				}
				break;
			case 'location':
				if(mode == 'SUCCESS') {
					user.locationSuccess = true;
				} else if(mode == 'ERROR') {
					user.locationError = true;
				}
				break;
			case 'street':
				if(mode == 'SUCCESS') {
					user.streetSuccess = true;
				} else if(mode == 'ERROR') {
					user.streetError = true;
				}
				break;
			case 'telephone':
				if(mode == 'SUCCESS') {
					user.telephoneSuccess = true;
				} else if(mode == 'ERROR') {
					user.telephoneError = true;
				}
				break;
			case 'email': 
				if(mode == 'SUCCESS') {
					user.emailSuccess = true;
				} else if(mode == 'ERROR') {
					user.emailError = true;
				}
				break;
			case 'birthday':
				if(mode == 'SUCCESS') {
					user.birthdaySuccess = true;
				} else if(mode == 'ERROR') {
					user.birthdayError = true;
				}
				break;
			case 'emailActive':
				if(mode == 'SUCCESS') {
					user.emailActiveSuccess = true;
				} else if(mode == 'ERROR') {
					user.emailActiveError = true;
				}
				break;
			case 'userActive':
				if(mode == 'SUCCESS') {
					user.userActiveSuccess = true;
				} else if(mode == 'ERROR') {
					user.userActiveError = true;
				}
				break;
			case 'userRole':
				if(mode == 'SUCCESS') {
					user.userRoleSuccess = true;
				} else if(mode == 'ERROR') {
					user.userRoleError = true;
				}
				break;
			case 'username':
				if(mode == 'SUCCESS') {
					user.usernameSuccess = true;
				} else if(mode == 'ERROR') {
					user.usernameError = true;
				}
				break;
			case 'password':
				if(mode == 'SUCCESS') {
					user.passwordSuccess = true;
				} else if(mode == 'ERROR') {
					user.passwordError = true;
				}
				break;
			case 'role':
				if(mode == 'SUCCESS') {
					user.roleSuccess = true;
				} else if(mode == 'ERROR') {
					user.roleError = true;
				}
				break;
			case 'agentNumberContainers':
				if(mode == 'SUCCESS') {
					user.agentNumberContainersSuccess = true;
				} else if(mode == 'ERROR') {
					user.agentNumberContainersError = true;
				}
				break;
			case 'allContactsAvailable':
				if(mode == 'SUCCESS') {
					user.allContactsAvailableSuccess = true;
				} else if(mode == 'ERROR') {
					user.allContactsAvailableError = true;
				}
				break;
			}
		}
    	
    	function addNewLabel(newLabel) {
    		vm.labelStillExistsError = false;
    		vm.labelsPredefinedTypeError = false;
    		if(labelStillExists(newLabel)) {
    			return;
    		}
    		if(vm.labelsPredefinedType == null || vm.labelsPredefinedType === false) {
    			vm.labelsPredefinedTypeError = true;
    			return;  
    		}
    		var labelPredefined = {};
    		labelPredefined.name = newLabel;
    		labelPredefined.labelsPredefinedType = vm.labelsPredefinedType;
    		labelsPredefinedService.createLabelPredefined(labelPredefined).then(function(response) {
    			vm.allLabelsPredefined.push(response.data);
    			vm.newLabel = null;
    			vm.createLabelClicked = true;
    			setLabelCreatedTimeout();
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#addNewLabel');
  	   		 });
    	}
    	
    	function labelStillExists(newLabel) {
    		vm.labelStillExistsError = false;
    		for(var i = 0; i < vm.allLabelsPredefined.length; i++) {
    			if(vm.allLabelsPredefined[i].name == newLabel) {
    				vm.allLabelsPredefined[i].stillExists = true;
    				vm.labelStillExistsError = true;
    			} else {
    				vm.allLabelsPredefined[i].stillExists = false;
    			}
    		}
    		return vm.labelStillExistsError;
    	}
    	
    	function deleteLabel(label) {
    		labelsPredefinedService.deleteLabelPredefined(label.id).then(function(response) {
    			removeLabelPredefined(label);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#deleteLabel');
  	   		 });
    	}
    	
    	function removeLabelPredefined(labelPredefined) {
    		for(var i = 0; i < vm.allLabelsPredefined.length; i++) {
    			if(vm.allLabelsPredefined[i].id == labelPredefined.id) {
    				vm.allLabelsPredefined.splice(i, 1);
    			}
    		}
    	}
    	
    	function searchUsersByTerm(term) {
    		if(typeof term == 'undefined' || term == null || term.length < 2) {
    			vm.foundedUsers = null;
    			return;
    		}
    		userService.findUsersByTerm(term).then(function(response) {
    			vm.foundedUsers = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#searchUsersByTerm');
 	   		 });
    	}
    	
    	function addSelectedProject(project) {
    		removeProjectSelection();
    		project.selected = true;
    		vm.selectedProject = project;
    	}
    	
    	function removeSelectedProject(project) {
    		project.selected = false;
    		vm.selectedProject = null;
    		removeProjectSelection();
    	}
    	
    	function addTargetProject(project) {
    		vm.targetProject = project;
    	}
    	
    	function removeTargetProject(project) {
    		vm.targetProject = null;
    	}
    	
    	function removeProjectSelection() {
    		for(var i = 0; i < vm.projects.length; i++) {
    			vm.projects[i].selected = false;
    		}
    	}
    	
    	function saveUserAuthorizationUserContainer(user, userAuthorizationUserContainer, userAuthorization) {
    		userAuthorization.state = !userAuthorization.state;
    		userService.updateUserAuthorizationUserContainerOfUser(userAuthorizationUserContainer).then(function(response) {
    			vm.userAuthenticationUserContainerTrigger = true;
    			setUserAuthenticationUserContainerTimeout();
    			userAuthorizationUserContainer = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#deleteLabel');
 	   		 });
    	}
    	
    	function updateUserAuthorizationUserContainer(user, projectUserConnectionRole) {
    		userService.createUserAuthorizationUserContainer(user.id, projectUserConnectionRole).then(function(response) {
    			var userAuthorizationUserContainer = response.data;
    			user.userAuthorizationUserContainer = userAuthorizationUserContainer;
    			loadCurrentUser(user);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#updateUserAuthorizationUserContainer');
 	   		 });
    	}
    	
    	function deleteUserAuthorizationUserContainer(user, userAuthorizationUserContainer) {
    		userService.deleteUserAuthorizationUserContainerOfUser(user.id, userAuthorizationUserContainer.id).then(function(response) {
    			user.userAuthorizationUserContainer = null;
    			loadCurrentUser(user);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#deleteUserAuthorizationUserContainer');
 	   		 });
    	}
    	
    	function loadCurrentUser(user) {
    		userService.getUserById(user.id).then(function(response) {
    			user = response.data;
    			updateUserInUsers(user);
	   		 }, function errorCallback(response) {
	   			  console.log('error administration.controller#loadCurrentUser');
	   		 });
    	}
    	
    	function updateUserInUsers(user) {
    		for(var i = 0; i < vm.allUsers.length; i++) {
    			if(vm.allUsers[i].id == user.id) {
    				vm.allUsers[i] = user;
    			}
    		}
    	}
    	
    	function duplicateUser() {
    		if(vm.duplicateUserFrom == null) {
    			vm.duplicateUserFromSelectionError = true;
    			return;
    		}
    		vm.duplicateUserFromSelectionError = false;
    		if(vm.duplicateUserTo == null) {
    			vm.duplicateUserToSelectionError = true;
    			return;
    		}
    		vm.duplicateUserToSelectionError = false;
    		if(vm.duplicateUserFrom.id == vm.duplicateUserTo.id) {
    			vm.duplicateUserFromSelectionError = true;
    			vm.duplicateUserToSelectionError = true;
    			return;
    		}
    		vm.duplicateUserFromSelectionError = false;
    		vm.duplicateUserToSelectionError = false;
    		 
    		userDuplicationService.duplicateUser(vm.duplicateUserFrom.id, vm.duplicateUserTo.id).then(function(response) {
    			vm.userDuplicationSuccess = true;
    			vm.userDuplicationError = false;
    			vm.duplicationSecurity = false;
    			setDuplicationTimeout();
	   		 }, function errorCallback(response) {
	   			vm.userDuplicationSuccess = false;
	   			vm.userDuplicationError = true;
	   			console.log('error administration.controller#duplicateUser'); 
	   		 });
    	}
    	
    	function startListening() {
    		accessControlService.startListening().then(function(response) {
    			vm.accessControlStarted = true;
    			setAccessControlTimeout();
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#startListening');
	   		 });
    	}
    	
    	function stopListening() {
    		accessControlService.stopListening().then(function(response) {
    			vm.accessControlStopped = true;
    			setAccessControlTimeout();
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#stopListening');
	   		 });
    	}
    	
    	function findAllUsers() {
    		userService.findAllUsers().then(function(response) {
    			vm.allUsers = response.data;
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#findAllUsers');
	   		 });
    	}
    	
    	function signup(newUser, callback, errorCallback) {   
			newUser.active = true;
			authUserService.signup(newUser).then(function () {
				vm.userRegistered = true;
				findAllUsers();
				setUserRegisteredTimeout();
				callback(newUser);
			}).catch(function (response) {
				vm.signupError = response.data.errorCodeType;
				console.log("error administration.controller#signup: " + response.data.message);
				errorCallback(response);
			});
		}
    	
    	function addAgentNumberContainer(user) {
    		if(user.agentNumberContainers == null) {
    			user.agentNumberContainers = [];
    		}
    		var agentNumberContainer = {};
    		agentNumberContainer.customerNumber = '';
    		user.agentNumberContainers.push(agentNumberContainer);
    	}
    	
    	function removeAgentNumberContainer(user, index) {
    		user.agentNumberContainers.splice(index, 1);
    		updateUser(user, 'agentNumberContainers');
    	}
    	
    	function sendTestEmail(testEmailAddress) {
    		administrationService.sendTestEmail(testEmailAddress).then(function(response) {
    			vm.testEmailSent = true;
    			testEmailSentTimeout();
    			console.log('success sendTestEmail');
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#sendTestEmail');
	   		 });
    	}
    	
    	function mergeContactsWithAddressesAndSave(pathAddress, pathContact) {
    		vm.mergeContactsWithAddressesAndSaveTrigger = true;
    		systemImportService.mergeContactsWithAddressesAndSave(pathAddress, pathContact, mergeContactsWithAddressesAndSaveCallback, errorMergeContactsWithAddressesAndSaveCallback);
    	}
    	
    	function mergeContactsWithAddressesAndSaveOfStandardPath() {
    		vm.mergeContactsWithAddressesAndSaveOfStandardPathTrigger = true;
    		systemImportService.mergeContactsWithAddressesAndSaveOfStandardPath().then(function(response) {
    			vm.mergeContactsWithAddressesAndSaveOfStandardPathTrigger = false;
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#mergeContactsWithAddressesAndSaveOfStandardPath');
	   			if(response.data.error === 'CONTACT_FILE_NOT_FOUND') {
	    			vm.systemImportContactFileOfStandardPathNotFoundError = true;
	    			systemImportTimeout();
	   			}
	   		 });
    	}
    	
    	function mergeContactsWithAddressesAndSaveCallback(response) {
    		vm.mergeContactsWithAddressesAndSaveTrigger = false;
    		vm.mergeContactsWithAddressesAndSaveSuccess = true;
    		systemImportTimeout();
    		getAmountOfContactsImported();
    	}
    	
    	function errorMergeContactsWithAddressesAndSaveCallback(response) {
    		if(response.data.error === 'NO_PATH_ADDRESS') {
    			vm.systemImportNoPathAddressError = true; 
    		} else if(response.data.error === 'NO_PATH_CONTACT') {
    			vm.systemImportNoPathContactError = true;
    		} else if(response.data.error === 'CONTACT_FILE_NOT_FOUND') {
    			vm.systemImportContactFileNotFoundError = true;
    		} else if(response.data.error === 'ADDRESS_FILE_NOT_FOUND') {
    			vm.systemImportAddressFileNotFoundError = true;
    		} else if(response.data.error === 'CONTACT_FILE_NOT_READABLE') {
    			vm.systemImportContactFileNotReadableError = true;
    		} else if(response.data.error === 'ADDRESS_FILE_NOT_READABLE') {
    			vm.systemImportAddressFileNotReadableError = true;
    		} else {
    			vm.generalSystemImportError = true;
    		}
    		systemImportTimeout();
    		vm.mergeContactsWithAddressesAndSaveTrigger = false;
    	}    	
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('administrationService', administrationService);
        
    administrationService.$inject = ['$http', 'api_config'];
    
    function administrationService($http, api_config) {
		var service = {
			sendTestEmail: sendTestEmail
		};
		
		return service;
		
		////////////
		
		function sendTestEmail(receiver) {
			return $http.get(api_config.BASE_URL + '/testemails/testemail/' + receiver + '/');
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('calendarEventScheduleDaysService', calendarEventScheduleDaysService);
        
    calendarEventScheduleDaysService.$inject = ['$http', 'api_config'];
    
    function calendarEventScheduleDaysService($http, api_config) {
		var service = {
				findCalendarEventScheduleDaysByTenant: findCalendarEventScheduleDaysByTenant,
				create: create,
				update: update,
				deleteCalendarEventScheduleDays: deleteCalendarEventScheduleDays
		};
		
		return service;
		
		////////////
		
		function findCalendarEventScheduleDaysByTenant(userId) {
			return $http.get(api_config.BASE_URL + '/calendareventscheduledays/calendareventscheduleday/' + userId);
		}
		
		function create(calendarEventScheduleDays) {
			return $http.post(api_config.BASE_URL + '/calendareventscheduledays/calendareventscheduleday', calendarEventScheduleDays);
		}
		
		function update(calendarEventScheduleDays) {
			return $http.put(api_config.BASE_URL + '/calendareventscheduledays/calendareventscheduleday', calendarEventScheduleDays);
		}
		
		function deleteCalendarEventScheduleDays(id) {
			return $http.delete(api_config.BASE_URL + '/calendareventscheduledays/calendareventscheduleday/' + id);
		}
		
    }
})();

(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.administration')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getAdministrationState());
    	 
    	////////////
			    	
    	function getAdministrationState() {
    		var state = {
    			name: 'auth.administration',
				url: '/administration/:userId',
				templateUrl: 'app/administration/administration/administration.html',
				controller: 'AdministrationController',
				controllerAs: 'vm',
				resolve: {
					optionsService: 'optionsService',
					projectsService: 'projectsService',
					labelsPredefinedService: 'labelsPredefinedService',
					calendarEventScheduleDaysService: 'calendarEventScheduleDaysService',
					allLabelsPredefined: function getAllLabelsPredefined($stateParams, labelsPredefinedService) {
						return [];
					},
			    	userRoles: function getUserRoles(optionsService) {
			    		return optionsService.getUserRoles();
			    	},
			    	allUsers: function findAllUsers(userService) {
			    		return userService.findAllUsers();
			    	},
			    	projectUserConnectionRoles: function getProjectUserConnectionRoles(optionsService) {
			    		return optionsService.getRoles();
			    	},
			    	userAuthorizationUserContainer: function($stateParams, userService) {
			    		return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
			    	},
			    	calendarEventScheduleDays: function($stateParams, calendarEventScheduleDaysService) {
			    		return calendarEventScheduleDaysService.findCalendarEventScheduleDaysByTenant($stateParams.userId);
			    	},
					projects: function getAllProjects(projectsService) {
						return projectsService.findAllProjects();
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					},
					countryTypes: function getCountryTypes(optionsService) {
						return optionsService.getCountryTypes();
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('contactsImportedYesNoModalService', contactsImportedYesNoModalService);
        
    contactsImportedYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function contactsImportedYesNoModalService($modal, $stateParams) {
		var service = {
			showContactsImportedYesNoModal: showContactsImportedYesNoModal
		};		
		return service;
		
		////////////
				
		function showContactsImportedYesNoModal(invoker) {
			 var contactsImportedYesNoModal = $modal.open({
				controller: ContactsImportedYesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/administration/modals/contactsImportedYesNoModal/contactsImportedYesNo.modal.html'
			});
			return contactsImportedYesNoModal;
			
			function ContactsImportedYesNoModalController($modalInstance, $scope, invoker) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	
		    	vm.contactsImportedDeleteAll = contactsImportedDeleteAll;
		    	vm.closeContactsImportedYesNoModal = closeContactsImportedYesNoModal;
		    	
		    	////////////
		    	
		    	function contactsImportedDeleteAll() {
		    		vm.invoker.deleteAllContactsImported();
		    		vm.closeContactsImportedYesNoModal();
		    	}
		    	
		    	function closeContactsImportedYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('contactsYesNoModalService', contactsYesNoModalService);
        
    contactsYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function contactsYesNoModalService($modal, $stateParams) {
		var service = {
			showContactsYesNoModal: showContactsYesNoModal
		};		
		return service;
		
		////////////
				
		function showContactsYesNoModal(invoker, amount, selectedCountry) {
			 var contactsYesNoModal = $modal.open({
				controller: ContactsYesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	amount: function() {
			    		return amount;
			    	},
			    	selectedCountry: function() {
			    		return selectedCountry;
			    	}
			    }, 
				templateUrl: 'app/administration/modals/contactsYesNoModal/contactsYesNo.modal.html'
			});
			return contactsYesNoModal;
			
			function ContactsYesNoModalController($modalInstance, $scope, invoker, amount, selectedCountry) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.amount = amount;
		    	vm.selectedCountry = selectedCountry;
		    	
		    	vm.contactsDeleteAll = contactsDeleteAll;
		    	vm.closeContactsYesNoModal = closeContactsYesNoModal;
		    	
		    	////////////
		    	
		    	function contactsDeleteAll() {
		    		vm.invoker.deleteAllCustomerContacts(vm.selectedCountry);
		    		vm.closeContactsYesNoModal();
		    	}
		    	
		    	function closeContactsYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('yesNoAdministrationModalService', yesNoAdministrationModalService);
        
    yesNoAdministrationModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoAdministrationModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, user) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	user: function() {
			    		return user;
			    	}
			    }, 
				templateUrl: 'app/administration/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, user) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.user = user;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.deleteUser(vm.user);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.controller('CalendarController', CalendarController);
    
    CalendarController.$inject = ['$scope', '$filter', 'calendarConfig', 'moment', 'calendarEventSerialDateTypes', 'calendarEventTypeEnums', 'currentUser', 'defaultProject', 'userAuthorizationUserContainer', 'optionsService', 'projectsService', 'calendarEvents', 'createNewEventModalService', 'contactAndUserService', 'deleteCalendarEventModalService', 'userOfCalendarEventModalService', 'calendarService', 'printCalendarEventsModalService', 'calendarEventUserConnectionService', 'calendarICSLinkModalService', 'calendarEventClickedModalService', 'calendarEventInListModalService', 'dateUtilityService'];
    
    function CalendarController($scope, $filter, calendarConfig, moment, calendarEventSerialDateTypes, calendarEventTypeEnums, currentUser, defaultProject, userAuthorizationUserContainer, optionsService, projectsService, calendarEvents, createNewEventModalService, contactAndUserService, deleteCalendarEventModalService, userOfCalendarEventModalService, calendarService, printCalendarEventsModalService, calendarEventUserConnectionService, calendarICSLinkModalService, calendarEventClickedModalService, calendarEventInListModalService, dateUtilityService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	moment.locale('de', { 
  		  week : {
  		    dow : 1
  		  }
  		});
    	calendarConfig.dateFormatter = 'moment';
      	calendarConfig.i18nStrings.weekNumber = 'Woche {week}'; 
      	calendarConfig.allDateFormats.moment.date.hour = 'HH:mm';
      	calendarConfig.displayAllMonthEvents = true; 
      	vm.currentUser = currentUser;
    	vm.calendarView = 'month';
    	vm.calendarDate = new Date();
    	vm.calendarEvents = prepareCalendarEvents(calendarEvents.data);
    	vm.filteredEvents = vm.calendarEvents;
    	vm.calendarEventTypes = calendarEventTypeEnums.data;
    	vm.allContactsAndUsers = null;
    	vm.projects = null;
    	vm.selectedFilterProject = null;
    	vm.defaultProject = defaultProject.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.calendarEventSerialDateTypes = calendarEventSerialDateTypes.data;
    	vm.allCalendarEventsTrigger = false;
    	vm.deactivatedCalendarEventsTrigger = false;
    	vm.loadAllContactsAndUsers = true;
    	
    	vm.showCreateNewEventModal = showCreateNewEventModal;
    	vm.prepareAndAddCalendarEvent = prepareAndAddCalendarEvent;
    	vm.prepareAndUpdateCalendarEvent = prepareAndUpdateCalendarEvent;
    	vm.deleteCalendarEvent = deleteCalendarEvent;
    	vm.setSelectedFilterProject = setSelectedFilterProject;
    	vm.unselectProjects = unselectProjects;
    	vm.showPrintCalendarEvents = showPrintCalendarEvents;
    	vm.showCalendarICSLink = showCalendarICSLink;
    	vm.eventClicked = eventClicked;
    	vm.searchCalendarEvents = searchCalendarEvents;
    	vm.showAllCalendarEvents = showAllCalendarEvents;
    	vm.showDeactivatedCalendarEvents = showDeactivatedCalendarEvents;
    	vm.showCalendarEventsInList = showCalendarEventsInList;
    	findProjectsOfUser();
    	getAllContactsAndUsers();
    	
    	////////////
    	
    	$scope.$watch('vm.calendarView', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			if(vm.allCalendarEventsTrigger == true) {
    				showAllCalendarEvents();
    			} else {
    				loadPreparedCalendarEvents(false);
    			}
    		}
		});
    	
    	$scope.$watch('vm.calendarDate', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			if(vm.allCalendarEventsTrigger == true) {
    				showAllCalendarEvents();
    			} else {
    				loadPreparedCalendarEvents(false);
    			}
    		}
		});
    	
    	$scope.$watch('vm.allCalendarEventsTrigger', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			if(vm.allCalendarEventsTrigger == true) {
    				showAllCalendarEvents();
    			} else {
    				loadPreparedCalendarEvents(false);
    			}
    		}
		});
    	
    	$scope.$watch('vm.selectedFilterProject', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			setFilteredEvents();
    		}
		});
    	
    	function prepareAndUpdateCalendarEvent(calendarEvent) {
    		if(vm.allCalendarEventsTrigger == true) {
				showAllCalendarEvents();
			} else {
				loadPreparedCalendarEvents(false);
			}
    	}
    	
    	function searchCalendarEvents() {
			var day = vm.calendarDate.getDate();
    		var month = vm.calendarDate.getMonth()+1;
    		var year = vm.calendarDate.getFullYear();   
			if(vm.calendarEventSearchString != null && vm.calendarEventSearchString.length > 3) {
        		calendarService.getPreparedFilteredCalendarEvents(currentUser.id, vm.calendarView.toUpperCase(), vm.calendarEventSearchString, day, month, year).then(function (response) {
        			vm.calendarEvents = prepareCalendarEvents(response.data);
        			setFilteredEvents();
    			}).catch(function (data) {
    				console.log('error in calendar.controller.js#searchCalendarEvents: ' + data);
    			});
			} else {
				calendarEventUserConnectionService.getPreparedCalendarEventsOfUser(currentUser.id, vm.calendarView.toUpperCase(), day, month, year, false).then(function (response) {
        			vm.calendarEvents = prepareCalendarEvents(response.data);
        			setFilteredEvents();
    			}).catch(function (data) {
    				console.log('error in calendar.controller.js#searchCalendarEvents: ' + data);
    			});
			}
		}
    	
    	function eventClicked(calendarEvent) {
    		calendarEventClickedModalService.showCalendarEventClickedModal(calendarEvent);
    	}
    	
    	function setSelectedFilterProject(project) {
    		unselectProjects();
    		vm.selectedFilterProject = project;
    		project.selected = true;
    	}
    	
    	function unselectProjects() {
    		vm.selectedFilterProject = null;
    		for(var i = 0; i < vm.projects.length; i++) {
    			vm.projects[i].selected = false;
    		}
    		if(vm.allCalendarEventsTrigger == true) {
				showAllCalendarEvents();
			} else {
				loadPreparedCalendarEvents(false);
			}
    	}
    	
    	function setFilteredEvents() { 
    		var events = vm.calendarEvents;
    		if(vm.selectedFilterProject != null) {
    			events = $filter('filter')(events, {projectId: vm.selectedFilterProject.id});
    		}
    		vm.filteredEvents = events;
    	}
    	
    	function loadPreparedCalendarEvents(archived) {
    		var day = vm.calendarDate.getDate();
    		var month = vm.calendarDate.getMonth()+1;
    		var year = vm.calendarDate.getFullYear();
    		
    		calendarEventUserConnectionService.getPreparedCalendarEventsOfUser(currentUser.id, vm.calendarView.toUpperCase(), day, month, year, archived).then(function (response) {
    			vm.calendarEvents = prepareCalendarEvents(response.data);
    			setFilteredEvents();
			}).catch(function (data) {
				console.log('error in calendar.controller#loadPreparedCalendarEvents: ' + data);
			});
		}
    	
    	function showAllCalendarEvents() {
    		var day = vm.calendarDate.getDate();
    		var month = vm.calendarDate.getMonth()+1;
    		var year = vm.calendarDate.getFullYear();
    		calendarEventUserConnectionService.getPreparedCalendarEventsOfAllUsers(currentUser.id, vm.calendarView.toUpperCase(), day, month, year).then(function (response) {
    			vm.filteredEvents = prepareCalendarEvents(response.data);
			}).catch(function (data) {
				console.log('error in calendar.controller#showAllCalendarEvents: ' + data);
			});
    	}
    	
    	function showDeactivatedCalendarEvents() {
    		if(vm.deactivatedCalendarEventsTrigger) {
    			loadPreparedCalendarEvents(true);
    		} else {
    			loadPreparedCalendarEvents(false);
    		}
    	}
    	
    	function showCreateNewEventModal() {
    		createNewEventModalService.showCreateNewEventModal(vm.calendarEventSerialDateTypes, currentUser, vm.selectedFilterProject, vm.projects, vm.allContactsAndUsers, vm, null, /*calendarEventUserConnections*/[], 'CREATE');
    	}
    	
    	function findProjectsOfUser() {
    		projectsService.findProjectsOfUser(currentUser.id).then(function (response) {
    			vm.projects = response.data;
    			if(vm.projects.length == 1) {
    				vm.selectedFilterProject = vm.projects[0];
    				vm.projects[0].selected = true;
    			}
    			setDefaultProject();
    		}).catch(function (data) { 
				console.log('error calendar.controller#findProjectsOfUser: ' + data);
			});
    	}
    	
    	function setDefaultProject() {
    		if(vm.defaultProject != null) {
    			for(var i = 0; i < vm.projects.length; i++) {
    				if(vm.projects[i].id == vm.defaultProject.id) {
    					setSelectedFilterProject(vm.projects[i]);
    					vm.projects[i].selected = true;
    		    		break;
    				}
    			}
	    	}
    	}
    	
    	function getAllContactsAndUsers() {
    		contactAndUserService.findAllContactsAndUsers().then(function (response) {
    			vm.allContactsAndUsers = response.data;
    			vm.loadAllContactsAndUsers = false;
			}).catch(function (data) {
				console.log('error createNewCalendarEvent.service#addProject#getAllContactsAndUsers: ' + data);
			});
    	}
    	
    	function prepareCalendarEvents(calendarEvents) {
    		angular.forEach(calendarEvents, function(value, key) {
    			value.actions = createActions();
			});
    		return calendarEvents;
    	}
    	
    	function prepareAndAddCalendarEvent(calendarEvents) {
    		for(var i = 0; i < calendarEvents.length; i++) {
    			calendarEvents[i].actions = createActions();
        		vm.filteredEvents.push(calendarEvents[i]);
    		}
    	}
    	
    	function deleteCalendarEvent(calendarEvent, deleteSerialDates) {
    		var i  = vm.filteredEvents.length;
    		while(i--) {
    			if(deleteSerialDates === false) {
	    			if(vm.filteredEvents[i].id == calendarEvent.id) {
	    				vm.filteredEvents.splice(i, 1);
	    				break;
	    			}
    			} else if(deleteSerialDates === true) {
    				if(vm.filteredEvents[i].serialDateNumber == calendarEvent.serialDateNumber) {
	    				vm.filteredEvents.splice(i, 1);
	    			}
    			}
			}
    	}
    	
    	function createActions() {
    		var actions = [{
  		      label: vm.currentUser.userAuthorizationUserContainer.userAuthorizations[7].state === true ? '<i class=\'glyphicon glyphicon-pencil\'></i>' : '',
  		      onClick: function(args) {
  		    	calendarService.findCalendarEventById(args.calendarEvent.id).then(function (response) {
  		    		var originalCalendarEvent = response.data;
  		    		calendarEventUserConnectionService.findCalendarEventUserConnectionsByCalendarEvent(args.calendarEvent.id).then(function (response) {
  		    			var calendarEventUserConnections = response.data;
  		    			createNewEventModalService.showCreateNewEventModal(vm.calendarEventSerialDateTypes, currentUser, null, vm.projects, vm.allContactsAndUsers, vm, originalCalendarEvent, calendarEventUserConnections, 'EDIT');
  		    		}).catch(function (data) {
  		    			console.log('error calendar.controller#userOnClick: ' + data);
  		    		});
		    	}).catch(function (data) {
					console.log('error calendar.controller#userOnClick: ' + data);
				});
  		      }
  		    }, {
  		      label: userAuthorizationUserContainer.data.userAuthorizations[5].state === true ? '<i class=\'glyphicon glyphicon-remove\'></i>' : '',
  		      onClick: function(args) {
  		    	deleteCalendarEventModalService.showDeleteCalendarEventModal(args.calendarEvent, vm);
  		      }
  		    },{
		      label: '<i class=\'glyphicon glyphicon-user\'></i>',
		      onClick: function(args) {
		    	  calendarEventUserConnectionService.findCalendarEventUserConnectionsByCalendarEvent(args.calendarEvent.id).then(function (response) {
		    		  var calendarEventUserConnections = response.data;
		    		 userOfCalendarEventModalService.showUserOfCalendarEventModal(args.calendarEvent, calendarEventUserConnections, vm);
		    	  }).catch(function (data) {
					console.log('error calendar.controller#userOnClick: ' + data);
		    	  });
  		      }
  		    }];
    		return actions;
    	}
 
    	function showPrintCalendarEvents(calendarEvents, calendarEventSearchString) {
    		printCalendarEventsModalService.showPrintCalendarEventsModal(calendarEvents, calendarEventSearchString, vm.allCalendarEventsTrigger);
    	}
    	
    	function showCalendarICSLink() { 
    		calendarService.getICSLinkOfUser(currentUser.id).then(function (response) {
    			calendarICSLinkModalService.showCalendarICSLinkModal(vm.currentUser, response.data);
			}).catch(function (data) {
				console.log('error calendar.controller#showCalendarICSLink: ' + data);
			});
    	}
    	
    	function showCalendarEventsInList() {
    		var start = dateUtilityService.oneMonthBack(new Date());
     		var end = new Date();
     		var startString = dateUtilityService.formatDateToString(start);
     		var endString = dateUtilityService.formatDateToString(end);
     		
     		calendarEventInListModalService.showCalendarEventInListModal(start, end, vm.calendarEventTypes);
    	}
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarService', calendarService);
        
    calendarService.$inject = ['$http', 'api_config'];
    
    function calendarService($http, api_config) {
		var service = {
			findCalendarEventById: findCalendarEventById,
			findAllCalendarEvents: findAllCalendarEvents,
			findAllCalendarEventsOfNextWeeks: findAllCalendarEventsOfNextWeeks,
			findCalendarEventOfCriteria: findCalendarEventOfCriteria,
			getAllPreparedCalendarEvents: getAllPreparedCalendarEvents,
			getPreparedCalendarEvents: getPreparedCalendarEvents,
			getPreparedFilteredCalendarEvents: getPreparedFilteredCalendarEvents,
			create: create,
			update: update,
			deleteCalendarEvent: deleteCalendarEvent, 
			getICSLinkOfUser: getICSLinkOfUser
		};
		
		return service;
		
		////////////
		
		function findCalendarEventById(id) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/' + id);
		}
		
		function findAllCalendarEvents() {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent');
		}
		
		function findAllCalendarEventsOfNextWeeks(amountOfWeeks) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/weeks/' + amountOfWeeks);
		}
		
		function findCalendarEventOfCriteria(calendarEventSearchCriteriaContainer) {
			return $http.put(api_config.BASE_URL + '/calendarevents/calendarevent/criteria', calendarEventSearchCriteriaContainer);
		}
		
		function getAllPreparedCalendarEvents() {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/prepared');
		}
		
		function getPreparedCalendarEvents(calendarViewType, day, month, year) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/' + calendarViewType.toUpperCase() + '/' + day + '/' + month + '/' + year);
		}
		
		function getPreparedFilteredCalendarEvents(userId, calendarViewType, searchString, day, month, year) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/' + userId + '/' + calendarViewType.toUpperCase() + '/' + searchString + '/' + day + '/' + month + '/' + year);
		}
		
		function create(calendarEvent, serialDates, calendarEventSerialDateType, serialEndDate) {
			return $http.post(api_config.BASE_URL + '/calendarevents/calendarevent/' + serialDates + '/' + calendarEventSerialDateType + '/' + serialEndDate + '/', calendarEvent);
		}
		
		function update(calendarEvent) {
			return $http.put(api_config.BASE_URL + '/calendarevents/calendarevent', calendarEvent);
		}
		
		function deleteCalendarEvent(calendarEventId, deleteSerialDates) {
			return $http.delete(api_config.BASE_URL + '/calendarevents/calendarevent/' + calendarEventId + '/delete/' + deleteSerialDates);
		}
		
		function getICSLinkOfUser(userId) {
			return $http.get(api_config.BASE_URL + '/calendarevents/calendarevent/icslink/' + userId);
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarEventScheduleService', calendarEventScheduleService);
        
    calendarEventScheduleService.$inject = ['$http', 'api_config'];
    
    function calendarEventScheduleService($http, api_config) {
		var service = {
			sendCalendarEventRememberingMessage: sendCalendarEventRememberingMessage,
			sendCalendarEventOutsideWorkRememberingMessage: sendCalendarEventOutsideWorkRememberingMessage
		};
		
		return service;
		
		////////////
		
		function sendCalendarEventRememberingMessage() {
			return $http.get(api_config.BASE_URL + '/calendareventschedules/calendareventschedule/calendareventrememberingmessage/send');
		}
		
		function sendCalendarEventOutsideWorkRememberingMessage() {
			return $http.get(api_config.BASE_URL + '/calendareventschedules/calendareventschedule/calendareventoutsideworkmessage/send');
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarEventUserConnectionService', calendarEventUserConnectionService);
        
    calendarEventUserConnectionService.$inject = ['$http', 'api_config'];
    
    function calendarEventUserConnectionService($http, api_config) {
		var service = {
			create: create,
			update: update,
			getPreparedCalendarEventsOfUser: getPreparedCalendarEventsOfUser,
			getPreparedCalendarEventsOfAllUsers: getPreparedCalendarEventsOfAllUsers,
			getPreparedCalendarEventsOfAllUsersInRange: getPreparedCalendarEventsOfAllUsersInRange,
			getPreparedCalendarEventsOfUserOfNextWeeks: getPreparedCalendarEventsOfUserOfNextWeeks,
			findCalendarEventUserConnectionsByCalendarEvent: findCalendarEventUserConnectionsByCalendarEvent,
			findCalendarEventUserConnectionsByContact: findCalendarEventUserConnectionsByContact
		};
		
		return service;
		
		////////////
		
		function create(calendarEventId, calendarEventUserConnection, creationIndex) {   
			return $http.post(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + calendarEventId + '/' + creationIndex, calendarEventUserConnection);
		}
		
		function update(calendarEventId, calendarEventUserConnection) {
			return $http.put(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + calendarEventId, calendarEventUserConnection);
		}
		
		function getPreparedCalendarEventsOfUser(userId, calendarViewType, day, month, year, archived) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + userId + '/' + calendarViewType + '/' + day + '/' + month + '/' + year + '/' + archived);
		}	
		
		function getPreparedCalendarEventsOfAllUsersInRange(userId, start, end) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + userId + '/' + start + '/' + end + '/all');
		}
		
		function getPreparedCalendarEventsOfAllUsers(userId, calendarViewType, day, month, year) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + userId + '/' + calendarViewType + '/' + day + '/' + month + '/' + year + '/all');
		}	
		
		function getPreparedCalendarEventsOfUserOfNextWeeks(userId, amountOfWeeks) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + userId + '/' + amountOfWeeks);
		}	
		
		function findCalendarEventUserConnectionsByCalendarEvent(calendarEventId) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + calendarEventId);
		}	
		
		function findCalendarEventUserConnectionsByContact(contactId) {
			return $http.get(api_config.BASE_URL + '/calendareventuserconnections/calendareventuserconnection/' + contactId + '/contact');
		}	
    }
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.calendar')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getCalendarState());
    	 
    	////////////
			    	
    	function getCalendarState() {
    		var state = {
    			name: 'auth.calendar',
				url: '/calendar/:userId',
				templateUrl: 'app/calendar/calendar/calendar.html',
				controller: 'CalendarController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					optionsService: 'optionsService',
					projectsService: 'projectsService',
					calendarEventUserConnectionService: 'calendarEventUserConnectionService',
					calendarEvents: function getPreparedCalendarEventsOfUser($stateParams, calendarEventUserConnectionService) {
						var date = new Date();
						var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
						var month = date.getMonth()+1;
						return calendarEventUserConnectionService.getPreparedCalendarEventsOfUser($stateParams.userId, 'MONTH', firstDay.getDate(), month, firstDay.getFullYear(), false);
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					calendarEventSerialDateTypes: function(optionsService) {
						return optionsService.getCalendarEventSerialDateTypes();
					},
					calendarEventTypeEnums: function(optionsService) {
						return optionsService.getCalendarEventTypes();
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar') 
    	.directive('calendarEventInListDirective', calendarEventInListDirective);
    
    calendarEventInListDirective.$inject = [];
    
	function calendarEventInListDirective() {
		var directive = {
			restrict: 'E',
			scope: {
				calendarEvents: '='
			},
			templateUrl: 'app/calendar/directives/calendarEventInList/calendarEventInList.html',
			link: function($scope) {

			}
		};
		return directive;
		
		////////////
	}
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarEventClickedModalService', calendarEventClickedModalService);
        
    calendarEventClickedModalService.$inject = ['$modal', '$stateParams'];
    
    function calendarEventClickedModalService($modal, $stateParams) {
		var service = {
			showCalendarEventClickedModal: showCalendarEventClickedModal
		};		
		return service;
		
		////////////
				
		function showCalendarEventClickedModal(calendarEvent) {
			 var calendarEventClickedModal = $modal.open({
				controller: CalendarEventClickedModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEvent: function() {
			    		return calendarEvent;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/calendarEventClicked/calendarEventClicked.modal.html'
			});
			return calendarEventClickedModal;
			
			function CalendarEventClickedModalController($modalInstance, $scope, calendarEvent) {
		    	var vm = this;
		    	vm.calendarEvent = calendarEvent;
		    	
		    	vm.closeCalendarEventClickedModal = closeCalendarEventClickedModal;
		    	
		    	function closeCalendarEventClickedModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarEventInListModalService', calendarEventInListModalService);
        
    calendarEventInListModalService.$inject = ['$modal', '$stateParams', 'dateUtilityService', 'calendarService', 'contactAndUserService'];
    
    function calendarEventInListModalService($modal, $stateParams, dateUtilityService, calendarService, contactAndUserService) {
		var service = {
			showCalendarEventInListModal: showCalendarEventInListModal
		};		
		return service;
		
		////////////
				
		function showCalendarEventInListModal(start, end, calendarEventTypes) {  
			 var calendarEventInListModal = $modal.open({
				controller: CalendarEventInListModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	}, 
			    	calendarEventTypes: function() {
			    		return calendarEventTypes;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/calendarEventInList/calendarEventInList.modal.html'
			});
			return calendarEventInListModal;
			
			function CalendarEventInListModalController($modalInstance, $scope, start, end, calendarEventTypes) {
		    	var vm = this;
		    	vm.filterStartDate = start;
		    	vm.filterEndDate = end;
		    	vm.calendarEventTypes = calendarEventTypes;
		    	vm.calendarEvents = null;
		    	vm.filterStartDatePicker = false;
		    	vm.filterEndDatePicker = false;
		    	vm.selectedUser = null;
		    	vm.selectedCalendarEventType = null;
		    	
		    	vm.searchUsersByTerm = searchUsersByTerm;
		    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
		    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
		    	vm.searchCalendarEvents = searchCalendarEvents;
		    	vm.resetSearchCriteria = resetSearchCriteria;
		    	vm.closeCalendarEventInListModal = closeCalendarEventInListModal;
		    	searchCalendarEvents();
		    	
		    	////////////
		    	
		    	function openFilterStartDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterStartDatePicker = true;
					vm.searchCriteriaChangedTrigger = true;
		        }
		    	
		    	function openFilterEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterEndDatePicker = true;
					vm.searchCriteriaChangedTrigger = true;
		        }
		    	
		    	function searchCalendarEvents() {
		    		var startString = dateUtilityService.formatDateToString(vm.filterStartDate);
		     		var endString = dateUtilityService.formatDateToString(vm.filterEndDate);
		     		
		     		var calendarEventSearchCriteriaContainer = {};
		     		calendarEventSearchCriteriaContainer.start = startString;
		     		calendarEventSearchCriteriaContainer.end = endString;
		     		calendarEventSearchCriteriaContainer.userId = vm.selectedUser != null ? vm.selectedUser.id : null;
		     		calendarEventSearchCriteriaContainer.calendarEventType = vm.selectedCalendarEventType != null ? vm.selectedCalendarEventType : null;
		     		
		    		calendarService.findCalendarEventOfCriteria(calendarEventSearchCriteriaContainer).then(function(response) {
		    			vm.calendarEvents = response.data;
			    	}).catch(function (data) {
						console.log('error calendarEventInList.service#searchCalendarEvents: ' + data);
			    	});
		    	}
		    	
		    	function searchUsersByTerm(searchString) {   
		    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function(response) {
		    			var usersAndContacts = response.data;
		    			var users = [];
		    			for(var i = 0; i < usersAndContacts.length; i++) {
		    				if(usersAndContacts[i].contact == null) {
		    					users.push(usersAndContacts[i]);
		    				}
		    			}
		    			return users;
		    		}).catch(function (data) {
						console.log('error calendarEventInList.service#contactSearch#searchUsersByTerm: ' + data);
					});
		    	}
		    	
		    	function resetSearchCriteria() {
		    		vm.filterStartDate = dateUtilityService.oneMonthBack(new Date());
		    		vm.filterEndDate = new Date();
		    		vm.selectedUser = null;
			    	vm.selectedCalendarEventType = null;
		    	}
		    	
		    	function closeCalendarEventInListModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarICSLinkModalService', calendarICSLinkModalService);
        
    calendarICSLinkModalService.$inject = ['$modal', '$stateParams'];
    
    function calendarICSLinkModalService($modal, $stateParams) {
		var service = {
			showCalendarICSLinkModal: showCalendarICSLinkModal
		};		
		return service;
		
		////////////
				
		function showCalendarICSLinkModal(currentUser, calendarHandlingBean) {
			 var calendarICSLinkModal = $modal.open({
				controller: CalendarICSLinkModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	}, 
			    	calendarHandlingBean: function() {
			    		return calendarHandlingBean;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/calendarICSLink/calendarICSLink.modal.html'
			});
			return calendarICSLinkModal;
			
			function CalendarICSLinkModalController($modalInstance, $scope, currentUser, calendarHandlingBean) {
		    	var vm = this;
		    	vm.currentUser = currentUser;
		    	vm.calendarHandlingBean = calendarHandlingBean;
		    	
		    	vm.closeCalendarICSLinkModal = closeCalendarICSLinkModal;
		    	
		    	function closeCalendarICSLinkModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('createNewEventModalService', createNewEventModalService);
        
    createNewEventModalService.$inject = ['$modal', '$stateParams', 'calendarService', 'calendarEventUserConnectionService', 'contactAndUserService', 'projectsService', 'dateUtilityService', 'projectUserConnectionService'];
    
    function createNewEventModalService($modal, $stateParams, calendarService, calendarEventUserConnectionService, contactAndUserService, projectsService, dateUtilityService, projectUserConnectionService) {
		var service = {
			showCreateNewEventModal: showCreateNewEventModal
		};		
		return service;
		
		////////////
				
		function showCreateNewEventModal(calendarEventSerialDateTypes, currentUser, selectedFilterProject, projects, allContactsAndUsers, invoker, originalCalendarEvent, calendarEventUserConnections, type) {
			 var createNewEventModal = $modal.open({
				controller: CreateNewEventModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEventSerialDateTypes: function() {
			    		return calendarEventSerialDateTypes;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	selectedFilterProject: function() {
			    		return selectedFilterProject;
			    	}, 
			    	projects: function() {
			    		return projects;
			    	}, 
			    	allContactsAndUsers: function() {
			    		return allContactsAndUsers;
			    	}, 
			    	invoker: function() {
			    		return invoker;
			    	},
			    	originalCalendarEvent: function() {
			    		return originalCalendarEvent;
			    	},
			    	calendarEventUserConnections: function() {
			    		return calendarEventUserConnections;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/createNewCalendarEvent/createNewCalendarEvent.modal.html'
			});
			return createNewEventModal;
			
			function CreateNewEventModalController($modalInstance, $scope, calendarEventSerialDateTypes, currentUser, selectedFilterProject, projects, allContactsAndUsers, invoker, originalCalendarEvent, calendarEventUserConnections, type) {
		    	var vm = this;
		    	
		    	vm.today = new Date();
		    	vm.todayPlusOneYear = setTodayPlusYears(vm.today, 1);
		    	vm.todayPlusThreeYears = setTodayPlusYears(vm.today, 3);
		    	vm.calendarEventSerialDateTypes = calendarEventSerialDateTypes;
		    	vm.currentUser = currentUser;
		    	vm.selectedFilterProject = selectedFilterProject;
		    	vm.originalCalendarEvent = originalCalendarEvent;
		    	vm.projects = projects;
		    	vm.allContactsAndUsers = allContactsAndUsers;
		    	unset();
		    	vm.type = type;
		    	vm.calendarEventStartsAtDatePicker = false;
		    	vm.calendarEventEndsAtDatePicker = false;
		    	vm.serialEndDatePicker = false;
		    	vm.startDate = new Date();
		    	vm.endDate = new Date();
		    	vm.startTime = new Date();
		    	vm.endTime = new Date();
		    	vm.calendarEvent = setCalendarEvent();
		    	vm.calendarUserConnections = calendarEventUserConnections;
		    	vm.isSerialDate = false;
		    	
		    	vm.disabledFrom = disabledFrom;
		    	vm.createOrUpdateCalendarEvent = createOrUpdateCalendarEvent;
		    	vm.createCalendarEvent = createCalendarEvent;
		    	vm.updateCalendarEvent = updateCalendarEvent;
		    	vm.addCalendarUserConnection = addCalendarUserConnection;
		    	vm.findProjectByTerm = findProjectByTerm;
		    	vm.findContactAndUserBySearchString = findContactAndUserBySearchString;
		    	vm.removeCalendarUserConnection = removeCalendarUserConnection;
		    	vm.closeCreateNewEventModal = closeCreateNewEventModal;
		    	vm.openCalendarEventStartsAtDatePicker = openCalendarEventStartsAtDatePicker;
		    	vm.openCalendarEventEndsAtDatePicker = openCalendarEventEndsAtDatePicker;
		    	vm.openSerialEndDatePicker = openSerialEndDatePicker;
		    	vm.addProject = addProject;
		    	vm.removeProject = removeProject;
		    	vm.addContactOrUser = addContactOrUser;
		    	vm.removeContactOrUser = removeContactOrUser;
		    	vm.removeContactsOrUsers = removeContactsOrUsers;
		    	vm.contactOrUserStillSelected = contactOrUserStillSelected;
		    	vm.isCurrentUserMemberOfCalendarEvent = isCurrentUserMemberOfCalendarEvent;
		    	vm.contactPersonSelectionChanged = contactPersonSelectionChanged;
		    	vm.isContactPersonSelected = isContactPersonSelected;
		    	
		    	if(vm.type == 'EDIT') {
		    		addProject(vm.calendarEvent.project, 'INIT');
		    		setProjectSelection(vm.calendarEvent.project);
		    	} else {
		    		addProject(vm.selectedFilterProject, 'NON_INIT');
		    		setProjectSelection(vm.selectedFilterProject);
		    	}
		    	
		    	////////////
		    	
		    	$scope.$watch('vm.startDate', function () {
		    		vm.endDate = vm.startDate;
		        });	
		    	
		    	function setTodayPlusYears(today, years) {
		    		var todayPlusYears = new Date();
		    		todayPlusYears.setFullYear(todayPlusYears.getFullYear()+years);
		    		return todayPlusYears;
		    	}
		    	
		    	function disabledFrom(date, mode) { 
		    		var serialDateType = vm.calendarEvent.calendarEventSerialDateType;
		    		if(serialDateType === 'NO_SERIAL_DATE') {
		    			return false;
		    		} else if(serialDateType === 'DAILY') { 
		    			return mode === 'day' && date > vm.todayPlusOneYear;
		    		} else {
		    			return mode === 'day' && date > vm.todayPlusThreeYears;
		    		}
		      	}
		    	
		    	function setCalendarEvent() {
		    		if(vm.type == 'CREATE') {
		    			var emptyCalendarEvent = {
		    		    		title: '',
		    		    		location: '',
		    		    		startsAt: null,
		    		    		endsAt: null,
		    		    		project: vm.selectedFilterProject,
		    		    		serialDate: false,
		    		    		serialEndDate: new Date(),
		    		    		calendarEventSerialDateType: 'NO_SERIAL_DATE',
		    		    		calendarUserConnections: []
		    		    	};
		    			return emptyCalendarEvent;
		    		} else if(vm.type == 'EDIT') {
		    			var calendarEvent = vm.originalCalendarEvent;
		    			calendarEvent.serialEndDate = dateUtilityService.convertToDateOrUndefined(vm.originalCalendarEvent.serialEndDate);
		    			vm.startDate = new Date(vm.originalCalendarEvent.startsAt);
				    	vm.endDate = new Date(vm.originalCalendarEvent.endsAt);
				    	vm.startTime = new Date(vm.originalCalendarEvent.startsAt);
				    	vm.endTime = new Date(vm.originalCalendarEvent.endsAt);
		    			return calendarEvent;
		    		}
		    	}
		    	
		    	function contactPersonSelectionChanged(contactPersonSelected, contactPerson, calendarUserConnection) {
		    		if(contactPersonSelected) {
		    			if(calendarUserConnection.contactPersons == null) {
		    				calendarUserConnection.contactPersons = [];
		    			}
		    			calendarUserConnection.contactPersons.push(contactPerson);
		    		} else {
			    		for(var i = 0; i < calendarUserConnection.contactPersons.length; i++) {
			    			if(calendarUserConnection.contactPersons[i].firstname == contactPerson.firstname &&
			    					 calendarUserConnection.contactPersons[i].surname == contactPerson.surname) {
			    				calendarUserConnection.contactPersons.splice(i, 1);
			    				break;
			    			}
			    		}
		    		}
		    	}
		    	
		    	function isContactPersonSelected(contactPerson, calendarUserConnection) {
		    		if(calendarUserConnection == null || calendarUserConnection.contactPersons == null || calendarUserConnection.contactPersons.length === 0) {
		    			return false;
		    		}
		    		for(var i = 0; i < calendarUserConnection.contactPersons.length; i++) {
		    			if(calendarUserConnection.contactPersons[i].firstname == contactPerson.firstname &&
		    					 calendarUserConnection.contactPersons[i].surname == contactPerson.surname) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function unsetCalendarEventObject() {
		    		vm.calendarEvent = {
				    		title: '',
				    		location: '',
				    		startsAt: null,
				    		endsAt: null,
				    		project: null,
				    		calendarUserConnections: [{
				    			user: null,
				    			contact: null,
				    			emailActive: true
				    		}]
				    	};
		    	}
		    	
		    	function setProjectSelection(project) {
		    		if(project == null) {
		    			return;
		    		}
		    		for(var i = 0; i < vm.projects.length; i++) {
		    			if(vm.projects[i].id == project.id) {
		    				vm.projects[i].selected = true;
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function openCalendarEventStartsAtDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.calendarEventStartsAtDatePicker = true;
	            	}
		    	
		    	function openCalendarEventEndsAtDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.calendarEventEndsAtDatePicker = true;
	            	}
		    	
		    	function openSerialEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.serialEndDatePicker = true;
	            	}
		    			    	
		    	function findContactAndUserBySearchString(searchString) {
		    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function (response) {
		    			return response.data;
		    		}).catch(function (data) {
						console.log('error createNewCalendarEvent.service#findContactAndUserBySearchString: ' + data);
					});
		    	}
		    	
		    	function findProjectByTerm(searchTerm) {
		    		return projectsService.findProjectByTerm(searchTerm).then(function (response) {
		    			return response.data;
		    		}).catch(function (data) {
						console.log('error createNewCalendarEvent.service#findProjectByTerm: ' + data);
					});
		    	}

		    	function addCalendarUserConnection(index) {
		    		vm.calendarUserConnections.push(vm.calendarUserConnections.length + 1);
		    	}
		    	
		    	function removeCalendarUserConnection(index) {
		    		vm.calendarUserConnections.splice(index, 1);
		    	}
		    	
		    	function reduceProjectUserConnectionsOfNonExistingCalendarUserConnections(projectUserConnections, calendarUserConnections) {
		    		if(calendarUserConnections == null) {
		    			return projectUserConnections;
		    		}
		    		var reducedProjectUserConnection = [];
		    		for(var i = 0; i < projectUserConnections.length; i++) {
		    			var projectUserConnection = projectUserConnections[i];
		    			var removeTrigger = true;
		    			for(var j = 0; j < calendarUserConnections.length; j++) {
		    				var calendarUserConnection = calendarUserConnections[j];
		    				if((projectUserConnection.user != null && projectUserConnection.user.id == calendarUserConnection.user.id) ||
		    				   (projectUserConnection.contact != null && projectUserConnection.contact.id == calendarUserConnection.contact.id)) {
		    					removeTrigger = false;
		    					break;
		    				}
		    			}
		    			if(removeTrigger === false) {
		    				reducedProjectUserConnection.push(projectUserConnection);
		    			}
		    		}
		    		return reducedProjectUserConnection;
		    	}
		    	
		    	function addProject(project, mode) {
		    		if(vm.type == 'EDIT' && mode == 'INIT') {
		    			return;
		    		}
		    		if(project == null) {
		    			return;
		    		}
		    		unsetProjects();
		    		project.selected = true;
		    		if(typeof vm.calendarEvent != 'undefined' && vm.calendarEvent.project != null) {
		    			vm.calendarEvent.project.selected = false;
		    		}
		    		if(typeof vm.calendarEvent != 'undefined') {
		    			vm.calendarEvent.project = project;
		    		}
		    		vm.calendarUserConnections = [];
		    		projectUserConnectionService.findProjectUserConnectionByProject(project.id).then(function (response) {
	    				var userOrContacts = response.data;
	    				for(var i = 0; i < userOrContacts.length; i++) {
	    					var calendarUserConnection = {};
	    					if(userOrContacts[i].user != null) {
	    						calendarUserConnection.user = userOrContacts[i].user;
	    					}
	    					if(userOrContacts[i].contact != null) {
	    						calendarUserConnection.contact = userOrContacts[i].contact;
	    					}
	    					calendarUserConnection.emailActive = true;
	    					vm.calendarUserConnections.push(calendarUserConnection);
	    				} 
	    			}).catch(function (data) {
	    				console.log('error in calendar.controller#loadPreparedCalendarEvents: ' + data);
	    			});
		    	}

		    	function removeProject(project) {
		    		project.selected = false;
		    		vm.calendarEvent.project = null;
		    	}
		    	
		    	function isEmailActiveInEditMode(contactOrUser) {
		    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
		    			if((vm.calendarUserConnections[i].user != null && vm.calendarUserConnections[i].user.id == contactOrUser.id) || 
		    		    		(vm.calendarUserConnections[i].contact != null && vm.calendarUserConnections[i].contact.id == contactOrUser.id)) {
		    				return vm.calendarUserConnections[i].emailActive;
		    			}
		    		}
		    		return true;
		    	}
		    	
		    	function addContactOrUser(contactOrUser) {
		    		addCalendarUserConnectionsToObject(contactOrUser);
		    	}
		    	
		    	function removeContactsOrUsers(contactsOrUsers) {
		    		for(var i = 0; i < contactsOrUsers.length; i++) {
		    			removeContactOrUser(contactsOrUsers[i]);
		    		}
		    	}
		    	
		    	function removeContactOrUser(calendarUserConnection) {
		    		if(calendarUserConnection.user != null) {
			    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
			    			if(vm.calendarUserConnections[i].user != null && vm.calendarUserConnections[i].user.id == calendarUserConnection.user.id) {
			    				vm.calendarUserConnections.splice(i, 1);
			    				break;
			    			}
			    		}
		    		}
		    		if(calendarUserConnection.contact != null) {
			    		for(var j = 0; j < vm.calendarUserConnections.length; j++) {
			    			if(vm.calendarUserConnections[j].contact != null && vm.calendarUserConnections[j].contact.id == calendarUserConnection.contact.id) {
			    				vm.calendarUserConnections.splice(j, 1);
			    				break;
			    			}
			    		}
		    		}
		    	}
		    	
		    	function addCalendarUserConnectionsToObject(contactOrUser) {
		    		if(userStillAdded(contactOrUser) != null) {
		    			return;
		    		}
		    		if(contactStillAdded(contactOrUser) != null) {
		    			return;
		    		}
	    			var calendarUserConnection = {};
	    			calendarUserConnection.user = contactOrUser.user;
	    			calendarUserConnection.contact = contactOrUser.contact;
	    			calendarUserConnection.emailActive = true;
	    			vm.calendarUserConnections.push(calendarUserConnection);
		    	}
		    	
		    	function userStillAdded(contactOrUser) {
		    		if(contactOrUser.contactUserType == 'USER') {
			    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
			    			if(vm.calendarUserConnections[i].user != null && vm.calendarUserConnections[i].user.id == contactOrUser.id) {
			    				return vm.calendarUserConnections[i];
			    			}
			    		}
			    		return null;
		    		}
		    	}
		    	
		    	function contactStillAdded(contactOrUser) {
		    		if(contactOrUser.contactUserType == 'CONTACT') {
			    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
			    			if(vm.calendarUserConnections[i].contact != null && vm.calendarUserConnections[i].contact.id == contactOrUser.id) {
			    				return vm.calendarUserConnections[i];
			    			}
			    		}
			    		return null;
		    		}
		    	}
		    	
		    	function createOrUpdateCalendarEvent() {
		    		if(vm.type == 'CREATE') {
		    			createCalendarEvent();
		    		} else if(vm.type == 'EDIT') {
		    			updateCalendarEvent();
		    		}
		    	}
		    	
		    	function createCalendarEvent() {
		    		var start = dateUtilityService.joinDateObjectsToDateTimeObject(vm.startDate, vm.startTime).getTime();
		    		var end = dateUtilityService.joinDateObjectsToDateTimeObject(vm.endDate, vm.endTime).getTime();
		    		
		    		if(start > end) {
	    				$scope.endDateBeforeStartDate = true;
	    				return;
	    			} else {
	    				$scope.endDateBeforeStartDate = false;
	    			}
		    		
		    		vm.calendarEvent.startsAt = start;
		    		vm.calendarEvent.endsAt = end;
		    		
		    		var calendarUserConnections = vm.calendarUserConnections;
		    		if(calendarUserConnections == null || calendarUserConnections.length === 0) {
		    			return;
		    		}
		    		var serialEndDateFormatted = vm.calendarEvent.serialEndDate.getDate() + '.' + (vm.calendarEvent.serialEndDate.getMonth()+1) + '.' + vm.calendarEvent.serialEndDate.getFullYear();
		    		calendarService.create(vm.calendarEvent, vm.calendarEvent.serialDate, vm.calendarEvent.calendarEventSerialDateType, serialEndDateFormatted).then(function (response) {
		    			var calendarEvents = response.data;
		    			invoker.prepareAndAddCalendarEvent(calendarEvents);
		    			if(calendarUserConnections != null) {
		    				for(var i = 0; i < calendarEvents.length; i++) {
				    			calendarEventUserConnectionService.create(calendarEvents[i].id, calendarUserConnections, i).then(function (response) {
				    				vm.calendarUserConnections = response.data;
				    				unsetCalendarEventObject();
				    				closeCreateNewEventModal();
				    			}).catch(function (data) {
									console.log('error createNewCalendarEvent.service#create#calendarEventUserConnection: ' + data);
								});
		    				}
		    			} else {
		    				unsetCalendarEventObject();
		    				closeCreateNewEventModal();
		    			}
		    		}).catch(function (data) {
						console.log('error createNewCalendarEvent.service#createCalendarEvent: ' + data);
					});
		    	}
		    	
		    	function updateCalendarEvent() { 
		    		if(vm.calendarUserConnections == null || vm.calendarUserConnections.length === 0) {
		    			return;
		    		}
		    		vm.calendarEvent.startsAt = dateUtilityService.joinDateObjectsToDateTimeObject(vm.startDate, vm.startTime).getTime();
		    		vm.calendarEvent.endsAt = dateUtilityService.joinDateObjectsToDateTimeObject(vm.endDate, vm.endTime).getTime();
		    		calendarService.update(vm.calendarEvent).then(function (response) {
		    			var calendarEvents = response.data; 
		    			if(calendarEvents.length === 0) {
		    				closeCreateNewEventModal(); 
		    			}
		    			if(vm.calendarUserConnections != null) {
			    			calendarEventUserConnectionService.update(calendarEvents[0].id, vm.calendarUserConnections).then(function (response) {
			    				invoker.prepareAndUpdateCalendarEvent(calendarEvents);
			    				unsetCalendarEventObject();
			    				closeCreateNewEventModal();
			    			}).catch(function (data) {
								console.log('error createNewCalendarEvent.service#update#calendarEventUserConnection: ' + data);
							});
		    			} else {
		    				unsetCalendarEventObject();
		    				closeCreateNewEventModal();
		    			}
		    		}).catch(function (data) {
						console.log('error createNewCalendarEvent.service#updateCalendarEvent: ' + data);
					});
		    	}
		    	
		    	function contactOrUserStillSelected(contactOrUser) {
		    		if(userStillAdded(contactOrUser) != null || contactStillAdded(contactOrUser) != null) {
		    			return true;
		    		}
		    		return false;
		    	}
		    	
		    	function unset() {
		    		unsetProjects();
		    		unsetCalendarEventObject();
		    	}
		    	
		    	function unsetProjects() {
		    		if(vm.projects != null && typeof vm.projects != 'undefined') {
			    		for(var i = 0; i < vm.projects.length; i++) {
			    			vm.projects[i].selected = false;
			    		}
		    		}
		    	}
		    	
		    	function isCurrentUserMemberOfCalendarEvent() {
		    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
		    			if(vm.calendarUserConnections[i].user != null && vm.calendarUserConnections[i].user.id == vm.currentUser.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function closeCreateNewEventModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('deleteCalendarEventModalService', deleteCalendarEventModalService);
        
    deleteCalendarEventModalService.$inject = ['$modal', '$stateParams', 'calendarService'];
    
    function deleteCalendarEventModalService($modal, $stateParams, calendarService) {
		var service = {
			showDeleteCalendarEventModal: showDeleteCalendarEventModal
		};		
		return service;
		
		////////////
				
		function showDeleteCalendarEventModal(calendarEvent, invoker) {
			 var deleteCalendarEventModal = $modal.open({
				controller: DeleteCalendarEventModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEvent: function() {
			    		return calendarEvent;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/deleteCalendarEvent/deleteCalendarEvent.modal.html'
			});
			return deleteCalendarEventModal;
			
			function DeleteCalendarEventModalController($modalInstance, $scope, calendarEvent, invoker) {
		    	var vm = this;
		    	vm.calendarEvent = calendarEvent;
		    	vm.deleteSerialDates = false;
		    	vm.deleteUniqueDate = false;
		    	
		    	$scope.$watch('vm.deleteSerialDates', function () {
		    		if(vm.deleteSerialDates === true) {
		    			vm.deleteUniqueDate = false;
		    		}
		        });
		    	
		    	$scope.$watch('vm.deleteUniqueDate', function () {
		    		if(vm.deleteUniqueDate === true) {
		    			vm.deleteSerialDates = false;
		    		}
		        });
		    	
		    	vm.closeCalendarEventDeletionModal = closeCalendarEventDeletionModal;
		    	vm.calendarEventDeletion = calendarEventDeletion;
		    	
		    	function calendarEventDeletion() {
		    		calendarService.deleteCalendarEvent(vm.calendarEvent.id, vm.deleteSerialDates).then(function (response) {
		    			invoker.deleteCalendarEvent(vm.calendarEvent, vm.deleteSerialDates);
		    			closeCalendarEventDeletionModal();
		    		}).catch(function (data) {
						console.log('error deleteCalendarEvent.service#calendarEventDeletion: ' + data);
					});
		    	}
		    	
		    	function closeCalendarEventDeletionModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('printCalendarEventsModalService', printCalendarEventsModalService);
        
    printCalendarEventsModalService.$inject = ['$modal', '$stateParams', 'calendarService', 'dateUtilityService'];
    
    function printCalendarEventsModalService($modal, $stateParams, calendarService, dateUtilityService) {
		var service = {
			showPrintCalendarEventsModal: showPrintCalendarEventsModal
		};		
		return service;
		
		////////////
				
		function showPrintCalendarEventsModal(calendarEvents, calendarEventSearchString, allCalendarEventsTrigger) {
			 var printCalendarEventModal = $modal.open({
				controller: PrintCalendarEventsModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEvents: function() {
			    		return calendarEvents;
			    	}, 
			    	calendarEventSearchString: function() {
			    		return calendarEventSearchString;
			    	}, 
			    	allCalendarEventsTrigger: function() {
			    		return allCalendarEventsTrigger;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/printCalendarEvents/printCalendarEvents.modal.html'
			});
			return printCalendarEventModal;
			
			function PrintCalendarEventsModalController($modalInstance, $scope, calendarEvents, calendarEventSearchString, allCalendarEventsTrigger) {
		    	var vm = this;		
		    	vm.calendarEvents = calendarEvents;
		    	vm.calendarEventSearchString = calendarEventSearchString;
		    	vm.allCalendarEventsTrigger = allCalendarEventsTrigger;
		    	
		    	vm.orderType = 'startsAt';
		    	
		    	vm.changeLongDateToFormattedDate = changeLongDateToFormattedDate;
		    	vm.closeCalendarEventPrintModal = closeCalendarEventPrintModal;
		    	
		    	function changeLongDateToFormattedDate(dateInLong) {
	      			  var formattedDate = dateUtilityService.getFormattedDateFromLongDate(dateInLong);
	      			  var formattedTime = dateUtilityService.getFormattedTimeFromLongDate(dateInLong);
	      			  return formattedDate + ' <b>' + formattedTime + '</b>';
	      		  }
		    	
		    	function closeCalendarEventPrintModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('userOfCalendarEventModalService', userOfCalendarEventModalService);
        
    userOfCalendarEventModalService.$inject = ['$modal', '$stateParams', 'calendarService'];
    
    function userOfCalendarEventModalService($modal, $stateParams, calendarService) {
		var service = {
			showUserOfCalendarEventModal: showUserOfCalendarEventModal
		};		
		return service;
		
		////////////
				
		function showUserOfCalendarEventModal(calendarEvent, calendarEventUserConnections, invoker) {
			 var userOfCalendarEventModal = $modal.open({
				controller: UserOfCalendarEventModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEvent: function() {
			    		return calendarEvent;
			    	},
			    	calendarEventUserConnections: function() {
			    		return calendarEventUserConnections;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/userOfCalendarEvent/userOfCalendarEvent.modal.html'
			});
			return userOfCalendarEventModal;
			
			function UserOfCalendarEventModalController($modalInstance, $scope, calendarEvent, calendarEventUserConnections, invoker) {
		    	var vm = this;
		    	vm.calendarEvent = calendarEvent;
		    	vm.calendarEventUserConnections = calendarEventUserConnections;
		    	
		    	vm.closeUserOfCalendarEventModal = closeUserOfCalendarEventModal;
		    	
		    	function closeUserOfCalendarEventModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('asFormGroup', asFormGroup);
    					
	function asFormGroup() {
		var directive = {
			restrict: 'E',
			replace: true,
			transclude: true,
			scope: {
				asLabel: '@'
			},
			templateUrl: 'app/common/as-form-group/as-form-group.directive.html'
		};
		
		return directive;
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.factory('authTokenService', authTokenService);
    
    authTokenService.$inject = ['localStorageService', 'token_config'];
    
    function authTokenService(localStorageService, token_config) {
		var service = {
			saveToken: saveToken,
			deleteToken: deleteToken,
			getToken: getToken
		};
		
		return service;
		
		////////////
		
		function saveToken(token) {
			return localStorageService.set(token_config.AUTH_TOKEN_KEY, token);
		}
		
		function deleteToken() {
			return localStorageService.remove(token_config.AUTH_TOKEN_KEY);
		}	
		
		function getToken(){
			return localStorageService.get(token_config.AUTH_TOKEN_KEY);
		}
	}
})();
(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('authUserService', authUserService);
    
	authUserService.$inject = ['$http', '$q', '$location', 'authService', 'authTokenService', 'api_config', 'token_config'];
	
	function authUserService($http, $q, $location, authService, authTokenService, api_config, token_config) {
		var currentUser = false;
		return {
			signin: signin,
			signup: signup,
			logout: logout,
			changePassword: changePassword,
			misrememberPassword: misrememberPassword,
			setCurrentUser: setCurrentUser,
			getCurrentUser: getCurrentUser,
			getLastLogin: getLastLogin
		};
		
		////////////
		
		function signin(username, password) {
			return $http.post(api_config.BASE_URL + '/login/WDM', {
				username: username,
				password: password
			}).success(function(data){
				setCurrentUser(data.user);
				authTokenService.saveToken(data.token);
				authService.loginConfirmed('success', function(config) {
					var token = authTokenService.getToken();
					if (token) {
						config.headers[token_config.AUTH_TOKEN_HEADER] = token;
					}
					return config;
				});
			}).error(function(data) {
				return data;
			});
		}
		
		function signup(newUser) {
			return $http.post(api_config.BASE_URL + '/signup', newUser);
		}
		
		function logout() {
			authUserService.setCurrentUser(false);
			authTokenService.deleteToken();
		}

		function changePassword(changePasswordParams) {
			return $http.put(api_config.BASE_URL + '/changepassword', changePasswordParams);
		}

		function misrememberPassword(emailAddress) {
			return $http.put(api_config.BASE_URL + '/misrememberpassword', emailAddress);
		}
		
		function getCurrentUserFromAPI() {
			return $http.get(api_config.BASE_URL + '/getCurrentLoggedUser');
		}
		
		function getCurrentUser() {
		    if (currentUser) {
		    	return $q.when(currentUser);
		    }
		    return getCurrentUserFromAPI().then(function(response) {
	               if (response != null && response.data != null) {
	                   currentUser = response.data;
	                   return currentUser;
	               } else {
	            	   $location.path('/signin');
	               }     
	           }, function(error) {
	               var token = authTokenService.getToken();
	               if (token) {
	                   authTokenService.deleteToken();
	               }
	               $location.path('/signin');
	           });
		}
		
		function setCurrentUser(newCurrentUserData) {
			currentUser = newCurrentUserData;
			return currentUser;
		}
		
		function getLastLogin(userId) {
			return $http.get(api_config.BASE_URL + '/login/lastlogin/' + userId);
		} 
	}
})();
(function() {
    'use strict';
     
    angular
    	.module('legalprojectmanagement.common')
    	.run(runBlock);
    
    runBlock.$inject = ['$http', '$templateCache'];
    
    function runBlock($http, $templateCache) {
    	$http.get('app/common/as-form-group/as-form-group.directive.html', { cache: $templateCache });
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.factory('dateUtilityService', dateUtilityService);
    
    dateUtilityService.$inject = [];
    
    function dateUtilityService() {
		var service = {
			longDatesAreEqual: longDatesAreEqual,
			convertToDateOrUndefined: convertToDateOrUndefined,
			convertToDateTimeOrUndefined: convertToDateTimeOrUndefined,
			isDateValid: isDateValid,
			isTimeValid: isTimeValid,
			isAmountOfTimeValid: isAmountOfTimeValid,
			isTimeBeforeOther: isTimeBeforeOther,
			isDateBeforeOrEqualOtherDate: isDateBeforeOrEqualOtherDate, 
			formatDateToString: formatDateToString,
			formatDateToDateTimeString: formatDateToDateTimeString,
			overlapping: overlapping,
			checkArrayConcerningOverlapping: checkArrayConcerningOverlapping,
			createDate: createDate,
			joinDateObjectsToDateTimeObject: joinDateObjectsToDateTimeObject,
			formatStartEndDateTime: formatStartEndDateTime, 
			oneMonthBack: oneMonthBack
		};
		
		return service;
		
		////////////

		function longDatesAreEqual(longDate1, longDate2) {
			var start = new Date(longDate1);
			var end = new Date(longDate2);
			
			var startDay = start.getDate();
			var startMonth = start.getMonth();
			var startYear = start.getFullYear();
			
			var endDay = end.getDate();
			var endMonth = end.getMonth();
			var endYear = end.getFullYear();
			
			if(startDay == endDay && startMonth == endMonth && startYear == endYear) {
				return true;
			}
			return false;
		}
		
		function convertToDateOrUndefined(dateString) {
    		if(dateString instanceof Date) { 
    			return dateString;
    		}
    		if(dateString == null) {
    			return undefined;
    		}
    		return new Date(dateString.replace(/(\d{2})\.(\d{2})\.(\d{4})/,'$3-$2-$1'));
    	}
		
		function convertToDateTimeOrUndefined(dateString) {
			if(dateString instanceof Date || dateString == null) { 
    			return undefined;
    		}
			return new Date(dateString.replace(/(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2})/,'$3-$2-$1 $4:$5'));
		}
		
		function isDateValid(dateString) {
			var regex_date =/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/;
			if(regex_date.test(dateString) === false) {
				return false;
			}
			var parts   = dateString.split(".");
			var day     = parseInt(parts[0], 10);
			var month   = parseInt(parts[1], 10);
			var year    = parseInt(parts[2], 10);
			if(year < 1000 || year > 3000 || month === 0 || month > 12) {
				return false;
			}
			var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
			if(year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
				monthLength[1] = 29;
			}
			return day > 0 && day <= monthLength[month - 1];
		}
		
		function isTimeValid(timeString) {
			if(timeString.startsWith('-')) {
				timeString = timeString.substring(1, timeString.length);
			}
			
			var regex_time =/^([0-9]{2})\:([0-9]{2})$/;
			if(regex_time.test(timeString) === false) {
				return false;
			}
			var parts = timeString.split(":");
			var hours = parseInt(parts[0], 10);
			var minutes = parseInt(parts[1], 10);
			if(hours >= 0 && hours < 24 && minutes >= 0 && minutes < 60) {
				return true;
			}
			return false;
		} 
		
		function isAmountOfTimeValid(amountOfTime) {
			if(amountOfTime.indexOf(':') == -1) {
				return false;
			}
			var pattern = new RegExp('^-?\\d{1,3}(:\\d{1,2})?$');
			var result = pattern.test(amountOfTime);
			return result;
		}
		
		function isTimeBeforeOther(time1, time2) {
			var t1 = parseFloat( time1.replace(':', '.') );
			var t2 = parseFloat( time2.replace(':', '.') );
			if(t1 < t2) {
				return true;
			}
			return false;
		}
		
		function isDateBeforeOrEqualOtherDate(date1, date2) {
			return date1 > date2;
		}
		
		function formatDateToString(date) {
		   var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
		   var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
		   var yyyy = date.getFullYear();
		   return (dd + "." + MM + "." + yyyy);
		}
		
		function formatDateToDateTimeString(date) {
			   var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
			   var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
			   var yyyy = date.getFullYear();
			   
			   var hh = (date.getHours() < 10 ? '0' : '') + date.getHours();
			   var mm = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
			   
			   return (dd + "." + MM + "." + yyyy + ' ' + hh + ':' + mm);
			}
		
		function overlapping(a, b) {
		    function getMinutes(s) {
		        var p = s.split(':').map(Number);
		        return p[0] * 60 + p[1];
		    }
		    return getMinutes(a.endTime) > getMinutes(b.startTime) && getMinutes(b.endTime) > getMinutes(a.startTime);
		}
		
		function checkArrayConcerningOverlapping(array) {
			var overlap = false;
			for(var n = 0; n < array.length; n++) {
				for(var i = 0; i < n; i++) {
				   var result = overlapping(array[n], array[i]);
				   if(result === true) {
					 overlap = result;
					 break;
				   }
				}
			}
			return overlap;
		}
		
		function createDate(input) {
    		if(typeof input != 'undefined') {
    			var dateString = input.split(' ')[1];
    			var dateTerms = dateString.split('.');
    			var initDate = new Date(dateTerms[2], dateTerms[1], dateTerms[0]);
    			return initDate;
    		}
        }
		
		function joinDateObjectsToDateTimeObject(dateObject, timeObject) {
    		var hours = timeObject.getHours();
    		var minutes = timeObject.getMinutes();
    		var seconds = timeObject.getSeconds();
    		dateObject.setHours(hours);
    		dateObject.setMinutes(minutes);
    		dateObject.setSeconds(seconds);
    		return dateObject;
    	}
		
		function formatStartEndDateTime(dateTimeFrom, dateTimeUntil) {
    		var dateAndTimeFrom = dateTimeFrom.substring(0, dateTimeFrom.indexOf(' '));
    		var dateAndTimeUntil = dateTimeUntil.substring(0, dateTimeUntil.indexOf(' '));
    		if(dateAndTimeFrom == dateAndTimeUntil) {
    			return dateAndTimeFrom + ': <b>' + dateTimeFrom.substring(dateTimeFrom.indexOf(' ')) + '</b> - ' + ' <b>' + dateTimeUntil.substring(dateTimeFrom.indexOf(' ')) + '</b>'; 
    		} else {
    			return dateAndTimeFrom + ': <b>' + dateTimeFrom.substring(dateTimeFrom.indexOf(' ')) + '</b> - ' + '<i>' + dateAndTimeUntil + '</i>: <b>' + dateTimeUntil.substring(dateTimeFrom.indexOf(' ')) + '</b>'; 
    		}
    	}
		
		function oneMonthBack(date) {
			var day = date.getDate();
			var month = date.getMonth();
			var year = date.getFullYear();
    		if(month > 1) {
    			month = month-1;
    		} else {
    			month = 11;
    			year = year-1;
    		}
    		return new Date(year, month, day);
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('numberDrop', numberDrop);
    
	function numberDrop() {
		function getRange(start, offset, range) {
	        var array = [];
	        for (var i = 0; i < range + 1; i++) {
	            array.push(start + offset + i);
	        } 
	        return array;
	    }
		var directive = {
			restrict: 'A',
	        scope: {
	            bindModel: '=',
	            start: '=',
	            range: '=',
	            offset: '='
	        },
	        link: function(scope,element,attrs) {
	            scope.array = getRange(+scope.start, +scope.offset, +scope.range);
	        },
	        template: '<select ng-model="bindModel" ng-options="y for y in array"></select>'
		};
		return directive;	
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('asExcelDownload', asExcelDownload);
    
	function asExcelDownload() {
		var directive = {
			restrict: 'E',
			scope: {
				projectId: '=',
				start: '=',
				end: '='
			},
			templateUrl: 'app/common/excelDownload/asExcelDownload.html'
		};
		return directive;
		
		////////////
	}
})();




(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('folderTreeService', folderTreeService);
    
	folderTreeService.$inject = ['$http', '$q', 'api_config'];
	
	function folderTreeService($http, $q, api_config) {
		var service = {		
			createTree: createTree
		};
		
		return service;
		
		////////////
	
		function createTree(options) {
			var children, e, id, o, pid, temp, _i, _len, _ref;     
	         id =  "id";
	         pid =  "superFolderId";
	         children =  "children";
	         temp = {};
	         o = [];
	         _ref = options.q;
	         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	           e = _ref[_i];
	           e[children] = [];
	           temp[e.id] = e;
	           if (temp[e.superFolderId] != null) { 
	             e.path =  temp[e.superFolderId].path + '/' + e.name;  
	             temp[e.superFolderId].children.push(e);                          
	           }
	           else { // no parent
	             e.path = e.name;
	             o.push(e);     
	           }
	         }
	         return o;
		}
	}
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.common')
    	.directive('modalMovable', ['$document',
    	    function($document) {
    	        return {
    	            restrict: 'AC',
    	            link: function(scope, iElement, iAttrs) {
    	                var startX = 0,
    	                    startY = 0,
    	                    x = 0,
    	                    y = 0;

    	                var dialogWrapper = iElement.parent();

    	                dialogWrapper.css({
    	                    position: 'relative'
    	                });

    	                dialogWrapper.on('mousedown', function(event) {
    	                    // Prevent default dragging of selected content
    	                    var offsetX = event.offsetX;
    	                    var offsetY = event.offsetY;
    	                    if(offsetX < 20 && offsetY < 20) {
    	                    	event.preventDefault();
    	                    	startX = event.pageX - x;
        	                    startY = event.pageY - y;
    	                    	$document.on('mousemove', mousemove);
    	                    	$document.on('mouseup', mouseup);
    	                    }
    	                });

    	                function mousemove(event) {
    	                    y = event.pageY - startY;
    	                    x = event.pageX - startX;
    	                    dialogWrapper.css({
    	                        top: y + 'px',
    	                        left: x + 'px'
    	                    });
    	                }

    	                function mouseup() {
    	                    $document.unbind('mousemove', mousemove);
    	                    $document.unbind('mouseup', mouseup);
    	                }
    	            }
    	        };
    	    }
    	]);
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.common')
    	.directive('ngEnter', function () {
    	    return function (scope, element, attrs) {
    	        element.bind("keydown, keypress", function (event) {
    	            if(event.which === 13) {
    	                scope.$apply(function (){
    	                    scope.$eval(attrs.ngEnter);
    	                });
    	                event.preventDefault();
    	            } 
    	        });
    	    };
    	});
})();
(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('optionsService', optionsService);
    
	optionsService.$inject = ['$http', '$q', 'api_config'];
	
	function optionsService($http, $q, api_config) {
		var roles = false;
		var service = {		
			getCountryTypes: getCountryTypes,
			getRoles: getRoles,
			getAllTitles: getAllTitles,
			getProvinceTypes: getProvinceTypes,
			getProvinceTypesOfCountryType: getProvinceTypesOfCountryType,
			getUserRoles: getUserRoles,
			getCommunicationModes: getCommunicationModes,
			getCalendarEventSerialDateTypes: getCalendarEventSerialDateTypes,
			getProductTypes: getProductTypes,
			getContactTypes: getContactTypes,
			getUnityTypes: getUnityTypes,
			getProductAggregateConditionTypes: getProductAggregateConditionTypes,
			getActivityTypes: getActivityTypes,
			getFacilityDetailsTypes: getFacilityDetailsTypes,
			getCalendarEventTypes: getCalendarEventTypes
		};
		
		return service;
		
		////////////
		
		function getCountryTypes() {
			return $http.get(api_config.BASE_URL + '/options/countrytype');
		}
		
		function getRoles() {
			return $http.get(api_config.BASE_URL + '/options/roles');
		}
		
		function getAllTitles() {
			return $http.get(api_config.BASE_URL + '/options/option/titles');
		}
		
		function getProvinceTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/provincetypes');
		}
		
		function getProvinceTypesOfCountryType(countryType) {
			return $http.get(api_config.BASE_URL + '/options/option/provincetypes/' + countryType);
		}
		
		function getUserRoles() {
			return $http.get(api_config.BASE_URL + '/options/option/userroles');
		}
		
		function getCommunicationModes() {
			return $http.get(api_config.BASE_URL + '/options/option/communicationmodes');
		}
		
		function getCalendarEventSerialDateTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/calendareventserialdates');
		}
		
		function getProductTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/producttypes');
		}
		
		function getContactTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/contacttypes');
		}
		
		function getUnityTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/unitytype');
		}
		
		function getProductAggregateConditionTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/productaggregateconditions');
		}
		
		function getActivityTypes(type) {
			return $http.get(api_config.BASE_URL + '/options/activitytype/' + type);
		}
		
		function getFacilityDetailsTypes() {
			return $http.get(api_config.BASE_URL + '/options/facilitydetails');
		}
		
		function getCalendarEventTypes() {
			return $http.get(api_config.BASE_URL + '/options/calendareventtypes');
		}
	}
})();
(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('printingPDFService', printingPDFService);
    
	printingPDFService.$inject = ['$http', 'api_config'];
	
	function printingPDFService($http, api_config) {
		var service = {			
				printSchedule: printSchedule 
		};
		
		return service;
		
		////////////
    	
		function printSchedule(container, name) {
    		var printElement = document.getElementById(container);
    		makePDF(printElement, name);
    	}
		
    	function makePDF(container, pdfName) {
    	    var quotes = container;
    	    html2canvas(quotes, {
    	        onrendered: function(canvas) {

    	        var pdf = new jsPDF('p', 'pt', 'letter');

    	        var loop = Math.ceil(quotes.clientHeight/980);
    	        for (var i = 0; i <= loop; i++) {
    	            var srcImg  = canvas;
    	            var sX      = 0;
    	            var sY      = 980*i; // start 980 pixels down for every new page
    	            var sWidth  = 900;
    	            var sHeight = 980;
    	            var dX      = 0;
    	            var dY      = 0;
    	            var dWidth  = 900;
    	            var dHeight = 980;

    	            window.onePageCanvas = document.createElement("canvas");
    	            onePageCanvas.setAttribute('width', 900);
    	            onePageCanvas.setAttribute('height', 980);
    	            var ctx = onePageCanvas.getContext('2d');
    	            ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
    	            var canvasDataURL = onePageCanvas.toDataURL("image/png", 0.92);

    	            var width         = onePageCanvas.width;
    	            var height        = onePageCanvas.clientHeight;

    	            var pageHeight = pdf.internal.pageSize.height;
    	            
    	            if (i > 0) {
    	                pdf.addPage(612, pageHeight); //8.5" x 11" in pts (in*72)
    	            }
    	            pdf.setPage(i+1);
    	            pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width*0.62), (height*0.62));

    	        }
    	        var today = new Date();
        	    pdf.save(pdfName + '_' + today.getFullYear() + (today.getMonth()+1) + today.getDate());
    	    }
    	  });
    	}
	}
})();



(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('uploadService', uploadService);
    
	uploadService.$inject = ['$http', '$q', 'api_config', 'Upload', '$timeout'];
	
	function uploadService($http, $q, api_config, Upload, $timeout) {
		var service = {	
			uploadFiles: uploadFiles,
			uploadGeneralFiles: uploadGeneralFiles,
			uploadNewDocumentVersion: uploadNewDocumentVersion,
			uploadSystemImportFile: uploadSystemImportFile 
		};
		
		return service;
		
		////////////
		
		function uploadFiles(files, userId, projectId, superFolderId, documentFileParentType, successCallback, errorCallback, progressCallback) {
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	              var file = files[i];
	              if (!file.$error) {
	                Upload.upload({
	                    url: 'api/fileuploads/fileupload/' + userId + '/' + projectId + '/' + documentFileParentType,
	                    data: {
	                      superfolderid: superFolderId,
	                      file: file  
	                    }
	                }).then(function (resp) {
	                	if (successCallback) {
	                        successCallback(resp);
	                    }
	                }, function (resp) {
	                    if(errorCallback) {
	                    	errorCallback(resp);
	                    }
	                }, function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    var log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n';
	                    if (progressCallback) {
	                        progressCallback(log);
	                    }
	                });
	              }
	            }
	        }
	    }
		
		function uploadGeneralFiles(type, files, successCallback, progressCallback) {
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	              var file = files[i];
	              if (!file.$error) {
	                Upload.upload({
	                    url: 'api/fileuploads/fileupload/' + type,
	                    data: {
	                      file: file  
	                    }
	                }).then(function (resp) {
	                	if (successCallback) {
	                        successCallback(resp);
	                    } 
	                }, null, function (evt) { 
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    var log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n';
	                    if (progressCallback) {
	                        progressCallback(log);
	                    }
	                });
	              }
	            }
	        }
	    }
		
		function uploadNewDocumentVersion(file, userId, projectId, documentFileId, successCallback, progressCallback) {
			if (!file.$error) {
                Upload.upload({
                    url: 'api/fileuploads/fileupload/documentversion/' + userId + '/' + projectId + '/' + documentFileId,
                    data: {
                      file: file  
                    }
                }).then(function (resp) {
                	if (successCallback) {
                        successCallback(resp);
                    }
                }, null, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    var log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n';
                    if (progressCallback) {
                        progressCallback(log);
                    }
                });
             }
		}	
		
		function uploadSystemImportFile(files, userId, systemImportUploadFileType, successCallback, errorCallback, progressCallback) {
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	              var file = files[i];
	              if (!file.$error) {
	                Upload.upload({
	                    url: 'api/fileuploads/fileupload/' + userId + '/systemimport/' + systemImportUploadFileType,
	                    data: {
	                      file: file  
	                    }
	                }).then(function (resp) {
	                	if (successCallback) {
	                        successCallback(resp);
	                    }
	                }, function (resp) {
	                    if(errorCallback) {
	                    	errorCallback(resp);
	                    }
	                }, function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    var log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n';
	                    if (progressCallback) {
	                        progressCallback(log);
	                    }
	                });
	              }
	            }
	        }
	    }
	}
})();

(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('utilService', utilService);
    
	utilService.$inject = ['$http', '$q', 'api_config'];
	
	function utilService($http, $q, api_config) {
		var service = {		
			isImage: isImage,
			replaceAll: replaceAll,
			prepareSearchTerm: prepareSearchTerm
		};
		
		return service;
		
		////////////
		
		function isImage(ending) {
			if(ending == 'jpg' || ending == 'JPG' || ending == 'png' || ending == 'PNG' || ending == 'gif' || ending == 'GIF' || ending == 'gif') {
				return true;
			}
			return false;
		}
		
		function replaceAll(str, find, replace) {
		    return str.replace(new RegExp(find, 'g'), replace);
		}
		
		function prepareSearchTerm(searchTerm) {
			if(searchTerm.indexOf('Rohstoff') > -1) {
				searchTerm = searchTerm.replace('Rohstoff', 'RAW_MATERIAL');
			}
			if(searchTerm.indexOf('rohstoff') > -1) {
				searchTerm = searchTerm.replace('rohstoff', 'RAW_MATERIAL');
			}
			if(searchTerm == 'Reine' || searchTerm == 'Handelsware') {
				searchTerm = searchTerm.replace('Reine', 'TRADING_GOODS');
				searchTerm = searchTerm.replace('Handelsware', 'TRADING_GOODS');
			}
			if(searchTerm == 'reine' || searchTerm == 'handelsware') {
				searchTerm = searchTerm.replace('reine', 'TRADING_GOODS');
				searchTerm = searchTerm.replace('handelsware', 'TRADING_GOODS');
			}
			if(searchTerm == 'Labor') {
				searchTerm = searchTerm.replace('Labor', 'LAB_CHEMICAL');
			} else if(searchTerm == 'labor') {
				searchTerm = searchTerm.replace('labor', 'LAB_CHEMICAL');
			} else if(searchTerm == 'chemikalie') {
				searchTerm = searchTerm.replace('chemikalie', 'LAB_CHEMICAL');
			} else if(searchTerm == 'Laborchemikalie') {
				searchTerm = searchTerm.replace('Laborchemikalie', 'LAB_CHEMICAL');
			} else if(searchTerm == 'laborchemikalie') {
				searchTerm = searchTerm.replace('laborchemikalie', 'LAB_CHEMICAL');
			}
			var preparedSearchTerm = searchTerm.replace('/', '-slash-');
			return preparedSearchTerm;
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('asUniqueEmail', asUniqueEmail);
    
    asUniqueEmail.$inject = ['validateService'];
    
	function asUniqueEmail(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////
		
		function link(scope, element, attrs, ngModel) {
			ngModel.$parsers.unshift(parser);
			ngModel.$formatters.unshift(formatter);
			
			////////////
			
			function parser(value) {
				if(value.length > 3) {
					validateService.checkUniqueEmail(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 3) {
					validateService.checkUniqueEmail(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('asUniqueUsername', asUniqueUsername);
    
    asUniqueUsername.$inject = ['validateService'];
    
	function asUniqueUsername(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////

		//noinspection JSUnusedLocalSymbols
		function link(scope, element, attrs, ngModel) {
			ngModel.$parsers.unshift(parser);
			ngModel.$formatters.unshift(formatter);
			
			////////////
			
			function parser(value){
				if(value.length > 3) {
					validateService.checkUniqueUsername(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 3) {
					validateService.checkUniqueUsername(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('uniqueProductName', uniqueProductName);
    
    uniqueProductName.$inject = ['validateService']; 
    
	function uniqueProductName(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////
		
		function link(scope, element, attrs, ngModel) {
			if(attrs.uniqueProductName == 'CREATE') {
				ngModel.$parsers.unshift(parser);
				ngModel.$formatters.unshift(formatter);
			}
			
			////////////
			
			function parser(value) { 
				if(value.length > 3) {
					validateService.checkUniqueProductName(value, attrs.uniqueProductName).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 3) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProductName(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('uniqueProductNumber', uniqueProductNumber); 
    
    uniqueProductNumber.$inject = ['validateService'];
    
	function uniqueProductNumber(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////
		
		function link(scope, element, attrs, ngModel) {
			if(attrs.uniqueProductNumber == 'CREATE') {
				ngModel.$parsers.unshift(parser);
				ngModel.$formatters.unshift(formatter);
			}
			
			////////////
			
			function parser(value) { 
				if(value.length > 1) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProductNumber(value, attrs.uniqueProductNumber).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 1) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProductNumber(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('uniqueProjectName', uniqueProjectName);
    
    uniqueProjectName.$inject = ['validateService'];
    
	function uniqueProjectName(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////
		
		function link(scope, element, attrs, ngModel) {
			if(attrs.uniqueProjectName == 'CREATE') {
				ngModel.$parsers.unshift(parser);
				ngModel.$formatters.unshift(formatter);
			}
			
			////////////
			
			function parser(value) { 
				if(value.length > 3) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProjectName(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 3) {
					if(value.indexOf('%') > -1) {
						ngModel.$setValidity('unique', false);
						return;
					}
					validateService.checkUniqueProjectName(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.factory('validateService', validateService);
        
    validateService.$inject = ['$http', 'api_config'];
    
    function validateService($http, api_config) {
		var service = {
			checkUniqueUsername: checkUniqueUsername,
			checkUniqueEmail: checkUniqueEmail,
			checkUniqueMandator: checkUniqueMandator,
			checkUniqueProjectName: checkUniqueProjectName,
			checkUniqueProductName: checkUniqueProductName,
			checkUniqueProductNumber: checkUniqueProductNumber
		};
		
		return service;
		
		////////////
		
		function checkUniqueUsername(username) {
			return $http.get(api_config.BASE_URL + '/checkUniqueUsername?username=' + username);
		}
		
		function checkUniqueEmail(email) {
			return $http.get(api_config.BASE_URL + '/checkUniqueEmail?email=' + email);
		}
		
		function checkUniqueMandator(mandator) {
			return $http.get(api_config.BASE_URL + '/checkUniqueMandator?mandator=' + mandator);
		}
		
		function checkUniqueProjectName(projectName) {
			return $http.get(api_config.BASE_URL + '/projects/project/checkuniqueprojectname?projectName=' + projectName);
		}
		
		function checkUniqueProductName(productName, type) {
			var encodedProductName = encodeURIComponent(productName);
			return $http.get(api_config.BASE_URL + '/products/product/checkuniqueproductname/' + type + '?productName=' + encodedProductName);
		}
		
		function checkUniqueProductNumber(productNumber, type) {
			var encodedProductNumber= encodeURIComponent(productNumber);
			return $http.get(api_config.BASE_URL + '/products/product/checkuniqueproductnumber/' + type + '?productNumber=' + encodedProductNumber);
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationService', communicationService);
        
    communicationService.$inject = ['$http', 'api_config'];
    
    function communicationService($http, api_config) {
		var service = {
			createCommunicationUserConnectionMessages: createCommunicationUserConnectionMessages,
			updateCommunicationUserConnectionMessages: updateCommunicationUserConnectionMessages,
			createCommunicationGroup: createCommunicationGroup,
			createCommunicationGroupUserConnections: createCommunicationGroupUserConnections,
			findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween: findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween,
			findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween: findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween,
			findCommunicationGroupUserConnectionsByUser: findCommunicationGroupUserConnectionsByUser,
			findCommunicationGroupUserConnectionsByCommunicationGroup: findCommunicationGroupUserConnectionsByCommunicationGroup,
			findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived: findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived,
			findCommunicationUserConnectionByUserCreatedMessageNotAndAndUserReceivedMessageAndTenantAndArchived: findCommunicationUserConnectionByUserCreatedMessageNotAndAndUserReceivedMessageAndTenantAndArchived,
			getCommunicationUserConnectionsOfUserAndPage: getCommunicationUserConnectionsOfUserAndPage,
			getMessageReceiverOverview: getMessageReceiverOverview,
			updateCommunicationGroup: updateCommunicationGroup,
			updateCommunicationGroupUserConnections: updateCommunicationGroupUserConnections,
			updateCommunicationUserConnection: updateCommunicationUserConnection,
			updateCommunicationMessage: updateCommunicationMessage,
			deleteCommunicationGroup: deleteCommunicationGroup,
			findSentAndOpenOrdersWithConfirmation: findSentAndOpenOrdersWithConfirmation,
			findReceivedAndOpenOrdersWithConfirmation: findReceivedAndOpenOrdersWithConfirmation,
			findSentAndOpenOrders: findSentAndOpenOrders,
			findReceivedAndOpenOrders: findReceivedAndOpenOrders
		};
		
		return service;
		
		////////////
		
		function createCommunicationUserConnectionMessages(userIdSender, communicationMessage) {
			return $http.post(api_config.BASE_URL + '/communications/communication/' + userIdSender, communicationMessage);
		}
		
		function updateCommunicationUserConnectionMessages(communicationMessage) {
			return $http.put(api_config.BASE_URL + '/communications/communication/communicationmessage', communicationMessage);
		}
		
		function createCommunicationGroup(communicationGroup) {
			return $http.post(api_config.BASE_URL + '/communications/communication/communicationgroup', communicationGroup);
		}
		
		function createCommunicationGroupUserConnections(communicationGroupId, userIds) {
			return $http.post(api_config.BASE_URL + '/communications/communication/communicationgroupuserconnection/' + communicationGroupId, userIds);
		}
		
		function findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween(userIdCreatedMessage, start, end) {
			return $http.get(api_config.BASE_URL + '/communications/communication/sent/range/' + userIdCreatedMessage + '/' + start + '/' + end + '/');
		}
		
		function findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween(userIdReceivedMessage, start, end) {
			return $http.get(api_config.BASE_URL + '/communications/communication/received/range/' + userIdReceivedMessage + '/' + start + '/' + end + '/');
		}
		
		function findCommunicationGroupUserConnectionsByUser(userId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/communicationgroupuserconnection/' + userId);
		}
		
		function findCommunicationGroupUserConnectionsByCommunicationGroup(communicationGroupId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/communicationgroupuserconnection/communicationgroup/' + communicationGroupId);
		}

		function findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived(userId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/usercreated/' + userId);
		}

		function findCommunicationUserConnectionByUserCreatedMessageNotAndAndUserReceivedMessageAndTenantAndArchived(userId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/userreceived/' + userId);
		}

		function getCommunicationUserConnectionsOfUserAndPage(userId, page) {
			return $http.get(api_config.BASE_URL + '/communications/communication/' + userId + '/' + page);
		}
		
		function getMessageReceiverOverview(messageId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/receiver/' + messageId);
		}
		
		function updateCommunicationGroup(communicationGroup) {
			return $http.put(api_config.BASE_URL + '/communications/communication', communicationGroup);
		}
		
		function updateCommunicationGroupUserConnections(communicationGroupId, userIds) {
			return $http.put(api_config.BASE_URL + '/communications/communication/communicationgroupuserconnection/' + communicationGroupId, userIds);
		}
		
		function updateCommunicationUserConnection(communicationUserConnection) {
			return $http.put(api_config.BASE_URL + '/communications/communication/communicationuserconnection/' + communicationUserConnection.id, communicationUserConnection);
		}
		
		function updateCommunicationMessage(communicationId, communicationUserConnection) {
			return $http.put(api_config.BASE_URL + '/communications/communication/communication/' + communicationId, communicationUserConnection);
		}
		
		function deleteCommunicationGroup(communicationGroupId) {
			return $http.delete(api_config.BASE_URL + '/communications/communication/' + communicationGroupId);
		}
		
		function findSentAndOpenOrdersWithConfirmation(userIdCreatedMessage, confirmationNeeded, read, executed) {
			return $http.get(api_config.BASE_URL + '/communications/communication/sentandopenorderswithconfirmation/' + userIdCreatedMessage + '/' + confirmationNeeded + '/' + read + '/' + executed);
		}
		
		function findReceivedAndOpenOrdersWithConfirmation(userIdReceivedMessage, confirmationNeeded, read, executed) {
			return $http.get(api_config.BASE_URL + '/communications/communication/sentandopenorderswithconfirmation/' + userIdReceivedMessage + '/' + confirmationNeeded + '/' + read + '/' + executed);
		}
		
		function findSentAndOpenOrders(userIdCreatedMessage, executed) {
			return $http.get(api_config.BASE_URL + '/communications/communication/sentandopenorders/' + userIdCreatedMessage + '/' + executed);
		}
		
		function findReceivedAndOpenOrders(userIdReceivedMessage, executed) {
			return $http.get(api_config.BASE_URL + '/communications/communication/receivedandopenorders/' + userIdReceivedMessage + '/' + executed);
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.controller('CommunicationController', CommunicationController);
    
    CommunicationController.$inject = ['$scope', '$timeout', 'moment', 'currentUser', 'communicationGroupUserConnections', 'communicationUserConnectionsOfUserAndPage', 'allUsers', 'sentAndOpenOrders', 'receivedAndOpenOrders', 'projects', 'defaultProject', 'userAuthorizationUserContainer', 'communicationService', 'communicationGroupCreationModalService', 'messageReceiverOverviewModalService', 'dateUtilityService', 'communicationWorkingOrderCreationModalService', 'projectUserConnectionService', 'yesNoCommunicationModalService', 'printMessageOrdersModalService', 'optionsService', 'imgTemplateModalService', 'printingPDFService'];
    
    function CommunicationController($scope, $timeout, moment, currentUser, communicationGroupUserConnections, communicationUserConnectionsOfUserAndPage, allUsers, sentAndOpenOrders, receivedAndOpenOrders, projects, defaultProject, userAuthorizationUserContainer, communicationService, communicationGroupCreationModalService, messageReceiverOverviewModalService, dateUtilityService, communicationWorkingOrderCreationModalService, projectUserConnectionService, yesNoCommunicationModalService, printMessageOrdersModalService, optionsService, imgTemplateModalService, printingPDFService) {
	    $scope.vm = this;   
    	var vm = this;
    	
    	vm.teaserMessageLength = 150;
    	vm.todoView = currentUser.communicationMode == 'TODO' ? true : false;
    	vm.currentUser = currentUser;
    	vm.allUsers = allUsers.data;
    	vm.communicationGroupUserConnections = communicationGroupUserConnections.data;
    	vm.communicationUserConnectionsOfUserAndPage = communicationUserConnectionsOfUserAndPage.data;
    	vm.sentAndOpenOrders = sentAndOpenOrders.data;
    	vm.receivedAndOpenOrders = receivedAndOpenOrders.data;
    	vm.projects = projects.data;
    	vm.defaultProject = defaultProject.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.selectedProject = vm.projects.length == 1 ? vm.projects[0] : (vm.defaultProject != null ? vm.defaultProject : null);
    	vm.selectedUsers = [];
    	vm.selectedCommunicationGroups = [];
    	vm.message = '';
    	vm.messageEdit = false;
    	vm.communicationUserConnectionOfUserAndPageEdit = null;
    	vm.todayDate = new Date(); 
    	vm.filterStartDate = moment();
    	vm.filterEndDate = moment().toDate();
    	vm.filterStartDate = vm.filterStartDate.add(-1, 'month').toDate();
    	vm.filterStartDatePicker = false;
    	vm.filterEndDatePicker = false;
    	vm.projectUserConnectionsOfProject = findProjectUserConnectionsOfProject();
    	vm.sentOrderType = '-sortDate';
    	vm.receivedOrderType = '-sortDate';
    	vm.communicationUserConnectionOfUserAndPage = null;
    	vm.confirmation = false;
    	vm.executionDate = null;
    	vm.executedSearchTrigger = false;
    	
    	vm.printContainer = printContainer;
    	vm.executionDateAfterToday = executionDateAfterToday;
    	vm.selectedUser = selectedUser;
    	vm.showMessageReceiverOverview = showMessageReceiverOverview;
    	vm.communicationConfirmation = communicationConfirmation;
    	vm.communicationOrderExecuted = communicationOrderExecuted;
    	vm.editCommunicationMessage = editCommunicationMessage;
    	vm.archiveCommunicationUserConnection = archiveCommunicationUserConnection;
    	vm.showCommunicationGroupCreationModal = showCommunicationGroupCreationModal;
    	vm.scrollToBottom = scrollToBottom;
    	vm.addSelectedProject = addSelectedProject;
    	vm.removeSelectedProject = removeSelectedProject;
    	vm.addCommunicationUserConnectionToSentAndOpenOrders = addCommunicationUserConnectionToSentAndOpenOrders;
    	vm.updateCommunicationUserConnectionToSentAndOpenOrders = updateCommunicationUserConnectionToSentAndOpenOrders;
    	vm.showYesNoArchiveCommunicationUserConnection = showYesNoArchiveCommunicationUserConnection;
    	vm.getTeasingMessage = getTeasingMessage;
    	vm.findOpenSentAndReceivedWorkingOrders = findOpenSentAndReceivedWorkingOrders;
    	vm.findWorkingOrdersInTimeRange = findWorkingOrdersInTimeRange;
    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
    	vm.showPrintMessageOrders = showPrintMessageOrders;
    	vm.showImagePreview = showImagePreview;
    	vm.createNewWorkingOrderEntry = createNewWorkingOrderEntry;
    	vm.editWorkingOrder = editWorkingOrder;
    	vm.showDocumentFilePreview = showDocumentFilePreview;
    	vm.executedSearchFilter = executedSearchFilter;
    	scrollToBottom();
    	setSelectionOfSelectedProject(vm.selectedProject);
    	
    	////////////
    	
    	$scope.$watch('vm.selectedProject', function(newValue, oldValue) {
    		if(oldValue != newValue && newValue != null) {
    			findProjectUserConnectionsOfProject();
    		}
		});
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function openFilterStartDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterStartDatePicker = true;
        }
    	
    	function openFilterEndDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterEndDatePicker = true;
        }
    	
    	function getTeasingMessage(message) {
    		var teasingMessage = message.substr(0, vm.teaserMessageLength);
    		return teasingMessage;
    	}
    	
    	function findProjectUserConnectionsOfProject() {
    		if(vm.selectedProject == null) {
    			return;
    		}
    		projectUserConnectionService.findProjectUserConnectionByProject(vm.selectedProject.id).then(function successCallback(response) {
    			vm.projectUserConnectionsOfProject = response.data;
   	   		 }, function errorCallback(response) {
   	   			  console.log('error communication.controller#findProjectUserConnectionsOfProject');
   	   		 });
    	}
    	
    	function executedSearchFilter() {
    		return function( item ) {
    		    if(item.executionDate != null && item.executed === vm.executedSearchTrigger) {
    		    	return true;
    		    } else if(vm.executedSearchTrigger === true) {
    		    	return true;
    		    } else {
    		    	return false;
    		    }
    		  };
    	}
    	
    	function findOpenSentAndReceivedWorkingOrders() {
    		vm.executedSearchTrigger = false;
    		communicationService.findSentAndOpenOrders(vm.currentUser.id, false).then(function (response) {
    			vm.sentAndOpenOrders = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findOpenSentAndReceivedWorkingOrders: ' + data);
			});
			communicationService.findReceivedAndOpenOrders(vm.currentUser.id, false).then(function (response) {
    			vm.receivedAndOpenOrders = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findOpenSentAndReceivedWorkingOrders: ' + data);
			});
    	}
    	
    	function findWorkingOrdersInTimeRange() { 
    		vm.executedSearchTrigger = true;
    		var start = dateUtilityService.formatDateToString(vm.filterStartDate);
    		var end = dateUtilityService.formatDateToString(vm.filterEndDate);    		
    		communicationService.findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween(vm.currentUser.id, start, end).then(function (response) {
    			vm.sentAndOpenOrders = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findWorkingOrdersByProjectAndTenantAndTimeRange: ' + data);
			});
    		communicationService.findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween(vm.currentUser.id, start, end).then(function (response) {
    			vm.receivedAndOpenOrders = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findWorkingOrdersReceivedByProjectAndTenantAndTimeRange: ' + data);
			});
    	}
    	
    	function scrollToBottom() {
          	$timeout(function(){
  	        	var communicationHistoryContainer = document.getElementById("communicationHistoryContainer");
  	        	communicationHistoryContainer.scrollTop = communicationHistoryContainer.scrollHeight;
          	});
          }
    	
    	function selectedUser(user) {
    		if(user.selected === true) {
    			vm.selectedUsers.push(user.id);
    		} else if(user.selected === false) {
    			for(var i = 0; i < vm.selectedUsers.length; i++) {
    				if(vm.selectedUsers[i] == user.id) {
    					vm.selectedUsers.splice(i, 1);
    					break;
    				}
    			}
    		}
    	}
    	
    	function executionDateAfterToday(executionDateString) {
    		if(executionDateString == null) {
    			return false;
    		}
    		var executionDate = executionDateString instanceof Date ? executionDateString : dateUtilityService.convertToDateOrUndefined(executionDateString);
    		if(vm.todayDate.getTime() > executionDate.getTime()) {
    			return true;
    		}
    		return false;
    	}
    	
    	function showMessageReceiverOverview(communicationUserConnectionOfUserAndPage, confirmationNeeded) {
    		var messageId = communicationUserConnectionOfUserAndPage.communication.id;
    		communicationService.getMessageReceiverOverview(messageId).then(function successCallback(response) {
    			var communicationUserConnections = response.data;
    			messageReceiverOverviewModalService.showMessageReceiverOverviewModal(currentUser, communicationUserConnections, confirmationNeeded);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error communication.controller#showMessageReceiverOverview');
  	   		 });
    	}
    	
    	function communicationConfirmation(communicationUserConnectionOfUserAndPage) {
    		communicationService.updateCommunicationUserConnection(communicationUserConnectionOfUserAndPage).then(function successCallback(response) {
  	   		 }, function errorCallback(response) {
  	   			  console.log('error communication.controller#communicationConfirmation');
  	   		 });
    	}
    	
    	function communicationOrderExecuted(communicationUserConnectionOfUserAndPage) {
    		if(false && communicationUserConnectionOfUserAndPage.executed) {
    			communicationUserConnectionOfUserAndPage.executed = false;
    			vm.communicationUserConnectionOfUserAndPage = communicationUserConnectionOfUserAndPage;
    		} else {
	    		communicationService.updateCommunicationUserConnection(communicationUserConnectionOfUserAndPage).then(function successCallback(response) {
	  	   		 }, function errorCallback(response) {
	  	   			  console.log('error communication.controller#communicationOrderExecuted');
	  	   		 });
    		}
    	}
    	
    	function editCommunicationMessage(communicationUserConnectionOfUserAndPage) {
    		if(vm.messageEdit === false) {
    			var message = (typeof communicationUserConnectionOfUserAndPage.communication.title != 'undefined' && communicationUserConnectionOfUserAndPage.communication.title !== '') ? communicationUserConnectionOfUserAndPage.communication.title + '<br />' + communicationUserConnectionOfUserAndPage.communication.message : '' + communicationUserConnectionOfUserAndPage.communication.message;
    			var messageFormatted = message.replace(/<br \/>/g, '\n');
    			vm.message = messageFormatted;
    			vm.messageEdit = true;
    			vm.confirmation = communicationUserConnectionOfUserAndPage.confirmationNeeded;
    			vm.communicationUserConnectionOfUserAndPageEdit = communicationUserConnectionOfUserAndPage;
    			vm.executionDate = communicationUserConnectionOfUserAndPage.executionDate;
    			communicationUserConnectionOfUserAndPage.messageEdit = true;
    		} else {
    			vm.message = '';
    			vm.messageEdit = false;
    			communicationUserConnectionOfUserAndPage.messageEdit = false;
    		}
    	}
    	
    	function archiveCommunicationUserConnection(communicationUserConnectionOfUserAndPage) {
    		if(communicationUserConnectionOfUserAndPage.executed === false && communicationUserConnectionOfUserAndPage.executionDate != null) {
    			communicationUserConnectionOfUserAndPage.archivationNotPossible = true;
    			return;
    		}
    		communicationUserConnectionOfUserAndPage.archivationNotPossible = false;
    		communicationUserConnectionOfUserAndPage.archived = true;
    		communicationService.updateCommunicationUserConnection(communicationUserConnectionOfUserAndPage).then(function successCallback(response) {
    			removeCommunicationUserConnection(communicationUserConnectionOfUserAndPage);
    			removeSentAndOpenOrders(communicationUserConnectionOfUserAndPage);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error communication.controller#archiveCommunicationUserConnection');
  	   		 });
    	}
    	
    	function removeSentAndOpenOrders(communicationUserConnection) {
    		for(var i = 0; i < vm.sentAndOpenOrders.length; i++) {
    			if(vm.sentAndOpenOrders[i].id == communicationUserConnection.id) {
    				vm.sentAndOpenOrders.splice(i, 1);
    			}
    		}
    	}
    	
    	function removeCommunicationUserConnection(communicationUserConnection) {
    		for(var i = 0; i < vm.communicationUserConnectionsOfUserAndPage.length; i++) {
    			if(vm.communicationUserConnectionsOfUserAndPage[i].id == communicationUserConnection.id) {
    				vm.communicationUserConnectionsOfUserAndPage.splice(i, 1);
    				break;
    			}
    		}
    	}
    	
    	function addSelectedProject(project) {
    		vm.selectedProject = project;
    		removeAllProjectSelections(vm.projects);
    		vm.selectedProject.selected = true;
    	}
    	
    	function setSelectionOfSelectedProject(project) {
    		if(project == null) {
    			return;
    		}
    		for(var i = 0; i < vm.projects.length; i++) {
    			if(vm.projects[i].id == project.id) {
    				vm.projects[i].selected = true;
    				break;
    			}
    		}
    	}
    	
    	function removeSelectedProject(project) {
    		vm.selectedProject = null;
    		removeAllProjectSelections(vm.projects);
    	}
    	
    	function removeAllProjectSelections(projects) {
    		for(var i = 0; i < projects.length; i++) {
    			projects[i].selected = false;
    		}
    	}
    	
    	function showCommunicationGroupCreationModal(mode) {
    		communicationGroupCreationModalService.showCommunicationGroupCreationModal(mode, currentUser, null, null, vm.communicationGroupUserConnections);
    	}
    	
    	function createNewWorkingOrderEntry() {
    		if(vm.selectedProject != null) {
	    		projectUserConnectionService.findProjectUserConnectionByProject(vm.selectedProject.id).then(function successCallback(response) {
	    			var usersOfProject = response.data;
	    			communicationWorkingOrderCreationModalService.showCommunicationWorkingOrderCreationModal(vm, currentUser, 'CREATE', vm.selectedProject, vm.projects, usersOfProject, null);
	   	   		 }, function errorCallback(response) {
	   	   			  console.log('error communication.controller#createNewWorkingOrderEntry');
	   	   		 });
    		} else {
    			communicationWorkingOrderCreationModalService.showCommunicationWorkingOrderCreationModal(vm, currentUser, 'CREATE', vm.selectedProject, vm.projects, null, null);
    		}
    	}
    	
    	function addCommunicationUserConnectionToSentAndOpenOrders(communicationUserConnections) {
    		for(var i = 0; i < communicationUserConnections.length; i++) {
    			vm.sentAndOpenOrders.push(communicationUserConnections[i]);
    		}
    	}
    	
    	function updateCommunicationUserConnectionToSentAndOpenOrders(communicationUserConnections) {
    		communicationService.findSentAndOpenOrders(vm.currentUser.id, false).then(function successCallback(response) {
    			vm.sentAndOpenOrders = response.data;
    		}, function errorCallback(response) {
   	   			  console.log('error communication.controller#updateCommunicationUserConnectionToSentAndOpenOrders');
   	   		});
    	}
    	
    	function showYesNoArchiveCommunicationUserConnection(communicationUserConnection) {
    		yesNoCommunicationModalService.showYesNoModal(vm, communicationUserConnection);
    	}
    	
    	function editWorkingOrder(sentAndOpenOrder) {
    		var projectId = null;
    		if(vm.selectedProject != null) {
    			projectId = vm.selectedProject.id;
    		} else {
    			projectId = sentAndOpenOrder.project.id;
    		}
    		projectUserConnectionService.findProjectUserConnectionByProject(projectId).then(function successCallback(response) {
    			var usersOfProject = response.data;
    			communicationWorkingOrderCreationModalService.showCommunicationWorkingOrderCreationModal(vm, currentUser, 'EDIT', vm.selectedProject, vm.projects, usersOfProject, sentAndOpenOrder);
    		}, function errorCallback(response) {
   	   			  console.log('error communication.controller#editWorkingOrder');
   	   		});
    	}
    	
    	function showPrintMessageOrders(mode) {
    		if(mode == 'SENT') {
    			printMessageOrdersModalService.showPrintMessageOrdersModal(vm.sentAndOpenOrders, mode, currentUser);
    		} else if(mode == 'RECEIVED') {
    			printMessageOrdersModalService.showPrintMessageOrdersModal(vm.receivedAndOpenOrders, mode, currentUser);
    		}
    	}
    	
    	function showImagePreview(attachment, subUrl, workingBookEntryId, filename) {
    		var url = subUrl + workingBookEntryId + '/' + filename;
    		imgTemplateModalService.showImgTemplateModal(url, attachment.originalFilename, '--', '--');
    	}
    	
    	function showDocumentFilePreview(attachedFileUrl) {
    		imgTemplateModalService.showImgTemplateModal(attachedFileUrl.url, attachedFileUrl.fileName, '--', attachedFileUrl.version);
    	}
	} 
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.communication')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getCommunicationState());
    	 
    	////////////
			    	
    	function getCommunicationState() {
    		var state = {
    			name: 'auth.communication',
				url: '/communication/:userId',
				templateUrl: 'app/communication/communication/communication.html',
				controller: 'CommunicationController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					projectsService: 'projectsService',
					communicationService: 'communicationService',
					communicationGroupUserConnections: function findCommunicationGroupUserConnectionsByUser(communicationService, $stateParams) {
						return null;
					},
					communicationUserConnectionsOfUserAndPage: function getCommunicationUserConnectionsOfUserAndPage($stateParams, communicationService) {
						return null;
					},
					allUsers: function findAllUsers(userService) {
						return userService.findAllUsers();
					},
					sentAndOpenOrders: function findSentAndOpenOrders($stateParams, communicationService) {
						return communicationService.findSentAndOpenOrders($stateParams.userId, false);
					},
					receivedAndOpenOrders: function findReceivedAndOpenOrders($stateParams, communicationService) {
						return communicationService.findReceivedAndOpenOrders($stateParams.userId, false);
					},
					projects: function findProjectsOfUser($stateParams, projectsService) {
						return projectsService.findProjectsOfUser($stateParams.userId);
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.directive('communicationCreateFormDirective', communicationCreateFormDirective);
    
    communicationCreateFormDirective.$inject = ['$timeout', 'communicationService', 'communicationAttachmentFilesModalService', 'dateUtilityService'];
    
	function communicationCreateFormDirective($timeout, communicationService, communicationAttachmentFilesModalService, dateUtilityService) {
		var directive = {
			restrict: 'E',
			scope: {
				invoker: '=',
				confirmation: '=',
				selectedProject: '=',
				executionDate: '=',
				selectedCommunicationGroups: '=',
				selectedUsers: '=',
				currentUser: '=',
				communicationUserConnectionsOfUserAndPage: '=',
				message: '=',
				messageEdit: '=',
				communicationUserConnectionOfUserAndPageEdit: '='
			},
			templateUrl: 'app/communication/directives/communicationCreateFormDirective/communicationCreateForm.html',
			link: function($scope) {
				$scope.message = null;
				$scope.editMessageMode = false;
				$scope.confirmation = $scope.confirmation;
				$scope.message = null;
				$scope.executionDate = $scope.executionDate;
				
				$scope.openExecutionDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.executionDatePicker = true;
		        };
				
				$scope.sendMessage = function() {
		        	if($scope.messageEdit) {
		        		$scope.editMode();
		        	} else {
			            $scope.createMode();
		        	}
				};
				
				$scope.editMode = function() {
					$scope.communicationUserConnectionOfUserAndPageEdit.executionDate = $scope.executionDate;
					$scope.communicationUserConnectionOfUserAndPageEdit.confirmationNeeded = $scope.confirmation;
					$scope.communicationUserConnectionOfUserAndPageEdit.communication.message = $scope.message;
					Date.prototype.toJSON = function () { return this.toLocaleString(); };
					communicationService.updateCommunicationMessage($scope.communicationUserConnectionOfUserAndPageEdit.communication.id, $scope.communicationUserConnectionOfUserAndPageEdit).then(function successCallback(response) {
						$scope.messageEdit = false;
						$scope.communicationUserConnectionOfUserAndPageEdit = response.data;
						$scope.unsetCommunicationCreateForm();
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error communicationCreateForm.directive#editMode');
		  	   		 });
				};
				
				$scope.createMode = function() {
		            if($scope.message != null && $scope.message.length > 0 && $scope.usersOrGroupsSelected() === false) {
		            	$scope.files = null;
		            	$scope.noMessageError = false;
		            	$scope.noUserSelectedError = false;
		            	
		            	Date.prototype.toJSON = function () { return this.toLocaleString(); };
		            	$scope.stompClient.send("/app/api/communications/communication/" + $scope.currentUser.id, {}, JSON.stringify({ 'id': $scope.currentUser.id,'name': $scope.currentUser.title + ' ' + $scope.currentUser.firstname + ' ' + $scope.currentUser.surname, 'message': $scope.message, 'associatedToGroup': $scope.selectedCommunicationGroups.length > 0, 'communicationGroupIds': $scope.selectedCommunicationGroups,'usersSelected': $scope.selectedUsers,'project': $scope.selectedProject ,'confirmation': $scope.confirmation,'relevantForWorkingBook': false,'elapsedDate': '', 'executionDate': $scope.executionDate, 'selectedDocumentFiles': $scope.selectedDocumentFiles}));
		            	$scope.unsetCommunicationCreateForm();
		            } else if($scope.message == null || $scope.message.length < 2) {
		            	$scope.noMessageError = true;
		            	$scope.noUserSelectedError = false;
		            } else if($scope.usersOrGroupsSelected() === true) {
		            	$scope.noUserSelectedError = true;
		            	$scope.noMessageError = false;
		            	console.log('please select user to communicate for chat.controller.js');
		            }
				};
				
				$scope.unsetCommunicationCreateForm = function() {
					$scope.confirmation = false;
					$scope.message = null;
					$scope.executionDate = null;
					$scope.selectedDocumentFiles = [];
				};
				
				$scope.usersOrGroupsSelected = function() {
					return ($scope.selectedCommunicationGroup == null && $scope.selectedUsers == null) || ($scope.selectedCommunicationGroups.length === 0 && $scope.selectedUsers.length === 0);
				};
				
				$scope.connect = function() {
		            var socket = new SockJS('/api/chat');
		            $scope.stompClient = Stomp.over(socket);            
		            $scope.stompClient.connect({}, function(frame) {
		                console.log('webSocket Connected in communicationCreateForm.directive.js: ' + frame);
	                	$scope.stompClient.subscribe('/topic/communication/' + $scope.currentUser.id, function(message) {
	                		var parsedMessage = JSON.parse(message.body);
	                		$scope.communicationUserConnectionsOfUserAndPage.push(parsedMessage);
	                		$scope.$apply();
	                		$scope.invoker.scrollToBottom();
	                    });
		            });
		        };
				
				$scope.addAttachmentFiles = function() {
					communicationAttachmentFilesModalService.showCommunicationAttachmentFilesModal($scope);
				};
				
				$scope.attachFiles = function(selectedDocumentFiles) {
		    		$scope.selectedDocumentFiles = selectedDocumentFiles;
		    	};
				
				$scope.$on('$destroy', function() { // invoked if page is leaved 
		        	if($scope.stompClient != null) {
		        		$scope.stompClient.unsubscribe();
		        		$scope.stompClient.disconnect(function() {
		                	console.log("Disconnected communicationCreateForm.directive#destroy");
		                });
		            } else {
		            	console.log("vm.stompClient is null and cannot be disconnected communicationCreateForm.directive#destroy");
		            }
		    	});
				$scope.connect();
			 }
		};
		return directive;
		
		////////////
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.directive('communicationGroupsDirective', communicationGroupsDirective);
    
    communicationGroupsDirective.$inject = ['$timeout', 'communicationService', 'communicationGroupCreationModalService', 'communicationGroupDeletionModalService'];
    
	function communicationGroupsDirective($timeout, communicationService, communicationGroupCreationModalService, communicationGroupDeletionModalService) {
		var directive = {
			restrict: 'E',
			scope: {
				selectedCommunicationGroups: '=',
				communicationGroupUserConnections: '=',
				currentUser: '='
			},
			templateUrl: 'app/communication/directives/communicationGroupsDirective/communicationGroups.html',
			link: function($scope) {
				$scope.membersOfMessageGroup = null;
				
				$scope.communicationGroupUserConnectionSelected = function(communicationGroupUserConnection) {
					if(typeof $scope.selectedCommunicationGroups != 'undefined') { 
						if(communicationGroupUserConnection.selected === true) {
							$scope.selectedCommunicationGroups.push(communicationGroupUserConnection.communicationGroup.id);
						} else if(communicationGroupUserConnection.selected === false) {
							for(var i = 0; i < $scope.selectedCommunicationGroups.length; i++) {
								if($scope.selectedCommunicationGroups[i] == communicationGroupUserConnection.communicationGroup.id) {
									$scope.selectedCommunicationGroups.splice(i, 1);
									break;
								}
							}
						}
					}
				};
				
				$scope.showMembersOfCommunicationGroup = function(communicationGroupUserConnection) {
					for(var i = 0; i < $scope.communicationGroupUserConnections.length; i++) {
		        		if($scope.communicationGroupUserConnections[i].id != communicationGroupUserConnection.id) {
		        			$scope.communicationGroupUserConnections[i].showMembers = false;
		        		}
		        	}
		        	
		        	if(communicationGroupUserConnection.showMembers == null || communicationGroupUserConnection.showMembers === false) {
		        		communicationService.findCommunicationGroupUserConnectionsByCommunicationGroup(communicationGroupUserConnection.communicationGroup.id).then(function (response) {
		        			$scope.membersOfCommunicationGroup = response.data;
		        			communicationGroupUserConnection.showMembers = true;
		        		}).catch(function (data) {
		        			console.log('error communicationGroups.directive#showMembersOfCommunicationGroup: ' + data);
		        		});
					  } else {
						  communicationGroupUserConnection.showMembers = false;
					  }
				};
				
				$scope.showCommunicationGroupEditModal = function(mode, communicationGroupUserConnection) {
					communicationService.findCommunicationGroupUserConnectionsByCommunicationGroup(communicationGroupUserConnection.communicationGroup.id).then(function (response) {
						var membersOfCommunicationGroup = response.data;
						communicationGroupCreationModalService.showCommunicationGroupCreationModal(mode, $scope.currentUser, membersOfCommunicationGroup, communicationGroupUserConnection, $scope.communicationGroupUserConnections);
		    		}).catch(function (data) {
						console.log('error communicationGroups.directive#showCommunicationGroupEditModal: ' + data);
					});
				};
				
				$scope.showCommunicationGroupDeletionModal = function(communicationGroup, communicationGroupUserConnections) {
		    		communicationGroupDeletionModalService.showCommunicationGroupDeletionModal(communicationGroup, communicationGroupUserConnections);
		    	};
			 }
		};
		return directive;
		
		////////////
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationAttachmentFilesModalService', communicationAttachmentFilesModalService);
        
    communicationAttachmentFilesModalService.$inject = ['$modal', '$timeout', '$stateParams', 'documentFileService'];
    
    function communicationAttachmentFilesModalService($modal, $timeout, $stateParams, documentFileService) {
		var service = {
			showCommunicationAttachmentFilesModal: showCommunicationAttachmentFilesModal
		};		
		return service;
		
		////////////
				
		function showCommunicationAttachmentFilesModal(invoker) {
			 var communicationAttachmentFilesModal = $modal.open({
				controller: CommunicationAttachmentFilesModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/communicationAttachmentFiles/communicationAttachmentFiles.modal.html'
			});
			return communicationAttachmentFilesModal;
			
			function CommunicationAttachmentFilesModalController($modalInstance, $scope, invoker) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.selectedDocumentFiles = [];
		    	vm.selectedDocumentFileSuccess = false;
		    	vm.selectedDocumentFileStillExists = false;
		    	
		    	vm.findDocumentFilesBySearchString = findDocumentFilesBySearchString;
		    	vm.addDocumentFile = addDocumentFile;
		    	vm.documentFileVersionSelected = documentFileVersionSelected;
		    	vm.existingDocumentFile = existingDocumentFile;
		    	vm.removeDocumentFile = removeDocumentFile;
		    	vm.attachFiles = attachFiles;
		    	vm.closeCommunicationAttachmentFilesModal = closeCommunicationAttachmentFilesModal;
		    	
		    	function findDocumentFilesBySearchString(searchString) {
		    		return documentFileService.findDocumentFilesBySearchString(searchString).then(function (response) {
		    			return response.data;
		    		}).catch(function (data) {
						console.log('error communicationAttachmentFiles.controller#findDocumentFilesBySearchString: ' + data);
					});
		    	}
		    	
		    	function addDocumentFile(selectedDocumentFile) {
		    		vm.selectedDocumentFileStillExists = false;
		    		if(selectedDocumentFile.documentFileVersions.length == 1) {
		    			if(existingDocumentFile(selectedDocumentFile, selectedDocumentFile.documentFileVersions[0]) === true) {
			    			return;
			    		}
		    			var selectedDocumentFileWithVersion = {};
		    			selectedDocumentFileWithVersion.selectedDocumentFile = selectedDocumentFile;
		    			selectedDocumentFileWithVersion.documentFileVersion = selectedDocumentFile.documentFileVersions[0];
			    		vm.selectedDocumentFiles.push(selectedDocumentFileWithVersion);
			    		vm.selectedDocumentFileSuccess = true;
			    		setAddDocumentFileTimeout();
			    		$scope.selectedDocumentFile = null;
		    		} else {
		    			vm.showVersionsOfDocumentFile = true;
		    		}
		    	}
		    	
		    	function documentFileVersionSelected(selectedDocumentFile, documentFileVersion, selectedState) {
		    		if(selectedState === true && existingDocumentFile(selectedDocumentFile, documentFileVersion) === true) {
		    			return;
		    		}
		    		if(selectedState === true) {
		    			var selectedDocumentFileWithVersion = {};
		    			selectedDocumentFileWithVersion.selectedDocumentFile = selectedDocumentFile;
		    			selectedDocumentFileWithVersion.documentFileVersion = documentFileVersion;
		    			vm.selectedDocumentFiles.push(selectedDocumentFileWithVersion);
		    		} else if(selectedState === false) {
		    			removeDocumentFile(selectedDocumentFile, documentFileVersion);
		    		}
		    	}
		    	
		    	function existingDocumentFile(selectedDocumentFile, documentFileVersion) {
		    		for(var i = 0; i < vm.selectedDocumentFiles.length; i++) {
		    			if(vm.selectedDocumentFiles[i].selectedDocumentFile.id == selectedDocumentFile.id &&
		    					vm.selectedDocumentFiles[i].documentFileVersion.version == documentFileVersion.version) {
		    				vm.selectedDocumentFileStillExists = true;
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function removeDocumentFile(selectedDocumentFile, documentFileVersion) {
		    		for(var i = 0; i < vm.selectedDocumentFiles.length; i++) {
		    			if(vm.selectedDocumentFiles[i].selectedDocumentFile.id == selectedDocumentFile.id &&
		    					vm.selectedDocumentFiles[i].documentFileVersion.version == documentFileVersion.version) {
		    				vm.selectedDocumentFiles.splice(i, 1);
		    				break;
		    			}
		    		}
		    	}
		   
		    	function attachFiles() {
		    		vm.invoker.attachFiles(vm.selectedDocumentFiles);
		    		closeCommunicationAttachmentFilesModal();
		    	}
		    	
		    	function setAddDocumentFileTimeout() {
		      		$timeout(function() {
		      			vm.selectedDocumentFileSuccess = false;
		  			   }, 2000); 
		      	}
		    	
		    	function closeCommunicationAttachmentFilesModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationGroupCreationModalService', communicationGroupCreationModalService);
        
    communicationGroupCreationModalService.$inject = ['$modal', '$stateParams', 'userService', 'communicationService'];
    
    function communicationGroupCreationModalService($modal, $stateParams, userService, communicationService) {
		var service = {
			showCommunicationGroupCreationModal: showCommunicationGroupCreationModal
		};		
		return service;
		
		////////////
				
		function showCommunicationGroupCreationModal(mode, currentUser, membersOfCommunicationGroup, communicationGroupUserConnection, communicationGroupUserConnections) {
			 var communicationGroupCreationModal = $modal.open({
				controller: CommunicationGroupCreationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	mode: function() {
			    		return mode;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}, 
			    	membersOfCommunicationGroup: function() {
			    		return membersOfCommunicationGroup;
			    	},
			    	communicationGroupUserConnection: function() {
			    		return communicationGroupUserConnection;
			    	},
			    	communicationGroupUserConnections: function() {
			    		return communicationGroupUserConnections;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/communicationGroupCreation/communicationGroupCreation.modal.html'
			});
			return communicationGroupCreationModal;
			
			function CommunicationGroupCreationModalController($modalInstance, $scope, mode, currentUser, membersOfCommunicationGroup, communicationGroupUserConnection, communicationGroupUserConnections) {
		    	var vm = this; 
		    	vm.mode = mode;
		    	vm.currentUser = currentUser;
		    	vm.selectedUser = null;
		    	vm.membersOfCommunicationGroup = membersOfCommunicationGroup != null ? membersOfCommunicationGroup : [];
		    	vm.usersOfGroup = vm.mode === 'EDIT' ? addUsersOfGroup() : [currentUser];
		    	vm.communicationGroupName = vm.mode === 'EDIT' ? communicationGroupUserConnection.communicationGroup.name : '';
		    	vm.communicationGroupDescription = vm.mode === 'EDIT' ? communicationGroupUserConnection.communicationGroup.description : '';
		    	vm.communicationGroupColor = vm.mode === 'CREATE' ? '#ff0000' : communicationGroupUserConnection.communicationGroup.color;
		    	vm.communicationGroupUserConnections = communicationGroupUserConnections;
		    	
		    	vm.getUsersByTerm = getUsersByTerm;
		    	vm.addUserToGroup = addUserToGroup;
		    	vm.removeUserFromGroup = removeUserFromGroup;
		    	vm.createCommunicationGroup = createCommunicationGroup;
		    	vm.updateCommunicationGroup = updateCommunicationGroup;
		    	vm.closeCommunicationGroupCreationModal = closeCommunicationGroupCreationModal;
		    	
		    	////////////
		    	
		    	function addUsersOfGroup() {
	    			  var usersOfGroupToEdit = [];
	    			  for(var i = 0; i <  vm.membersOfCommunicationGroup.length; i++) {
	    				  usersOfGroupToEdit.push(vm.membersOfCommunicationGroup[i].user);
	    			  }
	    			  return usersOfGroupToEdit;
	    		  }
		    	
		    	function getUsersByTerm(term) {
		    		return userService.findUsersByTerm(term).then(function successCallback(response) {
		    			return response.data;
		 	   		 }, function errorCallback(response) {
		 	   			  console.log('error administration.controller#searchUsersByTerm');
		 	   		 });
		    	}
		    	
		    	function addUserToGroup() {
		    		if(typeof vm.selectedUser === 'undefined') {
	    				  return;
	    			  }
	    			  var addTrigger = true;
	    			  for(var i = 0; i < vm.usersOfGroup.length; i++) {
	    				  if(vm.selectedUser != null && vm.usersOfGroup[i].id === vm.selectedUser.id) {
	    					  addTrigger = false;
	    					  vm.communicationGroupCreationError = true;
	    					  break;
	    				  }
	    			  }
	    			  if(addTrigger) {
	    				  vm.communicationGroupCreationError = false;
	    				  vm.usersOfGroup.push(vm.selectedUser);
	        			  vm.selectedUser = null;
	    			  }
		    	}
		    	
		    	function removeUserFromGroup(userOfGroup) {
	    			  for(var i = 0; i < vm.usersOfGroup.length; i++) {
	    				  if(vm.usersOfGroup[i].id === userOfGroup.id) {
	    					  vm.usersOfGroup.splice(i, 1);
	    					  break;
	    				  }
	    			  }
	    		  }
		    	
		    	function createCommunicationGroup() {
	    			  var communicationGroup = {};
	    			  communicationGroup.name = vm.communicationGroupName;
	    			  communicationGroup.description = vm.communicationGroupDescription; 
	    			  communicationGroup.color = vm.communicationGroupColor;
	    			  communicationGroup.userCreatedCommunicationGroup = currentUser;
	    			    
	    			  communicationService.createCommunicationGroup(communicationGroup).then(function (response) {
	    				  var createdCommunicationGroup = response.data;
	    				  communicationService.createCommunicationGroupUserConnections(createdCommunicationGroup.id, vm.usersOfGroup).then(function (response) {
	    					  for(var i = 0; i < response.data.length; i++) {
	    						  if(response.data[i].user.id == currentUser.id) {
	    							  vm.communicationGroupUserConnections.push(response.data[i]);
	    							  vm.closeCommunicationGroupCreationModal();
	    							  break;
	    						  } 
	    					  }
	    				  }).catch(function (data) {
		   	  				   console.log('error communicationGroupCreation#createCommunicationGroupUserConnections: ' + data);
		   	  			   });
		  			   }).catch(function (data) {
		  				   console.log('error communicationGroupCreation#createCommunicationGroup: ' + data);
		  			   });
	    		  }
	    		  
	    		  function updateCommunicationGroup() {
	    			  var communicationGroup = {};
	    			  communicationGroup.id = communicationGroupUserConnection.communicationGroup.id;
	    			  communicationGroup.name = vm.communicationGroupName;
	    			  communicationGroup.description = vm.communicationGroupDescription; 
	    			  communicationGroup.color = vm.communicationGroupColor;
	    			  communicationGroup.userCreatedCommunicationGroup = currentUser;
	    			  communicationService.updateCommunicationGroup(communicationGroup).then(function (response) {
	    				  for(var i = 0; i < communicationGroupUserConnections.length; i++) {
	    					  if(communicationGroupUserConnections[i].communicationGroup.id === communicationGroupUserConnection.communicationGroup.id) {
	    						  communicationGroupUserConnections[i].communicationGroup.name = communicationGroup.name;
	    						  communicationGroupUserConnections[i].communicationGroup.description = communicationGroup.description;
	    						  communicationGroupUserConnections[i].communicationGroup.color = communicationGroup.color;
	    						  
	    						  communicationService.updateCommunicationGroupUserConnections(communicationGroupUserConnection.communicationGroup.id, vm.usersOfGroup).then(function (response) {
	    							  communicationGroupUserConnection.showMembers = false;
	    							  vm.closeCommunicationGroupCreationModal();
	    						  }).catch(function (data) {
	    			  				   console.log('error communicationGroupCreation.service#updateCommunicationGroupUserConnection: ' + data);
	    			  			   });
	    						  break;
	    					  }
	    				  }
		  			   }).catch(function (data) {
		  				   console.log('error communicationGroupCreation.service#updateCommunicationGroup: ' + data);
		  			   });
	    		  }
		    	
		    	function closeCommunicationGroupCreationModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationGroupDeletionModalService', communicationGroupDeletionModalService);
        
    communicationGroupDeletionModalService.$inject = ['$modal', '$stateParams', 'communicationService'];
    
    function communicationGroupDeletionModalService($modal, $stateParams, communicationService) {
		var service = {
			showCommunicationGroupDeletionModal: showCommunicationGroupDeletionModal
		};		
		return service;
		
		////////////
				
		function showCommunicationGroupDeletionModal(communicationGroup, communicationGroupUserConnections) {
			 var communicationGroupDeletionModal = $modal.open({
				controller: CommunicationGroupDeletionnModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	communicationGroup: function() {
			    		return communicationGroup;
			    	},
			    	communicationGroupUserConnections: function() {
			    		return communicationGroupUserConnections;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/communicationGroupDeletion/communicationGroupDeletion.modal.html'
			});
			return communicationGroupDeletionModal;
			
			function CommunicationGroupDeletionnModalController($modalInstance, $scope, communicationGroup, communicationGroupUserConnections) {
		    	var vm = this; 
		    	vm.communicationGroup = communicationGroup;
		    	vm.communicationGroupUserConnections = communicationGroupUserConnections;
		    	
		    	vm.deleteCommunicationGroup = deleteCommunicationGroup;
		    	vm.closeCommunicationGroupDeletionModal = closeCommunicationGroupDeletionModal;
		    	
		    	////////////
		    	
		    	function deleteCommunicationGroup() {
	      			communicationService.deleteCommunicationGroup(communicationGroup.id).then(function (response) {
	            		for(var i = 0; i < vm.communicationGroupUserConnections.length; i++) {
	            			if(vm.communicationGroupUserConnections[i].communicationGroup.id === communicationGroup.id) {
	            				communicationGroupUserConnections.splice(i, 1);
	            				vm.closeCommunicationGroupDeletionModal();
	            				break;
	            			}
	            		}
	        		}).catch(function (data) {
	        			console.log('error communicationGroupDeletion.service#deleteCommunicationGroup: ' + data);
	        		});
		    	}
		    	
		    	function closeCommunicationGroupDeletionModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationWorkingOrderCreationModalService', communicationWorkingOrderCreationModalService);
        
    communicationWorkingOrderCreationModalService.$inject = ['$modal', '$stateParams', 'projectUserConnectionService', 'communicationService', 'dateUtilityService', 'communicationAttachmentFilesModalService', 'uploadService'];
    
    function communicationWorkingOrderCreationModalService($modal, $stateParams, projectUserConnectionService, communicationService, dateUtilityService, communicationAttachmentFilesModalService, uploadService) {
		var service = {
			showCommunicationWorkingOrderCreationModal: showCommunicationWorkingOrderCreationModal
		};		
		return service;
		
		////////////
				
		function showCommunicationWorkingOrderCreationModal(invoker, currentUser, mode, selectedFilterProject, projects, usersOfProject, communicationUserConnection) {
			 var communicationGroupCreationModal = $modal.open({
				controller: CommunicationWorkingOrderCreationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	mode: function() {
			    		return mode;
			    	},
			    	selectedFilterProject: function() {
			    		return selectedFilterProject;
			    	},
			    	projects: function() {
			    		return projects;
			    	}, 
			    	usersOfProject: function() {
			    		return usersOfProject;
			    	}, 
			    	communicationUserConnection: function() {
			    		return communicationUserConnection;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/communicationWorkingOrderCreation/communicationWorkingOrderCreation.modal.html'
			});
			return communicationGroupCreationModal;
			
			function CommunicationWorkingOrderCreationModalController($modalInstance, $scope, invoker, currentUser, mode, selectedFilterProject, projects, usersOfProject, communicationUserConnection) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.currentUser = currentUser;
		    	vm.name = vm.currentUser.title + " " + vm.currentUser.firstname + " " + vm.currentUser.surname;
		    	vm.mode = mode;
		    	vm.communicationUserConnection = communicationUserConnection;
		    	vm.selectedFilterProject = vm.mode == 'EDIT' ? communicationUserConnection.project : selectedFilterProject;
		    	vm.projects = projects;
		    	vm.usersOfProject = usersOfProject;
		    	vm.executionDateDatePicker = false;
		    	vm.uploadFiles = false;
		    	vm.message = vm.mode == 'EDIT' ? vm.communicationUserConnection.communication.title + ' ' + vm.communicationUserConnection.communication.message : '';
		    	vm.message = prepareMessage(vm.message);
		    	vm.communicationWorkingOrder = {
		    		id: vm.currentUser.id,
		    		name: vm.name,
		    		message: vm.mode == 'EDIT' ? vm.message : '',
		    		associatedToGroup: null,
		    		project: vm.selectedFilterProject,
		    		communicationGroupIds: null,
		    		usersSelected: vm.mode == 'EDIT' ? [vm.communicationUserConnection.userReceivedMessage] : [],
		    		confirmation: true,
		    		elapsedDate: null,
		    		executionDate: vm.mode == 'EDIT' ? dateUtilityService.convertToDateOrUndefined(vm.communicationUserConnection.executionDate) : null,
		    		relevantForWorkingBook: false,
		    		selectedDocumentFiles: vm.mode == 'EDIT' ? prepareSelectedDocumentFiles(communicationUserConnection.communication.attachedFileUrls) : [],
		    		communicationAttachments: vm.mode == 'EDIT' ? communicationUserConnection.communication.communicationAttachments : []
		    	};
		    	
		    	vm.openEndDatePicker = openEndDatePicker;
		    	vm.addUser = addUser;
		    	vm.addProject = addProject;
		    	vm.removeProject = removeProject;
		    	vm.userStillSelected = userStillSelected;
		    	vm.removeUser = removeUser;
		    	vm.addAttachmentFiles = addAttachmentFiles;
		    	vm.attachFiles = attachFiles;
		    	vm.removeAttachment = removeAttachment;
		    	vm.removeAttachmentDocumentFile = removeAttachmentDocumentFile;
		    	vm.createCommunicationWorkingOrder = createCommunicationWorkingOrder;
		    	vm.closeCommunicationWorkingOrderCreationModal = closeCommunicationWorkingOrderCreationModal;
		    	
		    	////////////
		    	
		    	function prepareSelectedDocumentFiles(attachedFileUrls) {
		    		var selectedDocumentFiles = [];
		    		if(attachedFileUrls == null) {
		    			return selectedDocumentFiles;
		    		}
		    		for(var i = 0; i < attachedFileUrls.length; i++) {
		    			var selectedDocumentFile = {};
		    			var selectedDocumentFileContainer = {};
		    			selectedDocumentFileContainer.projectId = attachedFileUrls[i].projectId;
		    			selectedDocumentFileContainer.id = attachedFileUrls[i].documentFileId;
		    			selectedDocumentFileContainer.fileName = attachedFileUrls[i].fileName;
		    			selectedDocumentFileContainer.url = attachedFileUrls[i].url;
		    			selectedDocumentFile.selectedDocumentFile = selectedDocumentFileContainer;
		    			
		    			var documentFileVersion = {};
		    			documentFileVersion.version = attachedFileUrls[i].version; 
		    			selectedDocumentFile.documentFileVersion = documentFileVersion;
		    			selectedDocumentFiles.push(selectedDocumentFile);
		    		}
		    		return selectedDocumentFiles;
		    	}
		    	
		    	function prepareMessage(message) {
		    		while(message.indexOf('<br />') > -1) {
		    			message = message.replace('<br />', '\n');
		    	      }
		    		return message;
		    	}
		    	
		    	function addProject(project) {
		    		projectUserConnectionService.findProjectUserConnectionByProject(project.id).then(function successCallback(response) {
		    			removeProjectSelection();
		    			vm.communicationWorkingOrder.usersSelected = [];
		    			vm.usersOfProject = response.data;
		    			project.selected = true;
			    		vm.selectedFilterProject = project;
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error communication.controller#createNewWorkingOrderEntry');
		   	   		 });
		    	}
		    	
		    	function addAttachmentFiles() {
		    		communicationAttachmentFilesModalService.showCommunicationAttachmentFilesModal(vm);
		    	}
		    	
		    	function attachFiles(selectedDocumentFiles) {
		    		for(var i = 0; i < selectedDocumentFiles.length; i++) {
		    			vm.communicationWorkingOrder.selectedDocumentFiles.push(selectedDocumentFiles[i]);
		    		}
		    	}
		    	
		    	$scope.uploadExternalFiles = function(files, errFiles) {
		      		uploadService.uploadGeneralFiles('communication', files, $scope.successCallback, $scope.progressCallback);
		    	};
		    	
		    	$scope.progressCallback = function(response) {
		    		vm.uploadFiles = true;
		    		console.log("file upload progress in communicationWorkingOrderCreation.service#progressCallback");
		    	};
		    	
		    	$scope.successCallback = function(response) {
		    		var fileUploadResponseContainer = response.data;
		    		$scope.initializeAttachments();
		    		vm.communicationWorkingOrder.communicationAttachments.push(fileUploadResponseContainer);
		    		vm.uploadFiles = false;
		    		console.log("file upload success in communicationWorkingOrderCreation.service#successCallback");
		    	};
		    	
		    	$scope.initializeAttachments = function() {
		    		if(vm.communicationWorkingOrder.communicationAttachments == null || typeof vm.communicationWorkingOrder.communicationAttachments == 'undefined') {
		    			vm.communicationWorkingOrder.communicationAttachments = [];
		    		}
		    	};
		    	
		    	function removeAttachment(attachment) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.communicationAttachments.length; i++) {
		    			if(vm.communicationWorkingOrder.communicationAttachments[i].filename == attachment.filename) {
		    				vm.communicationWorkingOrder.communicationAttachments.splice(i, 1);
		    			}
		    		}
		    	}
		    	
		    	function removeAttachmentDocumentFile(selectedDocumentFile) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.selectedDocumentFiles.length; i++) {
		    			if(vm.communicationWorkingOrder.selectedDocumentFiles[i].fileName == selectedDocumentFile.fileName) {
		    				vm.communicationWorkingOrder.selectedDocumentFiles.splice(i, 1);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function addUser(user) { 
		    		if(userStillAdded(user) === false) {
		    			vm.noUsersSelected = false;
		    			vm.communicationWorkingOrder.usersSelected.push(user);
		    		}
		    	}
		    	
		    	function userStillAdded(user) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.usersSelected.length; i++) {
		    			if(vm.communicationWorkingOrder.usersSelected[i].id == user.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function removeProject(project) {
		    		vm.communicationWorkingOrder.project = null;
		    		vm.communicationWorkingOrder.usersSelected = [];
		    		vm.usersOfProject = [];
		    		project.selected = false;
		    		vm.selectedFilterProject = null;
		    	}
		    	
		    	function userStillSelected(user) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.usersSelected.length; i++) {
		    			if(user != null && vm.communicationWorkingOrder.usersSelected[i].id == user.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function removeUser(user) {
		    		for(var i = 0; i < vm.communicationWorkingOrder.usersSelected.length; i++) {
		    			if(vm.communicationWorkingOrder.usersSelected[i].id == user.id) {
		    				vm.communicationWorkingOrder.usersSelected.splice(i, 1);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function removeProjectSelection() {
		    		for(var i = 0; i < vm.projects.length; i++) {
		    			vm.projects[i].selected = false;
		    		}
		    	}
		    	
		    	function openEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.executionDateDatePicker = true;
	            }
		    	
		    	function createCommunicationWorkingOrder() {
		    		if(vm.communicationWorkingOrder.usersSelected.length === 0) {
		    			vm.noUsersSelected = true;
		    			return;
		    		}
		    		if(vm.selectedFilterProject == null) {
		    			return;
		    		}
		    		vm.communicationWorkingOrder.project = vm.selectedFilterProject;
		    		var userIds = [];
		    		for(var i = 0; i < vm.communicationWorkingOrder.usersSelected.length; i++) {
		    			userIds.push(vm.communicationWorkingOrder.usersSelected[i].id);
		    		}
		    		vm.communicationWorkingOrder.usersSelected = userIds;
		    		
		    		if(vm.mode == 'CREATE') {
			    		communicationService.createCommunicationUserConnectionMessages(vm.currentUser.id, vm.communicationWorkingOrder).then(function successCallback(response) {
			    			var communicationUserConnections = response.data;
			    			invoker.addCommunicationUserConnectionToSentAndOpenOrders(communicationUserConnections);
			   	   		 }, function errorCallback(response) {
			   	   			  console.log('error communicationWorkingOrderCreation.service#createCommunicationWorkingOrder');
			   	   		 });
		    		} else if(vm.mode == 'EDIT') {
		    			vm.communicationWorkingOrder.communicationUserConnectionId = vm.communicationUserConnection.id;
		    			communicationService.updateCommunicationUserConnectionMessages(vm.communicationWorkingOrder).then(function successCallback(response) {
		    				var communicationUserConnections = response.data;
			    			invoker.updateCommunicationUserConnectionToSentAndOpenOrders(communicationUserConnections);
			   	   		 }, function errorCallback(response) {
			   	   			  console.log('error communicationWorkingOrderCreation.service#updateCommunicationWorkingOrder');
			   	   		 });
		    		}
		    		
		    		closeCommunicationWorkingOrderCreationModal();
		    	}
		    	
		    	function closeCommunicationWorkingOrderCreationModal() {
					$modalInstance.dismiss('cancel');
					}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('messageReceiverOverviewModalService', messageReceiverOverviewModalService);
        
    messageReceiverOverviewModalService.$inject = ['$modal', '$stateParams'];
    
    function messageReceiverOverviewModalService($modal, $stateParams) {
		var service = {
			showMessageReceiverOverviewModal: showMessageReceiverOverviewModal
		};		
		return service;
		
		////////////
				
		function showMessageReceiverOverviewModal(currentUser, communicationUserConnections, confirmationNeeded) {
			 var messageReceiverOverviewModal = $modal.open({
				controller: MessageReceiverOverviewModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	communicationUserConnections: function() {
			    		return communicationUserConnections;
			    	},
			    	confirmationNeeded: function() {
			    		return confirmationNeeded;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/messageReceiverOverview/messageReceiverOverview.modal.html'
			});
			return messageReceiverOverviewModal;
			
			function MessageReceiverOverviewModalController($modalInstance, $scope, currentUser, communicationUserConnections, confirmationNeeded) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.communicationUserConnections = communicationUserConnections;
		    	vm.confirmationNeeded = confirmationNeeded;
		    	
		    	vm.closeMessageReceiverOverviewModal = closeMessageReceiverOverviewModal;
		    	
		    	function closeMessageReceiverOverviewModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('printMessageOrdersModalService', printMessageOrdersModalService);
        
    printMessageOrdersModalService.$inject = ['$modal', '$stateParams', 'dateUtilityService'];
    
    function printMessageOrdersModalService($modal, $stateParams, dateUtilityService) {
		var service = {
			showPrintMessageOrdersModal: showPrintMessageOrdersModal
		};		
		return service;
		
		////////////
				
		function showPrintMessageOrdersModal(workingOrders, mode, currentUser) {
			 var printMessageOrdersModal = $modal.open({
				controller: PrintMessageOrdersModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    size: 'lg',
			    resolve: {
			    	workingOrders: function() {
			    		return workingOrders;
			    	},
			    	mode: function() {
			    		return mode;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/printMessageOrders/printMessageOrders.modal.html'
			});
			return printMessageOrdersModal;
			
			function PrintMessageOrdersModalController($modalInstance, $scope, workingOrders, mode, currentUser) {
		    	var vm = this; 
		    	vm.today = dateUtilityService.formatDateToDateTimeString(new Date());
		    	vm.workingOrders = workingOrders;
		    	vm.mode = mode;
		    	vm.currentUser = currentUser;
		    	vm.orderType = 'executionDate';
		    	
		    	vm.closePrintMessageOrdersModal = closePrintMessageOrdersModal;
		    	
		    	function closePrintMessageOrdersModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoCommunicationModalService', yesNoCommunicationModalService);
        
    yesNoCommunicationModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoCommunicationModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, communicationUserConnection) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	communicationUserConnection: function() {
			    		return communicationUserConnection;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, communicationUserConnection) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.communicationUserConnection = communicationUserConnection;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.archiveCommunicationUserConnection(vm.communicationUserConnection);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('activityService', activityService);
        
    activityService.$inject = ['$http', 'api_config'];
    
    function activityService($http, api_config) {
		var service = {
			getActivitiesOfContactId: getActivitiesOfContactId,
			getActivitiesOfContactIdInRange: getActivitiesOfContactIdInRange,
			update: update,
			deleteActivity: deleteActivity
		};
		
		return service;
		
		////////////
		
		function getActivitiesOfContactId(contactId) {
			return $http.get(api_config.BASE_URL + '/activities/activity/' + contactId + '/contact');
		}
		
		function getActivitiesOfContactIdInRange(contactId, start, end) {
			return $http.get(api_config.BASE_URL + '/activities/activity/' + contactId + '/contact/' + start + '/' + end + '/');
		}
		
		function update(activity) {
			return $http.put(api_config.BASE_URL + '/activities/activity', activity);
		}
		
		function deleteActivity(activity) {
			return $http.delete(api_config.BASE_URL + '/activities/activity/' + activity.id);
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactAndUserService', contactAndUserService);
        
    contactAndUserService.$inject = ['$http', 'api_config'];
    
    function contactAndUserService($http, api_config) {
		var service = {
			findAllContactsAndUsers: findAllContactsAndUsers,
			findContactsAndUsersBySearchString: findContactsAndUsersBySearchString
		};
		
		return service;
		
		////////////
		
		function findAllContactsAndUsers() {
			return $http.get(api_config.BASE_URL + '/contactsandusers/contactanduser/');
		}
		
		function findContactsAndUsersBySearchString(searchString) {
			return $http.get(api_config.BASE_URL + '/contactsandusers/contactanduser/' + searchString + '/search');
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactsService', contactsService);
        
    contactsService.$inject = ['$http', 'api_config', 'Upload'];
    
    function contactsService($http, api_config, Upload) {
		var service = {
			createContact: createContact, 
			updateContact: updateContact,
			importContacts: importContacts,
			countByTenantAndActive: countByTenantAndActive,
			countCustomerContactsByTenantAndActive: countCustomerContactsByTenantAndActive,
			findContactById: findContactById,
			findAllTenantContacts: findAllTenantContacts,
			findContactBySearchString: findContactBySearchString,
			findPagedContacts: findPagedContacts,
			calculateAmountOfPages: calculateAmountOfPages,
			getContactsReferencedToProduct: getContactsReferencedToProduct,
			getContactPageSize: getContactPageSize,
			addProductToContact: addProductToContact,
			removeProductFromContact: removeProductFromContact,
			getContactImportationDateGroup: getContactImportationDateGroup,
			deleteContact: deleteContact,
			deleteImportedContactsOfBulk: deleteImportedContactsOfBulk,
			deleteAllContacts: deleteAllContacts,
			deleteAllCustomerContacts: deleteAllCustomerContacts,
			deactivateContact: deactivateContact
		};
		
		return service;
		
		////////////
		
		function createContact(contact) {
			return $http.post(api_config.BASE_URL + '/contacts/contact', contact);
		}
		
		function updateContact(contact) {
			return $http.put(api_config.BASE_URL + '/contacts/contact', contact);
		}
		
		function importContacts(file, tenant, successCallback, errorCallback, progressCallback) {
			if(file == null) {
				return;
			}
			Upload.upload({
                url: 'api/contacts/contact/import',
                data: {
                  file: file,
                  tenant: tenant
                }
            }).then(function (resp) {
            	if (successCallback) {
                    successCallback(resp);
                }
            }, function (resp) {
                if(errorCallback) {
                	errorCallback(resp);
                }
            }, function (evt) {
                if (progressCallback) {
                    progressCallback(evt);
                }
            });
		}
		
		function countByTenantAndActive(userId) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + userId + '/amount');
		}
		
		function countCustomerContactsByTenantAndActive(userId) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + userId + '/amount/customer');
		}
		
		function findContactById(id) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + id + '/contact');
		}
		
		function findAllTenantContacts(userId) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + userId);
		}
		
		function findContactBySearchString(searchString) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + searchString + '/search');
		}
		
		function findPagedContacts(page, sortingType) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/page/' + page + '/' + sortingType);
		}
		
		function calculateAmountOfPages() {
			return $http.get(api_config.BASE_URL + '/contacts/contact/amountofpages');
		}
		
		function getContactsReferencedToProduct(productId) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + productId + '/contacts');
		}
		
		function getContactPageSize() {
			return $http.get(api_config.BASE_URL + '/contacts/contact/contactpagesize');
		}
		
		function addProductToContact(addressId, productId, contact) {
			return $http.put(api_config.BASE_URL + '/contacts/contact/' + addressId + '/' + productId + '/add', contact);
		}
		
		function removeProductFromContact(addressId, productId, contact) {
			return $http.put(api_config.BASE_URL + '/contacts/contact/' + addressId + '/' + productId + '/remove', contact);
		}
		
		function getContactImportationDateGroup() {
			return $http.get(api_config.BASE_URL + '/contacts/contact/contactimportationdategroup');
		}
		
		function deleteContact(contactId) {
			return $http.delete(api_config.BASE_URL + '/contacts/contact/' + contactId);
		}
		
		function deleteImportedContactsOfBulk(date) {
			return $http.delete(api_config.BASE_URL + '/contacts/contact/' + date + '/importedcontacts');
		}
		
		function deleteAllContacts() {
			return $http.delete(api_config.BASE_URL + '/contacts/contact/all');
		}
		
		function deleteAllCustomerContacts(countryType) {
			return $http.delete(api_config.BASE_URL + '/contacts/contact/all/customers/' + countryType);
		}
		
		function deactivateContact(contactId, state) {
			return $http.patch(api_config.BASE_URL + '/contacts/contact/' + contactId + '/' + state + '/deactivate');
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('facilityDetailsService', facilityDetailsService);
        
    facilityDetailsService.$inject = ['$http', 'api_config'];
    
    function facilityDetailsService($http, api_config) {
		var service = {
			getFacilityDetailsOfContactId: getFacilityDetailsOfContactId,
			getFacilityDetailsOfContactIdInRange: getFacilityDetailsOfContactIdInRange,
			update: update,
			deleteFacilityDetail: deleteFacilityDetail
		};
		
		return service;
		
		////////////
		
		function getFacilityDetailsOfContactId(contactId) {
			return $http.get(api_config.BASE_URL + '/facilitydetails/facilitydetail/' + contactId + '/contact');
		}
		
		function getFacilityDetailsOfContactIdInRange(contactId, start, end) {
			return $http.get(api_config.BASE_URL + '/facilitydetails/facilitydetail/' + contactId + '/contact/' + start + '/' + end + '/');
		}
		
		function update(facilityDetail) {
			return $http.put(api_config.BASE_URL + '/facilitydetails/facilitydetail', facilityDetail);
		}
		
		function deleteFacilityDetail(facilityDetail) {
			return $http.delete(api_config.BASE_URL + '/facilitydetails/facilitydetail/' + facilityDetail.id);
		}
    }
})();
(function() {
    'use strict';
       
    angular
    	.module('legalprojectmanagement.contacts')
    	.controller('ContactsController', ContactsController);
     
    ContactsController.$inject = ['$scope', 'currentUser', 'countryTypes', 'titles', 'provinceTypes', 'contactTypes', 'amountOfContacts', 'contactPageSize', 'amountOfPages', 'userAuthorizationUserContainer', 'roles', 'usersOfProject', 'projectsOfContactModalService', 'contactsService', 'projectUserConnectionService', 'projectsAssignedToContactModalService', 'editContactModalService', 'yesNoContactModalService', 'projectsService', 'documentFileService', 'projectTemplateService', 'printingPDFService', 'productsService', 'facilityDetailsOfContactModalService', 'facilityDetailsService', 'contactActivityModalService', 'activityService', 'dateUtilityService', 'calendarEventUserConnectionService', 'userService', 'usersAssignedToContactModalService', 'optionsService', 'deleteImportedContactsModalService', 'yesNoDeactivateContactModalService'];
     
    function ContactsController($scope, currentUser, countryTypes, titles, provinceTypes, contactTypes, amountOfContacts, contactPageSize, amountOfPages, userAuthorizationUserContainer, roles, usersOfProject, projectsOfContactModalService, contactsService, projectUserConnectionService, projectsAssignedToContactModalService, editContactModalService, yesNoContactModalService, projectsService, documentFileService, projectTemplateService, printingPDFService, productsService, facilityDetailsOfContactModalService, facilityDetailsService, contactActivityModalService, activityService, dateUtilityService, calendarEventUserConnectionService, userService, usersAssignedToContactModalService, optionsService, deleteImportedContactsModalService, yesNoDeactivateContactModalService) {
	    $scope.vm = this;   
    	var vm = this;
    	
    	vm.currentUser = currentUser; 
    	vm.countryTypes = countryTypes.data; 
    	vm.titles = titles.data;
    	vm.amountOfContacts = amountOfContacts.data;
    	vm.provinceTypes = provinceTypes.data;
    	vm.contactTypes = contactTypes.data;
    	vm.contactPageSize = contactPageSize.data;
    	vm.amountOfPages = amountOfPages.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.roles = roles.data;
    	vm.usersOfProject = usersOfProject.data;
    	vm.showContactForm = false; 
    	vm.orderType = 'institution';
    	vm.currentPage = 0;
    	vm.searchContactsTerm = null;
    	vm.searchTrigger = false;
    	vm.loadingContacts = false;
    	vm.searchingContacts = false;
    	vm.importContactsFromExcel = false;
    	vm.selectedTenantForExcelImport = 'EMPTY_COUNTRY';
    	vm.contacts = [];
    	vm.activityTypes = [];
    	vm.facilityDetailsTypes = [];
    	vm.files = [];
    	vm.selectedCountryImport = vm.countryTypes[0];
    	
    	vm.printContainer = printContainer;
    	vm.createContact = createContact;
    	vm.updateContact = updateContact;
    	vm.deleteContact = deleteContact;
    	vm.deactivateContact = deactivateContact;
    	vm.pageChange = pageChange;
    	vm.arrayRang = arrayRang;
    	vm.isCurrentPage = isCurrentPage;
    	vm.findContactBySearchString = findContactBySearchString;
    	vm.removeSearchResults = removeSearchResults;
    	vm.showProductsOfContact = showProductsOfContact;
    	vm.showUsersAssignedToContactModal = showUsersAssignedToContactModal;
    	vm.showProjectsAssignedToContactModal = showProjectsAssignedToContactModal;
    	vm.showEditContactModal = showEditContactModal;
    	vm.showDeleteContactModal = showDeleteContactModal;
    	vm.showContactActivityModal = showContactActivityModal;
    	vm.showFacilityDetailsModal = showFacilityDetailsModal;
    	vm.createDocumentUrl = createDocumentUrl;
    	vm.hideAddressWithProducts = hideAddressWithProducts;
    	vm.searchContacts = searchContacts;
    	vm.showTemplateModal = showTemplateModal;
    	vm.showAccountAddress = showAccountAddress;
    	vm.successCallback = successCallback;
    	vm.errorCallback = errorCallback;
    	vm.progressCallback = progressCallback;
    	vm.showDeleteAllImportedContacts = showDeleteAllImportedContacts;
    	vm.showDeactivateContactModal = showDeactivateContactModal;
    	vm.contactTypesCheck = contactTypesCheck;
    	vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    	createContactVariable();
    	pageChange(0);
    	loadActivityTypes();
    	loadFacilityDetailsTypes();
    	
    	////////////
    	
    	$scope.$watch('vm.orderType', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			pageChange(0);
    		} 
		});
    	
    	$scope.$watch('vm.files', function () {
    		if(vm.files != null && vm.files.length > 0) {
    			contactsService.importContacts(vm.files[0], vm.selectedCountryImport, vm.successCallback, vm.errorCallback, vm.progressCallback);
    		}
        });
    	
    	function successCallback(response) {
    		pageChange(0);
    		vm.importContactsFromExcel = false;
    	}
    	
    	function errorCallback(response) {
    		console.log('error importing excel in contacts.controller#errorCallback: ' + response.data);
    	}

    	function progressCallback(response) {
    		vm.importContactsFromExcel = true;
    	}
    	
    	function showDeleteAllImportedContacts() {
    		contactsService.getContactImportationDateGroup().then(function(response) {
    			var contactImportationDateGroups = response.data;
    			deleteImportedContactsModalService.showDeleteImportedContactsModal(vm, contactImportationDateGroups);
			}).catch(function(data) {
				console.log('error in contacts.controller.js#showDeleteAllImportedContacts: ' + data);
			});
    	}
    	
    	function showAccountAddress(accountAddress) {
    		if(accountAddress == null) {
    			return false;
    		}
    		if((accountAddress.street == null || accountAddress.street.length == 0) && 
    		   (accountAddress.postalCode == null || accountAddress.postalCode.length == 0) &&
    		   (accountAddress.region == null || accountAddress.region.length == 0) &&
    		   (accountAddress.provinceType == null || accountAddress.provinceType.length == 0) &&
    		   (accountAddress.country == null || accountAddress.country.length == 0) &&
    		   (accountAddress.email == null || accountAddress.email.length == 0)) {
    			return false;
    		}
    		return true;
    	}
    	
    	function searchContacts() {
			if(vm.searchContactsTerm != null && vm.searchContactsTerm.length > 3) {
				vm.searchingContacts = true;
				findContactBySearchString(vm.searchContactsTerm);
				vm.searchTrigger = true;
				vm.searchingContacts = false;
			}
		}
    	
    	function hideAddressWithProducts(addressWithProducts) {
			var address = addressWithProducts.address;
			if(address == null) {
				return false;
			}
			if(address.userIdBlackList != null && address.userIdBlackList.indexOf(vm.currentUser.id) !== -1) {
				return true;
			}
			if(address.rolesBlackList != null && address.rolesBlackList.indexOf(vm.currentUser.projectUserConnectionRole) !== -1) {
				return true;
			}
			return false;
		}
    	
    	function removeSearchResults() {
    		vm.searchContactsTerm = null;
    		vm.searchTrigger = false;
    		pageChange(0);
    	}
    	
    	function loadAmountOfContacts() {
    		contactsService.countByTenantAndActive(vm.currentUser.id).then(function(response) {
    			vm.amountOfContacts = response.data;
			}).catch(function (data) {
				console.log('error in contacts.controller.js#loadAmountOfContacts: ' + data);
			});
    	}
    	
    	function createContactVariable() {
    		vm.contact = {
    			institution: '',
    			contactTenant: 'EMPTY_COUNTRY',
    			contactPerson: '',
    			customerNumberContainers: [],
    			deliveryNumberContainers: [],
    			contactTypes: [],
    			address: {
					street: '',
					postalCode: '',
					region: '',
					provinceType: 'EMPTY_PROVINCE',
					country: 'EMPTY_COUNTRY'
					},
				accountAddress: null,
				addressesWithProducts: [],
				emails: [''],
				telephones: [''],
				homepage: '',
				products: [],
				contactPersons: [],
				contactAttachments: []
	    		};
    	}
    	
    	function findContactBySearchString(searchString) {
    		vm.loadingContacts = true;
    		contactsService.findContactBySearchString(searchString).then(function (response) {
    			vm.contacts = response.data;
    			vm.loadingContacts = false;
			}).catch(function (data) {
				console.log('error in contacts.controller.js#findContactBySearchString: ' + data);
			});
    	}
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}

    	function createContact(contact) {
    		vm.createContactClicked = true;
    		contactsService.createContact(contact).then(function (response) {
    			var createdContact = response.data;
    			createdContact.stillCreated = true;
    			createContactVariable();
    			loadAmountOfContacts();
    			vm.contacts.splice(0, 0, createdContact);
			}).catch(function (data) {
				console.log('error in contacts.controller.js#createContact: ' + data);
			});
    	}
    	
    	function updateContact(contact) {
    		contact.importConfirmed = true;
    		contactsService.updateContact(contact).then(function(response) {
    			updateContactFromView(response.data);
			}).catch(function (data) {
				console.log('error in contacts.controller.js#updateContact: ' + data);
			});
    	}
    	
    	function deleteContact(contact) {
    		contactsService.deleteContact(contact.id).then(function (response) {
    			deleteContactFromView(contact);
    			loadAmountOfContacts();
			}).catch(function (data) {
				console.log('error in contacts.controller.js#deleteContact: ' + data);
			});
    	}
    	
    	function updateContactFromView(contact) {
    		for(var i = 0; i < vm.contacts.length; i++) {
    			if(vm.contacts[i].id == contact.id) {
    				vm.contacts[i] = contact;
    				break;
    			}
    		}
    	}
    	
    	function deleteContactFromView(contact) {
    		for(var i = 0; i < vm.contacts.length; i++) {
    			if(vm.contacts[i].id == contact.id) {
    				vm.contacts.splice(i, 1);
    				break;
    			}
    		}
    	}
    	
    	function showEditContactModal(contact) {
    		editContactModalService.showEditContactModal(contact, vm.titles, vm.provinceTypes, vm.contactTypes, vm.countryTypes, vm, vm.currentUser, vm.roles, vm.usersOfProject);
    	}
    	
    	function showDeleteContactModal(contact) {
    		productsService.getProductsReferencedToContact(contact.id).then(function(response) {
    			var productsReferencedToContact = response.data;
    			calendarEventUserConnectionService.findCalendarEventUserConnectionsByContact(contact.id).then(function(response) {
    				var calendarEventUserConnections = response.data;
    				yesNoContactModalService.showYesNoModal(vm, contact, productsReferencedToContact, calendarEventUserConnections);
    			 }, function errorCallback(response) {
    	   			  console.log('error contacts.controller.js#showDeleteContactModal#findCalendarEventUserConnectionsByContact');
     	   		 });
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#showDeleteContactModal#showDeleteContactModal');
  	   		 });
    	}
    	
    	function showDeactivateContactModal(contact, state) {
    		yesNoDeactivateContactModalService.showYesNoModal(vm, contact, state);
    	}
    	
    	function deactivateContact(contact, state) {
    		contactsService.deactivateContact(contact.id, state).then(function (response) {
    			if(state == false) {
    				deleteContactFromView(contact);
    			} else {
    				contact.active = true;
    			}
    			loadAmountOfContacts();
			}).catch(function (data) {
				console.log('error in contacts.controller.js#deactivateContact: ' + data);
			});
    	}
    	
    	function showProductsOfContact(contact) {
    		contactsService.findContactById(contact.id).then(function(response) {
    			var contactWithProducts = response.data;
    			projectsOfContactModalService.showProductsOfContactModal(contactWithProducts, vm.currentUser, vm.userAuthorizationUserContainer);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#showProductsOfContact');
  	   		 });
    	}
    	
    	function showProjectsAssignedToContactModal(contact) {
    		projectsService.findProjectsOfUser(currentUser.id).then(function (projectsResponse) {
    			var projects = projectsResponse.data;
    			projectUserConnectionService.findProjectUserConnectionsByContactAndProject(contact.id).then(function (projectUserConnectionResponse) {
        			var projectUserConnections = projectUserConnectionResponse.data;
        			projectsAssignedToContactModalService.showProjectsAssignedToContactModal(contact, projects, projectUserConnections);
    			}).catch(function (data) {
    				console.log('error in contacts.controller.js#findProjectUserConnectionsByContact: ' + data);
    			});
			}).catch(function (data) {
				console.log('error in contacts.controller.js#showProjectsAssignedToContactModal: ' + data);
			});
    	}
    	
    	function showUsersAssignedToContactModal(contact) {
    		userService.findAllUsers().then(function(projectsResponse) {
    			var users = projectsResponse.data;
    			projectUserConnectionService.findProjectUserConnectionsByContactAndUser(contact.id).then(function (projectUserConnectionResponse) {
        			var projectUserConnections = projectUserConnectionResponse.data;
        			usersAssignedToContactModalService.showUsersAssignedToContactModal(contact, users, projectUserConnections);
    			}).catch(function (data) {
    				console.log('error in contacts.controller.js#findProjectsOfUser: ' + data);
    			});
			}).catch(function (data) {
				console.log('error in contacts.controller.js#showProjectsAssignedToContactModal: ' + data);
			});
    	}
    	
    	function showContactActivityModal(contact) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		activityService.getActivitiesOfContactIdInRange(contact.id, startDateString, endDateString).then(function(response) {
    			var activities = response.data;
    			contactActivityModalService.showContactActivityModal(contact, activities, start, end, vm.activityTypes, vm.userAuthorizationUserContainer);
    		}, function errorCallback(response) {
	   			  console.log('error contacts.controller.js#showContactActivityModal');
	   		 });
    	}
    	
    	function showFacilityDetailsModal(contact) {  
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		facilityDetailsService.getFacilityDetailsOfContactIdInRange(contact.id, startDateString, endDateString).then(function(response) {
    			var facilityDetails = response.data;
    			facilityDetailsOfContactModalService.showFacilityDetailsOfContactModal(contact, facilityDetails, start, end, vm.activityTypes, vm.facilityDetailsTypes, vm.userAuthorizationUserContainer);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#showFacilityDetailsModal');
  	   		 });
    	}
    	
    	function pageChange(page) {
    		contactsService.calculateAmountOfPages().then(function(response) {
    			vm.amountOfPages = response.data;
    			vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    			if(page < 0 || (page > 0 && page > vm.amountOfPagesArray.length-1 )) {
        			return;
        		}
        		vm.loadingContacts = true;
        		contactsService.findPagedContacts(page, vm.orderType).then(function(response) {
        			vm.contacts = response.data;
        			vm.currentPage = page;
        			vm.loadingContacts = false;
        			loadAmountOfContacts();
      	   		 }, function errorCallback(response) {
      	   			  console.log('error contacts.controller.js#pageChange');
      	   		 });
			}).catch(function (data) {
				console.log('error in contacts.controller.js#pageChange: ' + data);
			});
    	}
    	
    	function isCurrentPage(indexHistory) {
    		if(indexHistory == (vm.currentPage+1)) {
    			return true;
    		}
    		return false; 
    	}
    	
    	function arrayRang(min, max, step) {
    	    step = step || 1;
    	    var input = [];
    	    for (var i = min; i <= max; i += step) {
    	        input.push(i);
    	    }
    	    return input;
    	}
    	
    	function createDocumentUrl(user, product, receipeAttachment) {	
    		if(receipeAttachment == null) {
    			return;
    		}
    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
    		return 'api/filedownloads/filedownload/' + currentUser.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
    	}
    	
    	function showTemplateModal(url, receipeAttachment) {
    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
    			var documentFile = response.data;
    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
  	   		 }, function errorCallback(response) {
  	   			 console.log('error contacts.controller#showTemplateModal'); 
  	   		 });
    	}
    	
    	function loadActivityTypes() {     
    		optionsService.getActivityTypes('all').then(function(response) {
    			vm.activityTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#loadActivityTypes');
  	   		 });
    	}
    	
    	function loadFacilityDetailsTypes() {
    		optionsService.getFacilityDetailsTypes().then(function(response) {
    			vm.facilityDetailsTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error contacts.controller.js#loadFacilityDetailsTypes');
  	   		 });
    	}
    	
    	function contactTypesCheck(contact) {
    		if(contact.contactTypes == null || contact.contactTypes.length == 0) {
    			return true;
    		}
    		for(var i = 0; i < contact.contactTypes.length; i++) {
    			if(contact.contactTypes[i] == 'POTENTIAL_BUYER') {
    				return false;
    			}
    		}
    		return true;
    	}
	} 
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.contacts')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getContactsState());
    	 
    	////////////
			    	
    	function getContactsState() {
    		var state = {
    			name: 'auth.contacts',
				url: '/contacts/:userId',
				templateUrl: 'app/contacts/contacts/contacts.html',
				controller: 'ContactsController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					optionsService: 'optionsService',
					contactsService: 'contactsService',
					countryTypes: function getCountryTypes(optionsService) {
						return optionsService.getCountryTypes();
					},
					titles: function getAllTitles(optionsService) {
						return optionsService.getAllTitles();
					},
					provinceTypes: function getProvinceTypes(optionsService) {
						return optionsService.getProvinceTypes();
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					contactTypes: function($stateParams, optionsService) {
						return optionsService.getContactTypes();
					},
					contactPageSize: function getContactPageSize(contactsService) {
						return contactsService.getContactPageSize();
					},
					amountOfPages: function calculateAmountOfPages(contactsService) {
						return contactsService.calculateAmountOfPages(); 
					},
					amountOfContacts: function($stateParams, contactsService) {
						return contactsService.countByTenantAndActive($stateParams.userId);
					},
					usersOfProject: function findProjectUserConnectionByProject(userService) {
						return userService.findAllUsers();
					},
					roles: function(optionsService) {
						return optionsService.getRoles();
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.directive('contactActivityDirective', contactActivityDirective);
    
    contactActivityDirective.$inject = ['$timeout', 'dateUtilityService', 'yesNoActivityContactModalService', 'contactActivityCreateEditModalService'];
    
	function contactActivityDirective($timeout, dateUtilityService, yesNoActivityContactModalService, contactActivityCreateEditModalService) {
		var directive = {
			restrict: 'E',    
			scope: {
				invoker: '=',
				calendarEvent: '=',
				activity: '=',
				activityTypes: '=',
				user: '=',
				contact: '=',
				userAuthorizationUserContainer: '=',
				type: '='
			},
			templateUrl: 'app/contacts/directives/contactActivityDirective/contactActivity.html',
			link: function($scope) {
				
				$scope.formatStartEndDateTime = function(start, end) {
					var startDate = new Date(start);
		    		var formattedStartDate = dateUtilityService.formatDateToDateTimeString(startDate);
		    		var endDate = new Date(end);
		    		var formattedEndDate = dateUtilityService.formatDateToDateTimeString(endDate);
		    		return dateUtilityService.formatStartEndDateTime(formattedStartDate, formattedEndDate);
				};
				
				$scope.showRemoveActivity = function() {
					yesNoActivityContactModalService.showYesNoModal($scope.invoker, $scope.contact, $scope.activity);
				};
				
				$scope.showContactActivityCreateEditModal = function(activity) {
					contactActivityCreateEditModalService.showContactActivityCreateEditModal($scope.contact, $scope.activity, $scope.activityTypes);
				};
				
				$scope.getFileNameWithoutExtension = function(activityDocumentFile) {
					var fileName = activityDocumentFile.fileName;
					var fileNameWithoutEnding = activityDocumentFile.fileName;
					var fileNameEnding = fileName.substr(fileName.lastIndexOf('.')+1, fileName.length);
			        if(fileNameEnding.length === 3) {
			            fileNameWithoutEnding = fileName.substr(0, fileName.lastIndexOf('.'));
			            return fileNameWithoutEnding;
			        } else {
			        	return fileName;
			        }
				};
				
				$scope.getFileNameExtension = function(activityDocumentFile) {
					var fileName = activityDocumentFile.fileName;
					var fileNameEnding = fileName.substr(fileName.lastIndexOf('.')+1, fileName.length);
			        if(fileNameEnding.length === 3) {
			            return fileNameEnding;
			        } else {
			        	return 'NO_ENDING';
			        }
				};
			 }
		};
		return directive;
		
		////////////
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.directive('contactFacilityDetailsDirective', contactFacilityDetailsDirective);
    
    contactFacilityDetailsDirective.$inject = ['$timeout', 'dateUtilityService', 'yesNoFacilityDetailsContactModalService', 'contactFacilityDetailCreateEditModalService'];
    
	function contactFacilityDetailsDirective($timeout, dateUtilityService, yesNoFacilityDetailsContactModalService, contactFacilityDetailCreateEditModalService) {
		var directive = {
			restrict: 'E',    
			scope: {
				invoker: '=',
				calendarEvent: '=',
				facilityDetail: '=',
				activityTypes: '=',
				facilityDetailsTypes: '=',
				user: '=',
				contact: '=',
				userAuthorizationUserContainer: '=',
				type: '='
			},
			templateUrl: 'app/contacts/directives/contactFacilityDetailsDirective/contactFacilityDetails.html',
			link: function($scope) {
				
				$scope.formatStartEndDateTime = function(start, end) {
					var startDate = new Date(start);
		    		var formattedStartDate = dateUtilityService.formatDateToDateTimeString(startDate);
		    		var endDate = new Date(end);
		    		var formattedEndDate = dateUtilityService.formatDateToDateTimeString(endDate);
		    		return dateUtilityService.formatStartEndDateTime(formattedStartDate, formattedEndDate);
				};
				
				$scope.showRemoveFacilityDetail = function() {
					yesNoFacilityDetailsContactModalService.showYesNoModal($scope.invoker, $scope.contact, $scope.facilityDetail);
				};
				
				$scope.showContactFacilityDetailCreateEditModal = function(activity) {                          
					contactFacilityDetailCreateEditModalService.showContactFacilityDetailCreateEditModal($scope.contact, $scope.facilityDetail, $scope.activityTypes, $scope.facilityDetailsTypes);
				};
			 }
		};
		return directive;
		
		////////////
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.directive('createEditContactDirective', createEditContactDirective);
    
    createEditContactDirective.$inject = ['$timeout', 'dateUtilityService', 'documentFileService', 'utilService', 'projectTemplateService', 'productsService', 'optionsService'];
    
	function createEditContactDirective($timeout, dateUtilityService, documentFileService, utilService, projectTemplateService, productsService, optionsService) {
		var directive = {
			restrict: 'E',
			scope: {
				contact: '=',
				titles: '=',
				provinceTypes: '=',
				contactTypes: '=',
				countryTypes: '=',
				invoker: '=',
				modalInvoker: '=',
				type: '=',
				currentUser: '=',
				roles: '=',
				usersOfProject: '=',
				disabled: '='
			},
			templateUrl: 'app/contacts/directives/createEditContactDirective/createEditContact.html',
			link: function($scope) {
				    
				$scope.customerSinceDateDatePicker = false;
				$scope.importationDateDatePicker = false;
				$scope.developmentDateDatePicker = false;
				$scope.provinceTypesOfCountry = null;
				$scope.provinceTypesOfAccountCountry = null;
				if($scope.contact != null) {
					$scope.contact.creationDate = $scope.contact.creationDate != null ? dateUtilityService.convertToDateOrUndefined($scope.contact.creationDate) : null;
					$scope.contact.importationDate = $scope.contact.importationDate != null ? dateUtilityService.convertToDateOrUndefined($scope.contact.importationDate) : null;
					$scope.contact.customerSince = $scope.contact.customerSince != null ? dateUtilityService.convertToDateOrUndefined($scope.contact.customerSince) : null;
				}
				
				if($scope.contact != null && $scope.contact.emails == null) {
					$scope.contact.emails = [];
					$scope.contact.emails.push('');
				}
				
				if($scope.contact != null && $scope.contact.emails.length == 0) {
					$scope.contact.emails.push('');
				}
				
				if($scope.contact != null && $scope.contact.telephones == null) {
					$scope.contact.telephones = [];
					$scope.contact.telephones.push('');
				}
				
				if($scope.contact != null && $scope.contact.telephones.length == 0) {
					$scope.contact.telephones.push('');
				}
				
				$scope.openDevelopmentDateDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.developmentDateDatePicker = true;
				};
				   
				$scope.openImportationDateDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.importationDateDatePicker = true;
				};
				
				$scope.openCustomerSinceDateDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.customerSinceDateDatePicker = true;
				};
				
				$scope.initProvinceTypes = function() {
					if($scope.type == 'EDIT' || $scope.type == 'SHOW') {
						if($scope.contact != null && $scope.contact.address != null && $scope.contact.address.country != null) {
							optionsService.getProvinceTypesOfCountryType($scope.contact.address.country).then(function (response) {
								$scope.provinceTypesOfCountry = response.data;
							}).catch(function (data) {
								console.log('error in createEditContact.directive#initProvinceTypes: ' + data);
							});
						}
						if($scope.contact != null && $scope.contact.accountAddress != null && $scope.contact.accountAddress.country != null) {
							optionsService.getProvinceTypesOfCountryType($scope.contact.accountAddress.country).then(function (response) {
								$scope.provinceTypesOfAccountCountry = response.data;
							}).catch(function (data) {
								console.log('error in createEditContact.directive#initProvinceTypes: ' + data);
							});
						}
						
						if($scope.contact != null && $scope.contact.addressesWithProducts != null) {
							for(var i = 0; i < $scope.contact.addressesWithProducts.length; i++) {
								var addressWithProducts = $scope.contact.addressesWithProducts[i];
								if(addressWithProducts != null && addressWithProducts.address != null && addressWithProducts.address.country != null) {
									optionsService.getProvinceTypesOfCountryType(addressWithProducts.address.country).then(function (response) {
										for(var j = 0; j < $scope.contact.addressesWithProducts.length; j++) {
											var provinceTypes = response.data;
											var provinceType = $scope.contact.addressesWithProducts[j].address.provinceType;
											for(var k = 0; k < provinceTypes.length; k++) {
												if(provinceTypes[k] == provinceType) {
													$scope.contact.addressesWithProducts[j].addressLocationCountries = response.data;
													break;
												}
											}
										}
									}).catch(function (data) {
										console.log('error in createEditContact.directive#initProvinceTypes: ' + data);
									});
								}
							}
						}
					}
				};
				$scope.initProvinceTypes();
				
				$scope.setAddressLocationCountries = function() {
					if($scope.type == 'EDIT') {
						if($scope.contact.addressesWithProducts != null) {
							for(var i = 0; i < $scope.contact.addressesWithProducts.length; i++) {
								$scope.contact.addressesWithProducts[i].addressLocationCountries = $scope.provinceTypes;
							}
						}
					}
				};
				$scope.setAddressLocationCountries();
				
				
				$scope.addAddressWithProducts = function() {
					var addressWithProduct = {
							address: {
								additionalInformation: '',
								street: '',
								postalCode: '',  
								region: '',
								email: '',
								telefone: '',
								provinceType: 'EMPTY_PROVINCE',
								country: 'EMPTY_COUNTRY'
								},
							products: []
							};
					if($scope.contact.addressesWithProducts == null) {
						$scope.contact.addressesWithProducts = [];
					}
					$scope.contact.addressesWithProducts.push(addressWithProduct); 
				};
				
				$scope.addAccountAddress = function() {
					var accountAddress = {
							street: '',
							postalCode: '', 
							region: '',
							provinceType: 'EMPTY_PROVINCE',
							country: 'EMPTY_COUNTRY'
							};
					$scope.contact.accountAddress = accountAddress; 
				};
				
				$scope.changedCountry = function(countryType) {
					optionsService.getProvinceTypesOfCountryType(countryType).then(function (response) {
						$scope.provinceTypesOfCountry = response.data;
					}).catch(function (data) {
						console.log('error in createEditContact.directive#getProvinceTypesOfCountryType: ' + data);
					});
				};
				
				$scope.changedAccountCountry = function(countryType) {
					optionsService.getProvinceTypesOfCountryType(countryType).then(function (response) {
						$scope.provinceTypesOfAccountCountry = response.data;
					}).catch(function (data) {
						console.log('error in createEditContact.directive#changedAccountCountry: ' + data);
					});
				};
				
				$scope.changedAddressLocationCountry = function(addressWithProducts, countryType) {
					optionsService.getProvinceTypesOfCountryType(countryType).then(function (response) {
						addressWithProducts.addressLocationCountries = response.data;
					}).catch(function (data) {
						console.log('error in createEditContact.directive#changedAddressLocationCountry: ' + data);
					});
				};
				
				$scope.removeContactLocation = function(index) {
	    			$scope.contact.addressesWithProducts.splice(index, 1);
				};
				
				$scope.addContactPerson = function() {
					var contactPerson = {
					    title: '',
					    firstname: '',
					    surname: '',
					    department: '',
					    emails: [''],
					    telephones: ['']
						};
					if($scope.contact.contactPersons == null) {
						$scope.contact.contactPersons = [];
					}
					$scope.contact.contactPersons.push(contactPerson);
				};
				
				$scope.addContactPersonToAddressesWithProducts = function(addressesWithProducts) {
					var contactPerson = {
					    title: '',
					    firstname: '',
					    surname: '',
					    department: '',
					    emails: [''],
					    telephones: ['']
						};
					if(addressesWithProducts.contactPersons == null) {
						addressesWithProducts.contactPersons = [];
					}
					addressesWithProducts.contactPersons.push(contactPerson);
				};
				
				$scope.removeContactPerson = function(index) {
		    		$scope.contact.contactPersons.splice(index, 1);
				};
				
				$scope.addContactPersonEmail = function(contactPersonIndex) {
					$scope.contact.contactPersons[contactPersonIndex].emails.push('');
				};
				
				$scope.addContactPersonAddressesWithProductsEmail = function(contactPerson) {
					contactPerson.emails.push('');
				};
				
				$scope.removeContactPersonEmail = function(contactPersonIndex, index) {
		    		if($scope.contact.contactPersons[contactPersonIndex].emails.length > 1) {
		    			$scope.contact.contactPersons[contactPersonIndex].emails.splice(index, 1);
		    		}
		    	};
		    	
		    	$scope.removeContactPersonAddressesWithProductsEmail = function(addressesWithProducts, parentParentIndex, parentIndex, index) {
		    		addressesWithProducts[parentParentIndex].contactPersons[parentIndex].emails.splice(index, 1);
		    	};
		    	
		    	$scope.addContactPersonTelephone = function(contactPersonIndex) {
					$scope.contact.contactPersons[contactPersonIndex].telephones.push('');
				};  
				
				$scope.removeContactPersonAddressesWithProductsTelephone = function(addressesWithProducts, parentParentIndex, parentIndex, index) {
		    		addressesWithProducts[parentParentIndex].contactPersons[parentIndex].telephones.splice(index, 1);
		    	};
				
				$scope.addContactPersonAddressesWithProductsTelephone = function(contactPerson) {
					contactPerson.telephones.push('');
				};
				
				$scope.removeContactPersonTelephone = function(contactPersonIndex, index) {
		    		if($scope.contact.contactPersons[contactPersonIndex].telephones.length > 1) {
		    			$scope.contact.contactPersons[contactPersonIndex].telephones.splice(index, 1);
		    		}
		    	};
		    	
		    	$scope.addCustomerNumberContainer = function() {
		    		var customerNumberContainer = {};
		    		customerNumberContainer.contactTenant = 'EMPTY_COUNTRY';
		    		customerNumberContainer.customerNumber = '';
		    		if($scope.contact.customerNumberContainers == null) {
		    			$scope.contact.customerNumberContainers = [];
		    		}
		    		$scope.contact.customerNumberContainers.push(customerNumberContainer);
		    	};
		    	
		    	$scope.removeCustomerNumberContainer = function(index) {
		    		$scope.contact.customerNumberContainers.splice(index, 1);
		    	};
		    	
		    	$scope.addDeliveryNumberContainer = function() {
		    		var deliveryNumberContainer = {};
		    		deliveryNumberContainer.contactTenant = 'EMPTY_COUNTRY';
		    		deliveryNumberContainer.deliveryNumber = '';
		    		if($scope.contact.deliveryNumberContainers == null) {
		    			$scope.contact.deliveryNumberContainers = [];
		    		}
		    		$scope.contact.deliveryNumberContainers.push(deliveryNumberContainer);
		    	};
		    	
		    	$scope.removeDeliveryNumberContainer = function(index) {
		    		$scope.contact.deliveryNumberContainers.splice(index, 1);
		    	};
				
				$scope.addContactEmail = function() {
					$scope.contact.emails.push('');
				};
				
				$scope.removeContactEmail = function(index) {
		    		if($scope.contact.emails.length > 1) {
		    			$scope.contact.emails.splice(index, 1);
		    		}
		    	};
				
				$scope.addContactTelephone = function() {
					$scope.contact.telephones.push('');
				};
				
				$scope.removeContactTelephone = function(index) {
		    		if($scope.contact.telephones.length > 1) {
		    			$scope.contact.telephones.splice(index, 1);
		    		}
		    	};
				
				$scope.createContact = function(contact) {
					if($scope.type == 'CREATE') {
						$scope.createContactClicked = true;
						$scope.setContactCreatedTimeout();
						$scope.removeProductObjectsFromContact(contact);
						$scope.invoker.createContact(contact);
					} else if($scope.type == 'EDIT') {
						$scope.removeProductObjectsFromContact(contact);
						$scope.invoker.updateContact(contact);
						$scope.modalInvoker.closeEditContactModal();
					}
				};
				
				$scope.removeProductObjectsFromContact = function(contact) {
					contact.products = null;
					if(contact.addressesWithProducts != null) {
						for(var i = 0; i < contact.addressesWithProducts.length; i++) {
							contact.addressesWithProducts[i].products = [];
						}
					}
				};
				
				$scope.setContactCreatedTimeout = function() {
		      		$timeout(function() {
		      			$scope.createContactClicked = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.isContactTypeSelected = function(contact, contactType) {
		      		if(contact != null && contact.contactTypes == null) {
		      			return false;
		      		}
		      		if(contact != null) {
						for(var i = 0; i < contact.contactTypes.length; i++) {
							if(contact.contactTypes[i] == contactType) {
								return true;
							}
						}
		      		}
					return false;
				};
				
				$scope.addContactType = function(contact, contactType) {
					var contactTypeStillExists = false;
					if(contact != null && contact.contactTypes == null) {
						contact.contactTypes = [];
					}
					if(contact != null) {
						for(var i = 0; i < contact.contactTypes.length; i++) {
							if(contact.contactTypes[i] == contactType) {
								contactTypeStillExists = true;
								break;
							}
						}
					}
					if(!contactTypeStillExists) {
						contact.contactTypes.push(contactType);
					}
				};
				
				$scope.removeContactType = function(contact, contactType) {
					if(contact != null) {
						for(var i = 0; i < contact.contactTypes.length; i++) {
							if(contact.contactTypes[i] == contactType) {
								contact.contactTypes.splice(i, 1);
								break;
							}
						}
					}
				};
				
				$scope.findDocumentsByTerm = function(term) {
					return documentFileService.findDocumentFilesBySearchStringOverAllProjectsAndProducts(term).then(function(response) {
						var documentFiles = response.data;
						for(var i = 0; i < documentFiles.length; i++) {
							documentFiles[i].vm = $scope;
						}
			    		return documentFiles;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createEditContact.directive#findDocumentsByTerm'); 
		  	   		 });
				};
				
				$scope.attachFile = function(selected, selectedDocumentFile, documentFileVersion) {
		    		if(!selected) {
		    			for(var i = 0; i < $scope.contact.contactAttachments.length; i++) {
		    				if($scope.contact.contactAttachments[i].filename == selectedDocumentFile.fileName && $scope.contact.contactAttachments[i].version == documentFileVersion.version) {
		    					$scope.contact.contactAttachments.splice(i, 1);
		    					break;
		    				}
		    			}
		    		} else {
		    			var contactAttachment = {};
		    			contactAttachment.documentFileId = selectedDocumentFile.id;
		    			contactAttachment.filepath = selectedDocumentFile.subFilePath + '/' + selectedDocumentFile.documentFileVersions[0].filePathName;
		    			contactAttachment.filename = selectedDocumentFile.fileName;
		    			contactAttachment.ending = selectedDocumentFile.ending; 
		    			contactAttachment.version = documentFileVersion.version;
		    			contactAttachment.originalFilename = selectedDocumentFile.fileName;
		    			contactAttachment.isImage = utilService.isImage(selectedDocumentFile.ending);
		    			if($scope.contact.contactAttachments == null) {
		    				$scope.contact.contactAttachments = [];
		    			}
		    			$scope.contact.contactAttachments.push(contactAttachment);
		    		}
		    	};
		    	
		    	$scope.removeFile = function(index) {
		    		 $scope.contact.contactAttachments.splice(index, 1);
		    	};
		    	
		    	$scope.createDocumentUrl = function(user, receipeAttachment) {		    	
		    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
		    		return 'api/filedownloads/filedownload/' + $scope.currentUser.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
		    	};
		    	
		    	$scope.showTemplateModal = function(url, receipeAttachment) {
		    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
		    			var documentFile = response.data;
		    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
		    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createEditContact.directive#showTemplateModal'); 
		  	   		 });
		    	};
		    	
		    	$scope.getProductsByTerm = function(term) {
		    		return productsService.findProductsByTerm(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createEditContact.directive#getProductsByTerm'); 
		  	   		 });
		    	};
		    	
		    	$scope.productSelected = function(selectedProduct) {
		    		if($scope.contact.products == null) {
		    			$scope.contact.products = [];
		    		}
		    		for(var i = 0; i < $scope.contact.products.length; i++) {
		    			if($scope.contact.products[i].id == selectedProduct.id) { 
		    				$scope.productStillAdded = true;
		    				$scope.setTimeout();
		    				return;
		    			}
		    		}
		    		if($scope.contact.productIds == null) {
		    			$scope.contact.productIds = [];
		    		}
		    		$scope.contact.productIds.push(selectedProduct.id);
		    		$scope.contact.products.push(selectedProduct);
		    	};
		    	
		    	$scope.productSelectedAtLocation = function(selectedProduct, addressWithProducts) {
		    		if(addressWithProducts.products == null) {
		    			addressWithProducts.products = [];
		    		}
		    		for(var i = 0; i < addressWithProducts.products.length; i++) {
		    			if(addressWithProducts.products[i].id == selectedProduct.id) { 
		    				addressWithProducts.productStillAdded = true;
		    				$scope.setAddressWithProductsTimeout(addressWithProducts);
		    				return;
		    			}
		    		}
		    		if(addressWithProducts.productIds == null) {
		    			addressWithProducts.productIds = [];
		    		}
		    		addressWithProducts.productIds.push(selectedProduct.id);
		    		addressWithProducts.products.push(selectedProduct);
		    	};
		    	
		    	$scope.removeProduct = function(index) {
		    		var product = $scope.contact.products[index];
		    		for(var i = 0; i < $scope.contact.productIds.length; i++) {
		    			if($scope.contact.productIds[i] == product.id) {
		    				$scope.contact.productIds.splice(i, 1);
		    				break;
		    			}
		    		}
		    		$scope.contact.products.splice(index, 1);
		    	};
		    	
		    	$scope.removeProductAtLocation = function(index, addressWithProducts) {
		    		var product = addressWithProducts.products[index];
		    		for(var i = 0; i < addressWithProducts.productIds.length; i++) {
		    			if(addressWithProducts.productIds[i] == product.id) {
		    				addressWithProducts.productIds.splice(i, 1);
		    				break;
		    			}
		    		}
		    		addressWithProducts.products.splice(index, 1);
		    	};
		    	
		    	$scope.setTimeout = function() { 
		      		$timeout(function() { 
		      			$scope.productStillAdded = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.setAddressWithProductsTimeout = function(addressWithProducts) { 
		      		$timeout(function() {
		      			addressWithProducts.productStillAdded = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.hideAddressWithProducts = function(addressWithProducts) {
					var address = addressWithProducts.address;
					if(address.userIdBlackList != null && address.userIdBlackList.indexOf($scope.currentUser.id) !== -1) {
						return true;
					}
					if(address.rolesBlackList != null && address.rolesBlackList.indexOf($scope.currentUser.projectUserConnectionRole) !== -1) {
						return true;
					}
					return false;
				};
		      	
		      	$scope.isRoleBlackListed = function(role, address) {
					if(address == null || address.rolesBlackList == null) {
						return false;
					}
					for(var i = 0; i < address.rolesBlackList.length; i++) {
						if(address.rolesBlackList[i] == role) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addBlackListRole = function(address, role) {
					if(address.rolesBlackList == null) {
						address.rolesBlackList = [];
					}
					address.rolesBlackList.push(role);
				};
				
				$scope.removeBlackListRole = function(address, role) {
					for(var i = 0; i < address.rolesBlackList.length; i++) {
						if(address.rolesBlackList[i] == role) {
							address.rolesBlackList.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.isBlackListed = function(user, address) {
					if(address.userIdBlackList == null) {
						return false;
					}
					for(var i = 0; i < address.userIdBlackList.length; i++) {
						if(address.userIdBlackList[i] == user.id) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addBlackListId = function(address, user) {
					if(address.userIdBlackList == null) {
						address.userIdBlackList = [];
					}
					address.userIdBlackList.push(user.id);
				};
				
				$scope.removeBlackListId = function(address, user) {
					for(var i = 0; i < address.userIdBlackList.length; i++) {
						if(address.userIdBlackList[i] == user.id) {
							address.userIdBlackList.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.addAgentNumberContainer = function(contact) {
		    		if($scope.contact.agentNumberContainers == null) {
		    			contact.agentNumberContainers = [];
		    		}
		    		var agentNumberContainer = {};
		    		agentNumberContainer.customerNumber = '';
		    		contact.agentNumberContainers.push(agentNumberContainer);
		    	};
		    	
				$scope.removeAgentNumberContainer = function(contact, index) {
					contact.agentNumberContainers.splice(index, 1);
		    	};
		    	
		    	$scope.contactTypesCheck = function(contact) {
		    		if(contact.contactTypes == null || contact.contactTypes.length == 0) {
		    			return true;
		    		}
		    		for(var i = 0; i < contact.contactTypes.length; i++) {
		    			if(contact.contactTypes[i] == 'POTENTIAL_BUYER') {
		    				return false;
		    			}
		    		}
		    		return true;
		    	};
			 }
		};
		return directive;
		
		////////////
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactActivityModalService', contactActivityModalService);
        
    contactActivityModalService.$inject = ['$modal', '$stateParams', 'dateUtilityService', 'activityService', 'contactActivityCreateEditModalService', 'yesNoActivityContactModalService'];
    
    function contactActivityModalService($modal, $stateParams, dateUtilityService, activityService, contactActivityCreateEditModalService, yesNoActivityContactModalService) {
		var service = {
			showContactActivityModal: showContactActivityModal
		};		
		return service;
		
		////////////
				
		function showContactActivityModal(contact, activities, start, end, activityTypes, userAuthorizationUserContainer) {
			 var contactActivityModal = $modal.open({
				controller: ContactActivityModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {   
			    	contact: function() {
			    		return contact; 
			    	}, 
			    	activities: function() {
			    		return activities;
			    	}, 
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	},
			    	activityTypes: function() {
			    		return activityTypes;
			    	}, 
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/contactActivity/contactActivity.modal.html'
			});
			return contactActivityModal;
			
			function ContactActivityModalController($modalInstance, $scope, contact, activities, start, end, activityTypes, userAuthorizationUserContainer) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.activities = activities;
		    	vm.filterStartDatePicker = false;
		    	vm.filterEndDatePicker = false;
		    	vm.filterStartDate = start;
		    	vm.filterEndDate = end;
		    	vm.activityTypes = activityTypes;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	
		    	vm.formatStartEndDateTime = formatStartEndDateTime;
		    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
		    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
		    	vm.showContactActivityCreateEditModal = showContactActivityCreateEditModal;
		    	vm.removeActivity = removeActivity;
		    	vm.showRemoveActivity = showRemoveActivity;
		    	vm.closeContactActivityModal = closeContactActivityModal;
		    	
		    	////////////

		    	$scope.$watch('vm.filterStartDate', function(newValue, oldValue) {
		    		if(oldValue != newValue && newValue != null) {
		    			loadActivitiesOfContact();
		    		}
				});
		    	
		    	$scope.$watch('vm.filterEndDate', function(newValue, oldValue) {
		    		if(oldValue != newValue && newValue != null) {
		    			loadActivitiesOfContact();
		    		}
				});
		    	
		    	function loadActivitiesOfContact() {
		    		var startDateString = dateUtilityService.formatDateToString(vm.filterStartDate);
		    		var endDateString = dateUtilityService.formatDateToString(vm.filterEndDate);
		    		activityService.getActivitiesOfContactIdInRange(vm.contact.id, startDateString, endDateString).then(function(response) {
		    			vm.activities = response.data;
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error contactActivitiy.service#loadActivitiesOfContact');
		  	   		 });
		    	}
		    	
		    	function openFilterStartDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterStartDatePicker = true;
		        }
		    	
		    	function openFilterEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterEndDatePicker = true;
		        }
		    	
		    	function showContactActivityCreateEditModal(activity) {
		    		contactActivityCreateEditModalService.showContactActivityCreateEditModal(vm.contact, activity, vm.activityTypes);
		    	}
		    	
		    	function removeActivity(activity) {
		    		activityService.deleteActivity(activity).then(function(response) {
		    			for(var i = 0; i < vm.activities.length; i++) {
		    				if(vm.activities[i].id === activity.id) {
		    					vm.activities.splice(i, 1);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error contactActivitiy.service#removeActivity');
		  	   		 });
		    	}
		    	
		    	function formatStartEndDateTime(start, end) {
		    		var startDate = new Date(start);
		    		var formattedStartDate = dateUtilityService.formatDateToDateTimeString(startDate);
		    		var endDate = new Date(end);
		    		var formattedEndDate = dateUtilityService.formatDateToDateTimeString(endDate);
		    		return dateUtilityService.formatStartEndDateTime(formattedStartDate, formattedEndDate);
		    	}
		    	
		    	function showRemoveActivity(activity) {
		    		yesNoActivityContactModalService.showYesNoModal(vm, vm.contact, activity);
		    	}
		    	
		    	function closeContactActivityModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactActivityCreateEditModalService', contactActivityCreateEditModalService);
        
    contactActivityCreateEditModalService.$inject = ['$modal', '$timeout', '$stateParams', 'dateUtilityService', 'activityService'];
    
    function contactActivityCreateEditModalService($modal, $timeout, $stateParams, dateUtilityService, activityService) {
		var service = {
			showContactActivityCreateEditModal: showContactActivityCreateEditModal
		};		
		return service;
		
		////////////
				
		function showContactActivityCreateEditModal(contact, activity, activityTypes) {
			 var contactActivityCreateEditModal = $modal.open({
				controller: ContactActivityCreateEditModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {   
			    	contact: function() {
			    		return contact; 
			    	}, 
			    	activity: function() {
			    		return activity;
			    	}, 
			    	activityTypes: function() {
			    		return activityTypes;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/contactActivityCreateEdit/contactActivityCreateEdit.modal.html'
			});
			return contactActivityCreateEditModal;
			
			function ContactActivityCreateEditModalController($modalInstance, $scope, contact, activity, activityTypes) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.activity = activity;
		    	vm.activityTypes = activityTypes;
		    	vm.activityUpdatedError = false;
		    	
		    	vm.addOrRemoveActivityType = addOrRemoveActivityType;
		    	vm.updateActivity = updateActivity;
		    	vm.isActivityTypeSelected = isActivityTypeSelected;
		    	vm.cancelFile = cancelFile;
		    	vm.closeContactActivityCreateEditModal = closeContactActivityCreateEditModal;
		    	
		    	////////////

		    	function setTimeout() {
		      		$timeout(function() {
				    	vm.activityUpdatedError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function isActivityTypeSelected(activityType) {
		    		for(var i = 0; i < vm.activity.activityTypes.length; i++) {
		    			if(vm.activity.activityTypes[i] === activityType) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function updateActivity() {
		    		activityService.update(vm.activity).then(function(response) {
		    			closeContactActivityCreateEditModal();
		  	   		 }, function errorCallback(response) {
		  	   			 vm.activityUpdatedError = true;
			  	   		 setTimeout();
		  	   			console.log('error contactActivityCreateEdit.service#updateActivity');
		  	   		 });
		    	}
		    	
		    	function addOrRemoveActivityType(activityType) {
		    		var removed = false;
		    		for(var i = 0; i < vm.activity.activityTypes.length; i++) {
		    			if(vm.activity.activityTypes[i] === activityType) {
		    				vm.activity.activityTypes.splice(i, i);
		    				removed = true;
		    				break;
		    			}
		    		}
		    		if(removed === false) {
		    			vm.activity.activityTypes.push(activityType);
		    		}
		    	}
		    	
		    	$scope.getFileNameWithoutExtension = function(activityDocumentFile) {
					var fileName = activityDocumentFile.fileName;
					var fileNameWithoutEnding = activityDocumentFile.fileName;
					var fileNameEnding = fileName.substr(fileName.lastIndexOf('.')+1, fileName.length);
			        if(fileNameEnding.length === 3) {
			            fileNameWithoutEnding = fileName.substr(0, fileName.lastIndexOf('.'));
			            return fileNameWithoutEnding;
			        } else {
			        	return fileName;
			        }
				};
				
				$scope.getFileNameExtension = function(activityDocumentFile) {
					var fileName = activityDocumentFile.fileName;
					var fileNameEnding = fileName.substr(fileName.lastIndexOf('.')+1, fileName.length);
			        if(fileNameEnding.length === 3) {
			            return fileNameEnding;
			        } else {
			        	return 'NO_ENDING';
			        }
				};
				
				function cancelFile(activityDocumentFile) {
					for(var i = 0; i < vm.activity.activityDocumentFiles.length; i++) {
						if(vm.activity.activityDocumentFiles[i].originalFileName === activityDocumentFile.originalFileName) {
							vm.activity.activityDocumentFiles.splice(i, 1);
							break;
						}
					}
				}
		    	
		    	function closeContactActivityCreateEditModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactFacilityDetailCreateEditModalService', contactFacilityDetailCreateEditModalService);
        
    contactFacilityDetailCreateEditModalService.$inject = ['$modal', '$timeout', '$stateParams', 'facilityDetailsService'];
    
    function contactFacilityDetailCreateEditModalService($modal, $timeout, $stateParams, facilityDetailsService) {
		var service = {
			showContactFacilityDetailCreateEditModal: showContactFacilityDetailCreateEditModal
		};		
		return service;
		
		////////////
				
		function showContactFacilityDetailCreateEditModal(contact, facilityDetail, activityTypes, facilityDetailsTypes) {
			 var contactFacilityDetailCreateEditModal = $modal.open({
				controller: ContactFacilityDetailCreateEditModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {   
			    	contact: function() {
			    		return contact; 
			    	}, 
			    	facilityDetail: function() {
			    		return facilityDetail;
			    	}, 
			    	activityTypes: function() {
			    		return activityTypes;
			    	},
			    	facilityDetailsTypes: function() {
			    		return facilityDetailsTypes;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/contactFacilityDetailCreateEdit/contactFacilityDetailCreateEdit.modal.html'
			});
			return contactFacilityDetailCreateEditModal;
			
			function ContactFacilityDetailCreateEditModalController($modalInstance, $scope, contact, facilityDetail, activityTypes, facilityDetailsTypes) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.facilityDetail = facilityDetail;
		    	vm.activityTypes = activityTypes;
		    	vm.facilityDetailsTypes = facilityDetailsTypes;
		    	vm.facilityDetailsUpdatedError = false;   
		    	
		    	vm.addOrRemoveActivityType = addOrRemoveActivityType;
		    	vm.addOrRemoveFacilityDetailsType = addOrRemoveFacilityDetailsType;
		    	vm.updateFacilityDetail = updateFacilityDetail;
		    	vm.isTypeSelected = isTypeSelected;
		    	vm.isActivityTypeSelected = isActivityTypeSelected;
		    	vm.isFacilityDetailTypeSelected = isFacilityDetailTypeSelected;
		    	vm.isFacilityTypeSelected = isFacilityTypeSelected;
		    	vm.closeContactFacilityDetailCreateEditModal = closeContactFacilityDetailCreateEditModal;
		    	
		    	////////////

		    	function setTimeout() {
		      		$timeout(function() {
		      			vm.facilityDetailsUpdatedError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function updateFacilityDetail() {
		    		facilityDetailsService.update(vm.facilityDetail).then(function (response) {
		    			closeContactFacilityDetailCreateEditModal();
					}).catch(function (data) {
						vm.facilityDetailsUpdatedError = true;
						setTimeout();
						console.log('error in contactFacilityDetailCreateEdit.service#updateFacilityDetail: ' + data);
					});  
		    	}
		    	
		    	function addOrRemoveActivityType(activityType) {
		    		if(activityType == null) {
		    			return;
		    		}
		    		var removed = false;
		    		for(var i = 0; i < vm.facilityDetail.activityTypes.length; i++) {
		    			if(vm.facilityDetail.activityTypes[i] === activityType) {
		    				vm.facilityDetail.activityTypes.splice(i, 1);
		    				removed = true;
		    				break;
		    			}
		    		}
		    		if(removed === false) {
		    			vm.facilityDetail.activityTypes.push(activityType);
		    		}
		    	}
		    	
		    	function addOrRemoveFacilityDetailsType(facilityDetailsType) {
		    		if(facilityDetailsType == null) {
		    			return;
		    		}
		    		var removed = false;
		    		for(var i = 0; i < vm.facilityDetail.facilityDetailsTypes.length; i++) {
		    			if(vm.facilityDetail.facilityDetailsTypes[i] === facilityDetailsType) {
		    				vm.facilityDetail.facilityDetailsTypes.splice(i, 1);
		    				if(facilityDetailsType === 'VORREINIGUNG') {
		    					for(var j = 0; j < vm.facilityDetail.facilityDetailsTypes.length; j++) {
		    		    			if(vm.facilityDetail.facilityDetailsTypes[j] === 'SEDIMENTATIONS_BECKEN' || vm.facilityDetail.facilityDetailsTypes[j] === 'FLOTATIONS_BECKEN') {
		    		    				vm.facilityDetail.facilityDetailsTypes.splice(j, 1);
		    		    			}
		    					}
		    				}
		    				removed = true;
		    				break;
		    			}
		    		}
		    		if(removed === false) {
		    			vm.facilityDetail.facilityDetailsTypes.push(facilityDetailsType);
		    		}
		    	}
		    	
		    	function isActivityTypeSelected(activityType) {
		    		for(var i = 0; i < vm.facilityDetail.activityTypes.length; i++) {   
		    			if(vm.facilityDetail.activityTypes[i] === activityType) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function isFacilityDetailTypeSelected(facilityDetailsType) {
		    		for(var i = 0; i < vm.facilityDetail.facilityDetailsTypes.length; i++) {   
		    			if(vm.facilityDetail.facilityDetailsTypes[i] === facilityDetailsType) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function isTypeSelected(type) {
		    		for(var i = 0; i < vm.facilityDetail.activityTypes.length; i++) {   
		    			if(vm.facilityDetail.activityTypes[i] === type) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function isFacilityTypeSelected(type) {
		    		for(var i = 0; i < vm.facilityDetail.facilityDetailsTypes.length; i++) {   
		    			if(vm.facilityDetail.facilityDetailsTypes[i] === type) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function closeContactFacilityDetailCreateEditModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('deleteImportedContactsModalService', deleteImportedContactsModalService);
        
    deleteImportedContactsModalService.$inject = ['$modal', '$stateParams', '$timeout', 'contactsService'];
    
    function deleteImportedContactsModalService($modal, $stateParams, $timeout, contactsService) {
		var service = {
			showDeleteImportedContactsModal: showDeleteImportedContactsModal
		};		
		return service;
		
		////////////
				
		function showDeleteImportedContactsModal(invoker, contactImportationDateGroups) {
			 var deleteImportedContactsModal = $modal.open({
				controller: DeleteImportedContactsModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: { 
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contactImportationDateGroups: function() {
			    		return contactImportationDateGroups;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/deleteImportedContacts/deleteImportedContacts.modal.html'
			});
			return deleteImportedContactsModal;
			
			function DeleteImportedContactsModalController($modalInstance, $scope, invoker, contactImportationDateGroups) {
		    	var vm = this;
		    	vm.invoker = invoker;
		    	vm.contactImportationDateGroups = contactImportationDateGroups;
		    	vm.selectedContactImportationDateGroup = null;
		    	vm.contactImportationDateGroupsTrigger = false;
		    	vm.contactImportationDateGroupNotSelectedError = false;
		    	
		    	vm.deleteImportedContactsOfBulk = deleteImportedContactsOfBulk;
		    	vm.closeDeleteImportedContactsModal = closeDeleteImportedContactsModal;
		    	
		    	////////////

		    	function setTimeout() {
		      		$timeout(function() {
		      			vm.contactImportationDateGroupNotSelectedError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function deleteImportedContactsOfBulk() {
		    		if(vm.selectedContactImportationDateGroup == null) {
		    			vm.contactImportationDateGroupNotSelectedError = true;
		    			setTimeout();
		    			return;
		    		}
		    		vm.contactImportationDateGroupsTrigger = true;
		    		contactsService.deleteImportedContactsOfBulk(vm.selectedContactImportationDateGroup.importationDate).then(function(response) {
		    			vm.invoker.pageChange(0);
		    			vm.contactImportationDateGroupsTrigger = false;
		    			vm.closeDeleteImportedContactsModal();
					}).catch(function (data) {
						console.log('error in contacts.controller.js#deleteImportedContacts: ' + data);
					});
		    	}
		    	
		    	function closeDeleteImportedContactsModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('editContactModalService', editContactModalService);
        
    editContactModalService.$inject = ['$modal', '$stateParams'];
    
    function editContactModalService($modal, $stateParams) {
		var service = {
			showEditContactModal: showEditContactModal
		};		
		return service;
		
		////////////
				
		function showEditContactModal(contact, titles, provinceTypes, contactTypes, countryTypes, invoker, currentUser, roles, usersOfProject) {
			 var editContactModal = $modal.open({
				controller: EditContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	contact: function() {
			    		return contact;
			    	},
			    	titles: function() {
			    		return titles;
			    	},
			    	provinceTypes: function() {
			    		return provinceTypes;
			    	},
			    	contactTypes: function() {
			    		return contactTypes;
			    	},
			    	countryTypes: function() {
			    		return countryTypes;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	roles: function() {
			    		return roles;
			    	},
			    	usersOfProject: function() {
			    		return usersOfProject;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/editContact/editContact.modal.html'
			});
			return editContactModal;
			
			function EditContactModalController($modalInstance, $scope, contact, titles, provinceTypes, contactTypes, countryTypes, invoker, currentUser, roles, usersOfProject) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.titles = titles;
		    	vm.provinceTypes = provinceTypes;
		    	vm.contactTypes = contactTypes;
		    	vm.countryTypes = countryTypes;
		    	vm.invoker = invoker;
		    	vm.currentUser = currentUser;
		    	vm.roles = roles;
				vm.usersOfProject = usersOfProject;
		    	
		    	vm.closeEditContactModal = closeEditContactModal;
		    	
		    	////////////
		    	
		    	function closeEditContactModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('facilityDetailsOfContactModalService', facilityDetailsOfContactModalService);
        
    facilityDetailsOfContactModalService.$inject = ['$modal', '$stateParams', 'dateUtilityService', 'facilityDetailsService', 'contactFacilityDetailCreateEditModalService', 'yesNoFacilityDetailsContactModalService'];
    
    function facilityDetailsOfContactModalService($modal, $stateParams, dateUtilityService, facilityDetailsService, contactFacilityDetailCreateEditModalService, yesNoFacilityDetailsContactModalService) {
		var service = {
			showFacilityDetailsOfContactModal: showFacilityDetailsOfContactModal
		};		
		return service;
		
		////////////
				
		function showFacilityDetailsOfContactModal(contact, facilityDetails, start, end, activityTypes, facilityDetailsTypes, userAuthorizationUserContainer) {
			 var facilityDetailsOfContactModal = $modal.open({
				controller: FacilityDetailsOfContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {   
			    	contact: function() {
			    		return contact; 
			    	}, 
			    	facilityDetails: function() {
			    		return facilityDetails;
			    	}, 
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	}, 
			    	activityTypes: function() {
			    		return activityTypes;
			    	}, 
			    	facilityDetailsTypes: function() {
			    		return facilityDetailsTypes;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/facilityDetailsOfContact/facilityDetailsOfContact.modal.html'
			});
			return facilityDetailsOfContactModal;
			
			function FacilityDetailsOfContactModalController($modalInstance, $scope, contact, facilityDetails, start, end, activityTypes, facilityDetailsTypes, userAuthorizationUserContainer) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.facilityDetails = facilityDetails;
		    	vm.filterStartDatePicker = false;
		    	vm.filterEndDatePicker = false;
		    	vm.filterStartDate = start;
		    	vm.filterEndDate = end;
		    	vm.activityTypes = activityTypes;
		    	vm.facilityDetailsTypes = facilityDetailsTypes;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	
		    	vm.showContactFacilityDetailCreateEditModal = showContactFacilityDetailCreateEditModal;
		    	vm.removeFacilityDetail = removeFacilityDetail;
		    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
		    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
		    	vm.formatStartEndDateTime = formatStartEndDateTime;
		    	vm.showRemoveFacilityDetail = showRemoveFacilityDetail;
		    	vm.closeFacilityDetailsOfContactModal = closeFacilityDetailsOfContactModal;
		    	
		    	////////////

		    	$scope.$watch('vm.filterStartDate', function(newValue, oldValue) {
		    		if(oldValue != newValue && newValue != null) {
		    			loadFacilityDetailsOfContact();
		    		}
				});
		    	
		    	$scope.$watch('vm.filterEndDate', function(newValue, oldValue) {
		    		if(oldValue != newValue && newValue != null) {
		    			loadFacilityDetailsOfContact();
		    		}
				});
		    	
		    	function loadFacilityDetailsOfContact() {
		    		var startDateString = dateUtilityService.formatDateToString(vm.filterStartDate);
		    		var endDateString = dateUtilityService.formatDateToString(vm.filterEndDate);
		    		facilityDetailsService.getFacilityDetailsOfContactIdInRange(vm.contact.id, startDateString, endDateString).then(function(response) {
		    			vm.facilityDetails = response.data;
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error facilityDetailsOfContact.service#loadFacilityDetailsOfContact');
		  	   		 });
		    	}
		    	
		    	function openFilterStartDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterStartDatePicker = true;
		        }
		    	
		    	function openFilterEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterEndDatePicker = true;
		        }
		    	
		    	function showContactFacilityDetailCreateEditModal(facilityDetail) {
		    		contactFacilityDetailCreateEditModalService.showContactFacilityDetailCreateEditModal(vm.contact, facilityDetail, vm.activityTypes, vm.facilityDetailsTypes);
		    	}
		    	
		    	function removeFacilityDetail(facilityDetail) {
		    		facilityDetailsService.deleteFacilityDetail(facilityDetail).then(function(response) {
		    			for(var i = 0; i < vm.facilityDetails.length; i++) {
		    				if(vm.facilityDetails[i].id === facilityDetail.id) {
		    					vm.facilityDetails.splice(i, 1);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error facilityDetailsOfContact.service#removeFacilityDetail');
		  	   		 });
		    	}
		    	
		    	function formatStartEndDateTime(start, end) {
		    		var startDate = new Date(start);
		    		var formattedStartDate = dateUtilityService.formatDateToDateTimeString(startDate);
		    		var endDate = new Date(end);
		    		var formattedEndDate = dateUtilityService.formatDateToDateTimeString(endDate);
		    		return dateUtilityService.formatStartEndDateTime(formattedStartDate, formattedEndDate);
		    	}
		    	
		    	function showRemoveFacilityDetail(facilityDetails) {
		    		yesNoFacilityDetailsContactModalService.showYesNoModal(vm, vm.contact, facilityDetails);
		    	}
		    	
		    	function closeFacilityDetailsOfContactModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('projectsOfContactModalService', projectsOfContactModalService);
        
    projectsOfContactModalService.$inject = ['$modal', '$timeout', '$stateParams', 'productsService', 'contactsService'];
    
    function projectsOfContactModalService($modal, $timeout, $stateParams, productsService, contactsService) {
		var service = {
			showProductsOfContactModal: showProductsOfContactModal
		};		
		return service;
		
		////////////
				
		function showProductsOfContactModal(contact, currentUser, userAuthorizationUserContainer) {
			 var productsOfContactContactModal = $modal.open({
				controller: ProductsOfContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	contact: function() {
			    		return contact;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}, 
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/productsOfContact/productsOfContact.modal.html'
			});
			return productsOfContactContactModal;
			
			function ProductsOfContactModalController($modalInstance, $scope, contact, currentUser, userAuthorizationUserContainer) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.currentUser = currentUser;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	
		    	vm.getProductsByTerm = getProductsByTerm;
		    	vm.productSelected = productSelected;
		    	vm.removeProduct = removeProduct;
		    	vm.removeProductOfAddressWithProducts = removeProductOfAddressWithProducts;
		    	vm.productSelectedAtAddressWithProducts = productSelectedAtAddressWithProducts;
		    	vm.hideAddressWithProducts = hideAddressWithProducts;
		    	vm.closeProductsOfContact = closeProductsOfContact;
		    	
		    	////////////

		    	function setFileUpdateTimeout(address) {
		      		$timeout(function() {
		      			address.productStillAddedToContactError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function getProductsByTerm(term) {
		    		return productsService.findProductsByTerm(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error productsOfContact.service#getProductsByTerm'); 
		  	   		 });
		    	}
		    	
		    	function productSelected(selectedProduct, address) {
		    		contactsService.addProductToContact(address.id, selectedProduct.id, vm.contact).then(function(response) {
		    			if(vm.contact.products == null) {
		    				vm.contact.products = [];
		    			}
		    			vm.contact.products.push(response.data);
		  	   		 }, function errorCallback(response) {
		  	   			 if(response.data.error == 'PRODUCT_STILL_ADDED_TO_CONTACT') {
		  	   				 address.productStillAddedToContactError = true;
		  	   				 setFileUpdateTimeout(address);
		  	   			 }
		  	   			 console.log('error productsOfContact.service#productSelected'); 
		  	   		 });
		    	}
		    	
		    	function productSelectedAtAddressWithProducts(selectedProduct, address) {
		    		contactsService.addProductToContact(address.id, selectedProduct.id, vm.contact).then(function(response) {
		    			for(var i = 0; i < vm.contact.addressesWithProducts.length; i++) {
		    				if(vm.contact.addressesWithProducts[i].address.id === address.id) {
		    					if(vm.contact.addressesWithProducts[i].products == null) {
		    						vm.contact.addressesWithProducts[i].products = [];
		    					}
		    					vm.contact.addressesWithProducts[i].products.push(response.data);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 if(response.data.error == 'PRODUCT_STILL_ADDED_TO_CONTACT') {
		  	   				 address.productStillAddedToContactError = true;
		  	   				 setFileUpdateTimeout(address);
		  	   			 }
		  	   			 console.log('error productsOfContact.service#productSelected'); 
		  	   		 });
		    	}
		    	
		    	function removeProduct(product, address) {
		    		contactsService.removeProductFromContact(address.id, product.id, vm.contact).then(function(response) {
		    			var removedProduct = response.data;
		    			for(var i = 0; i < vm.contact.products.length; i++) {
		    				if(vm.contact.products[i].id == removedProduct.id) {
		    					vm.contact.products.splice(i, 1);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error productsOfContact.service#removeProduct'); 
		  	   		 });
		    	}
		    	
		    	function removeProductOfAddressWithProducts(product, address) {
		    		contactsService.removeProductFromContact(address.id, product.id, vm.contact).then(function(response) {
		    			var removedProduct = response.data;
		    			for(var i = 0; i < vm.contact.addressesWithProducts.length; i++) {
		    				if(vm.contact.addressesWithProducts[i].address.id === address.id) {
		    					vm.contact.addressesWithProducts[i].products.splice(i, 1);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error productsOfContact.service#removeProductOfAddressWithProducts'); 
		  	   		 });
		    	}
		    	
		    	function hideAddressWithProducts(addressWithProducts) {
					var address = addressWithProducts.address;
					if(address == null) {
						return false;
					}
					if(address.userIdBlackList != null && address.userIdBlackList.indexOf(vm.currentUser.id) !== -1) {
						return true;
					}
					if(address.rolesBlackList != null && address.rolesBlackList.indexOf(vm.currentUser.projectUserConnectionRole) !== -1) {
						return true;
					}
					return false;
				}
		    	
		    	function closeProductsOfContact() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('projectsAssignedToContactModalService', projectsAssignedToContactModalService);
        
    projectsAssignedToContactModalService.$inject = ['$modal', '$stateParams', 'projectUserConnectionService'];
    
    function projectsAssignedToContactModalService($modal, $stateParams, projectUserConnectionService) {
		var service = {
			showProjectsAssignedToContactModal: showProjectsAssignedToContactModal
		};		
		return service;
		
		////////////
				
		function showProjectsAssignedToContactModal(contact, projects, projectUserConnections) {
			 var projectsAssignedToContactModal = $modal.open({
				controller: ProjectsAssignedToContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	contact: function() {
			    		return contact;
			    	},
			    	projects: function() {
			    		return projects;
			    	},
			    	projectUserConnections: function() {
			    		return projectUserConnections;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/projectsAssignedToContact/projectsAssignedToContact.modal.html'
			});
			return projectsAssignedToContactModal;
			
			function ProjectsAssignedToContactModalController($modalInstance, $scope, contact, projects, projectUserConnections) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.projects = projects;
		    	vm.projectUserConnections = projectUserConnections;
		    	
		    	vm.projectSelectionChanged = projectSelectionChanged;
		    	vm.projectStillAdded = projectStillAdded;
		    	vm.uncheckProject = uncheckProject;
		    	vm.closeProjectAssignedToContactModal = closeProjectAssignedToContactModal;
		    	
		    	////////////
		    	
		    	function projectSelectionChanged(project, checked) {
		    		if(checked === true) {
		    			addContactToProject(project);
		    		} else if(checked === false) {
		    			removeContactFromProject(project);
		    		}
		    	} 
		    	
		    	function addContactToProject(project) {
		    		var projectUserConnection = {};
		    		projectUserConnection.contact = vm.contact;
					projectUserConnection.project = project;
					projectUserConnection.projectUserConnectionRole = 'CONTACT';
					projectUserConnectionService.createProjectUserConnection(projectUserConnection).then(function successCallback(response) {
						vm.projectUserConnections.push(response.data);
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error projectsAssignedToContact.service#addContactToProject');
		   	   		 });	
		    	}
		    	
		    	function removeContactFromProject(project) {
		    		projectUserConnectionService.deleteProjectUserConnectionOverProjectAndContact(project.id, vm.contact.id).then(function successCallback(response) {
		    			for(var i = 0; i < vm.projectUserConnections.length; i++) {
		    				if(vm.projectUserConnections[i].id == response.data.id) {
		    					vm.projectUserConnections.splice(i, 1);
		    					break;
		    				}
		    			}
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error projectsAssignedToContact.service#removeUserFromProject');
		   	   		 });	
		    	}
		    	
		    	function projectStillAdded(project) {
		    		for(var i = 0; i < vm.projectUserConnections.length; i++) {
		    			if(vm.projectUserConnections[i].project.id == project.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function uncheckProject(project) {
		    		for(var i = 0; i < vm.projects.length; i++) {
		    			if(vm.projects[i].id === project.id) {
		    				vm.projects[i].checked = false;
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function closeProjectAssignedToContactModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('usersAssignedToContactModalService', usersAssignedToContactModalService);
        
    usersAssignedToContactModalService.$inject = ['$modal', '$timeout', '$stateParams', 'projectUserConnectionService'];
    
    function usersAssignedToContactModalService($modal, $timeout, $stateParams, projectUserConnectionService) {
		var service = {
			showUsersAssignedToContactModal: showUsersAssignedToContactModal
		};		
		return service;
		
		////////////
				
		function showUsersAssignedToContactModal(contact, users, projectUserConnections) {
			 var usersAssignedToContactModal = $modal.open({
				controller: UsersAssignedToContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	contact: function() {
			    		return contact;
			    	},
			    	users: function() {
			    		return users;
			    	},
			    	projectUserConnections: function() {
			    		return projectUserConnections;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/usersAssignedToContact/usersAssignedToContact.modal.html'
			});
			return usersAssignedToContactModal;
			
			function UsersAssignedToContactModalController($modalInstance, $scope, contact, users, projectUserConnections) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.users = users;
		    	vm.projectUserConnections = projectUserConnections;
		    	
		    	vm.userSelectionChanged = userSelectionChanged;
		    	vm.userStillAdded = userStillAdded;
		    	vm.uncheckUser = uncheckUser;
		    	vm.closeUsersAssignedToContactModal = closeUsersAssignedToContactModal;
		    	
		    	////////////
		    	
		    	function setTimeout(user) {
		      		$timeout(function() {
		      			user.addedUserToContactTrigger = false;
		      			user.removedUserFromContactTrigger = false;
		  			   }, 2000); 
		      	}
		    	
		    	function userSelectionChanged(user, checked) {
		    		if(checked === true) {
		    			addUserToContact(user);
		    		} else if(checked === false) {
		    			removeUserFromContact(user);
		    		}
		    	}
		    	
		    	function addUserToContact(user) {
		    		var projectUserConnection = {};
		    		projectUserConnection.contact = vm.contact;
					projectUserConnection.user = user;
					projectUserConnection.projectUserConnectionRole = 'USER';
					projectUserConnectionService.createProjectUserConnection(projectUserConnection).then(function(response) {
						vm.projectUserConnections.push(response.data);
						user.addedUserToContactTrigger = true;
						setTimeout(user);
		   	   		 }, function(response) {
		   	   			  console.log('error usersAssignedToContact.service#addUserToContact');
		   	   		 });	
		    	}
		    	
		    	function removeUserFromContact(user) {
		    		projectUserConnectionService.deleteProjectUserConnectionOverUserAndContact(user.id, vm.contact.id).then(function(response) {
		    			for(var i = 0; i < vm.projectUserConnections.length; i++) {
		    				if(vm.projectUserConnections[i].id == response.data.id) {
		    					vm.projectUserConnections.splice(i, 1);
		    					user.removedUserFromContactTrigger = true;
		    					setTimeout(user);
		    					break;
		    				}
		    			}
		   	   		 }, function(response) {
		   	   			  console.log('error usersAssignedToContact.service#removeUserFromContact');
		   	   		 });	
		    	}
		    	
		    	function userStillAdded(user) {
		    		for(var i = 0; i < vm.projectUserConnections.length; i++) {
		    			if(vm.projectUserConnections[i].user.id == user.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function uncheckUser(user) {
		    		for(var i = 0; i < vm.users.length; i++) {
		    			if(vm.users[i].id === user.id) {
		    				vm.users[i].checked = false;
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function closeUsersAssignedToContactModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoActivityContactModalService', yesNoActivityContactModalService);
        
    yesNoActivityContactModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoActivityContactModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, contact, activity) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contact: function() {
			    		return contact;
			    	},
			    	activity: function() {
			    		return activity;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/yesNoActivityModal/yesNoActivity.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, contact, activity) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contact = contact;
		    	vm.activity = activity;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.removeActivity(vm.activity);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoDeactivateContactModalService', yesNoDeactivateContactModalService);
        
    yesNoDeactivateContactModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoDeactivateContactModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, contact, state) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contact: function() {
			    		return contact;
			    	},
			    	state: function() {
			    		return state;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/yesNoDeactivateModal/yesNoDeactivate.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, contact, state) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contact = contact;
		    	vm.state = state;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.deactivateContact(vm.contact, vm.state);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoFacilityDetailsContactModalService', yesNoFacilityDetailsContactModalService);
        
    yesNoFacilityDetailsContactModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoFacilityDetailsContactModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, contact, facilityDetails) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contact: function() {
			    		return contact;
			    	},
			    	facilityDetails: function() {
			    		return facilityDetails;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/yesNoFacilityDetailsModal/yesNoFacilityDetails.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, contact, facilityDetails) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contact = contact;
		    	vm.facilityDetails = facilityDetails;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.removeFacilityDetail(vm.facilityDetails);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoContactModalService', yesNoContactModalService);
        
    yesNoContactModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoContactModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, contact, productsReferencedToContact, calendarEventUserConnections) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contact: function() {
			    		return contact;
			    	},
			    	productsReferencedToContact: function() {
			    		return productsReferencedToContact;
			    	}, 
			    	calendarEventUserConnections: function() {
			    		return calendarEventUserConnections;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, contact, productsReferencedToContact, calendarEventUserConnections) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contact = contact;
		    	vm.productsReferencedToContact = productsReferencedToContact;
		    	vm.calendarEventUserConnections = calendarEventUserConnections;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.deleteContact(vm.contact);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement')
    	.config(configure);
    			
    configure.$inject = ['$httpProvider', 'authTokenServiceProvider', 'token_config'];
    
	function configure($httpProvider, authTokenServiceProvider, token_config) {
		$httpProvider.interceptors.push(legalProjectManagementInterceptor);
		
		////////////
		
		function legalProjectManagementInterceptor($q) {
			var interceptor = {
					request: request,
					response: response,
					responseError: responseError
			};
			
			return interceptor;
			
			////////////
			
			function request(config) {
				var token = authTokenServiceProvider.$get().getToken();
				if (token) {
					config.headers[token_config.AUTH_TOKEN_HEADER] = token;
				}
				return config;
			}
			
			function response(response) {
				return response;
			}
			
			function responseError(response) {
				switch(response.status) {
				case 401:
					authTokenServiceProvider.$get().deleteToken();	
					break;
				}
				return $q.reject(response);
			}
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement')
    	.config(configure)
    	.run(['$state', function ($state) {}]);
    
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];
    
    function configure($stateProvider, $urlRouterProvider) {

		console.log("configure");

    	$urlRouterProvider.otherwise('/signin');
    	
    	$stateProvider
			.state(getAuthState());
    	
		////////////
			
		function getAuthState() {
			var state = {
				name: 'auth',
    			abstract: true,
    			views: {
    				'header': {
    					controller: 'NavController',
    					controllerAs: 'vm',
    					templateUrl: 'app/nav/nav.html'
    				},
    				'content': {
    					templateUrl: 'app/templates/content.html'
    				},
    				'footer': {
    					templateUrl: 'app/templates/footer.html'
    				}
    			},
				resolve: {
					authUserService: 'authUserService',	
					userService: 'userService',
					currentUser: function (authUserService) {
						return authUserService.getCurrentUser();
					}
				}
    		};
    		return state;
    	}
	}
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement')
    	.factory('RouterHelper', RouterHelper);
    
    RouterHelper.$inject = ['$stateProvider', '$urlRouterProvider', '$state'];

    function RouterHelper($stateProvider, $urlRouterProvider, $state) {

        console.log('RouterHelper');
        var hasOtherwise = false;

        var service = {
    		addState: addState,
    		configureOtherwise: configureOtherwise,
            getStates: getStates
        };

        return service;

        ///////////////


        function addState(stateConfig) {
            console.log('stateConfig', stateConfig);
        	return $stateProvider.state(stateConfig);
        }
        
        function configureOtherwise(otherwisePath) {
            if (otherwisePath && !hasOtherwise) {
                hasOtherwise = true;
                $urlRouterProvider.otherwise(otherwisePath);
            }
        }

        function getStates() { 
        	return $state.get(); 
        }
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactsChangedService', contactsChangedService);
        
    contactsChangedService.$inject = ['$http', 'api_config'];
    
    function contactsChangedService($http, api_config) {
		var service = {
			getPagedContactsChangedByCommitted: getPagedContactsChangedByCommitted,
			getAmountOfContactsChanged: getAmountOfContactsChanged,
			getAmountOfContactsChangedByCommitted: getAmountOfContactsChangedByCommitted,
			getContactsChangedByCommitted: getContactsChangedByCommitted,
			commitContactChanged: commitContactChanged,
			deleteContactChanged: deleteContactChanged
		};
		
		return service;
		
		////////////
		
		function getPagedContactsChangedByCommitted(page, committed) {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchange/' + page + '/' + committed);
		}
		
		function getAmountOfContactsChanged() {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchanged/amount');
		}
		
		function getAmountOfContactsChangedByCommitted(committed) {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchanged/amount/' + committed);
		}
		
		function getContactsChangedByCommitted(committed) {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchanged/' + committed);
		}
		
		function commitContactChanged(id) {
			return $http.get(api_config.BASE_URL + '/contactschanged/contactchanged/commit/' + id);
		}
		
		function deleteContactChanged(id) {
			return $http.delete(api_config.BASE_URL + '/contactschanged/contactchanged/' + id);
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactsImportedService', contactsImportedService);
        
    contactsImportedService.$inject = ['$http', 'api_config'];
    
    function contactsImportedService($http, api_config) {
		var service = {
			getAmountOfContactsImported: getAmountOfContactsImported,
			deleteContactImported: deleteContactImported,
			deleteAllContactsImported: deleteAllContactsImported
		};
		
		return service;
		
		////////////
		
		function getAmountOfContactsImported() {
			return $http.get(api_config.BASE_URL + '/contactsimported/contactimported/amount');
		}
		
		function deleteContactImported(id) {
			return $http.delete(api_config.BASE_URL + '/contactsimported/contactimported/' + id + '/delete');
		}
		
		function deleteAllContactsImported() {
			return $http.delete(api_config.BASE_URL + '/contactsimported/contactimported/delete/all');
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactsPreviousImportedService', contactsPreviousImportedService);
        
    contactsPreviousImportedService.$inject = ['$http', 'api_config'];
    
    function contactsPreviousImportedService($http, api_config) {
		var service = {
			getAmountOfContactsPreviousImported: getAmountOfContactsPreviousImported
		};
		
		return service;
		
		////////////
		
		function getAmountOfContactsPreviousImported() {
			return $http.get(api_config.BASE_URL + '/contactspreviousimported/contactpreviousimported/amount');
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('dashboardService', dashboardService);
        
    dashboardService.$inject = ['$http', 'api_config'];
    
    function dashboardService($http, api_config) {
		var service = {
		};
		
		return service;
		
		////////////
		
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('systemImportService', systemImportService);
        
    systemImportService.$inject = ['$http', 'api_config'];
    
    function systemImportService($http, api_config) {
		var service = {
			getContactsImported: getContactsImported,
			confirmContactImported: confirmContactImported,
			getPagedContactsImported: getPagedContactsImported,
			mergeContactsWithAddressesAndSave: mergeContactsWithAddressesAndSave,
			mergeContactsWithAddressesAndSaveOfStandardPath: mergeContactsWithAddressesAndSaveOfStandardPath
		};
		
		return service;
		
		////////////
		
		function getContactsImported() {
			return $http.get(api_config.BASE_URL + '/systemimports/systemimport/contactimported');
		}
		
		function confirmContactImported(contactAndContactImportedContainer) {
			return $http.put(api_config.BASE_URL + '/systemimports/systemimport/confirm', contactAndContactImportedContainer);
		}
		
		function getPagedContactsImported(page) {
			return $http.get(api_config.BASE_URL + '/systemimports/systemimport/' + page);			
		}
		  
		function mergeContactsWithAddressesAndSave(pathAddress, pathContact, callback, errorCallback) {
			var systemImportContainer = {};
			systemImportContainer.pathAddress = pathAddress;
			systemImportContainer.pathContact = pathContact;
			return $http.put(api_config.BASE_URL + '/systemimports/systemimport', systemImportContainer).then(function(resp) {
            	if (callback) {
            		callback(resp);
                }
            }, function(response) {
                if(errorCallback) {
                	errorCallback(response);
                }
            });
		}
		
		function mergeContactsWithAddressesAndSaveOfStandardPath() {
			return $http.put(api_config.BASE_URL + '/systemimports/systemimport/ofstandardpath');
		}
    }
})();

(function() {
    'use strict'; 

    angular
    	.module('legalprojectmanagement.dashboard')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getDashboardState());
    	 
    	////////////
			    	
    	function getDashboardState() {
    		var state = {
    			name: 'auth.dashboard',
				url: '/dashboard/:userId',
				templateUrl: 'app/dashboard/dashboard/dashboard.html',
				controller: 'DashboardController',
				controllerAs: 'vm',
				resolve: {
					projectsService: 'projectsService',
					calendarEventUserConnectionService: 'calendarEventUserConnectionService',
					authUserService: 'authUserService',
					calendarService: 'calendarService',
					contactsService: 'contactsService',
					contactsChangedService: 'contactsChangedService',
					communicationService: 'communicationService',
					contactsImportedService: 'contactsImportedService',
					contactsPreviousImportedService: 'contactsPreviousImportedService',
					amountOfContacts: function countByTenantAndActive($stateParams, contactsService) {
						return contactsService.countByTenantAndActive($stateParams.userId);
					},
					calendarEventsOfNextWeeks: function calendarEventsOfNextWeeks($stateParams, calendarEventUserConnectionService) {
						return null;
					},
					communicationUserConnectionsSent: function findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived($stateParams, communicationService) {
						return null;
					},
					communicationUserConnectionsReceived: function findCommunicationUserConnectionByUserCreatedMessageNotAndAndUserReceivedMessageAndTenantAndArchived($stateParams, communicationService) {
						return null;
					},
					sentAndOpenOrders: function findSentAndOpenOrders($stateParams, communicationService) {
						return null;
					},
					receivedAndOpenOrders: function findReceivedAndOpenOrders($stateParams, communicationService) {
						return null;
					},
					amountOfProducts: function(productsService) {
						return 0;
					},
					amountOfContactsImported: function(contactsImportedService) {
						return contactsImportedService.getAmountOfContactsImported();
					},
					amountOfContactsToExport: function(contactsChangedService) {
						return contactsChangedService.getAmountOfContactsChangedByCommitted(false);
					},
					amountOfContactsPreviousImported: function(contactsPreviousImportedService) {
						return contactsPreviousImportedService.getAmountOfContactsPreviousImported();
					},
					amountOfContactsChanged: function(contactsChangedService) {
						return contactsChangedService.getAmountOfContactsChanged();
					},
					amountOfCommittedContactsChanged: function(contactsChangedService) {
						return contactsChangedService.getAmountOfContactsChangedByCommitted(true);
					},
					lastLogin: function getLastLogin($stateParams, authUserService) {
						return authUserService.getLastLogin($stateParams.userId);
					},
					projects: function findProjectsOfUser($stateParams, projectsService) {
						return projectsService.findProjectsOfUser($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					countryTypes: function getCountryTypes(optionsService) {
						return optionsService.getCountryTypes();
					},
					titles: function getAllTitles(optionsService) {
						return optionsService.getAllTitles();
					},
					provinceTypes: function getProvinceTypes(optionsService) {
						return optionsService.getProvinceTypes();
					},
					contactTypes: function getContactTypes($stateParams, optionsService) {
						return optionsService.getContactTypes();
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.controller('DashboardController', DashboardController);
      
    DashboardController.$inject = ['$scope', '$timeout', '$translate', 'ngToast', 'uploadService', 'currentUser', 'amountOfProducts', 'amountOfContactsImported', 'amountOfContactsToExport', 'amountOfContactsPreviousImported', 'amountOfContactsChanged', 'amountOfCommittedContactsChanged', 'calendarEventsOfNextWeeks', 'amountOfContacts', 'communicationUserConnectionsSent', 'communicationUserConnectionsReceived', 'sentAndOpenOrders', 'receivedAndOpenOrders', 'projects', 'lastLogin', 'countryTypes', 'titles', 'provinceTypes', 'contactTypes', 'documentFileService', 'communicationService', 'messageReceiverOverviewModalService', 'communicationMessageService', 'dashboardYesNoModalService', 'projectUserConnectionService', 'dateUtilityService', 'optionsService', 'productsService', 'userService', 'userAuthorizationUserContainer', 'adjustmentModalService', 'systemImportService', 'contactAndContactImportedModalService', 'contactsService', 'contactsImportedService', 'contactsPreviousImportedService', 'contactImportedYesNoModalService', 'contactExportYesNoModalService', 'contactsChangedService', 'contactChangedModalService'];
        
    function DashboardController($scope, $timeout, $translate, ngToast, uploadService, currentUser, amountOfProducts, amountOfContactsImported, amountOfContactsToExport, amountOfContactsPreviousImported, amountOfContactsChanged, amountOfCommittedContactsChanged, calendarEventsOfNextWeeks, amountOfContacts, communicationUserConnectionsSent, communicationUserConnectionsReceived, sentAndOpenOrders, receivedAndOpenOrders, projects, lastLogin, countryTypes, titles, provinceTypes, contactTypes, documentFileService, communicationService, messageReceiverOverviewModalService, communicationMessageService, dashboardYesNoModalService, projectUserConnectionService, dateUtilityService, optionsService, productsService, userService, userAuthorizationUserContainer, adjustmentModalService, systemImportService, contactAndContactImportedModalService, contactsService, contactsImportedService, contactsPreviousImportedService, contactImportedYesNoModalService, contactExportYesNoModalService, contactsChangedService, contactChangedModalService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	vm.teaserMessageLength = 100;
    	vm.currentUser = currentUser;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.projects = projects.data;
    	vm.amountOfProducts = amountOfProducts.data;
    	vm.amountOfContactsImported = amountOfContactsImported.data;
    	vm.amountOfContactsToExport = amountOfContactsToExport.data;
    	vm.amountOfContactsPreviousImported = amountOfContactsPreviousImported.data;
    	vm.amountOfContacts = amountOfContacts.data;
    	vm.amountOfContactsChanged = amountOfContactsChanged.data;
    	vm.amountOfCommittedContactsChanged = amountOfCommittedContactsChanged.data;
    	vm.productsOfUser = null;
    	vm.sentAndOpenOrders = sentAndOpenOrders != null ? sentAndOpenOrders.data : null;
    	vm.receivedAndOpenOrders = receivedAndOpenOrders != null ? receivedAndOpenOrders.data : null;
    	vm.countryTypes = countryTypes.data; 
    	vm.titles = titles.data;
    	vm.provinceTypes = provinceTypes.data;
    	vm.contactTypes = contactTypes.data;
    	vm.communicationUserConnectionSentOrder = '-communication.receiveDate';
    	vm.calendarEventsOfNextWeeks = calendarEventsOfNextWeeks != null ? calendarEventsOfNextWeeks.data : null;
    	vm.projectUserConnections = null;
    	vm.communicationUserConnectionsSent = communicationUserConnectionsSent != null ? communicationUserConnectionsSent.data : null;
    	vm.communicationUserConnectionsReceived = communicationUserConnectionsReceived != null ? communicationUserConnectionsReceived.data : null;
    	vm.communicationUserConnectionOfUserAndPage = null;
    	vm.lastLogin = lastLogin.data;
    	vm.todayDate = new Date(); 
    	vm.sentOrderType = '-sortDate';
    	vm.receivedOrderType = '-sortDate';
    	vm.addedProductSuccess = false;
    	vm.addProductError = false;
    	vm.addedProjectUserConnectionSuccess = false;
    	vm.addedProjectUserConnectionError = false;
    	vm.selectedProduct = null;
    	vm.selectedProjectUserConnection = null;
    	vm.options = {legend: {display: true}};
    	vm.labels = [];
    	vm.data = [];
    	vm.contactAndContactImportedContainers = [];
    	vm.contactsChanged = [];
    	vm.loadingContactsImported = false;
    	vm.contactsImportedPage = 0;
    	vm.contactImportedConfirmed = false;
    	vm.contactImportedDeleted = false;
    	vm.loadingContactsToExport = false;
    	vm.contactsToExportPage = 0;
    	vm.contactToExportConfirmed = false;
    	vm.contactToExportDeleted = false;
    	
    	vm.executionDateAfterToday = executionDateAfterToday;
    	vm.getAmountOfDocumentFilesWithUniqueName = getAmountOfDocumentFilesWithUniqueName;
    	vm.archiveCommunicationUserConnection = archiveCommunicationUserConnection;
    	vm.showMessageReceiverOverview = showMessageReceiverOverview;
    	vm.showCommunicationMessageModal = showCommunicationMessageModal;
    	vm.communicationOrderExecuted = communicationOrderExecuted;
    	vm.showYesNoModal = showYesNoModal;
    	vm.setAsDefaultProject = setAsDefaultProject;
    	vm.datesAreEqual = datesAreEqual;
    	vm.getTeasingMessage = getTeasingMessage;
    	vm.getProductsByTerm = getProductsByTerm;
    	vm.getProjectUserConnectionsByTerm = getProjectUserConnectionsByTerm;
    	vm.removeProduct = removeProduct;
    	vm.removeProjectUserConnection = removeProjectUserConnection;
    	vm.productSelected = productSelected;
    	vm.projectUserConnectionSelected = projectUserConnectionSelected;
    	vm.showAdjustmentModal = showAdjustmentModal;
    	vm.loadingPagedContactsImported = loadingPagedContactsImported;
    	vm.loadingPagedContactsToExport = loadingPagedContactsToExport;
    	vm.loadContactsImported = loadContactsImported;
    	vm.confirmContactImported = confirmContactImported;
    	vm.showContactAndContactImported = showContactAndContactImported;
    	vm.getCustomerNumbers = getCustomerNumbers;
    	vm.showDeleteContactImported = showDeleteContactImported;
    	vm.showDeleteContactExport = showDeleteContactExport;
    	vm.deleteContactImported = deleteContactImported;
    	vm.showContactChanged = showContactChanged;
    	vm.commitContactChanged = commitContactChanged;
    	vm.deleteContactChanged = deleteContactChanged;
    	//getProductsOfUser();
    	//getProjectUserConnectionsOfUser();
    	//getProductTypeOverview();
    	loadContactsImported(vm.contactsImportedPage);
    	loadContactsToExport(vm.contactsToExportPage);
    	
    	vm.releaseToastMsg = ngToast.warning({ 
  		  content: '<span class="glyphicon glyphicon-info-sign infoSign"></span> <span>Version 1.0.26 - Versionsinformationen durch Klicken der Versionsnummer in der Fußzeile</span>',
  		  timeout: 3000,
  		  dismissButton: true
  		});
    	
    	////////////
    	
    	function datesAreEqual(start, end) {
    		var result = dateUtilityService.longDatesAreEqual(start, end);
    		return result;
    	}
    	
    	
    	function getAmountOfContacts() {
			contactsService.countByTenantAndActive(vm.currentUser.id).then(function(response) {
				vm.amountOfContacts = response.data;
			}, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#amountOfContacts');
 	   		 });
		}
    	
		function getAmountOfContactsImported() {
			contactsImportedService.getAmountOfContactsImported().then(function(response) {
				vm.amountOfContactsImported = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#amountOfContactsImported');
 	   		 });
		}
		
		function getAmountOfContactsPreviousImported() {
			contactsPreviousImportedService.getAmountOfContactsPreviousImported().then(function(response) {
				vm.amountOfContactsPreviousImported = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#getAmountOfContactsPreviousImported');
 	   		 });
		}
		
		function getAmountOfContactsChanged() {
			contactsChangedService.getAmountOfContactsChanged().then(function(response) {
				vm.amountOfContactsChanged = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#getAmountOfContactsChanged');
 	   		 });
		}  
		
		function getAmountOfContactsChangedByCommitted(committed, getAmountOfContactsChangedByCommittedCallback) {
			contactsChangedService.getAmountOfContactsChangedByCommitted(committed).then(function(response) {
				var oldValue = null;
				if(committed) {
					oldValue = vm.amountOfCommittedContactsChanged;
					vm.amountOfCommittedContactsChanged = response.data;
					if(getAmountOfContactsChangedByCommittedCallback) {
						getAmountOfContactsChangedByCommittedCallback(oldValue, vm.amountOfCommittedContactsChanged);
					}
				} else {
					oldValue = vm.amountOfContactsToExport;
					vm.amountOfContactsToExport = response.data;
					if(getAmountOfContactsChangedByCommittedCallback) {
						getAmountOfContactsChangedByCommittedCallback(oldValue, vm.amountOfContactsToExport);
					}
				}
 	   		 }, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#getAmountOfContactsChangedByCommitted');
 	   		 });
		}
		
    	function loadingPagedContactsImported() {
    		if(vm.loadingContactsImported == false) {
    			vm.loadingContactsImported = true;
	    		vm.contactsImportedPage = vm.contactsImportedPage + 1;
	    		loadContactsImported(vm.contactsImportedPage);
	    		vm.loadingContactsImported = false;
    		}
    	}
    	
    	function loadingPagedContactsToExport() {
			if(vm.loadingContactsToExport == false) {
    			vm.loadingContactsToExport = true;
	    		vm.contactsToExportPage = vm.contactsToExportPage + 1;
	    		loadContactsToExport(vm.contactsToExportPage);
	    		vm.loadingContactsToExport = false;
    		}
		}
    	
    	function loadContactsImported(page) {
    		if(vm.amountOfContactsImported > vm.contactAndContactImportedContainers.length) {
    			if(vm.contactAndContactImportedContainers == null) {
    				vm.contactAndContactImportedContainers = [];
    			}
    			systemImportService.getPagedContactsImported(page).then(function(response) {
	    			var loadedContactsImported = response.data;
	    			if(loadedContactsImported != null && loadedContactsImported.length > 0) {
	    				vm.contactAndContactImportedContainers.push.apply(vm.contactAndContactImportedContainers, loadedContactsImported);
		    			console.log("push contacts imported:" + vm.contactAndContactImportedContainers.length + ', ' + loadedContactsImported.length);
	    			}
	  	   		 }, function errorCallback(response) {
	  	   			 console.log('error products.controller#loadProducts');
	  	   		 });
    		}
    	}
    	
    	function loadContactsToExport(page) {
    		if(vm.amountOfContactsToExport > vm.contactsChanged.length) {
    			if(vm.contactsChanged == null) {
    				vm.contactsChanged = [];
    			}
    			contactsChangedService.getPagedContactsChangedByCommitted(page, false).then(function(response) {
	    			var loadedContactsToExport = response.data;
	    			if(loadedContactsToExport != null && loadedContactsToExport.length > 0) {
	    				vm.contactsChanged.push.apply(vm.contactsChanged, loadedContactsToExport);
		    			console.log("push contacts to export:" + vm.contactsChanged.length + ', ' + loadedContactsToExport.length);
	    			}
	  	   		 }, function errorCallback(response) {
	  	   			 console.log('error products.controller#loadContactsToExport');
	  	   		 });
    		}
    	}
    	
    	function communicationOrderExecuted(communicationUserConnectionOfUserAndPage) {
    		communicationService.updateCommunicationUserConnection(communicationUserConnectionOfUserAndPage).then(function(response) {
  	   		 }, function errorCallback(response) {
  	   			  console.log('error dashboard.controller#communicationOrderExecuted');
  	   		 }); 
    	}
    	
    	function executionDateAfterToday(executionDateString) {
    		if(executionDateString == null) {
    			return false;
    		}
    		var executionDate = dateUtilityService.convertToDateOrUndefined(executionDateString);
    		if(vm.todayDate.getTime() > executionDate.getTime()) {
    			return true;
    		}
    		return false;
    	}
    	
    	function getTeasingMessage(message) {
    		var teasingMessage = message.substr(0, vm.teaserMessageLength);
    		return teasingMessage;
    	}
    	 
    	function setUpdateProjectUserConnectionTimeout(projectUserConnection) {
      		$timeout(function() {
      			projectUserConnection.updatedSuccessfully = false;
  			   }, 2000); 
      	}
    	
    	function setTimeout() {
      		$timeout(function() {
      			vm.addedProjectUserConnectionSuccess = false;
      			vm.addProjectUserConnectionError = false;
      			vm.addedProductSuccess = false;
      			vm.addProductError = false;
      			vm.contactImportedConfirmed = false;
      	    	vm.contactImportedDeleted = false;
      	    	vm.contactToExportConfirmed = false;
      	    	vm.contactToExportDeleted = false;
  			   }, 3000); 
      	}

    	function setAsDefaultProject(projectUserConnection) {
    		for(var j = 0; j < vm.projectUserConnections.length; j++) {
    			if(vm.projectUserConnections[j].id != projectUserConnection.id && vm.projectUserConnections[j].defaultProject === true) {
						vm.projectUserConnections[j].defaultProject = false;
						projectUserConnectionService.updateProjectUserConnection(vm.projectUserConnections[j]).then(function(response) {
							vm.projectUserConnections[j] = response.data;
 		  	   		 }, function errorCallback(response) {
 		  	   			  console.log('error in dashboard.controller.js#setAsDefaultProject');
 		  	   		 });
				break;
				}
    		}
    		projectUserConnectionService.updateProjectUserConnection(projectUserConnection).then(function(response) {
    			projectUserConnection.updatedSuccessfully = true;
				setUpdateProjectUserConnectionTimeout(projectUserConnection);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error in dashboard.controller.js#setAsDefaultProject');
  	   		 });
    	}
    	
    	function getAmountOfDocumentFilesWithUniqueName(project) {
    		documentFileService.countByProjectIdGroupByOriginalFileName(project.id, false, 'PROJECT').then(function(response) {
    			project.amountOfDocumentFilesWithUniqueName = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error in dashboard.controller.js#getAmountOfDocumentFilesWithUniqueName');
  	   		 });
    	}
    	
    	function archiveCommunicationUserConnection(communicationUserConnection, updateType) {
    		communicationUserConnection.archived = true;
    		communicationService.updateCommunicationUserConnection(communicationUserConnection).then(function(response) {
    			if(updateType == 'SENT') {
    				removeCommunicationUserConnection(vm.communicationUserConnectionsSent, communicationUserConnection);
    			} else if(updateType == 'RECEIVED') {
    				removeCommunicationUserConnection(vm.communicationUserConnectionsReceived, communicationUserConnection);
    			}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error dashboard.controller#archiveCommunicationUserConnection');
  	   		 });
    	}
    	
    	function removeCommunicationUserConnection(communicationUserConnections, communicationUserConnection) {
    		for(var i = 0; i < communicationUserConnections.length; i++) {
    			if(communicationUserConnections[i].id == communicationUserConnection.id) {
    				communicationUserConnections.splice(i, 1);
    				break;
    			}
    		}
    	}
    
    	function showMessageReceiverOverview(communicationUserConnection, confirmationNeeded) {
    		var messageId = communicationUserConnection.communication.id;
    		communicationService.getMessageReceiverOverview(messageId).then(function(response) {
    			var communicationUserConnections = response.data;
    			messageReceiverOverviewModalService.showMessageReceiverOverviewModal(currentUser, communicationUserConnections, confirmationNeeded);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error dashboard.controller#showMessageReceiverOverview');
  	   		 });
    	}
    	
    	function getProjectUserConnectionsByTerm(term) { 
    		return projectUserConnectionService.findProjectUserConnectionsOfUserByProjectSearchString(vm.currentUser.id, term).then(function(response) {
	    		return response.data;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error dashboard.controller#getProjectsByTerm'); 
  	   		 });
		}
    	
    	function projectUserConnectionSelected(projectUserConnection) {
    		if(vm.currentUser.projectUserConnectionIds == null) {
    			vm.currentUser.projectUserConnectionIds = [];
    		}
    		if(projectUserConnectionStillExistsForUser(projectUserConnection.id) == true) {
    			vm.addProjectError = true;
    			setTimeout();
    			return;
    		}
    		vm.currentUser.projectUserConnectionIds.push(projectUserConnection.id);
    		userService.updateUser(vm.currentUser).then(function(response) {
    			vm.addedProjectUserConnectionSuccess = true;
    			vm.selectedProjectUserConnection = null;
    			setTimeout();
    			getProjectUserConnectionsOfUser();
 	   		 }, function errorCallback(response) {
 	   			 console.log('error dashboard.controller#projectUserConnectionSelected'); 
 	   		 });
    	}
    	
    	function getProductsByTerm(term) {
			return productsService.findProductsByTerm(term).then(function(response) {
	    		return response.data;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error dashboard.controller#getProductsByTerm'); 
  	   		 });
		}
    	
    	function productSelected(product) {
    		if(vm.currentUser.productIds == null) {
    			vm.currentUser.productIds = [];
    		}
    		if(productStillExistsForUser(product.id) == true) {
    			vm.addProductError = true;
    			setTimeout();
    			return;
    		}
    		vm.currentUser.productIds.push(product.id);
    		userService.updateUser(vm.currentUser).then(function(response) {
    			vm.addedProductSuccess = true;
    			vm.selectedProduct = null;
    			setTimeout();
    			getProductsOfUser();
 	   		 }, function errorCallback(response) {
 	   			 console.log('error dashboard.controller#productSelected'); 
 	   		 });
    	}
    	
    	function removeProduct(product) {
    		for(var i = 0; i < vm.currentUser.productIds.length; i++) {
    			if(vm.currentUser.productIds[i] == product.id) {
    				vm.currentUser.productIds.splice(i, 1);
    				userService.updateUser(vm.currentUser).then(function(response) {
    	    			vm.addedProductSuccess = true;
    	    			setTimeout();
    	    			getProductsOfUser();
    	 	   		 }, function errorCallback(response) {
    	 	   			 console.log('error dashboard.controller#removeProduct'); 
    	 	   		 });
    				break;
    			}
    		}
    	}
    	
    	function removeProjectUserConnection(projectUserConnection) {
    		for(var i = 0; i < vm.currentUser.projectUserConnectionIds.length; i++) {
    			if(vm.currentUser.projectUserConnectionIds[i] == projectUserConnection.id) {
    				vm.currentUser.projectUserConnectionIds.splice(i, 1);
    				userService.updateUser(vm.currentUser).then(function(response) {
    	    			vm.addedProjectUserConnectionSuccess = true;
    	    			setTimeout();
    	    			getProjectUserConnectionsOfUser();
    	 	   		 }, function errorCallback(response) {
    	 	   			 console.log('error dashboard.controller#removeProjectUserConnection'); 
    	 	   		 });
    				break;
    			}
    		}
    	}
    	
    	function projectUserConnectionStillExistsForUser(projectUserConnectionId) {
    		for(var i = 0; i < vm.currentUser.projectUserConnectionIds.length; i++) {
    			if(vm.currentUser.projectUserConnectionIds[i] == projectUserConnectionId) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function productStillExistsForUser(productId) {
    		for(var i = 0; i < vm.currentUser.productIds.length; i++) {
    			if(vm.currentUser.productIds[i] == productId) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function getProductsOfUser() {
    		productsService.getProductsOfUser(vm.currentUser.id).then(function(response) {
    			vm.productsOfUser = response.data;
 	   		 }, function errorCallback(response) {
 	   			 console.log('error dashboard.controller#getProductsOfUser'); 
 	   		 });
    	}
    	
    	function getProjectUserConnectionsOfUser() { 
    		projectUserConnectionService.findSelectedProjectUserConnectionsByUser(vm.currentUser.id).then(function(response) {
    			vm.projectUserConnections = response.data;
 	   		 }, function errorCallback(response) {
 	   			 console.log('error dashboard.controller#getProjectUserConnectionsOfUser'); 
 	   		 });
    	}
    	
    	function getProductTypeOverview() {
    		productsService.getProductTypeOverview().then(function(response) {
    			var productTypeOverviewContainers = response.data;
    			for(var i = 0; i < productTypeOverviewContainers.length; i++) {
    				$translate(productTypeOverviewContainers[i].productType).then(function(text) {
    					vm.labels.push(text);
            		});
    				vm.data.push(productTypeOverviewContainers[i].amount);
    			}
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#getProductTypeOverview'); 
	   		 });
    	}
    	
    	function showCommunicationMessageModal(communicationUserConnectionSent, type) {
    		communicationMessageService.showCommunicationMessageModal(communicationUserConnectionSent, type);
    	}
    	
    	function showYesNoModal(communicationUserConnection, type) {
    		dashboardYesNoModalService.showYesNoModal(vm, communicationUserConnection, type);
    	}
    	
    	function showAdjustmentModal(type) {
    		adjustmentModalService.showAdjustmentModal(vm.currentUser, type);
    	}
    	
    	function confirmContactImported(contactAndContactImportedContainer, index) {
    		systemImportService.confirmContactImported(contactAndContactImportedContainer).then(function(response) {
    			vm.contactAndContactImportedContainers.splice(index, 1);
    			vm.contactImportedConfirmed = true;
    			getAmountOfContacts();
    			getAmountOfContactsImported();
    			getAmountOfContactsPreviousImported();
    			setTimeout();
    			getAmountOfContactsChangedByCommitted(false, getAmountOfContactsChangedByCommittedCallback);
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#confirmContactImported'); 
	   		 });
    	}
    	
    	function getAmountOfContactsChangedByCommittedCallback(oldValue, newValue) {
			vm.contactsChanged = [];
			vm.contactsToExportPage = 0;
			loadContactsToExport(vm.contactsToExportPage);
			getAmountOfContactsChanged();
    	}
    	
    	function showContactAndContactImported(contactAndContactImportedContainer, index) {
    		contactAndContactImportedModalService.showContactAndContactImportedModal(vm, vm.currentUser, contactAndContactImportedContainer, index, vm.countryTypes, vm.titles, vm.provinceTypes, vm.contactTypes);
    	}
    	
    	function getCustomerNumbers(customerNumberContainers) {
    		var customerNumbers = '';
    		for(var i = 0; i < customerNumberContainers.length; i++) {
    			customerNumbers += customerNumberContainers[i].customerNumber + ' ';
    		}
    		return customerNumbers;
    	}
    	
    	function showDeleteContactImported(contactImported, index) {
    		contactImportedYesNoModalService.showContactImportedYesNoModal(vm, contactImported, index);
    	}
    	
    	function showDeleteContactExport(contactChanged, index) {
    		contactExportYesNoModalService.showContactExportYesNoModal(vm, contactChanged, index);
    	}
    	
    	function showContactChanged(contactChanged, index) {
    		contactChangedModalService.showContactChangedModal(vm, vm.currentUser, contactChanged, index, vm.countryTypes, vm.titles, vm.provinceTypes, vm.contactTypes);
    	}
    	
    	function commitContactChanged(contactChanged, index) {
    		contactsChangedService.commitContactChanged(contactChanged.id).then(function(response) {
    			vm.contactsChanged.splice(index, 1);
    			getAmountOfContactsChanged();
    			getAmountOfContactsChangedByCommitted(true);
    			getAmountOfContactsChangedByCommitted(false);
    			vm.contactToExportConfirmed = true;
    			setTimeout();
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#commitContactChanged'); 
	   		 });
    	}
    	
    	function deleteContactImported(contactImported, index) {
    		contactsImportedService.deleteContactImported(contactImported.id).then(function(response) {
    			vm.contactAndContactImportedContainers.splice(index, 1);
    	    	vm.contactImportedDeleted = true;
    			getAmountOfContacts();
    			getAmountOfContactsImported();
    			getAmountOfContactsPreviousImported();
    			setTimeout();
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#deleteContactImported'); 
	   		 });
    	}
    	
    	function deleteContactChanged(contactChanged, index) {
    		contactsChangedService.deleteContactChanged(contactChanged.id).then(function(response) {
    			vm.contactsChanged.splice(index, 1);
    			getAmountOfContactsChanged();
    			getAmountOfContactsChangedByCommitted(true);
    			getAmountOfContactsChangedByCommitted(false);
    			vm.contactToExportDeleted = true;
    			setTimeout();
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#deleteContactChanged'); 
	   		 });
    	}
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('adjustmentModalService', adjustmentModalService);
        
    adjustmentModalService.$inject = ['$modal', '$stateParams', 'userService'];
    
    function adjustmentModalService($modal, $stateParams, userService) {
		var service = {
			showAdjustmentModal: showAdjustmentModal
		};		
		return service;
		
		////////////
				
		function showAdjustmentModal(user, type) {
			 var adjustmentModal = $modal.open({
				controller: AdjustmentModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",  
			    resolve: {
			    	user: function() {
			    		return user;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/adjustmentModal/adjustment.modal.html'
			});
			return adjustmentModal;
			
			function AdjustmentModalController($modalInstance, $scope, user, type) {
		    	var vm = this; 
		    	vm.user = user;
		    	vm.type = type;
		    	vm.selectedColor = setSelectedColor(type);
		    	
		    	vm.closeAdjustmentModal = closeAdjustmentModal;
		    	
		    	////////////
		    	
		    	$scope.$watch('vm.selectedColor', function(newValue, oldValue) {  
		    		if(oldValue != newValue) {
		    			switch(type) {
				    		case 'PRODUCTS':
				    			vm.user.colorOfProducts = vm.selectedColor;
				    			break;
				    		case 'PROJECTS':
				    			vm.user.colorOfProjects = vm.selectedColor;
				    			break;
				    		case 'CALENDAR_EVENTS':
				    			vm.user.colorOfCalendarEvents = vm.selectedColor;
				    			break;
							case 'RECEIVED_ORDERS':
				    			vm.user.colorOfReceivedOrders = vm.selectedColor;
				    			break;
							case 'SENT_ORDERS':
								vm.user.colorOfSentOrders = vm.selectedColor;
								break;
							case 'CONTACTS':
								vm.user.colorOfContactsImported = vm.selectedColor;
								break;
				    	}
		    			
		    			userService.updateUser(user).then(function(data) {
		    				closeAdjustmentModal();
		    			}, function errorCallback(response) {
		    	   			  console.log('error in adjustment.service#watch');
		    	   		});
		    			
		    		}
				});
		    	
		    	function setSelectedColor(type) {   
		    		switch(type) {
		    		case 'PRODUCTS':
		    			vm.selectedColor = vm.user.colorOfProducts;
		    			break;
		    		case 'PROJECTS':
		    			vm.selectedColor = vm.user.colorOfProjects;
		    			break;
		    		case 'CALENDAR_EVENTS':
		    			vm.selectedColor = vm.user.colorOfCalendarEvents;
		    			break;
					case 'RECEIVED_ORDERS':
		    			vm.selectedColor = vm.user.colorOfReceivedOrders;
		    			break;
					case 'SENT_ORDERS':
						vm.selectedColor = vm.user.colorOfSentOrders;
						break;
					case 'CONTACTS':
						vm.selectedColor = vm.user.colorOfContactsImported;
						break;
		    		}
		    	}
		    	
		    	function closeAdjustmentModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('communicationMessageService', communicationMessageService);
        
    communicationMessageService.$inject = ['$modal', '$stateParams', '$timeout', 'communicationService'];
    
    function communicationMessageService($modal, $stateParams, $timeout, communicationService) {
		var service = {
			showCommunicationMessageModal: showCommunicationMessageModal
		};		
		return service;
		
		////////////
				
		function showCommunicationMessageModal(communicationUserConnectionSent, type) {
			 var communicationMessageModal = $modal.open({
				controller: CommunicationMessageModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	communicationUserConnectionSent: function() {
			    		return communicationUserConnectionSent;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/communicationMessage/communicationMessage.modal.html'
			});
			return communicationMessageModal;
			
			function CommunicationMessageModalController($modalInstance, $scope, communicationUserConnectionSent, type) {
		    	var vm = this; 
		    	vm.communicationUserConnectionSent = communicationUserConnectionSent;
		    	vm.type = type;
		    	vm.readUpdateSuccessful = false;
		    	vm.executedUpdateSuccessful = false;
		    	
		    	vm.updateCommunicationUserConnection = updateCommunicationUserConnection;
		    	vm.closeCommunicationMessageModal = closeCommunicationMessageModal;
		    	
		    	////////////
		    	
		    	function updateCommunicationUserConnection(communicationUserConnection, updateType) {
		    		communicationService.updateCommunicationUserConnection(communicationUserConnection).then(function successCallback(response) {
		    			if(updateType == 'READ') {
		    				vm.readUpdateSuccessful = true;
		    			} else if(updateType == 'EXECUTED') {
		    				vm.executedUpdateSuccessful = true;
		    			}
		    			setUpdateCommunicationUserConnectionTimeout();
		    			console.log('update of communicationUserConnection successful in communicationMessage.service.js#updateCommunicationUserConnection');
		  	   		 }, function errorCallback(response) {
		  	   			  console.log('error in communicationMessage.service.js#updateCommunicationUserConnection');
		  	   		 });
		    	}
		    	
		    	function setUpdateCommunicationUserConnectionTimeout() {
		      		$timeout(function() {
		      			vm.readUpdateSuccessful = false;
		      			vm.executedUpdateSuccessful = false;
		  			   }, 2000); 
		      	}
		    	
		    	function closeCommunicationMessageModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactAndContactImportedModalService', contactAndContactImportedModalService);
        
    contactAndContactImportedModalService.$inject = ['$modal', '$stateParams', 'printingPDFService'];
    
    function contactAndContactImportedModalService($modal, $stateParams, printingPDFService) {
		var service = {
			showContactAndContactImportedModal: showContactAndContactImportedModal
		};		
		return service;
		
		////////////
				
		function showContactAndContactImportedModal(invoker, currentUser, contactAndContactImportedContainer, index, countryTypes, titles, provinceTypes, contactTypes) {
			 var contactAndContactImportedModal = $modal.open({
				controller: ContactAndContactImportedModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",  
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	contactAndContactImportedContainer: function() {
			    		return contactAndContactImportedContainer;
			    	},
			    	index: function() {
			    		return index;
			    	},
			    	countryTypes: function() {
			    		return countryTypes;
			    	},
			    	titles: function() {
			    		return titles;
			    	},
			    	provinceTypes: function() {
			    		return provinceTypes;
			    	},
			    	contactTypes: function() {
			    		return contactTypes;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/contactAndContactImportedModal/contactAndContactImported.modal.html'
			});
			return contactAndContactImportedModal;
			
			function ContactAndContactImportedModalController($modalInstance, $scope, invoker, currentUser, contactAndContactImportedContainer, index, countryTypes, titles, provinceTypes, contactTypes) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.currentUser = currentUser;
		    	vm.contactAndContactImportedContainer = contactAndContactImportedContainer;
		    	vm.index = index;
		    	vm.countryTypes = countryTypes;
		    	vm.titles = titles;
		    	vm.provinceTypes = provinceTypes;
		    	vm.contactTypes = contactTypes;
		    	vm.date = new Date();
		    	
		    	vm.printContainer = printContainer;
		    	vm.confirmContactImported = confirmContactImported;
		    	vm.closeContactAndContactImportedModal = closeContactAndContactImportedModal;
		    	
		    	////////////
		    	
		    	function printContainer(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	}
		    	
		    	function confirmContactImported() {
		    		vm.invoker.confirmContactImported(vm.contactAndContactImportedContainer, vm.index);
		    		vm.closeContactAndContactImportedModal();
		    	}
		    	
		    	function closeContactAndContactImportedModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactChangedModalService', contactChangedModalService);
        
    contactChangedModalService.$inject = ['$modal', '$stateParams'];
    
    function contactChangedModalService($modal, $stateParams) {
		var service = {
			showContactChangedModal: showContactChangedModal
		};		
		return service;
		
		////////////
				
		function showContactChangedModal(invoker, currentUser, contactChanged, index, countryTypes, titles, provinceTypes, contactTypes) {
			 var contactChangedModal = $modal.open({
				controller: ContactChangedModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",  
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	contactChanged: function() {
			    		return contactChanged;
			    	},
			    	index: function() {
			    		return index;
			    	},
			    	countryTypes: function() {
			    		return countryTypes;
			    	},
			    	titles: function() {
			    		return titles;
			    	},
			    	provinceTypes: function() {
			    		return provinceTypes;
			    	},
			    	contactTypes: function() {
			    		return contactTypes;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/contactChangedModal/contactChanged.modal.html'
			});
			return contactChangedModal;
			
			function ContactChangedModalController($modalInstance, $scope, invoker, currentUser, contactChanged, index, countryTypes, titles, provinceTypes, contactTypes) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.currentUser = currentUser;
		    	vm.contactChanged = contactChanged;
		    	vm.index = index;
		    	vm.countryTypes = countryTypes;
		    	vm.titles = titles;
		    	vm.provinceTypes = provinceTypes;
		    	vm.contactTypes = contactTypes;
		    	vm.date = new Date();
		    	
		    	vm.confirmContactChanged = confirmContactChanged;
		    	vm.closeContactChangedModal = closeContactChangedModal;
		    	
		    	////////////
		    	
		    	function confirmContactChanged() {
		    		vm.invoker.commitContactChanged(vm.contactChanged, vm.index);
		    		vm.closeContactChangedModal();
		    	}
		    	
		    	function closeContactChangedModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactExportYesNoModalService', contactExportYesNoModalService);
        
    contactExportYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function contactExportYesNoModalService($modal, $stateParams) {
		var service = {
			showContactExportYesNoModal: showContactExportYesNoModal
		};		
		return service;
		
		////////////
				
		function showContactExportYesNoModal(invoker, contactChanged, index) {
			 var yesNoModal = $modal.open({
				controller: ContactExportYesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contactChanged: function() {
			    		return contactChanged;
			    	},
			    	index: function() {
			    		return index;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/contactExportYesNoModal/contactExportYesNo.modal.html'
			});
			return yesNoModal;
			
			function ContactExportYesNoModalController($modalInstance, $scope, invoker, contactChanged, index) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contactChanged = contactChanged;
		    	vm.index = index;
		    	
		    	vm.deleteContactExportYesNoModal = deleteContactExportYesNoModal;
		    	vm.closeContactExportYesNoModal = closeContactExportYesNoModal;
		    	
		    	////////////
		    	
		    	function deleteContactExportYesNoModal() {   
		    		invoker.deleteContactChanged(vm.contactChanged, vm.index);
		    		vm.closeContactExportYesNoModal();
		    	}
		    	
		    	function closeContactExportYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactImportedYesNoModalService', contactImportedYesNoModalService);
        
    contactImportedYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function contactImportedYesNoModalService($modal, $stateParams) {
		var service = {
			showContactImportedYesNoModal: showContactImportedYesNoModal
		};		
		return service;
		
		////////////
				
		function showContactImportedYesNoModal(invoker, contactImported, index) {
			 var yesNoModal = $modal.open({
				controller: ContactImportedYesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contactImported: function() {
			    		return contactImported;
			    	},
			    	index: function() {
			    		return index;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/contactImportedYesNoModal/contactImportedYesNo.modal.html'
			});
			return yesNoModal;
			
			function ContactImportedYesNoModalController($modalInstance, $scope, invoker, contactImported, index) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contactImported = contactImported;
		    	vm.index = index;
		    	
		    	vm.deleteContactImportedYesNoModal = deleteContactImportedYesNoModal;
		    	vm.closeContactImportedYesNoModal = closeContactImportedYesNoModal;
		    	
		    	////////////
		    	
		    	function deleteContactImportedYesNoModal() {   
		    		invoker.deleteContactImported(vm.contactImported, vm.index);
		    		vm.closeContactImportedYesNoModal();
		    	}
		    	
		    	function closeContactImportedYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('dashboardYesNoModalService', dashboardYesNoModalService);
        
    dashboardYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function dashboardYesNoModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(vm, communicationUserConnection, type) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return vm;
			    	},
			    	communicationUserConnection: function() {
			    		return communicationUserConnection;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, communicationUserConnection, type) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.communicationUserConnection = communicationUserConnection;
		    	vm.type = type;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		invoker.archiveCommunicationUserConnection(communicationUserConnection, type);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.error')
    	.config(configure);
    			
    configure.$inject = ['$httpProvider'];
    
	function configure($httpProvider) {
		$httpProvider.interceptors.push(errorInterceptor);
		
		////////////
		
		function errorInterceptor($injector, $q, $location) {
			var interceptor = {
					responseError: responseError
			};
			
			return interceptor;
			
			////////////
						
			function responseError(response) {
				switch(response.status) {
				case 401:
					console.log('HTTP 401 error occured and user will redirected to signin page in error.config.js#responseError');
					$location.path('/signin');
					break;
				case 403:
					console.log('HTTP 403 error occured and user will redirected to signin page in error.config.js#responseError');
					$location.path('/signin');
					break;
				case 400:
					console.log('HTTP 400 error occured in error.config.js#responseError');
					break;
				default:
					console.log('a general error occured in error.config.js#responseError: ' + response.message);
					break;
				}
				return $q.reject(response);
			}
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.error')
    	.controller('ErrorController', ErrorController);
    
    ErrorController.$inject = ['$scope', '$stateParams', '$window'];
    
    function ErrorController($scope, $stateParams, $window) {
	    $scope.vm = this;
    	var vm = this;
    	
    	vm.error = $stateParams.error;
    	vm.lastState = $stateParams.lastState;
    	vm.goBack = goBack;
    	
    	function goBack(){
    		$window.history.back();
    	}
    }
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.error')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	$stateProvider
	    	.state(getErrorState());
    	
    	////////////
			    	
		function getErrorState() {
			var state = {
				name: 'error',
				params: {
					'error' : 'undefinied',
					'lastState' : 'undefinied'
				},
				url: '/error',
				views: {
					'header': {},
					'content': {
						controller: 'ErrorController',
						controllerAs: 'vm',
						templateUrl: 'app/error/error.html'
					},
					'footer': {}
				}
			};	
			return state;
		}
	}
})();
(function() {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .factory('fuelService', fuelService);

    fuelService.$inject = ['$http', 'api_config'];

    function fuelService($http, api_config) {

        var FUEL_URL= api_config.BASE_URL + '/fuel';

        var service = {
            saveFuel: saveFuel,
            findAll: findAll,
            findPagedFuels: findPagedFuels,
            calculateAmountOfPages: calculateAmountOfPages,
            deleteFuel: deleteFuel,
            getFuelPageSize: getFuelPageSize,
            findSupplierContacts: findSupplierContacts,
        };

        return service;

        ////////////

        function saveFuel(fuel) {
            return $http.post(FUEL_URL, fuel);
        }


        function findAll() {
            return $http.get(FUEL_URL);
        }

        function findPagedFuels(page) {
            return $http.get(FUEL_URL + '/page/' + page);
        }

        function calculateAmountOfPages() {
            return $http.get(FUEL_URL + '/amountofpages');
        }

        function deleteFuel(fuelId) {
            return $http.delete(FUEL_URL+ '/' + fuelId);
        }

        function getFuelPageSize() {
            return $http.get(FUEL_URL + '/fuelpagesize');
        }

        function findSupplierContacts() {
            return $http.get(api_config.BASE_URL + '/contacts/suppliers')
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .config(configure);

    configure.$inject = ['$stateProvider'];

    function configure($stateProvider) {

        $stateProvider
            .state(getFuelState());

        ////////////

        function getFuelState() {
            var state = {
                name: 'auth.fuel',
                url: '/fuel/:userId',
                templateUrl: 'app/fuel/fuel/fuel.html',
                controller: 'FuelController',
                controllerAs: 'vm',
                resolve: {
                    fuelService: 'fuelService',
                    fuelList: function (fuelService) {
                        return fuelService.findPagedFuels(0);
                    },
                    amountOfPages: function (fuelService) {
                        return fuelService.calculateAmountOfPages();
                    },
                    fuelPageSize: function (fuelService) {
                        return fuelService.getFuelPageSize();
                    }
                }
            };
            return state;
        }
    }
})();

(function () {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .directive('editFuelDirective', editFuelDirective);

    editFuelDirective.$inject = ['$timeout', 'dateUtilityService', 'utilService', 'fuelService', 'optionsService'];

    function editFuelDirective($timeout, dateUtilityService, utilService, fuelService, optionsService) {
        var directive = {
            restrict: 'E',
            scope: {
                fuel: '=',
                locations: '=',
                articles: '=',
                invoker: '=',
                modalInvoker: '=',
                type: '=',
                currentUser: '=',
                roles: '=',
                disabled: '='
            },
            templateUrl: 'app/fuel/directives/editFuelDirective/editFuel.html',
            link: function ($scope) {

                $scope.selected = undefined;
                $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

                $scope.suppliers = [];

                fuelService.findSupplierContacts().then(result => {
                    console.log('findSupplierContacts', result);
                    $scope.suppliers = result.data;
                })

                $scope.deliveryDateDatePickerOpen = false;
                if (!$scope.fuel) {
                    $scope.fuel = {};
                }
                if (!$scope.fuel.deliveryDate) {
                    $scope.fuel.deliveryDate = new Date();
                }

                $scope.openDeliveryDateDatePicker = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.deliveryDateDatePickerOpen = true;
                };

                $scope.saveFuel = function () {
                    $scope.invoker.saveFuel($scope.fuel).then(function() {
                        $scope.modalInvoker.closeEditFuelModal();
                    });
                };

            }
        };
        return directive;

        ////////////
    }
})();

(function () {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .controller('FuelController', FuelController);


    FuelController.$inject = ['$scope', '$timeout', '$window', '$location', '$anchorScroll', 'currentUser', 'fuelService', 'fuelList', 'amountOfPages', 'fuelPageSize', 'editFuelModalService', 'yesNoFuelService'];


    function FuelController($scope, $timeout, $window, $location, $anchorScroll, currentUser, fuelService, fuelList, amountOfPages, fuelPageSize, editFuelModalService, yesNoFuelService) {

        $scope.vm = this;
        var vm = this;

        // TODO handle it
        vm.loadingInProgress = false;

        vm.searchTrigger = false;
        vm.fuelList = fuelList.data;
        vm.fuelPageSize = fuelPageSize.data;
        vm.amountOfPages = amountOfPages;
        vm.currentPage = 0;
        vm.currentUser = currentUser;
        // vm.userAuthorizationUserContainer = vm.currentUser.userAuthorizationUserContainer;
        vm.showFuelForm = false;
        vm.showEditFuelModal = showEditFuelModal;
        vm.printContainer = printContainer;
        vm.gotoTop = gotoTop;
        vm.saveFuel = saveFuel;
        vm.deleteFuel = deleteFuel;
        vm.searchFuels = searchFuels;
        vm.markFuelAsPaid = markFuelAsPaid;
        vm.showYesNoDeleteModal = showYesNoDeleteModal;
        vm.pageChange = pageChange;


        //////////////
        /*    $scope.$watch('vm.orderType', function(newValue, oldValue) {
                if(oldValue != newValue) {
                    removeSearchResults();
                }
            });
        */


        function errorCallback(response) {
            vm.failedUploadedFiles += 1;
        }


        function setTimeout() {
            $timeout(function () {
                vm.noLastReceipeExists = false;
            }, 2000);
        }

        function showEditFuelModal(fuel) {

            editFuelModalService.showEditFuelModal(fuel, vm, vm.currentUser, vm.roles, vm.usersOfProject);
        }

        function markFuelAsPaid(fuel) {
            if (!fuel.paid) {
                fuel.paid = true;
                vm.saveFuel(fuel);
            }
        }

        function printContainer(container, name) {
            printingPDFService.printSchedule(container, name);
        }

        function pageChange(page) {
            if (vm.currentPage != page) {
                vm.loadingInProgress = true;
                fuelService.findPagedFuels(page).then(function (result) {
                    vm.fuelList = result.data;
                }).finally(function () {
                    vm.loadingInProgress = false;
                    vm.currentPage = page;
                });
            }
        }


        function saveFuel(fuel) {
            var isCreate = !fuel.id;
            return fuelService.saveFuel(fuel).then(function () {
                if (isCreate) {
                    updateList();
                }
            });
        }

        function updateList() {
            fuelService.calculateAmountOfPages().then(function (result) {
                vm.amountOfPages = result.data;
            });

            fuelService.findPagedFuels(vm.currentPage).then(function (result) {
                vm.fuelList = result.data;
            });
        }

        function showYesNoDeleteModal(fuel) {
            var detailContentHmtl = 'Möchten Sie die Anlieferung mit dem Lieferschein "<b>' + fuel.deliveryNote + '</b>" löschen?';
            yesNoFuelService.showYesNoModal(this, 'Brennstoff Anlieferung <b>löschen</b>', detailContentHmtl, function () {
                console.log('!!!!YES called');
                deleteFuel(fuel)
            })
        }

        function deleteFuel(fuel) {
            fuelService.deleteFuel(fuel.id).then(function (response) {
                vm.updateList();
            }, function errorCallback(response) {
                console.log('error fuel.controller#deleteFuel');
            });
        }

        function removeFuel(fuel) {
            for (var i = 0; i < vm.fuel_list.length; i++) {
                if (vm.fuel_list[i].id == fuel.id) {
                    vm.fuel_list.splice(i, 1);
                    break;
                }
            }
        }

        function searchFuels() {
            console.log('Search not implemented yet');
        }

        function gotoTop() {
            $location.hash('top');
            $anchorScroll();
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .factory('editFuelModalService', editFuelModalService);

    editFuelModalService.$inject = ['$modal', '$stateParams'];

    function editFuelModalService($modal, $stateParams) {
        var service = {
            showEditFuelModal: showEditFuelModal
        };
        return service;

        ////////////

        function showEditFuelModal(fuel, invoker, currentUser, roles, usersOfProject) {
            var editFuelModal = $modal.open({
                controller: EditFuelModalController,
                controllerAs: 'vm',
                size: 'lg',
                windowClass: "modal fade in",
                resolve: {
                    fuel: function() {
                        return fuel;
                    },
                    invoker: function() {
                        return invoker;
                    },
                    currentUser: function() {
                        return currentUser;
                    },
                    roles: function() {
                        return roles;
                    },
                    usersOfProject: function() {
                        return usersOfProject;
                    }
                },
                templateUrl: 'app/fuel/modals/editFuel/editFuel.modal.html'
            });
            return editFuelModal;

            function EditFuelModalController($modalInstance, $scope, fuel, invoker, currentUser, roles, usersOfProject) {
                var vm = this;
                vm.fuel = fuel;
                vm.invoker = invoker;
                vm.currentUser = currentUser;
                vm.roles = roles;
                vm.usersOfProject = usersOfProject;

                vm.closeEditFuelModal = closeEditFuelModal;

                ////////////

                function closeEditFuelModal() {
                    $modalInstance.dismiss('cancel');
                }
            }
        }
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.fuel')
    	.factory('yesNoFuelService', yesNoFuelModalService);
        
    yesNoFuelModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoFuelModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, headerContent, detailContent, yesFunction) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	headerContent: function() {
			    		return headerContent;
			    	},
					detailContent: function() {
						return detailContent;
					},
					yesFunction: function() {
						return yesFunction;
					}
			    }, 
				templateUrl: 'app/fuel/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, headerContent, detailContent, yesFunction) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.headerContent = headerContent;
		    	vm.detailContent = detailContent;
		    	vm.yesFunction = yesFunction;

		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	function yes() {
		    		vm.yesFunction();
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.history')
    	.factory('historyService', historyService);
        
    historyService.$inject = ['$http', 'api_config'];
    
    function historyService($http, api_config) {
		var service = {
			findHistoryBetweenDates: findHistoryBetweenDates,
			findPagedHistoryBetweenDates: findPagedHistoryBetweenDates,
			getHistoryPageSize: getHistoryPageSize,
			calculateAmountOfPages: calculateAmountOfPages,
			sendHistoryMessages: sendHistoryMessages
		};
		
		return service;
		
		////////////
		
		function findHistoryBetweenDates(startDate, endDate) {
			return $http.get(api_config.BASE_URL + '/histories/history/' + startDate + '/' + endDate + '/');
		}
		
		function findPagedHistoryBetweenDates(startDate, endDate, page) {
			return $http.get(api_config.BASE_URL + '/histories/history/' + startDate + '/' + endDate + '/' + page);
		}
		
		function getHistoryPageSize() {
			return $http.get(api_config.BASE_URL + '/histories/history/historypagesize');
		}
		
		function calculateAmountOfPages(start, end) {
			return $http.get(api_config.BASE_URL + '/histories/history/amountofpages/' + start + '/' + end + '/');
		}
		
		function sendHistoryMessages() {
			return $http.get(api_config.BASE_URL + '/histories/history/send');
		}
    }
})();

(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.history')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getHistoryState());
    	 
    	////////////
			    	
    	function getHistoryState() {
    		var state = {
    			name: 'auth.history',
				url: '/history/:userId',
				templateUrl: 'app/history/history/history.html',
				controller: 'HistoryController',
				controllerAs: 'vm',
				resolve: {
					historyService: 'historyService',
					histories: function getHistories(historyService, dateUtilityService) {
						var startDate = new Date();
						startDate.setHours(0,0,0,0);
						startDate.setDate(startDate.getDate()-1);
						var endDate = new Date();
						var formattedStartDate = dateUtilityService.formatDateToString(startDate);
						var formattedEndDate = dateUtilityService.formatDateToString(endDate);
						return historyService.findPagedHistoryBetweenDates(formattedStartDate, formattedEndDate, 0);
					},
					startDate: function getStartDate() {
						var startDate = new Date();
						startDate.setHours(0,0,0,0);
						startDate.setDate(startDate.getDate()-1);
						return startDate;
					},
					endDate: function getStartDate() {
						var endDate = new Date();
						return endDate;
					},
					historyPageSize: function getHistoryPageSize(historyService) {
						return historyService.getHistoryPageSize();
					},
					amountOfPages: function calculateAmountOfPages(historyService, dateUtilityService) {
						var startDate = new Date();
						startDate.setHours(0,0,0,0);
						startDate.setDate(startDate.getDate()-1);
						var endDate = new Date();
						var formattedStartDate = dateUtilityService.formatDateToString(startDate);
						var formattedEndDate = dateUtilityService.formatDateToString(endDate);
						return historyService.calculateAmountOfPages(formattedStartDate, formattedEndDate);
					} 
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    
    angular
    	.module('legalprojectmanagement.history')
    	.controller('HistoryController', HistoryController);
    
    HistoryController.$inject = ['$scope', 'histories', 'startDate', 'endDate', 'historyPageSize', 'amountOfPages', 'historyService', 'dateUtilityService', 'printingPDFService'];
       
    function HistoryController($scope, histories, startDate, endDate, historyPageSize, amountOfPages, historyService, dateUtilityService, printingPDFService) {
	    $scope.vm = this;  
    	var vm = this;
    	
    	vm.histories = histories.data;
    	vm.historyPageSize = historyPageSize.data;
    	vm.amountOfPages = amountOfPages.data;
    	vm.historyFromTrigger = false;
    	vm.historyUntilTrigger = false;
    	vm.historyFrom = startDate;
    	vm.historyUntil = endDate;
    	vm.historyError = false;
    	vm.currentPage = 0;
    	
    	vm.printContainer = printContainer;
    	vm.pageChange = pageChange;
    	vm.isCurrentPage = isCurrentPage;
    	vm.arrayRang = arrayRang;
    	vm.openHistoryFrom = openHistoryFrom;
    	vm.openHistoryUntil = openHistoryUntil;
    	vm.getHistory = getHistory;
    	vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    	
    	////////////
    	
    	$scope.$watch('vm.historyFrom', function(newValue, oldValue) {
    		if(oldValue != newValue && newValue != null) {
    			if(vm.historyFrom != null && vm.historyUntil != null) {
    				getHistory();
    			}
    		}
		});
    	
    	$scope.$watch('vm.historyUntil', function(newValue, oldValue) {
    		if(oldValue != newValue && newValue != null) {
    			if(vm.historyFrom != null && vm.historyUntil != null) {
    				getHistory();
    			}
    		}
		});
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function openHistoryFrom($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.historyFromTrigger = true;
		}
    	
    	function openHistoryUntil($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.historyUntilTrigger = true;
		}
    	
    	function isCurrentPage(indexHistory) {
    		if(indexHistory == (vm.currentPage+1)) {
    			return true;
    		}
    		return false; 
    	}
    	
    	function pageChange(page) {
    		if(page < 0 || page > vm.amountOfPagesArray.length-1) {
    			return;
    		}
    		var formattedDateFrom = dateUtilityService.formatDateToString(vm.historyFrom);
    		var formattedDateUntil = dateUtilityService.formatDateToString(vm.historyUntil);
    		historyService.findPagedHistoryBetweenDates(formattedDateFrom, formattedDateUntil, page).then(function successCallback(response) {
    			vm.histories = response.data;
    			vm.currentPage = page;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error history.controller.js#pageChange');
  	   		 });
    	}
    	
    	function getHistory() {
    		if(vm.historyFrom == null || vm.historyUntil == null) {
    			vm.historyError = true;
    			return;
    		}
    		vm.currentPage = 0;
    		vm.historyError = false;
    		var formattedDateFrom = dateUtilityService.formatDateToString(vm.historyFrom);
    		var formattedDateUntil = dateUtilityService.formatDateToString(vm.historyUntil);
    		historyService.calculateAmountOfPages(formattedDateFrom, formattedDateUntil).then(function (response) {
    			vm.amountOfPages = response.data;
    			vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    		}).catch(function (data) {
				console.log('error history.controller#calculateAmountOfPages: ' + data);
			});
    		historyService.findPagedHistoryBetweenDates(formattedDateFrom, formattedDateUntil, 0).then(function (response) {
    			vm.histories = response.data;
    		}).catch(function (data) {
				console.log('error history.controller#findPagedHistoryBetweenDates: ' + data);
			});
    	}
    	
    	function arrayRang(min, max, step) {
    	    step = step || 1;
    	    var input = [];
    	    for (var i = min; i <= max; i += step) {
    	        input.push(i);
    	    }
    	    return input;
    	}
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.config(configure)
    	.run(['$state', function ($state) {}]); //TODO use reoutehelperprovider see styleguide ?
    
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];
    
    function configure($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/signin');
    	
    	$stateProvider
    		.state(getHomeState())
    		.state(getSigninState())
    		.state(getSignupState())
    		.state(getMisrememberPasswordState());
    	
		////////////
		
		function getHomeState() {
			var state = {
				name: 'home',
				abstract: true,
				views: {
					'header': {},
					'content': {
						templateUrl: 'app/home/core/home.html'
					},
					'footer': {}
				}
			};
			return state;
		}
		
    	function getSigninState() {
    		var state = {
    			name: 'home.signin',
				url: '/signin',
				templateUrl: 'app/home/signin/signin.html',
				controller: 'SigninController',
				controllerAs: 'vm'
    		};
    		return state;
    	}
    	
    	function getSignupState() {
    		var state = {
    			name: 'home.signup',
				url: '/signup',
				templateUrl: 'app/home/signup/signup.html',
				controller: 'SignupController',
				controllerAs: 'vm'
    		};
    		return state;
    	}
    	
    	function getMisrememberPasswordState() {
    		var state = {
    			name: 'home.misrememberPassword',
				url: '/misremember-password',
				templateUrl: 'app/home/misremember-password/misremember-password.html',
				controller: 'MisrememberPasswordController',
				controllerAs: 'vm'
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.directive('registrationDirective', registrationDirective);
    
    registrationDirective.$inject = ['$timeout'];
    
	function registrationDirective($timeout) {
		var directive = {
			restrict: 'E',
			scope: {
				invoker: '=',
				mandator: '='
			},
			templateUrl: 'app/home/directives/registrationDirective/registration.html',
			link: function($scope) {
				
				$scope.signupError = {};
				
				$scope.user = {
						username: '',
						password: '',
						title: '',
						firstname: '',
						surname: '',
						sex: 'FEMALE',
						birthdate: new Date(),
						email: '',
						confession: 'ROEM_CATH',
						telephone: '', 
						mandators: [],
						address: {
							country: 'austria',
							postalCode: '',
							region: '',
							street: ''
						}
					};
				$scope.openedDatePicker = false;
				
				$scope.unsetUserValues = function() {
					$scope.user.username = '';
					$scope.user.password = '';
					$scope.user.title = '';
					$scope.user.firstname = '';
					$scope.user.surname = '';
					$scope.user.tenant.name = '';
					$scope.user.sex = 'FEMALE';
					$scope.user.email = '';
					$scope.user.telephone = '';
					$scope.user.address.country = 'austria';  
					$scope.user.address.postalCode = ''; 
					$scope.user.address.region = ''; 
					$scope.user.address.street = ''; 
				};
				
				$scope.signup = function(user) {
					$scope.openedDatePicker = false;
					if($scope.validateUserObject(user) === false) {
						return;
					}
					$scope.invoker.signup(user, function() {
						$scope.unsetUserValues();
					}, function(response) {
						$scope.signupError.message = response.data.error;
						$scope.setTimeout();
					});
				};
				
				$scope.openDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
				};
				
				$scope.setTimeout = function() {
		      		$timeout(function() {
		      			$scope.usernameError = false;
		      			$scope.passwordError = false;
		      			$scope.firstnameError = false;
		      			$scope.surnameError = false;
		      			$scope.emailError = false;
		      			$scope.tenantError = false;
		      			$scope.signupError = {};
		  			   }, 2000); 
		      	};
				
				$scope.validateUserObject = function(user) { 
					if(user.username == null || user.username === '') {
						$scope.usernameError = true;
						$scope.setTimeout();
						return false;
					} else if(user.password == null || user.password === '') {
						$scope.passwordError = true;
						$scope.setTimeout();
						return false;
					} else if(user.firstname == null || user.firstname === '') {
						$scope.firstnameError = true;
						$scope.setTimeout();
						return false;
					} else if(user.surname == null || user.surname === '') {
						$scope.surnameError = true;
						$scope.setTimeout();
						return false;
					} else if(user.tenant == null) { 
						$scope.tenantError = true; 
						$scope.setTimeout();
						return false;
					}
					return true;
				};
			 }
		};
		return directive;
		
		////////////
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.controller('MisrememberPasswordController', MisrememberPasswordController);
    
    MisrememberPasswordController.$inject = ['$scope', '$state', 'authUserService'];
    
    function MisrememberPasswordController($scope, $state, authUserService) {
	    $scope.vm = this;
    	var vm = this;
    	
		vm.misrememberPassword = misrememberPassword;
		
		////////////
			
		function misrememberPassword(emailAddress) {
			authUserService.misrememberPassword(emailAddress).then(function () {
				$state.go('home.signin', {});
			}).catch(function (response) {
				vm.misrememberPasswordError = response.data;
			});
		}
	}
    
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.controller('SigninController', SigninController);
    
    SigninController.$inject = ['$scope', '$state', 'authUserService'];
    
    function SigninController($scope, $state, authUserService) {
	    $scope.vm = this;
    	var vm = this;
    	
    	vm.navigator = navigator;
    	vm.objappVersion = navigator.appVersion; 
    	vm.objAgent = navigator.userAgent; 
    	vm.objbrowserName = navigator.appName; 
    	vm.objfullVersion = ''+parseFloat(navigator.appVersion); 
    	vm.objBrMajorVersion = parseInt(navigator.appVersion,10); 
    	vm.objOffsetName = null;
    	vm.objOffsetVersion = null;
    	vm.ix = null; 
    	
    	vm.user = {
    		username: '',
    		password: ''
    	};
    	
		vm.signin = signin;
		detectBrowser();
		
		////////////
			
		function signin(user) {
			authUserService.signin(user.username, user.password).then(function (response) {
				$state.go('auth.dashboard', {userId : response.data.user.id});
			}).catch(function (response) {
				vm.signinError = response.data;
			});
		}
		
		function detectBrowser() {
			if ((vm.objOffsetVersion=vm.objAgent.indexOf("Chrome"))!=-1) { 
				vm.objbrowserName = "Chrome"; 
				vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+7); 
	    	} 
	    	else if ((vm.objOffsetVersion=vm.objAgent.indexOf("MSIE"))!=-1) { 
	    		vm.objbrowserName = "Microsoft Internet Explorer"; 
	    		vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+5); 
	    	} 
	    	else if ((vm.objOffsetVersion=vm.objAgent.indexOf("Firefox"))!=-1) { 
	    		vm.objbrowserName = "Firefox"; 
	    	} 
	    	else if ((vm.objOffsetVersion=vm.objAgent.indexOf("Safari"))!=-1) { 
	    		vm.objbrowserName = "Safari"; 
	    		vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+7); 
	    		if ((vm.objOffsetVersion=vm.objAgent.indexOf("Version"))!=-1) {
	    			vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+8);
	    		}
	    	} 
	    	else if ((vm.objOffsetVersion=vm.objAgent.indexOf("Opera"))!=-1) { 
	    		vm.objbrowserName = "Opera"; 
	    		vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+7); 
	    		if ((vm.objOffsetVersion=vm.objAgent.indexOf("Version"))!=-1) {
	    			vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+8);
	    		}
	    	} 
	    	// For other browser "name/version" is at the end of userAgent 
	    	else if ( (vm.objOffsetName=vm.objAgent.lastIndexOf(' ')+1) < (vm.objOffsetVersion=vm.objAgent.lastIndexOf('/')) ) { 
	    		vm.objbrowserName = vm.objAgent.substring(vm.objOffsetName,vm.objOffsetVersion); 
	    		vm.objfullVersion = vm.objAgent.substring(vm.objOffsetVersion+1); 
	    		if (vm.objbrowserName.toLowerCase()==vm.objbrowserName.toUpperCase()) { 
	    			vm.objbrowserName = navigator.appName;
	    			} 
	    		} 
	    	// trimming the fullVersion string at semicolon/space if present 
	    	if ((vm.ix=vm.objfullVersion.indexOf(";"))!=-1) {
	    		vm.objfullVersion=vm.objfullVersion.substring(0,vm.ix);
	    	}
	    	if ((vm.ix=vm.objfullVersion.indexOf(" "))!=-1) { 
	    		vm.objfullVersion=vm.objfullVersion.substring(0,vm.ix);
	    	}
	    	vm.objBrMajorVersion = parseInt(''+vm.objfullVersion,10); 
	    	if (isNaN(vm.objBrMajorVersion)) { 
	    		vm.objfullVersion = ''+parseFloat(navigator.appVersion); 
	    		vm.objBrMajorVersion = parseInt(navigator.appVersion,10); } 
		}
	}
    
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.controller('SignupController', SignupController);
    
    SignupController.$inject = ['$scope', '$state', 'authUserService'];
    
    function SignupController($scope, $state, authUserService) {
	    $scope.vm = this;
    	var vm = this;
    	
    	vm.signupError = null;
    	
    	vm.user = {
			username: '',
			password: '',
			title: 'Mag.',
			firstname: '',
			surname: '',
			sex: 'MALE',
			birthdate: new Date(),
    		email: '',
    		confession: 'ROEM_CATH',
    		telephone: '',
    		tenant: {
    			name: ''
    		},
    		address: {
    			country: 'austria',
    			postalCode: '',
    			region: '',
    			street: ''
    		}
		};
    	vm.openedDatePicker = false;
    	
    	vm.signup = signup;
    	
    	////////////
    	
    	function signup(newUser) {  
			newUser.active = true;
			authUserService.signup(newUser).then(function () {
				authUserService.signin(newUser.username, newUser.password).then(function(response) {
					var createdUserId = response.data.user.id;
					$state.go('auth.dashboard',{userId:createdUserId});
				});
			}).catch(function (response) {
				vm.signupError = response.data.errorCodeType;
				console.log("error in signup.controller#signup: " + response.data.message);
			});
		}
    }    
})();

(function() {
	'use strict';

	angular
		.module('legalprojectmanagement.lang')
		.config(configure);

	configure.$inject =['$translateProvider'];

	function configure($translateProvider) {
		$translateProvider.useStaticFilesLoader({
			files: [{
				prefix: 'app/lang/i18n/lang-',
				suffix: '.json'
			}]
		});
		
		$translateProvider.preferredLanguage('de_DE');
		$translateProvider.useSanitizeValueStrategy('escape');
		$translateProvider.fallbackLanguage('de_DE');
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.nav')
    	.directive('asUserNavPanel', asUserNavPanel);
    					
	function asUserNavPanel() {
		var directive = {
			restrict: 'E',
			replace: true,
			scope: {
				asUser: '='
			},
			controller: UserNavPanelController,
			controllerAs: 'vm',
			bindToController: true,
			templateUrl: 'app/nav/as-user-nav-panel/as-user-nav-panel.directive.html'
		};
		
		return directive;
	}
	
	UserNavPanelController.$inject = ['$scope', 'userService', '$state'];
	
	function UserNavPanelController($scope, userService, $state) {
		$scope.vm = this;
		var vm = this;

		vm.user = vm.asUser;
		vm.logout = logout;
		vm.imageUrl = 'api/files/profileimage/' + vm.user.profileImagePath + '/';
		
		////////////
		
		function logout() {
			userService.logout();
			$state.go('home.signin');
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.nav')
    	.controller('NavController', NavController);
    
    NavController.$inject = ['$scope', '$state', 'currentUser'];
    
    function NavController($scope, $state, currentUser) {
	    $scope.vm = this;
    	var vm = this;
    	vm.state = $state;
    	
    	vm.currentUser = currentUser;
    }
})();

(function() {
    'use strict'; 
    
    angular 
    	.module('legalprojectmanagement.products')
    	.factory('productsService', productsService);
        
    productsService.$inject = ['$http', 'api_config'];
    
    function productsService($http, api_config) {
		var service = {
			createProduct: createProduct,
			getAllProductsOfTenant: getAllProductsOfTenant,
			findProduct: findProduct,
			findProductsByTerm: findProductsByTerm,
			findProductsByDocumentFileSearchTerm: findProductsByDocumentFileSearchTerm,
			getProductsOfUser: getProductsOfUser,
			getProductTypeOverview: getProductTypeOverview,
			getPagedProductsOfTenant: getPagedProductsOfTenant,
			getProductsReferencedToContact: getProductsReferencedToContact,
			getAmountOfProducts: getAmountOfProducts,
			getProductsWithSameReceipe: getProductsWithSameReceipe,
			updateProduct: updateProduct, 
			deleteProduct: deleteProduct
		};
		
		return service;
		
		////////////
		
		function createProduct(product) {
			return $http.post(api_config.BASE_URL + '/products/product', product);
		}
		
		function getAllProductsOfTenant() {
			return $http.get(api_config.BASE_URL + '/products/product/all');
		}
		
		function findProduct(productId) {
			return $http.get(api_config.BASE_URL + '/products/product/' + productId);
		}
		
		function findProductsByTerm(searchString) {
			return $http.get(api_config.BASE_URL + '/products/product/' + searchString + '/search');
		}
		
		function findProductsByDocumentFileSearchTerm(searchString) {
			return $http.get(api_config.BASE_URL + '/products/product/documentfile/' + searchString + '/search');
		}
		
		function getProductsOfUser(userId) {
			return $http.get(api_config.BASE_URL + '/products/product/user/' + userId);
		}
		
		function getProductTypeOverview() {
			return $http.get(api_config.BASE_URL + '/products/product/typeoverview');
		}
		
		function getProductsReferencedToContact(contactId) {
			return $http.get(api_config.BASE_URL + '/products/product/' + contactId + '/products');
		}
		
		function getPagedProductsOfTenant(page, sortingType) {
			return $http.get(api_config.BASE_URL + '/products/product/page/' + page, {
				params: {
					sortingtype: sortingType
				} 
			});
		}
		
		function getAmountOfProducts() {
			return $http.get(api_config.BASE_URL + '/products/product/amount');
		}
		
		function getProductsWithSameReceipe(receipe) {
			return $http.put(api_config.BASE_URL + '/products/product/samereceipe', receipe);
		}
		
		function updateProduct(product) {
			return $http.put(api_config.BASE_URL + '/products/product', product);
		}
		
		function deleteProduct(productId) {
			return $http.delete(api_config.BASE_URL + '/products/product/' + productId);
		}
    }
})();

(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('receipeService', receipeService);
        
    receipeService.$inject = ['$http', 'api_config'];
    
    function receipeService($http, api_config) {
		var service = {
			createReceipe: createReceipe,
			updateReceipe: updateReceipe,
			findReceipesOfProductOfTypeInRange: findReceipesOfProductOfTypeInRange,
			findAllReceipesOfProductOfType: findAllReceipesOfProductOfType,
			findLastReceipesOfProductOfType: findLastReceipesOfProductOfType,
			deleteReceipe: deleteReceipe
		};
		
		return service;
		
		////////////
		
		function createReceipe(receipe) {
			return $http.post(api_config.BASE_URL + '/receipes/receipe', receipe);
		}
		
		function updateReceipe(receipe) {
			return $http.put(api_config.BASE_URL + '/receipes/receipe', receipe);
		}
		
		function findReceipesOfProductOfTypeInRange(productId, receipeType, start, end) {
			return $http.get(api_config.BASE_URL + '/receipes/receipe/' + productId + '/' + receipeType + '/' + start + '/' + end + '/');
		}
		
		function findAllReceipesOfProductOfType(productId, receipeType) {
			return $http.get(api_config.BASE_URL + '/receipes/receipe/' + productId + '/' + receipeType);
		}
		
		function findLastReceipesOfProductOfType(productId, receipeType) {
			return $http.get(api_config.BASE_URL + '/receipes/receipe/' + productId + '/' + receipeType + '/last');
		}
		
		function deleteReceipe(receipe) {
			return $http.delete(api_config.BASE_URL + '/receipes/receipe/' + receipe.id);
		}
    }
})();

(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('thinningService', thinningService);
        
    thinningService.$inject = ['$http', 'api_config'];
    
    function thinningService($http, api_config) {
		var service = {
			createThinning: createThinning,
			updateThinning: updateThinning,
			findThinningsOfProductInRange: findThinningsOfProductInRange,
			findLastThinningOfProduct: findLastThinningOfProduct,
			findAllThinningsOfProduct: findAllThinningsOfProduct,
			deleteThinning: deleteThinning
		};
		
		return service;
		
		////////////
		
		function createThinning(thinning) {
			return $http.post(api_config.BASE_URL + '/thinnings/thinning', thinning);
		}
		
		function updateThinning(thinning) {
			return $http.put(api_config.BASE_URL + '/thinnings/thinning', thinning);
		}
		
		function findThinningsOfProductInRange(productId, start, end) {
			return $http.get(api_config.BASE_URL + '/thinnings/thinning/' + productId + '/' + start + '/' + end + '/');
		}
		
		function findLastThinningOfProduct(productId) {
			return $http.get(api_config.BASE_URL + '/thinnings/thinning/' + productId + '/last');
		}
		
		function findAllThinningsOfProduct(productId) {
			return $http.get(api_config.BASE_URL + '/thinnings/thinning/' + productId);
		}
		
		function deleteThinning(thinningId) {
			return $http.delete(api_config.BASE_URL + '/thinnings/thinning/' + thinningId);
		}
    }
})();

(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('thinningPolymerDateTimeValueService', thinningPolymerDateTimeValueService);
        
    thinningPolymerDateTimeValueService.$inject = ['$http', 'api_config'];
    
    function thinningPolymerDateTimeValueService($http, api_config) {
		var service = {
			create: create,
			update: update,
			findAll: findAll,
			findThinningPolymerDateTimeValueOfDate: findThinningPolymerDateTimeValueOfDate,
			deleteThinningPolymerDateTimeValue: deleteThinningPolymerDateTimeValue
		};
		
		return service;
		
		////////////
		
		function create(thinningPolymerDateTimeValue) {
			return $http.post(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue', thinningPolymerDateTimeValue);
		}
		
		function update(thinningPolymerDateTimeValue) {
			return $http.put(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue', thinningPolymerDateTimeValue);
		}
		
		function findAll() {
			return $http.get(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue');
		}
		
		function findThinningPolymerDateTimeValueOfDate(date) {
			return $http.get(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue/' + date + '/');
		}
		
		function deleteThinningPolymerDateTimeValue(thinningPolymerDateTimeValueId) {
			return $http.delete(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue/' + thinningPolymerDateTimeValueId);
		}
    }
})();

(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.products')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getProductsState());
    	 
    	////////////
			    	
    	function getProductsState() {
    		var state = {
    			name: 'auth.products',
				url: '/products/:userId/:productId',
				templateUrl: 'app/products/products/products.html',
				controller: 'ProductsController',
				controllerAs: 'vm',
				resolve: { 
					productsService: 'productsService',
					userService: 'userService',
					optionsService: 'optionsService',
					thinningPolymerDateTimeValueService: 'thinningPolymerDateTimeValueService',
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					productTypes: function($stateParams, optionsService) {
						return optionsService.getProductTypes();
					},
					amountOfProducts: function(productsService) {
						return productsService.getAmountOfProducts();
					},
					clickedProduct: function($stateParams, productsService) {
						if($stateParams.productId != null && $stateParams.productId != '') {
							return productsService.findProduct($stateParams.productId);
						} else {
							return null;
						}
					},
					unityTypes: function(optionsService) {
						return optionsService.getUnityTypes();
					},
					productAggregateConditionTypes: function(optionsService) {
						return optionsService.getProductAggregateConditionTypes();
					},
					productId: function($stateParams) {
						return $stateParams.productId;
					},
					usersOfProject: function findProjectUserConnectionByProject(userService) {
						return userService.findAllUsers();
					},
					roles: function(optionsService) {
						return optionsService.getRoles();
					},
					currentThinningPolymerDateTimeValue: function findThinningPolymerDateTimeValueOfDate(thinningPolymerDateTimeValueService) {
						var date = new Date();
						var dateString = date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear();
 						return thinningPolymerDateTimeValueService.findThinningPolymerDateTimeValueOfDate(dateString);
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products') 
    	.directive('createProductsDirective', createProductsDirective);
    
    createProductsDirective.$inject = ['$timeout', 'Upload', 'productsService', 'productsOfTenantModalService', 'dateUtilityService', 'contactsService'];
    
	function createProductsDirective($timeout, Upload, productsService, productsOfTenantModalService, dateUtilityService, contactsService) {
		var directive = {
			restrict: 'E',
			scope: {
				product: '=',
				products: '=',
				productTypes: '=',
				typeOfChange: '=',
				currentUser: '=',
				productAggregateConditionTypeEnums: '=',
				invoker: '=',
				roles: '=',
				usersOfProject: '='
			},
			templateUrl: 'app/products/directives/createProductDirective/createProduct.html',
			link: function($scope) {
				
				$scope.product.inactiveDate = $scope.product.inactiveDate != null ? dateUtilityService.convertToDateOrUndefined($scope.product.inactiveDate) : null;
				$scope.product.developmentDate = $scope.product.developmentDate != null ? dateUtilityService.convertToDateOrUndefined($scope.product.developmentDate) : null;
				$scope.selectedContact = null;
				$scope.showAllAddressesWithProducts = $scope.typeOfChange == 'CREATE' ? true : false;
				$scope.previousProductFillingWeights = $scope.product.productFillingWeights;
				
				$scope.$watch('product.productAggregateConditionType', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.productFillingWeights();
		    		} 
				});
				
				$scope.addFirstProductName = function() {
					if($scope.product.names.length == 0) {
						$scope.product.names.push('');
					}
				};
				$scope.addFirstProductName();
				
				$scope.openProductInactiveDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.productInactiveDateDatePicker = true;
				};
				
				$scope.openProductDevelopmentDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.productDevelopmentDatePicker = true;
				};
				
				$scope.createOrChangeProduct = function() {
					if($scope.typeOfChange == 'CREATE') {
						$scope.createProduct(); 
					} else if($scope.typeOfChange == 'CHANGE') {
						$scope.changeProduct();
					}
				};
				
				$scope.productFillingWeights = function() {
					if($scope.product.productAggregateConditionType == 'LIQUID' && ($scope.typeOfChange == 'CREATE' || $scope.product.productFillingWeights.length == 3)) {
						$scope.product.productFillingWeights = [
							{fillingWeight: 'IBC (1.000 Liter)', information: ''},
							{fillingWeight: 'Fass (220 Liter)', information: ''},
							{fillingWeight: 'Fass (120 Liter)', information: ''},
							{fillingWeight: 'Kanister (25 Liter)', information: ''} 
							];
					} else if($scope.product.productAggregateConditionType == 'LIQUID' && $scope.product.productFillingWeights.length == 4) {
						$scope.product.productFillingWeights = $scope.previousProductFillingWeights;
					} else if($scope.product.productAggregateConditionType == 'HARD') {
						$scope.product.productFillingWeights = [
							{fillingWeight: 'Big-Bag (250 Kg)', information: ''},
							{fillingWeight: 'Big-Bag (750 Kg)', information: ''},
							{fillingWeight: 'Big-Bag (1000 Kg)', information: ''}
							]; 
					}
				};
				
				$scope.createProduct = function() {
		    		if($scope.product.name === '') {
		    			return;
		    		}
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		$scope.product.creationUserId = $scope.currentUser.id;
		    		$scope.product.orderNumber = $scope.products.length;
		    		productsService.createProduct($scope.product, false).then(function(response) {
		    			var createdProduct = response.data;
		    			createdProduct.stillCreated = true;
		    			$scope.products.splice(0, 0, createdProduct);
		    			$scope.resetProduct();
		    			$scope.productCreatedSuccess = true;
		    			$scope.invoker.createProductNotifier();
		    			setTimeout();
		    		}, function errorCallback(response) {
			   			  console.log('error createProduct.directive#createProduct');
			   		});
				};
				
				$scope.resetProduct = function() {
					$scope.product.number = '';
	    			$scope.product.name = '';
	    			$scope.product.names = [];
	    			$scope.product.names.push('');
	    			$scope.product.description = '';
	    			$scope.product.chemicalDescription = '';
	    			$scope.product.density = '';
	    			$scope.product.productAggregateConditionType = null;
	    			for(var i = 0; i < $scope.product.productFillingWeights.length; i++) {
	    				$scope.product.productFillingWeights[i].information = ''; 
	    			}
	    			$scope.product.productFillingWeights = [];
	    			$scope.product.color = '#007ab9';
	    			$scope.product.developmentDate = null;
	    			$scope.product.types = [];
	    			$scope.product.position = '';
	    			$scope.product.mixtureInstructions = null;
	    			$scope.product.contactsProduct = [];
	    			$scope.product.creationUserId = '';
	    			$scope.product.creationDate = null;
				};
				
				$scope.changeProduct = function() {
					Date.prototype.toJSON = function () { return this.toLocaleString(); };
					productsService.updateProduct($scope.product, false).then(function(response) {
						$scope.updateProduct(response.data);
						$scope.invoker.closeChangeProductPropertiesModal();
					}, function errorCallback(response) {
						  if(response.data.error == 'NO_UNIQUE_NAME') {
							  $scope.noUniqueNameError = true;
						  } else if(response.data.error == 'NO_UNIQUE_NUMBER') {
							  $scope.noUniqueNumberError = true;
						  }
						  setTimeout();
			   			  console.log('error createProduct.directive#changeProduct');
			   		});	
				};
				
				$scope.updateProduct = function(product) {
					for(var i = 0; i < $scope.products.length; i++) {
						if($scope.products[i].id == product.id) {
							$scope.products[i] = product;
							$scope.products[i].selected = true;
							break;
						}
					}
				};
				
				$scope.showAllProductsOfTenant = function() {
					productsService.getAllProductsOfTenant().then(function(response) {
						productsOfTenantModalService.showProductsOfTenantModal(response.data);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createProduct.directive#showAllProductsOfTenant');
		  	   		 });
				};
				
				$scope.isProductTypeSelected = function(product, productType) {
					for(var i = 0; i < product.types.length; i++) { 
						if(product.types[i] == productType) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addProductType = function(product, productType) {
					var productTypeStillExists = false;
					for(var i = 0; i < product.types.length; i++) {
						if(product.types[i] == productType) {
							productTypeStillExists = true;
							break;
						}
					}
					if(!productTypeStillExists) {
						product.types.push(productType);
					}
				};
				
				$scope.removeProductType = function(product, productType) {
					for(var i = 0; i < product.types.length; i++) {
						if(product.types[i] == productType) {
							product.types.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.getContactsByTerm = function(term) {
					return contactsService.findContactBySearchString(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createProduct.directive#getContactsByTerm'); 
		  	   		 });
				};
				
				$scope.addContact = function(contact) {
					if(contact == null) {
						return;
					}
					var contactStillAdded = false;
					for(var i = 0; i < $scope.product.contactsProduct.length; i++) {
						if($scope.product.contactsProduct[i].contact.id == contact.id) {
							contactStillAdded = true;
						}
					}
					if(!contactStillAdded) {
						var contactProduct = {};
						contactProduct.contact = contact;
						$scope.product.contactsProduct.push(contactProduct);
						$scope.selectedContact = null;
					} else {
						$scope.contactStillExists = true;
						setTimeout();
					}
				};
				
				$scope.removeContact = function(contactProduct) {
					for(var i = 0; i < $scope.product.contactsProduct.length; i++) {
						if($scope.product.contactsProduct[i].contact.id === contactProduct.contact.id) {
							$scope.product.contactsProduct.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.addOrRemoveAddressLocation = function(contactProduct, address, selectAddressLocation, orderConstruction) {
					if(selectAddressLocation == true) {
						$scope.addAddressLocation(contactProduct, address, orderConstruction);
					} else if(selectAddressLocation == false) {
						$scope.removeAddressLocation(contactProduct, address, orderConstruction);
					}
				};
				
				$scope.addAddressLocation = function(contactProduct, addressLocation, orderConstruction) {
					var productLocationAndOrderConstruction = {};
					productLocationAndOrderConstruction.address = addressLocation;
					productLocationAndOrderConstruction.orderConstruction = orderConstruction;
					if(contactProduct.productLocationAndOrderConstructions == null) {
						contactProduct.productLocationAndOrderConstructions = [];
					}
					contactProduct.productLocationAndOrderConstructions.push(productLocationAndOrderConstruction);
				};
				
				$scope.removeAddressLocation = function(contactProduct, addressLocation, orderConstruction) {
					for(var i = 0; i < contactProduct.productLocationAndOrderConstructions.length; i++) {
						if(contactProduct.productLocationAndOrderConstructions[i].address.id == addressLocation.id || $scope.addressEqualsOverProperties(contactProduct.productLocationAndOrderConstructions[i], addressLocation)) {
							contactProduct.productLocationAndOrderConstructions.splice(i, 1);
						}
					}
				};
				
				$scope.addOrderConstruction = function(contactProduct, addressWithProducts, orderConstruction) {
					if(contactProduct.productLocationAndOrderConstructions == null) {
						addressWithProducts.notSelectedError = true;
						setAddressWithProductsTimeout(addressWithProducts);
						return;
					}
					var addressStillSelected = false;
					for(var j = 0; j < contactProduct.productLocationAndOrderConstructions.length; j++) {
						if(contactProduct.productLocationAndOrderConstructions[j].address != null && addressWithProducts != null && contactProduct.productLocationAndOrderConstructions[j].address.id == addressWithProducts.id) {
							addressStillSelected = true;
						}
					}
					if(addressStillSelected == false) {
						addressWithProducts.notSelectedError = true;
						setAddressWithProductsTimeout(addressWithProducts);
						return;
					}
					
					for(var i = 0; i < contactProduct.productLocationAndOrderConstructions.length; i++) {
						if(contactProduct.productLocationAndOrderConstructions[i].address.id == addressWithProducts.id) {
							contactProduct.productLocationAndOrderConstructions[i].orderConstruction = orderConstruction;
						}
					}
				};
				
				$scope.isAddressWithProductsSelected = function(contactProduct, address) {
					if(contactProduct.productLocationAndOrderConstructions == null) {
						return false;
					}
					for(var i = 0; i < contactProduct.productLocationAndOrderConstructions.length; i++) {
						if((contactProduct.productLocationAndOrderConstructions[i].address != null && 
						   address != null && 
						   contactProduct.productLocationAndOrderConstructions[i].address.id == address.id) || $scope.addressEqualsOverProperties(contactProduct.productLocationAndOrderConstructions[i], address)) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addressEqualsOverProperties = function(productLocationAndOrderConstruction, address) {
					if(productLocationAndOrderConstruction.address == null || address == null) {
						return false;
					}
					var contactProductAddress = productLocationAndOrderConstruction.address;
					
					if(contactProductAddress.street === address.street &&
					   contactProductAddress.region === address.region &&
					   contactProductAddress.postalCode === address.postalCode &&
					   contactProductAddress.provinceType === address.provinceType &&
					   contactProductAddress.country === address.country) {
						return true;
					}
					return false;
				};
				
				$scope.getOrderConstruction = function(contactProduct, addressLocation) {
					if(contactProduct.productLocationAndOrderConstructions == null) {
						return '';
					}
					for(var i = 0; i < contactProduct.productLocationAndOrderConstructions.length; i++) {
						if((contactProduct.productLocationAndOrderConstructions[i].address != null && addressLocation != null && contactProduct.productLocationAndOrderConstructions[i].address.id == addressLocation.id)|| 
							$scope.addressEqualsOverProperties(contactProduct.productLocationAndOrderConstructions[i], addressLocation)) {
							return contactProduct.productLocationAndOrderConstructions[i].orderConstruction;
						}
					}
					return '';
				};
				
				$scope.addName = function() {
					$scope.product.names.push('');
				};
				
				$scope.removeName = function(index) {
					$scope.product.names.splice(index, 1);
				};
				
				function setAddressWithProductsTimeout(addressWithProducts) {
		      		$timeout(function() {
		      			addressWithProducts.notSelectedError = false;
		  			   }, 2000); 
		      	}
				
				function setTimeout() {
		      		$timeout(function() {
		      			$scope.contactStillExists = false;
		      			$scope.noUniqueNameError = false;
		      			$scope.noUniqueNumberError = false;
		      			$scope.productCreatedSuccess = false;
		  			   }, 2000); 
		      	}
				
				$scope.hideAddressWithProducts = function(addressWithProducts) {
					var address = addressWithProducts.address;
					if(address == null) {
						return false;
					}
					if(address.userIdBlackList != null && address.userIdBlackList.indexOf($scope.currentUser.id) !== -1) {
						return true;
					}
					if(address.rolesBlackList != null && address.rolesBlackList.indexOf($scope.currentUser.projectUserConnectionRole) !== -1) {
						return true;
					}
					return false;
				};
				
				$scope.isRoleBlackListed = function(role, address) {
					if(address == null || address.rolesBlackList == null) {
						return false;
					}
					for(var i = 0; i < address.rolesBlackList.length; i++) {
						if(address.rolesBlackList[i] == role) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addBlackListRole = function(address, role) {
					if(address.rolesBlackList == null) {
						address.rolesBlackList = [];
					}
					address.rolesBlackList.push(role);
				};
				
				$scope.removeBlackListRole = function(address, role) {
					for(var i = 0; i < address.rolesBlackList.length; i++) {
						if(address.rolesBlackList[i] == role) {
							address.rolesBlackList.splice(i, 1);
							break;
						}
					}
				};
				
				$scope.isBlackListed = function(user, address) {
					if(address == null || address.userIdBlackList == null) {
						return false;
					}
					for(var i = 0; i < address.userIdBlackList.length; i++) {
						if(address.userIdBlackList[i] == user.id) {
							return true;
						}
					}
					return false;
				};
				
				$scope.addBlackListId = function(address, user) {
					if(address.userIdBlackList == null) {
						address.userIdBlackList = [];
					}
					address.userIdBlackList.push(user.id);
				};
				
				$scope.removeBlackListId = function(address, user) {
					for(var i = 0; i < address.userIdBlackList.length; i++) {
						if(address.userIdBlackList[i] == user.id) {
							address.userIdBlackList.splice(i, 1);
							break;
						}
					}
				};
			}
		};
		return directive;
		
		////////////
	}
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products') 
    	.directive('createReceipeDirective', createReceipeDirective);
    
    createReceipeDirective.$inject = ['$timeout', 'productsService', 'documentFileService', 'utilService', 'receipeService', 'dateUtilityService', 'projectTemplateService', 'contactsService', 'printingPDFService', 'productOverviewModalService', 'printModalService'];
    
	function createReceipeDirective($timeout, productsService, documentFileService, utilService, receipeService, dateUtilityService, projectTemplateService, contactsService, printingPDFService, productOverviewModalService, printModalService) {
		var directive = {
			restrict: 'E',
			scope: {
				index: '=',
				currentUser: '=',
				receipe: '=',
				product: '=',
				invoker: '=',
				type: '=',
				receipeType: '=',
				userAuthorizationUserContainer: '=',
				unityTypes: '='
			},
			templateUrl: '/app/products/directives/createReceipe/createReceipe.html',
			link: function($scope) {
				
				$scope.receipe.date = $scope.receipe.date instanceof Date ? $scope.receipe.date : dateUtilityService.convertToDateTimeOrUndefined($scope.receipe.date);
				$scope.selectedContact = $scope.receipe.contact; 
				$scope.createReceipeDateDatePicker = false;
				$scope.percentageResultIs100 = false;
				$scope.amountInKiloError = false;
				$scope.addAdditionalsToProductTrigger = $scope.receipe.receipeAdditionals != null ? true : false;
				$scope.receipeDateTime = $scope.receipe.date;
				
				$scope.setReceipeTimeout = function(receipe) {
		      		$timeout(function() {
		      			$scope.receipe.updated = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.setTimeout = function() {  
		      		$timeout(function() {
		      			$scope.amountInKiloError = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.setContactTimeout = function(receipe) { 
		      		$timeout(function() {
		      			if($scope.receipe.contact != null) {
		      				$scope.receipe.contact.contactSelected = false;
		      			}
		      			$scope.receipe.contactTextSelected = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.moveUp = function(index) {
		      		$scope.receipe.receipeProducts.splice(index-1, 0, $scope.receipe.receipeProducts.splice(index, 1)[0]);
		      	};
		      	
		      	$scope.moveDown = function(index) {
		      		$scope.receipe.receipeProducts.splice(index+1, 0, $scope.receipe.receipeProducts.splice(index, 1)[0]);
		      	};
				
				$scope.openCreateReceipeDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.createReceipeDateDatePicker = true; 
				};
				
				$scope.getProductName = function(receipeProduct) { 
					if(receipeProduct == null || receipeProduct.product == null) { 
						return '';
					}
					return receipeProduct.product.name;
				};
				
				$scope.showProductOverviewModalService = function(product) {
		    		productsService.findProduct(product.id).then(function(response) {
		    			productOverviewModalService.showProductOverviewModal(response.data, $scope);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error createReceipe.directive#showProductOverviewModalService');
		 	   		 });
		    	};
				
				$scope.getProductsByTerm = function(term) {
					return productsService.findProductsByTerm(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#getProductsByTerm'); 
		  	   		 });
				};
				
				$scope.productSelected = function(selectedReceipeProduct, index) {
					var addedProduct = false;
		    		for(var i = 0; i < $scope.receipe.receipeProducts.length; i++) {
		    			if(Object.keys($scope.receipe.receipeProducts[i]).length === 0) {
		    				var receipeProduct = {};
		    				receipeProduct.product = selectedReceipeProduct;
		    				receipeProduct.percentage = null;
		    				$scope.receipe.receipeProducts[i] = receipeProduct;
		    				addedProduct = true;
		    				break;
		    			}
		    		}
		    		if(addedProduct == false) {
		    			$scope.receipe.receipeProducts[index].product = selectedReceipeProduct;
		    			$scope.receipe.receipeProducts[index].density = selectedReceipeProduct.density;
		    		}
		    	};
		    	
		    	$scope.contactSelected = function(receipe) { 
		    		$scope.receipe.contactText = null;
		    		$scope.receipe.contact = receipe.contactOrContactText;
		    		$scope.receipe.contact.contactSelected = true;
		    		$scope.setContactTimeout($scope.receipe);
		    	};
		    	
		    	$scope.setContactText = function(receipe) {
		    		$scope.receipe.contactText = receipe.contactOrContactText;
		    		$scope.receipe.contactTextSelected = true;
		    		$scope.setContactTimeout($scope.receipe);
		    		$scope.receipe.contact = null;
		    	};
		    	
		    	$scope.checkIfPercentageResultIs100 = function(receipeProducts) {
		    		if(receipeProducts == null) { 
		    			receipeProducts.percentageResult = 0;
		    			$scope.percentageResultIs100 = false;
		    			return;
		    		} 
		    		var result = 0;
		    		for(var i = 0; i < receipeProducts.length; i++) {
		    			if(receipeProducts[i].percentage != null) {
		    				result += receipeProducts[i].percentage;
		    				receipeProducts[i].errorPercentage = false;
		    			}
		    		}
		    		var resultInteger = parseFloat(result).toFixed(2);
		    		if(resultInteger == parseFloat(100).toFixed(2)) {
		    			receipeProducts.percentageResult = resultInteger;
		    			$scope.percentageResultIs100 = true;
		    			return;
		    		}
		    		receipeProducts.percentageResult = resultInteger;
		    		$scope.percentageResultIs100 = false;
		    	};
		    	$scope.checkIfPercentageResultIs100($scope.receipe.receipeProducts);
		    	
		    	$scope.addReceipeProduct = function() { 
		    		var receipeProduct = {}; 
		    		$scope.receipe.receipeProducts.push(receipeProduct);
		    	};
		    	
		    	$scope.additionalProductSelected = function(selectedReceipeProduct, index) {
		    		$scope.receipe.receipeAdditionals[index].product = selectedReceipeProduct;
		    		$scope.receipe.receipeAdditionals[index].productError = false;
		    		$scope.calculateAllReceipeAdditionals();
		    	};
		    	
		    	$scope.addReceipeAdditional = function() {
		    		var receipeAdditional = {};
		    		receipeAdditional.product = null;
		    		receipeAdditional.unityType = null;
		    		receipeAdditional.amount = 0;
		    		if($scope.receipe.receipeAdditionals == null) {
		    			$scope.receipe.receipeAdditionals = []; 
		    		}
		    		$scope.receipe.receipeAdditionals.push(receipeAdditional);
		    	};
		    	
		    	$scope.calculateAllReceipeAdditionals = function() {
		    		if($scope.receipe.receipeAdditionals != null) {
			    		for(var i = 0; i < $scope.receipe.receipeAdditionals.length; i++) {
			    			$scope.calculateReceipeAdditional($scope.receipe.receipeAdditionals[i], i);
			    		}
		    		}
		    	};
		    	
		    	$scope.calculateReceipeAdditional = function(receipeAdditional, index) {
		    		var amountInKilo = $scope.receipe.amountInKilo;
		    		if(amountInKilo == null) {
		    			receipeAdditional.result = null;
		    			$scope.amountInKiloError = true;
		    			$scope.setTimeout();
		    			return;
		    		}
		    		
		    		var product = $scope.receipe.receipeAdditionals[index].product;
	    			if(product == null) {
	    				console.log('product not set in createReceipe.directive#calculateReceipeAdditional');
	    				receipeAdditional.result = null;
	    				receipeAdditional.productError = true;
	    				return;
	    			} else {
	    				receipeAdditional.productError = false;
	    			}
		    		
		    		$scope.amountInKiloError = false;
		    		var kiloGramPerTon = null;
		    		var result = null;
		    		switch(receipeAdditional.unityType) {
		    		case 'GRAM':
		    			kiloGramPerTon = receipeAdditional.amount * 0.001;
		    			result = kiloGramPerTon * amountInKilo;
		    			receipeAdditional.result = result;
		    			break;
		    		case 'LITER':		    			
		    			kiloGramPerTon = receipeAdditional.amount * 0.001;
		    			result = kiloGramPerTon * amountInKilo;
		    			receipeAdditional.result = result;
		    			break;
		    		}
		    	};
		    	
		    	$scope.removeReceipeProduct = function(index) {
		    		$scope.receipe.receipeProducts.splice(index, 1);
		    		for(var i = 0; i < $scope.receipe.receipeProducts.length; i++) {
		    			$scope.receipe.receipeProducts[i].selectedReceipeProduct = $scope.getProductName($scope.receipe.receipeProducts[i]);
		    		}
		    		$scope.checkIfPercentageResultIs100($scope.receipe.receipeProducts);
		    	};
		    	
		    	$scope.removeReceipeAdditional = function(index) {
		    		$scope.receipe.receipeAdditionals.splice(index, 1);
		    		for(var i = 0; i < $scope.receipe.receipeAdditionals.length; i++) {
		    			$scope.receipe.receipeAdditionals[i].selectedReceipeAdditional = $scope.getProductName($scope.receipe.receipeAdditionals[i]);
		    		}
		    		if($scope.receipe.receipeAdditionals.length == 0) {
		    			$scope.receipe.receipeAdditionals = null;
		    			$scope.addAdditionalsToProductTrigger = false;
		    		}
		    	};
		    	
		    	$scope.attachFile = function(selected, selectedDocumentFile, documentFileVersion) {
		    		if(!selected) {
		    			for(var i = 0; i < $scope.receipe.receipeAttachments.length; i++) {
		    				if($scope.receipe.receipeAttachments[i].filename == selectedDocumentFile.fileName && $scope.receipe.receipeAttachments[i].version == documentFileVersion.version) {
		    					$scope.receipe.receipeAttachments.splice(i, 1);
		    					break;
		    				}
		    			}
		    		} else {
		    			var receipeAttachment = {};
		    			receipeAttachment.documentFileId = selectedDocumentFile.id;
		    			receipeAttachment.filepath = selectedDocumentFile.subFilePath + '/' + selectedDocumentFile.documentFileVersions[0].filePathName;
		    			receipeAttachment.filename = selectedDocumentFile.fileName;
		    			receipeAttachment.ending = selectedDocumentFile.ending;
		    			receipeAttachment.version = documentFileVersion.version;
		    			receipeAttachment.originalFilename = selectedDocumentFile.fileName;
		    			receipeAttachment.isImage = utilService.isImage(selectedDocumentFile.ending);
		    			$scope.receipe.receipeAttachments.push(receipeAttachment);
		    		}
		    	};
		    	
		    	$scope.removeFile = function(index) {
		    		$scope.receipe.receipeAttachments.splice(index, 1);
		    	};
		    	
		    	$scope.findDocumentsByTerm = function(term) {
					return documentFileService.findDocumentFilesByBySearchStringAndProjectIdAndFolderId(term, $scope.product.id, '').then(function(response) {
						var documentFiles = response.data;
						for(var i = 0; i < documentFiles.length; i++) {
							documentFiles[i].vm = $scope;
						}
			    		return documentFiles;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#findDocumentsByTerm'); 
		  	   		 });
				};
				
				$scope.createReceipe = function() {
					$scope.receipe.productId = $scope.product.id;
					$scope.receipe.receipeType = 'STANDARD_MIXTURE';
					Date.prototype.toJSON = function () { return this.toLocaleString(); };
					$scope.receipe.date = dateUtilityService.joinDateObjectsToDateTimeObject($scope.receipe.date, $scope.receipeDateTime);
		    		receipeService.createReceipe($scope.receipe).then(function(response) {
		    			$scope.resetReceipe();
		    			$scope.invoker.closeCreateReceipeModal();
		    			$scope.invoker.updateProduct($scope.product.id);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#createReceipe'); 
		  	   		 });
		    	};
		    	
		    	$scope.changeReceipe = function() {
		    		receipeService.updateReceipe($scope.receipe).then(function(response) {
		    			$scope.receipe.updated = true;
		    			$scope.setReceipeTimeout($scope.receipe);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#createReceipe'); 
		  	   		 });
		    	};
		    	
		    	$scope.deleteReceipe = function(receipe, canDeleteReceipeOrMixture) {
		    		if(canDeleteReceipeOrMixture == false) {
		    			return;
		    		}
		    		receipeService.deleteReceipe($scope.receipe).then(function(response) {
		    			$scope.invoker.deleteReceipe(receipe);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#deleteReceipe'); 
		  	   		 });
		    	};
		    	
		    	$scope.calculateKilogramAndLiterOfAllProducts = function(receipe) {
		    		for(var i = 0; i < receipe.receipeProducts.length; i++) {
		    			$scope.calculateKilogramAndLiter(receipe, receipe.receipeProducts[i]);
		    		}
		    	};
		    	
		    	$scope.calculateKilogramAndLiter = function(receipe, receipeProduct) {
		    		if($scope.receipeType == 'CHANGED_MIXTURE') {
		    			var amountInKilo = receipe.amountInKilo;
		    			if(amountInKilo == null) {
		    				$scope.amountInKiloError = true;
		    				$scope.setTimeout();
		    				return;
		    			} else {
		    				// kilogram = (amountInKilo * percentage) / 100;
		    				// liter = kilogram / density;
		    				var density = receipeProduct.density;
		    				var percentage = receipeProduct.percentage;
		    				var kilogram = (amountInKilo * percentage) / 100;
		    				var liter = kilogram / density;
		    				receipeProduct.kilogram = kilogram;
		    				receipeProduct.liter = liter;
		    			}
		    		}
		    	};
		    	
		    	$scope.resetReceipe = function() { 
		    		$scope.receipe.productId = '';
		    		$scope.receipe.date = new Date();
		    		$scope.receipe.receipeProducts = [];
		    		$scope.receipe.information = '';
		    		$scope.receipe.receipeAttachments = [];
		    		$scope.receipe.receipeType = '';
		    		$scope.receipe.receipeAdditionals = null;
		    	};
		    	
		    	$scope.getContactsByTerm = function(term) {
					return contactsService.findContactBySearchString(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#getContactsByTerm'); 
		  	   		 });
				};
		    	
		    	$scope.createDocumentUrl = function(user, product, receipeAttachment) {		    	
		    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
		    		return 'api/filedownloads/filedownload/' + user.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
		    	};
		    	
		    	$scope.showTemplateModal = function(url, receipeAttachment) {
		    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
		    			var documentFile = response.data;
		    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
		    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#showTemplateModal'); 
		  	   		 });
		    	};
		    	
		    	$scope.findLastReceipesOfProductOfType = function(receipeProduct, receipeType) { 
		    		receipeService.findLastReceipesOfProductOfType(receipeProduct.product.id, receipeType).then(function(response) {
		    			$scope.toolTipProductInfo = response.data; 
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#findLastReceipesOfProductOfType'); 
		  	   		 });
		    	};
		    	
		    	$scope.printContainer = function(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	};
		    	
		    	$scope.showPrintModal = function(receipe) {
		    		printModalService.showPrintModal($scope.product, receipe, $scope.receipeType, $scope.currentUser);
		    	};
		    	
		    	$scope.closeCreateReceipeModal = function() {   
		    		$scope.resetReceipe();
		    		$scope.invoker.closeCreateReceipeModal();
		    	};
				
			}
		};
		return directive;
		
		////////////
	}
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products') 
    	.directive('createThinningDirective', createThinningDirective);
    
    createThinningDirective.$inject = ['$timeout', 'dateUtilityService', 'thinningService', 'productsService', 'printingPDFService', 'printThinningModalService'];
    
	function createThinningDirective($timeout, dateUtilityService, thinningService, productsService, printingPDFService, printThinningModalService) {
		var directive = {
			restrict: 'E',
			scope: {
				index: '=',
				thinning: '=',
				product: '=',
				products: '=',
				invoker: '=',
				type: '=',
				currentUser: '=',
				userAuthorizationUserContainer: '=',
				polymerPromille: '='
			},
			templateUrl: 'app/products/directives/createThinning/createThinning.html',
			link: function($scope) { 
				
				$scope.createThinningDateDatePicker = false;
				$scope.thinningDateTime = $scope.thinning.date instanceof Date ? $scope.thinning.date : dateUtilityService.convertToDateTimeOrUndefined($scope.thinning.date);
				
				$scope.setTimeout = function() {
		      		$timeout(function() {
		      			$scope.thinning.updateSuccess = false;
		      			$scope.thinning.densityError = false;
		      			$scope.thinning.densityOfProductError = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.openCreateThinningDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.createThinningDateDatePicker = true; 
				};
		      	
				$scope.createThinning = function() {  
					$scope.thinning.productId = $scope.product.id;
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		$scope.thinning.date = dateUtilityService.joinDateObjectsToDateTimeObject($scope.thinning.date, $scope.thinningDateTime);
		    		thinningService.createThinning($scope.thinning).then(function(response) {
		    			productsService.findProduct($scope.product.id).then(function(response) {
		    				$scope.product = response.data;
		    				for(var i = 0; i < $scope.products.length; i++) {
		    					if($scope.products[i].id == $scope.product.id) {
		    						var selected = $scope.products[i].selected;
		    						$scope.products[i] = $scope.product;
		    						$scope.products[i].selected = selected;
		    						break;
		    					}
		    				}
		    				$scope.resetThinning();
		    			}, function errorCallback(response) {
			 	   			 console.log('error createThinning.directive#createThinning');
			 	   		});
		    			$scope.invoker.closeCreateThinningModal();
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error createThinning.directive#createThinning');
		 	   		 });
		    	};
		    	
		    	$scope.resetThinning = function() {
		    		$scope.thinning.productId = '';
		    		$scope.thinning.date = new Date();
		    		$scope.thinning.ironContent = null;
		    		$scope.thinning.targetContent = null;
		    		$scope.thinning.density = null;
		    		$scope.thinning.amount = null;
		    		$scope.thinning.amountLiter = null;
		    		$scope.thinning.iron = null;
		    		$scope.thinning.water = null;
		    		$scope.thinning.polymer = false;
		    		$scope.thinning.ascorbinSaeure = false;
		    		$scope.thinning.polymerSubtract = 1000;
		    		$scope.thinning.ascorbinSaeureSubtract = 2700;
		    		$scope.thinning.waterAmountCalculated = null;
		    		$scope.thinning.informationTop = null;
		    		$scope.thinning.information = null;
		    		$scope.thinning.labelLeft1 = 'Fe- Gehalt';
		    		$scope.thinning.labelLeft2 = 'Fe- Gehalt';
		    		$scope.thinning.labelRight1 = 'Eisenlösung';
		    	};
		    	
		    	$scope.updateThinning = function() {
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		var date = dateUtilityService.convertToDateOrUndefined($scope.thinning.date);
		    		$scope.thinning.date = dateUtilityService.joinDateObjectsToDateTimeObject(date, $scope.thinningDateTime);
		    		thinningService.updateThinning($scope.thinning).then(function(response) {
		    			$scope.thinning.updateSuccess = true;
		    			$scope.setTimeout();
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error createThinning.directive#editThinning');
		 	   		 });
		    	};		    	
		    	
		    	$scope.deleteThinning = function() {
		    		thinningService.deleteThinning($scope.thinning.id).then(function(response) {
		    			$scope.invoker.deleteThinning($scope.thinning);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error createThinning.directive#editThinning');
		 	   		 });
		    	};
		    	
		    	$scope.$watch('thinning.targetContent', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.calculateAmount();
		    		}
				});
		    	
		    	$scope.$watch('thinning.ironContent', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.calculateAmount();
		    		}
				});
		    	
		    	$scope.$watch('thinning.density', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.calculateAmount();
		    		}
				});
		    	
		    	$scope.$watch('thinning.amount', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.calculateAmount();
		    		}
				});
		    	
		    	$scope.$watch('thinning.polymer', function(newValue, oldValue) {
		    		if(oldValue != newValue) { 
		    			$scope.addRemoveLiterOfPolymer(newValue, oldValue);   
		    		}
				});
		    	
		    	$scope.$watch('thinning.ascorbinSaeure', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.addRemoveLiterOfAscorbinsaeure(newValue, oldValue);
		    		}
				});
		    	
		    	$scope.calculateAmount = function() {
		    		if($scope.product.density == null) {
		    			$scope.thinning.densityOfProductError = true;
		    			$scope.setTimeout();
		    			return;
		    		}
		    		$scope.targetMass = $scope.thinning.amountLiter * $scope.product.density; // (Masse (Ziel))
		    		$scope.ironInKg =  $scope.targetMass * ($scope.thinning.targetContent / $scope.thinning.ironContent);
		    		$scope.thinning.iron = $scope.ironInKg / $scope.thinning.density;
		    		$scope.thinning.water = $scope.thinning.amountLiter - $scope.thinning.iron;
		    		$scope.thinning.waterAmountCalculated = $scope.thinning.water;
		    		
		    		if($scope.thinning.polymer == true) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated - $scope.thinning.polymerSubtract;
		    		}
		    		if($scope.thinning.ascorbinSaeure == true) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated - $scope.thinning.ascorbinSaeureSubtract;
		    		}
		    	};
		    	
		    	$scope.addRemoveLiterOfPolymer = function(newValue, oldValue) {
		    		if(newValue == true && oldValue == false) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated - $scope.thinning.polymerSubtract;
		    		} else if(newValue == false && oldValue == true) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated + $scope.thinning.polymerSubtract;
		    		}
		    	};
		    	
		    	$scope.addRemoveLiterOfAscorbinsaeure = function(newValue, oldValue) {
		    		if(newValue == true && oldValue == false) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated - $scope.thinning.ascorbinSaeureSubtract;
		    		} else if(newValue == false && oldValue == true) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated + $scope.thinning.ascorbinSaeureSubtract;
		    		}
		    	};
		    	
		    	$scope.amountInLiterChanged = function() {
		    		if($scope.product.density == null) {
		    			$scope.thinning.densityOfProductError = true;
		    			$scope.setTimeout();
		    			return;
		    		}
		    		$scope.thinning.amount = $scope.thinning.amountLiter * $scope.product.density;
		    	};
		    	
		    	$scope.printContainer = function(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	};
		    	
		    	$scope.showPrintThinningModal = function(thinningOfProduct) {
		    		printThinningModalService.showPrintThinningModal($scope.product, thinningOfProduct, $scope.currentUser, $scope.polymerPromille);
		    	};
		    	
		    	$scope.resetReceipe = function() { 
		    		$scope.thinning.ironContent = null;
		    		$scope.thinning.targetContent = null;
		    		$scope.thinning.density = null;
		    		$scope.thinning.amountLiter = null;
		    		$scope.thinning.amount = null;
		    		$scope.thinning.informationTop = null;
		    		$scope.thinning.information = null;
		    	};
		    	
		    	$scope.closeCreateThinningModal = function() {
		    		$scope.resetThinning();
					$scope.invoker.closeCreateThinningModal();
				};
			}
		};
		return directive;
		
		////////////
	}
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('changeProductPropertiesModalService', changeProductPropertiesModalService);
        
    changeProductPropertiesModalService.$inject = ['$modal', '$stateParams'];
    
    function changeProductPropertiesModalService($modal, $stateParams) {
		var service = {
			showChangeProductPropertiesModal: showChangeProductPropertiesModal
		};		
		return service;
		
		////////////
				
		function showChangeProductPropertiesModal(product, products, productTypes, currentUser, productAggregateConditionTypes, roles, usersOfProject) {
			 var changeProductPropertiesModal = $modal.open({
				controller: ChangeProductPropertiesModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	productTypes: function() {
			    		return productTypes;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	productAggregateConditionTypes: function() {
			    		return productAggregateConditionTypes;
			    	},
			    	roles: function() {
			    		return roles;
			    	},
			    	usersOfProject: function() {
			    		return usersOfProject;
			    	}
			    }, 
				templateUrl: 'app/products/modals/changeProductProperties/changeProductProperties.modal.html'
			});
			return changeProductPropertiesModal;
			
			function ChangeProductPropertiesModalController($modalInstance, $scope, product, products, productTypes, currentUser, productAggregateConditionTypes, roles, usersOfProject) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.products = products;
		    	vm.productTypes = productTypes;
		    	vm.currentUser = currentUser;
		    	vm.productAggregateConditionTypes = productAggregateConditionTypes;
		    	vm.roles = roles;
		    	vm.usersOfProject = usersOfProject;
		    	
		    	vm.closeChangeProductPropertiesModal = closeChangeProductPropertiesModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	function closeChangeProductPropertiesModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('createReceipeModalService', createReceipeModalService);
        
    createReceipeModalService.$inject = ['$modal', '$stateParams', 'productsService', 'receipeService', 'documentFileService', 'utilService', 'projectTemplateService'];
    
    function createReceipeModalService($modal, $stateParams, productsService, receipeService, documentFileService, utilService, projectTemplateService) {
		var service = {
			showCreateReceipeModal: showCreateReceipeModal
		};		
		return service;
		
		////////////
				
		function showCreateReceipeModal(currentUser, product, products, receipe, allReceipesOfProduct, userAuthorizationUserContainer, unityTypes) {
			 var createReceipeModal = $modal.open({
				controller: CreateReceipeModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	receipe: function() {
			    		return receipe;
			    	},
			    	allReceipesOfProduct: function() {
			    		return allReceipesOfProduct;
			    	}, 
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	unityTypes: function() {
			    		return unityTypes;
			    	}
			    }, 
				templateUrl: 'app/products/modals/createReceipeModal/createReceipe.modal.html'
			});
			return createReceipeModal;
			
			function CreateReceipeModalController($modalInstance, $scope, product, products, receipe, allReceipesOfProduct, userAuthorizationUserContainer, unityTypes) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.receipe = receipe;
		    	vm.allReceipesOfProduct = allReceipesOfProduct;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.unityTypes = unityTypes;

		    	vm.updateProduct = updateProduct;
		    	vm.createDocumentUrl = createDocumentUrl;
		    	vm.showTemplateModal = showTemplateModal;
		    	vm.closeCreateReceipeModal = closeCreateReceipeModal;  
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	function updateProduct(productId) {
		    		productsService.findProduct(productId).then(function(response) {
		    			var product = response.data;
		    			if(vm.products != null) {
			    			for(var i = 0; i < vm.products.length; i++) {
			    				if(vm.products[i].id == productId) {
			    					var selected = vm.products[i].selected;
			    					vm.products[i] = product;
			    					vm.products[i].selected = selected;
			    					break;
			    				}
			    			}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.service#updateProduct'); 
		  	   		 });
		    	}
		    	
		    	function createDocumentUrl(user, product, receipeAttachment) {		    	
		    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
		    		return 'api/filedownloads/filedownload/' + user.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
		    	}
		    	
		    	function showTemplateModal(url, receipeAttachment) {
		    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
		    			var documentFile = response.data;
		    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
		    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#getProductsByTerm'); 
		  	   		 });
		    	}
		    	
		    	function closeCreateReceipeModal() {
	    			vm.receipe.productId = '';
	    			vm.receipe.date = new Date();
	    			vm.receipe.receipeProducts = [];
	    			vm.receipe.information = '';
	    			vm.receipe.receipeAttachments = [];
	    			vm.receipe.receipeType = '';
	    			vm.receipe.receipeAdditionals = null;
	    			
					$modalInstance.dismiss('cancel'); 
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('createThinningModalService', createThinningModalService);
        
    createThinningModalService.$inject = ['$modal', '$stateParams', 'productsService', 'thinningService', 'dateUtilityService'];
    
    function createThinningModalService($modal, $stateParams, productsService, thinningService, dateUtilityService) { 
		var service = {
			showCreateThinningModal: showCreateThinningModal
		};		
		return service;
		
		////////////
				
		function showCreateThinningModal(currentUser, product, products, thinning, allThinningsOfProduct, userAuthorizationUserContainer, polymerPromille) {
			 var createThinningModal = $modal.open({
				controller: CreateThinningModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	thinning: function() {
			    		return thinning;
			    	},
			    	allThinningsOfProduct: function() {
			    		return allThinningsOfProduct;
			    	}, 
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	}, 
			    	polymerPromille: function() {
			    		return polymerPromille;
			    	}
			    }, 
				templateUrl: 'app/products/modals/createThinningModal/createThinning.modal.html'
			});
			return createThinningModal;
			
			function CreateThinningModalController($modalInstance, $scope, currentUser, product, products, thinning, allThinningsOfProduct, polymerPromille) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.thinning = thinning;
		    	vm.allThinningsOfProduct = allThinningsOfProduct;
		    	vm.polymerPromille = polymerPromille;

		    	vm.closeCreateThinningModal = closeCreateThinningModal;  
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	function closeCreateThinningModal() {
		    		vm.thinning.productId = '';
		    		vm.thinning.date = new Date();
		    		vm.thinning.ironContent = null;
		    		vm.thinning.targetContent = null;
		    		vm.thinning.density = null;
		    		vm.thinning.amount = null;
		    		vm.thinning.amountLiter = null;
		    		vm.thinning.iron = null;
		    		vm.thinning.water = null;
		    		vm.thinning.polymer = false;
		    		vm.thinning.ascorbinSaeure = false;
		    		vm.thinning.polymerSubtract = 1000;
		    		vm.thinning.ascorbinSaeureSubtract = 2700;
		    		vm.thinning.waterAmountCalculated = null;
		    		vm.thinning.information = null;
		    		vm.thinning.labelLeft1 = 'Fe- Gehalt';
		    		vm.thinning.labelLeft2 = 'Fe- Gehalt';
		    		vm.thinning.labelRight1 = 'Eisenlösung';
					$modalInstance.dismiss('cancel'); 
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('editReceipeModalService', editReceipeModalService);
        
    editReceipeModalService.$inject = ['$modal', '$stateParams', 'productsService', 'receipeService', 'dateUtilityService'];
    
    function editReceipeModalService($modal, $stateParams, productsService, receipeService, dateUtilityService) {
		var service = {
			showEditReceipeModal: showEditReceipeModal
		};		
		return service;
		
		////////////
				
		function showEditReceipeModal(currentUser, product, products, receipesOfProduct, receipeType, start, end, userAuthorizationUserContainer, unityTypes) {
			 var editReceipeModal = $modal.open({
				controller: EditReceipeModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	receipesOfProduct: function() {
			    		return receipesOfProduct;
			    	},
			    	receipeType: function() {
			    		return receipeType;
			    	},
			    	start: function() {
			    		return start;
			    	},
			    	end: function() { 
			    		return end;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	unityTypes: function() {
			    		return unityTypes;
			    	}
			    }, 
				templateUrl: '/app/products/modals/editReceipeModal/editReceipe.modal.html'
			});
			return editReceipeModal;
			
			function EditReceipeModalController($modalInstance, $scope, currentUser, product, products, receipesOfProduct, receipeType, start, end, userAuthorizationUserContainer, unityTypes) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.receipesOfProduct = setContactOfReceipes(receipesOfProduct);
		    	vm.receipeType = receipeType;
		    	vm.searchReceipeDateFromDatePicker = false;
		    	vm.searchReceipeDateUntilDatePicker = false;
		    	vm.searchReceipeTerm = null;
		    	vm.receipeDateFrom = start;
		    	vm.receipeDateUntil = end;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.unityTypes = unityTypes;
		    	
		    	vm.updateProduct = updateProduct;
		    	vm.deleteReceipe = deleteReceipe;
		    	vm.openSearchReceipeFromDatePicker = openSearchReceipeFromDatePicker;
		    	vm.openSearchReceipeUntilDatePicker = openSearchReceipeUntilDatePicker;
		    	vm.closeEditReceipeModal = closeEditReceipeModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	$scope.$watch('vm.receipeDateFrom', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.receipeDateFrom, vm.receipeDateUntil);
			    		if(vm.receipeDateFrom != null && vm.receipeDateUntil != null && checkDateRange == false) {
			    			searchReceipesInTimeRange();
			    		}
		    		}
		        });
		    	
		    	$scope.$watch('vm.receipeDateUntil', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.receipeDateFrom, vm.receipeDateUntil);
			    		if(vm.receipeDateFrom != null && vm.receipeDateUntil != null && checkDateRange == false) {
			    			searchReceipesInTimeRange();
			    		}
		    		}
		        });
		    	
		    	function setContactOfReceipes(receipes) {
		    		for(var i = 0; i < receipes.length; i++) {
		    			if(receipes[i].contactText != null) {
		    				receipes[i].contactOrContactText = receipes[i].contactText;
		    			} else if(receipes[i].contact != null) {
		    				receipes[i].contactOrContactText = receipes[i].contact;
		    			}
		    		}
		    		return receipes;
		    	}
		  
		    	function openSearchReceipeFromDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchReceipeDateFromDatePicker = true; 
				}
		    	
		    	function openSearchReceipeUntilDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchReceipeDateUntilDatePicker = true; 
				}
		    	
		    	function deleteReceipe(receipe) {
		    		for(var i = 0; i < vm.receipesOfProduct.length; i++) {
		    			if(vm.receipesOfProduct[i].id == receipe.id) {
		    				vm.receipesOfProduct.splice(i, 1);
		    				updateProduct(vm.product.id);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function updateProduct(productId) {
		    		productsService.findProduct(productId).then(function(response) {
		    			var product = response.data;
		    			if(vm.products != null) {
			    			for(var i = 0; i < vm.products.length; i++) {
			    				if(vm.products[i].id == productId) {
			    					vm.products[i] = product;
			    					break;
			    				}
			    			}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error editReceipe.service#updateProduct'); 
		  	   		 });
		    	}
		    	
		    	function findReceipesOfProductOfTypeInLastMonth() {
					vm.receipeDateFrom = dateUtilityService.oneMonthBack(new Date());
			    	vm.receipeDateUntil = new Date();
			    	var startDateString = dateUtilityService.formatDateToString(start);
		    		var endDateString = dateUtilityService.formatDateToString(end);
			    	vm.searchReceipeTerm = null;
			    	receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
			    		vm.receipesOfProduct = response.data;    			
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error editReceipe.controller#findReceipesOfProductOfTypeInLastMonth');
		 	   		 });
				}
		    	
		    	function searchReceipesInTimeRange() {
					var start = vm.receipeDateFrom instanceof Date ? dateUtilityService.formatDateToString(vm.receipeDateFrom) : vm.receipeDateFrom;
					var end = vm.receipeDateUntil instanceof Date ? dateUtilityService.formatDateToString(vm.receipeDateUntil) : vm.receipeDateUntil;
					receipeService.findReceipesOfProductOfTypeInRange(vm.product.id, vm.receipeType, start, end).then(function(response) {
						vm.receipesOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error editReceipe.service#searchReceipesInTimeRange');
		 	   		 });
				}
		    	
		    	function closeEditReceipeModal() {
					$modalInstance.dismiss('cancel');  
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('editReceipeToMixtureModalService', editReceipeToMixtureModalService);
        
    editReceipeToMixtureModalService.$inject = ['$modal', '$stateParams', 'productsService', 'receipeService'];
    
    function editReceipeToMixtureModalService($modal, $stateParams, productsService, receipeService) {
		var service = {
			showEditReceipeToMixtureModal: showEditReceipeToMixtureModal
		};		
		return service;
		
		////////////
				
		function showEditReceipeToMixtureModal(currentUser, product, products, lastReceipeOfProduct, userAuthorizationUserContainer, unityTypes) {
			 var editReceipeToMixtureModal = $modal.open({
				controller: EditReceipeToMixtureModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	lastReceipeOfProduct: function() {
			    		return lastReceipeOfProduct;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	unityTypes: function() {
			    		return unityTypes;
			    	}
			    }, 
				templateUrl: 'app/products/modals/editReceipeToMixtureModal/editReceipeToMixture.modal.html'
			});
			return editReceipeToMixtureModal;
			
			function EditReceipeToMixtureModalController($modalInstance, $scope, currentUser, product, products, lastReceipeOfProduct, userAuthorizationUserContainer, unityTypes) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.lastReceipeOfProduct = lastReceipeOfProduct;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.unityTypes = unityTypes;
		    	
		    	vm.storeEditReceipeToMixture = storeEditReceipeToMixture;
		    	vm.closeEditReceipeToMixtureModal = closeEditReceipeToMixtureModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	function storeEditReceipeToMixture() {
		    		vm.lastReceipeOfProduct.receipeType = 'CHANGED_MIXTURE';
		    		vm.lastReceipeOfProduct.id = null;
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		receipeService.createReceipe(vm.lastReceipeOfProduct).then(function(response) {
		    			updateProduct(vm.product.id); 
		    			vm.closeEditReceipeToMixtureModal();
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error editReceipeToMixture.service#storeEditReceipeToMixture'); 
		  	   		 });
		    	}
		    	
		    	function updateProduct(productId) {
		    		productsService.findProduct(productId).then(function(response) {
		    			var product = response.data;
		    			for(var i = 0; i < vm.products.length; i++) {
		    				if(vm.products[i].id == productId) {
		    					var selected = vm.products[i].selected;
		    					vm.products[i] = product;
		    					vm.products[i].selected = selected;
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error editReceipeToMixture.service#updateProduct'); 
		  	   		 });
		    	}
		    	
		    	function closeEditReceipeToMixtureModal() {
					$modalInstance.dismiss('cancel');  
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('editThinningModalService', editThinningModalService);
        
    editThinningModalService.$inject = ['$modal', '$stateParams', 'productsService', 'thinningService', 'dateUtilityService'];
    
    function editThinningModalService($modal, $stateParams, productsService, thinningService, dateUtilityService) { 
		var service = {
			showEditThinningModal: showEditThinningModal
		};		
		return service;
		
		////////////
				
		function showEditThinningModal(currentUser, product, products, thinningsOfProduct, start, end, userAuthorizationUserContainer, polymerPromille) {
			 var editThinningModal = $modal.open({
				controller: EditThinningModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	thinningsOfProduct: function() {
			    		return thinningsOfProduct;
			    	},
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	polymerPromille: function() {
			    		return polymerPromille;
			    	}
			    }, 
				templateUrl: 'app/products/modals/editThinningModal/editThinning.modal.html'
			});
			return editThinningModal;
			
			function EditThinningModalController($modalInstance, $scope, currentUser, product, products, thinningsOfProduct, start, end, userAuthorizationUserContainer, polymerPromille) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.thinningsOfProduct = prepareDateOfThinnings(thinningsOfProduct);
		    	vm.start = start;
		    	vm.end = end;
		    	vm.thinningDateFrom = start;
		    	vm.thinningDateUntil = end;
		    	vm.searchThinninigTerm = null;
		    	vm.searchThinningDateFromDatePicker = false;
		    	vm.searchThinningDateUntilDatePicker = false;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.polymerPromille = polymerPromille;
		    	
		    	vm.findThinningsOfProductOfTypeInLastMonth = findThinningsOfProductOfTypeInLastMonth;
		    	vm.openSearchThinningFromDatePicker = openSearchThinningFromDatePicker;
		    	vm.openSearchThinningUntilDatePicker = openSearchThinningUntilDatePicker;
		    	vm.deleteThinning = deleteThinning;
		    	vm.closeEditThinningModal = closeEditThinningModal;  
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	$scope.$watch('vm.thinningDateFrom', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.thinningDateFrom, vm.thinningDateUntil);
			    		if(vm.thinningDateFrom != null && vm.thinningDateUntil != null && checkDateRange == false) {
			    			searchThinningsInTimeRange();
			    		}
		    		}
		        });
		    	
		    	$scope.$watch('vm.thinningDateUntil', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.thinningDateFrom, vm.thinningDateUntil);
			    		if(vm.thinningDateFrom != null && vm.thinningDateUntil != null && checkDateRange == false) {
			    			searchThinningsInTimeRange();
			    		}
		    		}
		        });
		    	
		    	function prepareDateOfThinnings(thinningsOfProduct) {
		    		for(var i = 0; i < thinningsOfProduct.length; i++) {
		    			thinningsOfProduct[i].date = dateUtilityService.convertToDateOrUndefined(thinningsOfProduct[i].date);
		    		}
		    		return thinningsOfProduct;
		    	}
		    	
		    	function searchThinningsInTimeRange() {
		    		var start = dateUtilityService.formatDateToString(vm.thinningDateFrom);
					var end = dateUtilityService.formatDateToString(vm.thinningDateUntil);
					thinningService.findThinningsOfProductInRange(vm.product.id, start, end).then(function(response) {
						vm.thinningsOfProduct = prepareDateOfThinnings(response.data);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error editThinning.service#searchThinningsInTimeRange');
		 	   		 });
		    	}
		    	
		    	function findThinningsOfProductOfTypeInLastMonth() {
		    		vm.thinningDateFrom = dateUtilityService.oneMonthBack(new Date());
			    	vm.thinningDateUntil = new Date();
			    	var startDateString = dateUtilityService.formatDateToString(start);
		    		var endDateString = dateUtilityService.formatDateToString(end);
		    		vm.searchThinninigTerm = null;
			    	thinningService.findThinningsOfProductInRange(product.id, startDateString, endDateString).then(function(response) {
			    		vm.thinningsOfProduct = prepareDateOfThinnings(response.data);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error editThinning.controller#findThinningsOfProductOfTypeInLastMonth');
		 	   		 });
		    	}
		    	
		    	function openSearchThinningFromDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchThinningDateFromDatePicker = true; 
				}
		    	
		    	function openSearchThinningUntilDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchThinningDateUntilDatePicker = true; 
				}
		    	
		    	function deleteThinning(thinning) {
		    		for(var i = 0; i < vm.thinningsOfProduct.length; i++) {
		    			if(vm.thinningsOfProduct[i].id == thinning.id) {
		    				vm.thinningsOfProduct.splice(i, 1);
		    				updateProduct(vm.product.id);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function updateProduct(productId) {
		    		productsService.findProduct(productId).then(function(response) {
		    			var product = response.data;
		    			if(vm.products != null) {
			    			for(var i = 0; i < vm.products.length; i++) {
			    				if(vm.products[i].id == productId) {
			    					vm.products[i] = product;
			    					break;
			    				}
			    			}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error editThinning.service#updateProduct'); 
		  	   		 });
		    	}
		    	
		    	function closeEditThinningModal() {
					$modalInstance.dismiss('cancel'); 
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('historyOfReceipeModalService', historyOfReceipeModalService);
        
    historyOfReceipeModalService.$inject = ['$modal', '$stateParams', 'productsService', 'documentFileService', 'projectTemplateService', 'receipeService', 'dateUtilityService', 'printingPDFService', 'productOverviewModalService', 'printModalService'];
    
    function historyOfReceipeModalService($modal, $stateParams, productsService, documentFileService, projectTemplateService, receipeService, dateUtilityService, printingPDFService, productOverviewModalService, printModalService) {
		var service = {
			showHistoryOfReceipeModal: showHistoryOfReceipeModal
		};		
		return service;
		
		////////////
				
		function showHistoryOfReceipeModal(currentUser, receipeType, product, receipesOfProduct, start, end) { 
			 var historyOfReceipeModal = $modal.open({
				controller: HistoryOfReceipeModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	receipeType: function() {
			    		return receipeType;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	receipesOfProduct: function() {
			    		return receipesOfProduct;
			    	},
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	}
			    }, 
				templateUrl: 'app/products/modals/historyOfReceipeModal/historyOfReceipe.modal.html'
			});
			return historyOfReceipeModal;
			
			function HistoryOfReceipeModalController($modalInstance, $scope, currentUser, receipeType, product, receipesOfProduct, start, end) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.receipeType = receipeType;
		    	vm.product = product;
		    	vm.receipesOfProduct = receipesOfProduct;
		    	vm.searchReceipeDateFromDatePicker = false;
		    	vm.searchReceipeDateUntilDatePicker = false;
		    	vm.receipeDateFrom = start;
		    	vm.receipeDateUntil = end;
		    	
		    	vm.printContainer = printContainer;
		    	vm.openSearchReceipeFromDatePicker = openSearchReceipeFromDatePicker;
		    	vm.openSearchReceipeUntilDatePicker = openSearchReceipeUntilDatePicker;
		    	vm.findReceipesOfProductOfTypeInLastMonth = findReceipesOfProductOfTypeInLastMonth;
		    	vm.createDocumentUrl = createDocumentUrl;
		    	vm.showTemplateModal = showTemplateModal;
		    	vm.showProductOverviewModalService = showProductOverviewModalService;
		    	vm.closeHistoryOfReceipeModal = closeHistoryOfReceipeModal;
		    	vm.showPrintModal = showPrintModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	$scope.$watch('vm.receipeDateFrom', function (newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.receipeDateFrom, vm.receipeDateUntil);
			    		if(vm.receipeDateFrom != null && vm.receipeDateUntil != null && checkDateRange == false) {
			    			searchReceipesInTimeRange();
			    		}
		    		}
		        });
		    	
		    	$scope.$watch('vm.receipeDateUntil', function (newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.receipeDateFrom, vm.receipeDateUntil);
			    		if(vm.receipeDateFrom != null && vm.receipeDateUntil != null && checkDateRange == false) {
			    			searchReceipesInTimeRange();
			    		}
		    		}
		        });
		    	
		    	function printContainer(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	}
		    	
		    	function openSearchReceipeFromDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchReceipeDateFromDatePicker = true; 
				}
		    	
		    	function openSearchReceipeUntilDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchReceipeDateUntilDatePicker = true; 
				}
				
				function searchReceipesInTimeRange() {
					var start = vm.receipeDateFrom instanceof Date ? dateUtilityService.formatDateToString(vm.receipeDateFrom) : vm.receipeDateFrom;
					var end = vm.receipeDateUntil instanceof Date ? dateUtilityService.formatDateToString(vm.receipeDateUntil) : vm.receipeDateUntil;
					receipeService.findReceipesOfProductOfTypeInRange(vm.product.id, vm.receipeType, start, end).then(function(response) {
						vm.receipesOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfReceipe.service#searchReceipesInTimeRange');
		 	   		 });
				}
				
				function findReceipesOfProductOfTypeInLastMonth() {
					vm.receipeDateFrom = dateUtilityService.oneMonthBack(new Date());
			    	vm.receipeDateUntil = new Date();
			    	var startDateString = dateUtilityService.formatDateToString(vm.receipeDateFrom);
		    		var endDateString = dateUtilityService.formatDateToString(vm.receipeDateUntil);
			    	vm.searchReceipeTerm = null;
					receipeService.findReceipesOfProductOfTypeInRange(vm.product.id, vm.receipeType, startDateString, endDateString).then(function(response) {
						vm.receipesOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfReceipe.controller#findAllReceipesOfProductOfType');
		 	   		 });
				}
		    	
		    	function createDocumentUrl(user, product, receipeAttachment) {		    	
		    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
		    		return 'api/filedownloads/filedownload/' + user.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
		    	}
		    	
		    	function showTemplateModal(url, receipeAttachment) {
		    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
		    			var documentFile = response.data;
		    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
		    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error createReceipe.directive#getProductsByTerm'); 
		  	   		 });
		    	}
		    	
		    	function showProductOverviewModalService(product) {
		    		productsService.findProduct(product.id).then(function(response) {
		    			productOverviewModalService.showProductOverviewModal(response.data, vm);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfReceipe.service#showProductOverviewModalService');
		 	   		 });
		    	}
		    	
		    	function showPrintModal(receipeOfProduct) {
		    		printModalService.showPrintModal(vm.product, receipeOfProduct, vm.receipeType, vm.currentUser);
		    	}
		    	
		    	function closeHistoryOfReceipeModal() {
					$modalInstance.dismiss('cancel');  
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('historyOfThinningModalService', historyOfThinningModalService);
        
    historyOfThinningModalService.$inject = ['$modal', '$stateParams', 'productsService', 'documentFileService', 'projectTemplateService', 'receipeService', 'dateUtilityService', 'printingPDFService', 'productOverviewModalService', 'thinningService', 'printThinningModalService'];
    
    function historyOfThinningModalService($modal, $stateParams, productsService, documentFileService, projectTemplateService, receipeService, dateUtilityService, printingPDFService, productOverviewModalService, thinningService, printThinningModalService) {
		var service = {
			showHistoryOfThinningModal: showHistoryOfThinningModal
		};		
		return service;
		
		////////////
				
		function showHistoryOfThinningModal(currentUser, product, thinningsOfProduct, start, end, polymerPromille) { 
			 var historyOfThinningModal = $modal.open({
				controller: HistoryOfThinnigModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	thinningsOfProduct: function() {
			    		return thinningsOfProduct;
			    	},
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	}, 
			    	polymerPromille: function() {
			    		return polymerPromille;
			    	}
			    }, 
				templateUrl: 'app/products/modals/historyOfThinningModal/historyOfThinning.modal.html'
			});
			return historyOfThinningModal;
			
			function HistoryOfThinnigModalController($modalInstance, $scope, currentUser, product, thinningsOfProduct, start, end, polymerPromille) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.thinningsOfProduct = thinningsOfProduct;
		    	vm.thinningDateFrom = start;
		    	vm.thinningDateUntil = end;
		    	vm.searchThinningTerm = null;
		    	vm.searchThinningDateFromDatePicker = false;
		    	vm.searchThinningDateUntilDatePicker = false;
		    	vm.polymerPromille = polymerPromille;
		    	
		    	vm.printContainer = printContainer;
		    	vm.openSearchThinningFromDatePicker = openSearchThinningFromDatePicker;
		    	vm.openSearchThinningUntilDatePicker = openSearchThinningUntilDatePicker;
		    	vm.findThinningsOfProductOfTypeInLastMonth = findThinningsOfProductOfTypeInLastMonth;
		    	vm.showPrintThinningModal = showPrintThinningModal;
		    	vm.closeHistoryOfThinningModal = closeHistoryOfThinningModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    
		    	$scope.$watch('vm.thinningDateFrom', function (newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.thinningDateFrom, vm.thinningDateUntil);
			    		if(vm.thinningDateFrom != null && vm.thinningDateUntil != null && checkDateRange == false) {
			    			searchThinningsInTimeRange();
			    		}
		    		}
		        });
		    	
		    	$scope.$watch('vm.thinningDateUntil', function (newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.thinningDateFrom, vm.thinningDateUntil);
			    		if(vm.thinningDateFrom != null && vm.thinningDateUntil != null && checkDateRange == false) {
			    			searchThinningsInTimeRange();
			    		}
		    		}
		        });
		    	
		    	function searchThinningsInTimeRange() {
					var start = dateUtilityService.formatDateToString(vm.thinningDateFrom);
					var end = dateUtilityService.formatDateToString(vm.thinningDateUntil);
					thinningService.findThinningsOfProductInRange(vm.product.id, start, end).then(function(response) {
						vm.thinningsOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfThinning.service#searchThinningsInTimeRange');
		 	   		 });
				}
		    	
		    	function printContainer(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	}
		    	
		    	function findThinningsOfProductOfTypeInLastMonth() {
					vm.thinningDateFrom = dateUtilityService.oneMonthBack(new Date());
			    	vm.thinningDateUntil = new Date();
			    	var startDateString = dateUtilityService.formatDateToString(vm.thinningDateFrom);
		    		var endDateString = dateUtilityService.formatDateToString(vm.thinningDateUntil);
			    	vm.searchThinningTerm = null;
			    	thinningService.findThinningsOfProductInRange(vm.product.id, startDateString, endDateString).then(function(response) {
						vm.thinningsOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error historyOfThinning.controller#findThinningsOfProductOfTypeInLastMonth');
		 	   		 }); 
				}
		    	
		    	function openSearchThinningFromDatePicker($event) { 
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchThinningDateFromDatePicker = true; 
				}
		    	
		    	function openSearchThinningUntilDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchThinningDateUntilDatePicker = true; 
				}
		    	
		    	function showPrintThinningModal(thinningOfProduct) {
		    		printThinningModalService.showPrintThinningModal(vm.product, thinningOfProduct, vm.currentUser, vm.polymerPromille);
		    	}
		    	
		    	function closeHistoryOfThinningModal() {
					$modalInstance.dismiss('cancel');  
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('printModalService', printModalService);
        
    printModalService.$inject = ['$modal', '$stateParams'];
    
    function printModalService($modal, $stateParams) {
		var service = {
			showPrintModal: showPrintModal
		};		
		return service;
		
		////////////
				
		function showPrintModal(product, receipeOfProduct, receipeType, currentUser) {   
			 var printModal = $modal.open({
				controller: PrintModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	},
			    	receipeOfProduct: function() {
			    		return receipeOfProduct;
			    	}, 
			    	receipeType: function() {
			    		return receipeType;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}
			    }, 
				templateUrl: 'app/products/modals/printModal/print.modal.html'
			});
			return printModal;
			
			function PrintModalController($modalInstance, $scope, product, receipeOfProduct, receipeType, currentUser) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.receipeOfProduct = receipeOfProduct;
		    	vm.receipeType = receipeType;
		    	vm.currentUser = currentUser;
		    	vm.urlsWithMetadata = [];
		    	vm.documentName = null;
		    	vm.ending = null;
		    	vm.version = null;
		    	vm.date = new Date();
		    	createDocumentUrl();
		    	
		    	vm.closePrintModal = closePrintModal;
		    	
		    	////////////
		    	
		    	function createDocumentUrl() { 
		    		if(vm.receipeOfProduct.receipeAttachments == null || vm.receipeOfProduct.receipeAttachments.length == 0) {
		    			return;
		    		}
		    		for(var i = 0; i < vm.receipeOfProduct.receipeAttachments.length; i++) {
		    			var version = vm.receipeOfProduct.receipeAttachments[i].version === undefined ? '_Version1' : vm.receipeOfProduct.receipeAttachments[i].version;
		    			var url = 'api/filedownloads/filedownload/' + currentUser.id + '/' + vm.receipeOfProduct.receipeAttachments[i].documentFileId + '/' + vm.receipeOfProduct.receipeAttachments[i].version + '/';
		    			var preparedUrl = preparePdfUrl(url, vm.receipeOfProduct.receipeAttachments[i].ending);
		    			var urlWithMetadata = {};
		    			urlWithMetadata.url = preparedUrl;
		    			urlWithMetadata.documentName = vm.receipeOfProduct.receipeAttachments[i].filename;
		    			urlWithMetadata.ending = vm.receipeOfProduct.receipeAttachments[i].ending;
		    			urlWithMetadata.version = vm.receipeOfProduct.receipeAttachments[i].version;
		    			vm.urlsWithMetadata.push(urlWithMetadata);
		    		}
		    		$scope.pdfUrl = vm.urlsWithMetadata[0].url; 
		    		vm.documentName = vm.urlsWithMetadata[0].documentName;
			    	vm.ending = vm.urlsWithMetadata[0].ending;
			    	vm.version = vm.urlsWithMetadata[0].version;
		    	}
		    	
		    	function preparePdfUrl(pdfUrl, ending) {
		    		if(ending.indexOf('pdf') === -1 && ending.indexOf('PDF') === -1) {
		    			return pdfUrl + 'previewpdf';
		    		}
		    		return pdfUrl;
		    	}
		    	
		    	function closePrintModal() {
		    		$modalInstance.dismiss('cancel');  
		    	}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('printThinningModalService', printThinningModalService);
        
    printThinningModalService.$inject = ['$modal', '$stateParams'];
    
    function printThinningModalService($modal, $stateParams) {
		var service = {
			showPrintThinningModal: showPrintThinningModal
		};		
		return service;
		
		////////////
				
		function showPrintThinningModal(product, thinningOfProduct, currentUser, polymerPromille) {   
			 var printThinningModal = $modal.open({
				controller: PrintThinningModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	}, 
			    	thinningOfProduct: function() {
			    		return thinningOfProduct;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}, 
			    	polymerPromille: function() {
			    		return polymerPromille;
			    	}
			    }, 
				templateUrl: 'app/products/modals/printThinningModal/printThinning.modal.html'
			});
			return printThinningModal;
			
			function PrintThinningModalController($modalInstance, $scope, product, thinningOfProduct, currentUser, polymerPromille) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.thinningOfProduct = thinningOfProduct;
		    	vm.currentUser = currentUser;
		    	vm.date = new Date();
		    	vm.polymerPromille = polymerPromille;
		    	
		    	vm.closePrintThinningModal = closePrintThinningModal;
		    	
		    	////////////
		    	
		    	function closePrintThinningModal() {
		    		$modalInstance.dismiss('cancel');  
		    	}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('productOverviewModalService', productOverviewModalService);
        
    productOverviewModalService.$inject = ['$modal', '$stateParams'];
    
    function productOverviewModalService($modal, $stateParams) {
		var service = {
			showProductOverviewModal: showProductOverviewModal
		};		
		return service;
		
		////////////
				
		function showProductOverviewModal(product, invoker) {
			 var productOverviewModal = $modal.open({
				controller: ProductOverviewModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/products/modals/productOverviewModal/productOverview.modal.html'
			});
			return productOverviewModal;
			
			function ProductOverviewModalController($modalInstance, $scope, product, invoker) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.invoker = invoker; 
		    	
		    	vm.closeProductOverviewModal = closeProductOverviewModal;  
		    	
		    	////////////
		    	
		    	function closeProductOverviewModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('productsOfTenantModalService', productsOfTenantModalService);
        
    productsOfTenantModalService.$inject = ['$modal', '$stateParams'];
    
    function productsOfTenantModalService($modal, $stateParams) {
		var service = {
			showProductsOfTenantModal: showProductsOfTenantModal
		};		
		return service;
		
		////////////
				
		function showProductsOfTenantModal(products) {
			 var productsOfTenantModal = $modal.open({
				controller: ProductsOfTenantModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	products: function() {
			    		return products;
			    	}
			    }, 
				templateUrl: 'app/products/modals/productsOfTenantModal/productsOfTenant.modal.html'
			});
			return productsOfTenantModal;
			
			function ProductsOfTenantModalController($modalInstance, $scope, products) {
		    	var vm = this; 
		    	vm.products = products;
		    	vm.orderType = 'name';

		    	vm.closeProductsOfTenantModal = closeProductsOfTenantModal;
		    	
		    	////////////
		    	
		    	function closeProductsOfTenantModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('yesNoDeleteProductModalService', yesNoDeleteProductModalService);
        
    yesNoDeleteProductModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoDeleteProductModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(product, invoker, contactsReferencedToProduct) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contactsReferencedToProduct: function() {
			    		return contactsReferencedToProduct;
			    	}
			    }, 
				templateUrl: 'app/products/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, product, invoker, contactsReferencedToProduct) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.invoker = invoker;
		    	vm.contactsReferencedToProduct = contactsReferencedToProduct;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeProductYesNoModal = closeProductYesNoModal; 
		    	
		    	////////////
		    	
		    	function yes() {
		    		invoker.deleteProduct(vm.product); 
		    		vm.closeProductYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeProductYesNoModal();
		    	}
		    	
		    	function closeProductYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.controller('ProductsController', ProductsController);
    
    ProductsController.$inject = ['$scope', '$timeout', '$window', '$location', '$anchorScroll', 'currentUser', 'amountOfProducts', 'userAuthorizationUserContainer', 'productTypes', 'clickedProduct', 'unityTypes', 'productAggregateConditionTypes', 'productId', 'roles', 'usersOfProject', 'currentThinningPolymerDateTimeValue', 'productsService', 'yesNoDeleteProductModalService', 'changeProductPropertiesModalService', 'createReceipeModalService', 'editReceipeModalService', 'historyOfReceipeModalService', 'receipeService', 'editReceipeToMixtureModalService', 'createThinningModalService', 'editThinningModalService', 'historyOfThinningModalService', 'dateUtilityService', 'printingPDFService', 'contactsService', 'uploadService', 'thinningService', 'utilService', 'documentInformationModalService', 'documentFileService', 'projectTemplateService'];
       
    function ProductsController($scope, $timeout, $window, $location, $anchorScroll, currentUser, amountOfProducts, userAuthorizationUserContainer, productTypes, clickedProduct, unityTypes, productAggregateConditionTypes, productId, roles, usersOfProject, currentThinningPolymerDateTimeValue, productsService, yesNoDeleteProductModalService, changeProductPropertiesModalService, createReceipeModalService, editReceipeModalService, historyOfReceipeModalService, receipeService, editReceipeToMixtureModalService, createThinningModalService, editThinningModalService, historyOfThinningModalService, dateUtilityService, printingPDFService, contactsService, uploadService, thinningService, utilService, documentInformationModalService, documentFileService, projectTemplateService) {
	    $scope.vm = this;   
    	var vm = this;
    	
    	vm.products = [];
    	vm.clickedProduct = clickedProduct != null ? clickedProduct.data : null;
    	vm.currentUser = currentUser;
    	vm.userAuthorizationUserContainer = vm.currentUser.userAuthorizationUserContainer;
    	vm.productTypes = productTypes != null ? productTypes.data : null;
    	vm.amountOfProducts = amountOfProducts != null ? amountOfProducts.data : 0;
    	vm.unityTypes = unityTypes.data;
    	vm.productId = productId;
    	vm.productAggregateConditionTypes = productAggregateConditionTypes.data;
    	vm.roles = roles.data;
    	vm.usersOfProject = usersOfProject.data;
    	vm.showProductCreationTrigger = false;
    	vm.orderType = 'name';
    	vm.uploadFiles = false;
    	vm.searchProductTerm = null;
    	vm.searchProductOverDocumentsTerm = null;
    	vm.loadingProducts = false;
    	vm.reloadProducts = false;
    	vm.page = 0;
    	vm.files = [];
    	vm.successfulUploadedFiles = 0;
    	vm.failedUploadedFiles = 0;
    	vm.percentageResultIs100 = false;
    	vm.searchReceipesTrigger = false;
    	vm.product = {
    		number: '',
    		name: '',
			names: [],
			description: '',
			chemicalDescription: '',
			density: '',
			productFillingWeights: [],
			color: '#007ab9', 
			developmentDate: null,
			types: [],
			position: '',
			mixtureInstructions: null,
			contactsProduct: [],
			creationDate: null,
			creationUserId: ''
			};
    	vm.receipe = {
    		productId: '',
    		date: new Date(),
    		receipeProducts: [],
    		information: '',
    		receipeAttachments: [],
    		receipeType: null
    	};
    	vm.receipeToSearch = {
        		productId: '',
        		date: new Date(),
        		receipeProducts: [],
        		information: '',
        		receipeAttachments: [],
        		receipeType: null
        	};
    	vm.thinning = {
        		productId: '',
        		date: new Date(),
        		ironContent: null,
        		targetContent: null,
        		density: null,
        		amountLiter: null,
        		amount: null,
        		iron: null,
        		water: null,
        		polymer: false,
        		ascorbinSaeure: false,
        		polymerSubtract: 1000,
        		polymerPromille: currentThinningPolymerDateTimeValue.data.value,
        		ascorbinSaeureSubtract: 2700,
        		waterAmountCalculated: null,
        		informationTop: null,
        		information: null,
        		labelLeft1: 'Fe- Gehalt',
				labelLeft2: 'Fe- Gehalt',
				labelRight1: 'Eisenlösung'
        	};
    	vm.polymerPromille = currentThinningPolymerDateTimeValue.data.value;

    	vm.loadingPagedProducts = loadingPagedProducts;
    	vm.printContainer = printContainer;
    	vm.createProductNotifier = createProductNotifier;
    	vm.removeSearchResults = removeSearchResults;
    	vm.deleteProduct = deleteProduct;
    	vm.createReceipe = createReceipe;
    	vm.editAndDeleteReceipe = editAndDeleteReceipe;
    	vm.editReceipeToMixture = editReceipeToMixture;
    	vm.editAndDeleteMixture = editAndDeleteMixture;
    	vm.historyOfReceipe = historyOfReceipe;
    	vm.createThinning = createThinning;
    	vm.editAndDeleteThinning = editAndDeleteThinning;
    	vm.historyOfThinning = historyOfThinning;
    	vm.gotoTop = gotoTop;
    	vm.productClicked = productClicked;
    	vm.addReceipeProduct = addReceipeProduct;
    	vm.removeReceipeProduct = removeReceipeProduct;
    	vm.getProductName = getProductName;
    	vm.getProductsByTerm = getProductsByTerm;
    	vm.productSelected = productSelected;
    	vm.checkIfPercentageResultIs100 = checkIfPercentageResultIs100;
    	vm.searchProductsWithSameReceipe = searchProductsWithSameReceipe;
    	vm.showChangeProductPropertiesModalService = showChangeProductPropertiesModalService;
    	vm.showYesNoDeleteProductModal = showYesNoDeleteProductModal;
    	vm.showTemplateModal = showTemplateModal;
    	vm.createDocumentUrl = createDocumentUrl;
    	vm.updateDocumentFile = updateDocumentFile;
    	vm.hideAddressWithProducts = hideAddressWithProducts;
    	vm.searchProducts = searchProducts;
    	vm.searchProductOverDocumentFiles = searchProductOverDocumentFiles;
    	vm.showDocumentInformationModal = showDocumentInformationModal;
    	vm.receipeSearchDisabled = receipeSearchDisabled;
    	loadProducts(vm.page);
    	
    	//////////////
    	
    	$scope.$watch('vm.orderType', function(newValue, oldValue) {
    		if(oldValue != newValue) {
    			removeSearchResults();
    		} 
		});
    	
    	$scope.$watch('vm.files', function () {
    		if(vm.products != null && vm.files != null && vm.files.length > 0) {
	    		for(var i = 0; i < vm.products.length; i++) {
	    			uploadService.uploadFiles(vm.files, currentUser.id, vm.products[i].id, '', 'PRODUCT', successCallback, errorCallback, progressCallback);
	    		}
    		}
        });
    	
    	function successCallback(response) {
    		setFileUpdateTimeout();
    		vm.successfulUploadedFiles += 1;
    		for(var i = 0; i < vm.products.length; i++) {
    			if(vm.products[i].id == response.data.projectProductId) {
    				vm.products[i].uploadedDocumentFileSuccessful = true;
    				setUploadedDocumentFileSuccessfulTimeout(vm.products[i]);
    				break;
    			}
    		}
    	}
    	
    	function errorCallback(response) {
    		vm.failedUploadedFiles += 1;
    	}
    	
    	function progressCallback(log) {
    		vm.uploadFiles = true;
    		console.log('progressCallback: ' + log);
    	}
    	
    	function setTimeout() {
      		$timeout(function() {
      			vm.noLastReceipeExists = false;
  			   }, 2000); 
      	}
    	
    	function setFileUpdateTimeout() {
      		$timeout(function() {
      			vm.uploadFiles = false;
  			   }, 2500); 
      	}
    	
    	function hideAddressWithProducts(addressWithProducts) {
			var address = addressWithProducts.address;
			if(address == null) {
				return false;
			}
			if(address.userIdBlackList != null && address.userIdBlackList.indexOf(vm.currentUser.id) !== -1) {
				return true;
			}
			if(address.rolesBlackList != null && address.rolesBlackList.indexOf(vm.currentUser.projectUserConnectionRole) !== -1) {
				return true;
			}
			return false;
		}
    	
    	function setUploadedDocumentFileSuccessfulTimeout(product) {
      		$timeout(function() {
      			product.uploadedDocumentFileSuccessful = false;
  			   }, 8500); 
      	}
    	
    	function productClicked(product) {
    		return product.id;
    	}
    	
    	function searchProductOverDocumentFiles() {
    		if(vm.searchProductOverDocumentsTerm != null && vm.searchProductOverDocumentsTerm.length > 3) {
    			productsService.findProductsByDocumentFileSearchTerm(vm.searchProductOverDocumentsTerm).then(function(response) {
    				vm.products = response.data;
    				for(var i = 0; i < vm.products.length; i++) {
    					vm.products[i].uploadDocumentFile = true;
    					setContactOrUser(vm.products[i].documentFiles);
    				}
					vm.loadingProducts = false;
		    		vm.page = 0;
     	   		 }, function errorCallback(response) {
     	   			 console.log('error products.controller#searchProductOverDocumentFiles');
     	   		 });
    		}
    	}
    	
    	function searchProducts() {
			if(vm.searchProductTerm != null && vm.searchProductTerm.length > 2) {
				var searchTerm = utilService.prepareSearchTerm(vm.searchProductTerm);
				vm.loadingProducts = true;
				vm.orderType = 'name';
				productsService.findProductsByTerm(searchTerm).then(function(response) {
					vm.products = response.data;
					vm.loadingProducts = false;
		    		vm.page = 0;
	  	   		 }, function errorCallback(response) {
	  	   			 console.log('error products.controller#searchProductTerm');
	  	   		 });
			}
		}
    	
    	function removeSearchResults() { 
    		vm.searchProductTerm = null;
    		vm.searchProductOverDocumentsTerm = null;
    		vm.clickedProduct = null;
    		vm.reloadProducts = false;
    		vm.searchReceipesTrigger = false;
    		resetReceipeToSearch();
    		vm.products = [];
    		vm.page = 0;
    		loadProducts(vm.page);
    	}
    	
    	function getAmountOfProducts() {
    		productsService.getAmountOfProducts().then(function(response) {
    			vm.amountOfProducts = response.data;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error products.controller#getAmountOfProducts');
  	   		 });
    	}
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function loadingPagedProducts() {
    		if(vm.loadingProducts == false && vm.searchProductOverDocumentsTerm == null && vm.searchProductTerm == null && vm.searchReceipesTrigger == false) {
    			vm.loadingProducts = true;
	    		vm.page = vm.page + 1;
	    		loadProducts(vm.page);
	    		vm.loadingProducts = false;
    		}
    	}
    	
    	function loadProducts(page) {
    		if(vm.clickedProduct != null) {
    			if(vm.products == null) {
    				vm.products = [];
    			}
    			for(var i = 0; i < vm.products.length; i++) {
    				if(vm.products[i].id == vm.clickedProduct.id) {
    					return;
    				}
    			}
    			vm.products.push(vm.clickedProduct); 
    		} else if(vm.amountOfProducts > vm.products.length) {
    			if(vm.products == null) {
    				vm.products = [];
    			}
	    		productsService.getPagedProductsOfTenant(page, vm.orderType).then(function(response) {
	    			var loadedProducts = response.data;
	    			if(loadedProducts != null && loadedProducts.length > 0) {
	    				vm.products.push.apply(vm.products, loadedProducts);
	    				for(var k = 0; k < loadedProducts.length; k++) {
	    					if(loadedProducts[k].id == vm.productId) {
	    						loadedProducts[k].selected = true;
	    						break;
	    					}
	    				}
		    			console.log("push products:" + vm.products.length + ', ' + loadedProducts.length);
	    			}
	  	   		 }, function errorCallback(response) {
	  	   			 console.log('error products.controller#loadProducts');
	  	   		 });
    		}
    	}
    	
    	function createProductNotifier() {
    		getAmountOfProducts();
    		vm.reloadProducts = true;
    	}
    	
    	function deleteProduct(product) {
    		productsService.deleteProduct(product.id).then(function(response) {
    			removeProduct(product);
    			getAmountOfProducts();
    			vm.reloadProducts = true;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error products.controller#deleteProduct');
  	   		 });
    	}
    	
    	function removeProduct(product) {
    		for(var i = 0; i < vm.products.length; i++) {
    			if(vm.products[i].id == product.id) {
    				vm.products.splice(i, 1);
    				break;
    			}
    		}
    	}
    	
    	function createReceipe(product, receipeType) {
    		receipeService.findAllReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var allReceipesOfProduct = response.data;
    			createReceipeModalService.showCreateReceipeModal(vm.currentUser, product, vm.products, vm.receipe, allReceipesOfProduct, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#createReceipe');
 	   		 });
    	}
    	
    	function editAndDeleteReceipe(product, receipeType) {
    		receipeService.findAllReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var receipesOfProduct = response.data;
    			var start = receipesOfProduct != null && receipesOfProduct.length > 0 ? receipesOfProduct[receipesOfProduct.length-1].date : new Date();
    			var end = dateUtilityService.formatDateToString(new Date());
    			editReceipeModalService.showEditReceipeModal(vm.currentUser, product, vm.products, receipesOfProduct, receipeType, start, end, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#editAndDeleteReceipe');
 	   		 });
    	}
    	
    	function editReceipeToMixture(product, receipeType, $event) {  
    		// get last standard receipe of product
    		$event.stopPropagation();
    		receipeService.findLastReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var lastReceipeOfProduct = response.data;
    			if(lastReceipeOfProduct == '') {
    				vm.noLastReceipeExists = true;
    				setTimeout();
    			} else {
    				lastReceipeOfProduct.date = new Date();
    				editReceipeToMixtureModalService.showEditReceipeToMixtureModal(vm.currentUser, product, vm.products, lastReceipeOfProduct, vm.userAuthorizationUserContainer, vm.unityTypes);
    			}
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#editReceipeToMixture');
 	   		 });
    	}
    	
    	function createThinning(product, $event) { 
    		thinningService.findAllThinningsOfProduct(product.id).then(function(response) {
    			var allThinningsOfProduct = response.data;
    			createThinningModalService.showCreateThinningModal(vm.currentUser, product, vm.products, vm.thinning, allThinningsOfProduct, vm.userAuthorizationUserContainer, vm.polymerPromille);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#createReceipe');
 	   		 });
    	}
    	
    	function editAndDeleteThinning(product) { 
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		thinningService.findThinningsOfProductInRange(product.id, startDateString, endDateString).then(function(response) {
    			var thinningsOfProduct = response.data;
    			editThinningModalService.showEditThinningModal(vm.currentUser, product, vm.products, thinningsOfProduct, start, end, vm.userAuthorizationUserContainer, vm.polymerPromille);
    		}, function errorCallback(response) {
	   			 console.log('error products.controller#editAndDeleteThinning');
	   		 });
    	}
    	
    	function historyOfThinning(product) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		thinningService.findThinningsOfProductInRange(product.id, startDateString, endDateString).then(function(response) {
    			var thinningsOfProduct = response.data;
    			historyOfThinningModalService.showHistoryOfThinningModal(vm.currentUser, product, thinningsOfProduct, start, end, vm.polymerPromille);
    		}, function errorCallback(response) {
	   			 console.log('error products.controller#historyOfThinning');
	   		 });
    	}
    	
    	function historyOfReceipe(product, receipeType) {
    		if(receipeType == 'STANDARD_MIXTURE') {
	    		receipeService.findAllReceipesOfProductOfType(product.id, receipeType).then(function(response) {
	    			var receipesOfProduct = response.data;
	    			var start = receipesOfProduct != null && receipesOfProduct.length > 0 ? receipesOfProduct[receipesOfProduct.length-1].date : new Date();
	    			var end = dateUtilityService.formatDateToString(new Date());
	    			historyOfReceipeModalService.showHistoryOfReceipeModal(vm.currentUser, receipeType, product, receipesOfProduct, start, end);
	 	   		 }, function errorCallback(response) {
	 	   			 console.log('error products.controller#historyOfReceipe');
	 	   		 });
    		} else if(receipeType == 'CHANGED_MIXTURE') {
    			var end = new Date();
        		var start = dateUtilityService.oneMonthBack(end);
        		var startDateString = dateUtilityService.formatDateToString(start);
        		var endDateString = dateUtilityService.formatDateToString(end);
        		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
        			var receipesOfProduct = response.data;
        			historyOfReceipeModalService.showHistoryOfReceipeModal(vm.currentUser, receipeType, product, receipesOfProduct, start, end);
     	   		 }, function errorCallback(response) {
     	   			 console.log('error products.controller#historyOfReceipe');
     	   		 });
    		}
    	}
    	
    	function editAndDeleteMixture(product, receipeType, $event) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
    			var receipesOfProduct = response.data;
    			editReceipeModalService.showEditReceipeModal(vm.currentUser, product, vm.products, receipesOfProduct, receipeType, start, end, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#editAndDeleteMixture');
 	   		 });
    	}
    	
    	function showChangeProductPropertiesModalService(product) {
    		changeProductPropertiesModalService.showChangeProductPropertiesModal(product, vm.products, vm.productTypes, vm.currentUser, vm.productAggregateConditionTypes, vm.roles, vm.usersOfProject);
    	}
    	
    	function showYesNoDeleteProductModal(product) {
    		contactsService.getContactsReferencedToProduct(product.id).then(function(response) {
    			var contactsReferencedToProduct = response.data;
    			yesNoDeleteProductModalService.showYesNoModal(product, vm, contactsReferencedToProduct);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error products.controller#showYesNoDeleteProductModal');
 	   		 });
    	}
    	
    	function setContactOrUser(pagedDocumentFiles) {
    		for(var i = 0; i < pagedDocumentFiles.length; i++) {
    			for(var j = 0; j < pagedDocumentFiles[i].documentFileVersions.length; j++) {
    				if(pagedDocumentFiles[i].documentFileVersions[j].responsibleUser != null) {
    					pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = pagedDocumentFiles[i].documentFileVersions[j].responsibleUser;
        			} else if(pagedDocumentFiles[i].documentFileVersions[j].responsibleContact != null) {
        				pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = pagedDocumentFiles[i].documentFileVersions[j].responsibleContact;
        			} else if(pagedDocumentFiles[i].documentFileVersions[j].responsibleText != null) {
        				pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact  = pagedDocumentFiles[i].documentFileVersions[j].responsibleText;
        			}
    			}
    		}
    		return pagedDocumentFiles;
    	}
    	
    	function addReceipeProduct() { 
    		var receipeProduct = {}; 
    		vm.receipeToSearch.receipeProducts.push(receipeProduct);
    	}
    	
    	function resetReceipeToSearch() {
    		vm.receipeToSearch = {
            		productId: '',
            		date: new Date(),
            		receipeProducts: [],
            		information: '',
            		receipeAttachments: [],
            		receipeType: null
            	};
    	}
    	
    	function removeReceipeProduct(index) {
    		vm.receipeToSearch.receipeProducts.splice(index, 1);
    		for(var i = 0; i < vm.receipeToSearch.receipeProducts.length; i++) {
    			vm.receipeToSearch.receipeProducts[i].selectedReceipeProduct = getProductName(vm.receipeToSearch.receipeProducts[i]);
    		}
    		vm.checkIfPercentageResultIs100(vm.receipeToSearch.receipeProducts);
    	}
    	
    	function getProductName(receipeProduct) { 
			if(receipeProduct == null || receipeProduct.product == null) { 
				return '';
			}
			return receipeProduct.product.name;
		}
    	
    	function getProductsByTerm(term) {
			return productsService.findProductsByTerm(term).then(function(response) {
	    		return response.data;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error products.controller#getProductsByTerm'); 
  	   		 });
		}
		
		function productSelected(selectedReceipeProduct, index) {
			var addedProduct = false;
    		for(var i = 0; i < vm.receipeToSearch.receipeProducts.length; i++) {
    			if(Object.keys(vm.receipeToSearch.receipeProducts[i]).length === 0) {
    				var receipeProduct = {};
    				receipeProduct.product = selectedReceipeProduct;
    				receipeProduct.percentage = null;
    				vm.receipeToSearch.receipeProducts[i] = receipeProduct;
    				addedProduct = true;
    				break;
    			}
    		}
    		if(addedProduct == false) {
    			vm.receipeToSearch.receipeProducts[index].product = selectedReceipeProduct;
    		}
    	}
		
		function checkIfPercentageResultIs100(receipeProducts) {
    		if(receipeProducts == null) { 
    			receipeProducts.percentageResult = 0;
    			vm.percentageResultIs100 = false;
    			return;
    		} 
    		var result = 0;
    		for(var i = 0; i < receipeProducts.length; i++) {
    			if(receipeProducts[i].percentage != null) {
    				result += receipeProducts[i].percentage;
    				receipeProducts[i].errorPercentage = false;
    			}
    		}
    		var resultInteger = parseFloat(result).toFixed(2);
    		if(resultInteger == parseFloat(100).toFixed(2)) {
    			receipeProducts.percentageResult = resultInteger;
    			vm.percentageResultIs100 = true;
    			return;
    		}
    		receipeProducts.percentageResult = resultInteger;
    		vm.percentageResultIs100 = false;
    	}
		
		function searchProductsWithSameReceipe() {
			productsService.getProductsWithSameReceipe(vm.receipeToSearch).then(function(response) { 
				vm.products = response.data;
			}, function errorCallback(response) {
 	   			  console.log('error products.controller.js#searchProductsWithSameReceipe');
 	   		 });
		}
    	
    	function gotoTop() {
    		 $location.hash('top'); 
    	     $anchorScroll();
    	}
    	
    	function showTemplateModal(url, documentFile, documentFileVersion) {
    		projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
    	}
    	
    	function updateDocumentFile(documentFile, updateDocumentFileResponsibility) {
    		documentFileService.updateDocumentFile(documentFile, documentFile.projectProductId).then(function(response) {
    			console.log('updated products in search.controller#updateDocumentFile');
  	   		 }, function errorCallback(response) {
  	   			  console.log('error products.controller.js#updateDocumentFile');
  	   		 });
    	} 
    	
    	function createDocumentUrl(user, projectId, documentFile, documentFileVersion) {
    		if(documentFile == null || documentFileVersion == null) {
    			return '';
    		}
    		var version = documentFileVersion === undefined ? '_Version1' : documentFileVersion.version;
    		return 'api/filedownloads/filedownload/' + user.id + '/' + documentFile.id + '/' + version + '/';
    	}
    	
    	function showDocumentInformationModal(documentFile, userAuthorizationUserContainer) {
    		documentInformationModalService.showDocumentInformationModal(documentFile, currentUser, userAuthorizationUserContainer, vm.type, vm, false);
    	}
    	
    	function receipeSearchDisabled() {
    		if(vm.receipeToSearch.receipeProducts.length == 0) {
    			return true;
    		}
    		for(var i = 0; i < vm.receipeToSearch.receipeProducts.length; i++) {
    			if(vm.receipeToSearch.receipeProducts[i].percentage == null || vm.receipeToSearch.receipeProducts[i].product == null) {
    				return true;
    			}
    		}
    		return false;
    	}
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('documentFileService', documentFileService);
        
    documentFileService.$inject = ['$http', 'api_config'];
    
    function documentFileService($http, api_config) { 
		var service = {
				searchPredefinedLabels: searchPredefinedLabels,
				findDocumentFileById: findDocumentFileById,
				getVersionObjectOfDocumentFile: getVersionObjectOfDocumentFile,
				countByProjectIdGroupByOriginalFileName: countByProjectIdGroupByOriginalFileName,
				countByProjectIdAndFolderIdGroupByOriginalFileName: countByProjectIdAndFolderIdGroupByOriginalFileName,
				findPagedDocumentFilesByProjectIdAndFolderId: findPagedDocumentFilesByProjectIdAndFolderId,
				findAssignedDocumentFilesByProjectId: findAssignedDocumentFilesByProjectId,
				findDocumentFilesBySearchString: findDocumentFilesBySearchString,
				findDocumentFilesBySearchStringOverAllProjectsAndProducts: findDocumentFilesBySearchStringOverAllProjectsAndProducts,
				findDocumentFilesByBySearchStringAndProjectIdAndFolderId: findDocumentFilesByBySearchStringAndProjectIdAndFolderId,
				calculateAmountOfPages: calculateAmountOfPages,
				updateDocumentFile: updateDocumentFile,
				updateDocumentFileLabels: updateDocumentFileLabels,
				deleteDocumentFile: deleteDocumentFile,
				getDocumentFilePageSize: getDocumentFilePageSize
		};
		return service;
		
		////////////
		
		function searchPredefinedLabels(searchString) {
			return $http.get(api_config.BASE_URL + '/documentfiles/labels/search/' + searchString);
		}
		
		function findDocumentFileById(id) {
			return $http.get(api_config.BASE_URL + '/documentfiles/' + id);
		}
		
		function getVersionObjectOfDocumentFile(documentFile, version) {
    		for(var i = 0; i < documentFile.documentFileVersions.length; i++) {
    			if(documentFile.documentFileVersions[i].version == version) {
    				return documentFile.documentFileVersions[i];
    			}
    		}
    		return null;
    	}
		
		function countByProjectIdGroupByOriginalFileName(projectId, onlyConfident, documentFileParentType) {
			return $http.get(api_config.BASE_URL + '/documentfiles/count/' + projectId + '/' + onlyConfident + '/' + documentFileParentType);
		}
		
		function countByProjectIdAndFolderIdGroupByOriginalFileName(projectId, folderId, onlyConfident, documentFileParentType) {
			return $http.get(api_config.BASE_URL + '/documentfiles/folder/count/' + projectId + '/' + onlyConfident + '/' + documentFileParentType, {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function findPagedDocumentFilesByProjectIdAndFolderId(projectId, folderId, page, sort, onlyConfident, documentFileParentType) { 
			return $http.get(api_config.BASE_URL + '/documentfiles/paged/' + projectId + '/' + page + '/' + sort + '/' + onlyConfident + '/' + documentFileParentType, {
				params: {
					folderid: folderId
				} 
			});
		}
		 
		function findAssignedDocumentFilesByProjectId(projectId, sort) {
			return $http.get(api_config.BASE_URL + '/documentfiles/' + projectId + '/' + sort + '/assigned');
		}
		
		function findDocumentFilesBySearchString(searchString) { 
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/' + searchString);
		}
		
		function findDocumentFilesBySearchStringOverAllProjectsAndProducts(searchString) { 
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/' + searchString + '/all');
		}
		
		function findDocumentFilesByBySearchStringAndProjectIdAndFolderId(searchString, projectId, folderId) { 
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/' + searchString + '/' + projectId, {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function calculateAmountOfPages(projectId, documentFileParentType, folderId) {
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/' + projectId + '/' + documentFileParentType + '/amountofpages', {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function updateDocumentFile(documentFile, projectId) {
			return $http.put(api_config.BASE_URL + '/documentfiles/documentfile/' + projectId, documentFile);
		}
		
		function updateDocumentFileLabels(documentFile, projectId) {
			return $http.put(api_config.BASE_URL + '/documentfiles/documentfile/labels/' + projectId, documentFile);
		}
		
		function deleteDocumentFile(documentFileId, projectId) {
			return $http.delete(api_config.BASE_URL + '/documentfiles/documentfile/' + projectId + '/' + documentFileId); 
		}
		
		function getDocumentFilePageSize() {
			return $http.get(api_config.BASE_URL + '/documentfiles/documentfile/documentpagesize');
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('folderService', folderService);
        
    folderService.$inject = ['$http', 'api_config'];
    
    function folderService($http, api_config) {
		var service = {
			create: create,
			update: update,
			findFolders: findFolders,
			findAllFoldersOfProject: findAllFoldersOfProject,
			findBreadCrumbOfFolder: findBreadCrumbOfFolder,
			findFoldersByBySearchStringAndProjectIdAndFolderId: findFoldersByBySearchStringAndProjectIdAndFolderId,
			deleteFolder: deleteFolder
		};
		return service;
		
		////////////
		
		function create(folder, documentFileParentType) {
			return $http.post(api_config.BASE_URL + '/folders/folder/' + documentFileParentType, folder);
		}
		
		function update(folder, documentFileParentType) {
			return $http.put(api_config.BASE_URL + '/folders/folder/' + documentFileParentType, folder);
		}
		
		function findFolders(projectId, superFolderId) {
			return $http.get(api_config.BASE_URL + '/folders/folder/' + projectId, {
				params: {
					superfolderid: superFolderId
				} 
			});
		}
		
		function findBreadCrumbOfFolder(folderId) {
			return $http.get(api_config.BASE_URL + '/folders/folder/breadcrumb/', {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function findAllFoldersOfProject(projectId) {
			return $http.get(api_config.BASE_URL + '/folders/folder/project/' + projectId);
		}
		
		function findFoldersByBySearchStringAndProjectIdAndFolderId(documentFileAndFolderSearchString, projectId, folderId) {
			return $http.get(api_config.BASE_URL + '/folders/folder/search/project/' + projectId + '/' + documentFileAndFolderSearchString, {
				params: {
					folderid: folderId
				} 
			});
		}
		
		function deleteFolder(folderId, documentFileParentType) {
			return $http.delete(api_config.BASE_URL + '/folders/folder/' + folderId + '/' + documentFileParentType);
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('labelsPredefinedService', labelsPredefinedService);
        
    labelsPredefinedService.$inject = ['$http', 'api_config'];
    
    function labelsPredefinedService($http, api_config) {
		var service = {
			createLabelPredefined: createLabelPredefined,
			getAllLabelsPredefined: getAllLabelsPredefined,
			deleteLabelPredefined: deleteLabelPredefined 
		};
		return service;
		
		////////////

		function createLabelPredefined(labelPredefined) {
			return $http.post(api_config.BASE_URL + '/labelspredefined', labelPredefined);
		}
		
		function getAllLabelsPredefined(userId) {
			return $http.get(api_config.BASE_URL + '/labelspredefined/' + userId);
		}
		
		function deleteLabelPredefined(label) {
			return $http.delete(api_config.BASE_URL + '/labelspredefined/' + label);
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('projectTemplateService', projectTemplateService);
        
    projectTemplateService.$inject = ['$http', 'api_config', 'pdfTemplateModalService', 'imgTemplateModalService', 'textTemplateModalService', 'htmlTemplateModalService', 'noTemplateModalService'];
    
    function projectTemplateService($http, api_config, pdfTemplateModalService, imgTemplateModalService, textTemplateModalService, htmlTemplateModalService, noTemplateModalService) {
    	var imgEndings = ['png', 'PNG', 'jpg', 'JPG', 'JPEG', 'jpeg'];
		var service = {
			showTemplateModal: showTemplateModal
		};
		return service;
		
		////////////
		
		function showTemplateModal(url, documentFile, documentFileVersion) {
    		if(documentFileVersion == null) {
    			return;
    		}
    		if(documentFile.ending.indexOf('pdf') > -1 || documentFile.ending.indexOf('PDF') > -1 || documentFile.ending.indexOf('docx') > -1) {
    			pdfTemplateModalService.showPdfTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			} else if(containsImgEnding(documentFile.ending)) {
				imgTemplateModalService.showImgTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			} else if(documentFile.ending.indexOf('msg') > -1 || documentFile.ending.indexOf('txt') > -1) {
				textTemplateModalService.showTextTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			} else if(documentFile.ending.indexOf('html') > -1) {
				htmlTemplateModalService.showHtmlTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			} else {
				noTemplateModalService.showNoTemplateModal(url, documentFile.fileName, documentFile.ending, documentFileVersion.version);
			}
    	}
		
		function containsImgEnding(ending) {
    		for(var i = 0; i < imgEndings.length; i++) {
    			if(imgEndings[i] === ending) {
    				return true;
    			}
    		}
    		return false;
    	}
    }
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.project')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getProjectState());
    	 
    	////////////
			    	
    	function getProjectState() {
    		var state = {
    			name: 'auth.project',
				url: '/project/:userId/:type/:projectUserConnectionId/:projectOrProductId/:folderId',
				templateUrl: 'app/project/project/project.html',
				controller: 'ProjectController',
				controllerAs: 'vm',
				resolve: {
					projectUserConnectionService: 'projectUserConnectionService',
					projectsService: 'projectsService',
					productsService: 'productsService',
					userService: 'userService',
					documentFileService: 'documentFileService',
					labelsPredefinedService: 'labelsPredefinedService',
					folderService: 'folderService',	
					type: function($stateParams) {
						return $stateParams.type;
					},
					projectOrProduct: function findProjectById(projectsService, productsService, $stateParams) {
						if($stateParams.type == 'PROJECT') {
							return projectsService.findProject($stateParams.projectOrProductId);
						} else if($stateParams.type == 'PRODUCT') {
							var projectOrProduct = productsService.findProduct($stateParams.projectOrProductId);
							return projectOrProduct;
						}
					},
					amountOfDocumentFilesWithUniqueName: function countByProjectIdGroupByOriginalFileName(documentFileService, $stateParams) {
						return documentFileService.countByProjectIdGroupByOriginalFileName($stateParams.projectOrProductId, false, $stateParams.type);
					},
					amountOfPages: function calculateAmountOfPages(documentFileService, $stateParams) {
						return documentFileService.calculateAmountOfPages($stateParams.projectOrProductId, $stateParams.type, $stateParams.folderId);
					}, 
					documentFilePageSize: function getDocumentFilePageSize(documentFileService) {
						return documentFileService.getDocumentFilePageSize(); 
					},
					pagedDocumentFiles: function getPagedDocumentFiles(documentFileService, $stateParams) {
						return documentFileService.findPagedDocumentFilesByProjectIdAndFolderId($stateParams.projectOrProductId, $stateParams.folderId, 0, 'fileName', false, $stateParams.type);
					},
					folders: function findFolders(folderService, $stateParams) {
						return folderService.findFolders($stateParams.projectOrProductId, $stateParams.folderId);
					},
					folderId: function getFolderId($stateParams) {
						return $stateParams.folderId;
					},
					projectUserConnectionsOfProject: function findProjectUserConnectionByProject(projectUserConnectionService, $stateParams) {
						if($stateParams.type == 'PROJECT') {
							return projectUserConnectionService.findProjectUserConnectionByProject($stateParams.projectOrProductId);
						} else {
							return null;
						}
					},
					usersOfProject: function findProjectUserConnectionByProject(projectUserConnectionService, userService, $stateParams) {
						if($stateParams.type == 'PROJECT') {
							return projectUserConnectionService.findUsersOfProjectUserConnectionByProject($stateParams.projectOrProductId);
						} else if($stateParams.type == 'PRODUCT') {
							return userService.findAllUsers();
						}
					},
					allLabelsPredefined: function getAllLabelsPredefined($stateParams, labelsPredefinedService) {
						return labelsPredefinedService.getAllLabelsPredefined($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					canSeeConfidentialDocumentFiles: function($stateParams, projectUserConnectionService, userService) {
						if($stateParams.type == 'PROJECT') {
							return projectUserConnectionService.canProjectUserConnectionRoleSeeConfidentialDocumentFiles($stateParams.projectUserConnectionId);
						} else if($stateParams.type == 'PRODUCT') {							
							return userService.canUserSeeConfidentialDocumentFiles($stateParams.userId);
						}
					},
					roles: function(optionsService) {
						return optionsService.getRoles();
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
		.module('legalprojectmanagement.project')
		.directive('documentFileIconDispatch', function () {
			return {
			      restrict: 'AE',
			      link: function (scope, element, attrs) {
			        element.bind('load', function () {
			          console.log('image loaded');
			        }).bind('error', function () {
			          element.attr('src', 'assets/img/default.PNG');
			        }); 
			      }
			};
	});	
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common') 
    	.directive('uploadDocumentFileDirective', uploadDocumentFileDirective);
    
    uploadDocumentFileDirective.$inject = ['$timeout'];
    
	function uploadDocumentFileDirective($timeout) {
		var directive = {
			restrict: 'E',  
			scope: {
				files: '=', 
				uploadFiles: '=',
				successfulUploadedFiles: '=',
				failedUploadedFiles: '='
			},
			templateUrl: 'app/project/directives/uploadDocumentFile/uploadDocumentFile.html',
			link: function($scope) {
				
			}
		};
		return directive;
		
		////////////
	}
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('createFolderModalService', createFolderModalService);
        
    createFolderModalService.$inject = ['$modal', '$stateParams', '$timeout'];
    
    function createFolderModalService($modal, $stateParams, $timeout) {
		var service = {
			showCreateFolderModal: showCreateFolderModal
		};		
		return service;
		
		////////////
				
		function showCreateFolderModal(invoker, folder, type) {
			 var createFolderModal = $modal.open({
				controller: CreateFolderModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	folder: function() {
			    		return folder;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/project/modals/createFolderModal/createFolder.modal.html'
			});
			return createFolderModal;
			
			function CreateFolderModalController($modalInstance, $scope, invoker, folder, type) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.folder = folder;
		    	vm.type = type;
		    	vm.folderWithNameStillExistsError = false;
		    	
		    	vm.createOrUpdateFolder = createOrUpdateFolder;
		    	vm.closeCreateFolderModal = closeCreateFolderModal;
		    	
		    	////////////
		    	
		    	function setTimeout() {
		      		$timeout(function() {
		      			vm.folderWithNameStillExistsError = false;
		  			   }, 4000); 
		      	}
		    	
		    	function createOrUpdateFolder() {
		    		if(vm.type == 'CREATE') {
		    			vm.invoker.createProjectFolder(vm.folder, function(response) {
		    				if(response.error == 'FOLDER_WITH_NAME_STILL_EXISTS') {
		    					vm.folderWithNameStillExistsError = true;
		    					setTimeout();
		    				} else {
		    					closeCreateFolderModal();
		    				}
		    			});
		    		} else if(vm.type == 'UPDATE') {
		    			vm.invoker.updateProjectFolder(vm.folder, function(response) {
		    				if(response.error == 'FOLDER_WITH_NAME_STILL_EXISTS') {
		    					vm.folderWithNameStillExistsError = true;
		    					setTimeout();
		    				} else {
		    					closeCreateFolderModal();
		    				}
		    			});
		    		}
		    	}
		    	
		    	function closeCreateFolderModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('deleteFolderModalService', deleteFolderModalService);
        
    deleteFolderModalService.$inject = ['$modal', '$stateParams'];
    
    function deleteFolderModalService($modal, $stateParams) {
		var service = {
			showDeleteFolderModal: showDeleteFolderModal
		};		
		return service;
		
		////////////
				
		function showDeleteFolderModal(invoker, folder) {
			 var deleteFolderModal = $modal.open({
				controller: DeleteFolderModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	folder: function() {
			    		return folder;
			    	}
			    }, 
				templateUrl: 'app/project/modals/deleteFolderModal/deleteFolder.modal.html'
			});
			return deleteFolderModal;
			
			function DeleteFolderModalController($modalInstance, $scope, invoker, folder) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.folder = folder;
		    	vm.errorCodeType = null;
		    	
		    	vm.deleteFolder = deleteFolder;
		    	vm.closeDeleteFolderModal = closeDeleteFolderModal;
		    	
		    	////////////
		    	
		    	function deleteFolder() {
		    		vm.invoker.deleteFolder(vm.folder, function(response) {
		    			if(response.errorCodeType != null) {
		    				vm.errorCodeType = response.errorCodeType;
		    			} else {
		    				closeDeleteFolderModal();
		    			}
		    		});
		    	}
		    	
		    	function closeDeleteFolderModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('documentInformationModalService', documentInformationModalService);
        
    documentInformationModalService.$inject = ['$modal', '$stateParams'];
    
    function documentInformationModalService($modal, $stateParams) {
		var service = {
			showDocumentInformationModal: showDocumentInformationModal
		};		
		return service;
		
		////////////
				
		function showDocumentInformationModal(documentFile, currentUser, userAuthorizationUserContainer, type, invoker, disabled) {
			 var yesNoModal = $modal.open({
				controller: DocumentInformationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	documentFile: function() {
			    		return documentFile;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	type: function() {
			    		return type;
			    	},
			    	invoker: function() {
			    		return invoker; 
			    	},
			    	disabled: function() {
			    		return disabled;
			    	}
			    }, 
				templateUrl: 'app/project/modals/documentInformationModal/documentInformation.modal.html'
			});
			return yesNoModal;
			
			function DocumentInformationModalController($modalInstance, $scope, documentFile, currentUser, userAuthorizationUserContainer, type, invoker, disabled) {
		    	var vm = this; 
		    	vm.documentFile = documentFile;
		    	vm.currentUser = currentUser;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.type = type;
		    	vm.invoker = invoker;
		    	vm.disabled = disabled;
		    	
		    	vm.storeInformation = storeInformation;
		    	vm.closeDocumentInformationModal = closeDocumentInformationModal;
		    	
		    	////////////
		    	
		    	function storeInformation() {
		    		vm.invoker.updateDocumentFile(vm.documentFile, false); 
		    		closeDocumentInformationModal();
		    	}
		    
		    	function closeDocumentInformationModal() {
					$modalInstance.dismiss('cancel'); 
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('htmlTemplateModalService', htmlTemplateModalService);
        
    htmlTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function htmlTemplateModalService($modal, $stateParams) {
		var service = {
			showHtmlTemplateModal: showHtmlTemplateModal
		};		
		return service;
		
		////////////
				
		function showHtmlTemplateModal(url, documentName, ending, version) {
			 var htmlTemplateModal = $modal.open({
				controller: HtmlTemplateModalController,
			    controllerAs: 'vm',
			    size: 'md',
			    windowClass: "modal fade in",
			    resolve: {
			    	url: function() {
			    		return url;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/htmlTemplateModal/htmlTemplate.modal.html'
			});
			return htmlTemplateModal;
			
			function HtmlTemplateModalController($modalInstance, $scope, url, documentName, ending, version) {
		    	var vm = this; 
		    	vm.url = prepareHtmlUrl(url, ending);
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;	
		    	
		    	vm.prepareHtmlUrl = prepareHtmlUrl;
		    	vm.closeHtmlTemplateModal = closeHtmlTemplateModal;
		    	
		    	////////////
		    	
		    	function prepareHtmlUrl(url, ending) {
		    		return url + 'previewhtml';
		    	}
		    	
		    	function closeHtmlTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('imgTemplateModalService', imgTemplateModalService);
        
    imgTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function imgTemplateModalService($modal, $stateParams) {
		var service = {
			showImgTemplateModal: showImgTemplateModal
		};		
		return service;
		
		////////////
				
		function showImgTemplateModal(url, documentName, ending, version) {
			 var imgTemplateModal = $modal.open({
				controller: ImgTemplateModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	url: function() {
			    		return url;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/imgTemplateModal/imgTemplate.modal.html'
			});
			return imgTemplateModal;
			
			function ImgTemplateModalController($modalInstance, $scope, url, documentName, ending, version) {
		    	var vm = this; 
		    	vm.url = url;
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;
		    	
		    	vm.closeImgTemplateModal = closeImgTemplateModal;
		    	
		    	////////////
		    	
		    	function closeImgTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('noTemplateModalService', noTemplateModalService);
        
    noTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function noTemplateModalService($modal, $stateParams) {
		var service = {
			showNoTemplateModal: showNoTemplateModal
		};		
		return service;
		
		////////////
				
		function showNoTemplateModal(url, documentName, ending, version) {
			 var noTemplateModal = $modal.open({
				controller: NoTemplateModalController,
			    controllerAs: 'vm',
			    size: 'md',
			    windowClass: "modal fade in",
			    resolve: {
			    	url: function() {
			    		return url;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/noTemplateModal/noTemplate.modal.html'
			});
			return noTemplateModal;
			
			function NoTemplateModalController($modalInstance, $scope, url, documentName, ending, version) {
		    	var vm = this; 
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;	
		    	
		    	vm.closeNoTemplateModal = closeNoTemplateModal;
		    	
		    	////////////
		    	
		    	function closeNoTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('pdfTemplateModalService', pdfTemplateModalService);
        
    pdfTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function pdfTemplateModalService($modal, $stateParams) {
		var service = {
			showPdfTemplateModal: showPdfTemplateModal
		};		
		return service;
		
		////////////
				
		function showPdfTemplateModal(pdfUrl, documentName, ending, version) {
			 var pdfTemplateModal = $modal.open({
				controller: PdfTemplateModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	pdfUrl: function() {
			    		return pdfUrl;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/pdfTemplateModal/pdfTemplate.modal.html'
			});
			return pdfTemplateModal;
			
			function PdfTemplateModalController($modalInstance, $scope, pdfUrl, documentName, ending, version) {
		    	var vm = this; 
		    	$scope.pdfUrl = preparePdfUrl(pdfUrl, ending);
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;
		    	
		    	vm.preparePdfUrl = preparePdfUrl;
		    	vm.closePdfTemplateModal = closePdfTemplateModal;
		    	
		    	////////////
		    	
		    	function preparePdfUrl(pdfUrl, ending) {
		    		if(ending.indexOf('pdf') === -1 && ending.indexOf('PDF') === -1) {
		    			return pdfUrl + 'previewpdf';
		    		}
		    		return pdfUrl;
		    	}
		    	
		    	function closePdfTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('roleOfAssignedUserIsBlackListedModalService', roleOfAssignedUserIsBlackListedModalService);
        
    roleOfAssignedUserIsBlackListedModalService.$inject = ['$modal', '$stateParams'];
    
    function roleOfAssignedUserIsBlackListedModalService($modal, $stateParams) {
		var service = {
			showRoleOfAssignedUserIsBlackListedModal: showRoleOfAssignedUserIsBlackListedModal
		};		
		return service;
		
		////////////
				
		function showRoleOfAssignedUserIsBlackListedModal(documentFile, user) {
			 var roleOfAssignedUserIsBlackListedModal = $modal.open({
				controller: RoleOfAssignedUserIsBlackListedModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	documentFile: function() {
			    		return documentFile;
			    	},
			    	user: function() {
			    		return user;
			    	}
			    }, 
				templateUrl: 'app/project/modals/roleOfAssignedUserIsBlackListedModal/roleOfAssignedUserIsBlackListed.modal.html'
			});
			return roleOfAssignedUserIsBlackListedModal;
			
			function RoleOfAssignedUserIsBlackListedModalController($modalInstance, $scope, documentFile, user) {
		    	var vm = this; 
		    	vm.documentFile = documentFile;
		    	vm.user = user;
		    	
		    	vm.closeRoleOfAssignedUserIsBlackListedModal = closeRoleOfAssignedUserIsBlackListedModal;
		    	
		    	////////////
		    	
		    	function closeRoleOfAssignedUserIsBlackListedModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('shiftDocumentFileModalService', shiftDocumentFileModalService);
        
    shiftDocumentFileModalService.$inject = ['$modal', '$stateParams', '$timeout', 'ngToast', 'folderTreeService'];
    
    function shiftDocumentFileModalService($modal, $stateParams, $timeout, ngToast, folderTreeService) {
		var service = {
			showShiftDocumentFileModal: showShiftDocumentFileModal
		};		
		return service;
		
		////////////
				
		function showShiftDocumentFileModal(invoker, folders, documentFile) {
			 var shiftDocumentFileModal = $modal.open({
				controller: ShiftDocumentFileModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	folders: function() {
			    		return folders;
			    	},
			    	documentFile: function() {
			    		return documentFile;
			    	}
			    }, 
				templateUrl: 'app/project/modals/shiftDocumentFileModal/shiftDocumentFile.modal.html'
			});
			return shiftDocumentFileModal;
			
			function ShiftDocumentFileModalController($modalInstance, $scope, invoker, folders, documentFile) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.folders = folders;
		    	vm.documentFile = documentFile;
		    	vm.folderTree = null;
		    	vm.errorShiftDocumentFileStillInFolder = false;
		    	
		    	createTree();
		    	vm.shiftToRoot = shiftToRoot;
		    	vm.shiftToFolder = shiftToFolder;
		    	vm.closeShiftDocumentFileModal = closeShiftDocumentFileModal;
		    	
		    	////////////
		    	
		    	function setTimeout() {
		      		$timeout(function() {
		      			vm.errorShiftDocumentFileStillInFolder = false;
		  			   }, 4000); 
		      	}
		    	
		    	function createTree() {
		    		vm.folderTree = folderTreeService.createTree({q:vm.folders});
		    	}
		    	
		    	function shiftToRoot() {
		    		if(documentFile.folderId !== '') {
		    			documentFile.folderId = '';
		    			invoker.updateDocumentFile(documentFile, false);
		    			invoker.removeDocumentFileOfPagedDocumentFiles(documentFile);
		    			closeShiftDocumentFileModal();
		    			vm.releaseToastMsg = ngToast.warning({
		    		  		  content: '<span class="glyphicon glyphicon-info-sign infoSign"></span> <span><b>' + documentFile.fileName + '</b> wurde in den Ordner <b>Dokumente</b> verschoben</span>',
		    		  		  timeout: 5000,
		    		  		  dismissButton: true
		    		  		});
		    		} else {
		    			vm.errorShiftDocumentFileStillInFolder = true;
		    			setTimeout();
		    		}
		    	}
		    	
		    	function shiftToFolder(folder) {
		    		if(documentFile.folderId != folder.id) {
		    			documentFile.folderId = folder.id;
		    			invoker.updateDocumentFile(documentFile, false);
		    			invoker.removeDocumentFileOfPagedDocumentFiles(documentFile);
		    			closeShiftDocumentFileModal();
		    			vm.releaseToastMsg = ngToast.warning({
		    		  		  content: '<span class="glyphicon glyphicon-info-sign infoSign"></span> <span><b>' + documentFile.fileName + '</b> wurde in den Ordner <b>' + folder.name + '</b> verschoben</span>',
		    		  		  timeout: 5000,
		    		  		  dismissButton: true
		    		  		});
		    		} else {
		    			vm.errorShiftDocumentFileStillInFolder = true;
		    			setTimeout();
		    		}
		    	}
		    	
		    	function closeShiftDocumentFileModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('textTemplateModalService', textTemplateModalService);
        
    textTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function textTemplateModalService($modal, $stateParams) {
		var service = {
			showTextTemplateModal: showTextTemplateModal
		};		
		return service;
		
		////////////
				
		function showTextTemplateModal(url, documentName, ending, version) {
			 var textTemplateModal = $modal.open({
				controller: TextTemplateModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	url: function() {
			    		return url;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/textTemplateModal/textTemplate.modal.html'
			});
			return textTemplateModal;
			
			function TextTemplateModalController($modalInstance, $scope, url, documentName, ending, version) {
		    	var vm = this; 
		    	vm.url = prepareTextUrl(url, ending);
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;	
		    	
		    	vm.prepareTextUrl = prepareTextUrl;
		    	vm.closeTextTemplateModal = closeTextTemplateModal;
		    	
		    	////////////
		    	
		    	function prepareTextUrl(url, ending) {
		    		return url + 'previewtext';
		    	}
		    	
		    	function closeTextTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('yesNoModalService', yesNoModalService);
        
    yesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(documentFile, documentFileVersion, invoker, type) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	documentFile: function() {
			    		return documentFile;
			    	},
			    	documentFileVersion: function() {
			    		return documentFileVersion;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/project/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, documentFile, documentFileVersion, invoker, type) {
		    	var vm = this; 
		    	vm.documentFile = documentFile;
		    	vm.documentFileVersion = documentFileVersion;
		    	vm.invoker = invoker;
		    	vm.type = type;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		if(vm.type == 'DELETE_VERSION') {
		    			invoker.deleteDocumentFileVersion(documentFile, documentFileVersion);
		    		} else if(vm.type == 'DELETE_ALL_VERSIONS') {
		    			invoker.deleteAllDocumentVersions(documentFile);
		    		}
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('yesNoModalService', yesNoModalService);
        
    yesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(documentFile, documentFileVersion, invoker, type) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	documentFile: function() {
			    		return documentFile;
			    	},
			    	documentFileVersion: function() {
			    		return documentFileVersion;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/project/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, documentFile, documentFileVersion, invoker, type) {
		    	var vm = this; 
		    	vm.documentFile = documentFile;
		    	vm.documentFileVersion = documentFileVersion;
		    	vm.invoker = invoker;
		    	vm.type = type;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		if(vm.type == 'DELETE_VERSION') {
		    			invoker.deleteDocumentFileVersion(documentFile, documentFileVersion);
		    		} else if(vm.type == 'DELETE_ALL_VERSIONS') {
		    			invoker.deleteAllDocumentVersions(documentFile);
		    		}
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.controller('ProjectController', ProjectController);
     
    ProjectController.$inject = ['$scope', '$timeout', 'currentUser', 'type', 'projectOrProduct', 'userAuthorizationUserContainer', 'projectUserConnectionsOfProject', 'projectTemplateService', 'uploadService', 'projectsService', 'documentFileService', 'yesNoModalService', 'contactsService', 'userService', 'contactAndUserService', 'folderService', 'createFolderModalService', 'amountOfDocumentFilesWithUniqueName', 'amountOfPages', 'documentFilePageSize', 'pagedDocumentFiles', 'folders', 'folderId', 'usersOfProject', 'allLabelsPredefined', 'canSeeConfidentialDocumentFiles', 'deleteFolderModalService', 'shiftDocumentFileModalService', 'documentInformationModalService', 'roles', 'roleOfAssignedUserIsBlackListedModalService'];
    
    function ProjectController($scope, $timeout, currentUser, type, projectOrProduct, userAuthorizationUserContainer, projectUserConnectionsOfProject, projectTemplateService, uploadService, projectsService, documentFileService, yesNoModalService, contactsService, userService, contactAndUserService, folderService, createFolderModalService, amountOfDocumentFilesWithUniqueName, amountOfPages, documentFilePageSize, pagedDocumentFiles, folders, folderId, usersOfProject, allLabelsPredefined, canSeeConfidentialDocumentFiles, deleteFolderModalService, shiftDocumentFileModalService, documentInformationModalService, roles, roleOfAssignedUserIsBlackListedModalService) {
	    $scope.vm = this;  
    	var vm = this;
    	
    	vm.type = type;
    	vm.amountOfDocumentFilesWithUniqueName = amountOfDocumentFilesWithUniqueName.data;
    	vm.amountOfDocumentFilesInFolderWithUniqueName = null;
    	vm.pagedDocumentFiles = setContactOrUser(pagedDocumentFiles.data);
    	vm.amountOfPages = amountOfPages.data;
    	vm.projectUserConnectionsOfProject = projectUserConnectionsOfProject != null ? projectUserConnectionsOfProject.data : null;
    	vm.folders = folders.data;
    	vm.roles = roles.data;
    	vm.folderId = folderId;
    	vm.documentFilePageSize = documentFilePageSize.data;
    	vm.usersOfProject = usersOfProject != null ? usersOfProject.data : null;
    	vm.allLabelsPredefined = allLabelsPredefined.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.canSeeConfidentialDocumentFiles = canSeeConfidentialDocumentFiles.data;
    	vm.projectOrProduct = projectOrProduct.data;
    	vm.currentUser = currentUser;
    	vm.orderType = 'fileName';
    	vm.currentPage = 0;
    	vm.selectedContact = null;
    	vm.labelOrder = 'labelsPredefinedType';
    	vm.uploadFiles = false;
    	vm.successfulUploadedFiles = 0;
    	vm.failedUploadedFiles = 0;
    	vm.documentFileAndFolderSearchString = '';
    	vm.superFolderId = '';
    	vm.assignedDocumentFiles = false;
    	vm.confidentDocumentFiles = false;
    	vm.breadCrumbFolderContainer = null;
    	vm.referenceToCommunicationError = false;
    	vm.referenceToRecipeError = false;
    	vm.referenceToContactError = false;

    	vm.successCallback = successCallback;
    	vm.errorCallback = errorCallback;
    	vm.progressCallback = progressCallback;
    	vm.updateDocumentFile = updateDocumentFile;
    	vm.updateDocumentFileLabels = updateDocumentFileLabels;
    	vm.labelInputChange = labelInputChange;
    	vm.removeLabel = removeLabel;
    	vm.addLabel = addLabel;
    	vm.arrayRang = arrayRang;
    	vm.pageChange = pageChange;
    	vm.deleteDocumentFileVersion = deleteDocumentFileVersion;
    	vm.deleteAllDocumentVersions = deleteAllDocumentVersions;
    	vm.createDocumentUrl = createDocumentUrl;
    	vm.showTemplateModal = showTemplateModal;
    	vm.showYesNoDeleteModal = showYesNoDeleteModal;
    	vm.setContactOrUser = setContactOrUser;
    	vm.findContactAndUserBySearchString = findContactAndUserBySearchString;
    	vm.prepareForUpdateDocumentFile = prepareForUpdateDocumentFile;
    	vm.isCurrentPage = isCurrentPage;
    	vm.uploadNewDocumentVersion = uploadNewDocumentVersion;
    	vm.isLabelSelected = isLabelSelected;
    	vm.isBlackListed = isBlackListed;
    	vm.isRoleBlackListed = isRoleBlackListed;
    	vm.addBlackListId = addBlackListId;
    	vm.addBlackListRole = addBlackListRole;
    	vm.removeBlackListId = removeBlackListId;
    	vm.removeBlackListRole = removeBlackListRole;
    	vm.isWhiteListed = isWhiteListed;
    	vm.addWhiteListId = addWhiteListId;
    	vm.removeWhiteListId = removeWhiteListId;
    	vm.isCurrentUserWhiteListed = isCurrentUserWhiteListed;
    	vm.searchForLabel = searchForLabel;
    	vm.removeSearchString = removeSearchString;
    	vm.changeOrderType = changeOrderType;
    	vm.isWhiteListedExclusion = isWhiteListedExclusion;
    	vm.isBlackListedExclusion = isBlackListedExclusion;
    	vm.findAssignedDocumentFiles = findAssignedDocumentFiles;
    	vm.showCreateProjectFolderModal = showCreateProjectFolderModal;
    	vm.showUpdateProjectFolderModal = showUpdateProjectFolderModal;
    	vm.createProjectFolder = createProjectFolder;
    	vm.updateProjectFolder = updateProjectFolder;
    	vm.selectFolder = selectFolder;
    	vm.selectBreadCrumbFolder = selectBreadCrumbFolder;
    	vm.showDeleteFolderModal = showDeleteFolderModal;
    	vm.deleteFolder = deleteFolder;
    	vm.showShiftModal = showShiftModal;
    	vm.searchDocumentFiles = searchDocumentFiles;
    	vm.setDocumentFileVersion = setDocumentFileVersion;
    	vm.removeDocumentFileOfPagedDocumentFiles = removeDocumentFileOfPagedDocumentFiles;
    	vm.showDocumentInformationModal = showDocumentInformationModal;
    	vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
    	var folder = {};
    	folder.id = vm.folderId;
    	findBreadCrumbFolder(folder);
    	
    	////////////
    	
    	$scope.$watch('vm.files', function () {
    		unsetStillUploadedFlag();
    		uploadService.uploadFiles(vm.files, currentUser.id, vm.projectOrProduct.id, vm.superFolderId, vm.type, vm.successCallback, vm.errorCallback, vm.progressCallback);
        });
    	
        $scope.$watch('file', function () {
            if (vm.file != null) {
                vm.files = [vm.file]; 
            }
        });
        
        function searchDocumentFiles() {
    		findDocumentFilesBySearchString();
    		findFoldersBySearchString();
        }
        
        function createFolderObject(project, name, description, superFolderId, active) {
        	var folder = {};
        	folder.name = name;
        	folder.description = description;
        	folder.created = new Date();
        	folder.projectId = project.id;
        	folder.superFolderId = superFolderId;
        	folder.creationUserId = vm.currentUser.id;
        	folder.active = active;
        	folder.confident = false;
        	return folder;
        }
        
        function removeSearchString() {
        	vm.documentFileAndFolderSearchString = '';
        	findDocumentFilesBySearchString();
        }
        
        function searchForLabel(label) {
        	vm.documentFileAndFolderSearchString = label;
        } 
        
        function findDocumentFilesBySearchString() {
        	if(vm.documentFileAndFolderSearchString != null && vm.documentFileAndFolderSearchString.length >= 3) {
        		documentFileService.findDocumentFilesByBySearchStringAndProjectIdAndFolderId(vm.documentFileAndFolderSearchString, vm.projectOrProduct.id, vm.superFolderId).then(function successCallback(response) {
        			vm.pagedDocumentFiles = setContactOrUser(response.data);
      	   		 }, function errorCallback(response) {
      	   			  console.log('error project.controller.js#findDocumentFilesBySearchString');
      	   		 });
        	} else if(vm.documentFileAndFolderSearchString == null || vm.documentFileAndFolderSearchString.length === 0) {
        		pageChange(0);
        	}
        }
        
        function setDocumentFileVersion(documentFile) {
        	if(documentFile.documentFileVersion != null) {
        		documentFile.documentFileVersion = documentFile.selectedDocumentFileVersion;
        	} else {
        		documentFile.documentFileVersion = documentFile.documentFileVersions[documentFile.documentFileVersions.length-1];
        		documentFile.selectedDocumentFileVersion = documentFile.documentFileVersions[documentFile.documentFileVersions.length-1];
        	}
        }
        
        function findFoldersBySearchString() {
        	if(vm.documentFileAndFolderSearchString != null && vm.documentFileAndFolderSearchString.length >= 3) {
        		folderService.findFoldersByBySearchStringAndProjectIdAndFolderId(vm.documentFileAndFolderSearchString, vm.projectOrProduct.id, vm.superFolderId).then(function(response) {
        			vm.folders = response.data;
      	   		 }, function errorCallback(response) {
      	   			  console.log('error project.controller.js#findFoldersBySearchString');
      	   		 });
        	} else {
        		folderService.findFolders(vm.projectOrProduct.id, vm.superFolderId).then(function(response) {
        			vm.folders = response.data;
     	   		 }, function errorCallback(response) {
     	   			  console.log('error project.controller.js#findFoldersBySearchString');
     	   		 });
        	}
        }
        
        function isLabelSelected(labelPredefined, documentFileVersion) {
        	if(documentFileVersion == null || documentFileVersion.labels == null) {
        		return false;
        	}
        	for(var i = 0; i < documentFileVersion.labels.length; i++) {
        		if(documentFileVersion.labels[i].name == labelPredefined.name) {
        			return true;
        		}
        	}
        	return false;
        }
        
        function uploadNewDocumentVersion(document, currentDocumentFile) {
        	unsetStillUploadedFlag();
        	uploadService.uploadNewDocumentVersion(document, currentUser.id, vm.projectOrProduct.id, currentDocumentFile.id, vm.successCallback, vm.progressCallback);
        }
        
        function setResponsibleUserOrContactAddedTimeout(documentFile) {
      		$timeout(function() {
      			documentFile.labelUpdated = false;
      			documentFile.setContactOrUserSuccess = false;
  			   }, 2000); 
      	}
    	
        function removeDocumentFileOfPagedDocumentFiles(documentFile) {
        	for(var i = 0; i < vm.pagedDocumentFiles.length; i++) {
        		var pagedDocumentFile = vm.pagedDocumentFiles[i];
        		if(pagedDocumentFile.id === documentFile.id) {
        			vm.pagedDocumentFiles.splice(i, 1);
        		}
        	}
        }
        
    	function successCallback(response) {
    		var documentFile = response.data;
    		removeDocumentFileOfPagedDocumentFiles(documentFile);
    		documentFile.stillUploaded = true;
    		vm.pagedDocumentFiles.splice(0, 0, documentFile); // add file
    		setContactOrUser([documentFile]);
    		calculateAmountOfPages();
    		getAmountOfFileDocuments();
    		getAmountOfFileDocumentsInFolder();
    		vm.successfulUploadedFiles += 1; 
    		setFileUpdateTimeout();
    		setSuccessUploadFilesTimeout();
    	}
    	
    	function errorCallback(response) {
    		vm.failedUploadedFiles += 1;
    		setFailedUploadFilesTimeout();
    		setFileUpdateTimeout();
    		console.log('upload error in project.controller: statusText: ' + response.statusText + ', response- object: ' + response + ', message: ' + response.message + ', data: ' + response.data);
    	}
    	
    	function progressCallback(log) {
    		vm.uploadFiles = true;
    		console.log('upload log in project.controller: ' + log);
    	}
    	
    	function setFileUpdateTimeout() {
      		$timeout(function() {
      			vm.uploadFiles = false;
  			}, 2500); 
      	}
    	
    	function setDocumentFileDeleteTimeout() {
    		$timeout(function() {
    			vm.referenceToCommunicationError = false;
            	vm.referenceToRecipeError = false;
            	vm.referenceToContactError = false;
  			 }, 2500);
    	}
    	
    	function setSuccessUploadFilesTimeout() {
      		$timeout(function() {
      			vm.successfulUploadedFiles = 0;
  			   }, 10000); 
      	}
    	
    	function setFailedUploadFilesTimeout() {
      		$timeout(function() {
      			vm.failedUploadedFiles = 0;
  			   }, 10000); 
      	}
    	
    	function calculateAmountOfPages() {
    		documentFileService.calculateAmountOfPages(vm.projectOrProduct.id, vm.type, vm.superFolderId).then(function successCallback(response) {
				vm.amountOfPages = response.data;
				vm.amountOfPagesArray = vm.arrayRang(1, vm.amountOfPages, 1);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#calculateAmountOfPages');
  	   		 });
    	}
    	
    	function getAmountOfFileDocuments() {
    		documentFileService.countByProjectIdGroupByOriginalFileName(vm.projectOrProduct.id, vm.confidentDocumentFiles, vm.type).then(function successCallback(response) {
    			vm.amountOfDocumentFilesWithUniqueName = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#getAmountOfFileDocuments');
  	   		 });
    	}
    	
    	function getAmountOfFileDocumentsInFolder() {
    		documentFileService.countByProjectIdAndFolderIdGroupByOriginalFileName(vm.projectOrProduct.id, vm.superFolderId, vm.confidentDocumentFiles, vm.type).then(function(response) {
    			vm.amountOfDocumentFilesInFolderWithUniqueName = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#getAmountOfFileDocumentsInFolder');
  	   		 });
    	}
    	
    	function unsetStillUploadedFlag() {
    		if(vm.pagedDocumentFiles  == null) {
    			return;
    		}
    		for(var i = 0; i < vm.pagedDocumentFiles.length; i++) {
    			vm.pagedDocumentFiles[i].stillUploaded = false;
    		}
    	}
    	
    	function addLabel(documentFile, foundedLabelPredefinedSelected, documentFileVersion) {
    		var stillExists = labelStillExist(documentFileVersion, foundedLabelPredefinedSelected);
    		if(stillExists) {
    			return;
    		}
    		if(documentFileVersion.labels == null) {
    			documentFileVersion.labels = [];
    		}
    		if(foundedLabelPredefinedSelected != null && typeof foundedLabelPredefinedSelected != 'undefined') {
    			documentFileVersion.labels.push(foundedLabelPredefinedSelected);
    		}
    		updateDocumentFileLabels(documentFile);
    	}
    	
    	function labelStillExist(documentFileVersion, newLabel) {
    		if(documentFileVersion.labels == null || typeof documentFileVersion.labels == 'undefined') {
    			return false;
    		}
    		for(var i = 0; i < documentFileVersion.labels.length; i++) {
    			if(documentFileVersion.labels[i].name == newLabel.name) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function updateDocumentFile(documentFile, updateDocumentFileResponsibility) {
    		if(documentFile.fileName.length < 3) {
    			documentFile.nameLengthError = true;
    			return;
    		}
    		documentFile.nameLengthError = false;
    		documentFileService.updateDocumentFile(documentFile, vm.projectOrProduct.id).then(function successCallback(response) {
    			documentFile.foundedLabelsPredefined = null;
    			var updatedDocumentFile = response.data;
    			if(updateDocumentFileResponsibility === true) {
    				setResponsibleUserOrContactOrPlainText(updatedDocumentFile);
    			}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller.js#updateDocumentFile');
  	   		 });
    	} 
    	
    	function updateDocumentFileLabels(documentFile) {
    		documentFileService.updateDocumentFileLabels(documentFile, vm.projectOrProduct.id).then(function successCallback(response) {
    			documentFile.labelUpdated = true;
    			setResponsibleUserOrContactAddedTimeout(documentFile);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller.js#updateDocumentFileLabels');
  	   		 });
    	}
    	
    	function getDocumentFileVersion(documentFile, documentFileVersion) {
    		for(var i = 0; i < documentFile.documentFileVersions.length; i++) {
    			var documentFileVersionOfIndex = documentFile.documentFileVersions[i];
    			if(documentFileVersionOfIndex.version === documentFileVersion.version) {
    				return documentFileVersionOfIndex;
    			}
    		}
    		return null;
    	}
    	
    	function labelInputChange(data, documentFile) {
    		if(data.length === 0) {
    			documentFile.foundedLabelsPredefined = [];
    			return;
    		}
    		documentFileService.searchPredefinedLabels(data).then(function successCallback(response) {
    			if(response.data.length > 0) {
    				var foundedLabelsPredefined = response.data;
    				documentFile.foundedLabelsPredefined = foundedLabelsPredefined;
    			}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#labelInputChange');
  	   		 });
    	}
    	
    	function removeLabel(documentFile, documentFileVersion, label) {
    		var keepGoing = true;
			angular.forEach(documentFileVersion.labels, function(value, key) {
				if(keepGoing) {
  				  if(value.name == label.name) { 
  					  documentFileVersion.labels.splice(key, 1);
  					  keepGoing = false;
  				  }
				}
			});
    		updateDocumentFileLabels(documentFile);
    	}
    	
    	function arrayRang(min, max, step) {
    	    step = step || 1;
    	    var input = [];
    	    for (var i = min; i <= max; i += step) {
    	        input.push(i);
    	    }
    	    return input;
    	}
    	
    	function changeOrderType(orderType) { 
    		vm.orderType = orderType;
    		pageChange(0);
    	}
    	
    	function pageChange(page) {
    		vm.pagedDocumentFiles = null;
    		if(page < 0 || page > vm.amountOfPagesArray.length-1) {
    			return;
    		}
    		if(vm.assignedDocumentFiles === false) {
    			findPagedDocumentFiles(page);
    		} else {
    			findAssignedDocumentFiles(page);
    		}
    		getAmountOfFileDocuments();
    	}
    	
    	function findPagedDocumentFiles(page) {
    		documentFileService.findPagedDocumentFilesByProjectIdAndFolderId(vm.projectOrProduct.id, vm.superFolderId, page, vm.orderType, vm.confidentDocumentFiles, vm.type).then(function successCallback(response) {
    			vm.pagedDocumentFiles = setContactOrUser(response.data);
    			vm.currentPage = page;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller.js#findPagedDocumentFiles');
  	   		 });
    	}
    	
    	function findAssignedDocumentFiles(page) {
    		if(vm.assignedDocumentFiles === false) {
    			findPagedDocumentFiles(page);
    			getAmountOfFileDocuments();
    		} else {
    			documentFileService.findAssignedDocumentFilesByProjectId(vm.projectOrProduct.id, vm.orderType).then(function successCallback(response) {
        			vm.pagedDocumentFiles = setContactOrUser(response.data);
        			vm.currentPage = page;
        			vm.amountOfDocumentFilesWithUniqueName = vm.pagedDocumentFiles.length;
      	   		 }, function errorCallback(response) {
      	   			  console.log('error project.controller.js#findPagedAndAssignedDocumentFiles');
      	   		 });
    		}
    	}

    	function showYesNoDeleteModal(documentFile, documentFileVersion, type) {
    		yesNoModalService.showYesNoModal(documentFile, documentFileVersion, vm, type);
    	}
    	
    	function deleteDocumentFileVersion(documentFile, documentFileVersion) {
    		if(documentFileVersion == null) {
    			return;
    		}
    		for(var i = 0; i < documentFile.documentFileVersions.length; i++) {
				if(documentFile.documentFileVersions[i].version == documentFileVersion.version) {
					documentFile.documentFileVersions[i].active = false;
					documentFile.documentFileVersions[i].userIdDeactivated = currentUser.id;
					break;
				}
    		}
    		documentFileService.updateDocumentFile(documentFile, vm.projectOrProduct.id).then(function successCallback(response) {
    			var updatedDocumentFile = response.data;
    			for(var i = 0; i < updatedDocumentFile.documentFileVersions.length; i++) {
    				if(updatedDocumentFile.documentFileVersions[i].active === true) {
    					documentFileVersion = updatedDocumentFile.documentFileVersions[i];
    					break;
    				}
        		}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error project.controller#deleteDocumentVersion');
  	   		 });
    	}
    	
    	function deleteAllDocumentVersions(documentFile) {
    		documentFile.active = false;
    		documentFileService.deleteDocumentFile(documentFile.id, vm.projectOrProduct.id).then(function(response) {
    			for(var i = 0; i < vm.pagedDocumentFiles.length; i++) {
    				if(vm.pagedDocumentFiles[i].id == documentFile.id) {
    					vm.pagedDocumentFiles.splice(i, 1);
    					getAmountOfFileDocuments();
    				}
    			}
  	   		 }, function errorCallback(response) {
  	   			 console.log('error project.controller#deleteAllDocumentVersions');
  	   			 if(response.data.message === 'REFERENCE_TO_COMMUNICATION') {
  	   				 vm.referenceToCommunicationError = true;
  	   			 }
  	   			 if(response.data.message === 'REFERENCE_TO_RECIPE') {
  	   				 vm.referenceToRecipeError = true;
	   			 }
	  	   		 if(response.data.message === 'REFERENCE_TO_CONTACT') {
	  	   			 vm.referenceToContactError = true;
	  	   		 }
	  	   		setDocumentFileDeleteTimeout();
  	   		 });
    	}
    	
    	function createDocumentUrl(user, project, documentFile, documentFileVersion) {
    		if(documentFileVersion == null) {
    			return '';
    		}
    		var version = documentFileVersion === undefined ? '_Version1' : documentFileVersion.version;
    		return 'api/filedownloads/filedownload/' + user.id + '/' + documentFile.id + '/' + version + '/';
    	}
    	
    	function showTemplateModal(url, documentFile, documentFileVersion) {
    		projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
    	}
    	
    	function findContactAndUserBySearchString(searchString) {
    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function (response) {
    			return response.data;
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findContactBySearchString: ' + data);
			});
    	}
    	
    	function setResponsibleUserOrContactOrPlainText(updatedDocumentFile) {
    		for(var i = 0; i < vm.pagedDocumentFiles.length; i++) {
				if(vm.pagedDocumentFiles[i].id == updatedDocumentFile.id) {
					vm.pagedDocumentFiles[i] = updatedDocumentFile;
					for(var j = 0; j < vm.pagedDocumentFiles[i].documentFileVersions.length; j++) {
						vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleUser != null ? vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleUser : (vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleContact != null ? vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleContact : vm.pagedDocumentFiles[i].documentFileVersions[j].responsibleText);
					}
					vm.pagedDocumentFiles[i].setContactOrUserSuccess = true;
					setResponsibleUserOrContactAddedTimeout(vm.pagedDocumentFiles[i]);
					break;
				}
			}
    	}
    	
    	function setContactOrUser(pagedDocumentFiles) {
    		for(var i = 0; i < pagedDocumentFiles.length; i++) {
    			for(var j = 0; j < pagedDocumentFiles[i].documentFileVersions.length; j++) {
    				if(pagedDocumentFiles[i].documentFileVersions[j].responsibleUser != null) {
    					pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = pagedDocumentFiles[i].documentFileVersions[j].responsibleUser;
        			} else if(pagedDocumentFiles[i].documentFileVersions[j].responsibleContact != null) {
        				pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact = pagedDocumentFiles[i].documentFileVersions[j].responsibleContact;
        			} else if(pagedDocumentFiles[i].documentFileVersions[j].responsibleText != null) {
        				pagedDocumentFiles[i].documentFileVersions[j].responsibleUserOrContact  = pagedDocumentFiles[i].documentFileVersions[j].responsibleText;
        			}
    			}
    		}
    		return pagedDocumentFiles;
    	}
    	
    	function prepareForUpdateDocumentFile(documentFile, documentFileVersion) {
    		if(documentFileVersion.responsibleUserOrContact == null || typeof documentFileVersion.responsibleUserOrContact == 'undefined') {
    			documentFile.setContactOrUserError = true;
    			documentFile.setContactOrUserSuccess = false;
    			return;
    		}
    		documentFile.setContactOrUserError = false;
    		if(documentFileVersion.responsibleUserOrContact.contactUserType == 'USER') {
    			documentFileVersion.responsibleUser = documentFileVersion.responsibleUserOrContact.user;
    			documentFileVersion.responsibleContact = null;
    			documentFileVersion.responsibleText = null;
    		} else if(documentFileVersion.responsibleUserOrContact.contactUserType == 'CONTACT') {
    			documentFileVersion.responsibleContact = documentFileVersion.responsibleUserOrContact.contact;
    			documentFileVersion.responsibleUser = null;
    			documentFileVersion.responsibleText = null;
    		} else if(documentFileVersion.responsibleUserOrContact.length > 0) {
    			documentFileVersion.responsibleContact = null;
    			documentFileVersion.responsibleUser = null;
    			documentFileVersion.responsibleText = documentFileVersion.responsibleUserOrContact;
    		} 
    		vm.updateDocumentFile(documentFile, true);
    		documentFile.setContactOrUserSuccess = true;
    		setResponsibleUserOrContactAddedTimeout(documentFile);
    	}
    	
    	function isCurrentPage(indexProject) {
    		if(indexProject == (vm.currentPage+1)) {
    			return true;
    		}
    		return false; 
    	}
    	
    	function isBlackListed(user, documentFile) {
    		if(documentFile.userIdBlackList == null || user == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdBlackList.length; i++) {
    			if(documentFile.userIdBlackList[i] == user.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function isRoleBlackListed(role, documentFile) {
    		if(documentFile.rolesBlackList == null || role == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.rolesBlackList.length; i++) {
    			if(documentFile.rolesBlackList[i] == role) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function addBlackListId(documentFile, user, userAuthorizationUserContainer) {
    		if(vm.type == 'PROJECT' && userAuthorizationUserContainer.userAuthorizations[1].state == false) {  // project not changable
    			return;
    		}
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		if(documentFile.userIdBlackList == null) {
    			documentFile.userIdBlackList = [];
    		} 
    		var addTrigger = true;
    		for(var i = 0; i < documentFile.userIdBlackList.length; i++) {
    			if(documentFile.userIdBlackList[i] == user.id) {
    				addTrigger = false;
    			}
    		}
    		if(addTrigger) {
    			documentFile.userIdBlackList.push(user.id);
    			updateDocumentFile(documentFile, false);
    		}
    	}
    	
    	function addBlackListRole(documentFile, role, userAuthorizationUserContainer) {
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		if(documentFile.rolesBlackList == null) {
    			documentFile.rolesBlackList = [];
    		} 
    		var addTrigger = true;
    		for(var i = 0; i < documentFile.rolesBlackList.length; i++) {
    			if(documentFile.rolesBlackList[i] == role) {
    				addTrigger = false;
    			}
    		}
    		if(addTrigger) {
    			documentFile.rolesBlackList.push(role);
    			updateDocumentFile(documentFile, false);
    		}
    	}
    	
    	function removeBlackListId(documentFile, user, userAuthorizationUserContainer) {
    		if(vm.type == 'PROJECT' && userAuthorizationUserContainer.userAuthorizations[1].state == false) {  // project not changable
    			return;
    		}
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		for(var i = 0; i < documentFile.userIdBlackList.length; i++) {
    			if(documentFile.userIdBlackList[i] == user.id) {
    				documentFile.userIdBlackList.splice(i, 1);
    				break;
    			}
    		}
    		updateDocumentFile(documentFile, false);
    	}
    	
    	function removeBlackListRole(documentFile, role, userAuthorizationUserContainer) {
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		for(var i = 0; i < documentFile.rolesBlackList.length; i++) {
    			if(documentFile.rolesBlackList[i] == role) {
    				documentFile.rolesBlackList.splice(i, 1);
    				break;
    			}
    		}
    		updateDocumentFile(documentFile, false);
    	}
    	
    	function isWhiteListed(user, documentFile) {
    		if(documentFile.userIdWhiteList == null || user == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdWhiteList.length; i++) {
    			if(documentFile.userIdWhiteList[i] == user.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function addWhiteListId(documentFile, user, userAuthorizationUserContainer) {
    		if(vm.type == 'PROJECT' && userAuthorizationUserContainer.userAuthorizations[1].state == false) {  // project not changable
    			return;
    		}
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		// is user role of user is black listed than show info
    		if(documentFile.rolesBlackList != null) {
	    		for(var i = 0; i < documentFile.rolesBlackList.length; i++) {
	    			if(documentFile.rolesBlackList[i] == user.projectUserConnectionRole) {
	    				roleOfAssignedUserIsBlackListedModalService.showRoleOfAssignedUserIsBlackListedModal(documentFile, user);
	    				return;
	    			}
	    		}
    		}
    		
    		if(documentFile.userIdWhiteList == null) {
    			documentFile.userIdWhiteList = [];
    		} 
    		var addTrigger = true;
    		for(var j = 0; j < documentFile.userIdWhiteList.length; j++) {
    			if(documentFile.userIdWhiteList[j] == user.id) {
    				addTrigger = false;
    			}
    		}
    		if(addTrigger) {
    			documentFile.userIdWhiteList.push(user.id);
    			updateDocumentFile(documentFile, false);
    		}
    	}
    	
    	function removeWhiteListId(documentFile, user, userAuthorizationUserContainer) {
    		if(vm.type == 'PROJECT' && userAuthorizationUserContainer.userAuthorizations[1].state == false) {  // project not changable
    			return;
    		}
    		if(vm.type == 'PRODUCT' && userAuthorizationUserContainer.userAuthorizations[4].state == false) {  // product not changable
    			return;
    		}
    		for(var i = 0; i < documentFile.userIdWhiteList.length; i++) {
    			if(documentFile.userIdWhiteList[i] == user.id) {
    				documentFile.userIdWhiteList.splice(i, 1);
    				break;
    			}
    		}
    		updateDocumentFile(documentFile, false);
    	}
    	
    	function isCurrentUserWhiteListed(documentFile) {
    		if(documentFile.userIdWhiteList == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdWhiteList.length; i++) {
    			if(documentFile.userIdWhiteList[i] == vm.currentUser.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function isWhiteListedExclusion(documentFile, user) {
    		if(documentFile.userIdWhiteList == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdWhiteList.length; i++) {
    			if(documentFile.userIdWhiteList[i] == user.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function isBlackListedExclusion(documentFile, user) {
    		if(documentFile.userIdBlackList == null) {
    			return false;
    		}
    		for(var i = 0; i < documentFile.userIdBlackList.length; i++) {
    			if(documentFile.userIdBlackList[i] == user.id) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function showCreateProjectFolderModal(project) {
    		var folder = createFolderObject(vm.projectOrProduct, '', '', vm.superFolderId, true);
    		createFolderModalService.showCreateFolderModal(vm, folder, 'CREATE'); 
    	}
    	
    	function showUpdateProjectFolderModal(folder) {
    		createFolderModalService.showCreateFolderModal(vm, folder, 'UPDATE'); 
    	}
    	
    	function createProjectFolder(folder, callback) {
    		if(vm.superFolderId != '') {
    			folder.superFolderId = vm.superFolderId;
    		}
    		folderService.create(folder, vm.type).then(function(response) {
    			vm.folders.push(response.data);
    			callback(response.data);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error project.controller.js#createProjectFolder');
 	   			  callback(response.data);
 	   		 });
    	}
    	
    	function updateProjectFolder(folder, callback) {
    		folderService.update(folder, vm.type).then(function(response) {
    			for(var i = 0; i < vm.folders.length; i++) {
    				if(vm.folders[i].id == folder.id) {
    					vm.folders[i] = folder;
    					break;
    				}
    			}
    			callback(response.data);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error project.controller.js#updateProjectFolder');
 	   			  callback(response.data);
 	   		 });
    	}
    	
    	function selectFolder(folder) {
    		vm.superFolderId = folder.id;
    		findFolders(folder);
    		findBreadCrumbFolder(folder);
    		findPagedDocumentFiles(0);
    		calculateAmountOfPages();
    		getAmountOfFileDocumentsInFolder();
    	}
    	
    	function selectBreadCrumbFolder(folder) {
    		vm.superFolderId = folder == null ? '' : folder.id;
    		findFolders(folder);
    		findBreadCrumbFolder(folder);
    		findPagedDocumentFiles(0);
    		calculateAmountOfPages();
    		getAmountOfFileDocumentsInFolder();
    	}
    	
    	function findFolders(folder) {
    		var folderId = folder == null ? '' : folder.id;
    		folderService.findFolders(vm.projectOrProduct.id, folderId).then(function(response) {
    			vm.folders = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error project.controller.js#findFolders');
 	   		 });
    	}
    	
    	function findBreadCrumbFolder(folder) {
    		var folderId = folder == null ? '' : folder.id;
    		folderService.findBreadCrumbOfFolder(folderId).then(function(response) {
    			vm.breadCrumbFolderContainer = response.data;
	   		 }, function errorCallback(response) {
	   			  console.log('error project.controller.js#findBreadCrumbOfFolder');
	   		 });
    	}
    	
    	function showDeleteFolderModal(folder) {
    		deleteFolderModalService.showDeleteFolderModal(vm, folder);
    	}
    	
    	function deleteFolder(folder, callback) {
    		folderService.deleteFolder(folder.id, vm.type).then(function(response) {
    			for(var i = 0; i < vm.folders.length; i++) {
    				if(vm.folders[i].id == folder.id) {
    					vm.folders.splice(i, 1);
    					break;
    				}
    			}
    			callback(response.data);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error project.controller.js#deleteFolder');
 	   			callback(response.data);
 	   		 });
    	}
    	
    	function showShiftModal(documentFile) {
    		folderService.findAllFoldersOfProject(vm.projectOrProduct.id).then(function(response) {
    			shiftDocumentFileModalService.showShiftDocumentFileModal(vm, response.data, documentFile);
	   		 }, function errorCallback(response) {
	   			  console.log('error project.controller.js#findBreadCrumbOfFolder');
	   		 });
    	}
    	
    	function showDocumentInformationModal(documentFile, userAuthorizationUserContainer) {
    		documentInformationModalService.showDocumentInformationModal(documentFile, currentUser, userAuthorizationUserContainer, vm.type, vm, false);
    	}
	} 
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectsService', projectsService);
        
    projectsService.$inject = ['$http', 'api_config'];
    
    function projectsService($http, api_config) {
		var service = {
			createProject: createProject,
			createProjectDataSheet: createProjectDataSheet,
			updateProjectDataSheet: updateProjectDataSheet,
			deleteProjectDataSheet: deleteProjectDataSheet,
			findProjectDataSheet: findProjectDataSheet,
			findProject: findProject,
			findProjectByTerm: findProjectByTerm,
			findAllProjects: findAllProjects,
			findProjectsOfUser: findProjectsOfUser,
			findPreparedProject: findPreparedProject,
			findProjectsGeneralAvailable: findProjectsGeneralAvailable,
			getDefaultProject: getDefaultProject,
			updateProject: updateProject,
			getProjectPageSize: getProjectPageSize,
			getAllProjectsOfTenant: getAllProjectsOfTenant,
			deleteProject: deleteProject 
		};
		
		return service;
		
		////////////
		
		function createProject(project, createCalendarEntry) {
			return $http.post(api_config.BASE_URL + '/projects/project/' + createCalendarEntry, project);
		}
		
		function createProjectDataSheet(projectDataSheet) {
			return $http.post(api_config.BASE_URL + '/projects/project/projectdatasheet', projectDataSheet);
		}
		
		function updateProjectDataSheet(projectDataSheet) {
			return $http.put(api_config.BASE_URL + '/projects/project/projectdatasheet', projectDataSheet);
		}
		
		function findProjectDataSheet(projectId) {
			return $http.get(api_config.BASE_URL + '/projects/project/projectdatasheet/' + projectId);
		}
		
		function deleteProjectDataSheet(projectId) {
			return $http.delete(api_config.BASE_URL + '/projects/project/projectdatasheet/' + projectId);
		}
		
		function findProject(projectId) {
			return $http.get(api_config.BASE_URL + '/projects/project/' + projectId);
		}
		
		function findAllProjects() {
			return $http.get(api_config.BASE_URL + '/projects/project');
		}
		
		function findProjectsOfUser(userId) {
			return $http.get(api_config.BASE_URL + '/projects/project/user/' + userId);
		}
		
		function findPreparedProject(projectId) {
			return $http.get(api_config.BASE_URL + '/projects/project/' + projectId + '/prepared');
		}
		
		function findProjectByTerm(searchTerm) {
			return $http.get(api_config.BASE_URL + '/projects/project/' + searchTerm + '/search');
		}
		
		function findProjectsGeneralAvailable() {
			return $http.get(api_config.BASE_URL + '/projects/project/generalaccess');
		}
		
		function getDefaultProject(userId) {
			return $http.get(api_config.BASE_URL + '/projects/project/default/' + userId);
		}
		
		function updateProject(project) {
			return $http.put(api_config.BASE_URL + '/projects/project', project);
		}
		
		function getProjectPageSize() {
			return $http.get(api_config.BASE_URL + '/projects/project/projectpagesize');
		}
		
		function getAllProjectsOfTenant() {
			return $http.get(api_config.BASE_URL + '/projects/project/all');
		}
		
		function deleteProject(projectId) {
			return $http.delete(api_config.BASE_URL + '/projects/project/' + projectId);
		}
    }
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectUserConnectionService', projectUserConnectionService);
        
    projectUserConnectionService.$inject = ['$http', 'api_config'];
    
    function projectUserConnectionService($http, api_config) {
		var service = {
			findProjectUserConnectionByUser: findProjectUserConnectionByUser,
			findProjectUserConnectionByProject: findProjectUserConnectionByProject,
			findUsersOfProjectUserConnectionByProject: findUsersOfProjectUserConnectionByProject,
			findProjectUserConnectionById: findProjectUserConnectionById,
			findProjectUserConnectionsByContactAndProject: findProjectUserConnectionsByContactAndProject,
			findProjectUserConnectionsByContactAndUser: findProjectUserConnectionsByContactAndUser,
			findProjectUserConnectionsOfUserByProjectSearchString: findProjectUserConnectionsOfUserByProjectSearchString,
			findSelectedProjectUserConnectionsByUser: findSelectedProjectUserConnectionsByUser,
			createProjectUserConnection: createProjectUserConnection,
			updateProjectUserConnection: updateProjectUserConnection,
			deleteProjectUserConnection: deleteProjectUserConnection,
			deleteProjectUserConnectionOverProjectAndContact: deleteProjectUserConnectionOverProjectAndContact, 
			deleteProjectUserConnectionOverUserAndContact: deleteProjectUserConnectionOverUserAndContact, 
			canProjectUserConnectionRoleSeeConfidentialDocumentFiles: canProjectUserConnectionRoleSeeConfidentialDocumentFiles
		}; 					
		
		return service;
		
		////////////
		
		function findProjectUserConnectionByUser(userId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/user/' + userId);
		}
		
		function findProjectUserConnectionByProject(projectId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/project/' + projectId);
		}
		
		function findUsersOfProjectUserConnectionByProject(projectId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/project/users/' + projectId);
		}
		
		function findProjectUserConnectionById(projectUserConnectionId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/' + projectUserConnectionId);
		}
		
		function findProjectUserConnectionsByContactAndProject(contactId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/contact/' + contactId + '/project');
		}
		
		function findProjectUserConnectionsByContactAndUser(contactId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/contact/' + contactId + '/user');
		}
		
		function findProjectUserConnectionsOfUserByProjectSearchString(userId, searchTerm) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/' + userId + '/' + searchTerm + '/search');
		}
		
		function findSelectedProjectUserConnectionsByUser(userId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/user/' + userId + '/selected');
		}
		
		function createProjectUserConnection(projectUserConnection) {
			return $http.post(api_config.BASE_URL + '/projectuserconnections', projectUserConnection);
		}
		
		function updateProjectUserConnection(projectUserConnection) {
			return $http.put(api_config.BASE_URL + '/projectuserconnections', projectUserConnection);
		}
		
		function deleteProjectUserConnection(projectUserConnectionId) {
			return $http.delete(api_config.BASE_URL + '/projectuserconnections/' + projectUserConnectionId);
		}
		
		function deleteProjectUserConnectionOverProjectAndContact(projectId, contactId) {
			return $http.delete(api_config.BASE_URL + '/projectuserconnections/' + projectId + '/' + contactId + '/project/contact');
		}
		
		function deleteProjectUserConnectionOverUserAndContact(userId, contactId) {
			return $http.delete(api_config.BASE_URL + '/projectuserconnections/' + userId + '/' + contactId + '/user/contact');
		}
		
		function canProjectUserConnectionRoleSeeConfidentialDocumentFiles(projectUserConnectionId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/confidentialdocumentfiles/' + projectUserConnectionId);
		}
    }
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.projects')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getProjectState());
    	 
    	////////////
			    	
    	function getProjectState() {
    		var state = {
    			name: 'auth.projects',
				url: '/projects/:userId',
				templateUrl: 'app/projects/projects/projects.html',
				controller: 'ProjectsController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					optionsService: 'optionsService',
					projectsService: 'projectsService',
					projectUserConnectionService: 'projectUserConnectionService',
					projectUserConnectionRoles: function(optionsService) {
						return optionsService.getRoles();
					},
					provinceTypes: function(optionsService) {
						return optionsService.getProvinceTypes();
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.directive('createProjectDirective', createProjectDirective);
    
    createProjectDirective.$inject = ['$timeout', 'Upload', 'projectsService', 'projectUserConnectionService', 'dateUtilityService', 'calendarService', 'calendarEventUserConnectionService', 'projectsOfTenantModalService'];
    
	function createProjectDirective($timeout, Upload, projectsService, projectUserConnectionService, dateUtilityService, calendarService, calendarEventUserConnectionService, projectsOfTenantModalService) {
		var directive = {
			restrict: 'E',
			scope: {
				project: "=",
				projectUserConnections: "=",
				currentUser: "=",
				typeOfChange: "=",
				invoker: "=",
				provinceTypes: "=",
				projectDataSheet: "="
			},
			templateUrl: 'app/projects/directives/createProjectDirective/createProject.html',
			link: function($scope) {
				var createProjectClicked = false;
				$scope.datasheet = false;
				$scope.projectEndDateBeforeStartDateError = false;
				$scope.deletedProjectDataSheet = false;
				$scope.project.start = dateUtilityService.convertToDateOrUndefined($scope.project.start);
				$scope.project.end = dateUtilityService.convertToDateOrUndefined($scope.project.end);
				$scope.createCalendarEntry = false;
				setProjectDataSheetApprovalOrdersDate();
				setProjectDataSheetChangesDate();
				$scope.showAllProjectsOfTenant = showAllProjectsOfTenant;
				
				function showAllProjectsOfTenant() {
					projectsService.getAllProjectsOfTenant().then(function successCallback(response) {
						projectsOfTenantModalService.showProjectsOfTenantModal(response.data);
		    		}, function errorCallback(response) {
		    			console.log('error createProject.directive#showAllProjectsOfTenant');
			   		});
				}
				
				function setProjectDataSheetApprovalOrdersDate() {
					if($scope.projectDataSheet == null || $scope.projectDataSheet.projectDataSheetApprovalOrders == null) {
						return;
					}
					for(var i = 0; i < $scope.projectDataSheet.projectDataSheetApprovalOrders.length; i++) {
						$scope.projectDataSheet.projectDataSheetApprovalOrders[i].approvalOrderDate = dateUtilityService.convertToDateOrUndefined($scope.projectDataSheet.projectDataSheetApprovalOrders[i].approvalOrderDate);
					}
				}
				
				function setProjectDataSheetChangesDate() {
					if($scope.projectDataSheet == null || $scope.projectDataSheet.projectDataSheetChanges == null) {
						return;
					}
					for(var i = 0; i < $scope.projectDataSheet.projectDataSheetChanges.length; i++) {
						$scope.projectDataSheet.projectDataSheetChanges[i].changeDate = dateUtilityService.convertToDateOrUndefined($scope.projectDataSheet.projectDataSheetChanges[i].changeDate);
					}
				}
				
				$scope.openProjectStartDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.projectStartDatePicker = true;
				};
				
				$scope.openProjectEndDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.projectEndDatePicker = true;
				};
				
				$scope.openApprovalOrderDatePicker = function($event, projectDataSheetApprovalOrder) {
					$event.preventDefault();
					$event.stopPropagation();
					projectDataSheetApprovalOrder.approvalOrderDatePicker = true;
				};
				
				$scope.openChangeDatePicker = function($event, projectDataSheetChange) {
					$event.preventDefault();
					$event.stopPropagation();
					projectDataSheetChange.changeDatePicker = true;
				};
				
				$scope.removeProjectLogo = function() {
					$scope.project.logoName = 'standardLogo.png';
				};
				
				$scope.createOrChangeProject = function() {
					var projectEndBeforeProjectStart = dateUtilityService.isDateBeforeOrEqualOtherDate($scope.project.start, $scope.project.end);
					if(projectEndBeforeProjectStart && $scope.project.start instanceof Date && $scope.project.end instanceof Date) {
						$scope.projectEndDateBeforeStartDateError = true;
						return;
					}
					$scope.projectEndDateBeforeStartDateError = false;
					if($scope.typeOfChange == 'CREATE') {
						$scope.createProject(); 
					} else if($scope.typeOfChange == 'CHANGE') {
						$scope.changeProject();
					}
				};
				
				$scope.uploadProjectLogo = function(logoFiles, logoErrFiles) {
					var files = logoFiles;
		            angular.forEach(files, function(file) {
		                file.upload = Upload.upload({
		                    url: '/api/projects/project/projectlogo/upload/' + $scope.currentUser.id,
		                    data: {file: file}
		                });
		                file.upload.then(function (response) {
		                    $timeout(function () {
		                        file.result = response.data;
		                        $scope.project.logoName = response.data.logoName;
		                        $scope.$apply();
		                    });
		                }, function (response) {
		                    if (response.status > 0) {
		                    	$scope.errorMsg = response.status + ': ' + response.data;
		                    }
		                }, function (evt) {
		                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
		                });
		            });
				};
				
				$scope.deleteProjectDataSheet = function() {
					projectsService.deleteProjectDataSheet($scope.project.id).then(function successCallback(response) {
						$scope.initProjectDataSheet(false);
						$scope.deletedProjectDataSheet = true;
						$scope.setProjectDataSheetDeletedTimeout();
		    			console.log('datasheet deleted in createProject.directive.js');
		    		}, function errorCallback(response) {
		    			console.log('error createProject.directive#deleteProjectDataSheet');
			   		});
				};
				
				$scope.createProject = function() {
		    		if($scope.project.name === '') {
		    			return;
		    		}
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		$scope.project.creationUserId = $scope.currentUser.id;
		    		projectsService.createProject($scope.project, false).then(function successCallback(response) {
		    			$scope.project.name = '';
		    			$scope.project.description = '';
		    			$scope.project.start = '';
		    			$scope.project.logoName = '';
		    			$scope.project.end = '';
		    			//$scope.createCalendarEntry = false;
		    			$scope.creationUserId = '';
		    			
		    			if($scope.datasheet === true) {
							Date.prototype.toJSON = function () { return this.toLocaleString(); };
							$scope.projectDataSheet.projectId = response.data.id;
				    		projectsService.createProjectDataSheet($scope.projectDataSheet).then(function(response) {
				    			$scope.initProjectDataSheet(false);
				    			console.log('datasheet created in createProject.directive.js');
				    		}, function errorCallback(response) {
				    			console.log('error createProject.directive#createProjectDataSheet');
					   		});
		    			}
		    			
		    			var createdProject = response.data;
		    			var projectUserConnection = {};
		    			projectUserConnection.user = $scope.currentUser;
		    			projectUserConnection.project = createdProject;
		    			projectUserConnection.projectUserConnectionRole = 'PROJECT_MANAGER';
		    			projectUserConnectionService.createProjectUserConnection(projectUserConnection).then(function successCallback(response) {
		    				var projectUserConnection = response.data;
		    				projectUserConnection.stillCreated = true;
		    				$scope.projectUserConnections.splice(0, 0, projectUserConnection);
		    				
		    				if($scope.createCalendarEntry) {
		    					if(createdProject.start != null && typeof createdProject.start != 'undefined') {
		    						var startDateTime = dateUtilityService.convertToDateOrUndefined(createdProject.start);
		    						startDateTime.setHours(8,0,0,0);
		    						var endDateTime = dateUtilityService.convertToDateOrUndefined(createdProject.start);
		    						endDateTime.setHours(9,0,0,0);
		    						
		    						var calendarEventStart = {};
		    						calendarEventStart.title = createdProject.name;
		    						calendarEventStart.startsAt = startDateTime.getTime();
		    						calendarEventStart.endsAt = endDateTime.getTime();
		    						calendarEventStart.location = "Projektstart";
		    						calendarEventStart.calendarEventTyp = 'MEETING';
		    						calendarEventStart.project = createdProject;
		    						calendarEventStart.tenant = $scope.currentUser.tenant;
		    						calendarEventStart.serialDate = false;

			    					calendarService.create(calendarEventStart, false, 'NO_SERIAL_DATE', '01.01.2017').then(function (response) {
			    						var createdCalendarEvents = response.data;
			    						var calendarUserConnections = [];
			    						var calendarUserConnection = {};
			    						calendarUserConnection.user = projectUserConnection.user;
			    						calendarUserConnection.calendarEvent = createdCalendarEvents[0];
			    						calendarUserConnection.emailActive = true;
			    						calendarUserConnections.push(calendarUserConnection);
			    						
			    						calendarEventUserConnectionService.create(createdCalendarEvents[0].id, calendarUserConnections, 0).then(function (response) {
		    			    				console.log('created calendarEventUserConnection in createProject.directive.js');
		    			    			}).catch(function (data) {
		    								console.log('error createNewCalendarEvent.service#create#calendarEventUserConnection: ' + data);
		    							});
			    		    		}).catch(function (data) {
			    						console.log('error createNewCalendarEvent.service#createCalendarEvent: ' + data);
			    					});
		    					}
		    					if(createdProject.end != null && typeof createdProject.end != 'undefined') {
		    						var startDateTimeEnd = dateUtilityService.convertToDateOrUndefined(createdProject.end);
		    						startDateTimeEnd.setHours(8,0,0,0);
		    						var endDateTimeEnd = dateUtilityService.convertToDateOrUndefined(createdProject.end);
		    						endDateTimeEnd.setHours(9,0,0,0);
		    						
		    						var calendarEventEnd = {};
		    						calendarEventEnd.title = createdProject.name;
		    						calendarEventEnd.location = "Projektende";
		    						calendarEventEnd.startsAt = startDateTimeEnd.getTime();
		    						calendarEventEnd.endsAt = endDateTimeEnd.getTime();
		    						calendarEventEnd.calendarEventTyp = 'MEETING';
		    						calendarEventEnd.project = createdProject;
		    						calendarEventEnd.tenant = $scope.currentUser.tenant;
		    						calendarEventEnd.serialDate = false;

			    					calendarService.create(calendarEventEnd, false, 'NO_SERIAL_DATE', '01.01.2017').then(function (response) {
			    						var createdCalendarEvents = response.data;

			    						var calendarUserConnections = [];
			    						var calendarUserConnection = {};
			    						calendarUserConnection.user = projectUserConnection.user;
			    						calendarUserConnection.calendarEvent = createdCalendarEvents[0];
			    						calendarUserConnection.emailActive = true;
			    						calendarUserConnections.push(calendarUserConnection);
			    						
			    						calendarEventUserConnectionService.create(createdCalendarEvents[0].id, calendarUserConnections, 0).then(function (response) {
		    			    				console.log('created calendarEventUserConnection in createProject.directive.js');
		    			    			}).catch(function (data) {
		    								console.log('error createNewCalendarEvent.service#create#calendarEventUserConnection: ' + data);
		    							});
			    		    		}).catch(function (data) {
			    						console.log('error createNewCalendarEvent.service#createCalendarEvent: ' + data);
			    					});
		    					}
		    				}
			   	   		 }, function errorCallback(response) {
			   	   			  console.log('error createProject.directive#createProjectUserConnection');
			   	   		 });
		    			$scope.createProjectClicked = true;
		    			$scope.setProjectCreatedTimeout();
			   		 }, function errorCallback(response) {
			   			  console.log('error createProject.directive#createProject');
			   		 });
		    	};
		    	
		    	$scope.changeProject = function() {
					Date.prototype.toJSON = function () { return this.toLocaleString(); };
					projectsService.updateProject($scope.project).then(function successCallback(response) {
						if($scope.datasheet === true && $scope.projectDataSheet !== '') {
							Date.prototype.toJSON = function () { return this.toLocaleString(); };
							$scope.projectDataSheet.projectId = response.data.id;
				    		projectsService.updateProjectDataSheet($scope.projectDataSheet).then(function successCallback(response) {
				    			$scope.invoker.closeChangeProjectPropertiesModal();
				    			console.log('datasheet updated in createProject.directive.js');
				    		}, function errorCallback(response) {
				    			console.log('error createProject.directive#updateProjectDataSheet');
					   		});
		    			} else {
		    				$scope.invoker.closeChangeProjectPropertiesModal();
		    			}
					}, function errorCallback(response) {
			   			  console.log('error createProject.directive#changeProject');
			   		});
				};
				
				$scope.initProjectDataSheet = function(createNewObject) {
					if(createNewObject === true) {
						$scope.projectDataSheet = {};
					}
					if($scope.projectDataSheet === null || $scope.projectDataSheet === '') {
						return;
					}
		    		$scope.projectDataSheet.operatorName = '';
		    		$scope.projectDataSheet.operatorStreet = '';
		    		$scope.projectDataSheet.operatorNumber = 0;
		    		$scope.projectDataSheet.operatorZip = 0;
		    		$scope.projectDataSheet.operatorLocation = '';
		    		$scope.projectDataSheet.operatorProvinceType = null;

		    		$scope.projectDataSheet.locationStreet = '';
		    		$scope.projectDataSheet.locationNumber = 0;
		    		$scope.projectDataSheet.locationZip = 0;
		    		$scope.projectDataSheet.locationLocation = '';
		    		$scope.projectDataSheet.locationProvinceType = null;
		    		$scope.projectDataSheet.locationRegion = '';
		    		$scope.projectDataSheet.locationEstateNumber = '';
		    		$scope.projectDataSheet.locationArchiveNumber = '';
		    		$scope.projectDataSheet.locationCastralCommunity = '';
		    		
		    		$scope.projectDataSheet.identificationFacilities = [{
		    			identificationFacilityName: '',
		    			identificationFacilityPower: '',
		    			identificationFacilityType: ''
		    		}];
		    		
		    		$scope.projectDataSheet.projectDataSheetApprovalOrders = [{
				    	approvalOrderAgency: '',
					    approvalOrderNumber: '',
					    approvalOrderDate: null
				    }];

		    		$scope.projectDataSheet.projectDataSheetAuthorities = [{
		    			authorityName: '',
		    			authorityStreet: '',
		    			authorityNumber: 0,
		    			authorityZip: 0,
		    			authorityLocation: '',
		    			authorityProvinceType: null,
		    			authorityApprovalPerson: ''
		    		}];
		    		
		    		$scope.projectDataSheet.projectDataSheetChanges = [{ 
				    	changeContent: '',
					    chanceResponsibleAgency: '',
					    changeNumber: '',
					    changeDate: null
				    }];
		    	};
		    	
		    	$scope.addProjectDataSheetApprovalOrder = function() {
					var projectDataSheetApprovalOrder = {
						approvalOrderAgency: '',
					    approvalOrderNumber: '',
					    approvalOrderDate: null
						};
					if($scope.projectDataSheet == null || $scope.projectDataSheet === '') {
						$scope.initProjectDataSheet(true); 
					}
					if($scope.projectDataSheet.projectDataSheetApprovalOrders == null) {
						$scope.projectDataSheet.projectDataSheetApprovalOrders = [];
					}
					$scope.projectDataSheet.projectDataSheetApprovalOrders.push(projectDataSheetApprovalOrder);
				};
				
				$scope.addIdentificationFacility = function() {
					var projectDataSheetIdentificationFacility = {
				    	identificationFacilityName: '',
					    identificationFacilityPower: '',
					    identificationFacilityType: ''
				    };
					if($scope.projectDataSheet == null || $scope.projectDataSheet === '') {
						$scope.initProjectDataSheet(true); 
					}
					if($scope.projectDataSheet.projectDataSheetIdentificationFacilities == null) {
						$scope.projectDataSheet.projectDataSheetIdentificationFacilities = [];
					}
					$scope.projectDataSheet.projectDataSheetIdentificationFacilities.push(projectDataSheetIdentificationFacility);
				};
				
				$scope.addProjectDataSheetChange = function() {
					var projectDataSheetChange = {
						changeContent: '',
						chanceResponsibleAgency: '',
						changeNumber: '',
						changeDate: null
						};
					if($scope.projectDataSheet == null || $scope.projectDataSheet === '') {
						$scope.initProjectDataSheet(true);
					}
					if($scope.projectDataSheet.projectDataSheetChanges == null) {
						$scope.projectDataSheet.projectDataSheetChanges = [];
					}
					$scope.projectDataSheet.projectDataSheetChanges.push(projectDataSheetChange);
				};
				
				$scope.addProjectDataSheetAuthority = function() {
					var projectDataSheetAuthority = {
						authorityName: '',
				    	authorityStreet: '',
				    	authorityNumber: 0,
				    	authorityZip: 1234,
				    	authorityLocation: '',
				    	authorityProvinceType: '', 
				    	authorityApprovalPerson: ''
						};
					if($scope.projectDataSheet == null || $scope.projectDataSheet === '') {
						$scope.initProjectDataSheet(true);
					}
					if($scope.projectDataSheet.projectDataSheetAuthorities == null) {
						$scope.projectDataSheet.projectDataSheetAuthorities = [];
					}
					$scope.projectDataSheet.projectDataSheetAuthorities.push(projectDataSheetAuthority);
				};
				
				$scope.removeProjectDataSheetApprovalOrder = function(index) {
					if($scope.projectDataSheet.projectDataSheetApprovalOrders.length > 1) {
						$scope.projectDataSheet.projectDataSheetApprovalOrders.splice(index, 1);
					}
				};
				
				$scope.removeProjectDataSheetChange = function(index) {
					if($scope.projectDataSheet.projectDataSheetChanges.length > 1) {
						$scope.projectDataSheet.projectDataSheetChanges.splice(index, 1);
					}
				};
				
				$scope.removeIdentificationFacility = function(index) {
					if($scope.projectDataSheet.projectDataSheetIdentificationFacilities.length > 1) {
						$scope.projectDataSheet.projectDataSheetIdentificationFacilities.splice(index, 1);
					}
				};
				
				$scope.removeProjectDataSheetAuthority = function(index) {
					if($scope.projectDataSheet.projectDataSheetAuthorities.length > 1) {
						$scope.projectDataSheet.projectDataSheetAuthorities.splice(index, 1);
					}
				};
				
				$scope.changeProjectDataSheet = function() {
				};
				
				$scope.setProjectCreatedTimeout = function() {
		      		$timeout(function() {
		      			$scope.createProjectClicked = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.setProjectDataSheetDeletedTimeout = function() {
		      		$timeout(function() {
		      			$scope.deletedProjectDataSheet = false;
		  			   }, 2000); 
		      	};
			 }
		};
		return directive;
		
		////////////
	}
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('changeProjectPropertiesModalService', changeProjectPropertiesModalService);
        
    changeProjectPropertiesModalService.$inject = ['$modal', '$stateParams'];
    
    function changeProjectPropertiesModalService($modal, $stateParams) {
		var service = {
			showChangeProjectPropertiesModal: showChangeProjectPropertiesModal
		};		
		return service;
		
		////////////
				
		function showChangeProjectPropertiesModal(project, projectDataSheet, provinceTypes, currentUser) {
			 var changeProjectPropertiesModal = $modal.open({
				controller: ChangeProjectPropertiesModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	project: function() {
			    		return project;
			    	}, 
			    	projectDataSheet: function() {
			    		return projectDataSheet;
			    	},
			    	provinceTypes: function() {
			    		return provinceTypes;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/changeProjectProperties/changeProjectProperties.modal.html'
			});
			return changeProjectPropertiesModal;
			
			function ChangeProjectPropertiesModalController($modalInstance, $scope, project, projectDataSheet, provinceTypes, currentUser) {
		    	var vm = this; 
		    	vm.project = project;
		    	vm.projectDataSheet = projectDataSheet;
		    	vm.provinceTypes = provinceTypes;
		    	vm.currentUser = currentUser;
		    	
		    	vm.closeChangeProjectPropertiesModal = closeChangeProjectPropertiesModal;
		    	
		    	////////////
		    	
		    	function closeChangeProjectPropertiesModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectDataSheetService', projectDataSheetService);
        
    projectDataSheetService.$inject = ['$modal', '$stateParams'];
    
    function projectDataSheetService($modal, $stateParams) {
		var service = {
			showProjectDataSheetModal: showProjectDataSheetModal
		};		
		return service;
		
		////////////
				
		function showProjectDataSheetModal(projectDataSheet) {
			 var projectDataSheetModal = $modal.open({
				controller: ProjectDataSheetModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: { 
			    	projectDataSheet: function() {
			    		return projectDataSheet;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/projectDataSheetModal/projectDataSheet.modal.html'
			});
			return projectDataSheetModal;
			
			function ProjectDataSheetModalController($modalInstance, $scope, projectDataSheet) {
		    	var vm = this; 
		    	vm.projectDataSheet = projectDataSheet;
		    	
		    	vm.closeProjectDataSheetModal = closeProjectDataSheetModal;
		    	
		    	////////////
		    	
		    	function closeProjectDataSheetModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectsOfTenantModalService', projectsOfTenantModalService);
        
    projectsOfTenantModalService.$inject = ['$modal', '$stateParams'];
    
    function projectsOfTenantModalService($modal, $stateParams) {
		var service = {
			showProjectsOfTenantModal: showProjectsOfTenantModal
		};		
		return service;
		
		////////////
				
		function showProjectsOfTenantModal(projects) {
			 var projectsOfTenantModal = $modal.open({
				controller: ProjectsOfTenantModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	projects: function() {
			    		return projects;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/projectsOfTenantModal/projectsOfTenant.modal.html'
			});
			return projectsOfTenantModal;
			
			function ProjectsOfTenantModalController($modalInstance, $scope, projects) {
		    	var vm = this; 
		    	vm.projects = projects;

		    	vm.closeProjectsOfTenantModal = closeProjectsOfTenantModal;
		    	
		    	////////////
		    	
		    	function closeProjectsOfTenantModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectUserManagementModalService', projectUserManagementModalService);
        
    projectUserManagementModalService.$inject = ['$modal', '$stateParams', '$timeout', 'projectUserConnectionService'];
    
    function projectUserManagementModalService($modal, $stateParams, $timeout, projectUserConnectionService) {
		var service = {
			showProjectUserManagementModal: showProjectUserManagementModal
		};		
		return service;
		
		////////////
				
		function showProjectUserManagementModal(project, projectUserConnectionsOfProject, currentProjectUserConnections, projectUserConnectionRoles, currentUser, invoker) {
			 var projectUserManagementModal = $modal.open({
				controller: ProjectUserManagementModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	project: function() {
			    		return project;
			    	},
			    	projectUserConnectionsOfProject: function() {
			    		return projectUserConnectionsOfProject;
			    	},
			    	currentProjectUserConnections: function() {
			    		return currentProjectUserConnections;
			    	},
			    	projectUserConnectionRoles: function() {
			    		return projectUserConnectionRoles;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/projectUserManagement/projectUserManagement.modal.html'
			});
			return projectUserManagementModal;
			
			function ProjectUserManagementModalController($modalInstance, $scope, project, projectUserConnectionsOfProject, currentProjectUserConnections, projectUserConnectionRoles, currentUser, invoker) {
		    	var vm = this; 
		    	vm.project = project;
		    	vm.projectUserConnectionsOfProject = projectUserConnectionsOfProject;
		    	vm.currentProjectUserConnections = currentProjectUserConnections;
		    	vm.projectUserConnectionRoles = projectUserConnectionRoles;
		    	vm.currentUser = currentUser;
		    	vm.invoker = invoker;
		    	vm.selectedUser = null;
		    	vm.userStillExist = false;
		    	vm.documentsAssignerError = false;
		    	
		    	vm.addUserToProject = addUserToProject;
		    	vm.removeUserFromProject = removeUserFromProject;
		    	vm.changeProjectUserConnectionRole = changeProjectUserConnectionRole;
		    	vm.findContactAndUserBySearchString = findContactAndUserBySearchString;
		    	vm.closeProjectUserManagementModal = closeProjectUserManagementModal;
		    	vm.userOrContactSelected = userOrContactSelected;
		    	
		    	////////////
		    	
		    	function setUpdateTimeout(projectUserConnection) {
		      		$timeout(function() {
		      			projectUserConnection.updateSuccessfully = false;
		  			   }, 2000); 
		      	}
		    	
		    	function setDeleteTimeout() {
		      		$timeout(function() {
		      			vm.documentsAssignerError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function addUserToProject() {
		    		if(vm.selectedUser == null || typeof vm.selectedUser === 'undefined') {
		    			vm.selectedUser = null;
						return;
					  }
					if(userOrContactStillExists(vm.selectedUser)) {
						vm.userStillExist = true;
						vm.selectedUser = null;
						return;
					}  
		    		vm.messageGroupCreationError = false;
		    		var projectUserConnection = {};
		    		if(vm.selectedUser.contactUserType == 'CONTACT') {
		    			projectUserConnection.contact = vm.selectedUser;
		    		} else {
		    			projectUserConnection.user = vm.selectedUser;
		    		}
					
					projectUserConnection.project = vm.project;
					projectUserConnection.projectUserConnectionRole = vm.selectedProjectUserConnectionRole;
					projectUserConnectionService.createProjectUserConnection(projectUserConnection).then(function(response) {
						var projectUserConnection = response.data;
						vm.projectUserConnectionsOfProject.push(projectUserConnection);
						vm.selectedUser = null;
						vm.userStillExist = false;
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error projectsUserManagement.service#addUserToProject');
		   	   		 });	
		    	}
		    	
		    	function changeProjectUserConnectionRole(projectUserConnection) {
		    		projectUserConnectionService.updateProjectUserConnection(projectUserConnection).then(function(response) {
		    			projectUserConnection = response.data;
		    			for(var i = 0; i < currentProjectUserConnections.length; i++) {
		    				if(currentProjectUserConnections[i].id == projectUserConnection.id) {
		    					currentProjectUserConnections[i] = projectUserConnection;
		    				}
		    			}
		    			setUpdateFlagOfProjectUserConnection(projectUserConnection);
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error projectsUserManagement.service#addUserToProject');
		   	   		 });	
		    	}
		    	
		    	function setUpdateFlagOfProjectUserConnection(projectUserConnection) {
		    		for(var i = 0; vm.projectUserConnectionsOfProject.length; i++) {
		    			if(vm.projectUserConnectionsOfProject[i].id == projectUserConnection.id) {
		    				vm.projectUserConnectionsOfProject[i].updateSuccessfully = true;
		    				setUpdateTimeout(vm.projectUserConnectionsOfProject[i]);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function removeUserFromProject(projectUserConnectionOfProject) {
		    		projectUserConnectionService.deleteProjectUserConnection(projectUserConnectionOfProject.id).then(function(response) {
		    			removeProjectUserConnection(projectUserConnectionOfProject);
		   	   		 }, function errorCallback(response) {
		   	   			 if(response.data.error == 'ASSIGNED_DOCUMENTS_TO_USER') {
		   	   				 vm.documentsAssignerError = true;
		   	   				 setDeleteTimeout();
		   	   			 }
		   	   			  console.log('error projectsUserManagement.service#removeUserFromProject');
		   	   		 });	
		    	}
		    	
		    	function removeProjectUserConnection(projectUserConnectionOfProject) {
		    		for(var i = 0; vm.projectUserConnectionsOfProject.length; i++) {
		    			if(vm.projectUserConnectionsOfProject[i].id == projectUserConnectionOfProject.id) {
		    				vm.projectUserConnectionsOfProject.splice(i, 1);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function userOrContactSelected()  {
		    		if(vm.selectedUser.contactUserType == 'CONTACT') {
		    			vm.selectedProjectUserConnectionRole = 'CONTACT';
		    		}
		    	}
		    	
		    	function userOrContactStillExists(userOrContact) {
		    		for(var i = 0; i < vm.projectUserConnectionsOfProject.length; i++) {
		    			var user = vm.projectUserConnectionsOfProject[i].user;
		    			var contact = vm.projectUserConnectionsOfProject[i].contact;
		    			if(user != null) {
			    			if(vm.projectUserConnectionsOfProject[i].user.id === userOrContact.id) {
			    				return true;
			    			}
		    			} else if(contact != null) {
		    				if(vm.projectUserConnectionsOfProject[i].contact.id === userOrContact.id) {
			    				return true;
			    			}
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function findContactAndUserBySearchString(term) {
		    		return invoker.findContactAndUserBySearchString(term);
		    	}
		    	
		    	function closeProjectUserManagementModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('yesNoDeleteProjectModalService', yesNoDeleteProjectModalService);
        
    yesNoDeleteProjectModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoDeleteProjectModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(project, invoker) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	project: function() {
			    		return project;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, project, invoker) {
		    	var vm = this; 
		    	vm.project = project;
		    	vm.invoker = invoker;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		invoker.deleteProject(vm.project);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.controller('ProjectsController', ProjectsController);
    
    ProjectsController.$inject = ['$scope', '$timeout', 'currentUser', 'projectUserConnectionRoles', 'provinceTypes', 'userAuthorizationUserContainer', 'dateUtilityService', 'projectsService', 'projectUserConnectionService', 'userService', 'yesNoDeleteProjectModalService', 'changeProjectPropertiesModalService', 'projectUserManagementModalService', 'contactAndUserService', 'documentFileService', 'projectDataSheetService', 'printingPDFService', 'utilService'];
     
    function ProjectsController($scope, $timeout, currentUser, projectUserConnectionRoles, provinceTypes, userAuthorizationUserContainer, dateUtilityService, projectsService, projectUserConnectionService, userService, yesNoDeleteProjectModalService, changeProjectPropertiesModalService, projectUserManagementModalService, contactAndUserService, documentFileService, projectDataSheetService, printingPDFService, utilService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	vm.currentUser = currentUser;
    	vm.generalAvailableProjects = null;
    	vm.projectUserConnections = null;
    	vm.projectUserConnectionRoles = projectUserConnectionRoles.data;
    	vm.provinceTypes = provinceTypes.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.dateUtilityService = dateUtilityService;
    	vm.showProjectCreationTrigger = false;
    	vm.project = {
    			name: '',
    			description: '',
    			creationUserId: '',
    			start: null,
    			end: null,
    			color: '#007ab9', 
    			logoName: ''
    			};
    	vm.projectDataSheet = { 
				operatorName: '',
			    operatorStreet: '',
			    operatorNumber: 0,
			    operatorZip: 1234,
			    operatorLocation: '',
			    operatorProvinceType: 'EMPTY_PROVINCE',

			    locationStreet: '',
			    locationNumber: 0,
			    locationZip: 1234,
			    locationLocation: '',
			    locationProvinceType: 'EMPTY_PROVINCE',
			    locationRegion: '',
			    locationEstateNumber: '',
			    locationArchiveNumber: '',
			    locationCastralCommunity: '',

			    projectDataSheetIdentificationFacilities: [{
			    	identificationFacilityName: '',
				    identificationFacilityPower: '',
				    identificationFacilityType: ''
			    }],

			    projectDataSheetApprovalOrders: [{
			    	approvalOrderAgency: '',
				    approvalOrderNumber: '',
				    approvalOrderDate: null
			    }],

			    authorityName: '',
			    authorityStreet: '',
			    authorityNumber: 0,
			    authorityZip: 1234,
			    authorityLocation: '',
			    authorityProvinceType: 'EMPTY_PROVINCE',
			    authorityApprovalPerson: '',

			    projectDataSheetChanges: [{
			    	changeContent: '',
				    chanceResponsibleAgency: '',
				    changeNumber: '',
				    changeDate: null
			    }]
			};
    	vm.projectUserConnectionsOfSelectedProject = [];
    	vm.selectedUser = null;
    	vm.selectedProject = null;
    	vm.selectedProjectUserConnectionRole = null;
    	vm.orderType = 'name';
    	vm.projectNameFilter = null;
    	
    	vm.printContainer = printContainer;
    	vm.deleteProject = deleteProject;
    	vm.getAmountOfDocumentFilesWithUniqueName = getAmountOfDocumentFilesWithUniqueName;
    	vm.findContactAndUserBySearchString = findContactAndUserBySearchString;
    	vm.projectFilter = projectFilter;
    	vm.showYesNoDeleteModal = showYesNoDeleteModal;
    	vm.showProjectUserManagementModal = showProjectUserManagementModal;
    	vm.showChangeProjectPropertiesModalService = showChangeProjectPropertiesModalService;
    	vm.showProjectDataSheetModal = showProjectDataSheetModal;
    	loadGeneralAvailableProjects();
    	loadProjectUserConnections();
    	
    	////////////
    	
    	function loadGeneralAvailableProjects() {
    		projectsService.findProjectsGeneralAvailable().then(function(response) {
    			vm.generalAvailableProjects = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error in projects.controller.js#loadProjectUserConnections');
 	   		 });
    	}
    	
    	function loadProjectUserConnections() {
    		projectUserConnectionService.findProjectUserConnectionByUser(vm.currentUser.id).then(function(response) {
    			vm.projectUserConnections = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error in projects.controller.js#loadProjectUserConnections');
 	   		 });
    	}
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function getAmountOfDocumentFilesWithUniqueName(project) { 
    		documentFileService.countByProjectIdGroupByOriginalFileName(project.id, false, 'PROJECT').then(function(response) {
    			project.amountOfDocumentFilesWithUniqueName = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error in projects.controller.js#getAmountOfDocumentFilesWithUniqueName');
  	   		 });
    	}
    	
    	function showYesNoDeleteModal(project) {
    		yesNoDeleteProjectModalService.showYesNoModal(project, vm, 'PROJECT');
    	}
    	
    	function showChangeProjectPropertiesModalService(project) {
    		projectsService.findProjectDataSheet(project.id).then(function successCallback(response) {
    			var projectDataSheet = response.data;
    			if(project.start instanceof Date) {
    				project.start = dateUtilityService.formatDateToDateTimeString(project.start);
    			} 
    			if(project.end instanceof Date) {
    				project.end = dateUtilityService.formatDateToDateTimeString(project.end);
    			} 
    			changeProjectPropertiesModalService.showChangeProjectPropertiesModal(project, projectDataSheet, vm.provinceTypes, vm.currentUser);
   	   		 }, function errorCallback(response) {
   	   			  console.log('error projectUserManagementModalService#showChangeProjectPropertiesModalService');
   	   		 });
    	}
    	
    	function showProjectUserManagementModal(project, projectUserConnection) {
    		projectUserConnectionService.findProjectUserConnectionByProject(project.id).then(function(response) {
    			unsetProjectUserConnectionsSelection();
    			if(projectUserConnection != null) {
    				projectUserConnection.selected = true;
    			}
        		projectUserManagementModalService.showProjectUserManagementModal(project, response.data, vm.projectUserConnections, vm.projectUserConnectionRoles, currentUser, vm);
   	   		 }, function errorCallback(response) {
   	   			  console.log('error projectUserManagementModalService#showProjectUserManagementModal');
   	   		 });
    	}
    	
    	function deleteProject(project) { 
    		if(project.documentFiles != null && project.documentFiles.length > 0) {
    			project.deletionError = true;
    			return;
    		}
    		projectsService.deleteProject(project.id).then(function successCallback(response) {
    			var keepGoing = true;
    			angular.forEach(vm.projectUserConnections, function(value, key) {
    				if(keepGoing) {
	  				  if(value.project.id == project.id) {
	  					  vm.projectUserConnections.splice(key, 1);
	  					  keepGoing = false;
	  				  }
    				}
  				});
  	   		 }, function errorCallback(response) {
  	   			 project.deletionError = true;
  	   			 project.deletionErrorMessage = response.data.error;
  	   			 console.log('error projects.controller#deleteProject: ' + response.data.error + ', message: ' + response.data.message + ', status: ' + response.data.status);
  	   		 });
    	}
    	
    	function unsetProjectUserConnectionsSelection() {
    		for(var i = 0; i < vm.projectUserConnections.length; i++) {
    			vm.projectUserConnections[i].selected = false;
    		}
    	}
    	
    	function findContactAndUserBySearchString(searchString) { 
    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function (response) {
    			return response.data;
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findContactBySearchString: ' + data);
			});
    	}

    	function projectFilter(projectNameFilter) {
    		return function(item) {
    			if(projectNameFilter == null ||projectNameFilter === '') {
    				return true;
    			}
    			projectNameFilter = utilService.replaceAll(projectNameFilter, '\\*', '');
    			return item.project.name.indexOf(projectNameFilter) > -1;
    		  };
    	}
    	
    	function showProjectDataSheetModal(project) {
    		projectsService.findProjectDataSheet(project.id).then(function (response) {
    			projectDataSheetService.showProjectDataSheetModal(response.data); 
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findContactBySearchString: ' + data);
			});
    	}
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.reporting')
    	.factory('reportingService', reportingService);
        
    reportingService.$inject = ['$http', 'api_config'];
    
    function reportingService($http, api_config) {
		var service = {
			findByReportingSearchCriteria: findByReportingSearchCriteria,
			getActivitiesBySearchCriteria: getActivitiesBySearchCriteria,
			getFacilityDetailsBySearchCriteria: getFacilityDetailsBySearchCriteria
		};
		return service;
		
		////////////
		
		function findByReportingSearchCriteria(start, end, reportingSearchCriteria) {
			return $http.put(api_config.BASE_URL + '/reportings/reporting/' + start + '/' + end + '/', reportingSearchCriteria);
		}
		
		function getActivitiesBySearchCriteria(start, end, reportingSearchCriteria) {
			return $http.put(api_config.BASE_URL + '/reportings/reporting/activities/' + start + '/' + end + '/', reportingSearchCriteria);
		}
		
		function getFacilityDetailsBySearchCriteria(start, end, reportingSearchCriteria) {
			return $http.put(api_config.BASE_URL + '/reportings/reporting/facilitydetails/' + start + '/' + end + '/', reportingSearchCriteria);
		}
    }
})();

(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.reporting')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getReportingState());
    	 
    	////////////
			    	
    	function getReportingState() {
    		var state = {
    			name: 'auth.reporting',
				url: '/reporting/:userId',
				templateUrl: 'app/reporting/reporting/reporting.html',
				controller: 'ReportingController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					userAuthorizationUserContainer: function($stateParams, userService) {
			    		return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
			    	}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.reporting')
    	.controller('ReportingController', ReportingController);
    
    ReportingController.$inject = ['$scope', 'dateUtilityService', 'reportingService', 'userAuthorizationUserContainer', 'calendarService', 'optionsService', 'userService', 'contactAndUserService'];
     
    function ReportingController($scope, dateUtilityService, reportingService, userAuthorizationUserContainer, calendarService, optionsService, userService, contactAndUserService) {
	    $scope.vm = this; 
    	var vm = this; 
    	
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.from = new Date();
    	vm.until = new Date();
    	vm.selectedFacilityDetailsAndActivitiesUser = null;
    	vm.selectedFacilityDetailsAndActivitiesContact = null;
    	vm.selectedFacilityDetailsAndActivitiesCalendarEvent = null;
    	vm.activities = [];
    	vm.facilityDetails = [];
    	vm.facilityDetailsTypes = [];
    	vm.reportingContainers = [];
    	vm.activityTypes = [];
    	vm.calendarEvents = [];
    	vm.fromTrigger = false;
    	vm.untilTrigger = false;
    	vm.loadingTrigger = false;
    	
    	vm.openFrom = openFrom;
    	vm.openUntil = openUntil;
    	vm.loadActivityTypes = loadActivityTypes;
    	vm.getActivitiesBySearchCriteria = getActivitiesBySearchCriteria;
    	vm.getFacilityDetailsBySearchCriteria = getFacilityDetailsBySearchCriteria;
    	vm.searchCalendarEvents = searchCalendarEvents;
    	vm.search = search;
    	vm.findUsersOrContactsBySearchString = findUsersOrContactsBySearchString;
    	loadActivityTypes();
    	loadFacilityDetailTypes();
    	
    	////////////
    	
    	function openFrom($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.fromTrigger = true;
		}
    	
    	function openUntil($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.untilTrigger = true;
		}
    	
    	function loadActivityTypes() {     
    		optionsService.getActivityTypes('all').then(function(response) {
    			vm.activityTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#loadActivityTypes');
  	   		 });
    	}
    	
    	function loadFacilityDetailTypes() {     
    		optionsService.getFacilityDetailsTypes().then(function(response) {
    			vm.facilityDetailsTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#loadFacilityDetailTypes');
  	   		 });
    	}
    	
    	function findUsersOrContactsBySearchString(searchString) { 
    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function (response) {
    			return response.data;
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findContactBySearchString: ' + data);
			});
    	}
    	
    	function search() {
    		if(vm.selectedUser == null) {
    			return;
    		}
    		vm.loadingTrigger = true;
    		vm.reportingContainers = [];
    		var start = dateUtilityService.formatDateToString(vm.from);
    		var end = dateUtilityService.formatDateToString(vm.until);
    		var reportingSearchCriteria = {};
    		if(vm.selectedUser.contactUserType == 'USER') {
    			reportingSearchCriteria.userId = vm.selectedUser.id;
    			reportingSearchCriteria.contactId = null; 
    		} else if (vm.selectedUser.contactUserType == 'CONTACT') {
    			reportingSearchCriteria.contactId = vm.selectedUser.id;
    			reportingSearchCriteria.userId = null;
    		}
    		reportingService.findByReportingSearchCriteria(start, end, reportingSearchCriteria).then(function (response) {
    			vm.reportingContainers = response.data;
    			for(var i = 0; i < vm.reportingContainers.length; i++) {
    				vm.reportingContainers[i].collapse = false;
    			}
    			vm.loadingTrigger = false;
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findUsersOrContactsBySearchString: ' + data);
			});
    	}
    	
    	function searchCalendarEvents() {
    		vm.loadingTrigger = true;
    		var start = dateUtilityService.formatDateToString(vm.from);
    		var end = dateUtilityService.formatDateToString(vm.until);
     		
     		var calendarEventSearchCriteriaContainer = {};
     		calendarEventSearchCriteriaContainer.start = start;
     		calendarEventSearchCriteriaContainer.end = end;
     		calendarEventSearchCriteriaContainer.userId = vm.selectedFacilityDetailsAndActivitiesUser != null ? vm.selectedFacilityDetailsAndActivitiesUser.id : null;
     		calendarEventSearchCriteriaContainer.calendarEventType = vm.selectedCalendarEventType != null ? vm.selectedCalendarEventType : null;
     		
    		calendarService.findCalendarEventOfCriteria(calendarEventSearchCriteriaContainer).then(function(response) {
    			vm.calendarEvents = response.data;
    			vm.loadingTrigger = false;
	    	}).catch(function (data) {
				console.log('error calendarEventInList.service#searchCalendarEvents: ' + data);
	    	});
    	}
    	
    	function getActivitiesBySearchCriteria() {
    		vm.loadingTrigger = true;
    		var start = dateUtilityService.formatDateToString(vm.from);
    		var end = dateUtilityService.formatDateToString(vm.until);
    		var reportingSearchCriteria = {};
    		reportingSearchCriteria.userId = vm.selectedFacilityDetailsAndActivitiesUser != null ? vm.selectedFacilityDetailsAndActivitiesUser.id : null;
    		reportingSearchCriteria.calendarEventId = vm.selectedFacilityDetailsAndActivitiesCalendarEvent != null ? vm.selectedFacilityDetailsAndActivitiesCalendarEvent.id : null;
    		reportingSearchCriteria.contactId = vm.selectedFacilityDetailsAndActivitiesContact != null ? vm.selectedFacilityDetailsAndActivitiesContact.id : null;
    		reportingService.getActivitiesBySearchCriteria(start, end, reportingSearchCriteria).then(function(response) {
    			vm.activities = response.data;
    			vm.loadingTrigger = false;
	   		 }, function errorCallback(response) {
	   			console.log('error reporting.controller#getActivitiesBySearchCriteria');
	   		 });
    	}
    	
    	function getFacilityDetailsBySearchCriteria() {
    		vm.loadingTrigger = true;
    		var start = dateUtilityService.formatDateToString(vm.from);
    		var end = dateUtilityService.formatDateToString(vm.until);
    		var reportingSearchCriteria = {};
    		reportingSearchCriteria.userId = vm.selectedFacilityDetailsAndActivitiesUser != null ? vm.selectedFacilityDetailsAndActivitiesUser.id : null;
    		reportingSearchCriteria.calendarEventId = vm.selectedFacilityDetailsAndActivitiesCalendarEvent != null ? vm.selectedFacilityDetailsAndActivitiesCalendarEvent.id : null;
    		reportingSearchCriteria.contactId = vm.selectedFacilityDetailsAndActivitiesContact != null ? vm.selectedFacilityDetailsAndActivitiesContact.id : null;
    		reportingService.getFacilityDetailsBySearchCriteria(start, end, reportingSearchCriteria).then(function(response) {
    			vm.facilityDetails = response.data;
    			vm.loadingTrigger = false;
	   		 }, function errorCallback(response) {
	   			console.log('error reporting.controller#getFacilityDetailsBySearchCriteria');
	   		 });
    	}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.search')
    	.factory('searchService', searchService);
        
    searchService.$inject = ['$http', 'api_config'];
    
    function searchService($http, api_config) {
		var service = {
			search: search,
			getSearchHistory: getSearchHistory
		};
		
		return service;
		
		////////////
		
		function search(searchString, categories, lastSearchResultLength) {
			return $http.put(api_config.BASE_URL + '/search/' + searchString + '/' + lastSearchResultLength, categories);
		}
		
		function getSearchHistory(userId) {
			return $http.get(api_config.BASE_URL + '/search/searchhistory/' + userId);
		}
    }
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.search')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getSearchState());
    	 
    	////////////
			    	
    	function getSearchState() {
    		var state = {
    			name: 'auth.search',
				url: '/search/:userId',
				templateUrl: 'app/search/search/search.html',
				controller: 'SearchController',
				controllerAs: 'vm',
				resolve: {
					searchService: 'searchService',
					labelsPredefinedService: 'labelsPredefinedService',
					userService: 'userService',
					optionsService: 'optionsService',
					thinningPolymerDateTimeValueService: 'thinningPolymerDateTimeValueService',
					searchHistory: function getSearchHistory($stateParams, searchService) {
						return searchService.getSearchHistory($stateParams.userId);
					},
					labelsPredefined: function getAllLabelsPredefined($stateParams, labelsPredefinedService) {
						return labelsPredefinedService.getAllLabelsPredefined($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					unityTypes: function(optionsService) {
						return optionsService.getUnityTypes();
					},
					currentThinningPolymerDateTimeValue: function findThinningPolymerDateTimeValueOfDate(thinningPolymerDateTimeValueService) {
						var date = new Date();
						var dateString = date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear();
 						return thinningPolymerDateTimeValueService.findThinningPolymerDateTimeValueOfDate(dateString);
					},
					titles: function getAllTitles(optionsService) {
						return optionsService.getAllTitles();
					},
					provinceTypes: function getProvinceTypes(optionsService) {
						return optionsService.getProvinceTypes();
					},
					contactTypes: function(optionsService) {
						return optionsService.getContactTypes();
					},
					countryTypes: function getCountryTypes(optionsService) {
						return optionsService.getCountryTypes();
					},
					roles: function(optionsService) {
						return optionsService.getRoles();
					},
					usersOfProject: function findProjectUserConnectionByProject(userService) {
						return userService.findAllUsers();
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    
    angular
    	.module('legalprojectmanagement.search')
    	.controller('SearchController', SearchController);
    
    SearchController.$inject = ['$scope', '$timeout', 'searchService', 'currentUser', 'searchHistory', 'labelsPredefined', 'userAuthorizationUserContainer', 'unityTypes', 'currentThinningPolymerDateTimeValue', 'titles', 'provinceTypes', 'contactTypes', 'countryTypes', 'roles', 'usersOfProject', 'dateUtilityService', 'projectTemplateService', 'calendarEventUserConnectionService', 'userOfCalendarEventModalService', 'projectsService', 'projectUserConnectionService', 'projectsAssignedToContactModalService', 'printingPDFService', 'documentInformationModalService', 'documentFileService', 'receipeService', 'createReceipeModalService', 'editReceipeModalService', 'historyOfReceipeModalService', 'editReceipeToMixtureModalService', 'thinningService', 'createThinningModalService', 'editThinningModalService', 'utilService', 'userService', 'usersAssignedToContactModalService', 'editContactModalService', 'contactsService', 'projectsOfContactModalService', 'activityService', 'contactActivityModalService', 'facilityDetailsService', 'facilityDetailsOfContactModalService', 'optionsService', 'productsService', 'yesNoContactModalService'];
    
    function SearchController($scope, $timeout, searchService, currentUser, searchHistory, labelsPredefined, userAuthorizationUserContainer, unityTypes, currentThinningPolymerDateTimeValue, titles, provinceTypes, contactTypes, countryTypes, roles, usersOfProject, dateUtilityService, projectTemplateService, calendarEventUserConnectionService, userOfCalendarEventModalService, projectsService, projectUserConnectionService, projectsAssignedToContactModalService, printingPDFService, documentInformationModalService, documentFileService, receipeService, createReceipeModalService, editReceipeModalService, historyOfReceipeModalService, editReceipeToMixtureModalService, thinningService, createThinningModalService, editThinningModalService, utilService, userService, usersAssignedToContactModalService, editContactModalService, contactsService, projectsOfContactModalService, activityService, contactActivityModalService, facilityDetailsService, facilityDetailsOfContactModalService, optionsService, productsService, yesNoContactModalService) {
	    $scope.vm = this; 
    	var vm = this; 
    	
    	vm.currentUser = currentUser;
    	vm.titles = titles.data;
    	vm.provinceTypes = provinceTypes.data;
    	vm.contactTypes = contactTypes.data;
    	vm.countryTypes = countryTypes.data;
    	vm.roles = roles.data;
    	vm.usersOfProject = usersOfProject.data;
    	vm.searchHistory = searchHistory.data;
    	vm.labelsPredefined = labelsPredefined.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.unityTypes = unityTypes.data;
    	vm.categories = [];
    	vm.searchString = null;
    	vm.projects = false;
    	vm.products = false;
    	vm.files = false;
    	vm.calendar = false;
    	vm.contacts = false;
    	vm.communication = false;
    	vm.timeRegistration = false;
    	vm.searchResults = null;
    	vm.lastSearchResultLength = 0;
    	vm.loadingSearchResults = false;
    	vm.noLastReceipeExists = false;
    	vm.labelOrder = 'name';
    	vm.activityTypes = null;
    	vm.facilityDetailsTypes = null;
    	vm.polymerPromille = currentThinningPolymerDateTimeValue.data.value;
    	
    	vm.search = search;
    	vm.removeSearchString = removeSearchString;
    	vm.printContainer = printContainer;
    	vm.updateContact = updateContact;
    	vm.categorieChanged = categorieChanged;
    	vm.createDocumentUrl = createDocumentUrl;
    	vm.createContactDocumentUrl = createContactDocumentUrl;
    	vm.showTemplateModal = showTemplateModal;
    	vm.showContactTemplateModal = showContactTemplateModal;
    	vm.showEditContactModal = showEditContactModal;
    	vm.showProductsOfContact = showProductsOfContact;
    	vm.showContactActivityModal = showContactActivityModal;
    	vm.showFacilityDetailsModal = showFacilityDetailsModal;
    	vm.showDeleteContactModal = showDeleteContactModal;
    	vm.deleteContact = deleteContact;
    	vm.searchForLabel = searchForLabel;
    	vm.showUserOfCalendarEventModal = showUserOfCalendarEventModal;
    	vm.showUsersAssignedToContactModal = showUsersAssignedToContactModal;
    	vm.showProjectsAssignedToContactModal = showProjectsAssignedToContactModal;
    	vm.formatStartEndDateTime = formatStartEndDateTime;
    	vm.loadHistorySearch = loadHistorySearch;
    	vm.showDocumentInformationModal = showDocumentInformationModal;
    	vm.updateDocumentFile = updateDocumentFile;
    	vm.createReceipe = createReceipe;
    	vm.editAndDeleteReceipe = editAndDeleteReceipe;
    	vm.historyOfReceipe = historyOfReceipe;
    	vm.editReceipeToMixture = editReceipeToMixture;
    	vm.editAndDeleteMixture = editAndDeleteMixture;
    	vm.createThinning = createThinning;
    	vm.editAndDeleteThinning = editAndDeleteThinning;
    	vm.purgeSearchString = purgeSearchString;
    	loadActivityTypes();
    	loadFacilityDetailsTypes();
    	
    	////////////
    	
    	function search() {
    		vm.loadingSearchResults = true;
			loadSearchResult();
    	}
    	
    	function setTimeout() {
      		$timeout(function() {
      			vm.noLastReceipeExists = false;
  			   }, 2000); 
      	}
    	
    	function printContainer(container, name) { 
    		if(vm.searchResults.length > 0) {
    			printingPDFService.printSchedule(container, name);
    		}
    	}
    	
    	function loadHistorySearch(searchString) {
    		vm.searchString = searchString;
    		loadSearchResult();
    	}
    	
    	function searchForLabel(label) {
    		if(vm.searchString != null) {
    			vm.searchString = vm.searchString + ' ' + label;
    		} else {
    			vm.searchString = label;
    		}
    	}
    	
    	function removeSearchString() {
    		vm.searchString = null;
    		vm.searchResults = [];
    	}
    	
    	function loadSearchResult() {
    		if(vm.searchString != null && vm.searchString.length >= 3) {
    			var searchTerm = utilService.prepareSearchTerm(vm.searchString);
	    		searchService.search(searchTerm, vm.categories, vm.lastSearchResultLength).then(function successCallback(response) {
					vm.searchResults = prepareLoadedSearchResults(response.data);
					vm.lastSearchResultLength = vm.searchResults.length;
					vm.loadingSearchResults = false;
	  	   		 }, function errorCallback(response) {
	  	   			  console.log('error search.controller#loadSearchResult');
	  	   		 });
    		} else {
				vm.searchResults = null;
				vm.lastSearchResultLength = 0;
				vm.loadingSearchResults = false;
			}
    	}
    	
    	function prepareLoadedSearchResults(searchResults) {
    		for(var i = 0; i < searchResults.length; i++) {
    			if(searchResults[i].documentFile != null) {
    				searchResults[i].documentFile.documentFileVersion = searchResults[i].documentFile.documentFileVersions[searchResults[i].documentFile.documentFileVersions.length-1];
        		}
    		}
    		return searchResults;
    	}
    	
    	function categorieChanged(categorie, state) {
    		if(state === true) {
    			vm.categories.push(categorie);
    		} else if(state === false) {
    			for(var i = 0; i < vm.categories.length; i++) {
    				if(vm.categories[i] == categorie) {
    					vm.categories.splice(i, 1);
    				}
    			}
    		}
    		loadSearchResult();
    	}
    	
    	function showEditContactModal(contact) {
    		editContactModalService.showEditContactModal(contact, vm.titles, vm.provinceTypes, vm.contactTypes, vm.countryTypes, vm, vm.currentUser, vm.roles, vm.usersOfProject);
    	}
    	
    	function showUserOfCalendarEventModal(calendarEvent) {
    		calendarEventUserConnectionService.findCalendarEventUserConnectionsByCalendarEvent(calendarEvent.id).then(function (response) {
	    		 var calendarEventUserConnections = response.data;
	    		 userOfCalendarEventModalService.showUserOfCalendarEventModal(calendarEvent, calendarEventUserConnections, vm);
	    	  }).catch(function (data) {
				console.log('error calendar.search.controller#showUserOfCalendarEventModal: ' + data);
	    	  });
    	}
    	
    	function showProjectsAssignedToContactModal(contact) {
    		projectsService.findProjectsOfUser(currentUser.id).then(function (projectsResponse) {
    			var projects = projectsResponse.data;
    			projectUserConnectionService.findProjectUserConnectionsByContactAndProject(contact.id).then(function (projectUserConnectionResponse) {
        			var projectUserConnections = projectUserConnectionResponse.data;
        			projectsAssignedToContactModalService.showProjectsAssignedToContactModal(contact, projects, projectUserConnections);
    			}).catch(function (data) {
    				console.log('error in search.controller.js#findProjectsOfUser: ' + data);
    			});
			}).catch(function (data) {
				console.log('error in search.controller.js#showProjectsAssignedToContactModal: ' + data);
			});
    	}
    	
    	function showUsersAssignedToContactModal(contact) {
    		userService.findAllUsers().then(function(projectsResponse) {
    			var users = projectsResponse.data;
    			projectUserConnectionService.findProjectUserConnectionsByContactAndUser(contact.id).then(function (projectUserConnectionResponse) {
        			var projectUserConnections = projectUserConnectionResponse.data;
        			usersAssignedToContactModalService.showUsersAssignedToContactModal(contact, users, projectUserConnections);
    			}).catch(function (data) {
    				console.log('error in contacts.controller.js#findProjectsOfUser: ' + data);
    			});
			}).catch(function (data) {
				console.log('error in contacts.controller.js#showProjectsAssignedToContactModal: ' + data);
			});
    	}
    	
    	function formatStartEndDateTime(dateTimeFrom, dateTimeUntil) {
    		return dateUtilityService.formatStartEndDateTime(dateTimeFrom, dateTimeUntil);
    	}
    	
    	function showDocumentInformationModal(documentFile) {
    		documentInformationModalService.showDocumentInformationModal(documentFile, currentUser, vm.userAuthorizationUserContainer, null, vm, false);
    	}
    	
    	function createDocumentUrl(user, projectId, documentFile, documentFileVersion) {
    		if(documentFile == null || documentFileVersion == null) {
    			return '';
    		}
    		var version = documentFileVersion === undefined ? '_Version1' : documentFileVersion.version;
    		return 'api/filedownloads/filedownload/' + user.id + '/' + documentFile.id + '/' + version + '/';
    	}
    	
    	function showTemplateModal(url, documentFile, documentFileVersion) {
    		projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
    	}
    	
    	function createContactDocumentUrl(user, product, receipeAttachment) {		    	
    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
    		return 'api/filedownloads/filedownload/' + currentUser.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
    	}
    	
    	function showContactTemplateModal(url, receipeAttachment) {
    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
    			var documentFile = response.data;
    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
  	   		 }, function errorCallback(response) {
  	   			 console.log('error search.controller#showTemplateModal'); 
  	   		 });
    	}
    	
    	function updateDocumentFile(documentFile, updateDocumentFileResponsibility) {
    		documentFileService.updateDocumentFile(documentFile, documentFile.projectProductId).then(function(response) {
    			console.log('updated documentFile in search.controller#updateDocumentFile');
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#updateDocumentFile');
  	   		 });
    	} 
    	
    	function updateContact(contact) {
    		contact.importConfirmed = true;
    		contactsService.updateContact(contact).then(function(response) {
			}).catch(function (data) {
				console.log('error in search.controller.js#updateContact: ' + data);
			});
    	}
    	
    	function showProductsOfContact(contact) {
    		contactsService.findContactById(contact.id).then(function(response) {
    			var contactWithProducts = response.data;
    			projectsOfContactModalService.showProductsOfContactModal(contactWithProducts, vm.currentUser, vm.userAuthorizationUserContainer);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#showProductsOfContact');
  	   		 });
    	}
    	
    	function showContactActivityModal(contact) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		activityService.getActivitiesOfContactIdInRange(contact.id, startDateString, endDateString).then(function(response) {
    			var activities = response.data;
    			contactActivityModalService.showContactActivityModal(contact, activities, start, end, vm.activityTypes, vm.userAuthorizationUserContainer);
    		}, function errorCallback(response) {
	   			  console.log('error search.controller.js#showContactActivityModal');
	   		 });
    	}
    	
    	function showFacilityDetailsModal(contact) {  
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		facilityDetailsService.getFacilityDetailsOfContactIdInRange(contact.id, startDateString, endDateString).then(function(response) {
    			var facilityDetails = response.data;
    			facilityDetailsOfContactModalService.showFacilityDetailsOfContactModal(contact, facilityDetails, start, end, vm.activityTypes, vm.facilityDetailsTypes, vm.userAuthorizationUserContainer);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#showFacilityDetailsModal');
  	   		 });
    	}
    	
    	function showDeleteContactModal(contact) {
    		productsService.getProductsReferencedToContact(contact.id).then(function(response) {
    			var productsReferencedToContact = response.data;
    			calendarEventUserConnectionService.findCalendarEventUserConnectionsByContact(contact.id).then(function(response) {
    				var calendarEventUserConnections = response.data;
    				yesNoContactModalService.showYesNoModal(vm, contact, productsReferencedToContact, calendarEventUserConnections);
    			 }, function errorCallback(response) {
    	   			  console.log('error search.controller.js#showDeleteContactModal#findCalendarEventUserConnectionsByContact');
     	   		 });
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#showDeleteContactModal#showDeleteContactModal');
  	   		 });
    	}
    	
    	function deleteContact(contact) {
    		contactsService.deleteContact(contact.id).then(function (response) {
    			for(var i = 0; i < vm.searchResults.length; i++) {
    				if(vm.searchResults[i].contact != null && vm.searchResults[i].contact.id == contact.id) {
    					vm.searchResults.splice(i, 1);
    					break;
    				}
    			}
			}).catch(function (data) {
				console.log('error in search.controller.js#deleteContact: ' + data);
			});
    	}
    	
    	function loadActivityTypes() {     
    		optionsService.getActivityTypes('all').then(function(response) {
    			vm.activityTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#loadActivityTypes');
  	   		 });
    	}
    	
    	function loadFacilityDetailsTypes() {
    		optionsService.getFacilityDetailsTypes().then(function(response) {
    			vm.facilityDetailsTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#loadFacilityDetailsTypes');
  	   		 });
    	}
    	
    	function createReceipe(product, receipeType) {
    		var receipe = {
	    		productId: '',
	    		date: new Date(),
	    		receipeProducts: [],
	    		information: '',
	    		receipeAttachments: [],
	    		receipeType: null
	    	};
    		receipeService.findAllReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var allReceipesOfProduct = response.data;
    			createReceipeModalService.showCreateReceipeModal(vm.currentUser, product, null, receipe, allReceipesOfProduct, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#createReceipe');
 	   		 });
    	}
    	
    	function editAndDeleteReceipe(product, receipeType) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
    			var receipesOfProduct = response.data;
    			editReceipeModalService.showEditReceipeModal(vm.currentUser, product, null, receipesOfProduct, receipeType, start, end, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#editAndDeleteReceipe');
 	   		 });
    	}
    	
    	function editReceipeToMixture(product, receipeType, $event) {
    		// get last standard receipe of product
    		$event.stopPropagation();
    		receipeService.findLastReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var lastReceipeOfProduct = response.data;
    			if(lastReceipeOfProduct == '') {
    				vm.noLastReceipeExists = true;
    				setTimeout();
    			} else {
    				editReceipeToMixtureModalService.showEditReceipeToMixtureModal(vm.currentUser, product, null, lastReceipeOfProduct, vm.userAuthorizationUserContainer, vm.unityTypes);
    			}
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#editReceipeToMixture');
 	   		 });
    	}
    	
    	function editAndDeleteMixture(product, receipeType, $event) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
    			var receipesOfProduct = response.data;
    			editReceipeModalService.showEditReceipeModal(vm.currentUser, product, null, receipesOfProduct, receipeType, start, end, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#editAndDeleteMixture');
 	   		 });
    	}
    	
    	function historyOfReceipe(product, receipeType) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
    			var receipesOfProduct = response.data;
    			historyOfReceipeModalService.showHistoryOfReceipeModal(vm.currentUser, receipeType, product, receipesOfProduct, start, end);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#historyOfReceipe');
 	   		 });
    	}
    	
    	function createThinning(product, $event) { 
    		var thinning = {
            		productId: '',
            		date: new Date(),
            		ironContent: null,
            		targetContent: null,
            		density: null,
            		amountLiter: null,
            		amount: null,
            		iron: null,
            		water: null,
            		polymer: false,
            		ascorbinSaeure: false,
            		polymerSubtract: 1000,
            		ascorbinSaeureSubtract: 2700,
            		waterAmountCalculated: null
            	};
    		thinningService.findAllThinningsOfProduct(product.id).then(function(response) {
    			var allThinningsOfProduct = response.data;
    			createThinningModalService.showCreateThinningModal(vm.currentUser, product, null, thinning, allThinningsOfProduct, vm.userAuthorizationUserContainer, vm.polymerPromille);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#createReceipe');
 	   		 });
    	}
    	
    	function editAndDeleteThinning(product) { 
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		thinningService.findThinningsOfProductInRange(product.id, startDateString, endDateString).then(function(response) {
    			var thinningsOfProduct = response.data;
    			editThinningModalService.showEditThinningModal(vm.currentUser, product, null, thinningsOfProduct, start, end, vm.userAuthorizationUserContainer, vm.polymerPromille);
    		}, function errorCallback(response) {
	   			 console.log('error search.controller#editAndDeleteThinning');
	   		 });
    	}
    	
    	function purgeSearchString(searchString) {
    		if(searchString.indexOf('*') > -1) {
    			searchString = searchString.substr(0, searchString.length-1);
    		}
    		return searchString;
    	}
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.superadministration')
    	.factory('historyAmountService', historyAmountService);
        
    historyAmountService.$inject = ['$http', 'api_config'];
    
    function historyAmountService($http, api_config) {
		var service = {
			getHistoryAmountsBetweenDates: getHistoryAmountsBetweenDates
		};
		
		return service;
		
		////////////
		
		function getHistoryAmountsBetweenDates(start, end) {
			return $http.get(api_config.BASE_URL + '/historyamounts/historyamount/' + start + '/' + end + '/');
		}
    }
})();



(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.superadministration')
    	.factory('superAdministrationService', superAdministrationService);
        
    superAdministrationService.$inject = ['$http', 'api_config'];
    
    function superAdministrationService($http, api_config) {
		var service = {
			findAllTenants: findAllTenants,
			createTenant: createTenant,
			deleteTenant: deleteTenant
		};
		
		return service;
		
		////////////
		
		function findAllTenants() {
			return $http.get(api_config.BASE_URL + '/tenants/tenant/all');
		}
		
		function createTenant(tenant) {
			return $http.post(api_config.BASE_URL + '/tenants/tenant', tenant);
		}
		
		function deleteTenant(id) {
			return $http.delete(api_config.BASE_URL + '/tenants/tenant/' + id);
		}
    }
})();



(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.superadministration')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getSuperAdministrationState());
    	 
    	////////////
			    	
    	function getSuperAdministrationState() {
    		var state = {
    			name: 'auth.superadministration',
				url: '/superadministration/:userId',
				templateUrl: 'app/superadministration/superadministration/superadministration.html',
				controller: 'SuperAdministrationController',
				controllerAs: 'vm',
				resolve: {
					superAdministrationService: 'superAdministrationService',
					thinningPolymerDateTimeValueService: 'thinningPolymerDateTimeValueService',
					tenants: function findAllTenants(superAdministrationService) {
						return superAdministrationService.findAllTenants();
					}, 
					thinningPolymerDateTimeValues: function getAll(thinningPolymerDateTimeValueService) {
						return thinningPolymerDateTimeValueService.findAll();
					}, 
					currentThinningPolymerDateTimeValue: function findThinningPolymerDateTimeValueOfDate(thinningPolymerDateTimeValueService) {
						var date = new Date();
						var dateString = date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear();
 						return thinningPolymerDateTimeValueService.findThinningPolymerDateTimeValueOfDate(dateString);
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    
    angular
    	.module('legalprojectmanagement.superadministration')
    	.controller('SuperAdministrationController', SuperAdministrationController);
     
    SuperAdministrationController.$inject = ['$scope', '$timeout', 'currentUser', 'tenants', 'thinningPolymerDateTimeValues', 'currentThinningPolymerDateTimeValue', 'superAdministrationService', 'userService', 'thinningPolymerDateTimeValueService', 'dateUtilityService', 'historyAmountService'];
    
    function SuperAdministrationController($scope, $timeout, currentUser, tenants, thinningPolymerDateTimeValues, currentThinningPolymerDateTimeValue, superAdministrationService, userService, thinningPolymerDateTimeValueService, dateUtilityService, historyAmountService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	vm.tenants = tenants.data;
    	vm.thinningPolymerDateTimeValues = thinningPolymerDateTimeValues.data;
    	vm.currentThinningPolymerDateTimeValue = currentThinningPolymerDateTimeValue.data;
    	vm.tenantStillExistsError = false;
    	vm.tenantSelected = false;
    	vm.tenantUpdated = false;
    	vm.thinningPolymerDateTimeValue = {
    		id: null,
    		dateTime: new Date(),
    		value: 0
    	};
    	vm.historyAmounts = [];
    	vm.labels = [];
    	vm.series = [];
    	vm.historyAmountFromTrigger = false;
    	vm.historyAmountUntilTrigger = false;
    	vm.historyAmountFrom = dateUtilityService.oneMonthBack(new Date());
    	vm.historyAmountUntil = new Date();
    	vm.options = { legend: { display: true } };
    	
    	vm.addNewTenant = addNewTenant;
    	vm.deleteTenant = deleteTenant;
    	vm.setTenantForSuperAdmin = setTenantForSuperAdmin;
    	vm.createThinningPolymerDateTimeValue = createThinningPolymerDateTimeValue;
    	vm.updateThinningPolymerDateTimeValue = updateThinningPolymerDateTimeValue;
    	vm.deleteThinningPolymerDateTimeValue = deleteThinningPolymerDateTimeValue;
    	vm.getHistoryAmountsBetweenDates = getHistoryAmountsBetweenDates;
    	vm.openHistoryAmountFrom = openHistoryAmountFrom;
    	vm.openHistoryAmountUntil = openHistoryAmountUntil;
    	initializeTenantOfCurrentUser();
    	getHistoryAmountsBetweenDates();
    	
    	////////////

    	function setTenantCreatedTimeout() {
    		$timeout(function() {
				vm.createTenantClicked = false;
			   }, 2000); 
    	}
    	
    	function setTenantSelectedTimeout(tenant) {
    		$timeout(function() {
    			vm.tenantUpdated = false;
			   }, 2000); 
    	}
    	
    	function openHistoryAmountFrom($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.historyAmountFromTrigger = true;
		}
    	
    	function openHistoryAmountUntil($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.historyAmountUntilTrigger = true;
		}
    	
    	function addNewTenant(newTenant) {
    		vm.tenantStillExistsError = false;
    		if(tenantStillExists(newTenant)) {
    			return;
    		}
    		var tenant = {};
    		tenant.name = newTenant;
    		superAdministrationService.createTenant(tenant).then(function successCallback(response) {
    			vm.tenants.push(response.data);
    			vm.newTenant = null;
    			vm.createTenantClicked = true;
    			setTenantCreatedTimeout();
  	   		 }, function errorCallback(response) {
  	   			  console.log('error superadministration.controller#addNewTenant');
  	   		 });
    	}
    	
    	function tenantStillExists(newTenant) {
    		vm.tenantStillExistsError = false;
    		for(var i = 0; i < vm.tenants.length; i++) {
    			if(vm.tenants[i].name == newTenant) {
    				vm.tenants[i].stillExists = true;
    				vm.tenantStillExistsError = true;
    			} else {
    				vm.tenants[i].stillExists = false;
    			}
    		}
    		return vm.tenantStillExistsError;
    	}
    	
    	function deleteTenant(tenant) {
    		superAdministrationService.deleteTenant(tenant.id).then(function successCallback(response) {
    			removeTenant(tenant);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error superadministration.controller#deleteTenant');
  	   		 });
    	}
    	
    	function removeTenant(tenant) {
    		for(var i = 0; i < vm.tenants.length; i++) {
    			if(vm.tenants[i].id == tenant.id) {
    				vm.tenants.splice(i, 1);
    			}
    		}
    	}
    	
    	function initializeTenantOfCurrentUser() {
    		if(currentUser.tenant != null) {
    			for(var i = 0; i < vm.tenants.length; i++) {
        			if(vm.tenants[i].id != currentUser.tenant.id) {
        				vm.tenants[i].selected = true;
        			}
        		}
    		}
    	}
    	
    	function setTenantForSuperAdmin(tenant) {
    		for(var i = 0; i < vm.tenants.length; i++) {
    			if(vm.tenants[i].id != tenant.id) {
    				vm.tenants[i].selected = false;
    			}
    		}
    		if(tenant.selected) {
    			currentUser.tenant = tenant;
    		} else if(!tenant.selected) {
    			currentUser.tenant = null;
    		}
    		userService.updateUser(currentUser).then(function successCallback(response) {
    			vm.tenantUpdated = true;
    			setTenantSelectedTimeout(currentUser.tenant);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error superadministration.controller#setTenantForSuperAdmin');
 	   		 });
    	}
    	
    	function createThinningPolymerDateTimeValue() {
    		thinningPolymerDateTimeValueService.create(vm.thinningPolymerDateTimeValue).then(function(response) {
    			thinningPolymerDateTimeValueService.findAll().then(function(response) {
    				vm.thinningPolymerDateTimeValues = response.data;
     	   		 }, function errorCallback(response) {
     	   			  console.log('error superadministration.controller#createThinningPolymerDateTimeValue');
     	   		 });
 	   		 }, function errorCallback(response) {
 	   			  console.log('error superadministration.controller#createThinningPolymerDateTimeValue');
 	   		 });
    	}
    	
    	function updateThinningPolymerDateTimeValue(thinningPolymerDateTimeValue) {
    		thinningPolymerDateTimeValueService.update(thinningPolymerDateTimeValue).then(function(response) {
	   		 }, function errorCallback(response) {
	   			  console.log('error superadministration.controller#updateThinningPolymerDateTimeValue');
	   		 });
    	}
    	
    	function deleteThinningPolymerDateTimeValue(thinningPolymerDateTimeValue) {
    		thinningPolymerDateTimeValueService.deleteThinningPolymerDateTimeValue(thinningPolymerDateTimeValue.id).then(function(response) {
    			for(var i = 0; i < vm.thinningPolymerDateTimeValues.length; i++) {
    				if(vm.thinningPolymerDateTimeValues[i].id === thinningPolymerDateTimeValue.id) {
    					vm.thinningPolymerDateTimeValues.splice(i, 1);
    					return;
    				}
    			}
 	   		 }, function errorCallback(response) {
 	   			  console.log('error superadministration.controller#deleteThinningPolymerDateTimeValue');
 	   		 });
    	}
    	
    	function getHistoryAmountsBetweenDates() {
    		vm.historyAmounts = [];
        	vm.labels = [];
        	vm.series = [];
    		var start = dateUtilityService.formatDateToString(vm.historyAmountFrom);
    		var end = dateUtilityService.formatDateToString(vm.historyAmountUntil);
    		historyAmountService.getHistoryAmountsBetweenDates(start, end).then(function(response) {
				var historyAmounts = response.data;
				
				var contacts = [];
				var activities = [];
				var facilityDetails = [];
				var documents = [];
				var products = [];
				var projects = [];
				var calendarEvents = [];
				var receipes = [];
				var mixtures = [];
				var thinnings = [];
				var communicationUserConnections = [];

				for(var i = 0; i < historyAmounts.length; i++) {
					vm.labels.push(historyAmounts[i].date);
					contacts.push(historyAmounts[i].contacts);
					activities.push(historyAmounts[i].activities);
					facilityDetails.push(historyAmounts[i].facilityDetails);
					documents.push(historyAmounts[i].documents);
					products.push(historyAmounts[i].products);
					projects.push(historyAmounts[i].projects);
					calendarEvents.push(historyAmounts[i].calendarEvents);
					receipes.push(historyAmounts[i].receipes);
					mixtures.push(historyAmounts[i].mixtures);
					thinnings.push(historyAmounts[i].thinnings);
					communicationUserConnections.push(historyAmounts[i].communicationUserConnections);
				}
				
				vm.series.push('Kontakt');
				vm.series.push('Tätigkeit');
				vm.series.push('Anlagendetails');
				vm.series.push('Dokumente');
				vm.series.push('Produkt');
				vm.series.push('Projekt');
				vm.series.push('Termine');
				vm.series.push('Rezepte');
				vm.series.push('Mischungen');
				vm.series.push('Verdünnungen');
				vm.series.push('Arbeitsaufträge');
				
				vm.historyAmounts.push(contacts);
				vm.historyAmounts.push(activities);
				vm.historyAmounts.push(facilityDetails);
				vm.historyAmounts.push(documents);
				vm.historyAmounts.push(products);
				vm.historyAmounts.push(projects);
				vm.historyAmounts.push(calendarEvents);
				vm.historyAmounts.push(receipes);
				vm.historyAmounts.push(mixtures);
				vm.historyAmounts.push(thinnings);
				vm.historyAmounts.push(communicationUserConnections);
				
	   		 }, function errorCallback(response) {
	   			  console.log('error superadministration.controller#getHistoryAmountsBetweenDates');
	   		 });
    	}
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.factory('timeregistrationService', timeregistrationService);
        
    timeregistrationService.$inject = ['$http', 'api_config'];
    
    function timeregistrationService($http, api_config) {
		var service = {
			findTimeRegistrations: findTimeRegistrations,
			getTimeRegistrationTimeRangeSum: getTimeRegistrationTimeRangeSum,
			createTimeRegistration: createTimeRegistration,
			updateTimeRegistration: updateTimeRegistration,
			deleteTimeRegistration: deleteTimeRegistration
		};
		
		return service;
		
		////////////
		
		function findTimeRegistrations(projectId, start, end) {
		return $http.get(api_config.BASE_URL + '/timeregistrations/timeregistration/' + projectId + '/' + start + '/' + end + '/');
		}
		
		function getTimeRegistrationTimeRangeSum(projectId, start, end) {
			return $http.get(api_config.BASE_URL + '/timeregistrations/timeregistration/' + projectId + '/' + start + '/' + end + '/timerangesum');
			}
		
		function createTimeRegistration(userId, timeRegistration) {
			return $http.post(api_config.BASE_URL + '/timeregistrations/timeregistration/' + userId, timeRegistration);
		}
		
		function updateTimeRegistration(userId, timeRegistration) {
			return $http.put(api_config.BASE_URL + '/timeregistrations/timeregistration/' + userId, timeRegistration);
		}
		
		function deleteTimeRegistration(id) {
			return $http.delete(api_config.BASE_URL + '/timeregistrations/timeregistration/' + id);
		}
    }
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.timeregistration')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getTimeregistrationState());
    	 
    	////////////
			    	
    	function getTimeregistrationState() {
    		var state = {
    			name: 'auth.timeregistration',
				url: '/timeregistration/:userId',
				templateUrl: 'app/timeregistration/timeregistration/timeregistration.html',
				controller: 'TimeregistrationController',
				controllerAs: 'vm',
				resolve: {
					projectsService: 'projectsService',
					projects: function findProjectsOfUser($stateParams, projectsService) {
						return projectsService.findProjectsOfUser($stateParams.userId);
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					}
				}
    		};
    		return state;
    	}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.directive('createOrUpdateTimeRegistrationDirective', createOrUpdateTimeRegistrationDirective);
    
    createOrUpdateTimeRegistrationDirective.$inject = ['$timeout', 'dateUtilityService'];
    
	function createOrUpdateTimeRegistrationDirective($timeout, dateUtilityService) {
		var directive = {
			restrict: 'E',
			scope: {
				type: '=',
				projects: '=',
				selectedProject: '=',
				timeRegistration: '=',
				invoker: '=',
				modalInvoker: '='
			},
			templateUrl: 'app/timeregistration/directives/createOrUpdateTimeRegistrationDirective/createOrUpdateTimeRegistration.html',
			link: function($scope) {
				$scope.startDate = convertDateTimeFromToDate();
				$scope.startTime = convertDateTimeFromToDate();
				$scope.endDate = convertDateTimeUntilToDate();
				$scope.endTime = convertDateTimeUntilToDate();
				$scope.startDatePicker = false;
				$scope.endDatePicker = false; 
				$scope.projectNotSetError = false;
		    	
				function convertDateTimeFromToDate() {
					if($scope.timeRegistration.dateTimeFrom == null) {
						return new Date();
					}
					if($scope.timeRegistration.dateTimeFrom instanceof Date) {
						return $scope.timeRegistration.dateTimeFrom;
					}
					return dateUtilityService.convertToDateTimeOrUndefined($scope.timeRegistration.dateTimeFrom);
				}
				
				function convertDateTimeUntilToDate() {
					if($scope.timeRegistration.dateTimeUntil == null) {
						return new Date();
					}
					if($scope.timeRegistration.dateTimeUntil instanceof Date) {
						return $scope.timeRegistration.dateTimeUntil;
					}
					return dateUtilityService.convertToDateTimeOrUndefined($scope.timeRegistration.dateTimeUntil);
				}
				
				$scope.openStartDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.startDatePicker = true;
		        };
		    	
				$scope.openEndDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.endDatePicker = true;
		        };
				
				$scope.addProject = function(project) {
		    		removeProjectSelection();
		    		project.selected = true;
		    		$scope.timeRegistration.project = project;
		    	};
				
		    	if($scope.selectedProject != null) {
		    		for(var i = 0; i < $scope.projects.length; i++) {
		    			if($scope.projects[i].id == $scope.selectedProject.id) {
		    				$scope.addProject($scope.projects[i]);
		    				break;
		    			}
		    		}
				}
		    	
		    	$scope.removeProject = function(project) {
		    		project.selected = false;
		    		$scope.timeRegistration.project = null;
		    	};
		    	
		    	function removeProjectSelection() {
		    		for(var i = 0; i < $scope.projects.length; i++) {
		    			$scope.projects[i].selected = false;
		    		}
		    	}
		    	
		    	$scope.createOrUpdateTimeRegistration = function() {
					if($scope.timeRegistration.project == null) {
						$scope.projectNotSetError = true;
						return;
					}
					$scope.projectNotSetError = false;
					if($scope.type == 'CREATE') {
						$scope.invoker.createTimeRegistration($scope.timeRegistration, $scope.startDate, $scope.startTime, $scope.endDate, $scope.endTime);
						removeProjectSelection();
					} else if($scope.type == 'EDIT') {
						$scope.invoker.updateTimeRegistration($scope.timeRegistration, $scope.startDate, $scope.startTime, $scope.endDate, $scope.endTime);
					}
					$scope.modalInvoker.closeCreateOrUpdateTimeRegistrationModal();
				};
				
			 }
		};
		return directive;
		
		////////////
	}
})();

(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.factory('createOrUpdateTimeRegistrationModalService', createOrUpdateTimeRegistrationModalService);
        
    createOrUpdateTimeRegistrationModalService.$inject = ['$modal', '$stateParams'];
    
    function createOrUpdateTimeRegistrationModalService($modal, $stateParams) {
		var service = {
			showCreateOrUpdateTimeRegistrationModal: showCreateOrUpdateTimeRegistrationModal
		};		
		return service;
		
		////////////
				
		function showCreateOrUpdateTimeRegistrationModal(type, projects, selectedProject, timeRegistration, invoker) {
			 var createOrUpdateTimeRegistrationModal = $modal.open({
				controller: CreateOrUpdateTimeRegistrationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	type: function() {
			    		return type;
			    	},
			    	projects: function() {
			    		return projects;
			    	},
			    	selectedProject: function() {
			    		return selectedProject;
			    	},
			    	timeRegistration: function() {
			    		return timeRegistration;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/timeregistration/modals/createOrUpdateTimeRegistration/createOrUpdateTimeRegistration.modal.html'
			});
			return createOrUpdateTimeRegistrationModal;
			
			function CreateOrUpdateTimeRegistrationModalController($modalInstance, $scope, type, projects, selectedProject, timeRegistration, invoker) {
		    	var vm = this; 
		    	vm.type = type;
		    	vm.projects = projects;
		    	vm.selectedProject = selectedProject;
		    	vm.timeRegistration = timeRegistration;
		    	vm.invoker = invoker;
		    	
		    	vm.closeCreateOrUpdateTimeRegistrationModal = closeCreateOrUpdateTimeRegistrationModal;
		    	
		    	////////////
		    	
		    	function closeCreateOrUpdateTimeRegistrationModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.factory('printTimeRegistrationModalService', printTimeRegistrationModalService);
        
    printTimeRegistrationModalService.$inject = ['$modal', '$stateParams'];
    
    function printTimeRegistrationModalService($modal, $stateParams) {
		var service = {
			showPrintTimeRegistrationModal: showPrintTimeRegistrationModal
		};		
		return service;
		
		////////////
				
		function showPrintTimeRegistrationModal(timeRegistrations) {
			 var printTimeRegistrationModal = $modal.open({
				controller: PrintTimeRegistrationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	timeRegistrations: function() {
			    		return timeRegistrations;
			    	}
			    }, 
				templateUrl: 'app/timeregistration/modals/printTimeRegistration/printTimeRegistration.modal.html'
			});
			return printTimeRegistrationModal;
			
			function PrintTimeRegistrationModalController($modalInstance, $scope, timeRegistrations) {
		    	var vm = this; 
		    	vm.timeRegistrations = timeRegistrations;
		    	vm.today = new Date();
		    	
		    	vm.closePrintTimeRegistrationModal = closePrintTimeRegistrationModal;
		    	
		    	////////////
		    	
		    	function closePrintTimeRegistrationModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.factory('yesNoTimeRegistrationModalService', yesNoTimeRegistrationModalService);
        
    yesNoTimeRegistrationModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoTimeRegistrationModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, timeRegistration) {
			 var yesNoModal = $modal.open({
				controller: YesNoTimeRegistrationModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	timeRegistration: function() {
			    		return timeRegistration;
			    	}
			    }, 
				templateUrl: 'app/timeregistration/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoTimeRegistrationModalController($modalInstance, $scope, invoker, timeRegistration) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.timeRegistration = timeRegistration;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.deleteTimeRegistration(vm.timeRegistration);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
(function() {
    'use strict';
    
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.controller('TimeregistrationController', TimeregistrationController);
    
    TimeregistrationController.$inject = ['$scope', '$timeout', 'moment', 'currentUser', 'projects', 'defaultProject', 'timeregistrationService', 'dateUtilityService', 'createOrUpdateTimeRegistrationModalService', 'yesNoTimeRegistrationModalService', 'printTimeRegistrationModalService', 'printingPDFService'];
    
    function TimeregistrationController($scope, $timeout, moment, currentUser, projects, defaultProject, timeregistrationService, dateUtilityService, createOrUpdateTimeRegistrationModalService, yesNoTimeRegistrationModalService, printTimeRegistrationModalService, printingPDFService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	vm.currentUser = currentUser;
    	vm.defaultProject = defaultProject.data;
    	vm.projects = projects.data;
    	vm.selectedProject = vm.projects.length == 1 ? vm.projects[0] : (vm.defaultProject != null ? vm.defaultProject : null);
    	vm.timeRegistration = setTimeRegistrationObject();
    	vm.todayDate = new Date(); 
    	vm.filterStartDate = moment();
    	vm.filterEndDate = moment().toDate();
    	vm.filterStartDate = vm.filterStartDate.add(-1, 'month').toDate();
    	vm.timeRegistrations = null;
    	vm.filterStartDatePicker = false;
    	vm.filterEndDatePicker = false;
    	vm.timeRegistrationTimeRangeSum = null;
    	
    	vm.printContainer = printContainer;
    	vm.findTimeRegistrationsInTimeRange = findTimeRegistrationsInTimeRange;
    	vm.getTimeRegistrationTimeRangeSum = getTimeRegistrationTimeRangeSum;
    	vm.createTimeRegistration = createTimeRegistration;
    	vm.deleteTimeRegistration = deleteTimeRegistration;
    	vm.addSelectedProject = addSelectedProject;
    	vm.updateTimeRegistration = updateTimeRegistration;
    	vm.removeSelectedProject = removeSelectedProject;
    	vm.showCreateOrUpdateTimeRegistrationModal = showCreateOrUpdateTimeRegistrationModal;
    	vm.showDeleteTimeRegistrationYesNoModal = showDeleteTimeRegistrationYesNoModal;
    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
    	vm.showPrintTimeRegistrations = showPrintTimeRegistrations;
    	vm.formatStartEndDateTime = formatStartEndDateTime;
    	findTimeRegistrationsInTimeRange();
    	
    	////////////
    	
    	function openFilterStartDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterStartDatePicker = true;
        }
    	
    	function openFilterEndDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterEndDatePicker = true;
        }
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function setTimeRegistrationObject() {
    		var timeRegistration = {
    	    		id: null,
    	    		dateTimeFrom: new Date(),
    	    		dateTimeUntil: new Date(),
    	    		user: vm.currentUser,
    	    		text: '',
    	    		project: vm.selectedProject,
    	    		tenant: null
    	    	};
    		return timeRegistration;
    	}
    	
    	function addSelectedProject(project) {
    		removeAllProjectSelections(vm.projects);
    		project.selected = true;
    		vm.selectedProject = project;
    	}
    	
    	function removeSelectedProject(project) {
    		vm.selectedProject = null;
    		removeAllProjectSelections(vm.projects);
    	}

    	function removeAllProjectSelections(projects) {
    		for(var i = 0; i < projects.length; i++) {
    			projects[i].selected = false;
    		}
    	}
    	
    	function findTimeRegistrationsInTimeRange() {
    		if(vm.selectedProject == null) {
    			return;
    		}
    		var start = dateUtilityService.formatDateToString(vm.filterStartDate);
    		var end = dateUtilityService.formatDateToString(vm.filterEndDate);    
    		timeregistrationService.findTimeRegistrations(vm.selectedProject.id, start, end).then(function (response) {
    			vm.timeRegistrations = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#findTimeRegistrationsInTimeRange: ' + data);
			});
    		getTimeRegistrationTimeRangeSum();
    	}
    	
    	function getTimeRegistrationTimeRangeSum() {
    		var start = dateUtilityService.formatDateToString(vm.filterStartDate);
    		var end = dateUtilityService.formatDateToString(vm.filterEndDate);  
    		timeregistrationService.getTimeRegistrationTimeRangeSum(vm.selectedProject.id, start, end).then(function (response) {
    			vm.timeRegistrationTimeRangeSum = response.data;
    		}).catch(function (data) {
				console.log('error communication.controller#getTimeRegistrationTimeRangeSum: ' + data);
			});
    	}
    	
    	function createTimeRegistration(timeRegistration, startDate, startTime, endDate, endTime) {
    		timeRegistration.dateTimeFrom = dateUtilityService.joinDateObjectsToDateTimeObject(startDate, startTime);
    		timeRegistration.dateTimeUntil = dateUtilityService.joinDateObjectsToDateTimeObject(endDate, endTime);
    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
    		timeregistrationService.createTimeRegistration(vm.currentUser.id, timeRegistration).then(function (response) {
    			if(vm.timeRegistrations != null) {
    				vm.timeRegistrations.push(response.data);
    				getTimeRegistrationTimeRangeSum();
    			}
    			vm.timeRegistration = setTimeRegistrationObject();
    		}).catch(function (data) {
				console.log('error communication.controller#findTimeRegistrationsInTimeRange: ' + data);
			});
    	}
    	
    	function updateTimeRegistration(timeRegistration, startDate, startTime, endDate, endTime) {
    		timeRegistration.dateTimeFrom = dateUtilityService.joinDateObjectsToDateTimeObject(startDate, startTime);
    		timeRegistration.dateTimeUntil = dateUtilityService.joinDateObjectsToDateTimeObject(endDate, endTime);
    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
    		timeregistrationService.updateTimeRegistration(vm.currentUser.id, timeRegistration).then(function (response) {
    			vm.timeRegistration = setTimeRegistrationObject();
    			for(var i = 0; i < vm.timeRegistrations.length; i++) {
    				if(vm.timeRegistrations[i].id == timeRegistration.id) {
    					vm.timeRegistrations[i] = response.data;
    					break;
    				}
    			}
    			getTimeRegistrationTimeRangeSum();
    		}).catch(function (data) {
				console.log('error communication.controller#updateTimeRegistration: ' + data);
			});
    	}
    	
    	function deleteTimeRegistration(timeRegistration) {
    		timeregistrationService.deleteTimeRegistration(timeRegistration.id).then(function (response) {
    			for(var i = 0; i < vm.timeRegistrations.length; i++) {
    				if(vm.timeRegistrations[i].id == timeRegistration.id) {
    					vm.timeRegistrations.splice(i, 1);
    					break;
    				}
    			}
    			getTimeRegistrationTimeRangeSum();
    		}).catch(function (data) {
				console.log('error communication.controller#deleteTimeRegistration: ' + data);
			});
    	}
    	
    	function showCreateOrUpdateTimeRegistrationModal(type, timeRegistration) {
    		createOrUpdateTimeRegistrationModalService.showCreateOrUpdateTimeRegistrationModal(type, vm.projects, vm.selectedProject, timeRegistration, vm);
    	}
    	
    	function showDeleteTimeRegistrationYesNoModal(timeRegistration) {
    		yesNoTimeRegistrationModalService.showYesNoModal(vm, timeRegistration);
    	}
    	
    	function showPrintTimeRegistrations() {
    		printTimeRegistrationModalService.showPrintTimeRegistrationModal(vm.timeRegistrations);
    	}
    	
    	function formatStartEndDateTime(start, end) {
    		return dateUtilityService.formatStartEndDateTime(start, end);
    	}
	} 
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.factory('userService', userService);
        
    userService.$inject = ['$http', 'authTokenService', 'authService', 'authUserService', 'api_config', 'token_config'];
    
    function userService($http, authTokenService, authService, authUserService, api_config, token_config) {
		var service = {
			logout: logout,
			findAllUsers: findAllUsers,
			updateUser: updateUser,
			deleteUser: deleteUser, 
			updatePassword: updatePassword,
			getUserById: getUserById,
			getHighestRoleOfUser: getHighestRoleOfUser,
			findUsersByTerm: findUsersByTerm,
			canUserSeeConfidentialDocumentFiles: canUserSeeConfidentialDocumentFiles,
			getUserAuthorizationUserContainerOfUser: getUserAuthorizationUserContainerOfUser,
			createUserAuthorizationUserContainer: createUserAuthorizationUserContainer,
			updateUserAuthorizationUserContainerOfUser: updateUserAuthorizationUserContainerOfUser,
			deleteUserAuthorizationUserContainerOfUser: deleteUserAuthorizationUserContainerOfUser
		};
		
		return service;
		
		////////////
		
		function logout() {
			authUserService.setCurrentUser(false);
			authTokenService.deleteToken();
		}
		
		function updateUser(updateUser) {
			return $http.put(api_config.BASE_URL + '/users/' + updateUser.id, updateUser);
		}
		
		function deleteUser(user) {
			return $http.delete(api_config.BASE_URL + '/users/' + user.id);
		}
		
		function updatePassword(userId, password) {
			return $http.put(api_config.BASE_URL + '/users/' + userId + '/password', password);
		}
		
		function findAllUsers() {
			return $http.get(api_config.BASE_URL + '/users');
		}
		
		function getUserById(userId) {
			return $http.get(api_config.BASE_URL + '/users/' + userId);
		}
		
		function getHighestRoleOfUser() {
			return $http.get(api_config.BASE_URL + '/users/highestrole').then(function successCallback(response) {
    			return response.data;
    		}, function errorCallback(response) {
    			console.log('error in user.service.js#getHighestRoleOfUser');
    		});
		}
		
		function findUsersByTerm(term) {
			return $http.get(api_config.BASE_URL + '/users/' + term + '/search');
		}
		
		function canUserSeeConfidentialDocumentFiles(userId) {
			return $http.get(api_config.BASE_URL + '/user/confidentialdocumentfiles/' + userId);
		}
		
		function getUserAuthorizationUserContainerOfUser(userId) {
			return $http.get(api_config.BASE_URL + '/users/userauthorizationusercontainer/' + userId);
		}
		
		function createUserAuthorizationUserContainer(userId, projectUserConnectionRole) {
			return $http.post(api_config.BASE_URL + '/users/userauthorizationusercontainer/' + userId + '/' + projectUserConnectionRole);
		}
		
		function updateUserAuthorizationUserContainerOfUser(userAuthorizationUserContainer) {
			return $http.put(api_config.BASE_URL + '/users/userauthorizationusercontainer', userAuthorizationUserContainer);
		}
		
		function deleteUserAuthorizationUserContainerOfUser(userId, id) {
			return $http.delete(api_config.BASE_URL + '/users/userauthorizationusercontainer/' + userId + '/' + id);
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.factory('userDuplicationService', userDuplicationService);
        
    userDuplicationService.$inject = ['$http', 'api_config'];
    
    function userDuplicationService($http, api_config) {
		var service = {
			duplicateUser: duplicateUser
		};
		
		return service;
		
		////////////
		
		function duplicateUser(userIdFrom, userIdTo) {
			return $http.post(api_config.BASE_URL + '/userduplications/userduplication/' + userIdFrom + '/' + userIdTo);
		}
    }
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.config(configure);
    			
    configure.$inject = ['$httpProvider', 'authTokenServiceProvider', 'token_config'];
    
	function configure($httpProvider, authTokenServiceProvider, token_config) {
		$httpProvider.interceptors.push(userInterceptor);
		
		////////////
		
		function userInterceptor($q) {
			var interceptor = {
					request: request,
					response: response,
					responseError: responseError
			};
			
			return interceptor;
			
			////////////
			
			function request(config) {
				var token = authTokenServiceProvider.$get().getToken();
				if (token) {
					config.headers[token_config.AUTH_TOKEN_HEADER] = token;
				}
				return config;
			}
			
			function response(response) {
				return response;
			}
			
			function responseError(response) {
				switch(response.status) {
				case 401:
					authTokenServiceProvider.$get().deleteToken();	
					break;
				}
				return $q.reject(response);
			}
		}
	}
})();
(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.user')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	$stateProvider
		    .state(getUserProfileState())
            .state(getUserSettingsState());
    	
    	////////////

	    function getUserProfileState() {
		    var state = {
			    name: 'auth.userprofile',
			    url: '/userprofile/:userId',
			    templateUrl: 'app/user/profile/userProfile.html',
			    controller: 'UserProfileController',
			    controllerAs: 'vm',
			    resolve: {
			    	optionsService: 'optionsService',
			    	userService: 'userService',
			    	communicationModes: function getCommunicationModes(optionsService) {
			    		return optionsService.getCommunicationModes();
			    	},
			    	user: function getCurrentUser(userService, $stateParams) {
			    		return userService.getUserById($stateParams.userId);
			    	}
				}
		    };
		    return state;
	    }

	    function getUserSettingsState() {
		    var state = {
			    name: 'auth.usersettings',
			    url: '/usersettings',
			    templateUrl: 'app/user/settings/usersettings.html',
			    controller: 'UserSettingsController',
			    controllerAs: 'vm',
			    resolve: {
				}
		    };

		    return state;
	    }
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.run(runBlock);
    
    runBlock.$inject = ['editableOptions', 'editableThemes'];
    
    function runBlock(editableOptions, editableThemes) {
    	editableThemes.bs3.inputClass = 'input-sm';
    	editableThemes.bs3.buttonsClass = 'btn-sm';
		editableOptions.theme = 'bs3';
    }
})();
(function() {
	'use strict';

	angular
		.module('legalprojectmanagement.user')
		.factory('changePasswordModalService', changePasswordModalService);

	changePasswordModalService.$inject = ['$modal', 'authUserService'];

	function changePasswordModalService($modal) {
		var service = {
			showChangePasswordModal: showChangePasswordModal
		};

		return service;

		////////////

		function showChangePasswordModal() {
			var changePasswordModal = $modal.open({
				controller: ChangePasswordModalController,
				controllerAs: 'vm',
				size: 'sm',
				templateUrl: '/app/user/profile/change-password/change-password-modal.html',
				windowClass: ''
			});
			return changePasswordModal;

			function ChangePasswordModalController($scope, $state, $modalInstance, authUserService) {
				$scope.vm = this;
				var vm = this;

				vm.changePassword = changePassword;

				////////////

				function changePassword(oldPassword, newPassword) {
					var changePasswordParams = {
						"oldPassword": oldPassword,
						"newPassword": newPassword
					};

					authUserService.changePassword(changePasswordParams).then(function () {
						$modalInstance.close();
						$state.go('home.signin');
					}).catch(function (response) {
						vm.changePasswordError = response.data;
					});
				}
			}
		}
	}
})();
(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.user')
    	.controller('UserProfileController', UserProfileController);
    
    UserProfileController.$inject = ['$scope', '$timeout', 'user', 'authUserService', 'userService', 'changePasswordModalService', 'Upload', 'currentUser', 'communicationModes'];
    
    function UserProfileController($scope, $timeout, user, authUserService, userService, changePasswordModalService, Upload, currentUser, communicationModes) {
	    $scope.vm = this;
		var vm = this;
		
		vm.Upload = Upload;
		vm.currentUser = user.data;
		vm.communicationModes = communicationModes.data;
		vm.currentUser.anotherEmails = (vm.currentUser.anotherEmails == null || typeof vm.currentUser.anotherEmails == 'undefined') ? [] : vm.currentUser.anotherEmails;
		vm.currentUser.anotherTelephones = (vm.currentUser.anotherTelephones == null || typeof vm.currentUser.anotherTelephones == 'undefined') ? [] : vm.currentUser.anotherTelephones;
		vm.updateUser = updateUser;
		vm.uploadProfileImage = uploadProfileImage;
	    vm.showChangePasswordModalService = showChangePasswordModalService;
		
		////////////
		
		vm.submitColor = function(type) {
			updateUser(type);
	    };
	    
		function updateUser(type) {
			userService.updateUser(vm.currentUser).success(function(data) {
				vm.currentUser = data;
				setUpdateSuccessType(type);
				setUpdateUserTimeout();
				return vm.currentUser;
			}).error(function(data) {
				console.log('error in user-profile.controller.js#updateUser: ' + data);
			});
		}
		
		function setUpdateUserTimeout() {
      		$timeout(function() {
      			vm.nameSuccess = false;
      			vm.countrySuccess = false;
      			vm.locationSuccess = false;
      			vm.streetSuccess = false;
      			vm.telephoneSuccess = false;
      			vm.anotherTelephoneSuccess = false;
      			vm.emailSuccess = false;
      			vm.anotherEmailSuccess = false;
      			vm.birthdaySuccess = false;
      			vm.colorSuccess = false;
      			vm.workingOrderViewSuccess = false;
      			vm.searchHistoryAmountSuccess = false;
      			vm.contactPageSizeSuccess = false;
  			   }, 2000); 
      	}
		
		function setUpdateSuccessType(type) {
			switch(type) {
			case 'name':
				vm.nameSuccess = true;
				break;
			case 'country':
				vm.countrySuccess = true;
				break;
			case 'location':
				vm.locationSuccess = true;
				break;
			case 'street':
				vm.streetSuccess = true;
				break;
			case 'telephone':
				vm.telephoneSuccess = true;
				break;
			case 'anotherTelephone':
				vm.anotherTelephoneSuccess = true;
				break;
			case 'email': 
				vm.emailSuccess = true;
				break;
			case 'anotherEmail':
				vm.anotherEmailSuccess = true;
				break;
			case 'birthday':
				vm.birthdaySuccess = true;
				break;
			case 'color':
				vm.colorSuccess = true;
				break;
			case 'workingOrderView': 
				vm.workingOrderViewSuccess = true;
				break;
			case 'searchHistoryAmount':
				vm.searchHistoryAmountSuccess = true;
				break;
			case 'contactPageSize':
				vm.contactPageSizeSuccess = true;
				break;
			}
		}
		
		function uploadProfileImage(files, errFiles) {
        	vm.files = files;
        	vm.errFiles = errFiles;
            angular.forEach(files, function(file) {
                file.upload = vm.Upload.upload({
                    url: '/api/users/profile/upload/' + vm.currentUser.id,
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        file.result = response.data;
                        vm.currentUser.profileImagePath = response.data.profileImagePath;
                        $scope.$apply();
                    });
                }, function (response) {
                    if (response.status > 0) {
                        vm.errorMsg = response.status + ': ' + response.data;
                    }
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            });
    	}
		
		function showChangePasswordModalService() {
			changePasswordModalService.showChangePasswordModal();
		}
    }
})();