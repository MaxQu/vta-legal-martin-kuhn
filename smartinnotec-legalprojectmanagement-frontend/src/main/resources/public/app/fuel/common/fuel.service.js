(function() {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .factory('fuelService', fuelService);

    fuelService.$inject = ['$http', 'api_config'];

    function fuelService($http, api_config) {

        var FUEL_URL= api_config.BASE_URL + '/fuel';

        var service = {
            saveFuel: saveFuel,
            findAll: findAll,
            findPagedFuels: findPagedFuels,
            calculateAmountOfPages: calculateAmountOfPages,
            deleteFuel: deleteFuel,
            getFuelPageSize: getFuelPageSize,
            findSupplierContacts: findSupplierContacts,
        };

        return service;

        ////////////

        function saveFuel(fuel) {
            return $http.post(FUEL_URL, fuel);
        }


        function findAll() {
            return $http.get(FUEL_URL);
        }

        function findPagedFuels(page) {
            return $http.get(FUEL_URL + '/page/' + page);
        }

        function calculateAmountOfPages() {
            return $http.get(FUEL_URL + '/amountofpages');
        }

        function deleteFuel(fuelId) {
            return $http.delete(FUEL_URL+ '/' + fuelId);
        }

        function getFuelPageSize() {
            return $http.get(FUEL_URL + '/fuelpagesize');
        }

        function findSupplierContacts() {
            return $http.get(api_config.BASE_URL + '/contacts/suppliers')
        }

    }
})();
