(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.search', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();