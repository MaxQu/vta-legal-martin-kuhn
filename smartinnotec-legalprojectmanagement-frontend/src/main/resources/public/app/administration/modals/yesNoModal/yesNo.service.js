(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('yesNoAdministrationModalService', yesNoAdministrationModalService);
        
    yesNoAdministrationModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoAdministrationModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, user) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	user: function() {
			    		return user;
			    	}
			    }, 
				templateUrl: 'app/administration/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, user) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.user = user;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.deleteUser(vm.user);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();