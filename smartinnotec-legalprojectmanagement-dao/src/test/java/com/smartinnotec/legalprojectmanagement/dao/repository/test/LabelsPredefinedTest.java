package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.LabelsPredefined;
import com.smartinnotec.legalprojectmanagement.dao.domain.LabelsPredefinedTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public class LabelsPredefinedTest extends AbstractDAOTest {

    @Test
    public void shouldCreateLabelsPredefined() {
        tenantRepository.deleteAll();

        final Tenant tenant = new Tenant();
        tenant.setName("TestTenantHallo");
        final Tenant createdTenant = tenantRepository.save(tenant);
        Assert.assertNotNull(createdTenant);
        Assert.assertEquals("TestTenantHallo", createdTenant.getName());

        final LabelsPredefined labelsPredefined = new LabelsPredefined();
        labelsPredefined.setName("Maschinenüberprüfung");
        labelsPredefined.setLabelsPredefinedType(LabelsPredefinedTypeEnum.CONTENT);
        labelsPredefined.setTenant(tenant);
        labelsPredefinedRepository.insert(labelsPredefined);
    }
}
