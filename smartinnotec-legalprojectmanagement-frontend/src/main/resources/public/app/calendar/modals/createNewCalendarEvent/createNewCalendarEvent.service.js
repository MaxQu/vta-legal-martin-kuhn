(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('createNewEventModalService', createNewEventModalService);
        
    createNewEventModalService.$inject = ['$modal', '$stateParams', 'calendarService', 'calendarEventUserConnectionService', 'contactAndUserService', 'projectsService', 'dateUtilityService', 'projectUserConnectionService'];
    
    function createNewEventModalService($modal, $stateParams, calendarService, calendarEventUserConnectionService, contactAndUserService, projectsService, dateUtilityService, projectUserConnectionService) {
		var service = {
			showCreateNewEventModal: showCreateNewEventModal
		};		
		return service;
		
		////////////
				
		function showCreateNewEventModal(calendarEventSerialDateTypes, currentUser, selectedFilterProject, projects, allContactsAndUsers, invoker, originalCalendarEvent, calendarEventUserConnections, type) {
			 var createNewEventModal = $modal.open({
				controller: CreateNewEventModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEventSerialDateTypes: function() {
			    		return calendarEventSerialDateTypes;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	selectedFilterProject: function() {
			    		return selectedFilterProject;
			    	}, 
			    	projects: function() {
			    		return projects;
			    	}, 
			    	allContactsAndUsers: function() {
			    		return allContactsAndUsers;
			    	}, 
			    	invoker: function() {
			    		return invoker;
			    	},
			    	originalCalendarEvent: function() {
			    		return originalCalendarEvent;
			    	},
			    	calendarEventUserConnections: function() {
			    		return calendarEventUserConnections;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/createNewCalendarEvent/createNewCalendarEvent.modal.html'
			});
			return createNewEventModal;
			
			function CreateNewEventModalController($modalInstance, $scope, calendarEventSerialDateTypes, currentUser, selectedFilterProject, projects, allContactsAndUsers, invoker, originalCalendarEvent, calendarEventUserConnections, type) {
		    	var vm = this;
		    	
		    	vm.today = new Date();
		    	vm.todayPlusOneYear = setTodayPlusYears(vm.today, 1);
		    	vm.todayPlusThreeYears = setTodayPlusYears(vm.today, 3);
		    	vm.calendarEventSerialDateTypes = calendarEventSerialDateTypes;
		    	vm.currentUser = currentUser;
		    	vm.selectedFilterProject = selectedFilterProject;
		    	vm.originalCalendarEvent = originalCalendarEvent;
		    	vm.projects = projects;
		    	vm.allContactsAndUsers = allContactsAndUsers;
		    	unset();
		    	vm.type = type;
		    	vm.calendarEventStartsAtDatePicker = false;
		    	vm.calendarEventEndsAtDatePicker = false;
		    	vm.serialEndDatePicker = false;
		    	vm.startDate = new Date();
		    	vm.endDate = new Date();
		    	vm.startTime = new Date();
		    	vm.endTime = new Date();
		    	vm.calendarEvent = setCalendarEvent();
		    	vm.calendarUserConnections = calendarEventUserConnections;
		    	vm.isSerialDate = false;
		    	
		    	vm.disabledFrom = disabledFrom;
		    	vm.createOrUpdateCalendarEvent = createOrUpdateCalendarEvent;
		    	vm.createCalendarEvent = createCalendarEvent;
		    	vm.updateCalendarEvent = updateCalendarEvent;
		    	vm.addCalendarUserConnection = addCalendarUserConnection;
		    	vm.findProjectByTerm = findProjectByTerm;
		    	vm.findContactAndUserBySearchString = findContactAndUserBySearchString;
		    	vm.removeCalendarUserConnection = removeCalendarUserConnection;
		    	vm.closeCreateNewEventModal = closeCreateNewEventModal;
		    	vm.openCalendarEventStartsAtDatePicker = openCalendarEventStartsAtDatePicker;
		    	vm.openCalendarEventEndsAtDatePicker = openCalendarEventEndsAtDatePicker;
		    	vm.openSerialEndDatePicker = openSerialEndDatePicker;
		    	vm.addProject = addProject;
		    	vm.removeProject = removeProject;
		    	vm.addContactOrUser = addContactOrUser;
		    	vm.removeContactOrUser = removeContactOrUser;
		    	vm.removeContactsOrUsers = removeContactsOrUsers;
		    	vm.contactOrUserStillSelected = contactOrUserStillSelected;
		    	vm.isCurrentUserMemberOfCalendarEvent = isCurrentUserMemberOfCalendarEvent;
		    	vm.contactPersonSelectionChanged = contactPersonSelectionChanged;
		    	vm.isContactPersonSelected = isContactPersonSelected;
		    	
		    	if(vm.type == 'EDIT') {
		    		addProject(vm.calendarEvent.project, 'INIT');
		    		setProjectSelection(vm.calendarEvent.project);
		    	} else {
		    		addProject(vm.selectedFilterProject, 'NON_INIT');
		    		setProjectSelection(vm.selectedFilterProject);
		    	}
		    	
		    	////////////
		    	
		    	$scope.$watch('vm.startDate', function () {
		    		vm.endDate = vm.startDate;
		        });	
		    	
		    	function setTodayPlusYears(today, years) {
		    		var todayPlusYears = new Date();
		    		todayPlusYears.setFullYear(todayPlusYears.getFullYear()+years);
		    		return todayPlusYears;
		    	}
		    	
		    	function disabledFrom(date, mode) { 
		    		var serialDateType = vm.calendarEvent.calendarEventSerialDateType;
		    		if(serialDateType === 'NO_SERIAL_DATE') {
		    			return false;
		    		} else if(serialDateType === 'DAILY') { 
		    			return mode === 'day' && date > vm.todayPlusOneYear;
		    		} else {
		    			return mode === 'day' && date > vm.todayPlusThreeYears;
		    		}
		      	}
		    	
		    	function setCalendarEvent() {
		    		if(vm.type == 'CREATE') {
		    			var emptyCalendarEvent = {
		    		    		title: '',
		    		    		location: '',
		    		    		startsAt: null,
		    		    		endsAt: null,
		    		    		project: vm.selectedFilterProject,
		    		    		serialDate: false,
		    		    		serialEndDate: new Date(),
		    		    		calendarEventSerialDateType: 'NO_SERIAL_DATE',
		    		    		calendarUserConnections: []
		    		    	};
		    			return emptyCalendarEvent;
		    		} else if(vm.type == 'EDIT') {
		    			var calendarEvent = vm.originalCalendarEvent;
		    			calendarEvent.serialEndDate = dateUtilityService.convertToDateOrUndefined(vm.originalCalendarEvent.serialEndDate);
		    			vm.startDate = new Date(vm.originalCalendarEvent.startsAt);
				    	vm.endDate = new Date(vm.originalCalendarEvent.endsAt);
				    	vm.startTime = new Date(vm.originalCalendarEvent.startsAt);
				    	vm.endTime = new Date(vm.originalCalendarEvent.endsAt);
		    			return calendarEvent;
		    		}
		    	}
		    	
		    	function contactPersonSelectionChanged(contactPersonSelected, contactPerson, calendarUserConnection) {
		    		if(contactPersonSelected) {
		    			if(calendarUserConnection.contactPersons == null) {
		    				calendarUserConnection.contactPersons = [];
		    			}
		    			calendarUserConnection.contactPersons.push(contactPerson);
		    		} else {
			    		for(var i = 0; i < calendarUserConnection.contactPersons.length; i++) {
			    			if(calendarUserConnection.contactPersons[i].firstname == contactPerson.firstname &&
			    					 calendarUserConnection.contactPersons[i].surname == contactPerson.surname) {
			    				calendarUserConnection.contactPersons.splice(i, 1);
			    				break;
			    			}
			    		}
		    		}
		    	}
		    	
		    	function isContactPersonSelected(contactPerson, calendarUserConnection) {
		    		if(calendarUserConnection == null || calendarUserConnection.contactPersons == null || calendarUserConnection.contactPersons.length === 0) {
		    			return false;
		    		}
		    		for(var i = 0; i < calendarUserConnection.contactPersons.length; i++) {
		    			if(calendarUserConnection.contactPersons[i].firstname == contactPerson.firstname &&
		    					 calendarUserConnection.contactPersons[i].surname == contactPerson.surname) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function unsetCalendarEventObject() {
		    		vm.calendarEvent = {
				    		title: '',
				    		location: '',
				    		startsAt: null,
				    		endsAt: null,
				    		project: null,
				    		calendarUserConnections: [{
				    			user: null,
				    			contact: null,
				    			emailActive: true
				    		}]
				    	};
		    	}
		    	
		    	function setProjectSelection(project) {
		    		if(project == null) {
		    			return;
		    		}
		    		for(var i = 0; i < vm.projects.length; i++) {
		    			if(vm.projects[i].id == project.id) {
		    				vm.projects[i].selected = true;
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function openCalendarEventStartsAtDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.calendarEventStartsAtDatePicker = true;
	            	}
		    	
		    	function openCalendarEventEndsAtDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.calendarEventEndsAtDatePicker = true;
	            	}
		    	
		    	function openSerialEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.serialEndDatePicker = true;
	            	}
		    			    	
		    	function findContactAndUserBySearchString(searchString) {
		    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function (response) {
		    			return response.data;
		    		}).catch(function (data) {
						console.log('error createNewCalendarEvent.service#findContactAndUserBySearchString: ' + data);
					});
		    	}
		    	
		    	function findProjectByTerm(searchTerm) {
		    		return projectsService.findProjectByTerm(searchTerm).then(function (response) {
		    			return response.data;
		    		}).catch(function (data) {
						console.log('error createNewCalendarEvent.service#findProjectByTerm: ' + data);
					});
		    	}

		    	function addCalendarUserConnection(index) {
		    		vm.calendarUserConnections.push(vm.calendarUserConnections.length + 1);
		    	}
		    	
		    	function removeCalendarUserConnection(index) {
		    		vm.calendarUserConnections.splice(index, 1);
		    	}
		    	
		    	function reduceProjectUserConnectionsOfNonExistingCalendarUserConnections(projectUserConnections, calendarUserConnections) {
		    		if(calendarUserConnections == null) {
		    			return projectUserConnections;
		    		}
		    		var reducedProjectUserConnection = [];
		    		for(var i = 0; i < projectUserConnections.length; i++) {
		    			var projectUserConnection = projectUserConnections[i];
		    			var removeTrigger = true;
		    			for(var j = 0; j < calendarUserConnections.length; j++) {
		    				var calendarUserConnection = calendarUserConnections[j];
		    				if((projectUserConnection.user != null && projectUserConnection.user.id == calendarUserConnection.user.id) ||
		    				   (projectUserConnection.contact != null && projectUserConnection.contact.id == calendarUserConnection.contact.id)) {
		    					removeTrigger = false;
		    					break;
		    				}
		    			}
		    			if(removeTrigger === false) {
		    				reducedProjectUserConnection.push(projectUserConnection);
		    			}
		    		}
		    		return reducedProjectUserConnection;
		    	}
		    	
		    	function addProject(project, mode) {
		    		if(vm.type == 'EDIT' && mode == 'INIT') {
		    			return;
		    		}
		    		if(project == null) {
		    			return;
		    		}
		    		unsetProjects();
		    		project.selected = true;
		    		if(typeof vm.calendarEvent != 'undefined' && vm.calendarEvent.project != null) {
		    			vm.calendarEvent.project.selected = false;
		    		}
		    		if(typeof vm.calendarEvent != 'undefined') {
		    			vm.calendarEvent.project = project;
		    		}
		    		vm.calendarUserConnections = [];
		    		projectUserConnectionService.findProjectUserConnectionByProject(project.id).then(function (response) {
	    				var userOrContacts = response.data;
	    				for(var i = 0; i < userOrContacts.length; i++) {
	    					var calendarUserConnection = {};
	    					if(userOrContacts[i].user != null) {
	    						calendarUserConnection.user = userOrContacts[i].user;
	    					}
	    					if(userOrContacts[i].contact != null) {
	    						calendarUserConnection.contact = userOrContacts[i].contact;
	    					}
	    					calendarUserConnection.emailActive = true;
	    					vm.calendarUserConnections.push(calendarUserConnection);
	    				} 
	    			}).catch(function (data) {
	    				console.log('error in calendar.controller#loadPreparedCalendarEvents: ' + data);
	    			});
		    	}

		    	function removeProject(project) {
		    		project.selected = false;
		    		vm.calendarEvent.project = null;
		    	}
		    	
		    	function isEmailActiveInEditMode(contactOrUser) {
		    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
		    			if((vm.calendarUserConnections[i].user != null && vm.calendarUserConnections[i].user.id == contactOrUser.id) || 
		    		    		(vm.calendarUserConnections[i].contact != null && vm.calendarUserConnections[i].contact.id == contactOrUser.id)) {
		    				return vm.calendarUserConnections[i].emailActive;
		    			}
		    		}
		    		return true;
		    	}
		    	
		    	function addContactOrUser(contactOrUser) {
		    		addCalendarUserConnectionsToObject(contactOrUser);
		    	}
		    	
		    	function removeContactsOrUsers(contactsOrUsers) {
		    		for(var i = 0; i < contactsOrUsers.length; i++) {
		    			removeContactOrUser(contactsOrUsers[i]);
		    		}
		    	}
		    	
		    	function removeContactOrUser(calendarUserConnection) {
		    		if(calendarUserConnection.user != null) {
			    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
			    			if(vm.calendarUserConnections[i].user != null && vm.calendarUserConnections[i].user.id == calendarUserConnection.user.id) {
			    				vm.calendarUserConnections.splice(i, 1);
			    				break;
			    			}
			    		}
		    		}
		    		if(calendarUserConnection.contact != null) {
			    		for(var j = 0; j < vm.calendarUserConnections.length; j++) {
			    			if(vm.calendarUserConnections[j].contact != null && vm.calendarUserConnections[j].contact.id == calendarUserConnection.contact.id) {
			    				vm.calendarUserConnections.splice(j, 1);
			    				break;
			    			}
			    		}
		    		}
		    	}
		    	
		    	function addCalendarUserConnectionsToObject(contactOrUser) {
		    		if(userStillAdded(contactOrUser) != null) {
		    			return;
		    		}
		    		if(contactStillAdded(contactOrUser) != null) {
		    			return;
		    		}
	    			var calendarUserConnection = {};
	    			calendarUserConnection.user = contactOrUser.user;
	    			calendarUserConnection.contact = contactOrUser.contact;
	    			calendarUserConnection.emailActive = true;
	    			vm.calendarUserConnections.push(calendarUserConnection);
		    	}
		    	
		    	function userStillAdded(contactOrUser) {
		    		if(contactOrUser.contactUserType == 'USER') {
			    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
			    			if(vm.calendarUserConnections[i].user != null && vm.calendarUserConnections[i].user.id == contactOrUser.id) {
			    				return vm.calendarUserConnections[i];
			    			}
			    		}
			    		return null;
		    		}
		    	}
		    	
		    	function contactStillAdded(contactOrUser) {
		    		if(contactOrUser.contactUserType == 'CONTACT') {
			    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
			    			if(vm.calendarUserConnections[i].contact != null && vm.calendarUserConnections[i].contact.id == contactOrUser.id) {
			    				return vm.calendarUserConnections[i];
			    			}
			    		}
			    		return null;
		    		}
		    	}
		    	
		    	function createOrUpdateCalendarEvent() {
		    		if(vm.type == 'CREATE') {
		    			createCalendarEvent();
		    		} else if(vm.type == 'EDIT') {
		    			updateCalendarEvent();
		    		}
		    	}
		    	
		    	function createCalendarEvent() {
		    		var start = dateUtilityService.joinDateObjectsToDateTimeObject(vm.startDate, vm.startTime).getTime();
		    		var end = dateUtilityService.joinDateObjectsToDateTimeObject(vm.endDate, vm.endTime).getTime();
		    		
		    		if(start > end) {
	    				$scope.endDateBeforeStartDate = true;
	    				return;
	    			} else {
	    				$scope.endDateBeforeStartDate = false;
	    			}
		    		
		    		vm.calendarEvent.startsAt = start;
		    		vm.calendarEvent.endsAt = end;
		    		
		    		var calendarUserConnections = vm.calendarUserConnections;
		    		if(calendarUserConnections == null || calendarUserConnections.length === 0) {
		    			return;
		    		}
		    		var serialEndDateFormatted = vm.calendarEvent.serialEndDate.getDate() + '.' + (vm.calendarEvent.serialEndDate.getMonth()+1) + '.' + vm.calendarEvent.serialEndDate.getFullYear();
		    		calendarService.create(vm.calendarEvent, vm.calendarEvent.serialDate, vm.calendarEvent.calendarEventSerialDateType, serialEndDateFormatted).then(function (response) {
		    			var calendarEvents = response.data;
		    			invoker.prepareAndAddCalendarEvent(calendarEvents);
		    			if(calendarUserConnections != null) {
		    				for(var i = 0; i < calendarEvents.length; i++) {
				    			calendarEventUserConnectionService.create(calendarEvents[i].id, calendarUserConnections, i).then(function (response) {
				    				vm.calendarUserConnections = response.data;
				    				unsetCalendarEventObject();
				    				closeCreateNewEventModal();
				    			}).catch(function (data) {
									console.log('error createNewCalendarEvent.service#create#calendarEventUserConnection: ' + data);
								});
		    				}
		    			} else {
		    				unsetCalendarEventObject();
		    				closeCreateNewEventModal();
		    			}
		    		}).catch(function (data) {
						console.log('error createNewCalendarEvent.service#createCalendarEvent: ' + data);
					});
		    	}
		    	
		    	function updateCalendarEvent() { 
		    		if(vm.calendarUserConnections == null || vm.calendarUserConnections.length === 0) {
		    			return;
		    		}
		    		vm.calendarEvent.startsAt = dateUtilityService.joinDateObjectsToDateTimeObject(vm.startDate, vm.startTime).getTime();
		    		vm.calendarEvent.endsAt = dateUtilityService.joinDateObjectsToDateTimeObject(vm.endDate, vm.endTime).getTime();
		    		calendarService.update(vm.calendarEvent).then(function (response) {
		    			var calendarEvents = response.data; 
		    			if(calendarEvents.length === 0) {
		    				closeCreateNewEventModal(); 
		    			}
		    			if(vm.calendarUserConnections != null) {
			    			calendarEventUserConnectionService.update(calendarEvents[0].id, vm.calendarUserConnections).then(function (response) {
			    				invoker.prepareAndUpdateCalendarEvent(calendarEvents);
			    				unsetCalendarEventObject();
			    				closeCreateNewEventModal();
			    			}).catch(function (data) {
								console.log('error createNewCalendarEvent.service#update#calendarEventUserConnection: ' + data);
							});
		    			} else {
		    				unsetCalendarEventObject();
		    				closeCreateNewEventModal();
		    			}
		    		}).catch(function (data) {
						console.log('error createNewCalendarEvent.service#updateCalendarEvent: ' + data);
					});
		    	}
		    	
		    	function contactOrUserStillSelected(contactOrUser) {
		    		if(userStillAdded(contactOrUser) != null || contactStillAdded(contactOrUser) != null) {
		    			return true;
		    		}
		    		return false;
		    	}
		    	
		    	function unset() {
		    		unsetProjects();
		    		unsetCalendarEventObject();
		    	}
		    	
		    	function unsetProjects() {
		    		if(vm.projects != null && typeof vm.projects != 'undefined') {
			    		for(var i = 0; i < vm.projects.length; i++) {
			    			vm.projects[i].selected = false;
			    		}
		    		}
		    	}
		    	
		    	function isCurrentUserMemberOfCalendarEvent() {
		    		for(var i = 0; i < vm.calendarUserConnections.length; i++) {
		    			if(vm.calendarUserConnections[i].user != null && vm.calendarUserConnections[i].user.id == vm.currentUser.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function closeCreateNewEventModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();