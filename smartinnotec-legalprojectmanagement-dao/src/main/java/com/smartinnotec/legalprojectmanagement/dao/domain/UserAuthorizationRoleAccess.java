package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "UserAuthorizationRoleAccess")
public @Data class UserAuthorizationRoleAccess {

    private ProjectUserConnectionRoleEnum projectUserConnectionRole;
    private UserAuthorizationStateEnum userAuthorizationState;
    private UserAuthorizationTypeEnum userAuthorizationType;
    private Boolean access;

}
