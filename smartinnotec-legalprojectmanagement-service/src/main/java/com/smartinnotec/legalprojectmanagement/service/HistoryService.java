package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface HistoryService {

    History create(final History history) throws BusinessException;

    List<History> findAll() throws BusinessException;

    List<History> findByTenantAndDateBetween(final Tenant tenant, final DateTime start, final DateTime end) throws BusinessException;

    List<History> findPagedHistoryByTenantAndDateBetween(final Tenant tenant, final DateTime start, final DateTime end, final Integer page)
            throws BusinessException;

    Integer getHistoryPageSize() throws BusinessException;

    Integer calculateAmountOfPages(final Tenant tenant, final DateTime start, final DateTime end) throws BusinessException;

}
