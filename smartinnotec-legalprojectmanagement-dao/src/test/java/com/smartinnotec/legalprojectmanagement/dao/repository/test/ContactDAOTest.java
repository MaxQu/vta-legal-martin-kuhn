package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAttachment;

public class ContactDAOTest extends AbstractDAOTest {

    @Test
    public void shouldFindContactsContainingAddressInAddressesWithProducts() {

        int count = 0;

        final List<Contact> contacts = contactRepository.findAll();
        final List<String> addressIds = new ArrayList<>();
        final List<Contact> foundedContacts = new ArrayList<>();
        addressIds.add("59f746d0c2dc8211734e5ab9");
        addressIds.add("5b47500dc2dc0371165522be");
        addressIds.add("5b486803c2dc0371165522eb");
        addressIds.add("5b83f5dbc2dc01e6d220933b");
        addressIds.add("5b843830c2dc01e6d22093a4");
        addressIds.add("5b9f9164c2dc01e6d2209d74");
        for (final Contact contact : contacts) {

            final Address mainAddress = contact.getAddress();
            if (addressIds.contains(mainAddress.getId())) {
                foundedContacts.add(contact);
                count++;
            }

            final Address accountAddress = contact.getAccountAddress();
            if (accountAddress != null && addressIds.contains(accountAddress.getId())) {
                foundedContacts.add(contact);
                count++;
            }

            final List<ContactAddressWithProducts> contactAddressesWithProducts = contact.getAddressesWithProducts();
            if (contactAddressesWithProducts != null) {
                for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
                    final Address address = contactAddressWithProducts.getAddress();
                    if (addressIds.contains(address.getId())) {
                        foundedContacts.add(contact);
                        count++;
                    }
                }
            }
        }
        Assert.assertEquals(0, count);
    }

    @Test
    public void shouldGetUser() {
        final Contact contact1 = new Contact();
        contact1.setInstitution("Contact1");
        contact1.setImported(true);
        contact1.setImportConfirmed(false);
        contact1.setCreationDate(dateTimeFormatter.parseDateTime("11.06.2018 08:00"));
        final Contact createdContact1 = contactRepository.insert(contact1);
        Assert.assertNotNull(createdContact1);

        contactRepositoryDAO.getContactsGroupedByImportationDateAndImported();
    }

    @Test
    public void shouldFindContactsOverAgentNumbers() {
        final Contact contact = new Contact();
        contact.setInstitution("TestContact");
        final List<AgentNumberContainer> agentNumberContainers = new ArrayList<>();
        final AgentNumberContainer agentNumberContainer1 = new AgentNumberContainer();
        agentNumberContainer1.setCustomerNumber("1234");
        agentNumberContainers.add(agentNumberContainer1);
        final AgentNumberContainer agentNumberContainer2 = new AgentNumberContainer();
        agentNumberContainer2.setCustomerNumber("6788");
        agentNumberContainers.add(agentNumberContainer2);
        contact.setAgentNumberContainers(agentNumberContainers);
        final Contact createdContact = contactRepository.insert(contact);
        Assert.assertNotNull(createdContact);

        final AgentNumberContainer agentNumberContainerToSearch = new AgentNumberContainer();
        agentNumberContainerToSearch.setCustomerNumber("6788");
        final List<AgentNumberContainer> agentNumberContainersToSearch = new ArrayList<>();
        agentNumberContainersToSearch.add(agentNumberContainerToSearch);
        final List<Contact> foundedContacts = contactRepository.findByAgentNumberContainersIn(agentNumberContainersToSearch);
        Assert.assertNotNull(foundedContacts);
        Assert.assertEquals(1, foundedContacts.size());
    }

    @Test
    public void shouldFindAllContactsWithAttachments() {

        final Contact contact1 = new Contact();
        contact1.setInstitution("TestContact");
        final List<AgentNumberContainer> agentNumberContainers1 = new ArrayList<>();
        final AgentNumberContainer agentNumberContainer1 = new AgentNumberContainer();
        agentNumberContainer1.setCustomerNumber("1234");
        agentNumberContainers1.add(agentNumberContainer1);
        final AgentNumberContainer agentNumberContainer2 = new AgentNumberContainer();
        agentNumberContainer2.setCustomerNumber("6788");
        agentNumberContainers1.add(agentNumberContainer2);
        contact1.setAgentNumberContainers(agentNumberContainers1);
        final Contact createdContact1 = contactRepository.insert(contact1);
        Assert.assertNotNull(createdContact1);

        final Contact contact2 = new Contact();
        contact2.setInstitution("TestContact");
        List<ContactAttachment> contactAttachments = new ArrayList<>();
        final ContactAttachment contactAttachment = new ContactAttachment();
        contactAttachment.setFilename("filename");
        contactAttachments.add(contactAttachment);
        contact2.setContactAttachments(contactAttachments);
        final Contact createdContact2 = contactRepository.insert(contact2);
        Assert.assertNotNull(createdContact2);

        final List<Contact> contacts = contactRepository.findByContactAttachmentsNotNull();
        Assert.assertEquals(1, contacts.size());
    }
}
