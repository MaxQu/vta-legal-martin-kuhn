(function() {
    'use strict';
        
    angular
    	.module('legalprojectmanagement.accesscontrol')
    	.controller('AccessControlController', AccessControlController);
        
    AccessControlController.$inject = ['$scope', 'accessControls', 'projects', 'defaultProject', 'dateUtilityService', 'accessControlService', 'printAccessControlModalService'];
         
    function AccessControlController($scope, accessControls, projects, defaultProjects, dateUtilityService, accessControlService, printAccessControlModalService) {
	    $scope.vm = this; 
    	var vm = this;
    	                   
    	vm.projects = projects.data;  
    	vm.defaultProjects = defaultProjects.data;
    	vm.selectedProject = vm.projects.length == 1 ? vm.projects[0] : (vm.defaultProject != null ? vm.defaultProject : null);
    	vm.accessControls = accessControls.data;
    	vm.filterStartDatePicker = false;
    	vm.filterEndDatePicker = false;
    	vm.filterStartDate = new Date();
    	vm.filterEndDate = new Date();
    	vm.activatedLiveAccessControl = false;
    	
    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
    	vm.getAccessControlsInRange = getAccessControlsInRange;
    	vm.showPrintAccessControlModalService = showPrintAccessControlModalService;
    	vm.activateLiveAccessControls = activateLiveAccessControls;
    	
    	////////////
    	
    	function openFilterStartDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterStartDatePicker = true;
        }
    	
    	function openFilterEndDatePicker($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.filterEndDatePicker = true;
        }
    	
    	function getAccessControlsInRange() {
    		if(vm.filterStartDate == null || vm.filterEndDate == null) {
    			return;
    		}
    		var start = dateUtilityService.formatDateToString(vm.filterStartDate);
    		var end = dateUtilityService.formatDateToString(vm.filterEndDate);
    		accessControlService.getAccessControlsInRange(start, end).then(function (response) {
    			vm.accessControls = response.data;
			}).catch(function (data) {
				console.log('error in accesscontrol.controller#getAccessControlsInRange: ' + data);
			});
    	}
    
    	function showPrintAccessControlModalService() {
    		printAccessControlModalService.showPrintAccessControlModal(vm.accessControls);
    	}
    	
    	$scope.connect = function() {
            var socket = new SockJS('/api/accesscontrol');
            $scope.stompClient = Stomp.over(socket);            
            $scope.stompClient.connect({}, function(frame) {
            	$scope.stompClient.subscribe('/topic/accesscontrol/projectId', function(message) {
            		var parsedMessage = JSON.parse(message.body);
            		vm.accessControls.push(parsedMessage);
            		$scope.$apply();
                });
            });
        };
		
        $scope.unconnection = function() {
        	if($scope.stompClient != null) {
        		$scope.stompClient.unsubscribe();
        		$scope.stompClient.disconnect(function() {
                	console.log("Disconnected stomClient in accesscontrol.controller#destroy");
                });
            } else {
            	console.log("vm.stompClient is null and cannot be disconnected in accesscontrol.controller#destroy");
            } 
        };
        
		$scope.$on('$destroy', function() {  // invoked if page is leaved 
			$scope.unconnection();
    	});
		
		function activateLiveAccessControls() {
			if(vm.activatedLiveAccessControl === true) {
				$scope.unconnection();
			} else {
				$scope.connect();
			}
			vm.activatedLiveAccessControl = !vm.activatedLiveAccessControl;
		}
	} 
})();