(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('productsOfTenantModalService', productsOfTenantModalService);
        
    productsOfTenantModalService.$inject = ['$modal', '$stateParams'];
    
    function productsOfTenantModalService($modal, $stateParams) {
		var service = {
			showProductsOfTenantModal: showProductsOfTenantModal
		};		
		return service;
		
		////////////
				
		function showProductsOfTenantModal(products) {
			 var productsOfTenantModal = $modal.open({
				controller: ProductsOfTenantModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	products: function() {
			    		return products;
			    	}
			    }, 
				templateUrl: 'app/products/modals/productsOfTenantModal/productsOfTenant.modal.html'
			});
			return productsOfTenantModal;
			
			function ProductsOfTenantModalController($modalInstance, $scope, products) {
		    	var vm = this; 
		    	vm.products = products;
		    	vm.orderType = 'name';

		    	vm.closeProductsOfTenantModal = closeProductsOfTenantModal;
		    	
		    	////////////
		    	
		    	function closeProductsOfTenantModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();