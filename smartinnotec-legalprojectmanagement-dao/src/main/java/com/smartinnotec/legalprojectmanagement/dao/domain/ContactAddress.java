package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class ContactAddress {

    private String street;
    private String location;
    private String zip;
    private CountryTypeEnum country;
}
