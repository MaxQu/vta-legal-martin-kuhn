package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;

@Repository
public class ActivityRepositoryDAO {

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Activity> findActivities(final String searchString) {
        Query query = new Query();
        query.addCriteria(Criteria.where("activityDocumentFiles.originalFileName").regex(searchString, "i"));
        final List<Activity> foundedActivities = mongoTemplate.find(query, Activity.class);
        return foundedActivities;
    }
}
