package com.smartinnotec.legalprojectmanagement.service.container;

import lombok.Data;

public @Data class ProductTypeOverviewContainer {

    private String productType;
    private Integer amount;
}
