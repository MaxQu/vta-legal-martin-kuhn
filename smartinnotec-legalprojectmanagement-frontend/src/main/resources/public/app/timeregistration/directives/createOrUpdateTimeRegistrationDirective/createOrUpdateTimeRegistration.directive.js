(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration')
    	.directive('createOrUpdateTimeRegistrationDirective', createOrUpdateTimeRegistrationDirective);
    
    createOrUpdateTimeRegistrationDirective.$inject = ['$timeout', 'dateUtilityService'];
    
	function createOrUpdateTimeRegistrationDirective($timeout, dateUtilityService) {
		var directive = {
			restrict: 'E',
			scope: {
				type: '=',
				projects: '=',
				selectedProject: '=',
				timeRegistration: '=',
				invoker: '=',
				modalInvoker: '='
			},
			templateUrl: 'app/timeregistration/directives/createOrUpdateTimeRegistrationDirective/createOrUpdateTimeRegistration.html',
			link: function($scope) {
				$scope.startDate = convertDateTimeFromToDate();
				$scope.startTime = convertDateTimeFromToDate();
				$scope.endDate = convertDateTimeUntilToDate();
				$scope.endTime = convertDateTimeUntilToDate();
				$scope.startDatePicker = false;
				$scope.endDatePicker = false; 
				$scope.projectNotSetError = false;
		    	
				function convertDateTimeFromToDate() {
					if($scope.timeRegistration.dateTimeFrom == null) {
						return new Date();
					}
					if($scope.timeRegistration.dateTimeFrom instanceof Date) {
						return $scope.timeRegistration.dateTimeFrom;
					}
					return dateUtilityService.convertToDateTimeOrUndefined($scope.timeRegistration.dateTimeFrom);
				}
				
				function convertDateTimeUntilToDate() {
					if($scope.timeRegistration.dateTimeUntil == null) {
						return new Date();
					}
					if($scope.timeRegistration.dateTimeUntil instanceof Date) {
						return $scope.timeRegistration.dateTimeUntil;
					}
					return dateUtilityService.convertToDateTimeOrUndefined($scope.timeRegistration.dateTimeUntil);
				}
				
				$scope.openStartDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.startDatePicker = true;
		        };
		    	
				$scope.openEndDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.endDatePicker = true;
		        };
				
				$scope.addProject = function(project) {
		    		removeProjectSelection();
		    		project.selected = true;
		    		$scope.timeRegistration.project = project;
		    	};
				
		    	if($scope.selectedProject != null) {
		    		for(var i = 0; i < $scope.projects.length; i++) {
		    			if($scope.projects[i].id == $scope.selectedProject.id) {
		    				$scope.addProject($scope.projects[i]);
		    				break;
		    			}
		    		}
				}
		    	
		    	$scope.removeProject = function(project) {
		    		project.selected = false;
		    		$scope.timeRegistration.project = null;
		    	};
		    	
		    	function removeProjectSelection() {
		    		for(var i = 0; i < $scope.projects.length; i++) {
		    			$scope.projects[i].selected = false;
		    		}
		    	}
		    	
		    	$scope.createOrUpdateTimeRegistration = function() {
					if($scope.timeRegistration.project == null) {
						$scope.projectNotSetError = true;
						return;
					}
					$scope.projectNotSetError = false;
					if($scope.type == 'CREATE') {
						$scope.invoker.createTimeRegistration($scope.timeRegistration, $scope.startDate, $scope.startTime, $scope.endDate, $scope.endTime);
						removeProjectSelection();
					} else if($scope.type == 'EDIT') {
						$scope.invoker.updateTimeRegistration($scope.timeRegistration, $scope.startDate, $scope.startTime, $scope.endDate, $scope.endTime);
					}
					$scope.modalInvoker.closeCreateOrUpdateTimeRegistrationModal();
				};
				
			 }
		};
		return directive;
		
		////////////
	}
})();
