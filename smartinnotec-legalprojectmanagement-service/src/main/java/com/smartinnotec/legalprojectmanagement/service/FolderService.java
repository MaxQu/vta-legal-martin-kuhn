package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Folder;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.container.BreadCrumbContainer;

public interface FolderService {

    Folder create(final Folder folder) throws BusinessException;

    Folder update(final Folder folder) throws BusinessException;

    Folder findById(final String folderId) throws BusinessException;

    List<Folder> findProjectFoldersBySearchString(final User user, final String searchString) throws BusinessException;

    List<Folder> findProductFoldersBySearchString(final User user, final String searchString) throws BusinessException;

    List<Folder> findAllFoldersOfProject(final String projectId) throws BusinessException;

    List<Folder> findByNameAndSuperFolderId(final String name, final String superFolderId) throws BusinessException;

    BreadCrumbContainer findBreadCrumbOfFolder(final String folderId) throws BusinessException;

    List<Folder> findFoldersByBySearchStringAndProjectIdAndFolderId(final String projectId, final String folderId,
            final String searchString) throws BusinessException;

    List<Folder> getFolders(final String projectId, final String superFolderId) throws BusinessException;

    void deleteFolder(final String folderId) throws BusinessException;

}
