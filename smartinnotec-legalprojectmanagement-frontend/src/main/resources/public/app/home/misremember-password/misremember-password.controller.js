(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.controller('MisrememberPasswordController', MisrememberPasswordController);
    
    MisrememberPasswordController.$inject = ['$scope', '$state', 'authUserService'];
    
    function MisrememberPasswordController($scope, $state, authUserService) {
	    $scope.vm = this;
    	var vm = this;
    	
		vm.misrememberPassword = misrememberPassword;
		
		////////////
			
		function misrememberPassword(emailAddress) {
			authUserService.misrememberPassword(emailAddress).then(function () {
				$state.go('home.signin', {});
			}).catch(function (response) {
				vm.misrememberPasswordError = response.data;
			});
		}
	}
    
})();