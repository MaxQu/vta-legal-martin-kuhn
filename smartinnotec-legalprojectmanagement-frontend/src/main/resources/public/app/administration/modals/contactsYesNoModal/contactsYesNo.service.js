(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('contactsYesNoModalService', contactsYesNoModalService);
        
    contactsYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function contactsYesNoModalService($modal, $stateParams) {
		var service = {
			showContactsYesNoModal: showContactsYesNoModal
		};		
		return service;
		
		////////////
				
		function showContactsYesNoModal(invoker, amount, selectedCountry) {
			 var contactsYesNoModal = $modal.open({
				controller: ContactsYesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	amount: function() {
			    		return amount;
			    	},
			    	selectedCountry: function() {
			    		return selectedCountry;
			    	}
			    }, 
				templateUrl: 'app/administration/modals/contactsYesNoModal/contactsYesNo.modal.html'
			});
			return contactsYesNoModal;
			
			function ContactsYesNoModalController($modalInstance, $scope, invoker, amount, selectedCountry) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.amount = amount;
		    	vm.selectedCountry = selectedCountry;
		    	
		    	vm.contactsDeleteAll = contactsDeleteAll;
		    	vm.closeContactsYesNoModal = closeContactsYesNoModal;
		    	
		    	////////////
		    	
		    	function contactsDeleteAll() {
		    		vm.invoker.deleteAllCustomerContacts(vm.selectedCountry);
		    		vm.closeContactsYesNoModal();
		    	}
		    	
		    	function closeContactsYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();