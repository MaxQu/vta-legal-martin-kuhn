package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface CalendarEventRepository extends MongoRepository<CalendarEvent, String> {

    List<CalendarEvent> findByTenant(final Tenant tenant);

    List<CalendarEvent> findByTenantAndArchivedAndStartsAtBetween(final Tenant tenant, final boolean archived, final Long start,
            final Long end);

    List<CalendarEvent> findByTenantAndArchivedAndEndsAtBetween(final Tenant tenant, final boolean archived, final Long start,
            final Long end);

    CalendarEvent findByLocationAndProjectAndArchived(final String location, final Project project, final boolean archived);

    List<CalendarEvent> findByTenantAndArchivedAndStartsAtLessThanAndEndsAtGreaterThan(final Tenant tenant, final boolean archived,
            final Long start, final Long end);

    CalendarEvent findByProjectAndLocationAndArchived(final Project project, final String location, final boolean archived);

    List<CalendarEvent> findByProjectAndArchived(final Project project, final boolean archived);

    List<CalendarEvent> findBySerialDateNumberAndArchived(final Long serialDateNumber, final boolean archived);

    List<CalendarEvent> findByCalendarEventTypAndConfirmedAndArchived(final CalendarEventTypeEnum calendarEventType,
            final boolean confirmed, final boolean archived);

    @Query("{$text : { $search : ?0 } }")
    List<CalendarEvent> findCalendarEventBySearchString(final String searchString);

}
