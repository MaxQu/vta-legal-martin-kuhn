package com.smartinnotec.legalprojectmanagement.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.activation.MimetypesFileTypeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;

@Service("fileUploadService")
public class FileUploadServiceImpl extends AbstractService implements FileUploadService {


    public FileUploadServiceImpl() {
    }

    @Override
    public String uploadFile(final String foldername, final String filename, final MultipartFile multipartFile) throws BusinessException {
        BusinessAssert.notNull(filename, "folderName is mandatory in FileUploadServiceImpl#uploadFile");
        final String folderName = foldername;
        final String dirName = uploadPath + File.separator + folderName;
        logger.info("store file at directory with name '{}' in file with name '{}' in FileUploadServiceImpl#uploadFile", dirName, filename);

        try {
            final File folder = new File(dirName);
            final File file = new File(dirName + File.separator + filename);
            if (!folder.exists()) {
                final boolean dirCreated = folder.mkdirs();
                logger.info("directory was created = '{}' in FileUploadServiceImpl#uploadFile", dirCreated);
            }
            final boolean newFileCreated = file.createNewFile();
            logger.info("new file created is '{}' in FileUploadServiceImpl#uploadFile", newFileCreated);

            final byte[] bytes = multipartFile.getBytes();
            final FileOutputStream fos = new FileOutputStream(file);
            final BufferedOutputStream stream = new BufferedOutputStream(fos);
            stream.write(bytes);
            fos.flush();
            stream.flush();
            fos.close();
            stream.close();

        
            logger.debug("stored file at directory with name '{}' in file with name '{}' in FileUploadServiceImpl#uploadFile", dirName,
                filename);

            return file.getAbsolutePath();
        } catch (Exception e) {
            logger.error("Exception '{}' in FileUploadServiceImpl#uploadFile", e.getMessage());
            throw new BusinessException(e.getMessage());
        }
    }

    @Override
    public boolean isImage(final File file) throws BusinessException {
        BusinessAssert.notNull(file, "file is mandatory in FileUploadServiceImpl#isImage", "400");
        logger.info("is file an image in FileUploadServiceImpl#isImage");
        final String mimetype = new MimetypesFileTypeMap().getContentType(file);
        if (mimetype.startsWith("image/")) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "[FileUploadServiceImpl]";
    }
}
