(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.controller('SignupController', SignupController);
    
    SignupController.$inject = ['$scope', '$state', 'authUserService'];
    
    function SignupController($scope, $state, authUserService) {
	    $scope.vm = this;
    	var vm = this;
    	
    	vm.signupError = null;
    	
    	vm.user = {
			username: '',
			password: '',
			title: 'Mag.',
			firstname: '',
			surname: '',
			sex: 'MALE',
			birthdate: new Date(),
    		email: '',
    		confession: 'ROEM_CATH',
    		telephone: '',
    		tenant: {
    			name: ''
    		},
    		address: {
    			country: 'austria',
    			postalCode: '',
    			region: '',
    			street: ''
    		}
		};
    	vm.openedDatePicker = false;
    	
    	vm.signup = signup;
    	
    	////////////
    	
    	function signup(newUser) {  
			newUser.active = true;
			authUserService.signup(newUser).then(function () {
				authUserService.signin(newUser.username, newUser.password).then(function(response) {
					var createdUserId = response.data.user.id;
					$state.go('auth.dashboard',{userId:createdUserId});
				});
			}).catch(function (response) {
				vm.signupError = response.data.errorCodeType;
				console.log("error in signup.controller#signup: " + response.data.message);
			});
		}
    }    
})();
