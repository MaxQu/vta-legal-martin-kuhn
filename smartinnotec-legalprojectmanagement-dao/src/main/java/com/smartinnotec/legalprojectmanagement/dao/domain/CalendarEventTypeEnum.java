package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum CalendarEventTypeEnum {

    OUTSIDE_WORK("OUTSIDE_WORK", false),
    MEETING("MEETING", false),
    PRIVATE_DATE("PRIVATE_DATE", false),
    FIX_DATE("FIX_DATE", true),
    FRAME_DATE("FRAME_DATE", true);

    private String value;
    private boolean active;

    private CalendarEventTypeEnum(final String value, final boolean active) {
        this.value = value;
        this.active = active;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }
}
