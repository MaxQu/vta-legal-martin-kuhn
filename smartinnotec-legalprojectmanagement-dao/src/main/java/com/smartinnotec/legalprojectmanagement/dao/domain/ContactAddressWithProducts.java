package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.Data;

public @Data class ContactAddressWithProducts {

    @DBRef
    private Address address;
    private List<ContactPerson> contactPersons;
    private List<String> productIds;
    @Transient
    private List<Product> products;

    public void addProduct(final Product product) {
        if (products == null) {
            products = new ArrayList<>();
        }
        products.add(product);
    }

    public void addProductId(final String productId) {
        if (productIds == null) {
            productIds = new ArrayList<>();
        }
        productIds.add(productId);
    }

    public void removeProductId(final String productId) {
        if (productIds == null) {
            return;
        }
        for (final Iterator<String> iterator = productIds.iterator(); iterator.hasNext();) {
            final String next = iterator.next();
            if (next.equals(productId)) {
                iterator.remove();
                break;
            }
        }
    }

    @Override
    public boolean equals(final Object obj) {
        final ContactAddressWithProducts contactAddressWithProducts = (ContactAddressWithProducts)obj;
        if (this.address == null || contactAddressWithProducts.getAddress() == null) {
            return false;
        } else if (this.address.getId().equals(contactAddressWithProducts.getAddress().getId())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((productIds == null) ? 0 : productIds.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "[Address:" + address + "]";
    }
}
