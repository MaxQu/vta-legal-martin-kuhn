package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface ProjectUserConnectionRepository extends MongoRepository<ProjectUserConnection, String> {

    List<ProjectUserConnection> findByUserAndActiveAndContactIsNull(final User user, final boolean active);

    List<ProjectUserConnection> findByProjectAndActive(final Project project, final boolean active);

    ProjectUserConnection findByUserAndProjectAndActive(final User user, final Project project, final boolean active);

    ProjectUserConnection findByProjectAndContactAndActive(final Project project, final Contact contact, final boolean active);

    ProjectUserConnection findByUserAndContactAndActive(final User user, final Contact contact, final boolean active);

    List<ProjectUserConnection> findByUserAndActiveAndContactIsNotNull(final User user, final boolean active);

    List<ProjectUserConnection> findByContactAndUserIsNull(final Contact contact);

    List<ProjectUserConnection> findByContactAndProjectIsNull(final Contact contact);

}
