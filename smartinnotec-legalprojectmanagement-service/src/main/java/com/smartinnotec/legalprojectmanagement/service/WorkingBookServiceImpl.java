package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.repository.WorkingBookRepository;

@Service("workingBookService")
public class WorkingBookServiceImpl extends AbstractService implements WorkingBookService {

    @Autowired
    private WorkingBookRepository workingBookRepository;
    @Autowired
    private SearchService searchService;

    @Override
    public WorkingBook create(final User user, final WorkingBook workingBook) throws BusinessException {
        BusinessAssert.notNull(workingBook, "workingBook is mandatory in WorkingBookServiceImpl#create", "400");
        logger.info("create working book in WorkingBookServiceImpl#create");
        workingBook.setCreationDateTime(new DateTime(System.currentTimeMillis()));
        workingBook.setTenant(user.getTenant());
        workingBook.setUser(user);
        return workingBookRepository.insert(workingBook);
    }

    @Override
    public WorkingBook update(final WorkingBook workingBook) throws BusinessException {
        BusinessAssert.notNull(workingBook, "workingBook is mandatory in WorkingBookServiceImpl#update", "400");
        logger.info("update working book with id '{}' in WorkingBookServiceImpl#update", workingBook.getId());
        return workingBookRepository.save(workingBook);
    }

    @Override
    public List<WorkingBook> findByProject(final Project project) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in WorkingBookServiceImpl#findByProject", "400");
        logger.info("find workingBook by project with id '{}' in WorkingBookServiceImpl#findByProject", project.getId());
        return workingBookRepository.findByProject(project);
    }

    @Override
    public WorkingBook findById(final String workingBookId) throws BusinessException {
        BusinessAssert.notNull(workingBookId, "workingBookId is mandatory in WorkingBookServiceImpl#findById", "400");
        BusinessAssert.isId(workingBookId, "workingBookId must be an id in WorkingBookServiceImpl#findById", "400");
        logger.info("find workingBook by id '{}' in WorkingBookServiceImpl#findById", workingBookId);
        return workingBookRepository.findOne(workingBookId);
    }

    @Override
    public List<WorkingBook> findByProjectAndTenant(final Project project, final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in WorkingBookServiceImpl#findByProjectOfTenant", "400");
        BusinessAssert.notNull(tenant, "tenant is mandatory in WorkingBookServiceImpl#findByProjectOfTenant", "400");
        logger.info("find workingbook by project with id '{}' and tenant with id '{}' in WorkingBookServiceImpl#findByProjectOfTenant",
            project.getId(), tenant.getId());
        return workingBookRepository.findByProjectAndTenant(project, tenant);
    }

    @Override
    public List<WorkingBook> findByProjectAndTenantAndDateTimeFromBetween(final Project project, final Tenant tenant, final DateTime start,
            final DateTime end) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in WorkingBookServiceImpl#findByProjectAndTenantAndDateTimeFromBetween",
            "400");
        BusinessAssert.notNull(tenant, "tenant is mandatory in WorkingBookServiceImpl#findByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(start, "start is mandatory in WorkingBookServiceImpl#findByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in WorkingBookServiceImpl#findByProjectAndTenantAndDateTimeFromBetween", "400");
        logger.info(
            "find workingBook entries by project with id '{}', tenant with id '{}', start '{}' and end '{}' in WorkingBookServiceImpl#findByProjectAndTenantAndDateTimeFromBetween",
            project.getId(), tenant.getId(), start, end);
        return workingBookRepository.findByProjectAndTenantAndDateTimeFromBetween(project, tenant, start, end);
    }

    @Override
    public List<WorkingBook> findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetween(final Integer page, final String sortType,
            Project project, Tenant tenant, DateTime start, DateTime end) throws BusinessException {
        BusinessAssert.notNull(project,
            "project is mandatory in WorkingBookServiceImpl#findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(tenant,
            "tenant is mandatory in WorkingBookServiceImpl#findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(start,
            "start is mandatory in WorkingBookServiceImpl#findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(end,
            "end is mandatory in WorkingBookServiceImpl#findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetween", "400");
        logger.info(
            "find paged workingBook entries by project with id '{}', tenant with id '{}', start '{}' and end '{}' in WorkingBookServiceImpl#findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetween",
            project.getId(), tenant.getId(), start, end);
        final Pageable pageable = new PageRequest(page, this.getWorkingBookPageSize(), new Sort(Sort.Direction.ASC, sortType));

        final Page<WorkingBook> pagedWorkingBooks = workingBookRepository
            .findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetweenOrderByCreationDateTimeDesc(pageable, project, tenant, start, end);

        final List<WorkingBook> pagedWorkingBookList = new ArrayList<>();
        for (final WorkingBook pagedWorkingBook : pagedWorkingBooks) {
            pagedWorkingBookList.add(pagedWorkingBook);
        }
        return pagedWorkingBookList;
    }

    @Override
    public List<WorkingBook> findWorkingBookByProjectAndTenantAndSearchString(final Project project, final Tenant tenant,
            String searchString) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in WorkingBookServiceImpl#findWorkingBookByProjectAndTenantAndSearchString",
            "400");
        BusinessAssert.notNull(tenant, "tenant is mandatory in WorkingBookServiceImpl#findWorkingBookByProjectAndTenantAndSearchString",
            "400");
        BusinessAssert.notNull(searchString, "searchString is mandatory in WorkingBookServiceImpl#findWorkingBookByProjectAndSearchString",
            "400");
        logger.info(
            "find workingBook by project with id '{}', tenant with id '{}' and searchString '{}' in WorkingBookServiceImpl#findWorkingBookByProjectAndTenantAndSearchString",
            project.getId(), tenant.getId(), searchString);

        List<WorkingBook> workingBooks = new ArrayList<>();
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        if (combineSearchStringWithAnd) {
            searchString = searchString.replaceAll("\\+", "");
            workingBooks = workingBookRepository.findWorkingBookBySearchString(searchString);
            for (final Iterator<WorkingBook> iterator = workingBooks.iterator(); iterator.hasNext();) {
                final WorkingBook workingBook = iterator.next();
                final String workingBookProperties = workingBook.getWorkingText();
                if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(workingBookProperties.toLowerCase()::contains)) {
                    iterator.remove();
                }
            }
        } else {
            workingBooks = workingBookRepository.findWorkingBookBySearchString(searchString);
        }

        workingBooks.removeIf(s -> s.getProject() == null || !s.getProject().getId().equals(project.getId())
            || !s.getProject().getTenant().getId().equals(tenant.getId()));
        return workingBooks;
    }

    @Override
    public Integer getWorkingBookPagesByProjectAndTenantAndDateTimeFromBetween(Project project, Tenant tenant, DateTime start, DateTime end)
            throws BusinessException {
        BusinessAssert.notNull(project,
            "project is mandatory in WorkingBookServiceImpl#getWorkingBookPagesByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(tenant,
            "tenant is mandatory in WorkingBookServiceImpl#getWorkingBookPagesByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(start,
            "start is mandatory in WorkingBookServiceImpl#getWorkingBookPagesByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(end,
            "end is mandatory in WorkingBookServiceImpl#getWorkingBookPagesByProjectAndTenantAndDateTimeFromBetween", "400");
        logger.info(
            "find amount of workingBook entries by project with id '{}', tenant with id '{}', start '{}' and end '{}' in WorkingBookServiceImpl#getWorkingBookPagesByProjectAndTenantAndDateTimeFromBetween",
            project.getId(), tenant.getId(), start, end);
        final Integer wholeAmountOfWorkingBooks = workingBookRepository.countWorkingBookByProjectAndTenantAndDateTimeFromBetween(project,
            tenant, start, end);
        final Integer amountOfPageSizes = (wholeAmountOfWorkingBooks + this.getWorkingBookPageSize() - 1) / this.getWorkingBookPageSize();
        return amountOfPageSizes;
    }

    @Override
    public Integer countWorkingBookByProjectAndTenantAndDateTimeFromBetween(final Project project, final Tenant tenant,
            final DateTime start, final DateTime end) throws BusinessException {
        BusinessAssert.notNull(project,
            "project is mandatory in WorkingBookServiceImpl#countWorkingBookByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(tenant,
            "tenant is mandatory in WorkingBookServiceImpl#countWorkingBookByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(start,
            "start is mandatory in WorkingBookServiceImpl#countWorkingBookByProjectAndTenantAndDateTimeFromBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in WorkingBookServiceImpl#countWorkingBookByProjectAndTenantAndDateTimeFromBetween",
            "400");
        logger.info(
            "find amount of workingBook entries by project with id '{}', tenant with id '{}', start '{}' and end '{}' in WorkingBookServiceImpl#countWorkingBookByProjectAndTenantAndDateTimeFromBetween",
            project.getId(), tenant.getId(), start, end);
        final Integer wholeAmountOfWorkingBooks = workingBookRepository.countWorkingBookByProjectAndTenantAndDateTimeFromBetween(project,
            tenant, start, end);
        return wholeAmountOfWorkingBooks;
    }

    @Override
    public WorkingBook findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc(final Project project, final Tenant tenant,
            final WorkingBookCategoryTypeEnum categoryType) throws BusinessException {
        BusinessAssert.notNull(project,
            "project is mandatory in WorkingBookServiceImpl#findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc", "400");
        BusinessAssert.notNull(tenant,
            "tenant is mandatory in WorkingBookServiceImpl#findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc", "400");
        BusinessAssert.notNull(categoryType,
            "categoryType is mandatory in WorkingBookServiceImpl#findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc",
            "400");
        logger.info(
            "find first workingBook entry of project with id '{}', tenant with id '{}' and categoryType '{}' in WorkingBookServiceImpl#findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc",
            project.getId(), tenant.getId(), categoryType);
        return workingBookRepository.findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc(project, tenant, categoryType);
    }

    @Override
    public void delete(final String workingBookEntryId) throws BusinessException {
        BusinessAssert.notNull(workingBookEntryId, "workingBookId is mandatory in WorkingBookServiceImpl#delete", "400");
        BusinessAssert.isId(workingBookEntryId, "workingBookId must be an id in WorkingBookServiceImpl#delete", "400");
        logger.info("delete workingBook enty with id '{}' in WorkingBookServiceImpl#delete", workingBookEntryId);
        workingBookRepository.delete(workingBookEntryId);
    }

    @Override
    public String toString() {
        return "[WorkingBookServiceImpl]";
    }
}
