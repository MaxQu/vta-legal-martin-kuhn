(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactImportedYesNoModalService', contactImportedYesNoModalService);
        
    contactImportedYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function contactImportedYesNoModalService($modal, $stateParams) {
		var service = {
			showContactImportedYesNoModal: showContactImportedYesNoModal
		};		
		return service;
		
		////////////
				
		function showContactImportedYesNoModal(invoker, contactImported, index) {
			 var yesNoModal = $modal.open({
				controller: ContactImportedYesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contactImported: function() {
			    		return contactImported;
			    	},
			    	index: function() {
			    		return index;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/contactImportedYesNoModal/contactImportedYesNo.modal.html'
			});
			return yesNoModal;
			
			function ContactImportedYesNoModalController($modalInstance, $scope, invoker, contactImported, index) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contactImported = contactImported;
		    	vm.index = index;
		    	
		    	vm.deleteContactImportedYesNoModal = deleteContactImportedYesNoModal;
		    	vm.closeContactImportedYesNoModal = closeContactImportedYesNoModal;
		    	
		    	////////////
		    	
		    	function deleteContactImportedYesNoModal() {   
		    		invoker.deleteContactImported(vm.contactImported, vm.index);
		    		vm.closeContactImportedYesNoModal();
		    	}
		    	
		    	function closeContactImportedYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();