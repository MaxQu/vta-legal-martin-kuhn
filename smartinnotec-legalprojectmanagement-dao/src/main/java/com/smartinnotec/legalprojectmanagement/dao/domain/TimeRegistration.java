package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "TimeRegistration")
public @Data class TimeRegistration {

    @Id
    private String id;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime dateTimeFrom;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime dateTimeUntil;
    @DBRef
    private User user;
    private String timeRange;
    @TextIndexed
    private String text;
    @DBRef
    private Project project;
    @DBRef
    private Tenant tenant;

}
