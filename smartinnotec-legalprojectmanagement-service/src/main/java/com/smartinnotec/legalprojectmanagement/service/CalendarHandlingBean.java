package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(locations = "classpath:application.properties", ignoreUnknownFields = true, prefix = "legalprojectmanagement.calendar")
public @Data class CalendarHandlingBean {

    private String icsLink;

}
