package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ReceipeTypeEnum {

    STANDARD_MIXTURE("STANDARD_MIXTURE"),
    CHANGED_MIXTURE("CHANGED_MIXTURE");

    private String value;

    private ReceipeTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
