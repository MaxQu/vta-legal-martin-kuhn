package com.smartinnotec.legalprojectmanagement.service.container;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.FacilityDetails;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

import lombok.Data;

public @Data class ReportingContainer {

    private CalendarEvent calendarEvent;
    private Contact contact;
    private User user;
    private List<Activity> activities;
    private List<FacilityDetails> facilityDetails;

}
