(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('shiftDocumentFileModalService', shiftDocumentFileModalService);
        
    shiftDocumentFileModalService.$inject = ['$modal', '$stateParams', '$timeout', 'ngToast', 'folderTreeService'];
    
    function shiftDocumentFileModalService($modal, $stateParams, $timeout, ngToast, folderTreeService) {
		var service = {
			showShiftDocumentFileModal: showShiftDocumentFileModal
		};		
		return service;
		
		////////////
				
		function showShiftDocumentFileModal(invoker, folders, documentFile) {
			 var shiftDocumentFileModal = $modal.open({
				controller: ShiftDocumentFileModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	folders: function() {
			    		return folders;
			    	},
			    	documentFile: function() {
			    		return documentFile;
			    	}
			    }, 
				templateUrl: 'app/project/modals/shiftDocumentFileModal/shiftDocumentFile.modal.html'
			});
			return shiftDocumentFileModal;
			
			function ShiftDocumentFileModalController($modalInstance, $scope, invoker, folders, documentFile) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.folders = folders;
		    	vm.documentFile = documentFile;
		    	vm.folderTree = null;
		    	vm.errorShiftDocumentFileStillInFolder = false;
		    	
		    	createTree();
		    	vm.shiftToRoot = shiftToRoot;
		    	vm.shiftToFolder = shiftToFolder;
		    	vm.closeShiftDocumentFileModal = closeShiftDocumentFileModal;
		    	
		    	////////////
		    	
		    	function setTimeout() {
		      		$timeout(function() {
		      			vm.errorShiftDocumentFileStillInFolder = false;
		  			   }, 4000); 
		      	}
		    	
		    	function createTree() {
		    		vm.folderTree = folderTreeService.createTree({q:vm.folders});
		    	}
		    	
		    	function shiftToRoot() {
		    		if(documentFile.folderId !== '') {
		    			documentFile.folderId = '';
		    			invoker.updateDocumentFile(documentFile, false);
		    			invoker.removeDocumentFileOfPagedDocumentFiles(documentFile);
		    			closeShiftDocumentFileModal();
		    			vm.releaseToastMsg = ngToast.warning({
		    		  		  content: '<span class="glyphicon glyphicon-info-sign infoSign"></span> <span><b>' + documentFile.fileName + '</b> wurde in den Ordner <b>Dokumente</b> verschoben</span>',
		    		  		  timeout: 5000,
		    		  		  dismissButton: true
		    		  		});
		    		} else {
		    			vm.errorShiftDocumentFileStillInFolder = true;
		    			setTimeout();
		    		}
		    	}
		    	
		    	function shiftToFolder(folder) {
		    		if(documentFile.folderId != folder.id) {
		    			documentFile.folderId = folder.id;
		    			invoker.updateDocumentFile(documentFile, false);
		    			invoker.removeDocumentFileOfPagedDocumentFiles(documentFile);
		    			closeShiftDocumentFileModal();
		    			vm.releaseToastMsg = ngToast.warning({
		    		  		  content: '<span class="glyphicon glyphicon-info-sign infoSign"></span> <span><b>' + documentFile.fileName + '</b> wurde in den Ordner <b>' + folder.name + '</b> verschoben</span>',
		    		  		  timeout: 5000,
		    		  		  dismissButton: true
		    		  		});
		    		} else {
		    			vm.errorShiftDocumentFileStillInFolder = true;
		    			setTimeout();
		    		}
		    	}
		    	
		    	function closeShiftDocumentFileModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
