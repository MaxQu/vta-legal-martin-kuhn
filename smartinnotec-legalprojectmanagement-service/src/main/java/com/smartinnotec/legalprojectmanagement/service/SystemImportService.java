package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.service.container.AddressContainer;
import com.smartinnotec.legalprojectmanagement.service.container.ContactContainer;

public interface SystemImportService {

    List<ContactContainer> importContactCSV(final String path) throws BusinessException;

    List<AddressContainer> importAddressCSV(final String path) throws BusinessException;

}
