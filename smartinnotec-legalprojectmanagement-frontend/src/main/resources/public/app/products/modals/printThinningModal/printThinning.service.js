(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('printThinningModalService', printThinningModalService);
        
    printThinningModalService.$inject = ['$modal', '$stateParams'];
    
    function printThinningModalService($modal, $stateParams) {
		var service = {
			showPrintThinningModal: showPrintThinningModal
		};		
		return service;
		
		////////////
				
		function showPrintThinningModal(product, thinningOfProduct, currentUser, polymerPromille) {   
			 var printThinningModal = $modal.open({
				controller: PrintThinningModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	}, 
			    	thinningOfProduct: function() {
			    		return thinningOfProduct;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}, 
			    	polymerPromille: function() {
			    		return polymerPromille;
			    	}
			    }, 
				templateUrl: 'app/products/modals/printThinningModal/printThinning.modal.html'
			});
			return printThinningModal;
			
			function PrintThinningModalController($modalInstance, $scope, product, thinningOfProduct, currentUser, polymerPromille) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.thinningOfProduct = thinningOfProduct;
		    	vm.currentUser = currentUser;
		    	vm.date = new Date();
		    	vm.polymerPromille = polymerPromille;
		    	
		    	vm.closePrintThinningModal = closePrintThinningModal;
		    	
		    	////////////
		    	
		    	function closePrintThinningModal() {
		    		$modalInstance.dismiss('cancel');  
		    	}
			} 
		}
    }
})();