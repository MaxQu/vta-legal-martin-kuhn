(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.calendar')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getCalendarState());
    	 
    	////////////
			    	
    	function getCalendarState() {
    		var state = {
    			name: 'auth.calendar',
				url: '/calendar/:userId',
				templateUrl: 'app/calendar/calendar/calendar.html',
				controller: 'CalendarController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					optionsService: 'optionsService',
					projectsService: 'projectsService',
					calendarEventUserConnectionService: 'calendarEventUserConnectionService',
					calendarEvents: function getPreparedCalendarEventsOfUser($stateParams, calendarEventUserConnectionService) {
						var date = new Date();
						var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
						var month = date.getMonth()+1;
						return calendarEventUserConnectionService.getPreparedCalendarEventsOfUser($stateParams.userId, 'MONTH', firstDay.getDate(), month, firstDay.getFullYear(), false);
					},
					defaultProject: function getDefaultProject($stateParams, projectsService) {
						return projectsService.getDefaultProject($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					calendarEventSerialDateTypes: function(optionsService) {
						return optionsService.getCalendarEventSerialDateTypes();
					},
					calendarEventTypeEnums: function(optionsService) {
						return optionsService.getCalendarEventTypes();
					}
				}
    		};
    		return state;
    	}
	}
})();