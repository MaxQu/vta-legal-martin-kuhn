package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.service.container.AddressContainer;
import com.smartinnotec.legalprojectmanagement.service.container.ContactContainer;

public interface SystemImportMergeScheduleService {

    void startContactsImport() throws BusinessException;

    void importContacts(final List<ContactContainer> importedContactContainers, final List<AddressContainer> importedAddressContainers)
            throws BusinessException;

    void mergeContactsWithAddressesOfStandardPathAndSave() throws BusinessException;

    void mergeContactsWithAddressesAndSave(final String pathAddress, final String pathContact) throws BusinessException;

}
