package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.ActivityDocumentFile;

public class ActivityTest extends AbstractDAOTest {

    @Test
    public void shouldCreateProducts() {

        final String calendarEventId = "5acc5c2df1bc0840de364d5c";

        final Activity activity1 = new Activity();
        activity1.setCalendarEventId(calendarEventId);
        final DateTime creationDate1 = dateFormatter.parseDateTime("07.04.2018");
        activity1.setCreationDate(creationDate1);
        final Activity createdActivity1 = activityRepository.insert(activity1);
        Assert.assertNotNull(createdActivity1);

        final Activity activity2 = new Activity();
        activity2.setCalendarEventId(calendarEventId);
        final DateTime creationDate2 = dateFormatter.parseDateTime("09.04.2018");
        activity2.setCreationDate(creationDate2);
        final Activity createdActivity2 = activityRepository.insert(activity2);
        Assert.assertNotNull(createdActivity2);

        final Activity activity3 = new Activity();
        activity3.setCalendarEventId(calendarEventId);
        final DateTime creationDate3 = dateFormatter.parseDateTime("15.04.2018");
        activity3.setCreationDate(creationDate3);
        final Activity createdActivity3 = activityRepository.insert(activity3);
        Assert.assertNotNull(createdActivity3);

        final Activity foundedActivity = activityRepository.findFirst1ByCalendarEventIdOrderByCreationDateDesc(calendarEventId);
        Assert.assertNotNull(foundedActivity);
        Assert.assertEquals(dateFormatter.print(foundedActivity.getCreationDate()), "15.04.2018");
    }

    @Test
    public void shouldFindActivities() {

        final String calendarEventId = "5acc5c2df1bc0840de364d5c";

        final Activity activity1 = new Activity();
        activity1.setCalendarEventId(calendarEventId);
        final DateTime creationDate1 = dateFormatter.parseDateTime("07.04.2018");
        activity1.setCreationDate(creationDate1);

        final List<ActivityDocumentFile> activityDocumentFiles = new ArrayList<>();
        final ActivityDocumentFile activityDocumentFile1 = new ActivityDocumentFile();
        activityDocumentFile1.setFileName("auszug.png");
        activityDocumentFile1.setImage(false);
        activityDocumentFile1.setOriginalFileName("auszug.png");
        activityDocumentFile1.setVersion("1.0.0");

        activity1.setActivityDocumentFiles(activityDocumentFiles);
        final Activity createdActivity1 = activityRepository.insert(activity1);
        Assert.assertNotNull(createdActivity1);

        final List<Activity> activities = activityRepository.findActivitiesBySearchString("auszug");
        Assert.assertEquals(1, activities.size());
    }
}
