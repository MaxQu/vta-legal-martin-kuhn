package com.smartinnotec.legalprojectmanagement.service;

public enum UserScheduleManagementTypeEnum {

    PREVIOUS_SCHEDULE("PREVIOUS_SCHEDULE"),
    CURRENT_SCHEDULE("CURRENT_SCHEDULE"),
    FUTURE_SCHEDULE("FUTURE_SCHEDULE"),
    NO_SCHEDULE("NO_SCHEDULE");

    private String value;

    private UserScheduleManagementTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
