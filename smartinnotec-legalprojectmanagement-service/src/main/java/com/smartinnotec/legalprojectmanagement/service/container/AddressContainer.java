package com.smartinnotec.legalprojectmanagement.service.container;

import lombok.Data;

public @Data class AddressContainer {

    private String tenant; // mandant
    private String accountNumber; // ktonr
    private String flfId; // vlfid
    private String name; // LieferadresseName
    private String contactPerson; // LieferadresseAnsprechpartner
    private String street; // LieferadresseStrassee
    private String postalCode; // LieferadressePlz
    private String region; // LieferadresseOrt
    private String telephone; // LieferadresseTelefon
    private String email; // email
}
