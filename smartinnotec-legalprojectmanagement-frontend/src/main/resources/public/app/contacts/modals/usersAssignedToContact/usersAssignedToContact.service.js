(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('usersAssignedToContactModalService', usersAssignedToContactModalService);
        
    usersAssignedToContactModalService.$inject = ['$modal', '$timeout', '$stateParams', 'projectUserConnectionService'];
    
    function usersAssignedToContactModalService($modal, $timeout, $stateParams, projectUserConnectionService) {
		var service = {
			showUsersAssignedToContactModal: showUsersAssignedToContactModal
		};		
		return service;
		
		////////////
				
		function showUsersAssignedToContactModal(contact, users, projectUserConnections) {
			 var usersAssignedToContactModal = $modal.open({
				controller: UsersAssignedToContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	contact: function() {
			    		return contact;
			    	},
			    	users: function() {
			    		return users;
			    	},
			    	projectUserConnections: function() {
			    		return projectUserConnections;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/usersAssignedToContact/usersAssignedToContact.modal.html'
			});
			return usersAssignedToContactModal;
			
			function UsersAssignedToContactModalController($modalInstance, $scope, contact, users, projectUserConnections) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.users = users;
		    	vm.projectUserConnections = projectUserConnections;
		    	
		    	vm.userSelectionChanged = userSelectionChanged;
		    	vm.userStillAdded = userStillAdded;
		    	vm.uncheckUser = uncheckUser;
		    	vm.closeUsersAssignedToContactModal = closeUsersAssignedToContactModal;
		    	
		    	////////////
		    	
		    	function setTimeout(user) {
		      		$timeout(function() {
		      			user.addedUserToContactTrigger = false;
		      			user.removedUserFromContactTrigger = false;
		  			   }, 2000); 
		      	}
		    	
		    	function userSelectionChanged(user, checked) {
		    		if(checked === true) {
		    			addUserToContact(user);
		    		} else if(checked === false) {
		    			removeUserFromContact(user);
		    		}
		    	}
		    	
		    	function addUserToContact(user) {
		    		var projectUserConnection = {};
		    		projectUserConnection.contact = vm.contact;
					projectUserConnection.user = user;
					projectUserConnection.projectUserConnectionRole = 'USER';
					projectUserConnectionService.createProjectUserConnection(projectUserConnection).then(function(response) {
						vm.projectUserConnections.push(response.data);
						user.addedUserToContactTrigger = true;
						setTimeout(user);
		   	   		 }, function(response) {
		   	   			  console.log('error usersAssignedToContact.service#addUserToContact');
		   	   		 });	
		    	}
		    	
		    	function removeUserFromContact(user) {
		    		projectUserConnectionService.deleteProjectUserConnectionOverUserAndContact(user.id, vm.contact.id).then(function(response) {
		    			for(var i = 0; i < vm.projectUserConnections.length; i++) {
		    				if(vm.projectUserConnections[i].id == response.data.id) {
		    					vm.projectUserConnections.splice(i, 1);
		    					user.removedUserFromContactTrigger = true;
		    					setTimeout(user);
		    					break;
		    				}
		    			}
		   	   		 }, function(response) {
		   	   			  console.log('error usersAssignedToContact.service#removeUserFromContact');
		   	   		 });	
		    	}
		    	
		    	function userStillAdded(user) {
		    		for(var i = 0; i < vm.projectUserConnections.length; i++) {
		    			if(vm.projectUserConnections[i].user.id == user.id) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function uncheckUser(user) {
		    		for(var i = 0; i < vm.users.length; i++) {
		    			if(vm.users[i].id === user.id) {
		    				vm.users[i].checked = false;
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function closeUsersAssignedToContactModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();