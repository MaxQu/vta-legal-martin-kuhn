(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('projectsOfContactModalService', projectsOfContactModalService);
        
    projectsOfContactModalService.$inject = ['$modal', '$timeout', '$stateParams', 'productsService', 'contactsService'];
    
    function projectsOfContactModalService($modal, $timeout, $stateParams, productsService, contactsService) {
		var service = {
			showProductsOfContactModal: showProductsOfContactModal
		};		
		return service;
		
		////////////
				
		function showProductsOfContactModal(contact, currentUser, userAuthorizationUserContainer) {
			 var productsOfContactContactModal = $modal.open({
				controller: ProductsOfContactModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	contact: function() {
			    		return contact;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}, 
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/productsOfContact/productsOfContact.modal.html'
			});
			return productsOfContactContactModal;
			
			function ProductsOfContactModalController($modalInstance, $scope, contact, currentUser, userAuthorizationUserContainer) {
		    	var vm = this; 
		    	vm.contact = contact;
		    	vm.currentUser = currentUser;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	
		    	vm.getProductsByTerm = getProductsByTerm;
		    	vm.productSelected = productSelected;
		    	vm.removeProduct = removeProduct;
		    	vm.removeProductOfAddressWithProducts = removeProductOfAddressWithProducts;
		    	vm.productSelectedAtAddressWithProducts = productSelectedAtAddressWithProducts;
		    	vm.hideAddressWithProducts = hideAddressWithProducts;
		    	vm.closeProductsOfContact = closeProductsOfContact;
		    	
		    	////////////

		    	function setFileUpdateTimeout(address) {
		      		$timeout(function() {
		      			address.productStillAddedToContactError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function getProductsByTerm(term) {
		    		return productsService.findProductsByTerm(term).then(function(response) {
			    		return response.data;
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error productsOfContact.service#getProductsByTerm'); 
		  	   		 });
		    	}
		    	
		    	function productSelected(selectedProduct, address) {
		    		contactsService.addProductToContact(address.id, selectedProduct.id, vm.contact).then(function(response) {
		    			if(vm.contact.products == null) {
		    				vm.contact.products = [];
		    			}
		    			vm.contact.products.push(response.data);
		  	   		 }, function errorCallback(response) {
		  	   			 if(response.data.error == 'PRODUCT_STILL_ADDED_TO_CONTACT') {
		  	   				 address.productStillAddedToContactError = true;
		  	   				 setFileUpdateTimeout(address);
		  	   			 }
		  	   			 console.log('error productsOfContact.service#productSelected'); 
		  	   		 });
		    	}
		    	
		    	function productSelectedAtAddressWithProducts(selectedProduct, address) {
		    		contactsService.addProductToContact(address.id, selectedProduct.id, vm.contact).then(function(response) {
		    			for(var i = 0; i < vm.contact.addressesWithProducts.length; i++) {
		    				if(vm.contact.addressesWithProducts[i].address.id === address.id) {
		    					if(vm.contact.addressesWithProducts[i].products == null) {
		    						vm.contact.addressesWithProducts[i].products = [];
		    					}
		    					vm.contact.addressesWithProducts[i].products.push(response.data);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 if(response.data.error == 'PRODUCT_STILL_ADDED_TO_CONTACT') {
		  	   				 address.productStillAddedToContactError = true;
		  	   				 setFileUpdateTimeout(address);
		  	   			 }
		  	   			 console.log('error productsOfContact.service#productSelected'); 
		  	   		 });
		    	}
		    	
		    	function removeProduct(product, address) {
		    		contactsService.removeProductFromContact(address.id, product.id, vm.contact).then(function(response) {
		    			var removedProduct = response.data;
		    			for(var i = 0; i < vm.contact.products.length; i++) {
		    				if(vm.contact.products[i].id == removedProduct.id) {
		    					vm.contact.products.splice(i, 1);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error productsOfContact.service#removeProduct'); 
		  	   		 });
		    	}
		    	
		    	function removeProductOfAddressWithProducts(product, address) {
		    		contactsService.removeProductFromContact(address.id, product.id, vm.contact).then(function(response) {
		    			var removedProduct = response.data;
		    			for(var i = 0; i < vm.contact.addressesWithProducts.length; i++) {
		    				if(vm.contact.addressesWithProducts[i].address.id === address.id) {
		    					vm.contact.addressesWithProducts[i].products.splice(i, 1);
		    					break;
		    				}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error productsOfContact.service#removeProductOfAddressWithProducts'); 
		  	   		 });
		    	}
		    	
		    	function hideAddressWithProducts(addressWithProducts) {
					var address = addressWithProducts.address;
					if(address == null) {
						return false;
					}
					if(address.userIdBlackList != null && address.userIdBlackList.indexOf(vm.currentUser.id) !== -1) {
						return true;
					}
					if(address.rolesBlackList != null && address.rolesBlackList.indexOf(vm.currentUser.projectUserConnectionRole) !== -1) {
						return true;
					}
					return false;
				}
		    	
		    	function closeProductsOfContact() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();