(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('yesNoDeactivateContactModalService', yesNoDeactivateContactModalService);
        
    yesNoDeactivateContactModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoDeactivateContactModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, contact, state) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contact: function() {
			    		return contact;
			    	},
			    	state: function() {
			    		return state;
			    	}
			    }, 
				templateUrl: 'app/contacts/modals/yesNoDeactivateModal/yesNoDeactivate.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, contact, state) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contact = contact;
		    	vm.state = state;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		vm.invoker.deactivateContact(vm.contact, vm.state);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();