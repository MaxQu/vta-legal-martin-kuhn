(function() { 
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.controller('AdministrationController', AdministrationController);
    
    AdministrationController.$inject = ['$scope', '$timeout', 'currentUser', 'countryTypes', 'allLabelsPredefined', 'labelsPredefinedService', 'accessControlService', 'userRoles', 'allUsers', 'projectUserConnectionRoles', 'userAuthorizationUserContainer', 'calendarEventScheduleDays', 'projects', 'defaultProject', 'userService', 'calendarEventScheduleService', 'historyService', 'yesNoAdministrationModalService', 'calendarEventScheduleDaysService', 'userDuplicationService', 'authUserService', 'administrationService', 'dateUtilityService', 'optionsService', 'systemImportService', 'uploadService', 'contactsImportedService', 'contactsService', 'contactsImportedYesNoModalService', 'contactsYesNoModalService'];
       
    function AdministrationController($scope, $timeout, currentUser, countryTypes, allLabelsPredefined, labelsPredefinedService, accessControlService, userRoles, allUsers, projectUserConnectionRoles, userAuthorizationUserContainer, calendarEventScheduleDays, projects, defaultProject, userService, calendarEventScheduleService, historyService, yesNoAdministrationModalService, calendarEventScheduleDaysService, userDuplicationService, authUserService, administrationService, dateUtilityService, optionsService, systemImportService, uploadService, contactsImportedService, contactsService, contactsImportedYesNoModalService, contactsYesNoModalService) {
	    $scope.vm = this; 
    	var vm = this;
    	  
    	vm.allLabelsPredefined = allLabelsPredefined.data;
    	vm.userRoles = userRoles.data; 
    	vm.countryTypes = countryTypes.data;
    	vm.currentUser = currentUser;
    	vm.allUsers = allUsers.data;
    	vm.projectUserConnectionRoles = projectUserConnectionRoles.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.calendarEventScheduleDays = calendarEventScheduleDays.data;
    	vm.projects = projects.data;
    	vm.defaultProject = defaultProject.data;
    	vm.selectedProject = vm.projects.length == 1 ? vm.projects[0] : (vm.defaultProject != null ? vm.defaultProject : null);
    	vm.targetProject = null;
    	vm.newLabel = null;
    	vm.labelStillExistsError = false;
    	vm.foundedUsers = null;
    	vm.sendCalendarEventRememberingMessageTrigger = false;  
    	vm.userAuthenticationUserContainerTrigger = false;
    	vm.sendHistoryMessageTrigger = false; 
    	vm.sendCalendarEventOutsideWorkRememberingMessageTrigger = false;
    	vm.labelsPredefinedType = null;
    	vm.labelsPredefinedTypeError = false;
    	vm.calendarEventScheduleDayUpdated = false;
    	vm.orderType = 'labelsPredefinedType';
    	vm.duplicateUserFrom = null;
    	vm.duplicateUserTo = null;
    	vm.duplicateUserFromSelectionError = false;
    	vm.duplicateUserToSelectionError = false;
    	vm.userDuplicationSuccess = false;
    	vm.userDuplicationError = false;
    	vm.duplicationSecurity = false;
    	vm.userRegistered = false;
    	vm.testEmailSent = false;
    	vm.mergeContactsWithAddressesAndSaveTrigger = false;
    	vm.mergeContactsWithAddressesAndSaveOfStandardPathTrigger = false;
    	vm.pathAddress = '';
    	vm.pathContact = '';
    	vm.systemImportNoPathAddressError = false; 
    	vm.systemImportNoPathContactError = false;
    	vm.generalSystemImportError = false;
    	vm.systemImportContactFileNotFoundError = false;
    	vm.systemImportAddressFileNotFoundError = false;
    	vm.mergeContactsWithAddressesAndSaveSuccess = false;
    	vm.systemImportContactFileNotReadableError = false;
    	vm.systemImportAddressFileNotReadableError = false;
    	vm.contactFiles = null;
    	vm.addressFiles = null;
    	vm.pathContactSet = false;
    	vm.pathAddressSet = false;
    	vm.contactsImportedDeleted = false;
    	vm.contactsDeleted = false;
    	vm.amountOfContactsImported = null;
    	vm.amountOfContacts = null;
    	vm.amountOfCustomerContacts = null;
    	vm.selectedCountry = null;

    	vm.addNewLabel = addNewLabel;
    	vm.labelStillExists = labelStillExists;
    	vm.setLabelCreatedTimeout = setLabelCreatedTimeout;
    	vm.deleteLabel = deleteLabel;
    	vm.searchUsersByTerm = searchUsersByTerm;
    	vm.sendCalendarEventRememberingMessage = sendCalendarEventRememberingMessage;
    	vm.updateUser = updateUser;
    	vm.deleteUser = deleteUser;
    	vm.sendHistoryMessages = sendHistoryMessages;
    	vm.updatePassword = updatePassword;
    	vm.saveUserAuthorizationUserContainer = saveUserAuthorizationUserContainer;
    	vm.updateUserAuthorizationUserContainer = updateUserAuthorizationUserContainer;
    	vm.deleteUserAuthorizationUserContainer = deleteUserAuthorizationUserContainer;
    	vm.showYesNoAdministrationModalService = showYesNoAdministrationModalService;
    	vm.addCalendarEventScheduleDay = addCalendarEventScheduleDay;
    	vm.addCalendarEventScheduleDay = addCalendarEventScheduleDay;
    	vm.updateCalendarEventScheduleDay = updateCalendarEventScheduleDay;
    	vm.removeCalendarEventScheduleDay = removeCalendarEventScheduleDay;
    	vm.addSelectedProject = addSelectedProject;
    	vm.removeSelectedProject = removeSelectedProject;
    	vm.sendCalendarEventOutsideWorkRememberingMessages = sendCalendarEventOutsideWorkRememberingMessages;
    	vm.addTargetProject = addTargetProject;
    	vm.removeTargetProject = removeTargetProject;
    	vm.duplicateUser = duplicateUser;
    	vm.startListening = startListening;
    	vm.stopListening = stopListening;
    	vm.addAgentNumberContainer = addAgentNumberContainer;
    	vm.removeAgentNumberContainer = removeAgentNumberContainer;
    	vm.showDeleteAllContactsImported = showDeleteAllContactsImported;
    	vm.deleteAllContactsImported = deleteAllContactsImported;
    	vm.showDeleteAllCustomerContacts = showDeleteAllCustomerContacts;
    	vm.deleteAllCustomerContacts = deleteAllCustomerContacts;
    	vm.signup = signup;
    	vm.sendTestEmail = sendTestEmail;
    	vm.mergeContactsWithAddressesAndSave = mergeContactsWithAddressesAndSave;
    	vm.mergeContactsWithAddressesAndSaveOfStandardPath = mergeContactsWithAddressesAndSaveOfStandardPath;
    	getAmountOfContacts();
    	getAmountOfCustomerContacts();
    	getAmountOfContactsImported();
    	
    	////////////
    	
    	$scope.$watch('vm.contactFiles', function () {
    		uploadService.uploadSystemImportFile(vm.contactFiles, vm.currentUser.id, 'CONTACT_FILE', successCallback, errorCallback, progressCallback);
        });
    	
    	$scope.$watch('vm.addressFiles', function () {
    		uploadService.uploadSystemImportFile(vm.addressFiles, vm.currentUser.id, 'ADDRESS_FILE', successCallback, errorCallback, progressCallback);
        });
    	
    	function successCallback(response) {
    		if(response.data.pathContact != null) {
    			vm.pathContact = response.data.pathContact;
    			vm.pathContactSet = true;
    		}
    		if(response.data.pathAddress != null) {
    			vm.pathAddress = response.data.pathAddress;
    			vm.pathAddressSet = true;
    		}
    		systemImportTimeout();
    	}
    	
    	function errorCallback(response) {
    		console.log('errorCallback: ' + response);
    	}
    	
    	function progressCallback(response) {
    		console.log('progressCallback: ' + response);
    	}
    	
    	function getAmountOfContacts() {
    		contactsService.countByTenantAndActive(vm.currentUser.id).then(function(response) {
				vm.amountOfContacts = response.data;
	   		 }, function errorCallback(response) {
	   			  console.log('error administration.controller#getAmountOfContacts');
	   		 });
    	}
    	
    	function getAmountOfCustomerContacts() {
    		contactsService.countCustomerContactsByTenantAndActive(vm.currentUser.id).then(function(response) {
				vm.amountOfCustomerContacts = response.data;
	   		 }, function errorCallback(response) {
	   			  console.log('error administration.controller#getAmountsOfCustomerContacts');
	   		 });
    	}
    	
    	function getAmountOfContactsImported() {
			contactsImportedService.getAmountOfContactsImported().then(function(response) {
				vm.amountOfContactsImported = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#amountOfContactsImported');
 	   		 });
		}
    	
    	function showDeleteAllContactsImported() {
    		contactsImportedYesNoModalService.showContactsImportedYesNoModal(vm);
    	}
    	
    	function deleteAllContactsImported() {
    		contactsImportedService.deleteAllContactsImported().then(function(response) {
    			vm.contactsImportedDeleted = true;
    			systemImportTimeout();
    			getAmountOfContactsImported();
    			getAmountOfCustomerContacts();
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#deleteAllContactsImported');
 	   		 });
    	}
    	
    	function showDeleteAllCustomerContacts() {
    		contactsYesNoModalService.showContactsYesNoModal(vm, vm.amountOfCustomerContacts, vm.selectedCountry);
    	}
    	
    	function deleteAllCustomerContacts() {
    		contactsService.deleteAllCustomerContacts(vm.selectedCountry).then(function(response) {
    			vm.contactsDeleted = true;
    			systemImportTimeout();
    			getAmountOfContacts();
    			getAmountOfCustomerContacts();
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#deleteAllContacts');
 	   		 });
    	}
    	
    	function setSendCalendarEventRememberingMessage() {
    		$timeout(function() {
    			vm.sendCalendarEventRememberingMessageTrigger = false;
    			vm.sendHistoryMessageTrigger = false;
    			vm.sendCalendarEventOutsideWorkRememberingMessageTrigger = false;
			   }, 2000); 
    	}
    	
    	function setLabelCreatedTimeout() {
    		$timeout(function() {
				vm.createLabelClicked = false;
			   }, 2000); 
    	}
    	
    	function systemImportTimeout() {
    		$timeout(function() {
    			vm.systemImportNoPathAddressError = false; 
    	    	vm.systemImportNoPathContactError = false;
    	    	vm.generalSystemImportError = false;
    	    	vm.systemImportContactFileNotFoundError = false;
    	    	vm.systemImportAddressFileNotFoundError = false;
    	    	vm.pathContactSet = false;
    	    	vm.pathAddressSet = false;
    	    	vm.contactsImportedDeleted = false;
    	    	vm.contactsDeleted = false;
    	    	vm.mergeContactsWithAddressesAndSaveSuccess = false;
    	    	vm.systemImportContactFileNotReadableError = false;
    	    	vm.systemImportAddressFileNotReadableError = false;
    	    	vm.mergeContactsWithAddressesAndSaveOfStandardPathTrigger = false;
    	    	vm.systemImportContactFileOfStandardPathNotFoundError = false;
			   }, 3000); 
    	}
    	
    	function setUserDeletedTimeout(user) {
    		$timeout(function() {
    			user.userDeleted = false;
			   }, 2000); 
    	}
    	
    	function setUserRegisteredTimeout() {
    		$timeout(function() {
    			vm.userRegistered = false;
			   }, 2000); 
    	}
    	
    	function testEmailSentTimeout() {
    		$timeout(function() {
    			vm.testEmailSent = false;
			   }, 2000); 
    	}
    	
    	function setCalendarEventScheduleDayUpdated(calendarEventScheduleDay) {
    		$timeout(function() {
    			calendarEventScheduleDay.calendarEventScheduleDayUpdated = false;
			   }, 2000); 
    	}
    	
    	function setUserAuthenticationUserContainerTimeout() {
    		$timeout(function() {
				vm.userAuthenticationUserContainerTrigger = false;
			   }, 2000); 
    	}
    	
    	function setDuplicationTimeout() {
    		$timeout(function() {
    			vm.userDuplicationSuccess = false;
    	    	vm.userDuplicationError = false;
			   }, 6000); 
    	}
    	
    	function setAccessControlTimeout() {
    		$timeout(function() {
    			vm.accessControlStarted = false;
    			vm.accessControlStopped = false;
			   }, 2000); 
    	}
    	
    	function addCalendarEventScheduleDay() {
    		var calendarEventScheduleDay = {};
    		calendarEventScheduleDay.days = 1;
    		calendarEventScheduleDay.tenant = currentUser.tenant;
    		calendarEventScheduleDaysService.create(calendarEventScheduleDay).then(function(response) {
    			vm.calendarEventScheduleDays.push(response.data);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#addCalendarEventScheduleDay');
  	   		 });
    	}
    	
    	function updateCalendarEventScheduleDay(calendarEventScheduleDay) {
    		calendarEventScheduleDaysService.update(calendarEventScheduleDay).then(function(response) {
    			calendarEventScheduleDay.calendarEventScheduleDayUpdated = true;
    			setCalendarEventScheduleDayUpdated(calendarEventScheduleDay);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#updateCalendarEventScheduleDay');
  	   		 });
    	}
    	
    	function removeCalendarEventScheduleDay(calendarEventScheduleDay) {
    		calendarEventScheduleDaysService.deleteCalendarEventScheduleDays(calendarEventScheduleDay.id).then(function(response) {
    			for(var i = 0; i < vm.calendarEventScheduleDays.length; i++) {
    				if(vm.calendarEventScheduleDays[i].id == calendarEventScheduleDay.id) {
    					vm.calendarEventScheduleDays.splice(i, 1);
    				}
    			}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#removeCalendarEventScheduleDay');
  	   		 });
    	}
    	
    	function sendHistoryMessages() {
    		historyService.sendHistoryMessages();
    		vm.sendHistoryMessageTrigger = true;
    		setSendCalendarEventRememberingMessage();
    	}
    	
    	function sendCalendarEventRememberingMessage() {
    		calendarEventScheduleService.sendCalendarEventRememberingMessage().then(function(response) {
    			vm.sendCalendarEventRememberingMessageTrigger = true;
    			setSendCalendarEventRememberingMessage();
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#sendCalendarEventRememberingMessage');
  	   		 });
    	}
    	
    	function sendCalendarEventOutsideWorkRememberingMessages() {
    		calendarEventScheduleService.sendCalendarEventOutsideWorkRememberingMessage().then(function(response) {
    			vm.sendCalendarEventOutsideWorkRememberingMessageTrigger = true;
    			setSendCalendarEventRememberingMessage();
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#sendCalendarEventOutsideWorkRememberingMessages');
  	   		 });
    	}
    	
    	function updateUser(user, type) {
			userService.updateUser(user).success(function(data) {
				setUpdateModeType(user, type, 'SUCCESS');
				setUpdateUserTimeout(user);
			}).error(function(data) { 
				setUpdateModeType(user, type, 'ERROR');
				setUpdateUserTimeout(user);
				console.log('error in administration.controller.js#updateUser: ' + data);
			});
		}
    	
    	function showYesNoAdministrationModalService(user) {
    		yesNoAdministrationModalService.showYesNoModal(vm, user);
    	}
    	
    	function deleteUser(user) {
    		userService.deleteUser(user).success(function(data) {
    			user.userDeleted = true;
    			user.userDeletionError = false;
    			setUserDeletedTimeout(user);
    			removeUser(user);
			}).error(function(data) {
				user.userDeletionError = true;
				user.userDeletionErrorMessage = data.message;
			});
		}
    	
    	function removeUser(user) {
    		for(var i = 0; i < vm.foundedUsers.length; i++) {
    			if(vm.foundedUsers[i].id == user.id) {
    				vm.foundedUsers.splice(i, 1);
    				break;
    			}
    		}
    	}
    	
    	function updatePassword(user, type) {
			userService.updatePassword(user.id, user.password).success(function(data) {
				setUpdateModeType(user, type, 'SUCCESS');
				setUpdateUserTimeout(user);
			}).error(function(data) {
				setUpdateModeType(user, type, 'ERROR');
				setUpdateUserTimeout(user);
				console.log('error in administration.controller.js#updatePassword: ' + data);
			});
		}
    	
    	function setUpdateUserTimeout(user) {
      		$timeout(function() {
      			user.nameSuccess = null;
      			user.countrySuccess = null;
      			user.locationSuccess = null;
      			user.streetSuccess = null;
      			user.telephoneSuccess = null;
      			user.emailSuccess = null;
      			user.birthdaySuccess = null;
      			user.emailActiveSuccess = null;
      			user.userActiveSuccess = null;
      			user.userRoleSuccess = null;
      			user.usernameSuccess = null;
      			user.passwordSuccess = null;
      			user.roleSuccess = null;
      			
      			user.nameError = null;
      			user.countryError = null;
      			user.locationError = null;
      			user.streetError = null;
      			user.telephoneError = null;
      			user.emailError = null;
      			user.birthdayError = null;
      			user.emailActiveError = null;
      			user.userRoleError = null;
      			user.usernameError = null;
      			user.passwordError = null;
      			user.roleError = null;
      			user.agentNumberContainersSuccess = null;
      			user.agentNumberContainersError = null;
      			user.allContactsAvailableSuccess = null;
      			user.allContactsAvailableError = null;
  			   }, 2000); 
      	}
    	
    	function setUpdateModeType(user, type, mode) {
			switch(type) {
			case 'name':
				if(mode == 'SUCCESS') {
					user.nameSuccess = true;
				} else if(mode == 'ERROR') {
					user.nameError = true;
				}
				break;
			case 'country':
				if(mode == 'SUCCESS') {
					user.countrySuccess = true;
				} else if(mode == 'ERROR') {
					user.countryError = true;
				}
				break;
			case 'location':
				if(mode == 'SUCCESS') {
					user.locationSuccess = true;
				} else if(mode == 'ERROR') {
					user.locationError = true;
				}
				break;
			case 'street':
				if(mode == 'SUCCESS') {
					user.streetSuccess = true;
				} else if(mode == 'ERROR') {
					user.streetError = true;
				}
				break;
			case 'telephone':
				if(mode == 'SUCCESS') {
					user.telephoneSuccess = true;
				} else if(mode == 'ERROR') {
					user.telephoneError = true;
				}
				break;
			case 'email': 
				if(mode == 'SUCCESS') {
					user.emailSuccess = true;
				} else if(mode == 'ERROR') {
					user.emailError = true;
				}
				break;
			case 'birthday':
				if(mode == 'SUCCESS') {
					user.birthdaySuccess = true;
				} else if(mode == 'ERROR') {
					user.birthdayError = true;
				}
				break;
			case 'emailActive':
				if(mode == 'SUCCESS') {
					user.emailActiveSuccess = true;
				} else if(mode == 'ERROR') {
					user.emailActiveError = true;
				}
				break;
			case 'userActive':
				if(mode == 'SUCCESS') {
					user.userActiveSuccess = true;
				} else if(mode == 'ERROR') {
					user.userActiveError = true;
				}
				break;
			case 'userRole':
				if(mode == 'SUCCESS') {
					user.userRoleSuccess = true;
				} else if(mode == 'ERROR') {
					user.userRoleError = true;
				}
				break;
			case 'username':
				if(mode == 'SUCCESS') {
					user.usernameSuccess = true;
				} else if(mode == 'ERROR') {
					user.usernameError = true;
				}
				break;
			case 'password':
				if(mode == 'SUCCESS') {
					user.passwordSuccess = true;
				} else if(mode == 'ERROR') {
					user.passwordError = true;
				}
				break;
			case 'role':
				if(mode == 'SUCCESS') {
					user.roleSuccess = true;
				} else if(mode == 'ERROR') {
					user.roleError = true;
				}
				break;
			case 'agentNumberContainers':
				if(mode == 'SUCCESS') {
					user.agentNumberContainersSuccess = true;
				} else if(mode == 'ERROR') {
					user.agentNumberContainersError = true;
				}
				break;
			case 'allContactsAvailable':
				if(mode == 'SUCCESS') {
					user.allContactsAvailableSuccess = true;
				} else if(mode == 'ERROR') {
					user.allContactsAvailableError = true;
				}
				break;
			}
		}
    	
    	function addNewLabel(newLabel) {
    		vm.labelStillExistsError = false;
    		vm.labelsPredefinedTypeError = false;
    		if(labelStillExists(newLabel)) {
    			return;
    		}
    		if(vm.labelsPredefinedType == null || vm.labelsPredefinedType === false) {
    			vm.labelsPredefinedTypeError = true;
    			return;  
    		}
    		var labelPredefined = {};
    		labelPredefined.name = newLabel;
    		labelPredefined.labelsPredefinedType = vm.labelsPredefinedType;
    		labelsPredefinedService.createLabelPredefined(labelPredefined).then(function(response) {
    			vm.allLabelsPredefined.push(response.data);
    			vm.newLabel = null;
    			vm.createLabelClicked = true;
    			setLabelCreatedTimeout();
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#addNewLabel');
  	   		 });
    	}
    	
    	function labelStillExists(newLabel) {
    		vm.labelStillExistsError = false;
    		for(var i = 0; i < vm.allLabelsPredefined.length; i++) {
    			if(vm.allLabelsPredefined[i].name == newLabel) {
    				vm.allLabelsPredefined[i].stillExists = true;
    				vm.labelStillExistsError = true;
    			} else {
    				vm.allLabelsPredefined[i].stillExists = false;
    			}
    		}
    		return vm.labelStillExistsError;
    	}
    	
    	function deleteLabel(label) {
    		labelsPredefinedService.deleteLabelPredefined(label.id).then(function(response) {
    			removeLabelPredefined(label);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#deleteLabel');
  	   		 });
    	}
    	
    	function removeLabelPredefined(labelPredefined) {
    		for(var i = 0; i < vm.allLabelsPredefined.length; i++) {
    			if(vm.allLabelsPredefined[i].id == labelPredefined.id) {
    				vm.allLabelsPredefined.splice(i, 1);
    			}
    		}
    	}
    	
    	function searchUsersByTerm(term) {
    		if(typeof term == 'undefined' || term == null || term.length < 2) {
    			vm.foundedUsers = null;
    			return;
    		}
    		userService.findUsersByTerm(term).then(function(response) {
    			vm.foundedUsers = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#searchUsersByTerm');
 	   		 });
    	}
    	
    	function addSelectedProject(project) {
    		removeProjectSelection();
    		project.selected = true;
    		vm.selectedProject = project;
    	}
    	
    	function removeSelectedProject(project) {
    		project.selected = false;
    		vm.selectedProject = null;
    		removeProjectSelection();
    	}
    	
    	function addTargetProject(project) {
    		vm.targetProject = project;
    	}
    	
    	function removeTargetProject(project) {
    		vm.targetProject = null;
    	}
    	
    	function removeProjectSelection() {
    		for(var i = 0; i < vm.projects.length; i++) {
    			vm.projects[i].selected = false;
    		}
    	}
    	
    	function saveUserAuthorizationUserContainer(user, userAuthorizationUserContainer, userAuthorization) {
    		userAuthorization.state = !userAuthorization.state;
    		userService.updateUserAuthorizationUserContainerOfUser(userAuthorizationUserContainer).then(function(response) {
    			vm.userAuthenticationUserContainerTrigger = true;
    			setUserAuthenticationUserContainerTimeout();
    			userAuthorizationUserContainer = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#deleteLabel');
 	   		 });
    	}
    	
    	function updateUserAuthorizationUserContainer(user, projectUserConnectionRole) {
    		userService.createUserAuthorizationUserContainer(user.id, projectUserConnectionRole).then(function(response) {
    			var userAuthorizationUserContainer = response.data;
    			user.userAuthorizationUserContainer = userAuthorizationUserContainer;
    			loadCurrentUser(user);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#updateUserAuthorizationUserContainer');
 	   		 });
    	}
    	
    	function deleteUserAuthorizationUserContainer(user, userAuthorizationUserContainer) {
    		userService.deleteUserAuthorizationUserContainerOfUser(user.id, userAuthorizationUserContainer.id).then(function(response) {
    			user.userAuthorizationUserContainer = null;
    			loadCurrentUser(user);
 	   		 }, function errorCallback(response) {
 	   			  console.log('error administration.controller#deleteUserAuthorizationUserContainer');
 	   		 });
    	}
    	
    	function loadCurrentUser(user) {
    		userService.getUserById(user.id).then(function(response) {
    			user = response.data;
    			updateUserInUsers(user);
	   		 }, function errorCallback(response) {
	   			  console.log('error administration.controller#loadCurrentUser');
	   		 });
    	}
    	
    	function updateUserInUsers(user) {
    		for(var i = 0; i < vm.allUsers.length; i++) {
    			if(vm.allUsers[i].id == user.id) {
    				vm.allUsers[i] = user;
    			}
    		}
    	}
    	
    	function duplicateUser() {
    		if(vm.duplicateUserFrom == null) {
    			vm.duplicateUserFromSelectionError = true;
    			return;
    		}
    		vm.duplicateUserFromSelectionError = false;
    		if(vm.duplicateUserTo == null) {
    			vm.duplicateUserToSelectionError = true;
    			return;
    		}
    		vm.duplicateUserToSelectionError = false;
    		if(vm.duplicateUserFrom.id == vm.duplicateUserTo.id) {
    			vm.duplicateUserFromSelectionError = true;
    			vm.duplicateUserToSelectionError = true;
    			return;
    		}
    		vm.duplicateUserFromSelectionError = false;
    		vm.duplicateUserToSelectionError = false;
    		 
    		userDuplicationService.duplicateUser(vm.duplicateUserFrom.id, vm.duplicateUserTo.id).then(function(response) {
    			vm.userDuplicationSuccess = true;
    			vm.userDuplicationError = false;
    			vm.duplicationSecurity = false;
    			setDuplicationTimeout();
	   		 }, function errorCallback(response) {
	   			vm.userDuplicationSuccess = false;
	   			vm.userDuplicationError = true;
	   			console.log('error administration.controller#duplicateUser'); 
	   		 });
    	}
    	
    	function startListening() {
    		accessControlService.startListening().then(function(response) {
    			vm.accessControlStarted = true;
    			setAccessControlTimeout();
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#startListening');
	   		 });
    	}
    	
    	function stopListening() {
    		accessControlService.stopListening().then(function(response) {
    			vm.accessControlStopped = true;
    			setAccessControlTimeout();
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#stopListening');
	   		 });
    	}
    	
    	function findAllUsers() {
    		userService.findAllUsers().then(function(response) {
    			vm.allUsers = response.data;
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#findAllUsers');
	   		 });
    	}
    	
    	function signup(newUser, callback, errorCallback) {   
			newUser.active = true;
			authUserService.signup(newUser).then(function () {
				vm.userRegistered = true;
				findAllUsers();
				setUserRegisteredTimeout();
				callback(newUser);
			}).catch(function (response) {
				vm.signupError = response.data.errorCodeType;
				console.log("error administration.controller#signup: " + response.data.message);
				errorCallback(response);
			});
		}
    	
    	function addAgentNumberContainer(user) {
    		if(user.agentNumberContainers == null) {
    			user.agentNumberContainers = [];
    		}
    		var agentNumberContainer = {};
    		agentNumberContainer.customerNumber = '';
    		user.agentNumberContainers.push(agentNumberContainer);
    	}
    	
    	function removeAgentNumberContainer(user, index) {
    		user.agentNumberContainers.splice(index, 1);
    		updateUser(user, 'agentNumberContainers');
    	}
    	
    	function sendTestEmail(testEmailAddress) {
    		administrationService.sendTestEmail(testEmailAddress).then(function(response) {
    			vm.testEmailSent = true;
    			testEmailSentTimeout();
    			console.log('success sendTestEmail');
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#sendTestEmail');
	   		 });
    	}
    	
    	function mergeContactsWithAddressesAndSave(pathAddress, pathContact) {
    		vm.mergeContactsWithAddressesAndSaveTrigger = true;
    		systemImportService.mergeContactsWithAddressesAndSave(pathAddress, pathContact, mergeContactsWithAddressesAndSaveCallback, errorMergeContactsWithAddressesAndSaveCallback);
    	}
    	
    	function mergeContactsWithAddressesAndSaveOfStandardPath() {
    		vm.mergeContactsWithAddressesAndSaveOfStandardPathTrigger = true;
    		systemImportService.mergeContactsWithAddressesAndSaveOfStandardPath().then(function(response) {
    			vm.mergeContactsWithAddressesAndSaveOfStandardPathTrigger = false;
	   		 }, function errorCallback(response) {
	   			console.log('error administration.controller#mergeContactsWithAddressesAndSaveOfStandardPath');
	   			if(response.data.error === 'CONTACT_FILE_NOT_FOUND') {
	    			vm.systemImportContactFileOfStandardPathNotFoundError = true;
	    			systemImportTimeout();
	   			}
	   		 });
    	}
    	
    	function mergeContactsWithAddressesAndSaveCallback(response) {
    		vm.mergeContactsWithAddressesAndSaveTrigger = false;
    		vm.mergeContactsWithAddressesAndSaveSuccess = true;
    		systemImportTimeout();
    		getAmountOfContactsImported();
    	}
    	
    	function errorMergeContactsWithAddressesAndSaveCallback(response) {
    		if(response.data.error === 'NO_PATH_ADDRESS') {
    			vm.systemImportNoPathAddressError = true; 
    		} else if(response.data.error === 'NO_PATH_CONTACT') {
    			vm.systemImportNoPathContactError = true;
    		} else if(response.data.error === 'CONTACT_FILE_NOT_FOUND') {
    			vm.systemImportContactFileNotFoundError = true;
    		} else if(response.data.error === 'ADDRESS_FILE_NOT_FOUND') {
    			vm.systemImportAddressFileNotFoundError = true;
    		} else if(response.data.error === 'CONTACT_FILE_NOT_READABLE') {
    			vm.systemImportContactFileNotReadableError = true;
    		} else if(response.data.error === 'ADDRESS_FILE_NOT_READABLE') {
    			vm.systemImportAddressFileNotReadableError = true;
    		} else {
    			vm.generalSystemImportError = true;
    		}
    		systemImportTimeout();
    		vm.mergeContactsWithAddressesAndSaveTrigger = false;
    	}    	
	} 
})();