(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();