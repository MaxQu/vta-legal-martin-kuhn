package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Folder;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.FolderRepository;
import com.smartinnotec.legalprojectmanagement.service.container.BreadCrumbContainer;

@Service("folderService")
public class FolderServiceImpl extends AbstractService implements FolderService {

    @Autowired
    private FolderRepository folderRepository;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;
    @Autowired
    private SearchService searchService;

    @Override
    public Folder create(final Folder folder) throws BusinessException {
        BusinessAssert.notNull(folder, "folder is mandatory in FolderServiceImpl#create", "400");
        logger.info("create folder in FolderServiceImpl#create");
        folder.setCreationDate(new DateTime());
        return folderRepository.insert(folder);
    }

    @Override
    public Folder update(final Folder folder) throws BusinessException {
        BusinessAssert.notNull(folder, "folder is mandatory in FolderServiceImpl#update", "400");
        logger.info("update folder in FolderServiceImpl#update");
        return folderRepository.save(folder);
    }

    @Override
    public Folder findById(String folderId) throws BusinessException {
        BusinessAssert.notNull(folderId, "folderId is mandatory in FolderServiceImpl#findById", "400");
        BusinessAssert.isId(folderId, "folderId is mandatory in FolderServiceImpl#findById", "400");
        logger.info("find folder by id '{}' in FolderServiceImpl#findById", folderId);
        return folderRepository.findOne(folderId);
    }

    @Override
    public List<Folder> findProjectFoldersBySearchString(final User user, String searchString) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in FolderServiceImpl#findFoldersBySearchString", "400");
        logger.info("find folders by searchString '{}' in FolderServiceImpl#findFoldersBySearchString", searchString);

        // get projects of user
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);
        final List<String> projectIds = new ArrayList<>();
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            projectIds.add(projectUserConnection.getProject().getId());
        }
        // find documentFiles of project
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        final List<Folder> foundedFolders = new ArrayList<>();
        for (final String projectId : projectIds) {
            if (combineSearchStringWithAnd) {
                searchString = searchString.replaceAll("\\+", "");
                final List<Folder> folders = folderRepository.findBySearchStringAndProjectId(searchString, projectId);
                for (final Iterator<Folder> iterator = folders.iterator(); iterator.hasNext();) {
                    final Folder folder = iterator.next();
                    final String folderProperties = folder.getName() + folder.getDescription();
                    if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(folderProperties.toLowerCase()::contains)) {
                        iterator.remove();
                    }
                }
                foundedFolders.addAll(folders);
            } else {
                final List<Folder> folders = folderRepository.findBySearchStringAndProjectId(searchString, projectId);
                foundedFolders.addAll(folders);
            }
        }
        return foundedFolders;
    }

    @Override
    public List<Folder> findProductFoldersBySearchString(final User user, String searchString) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in FolderServiceImpl#findProductFoldersBySearchString", "400");
        logger.info("find folders by searchString '{}' in FolderServiceImpl#findProductFoldersBySearchString", searchString);
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        if (combineSearchStringWithAnd) {
            searchString = searchString.replaceAll("\\+", "");
        }
        final List<Folder> folders = folderRepository.findBySearchString(searchString);
        final List<Folder> foundedProductFolders = new ArrayList<>();
        for (final Iterator<Folder> iterator = folders.iterator(); iterator.hasNext();) {
            final Folder folder = iterator.next();
            if (combineSearchStringWithAnd) {
                final String folderProperties = folder.getName() + folder.getDescription();
                if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(folderProperties.toLowerCase()::contains)) {
                    iterator.remove();
                }
            }
            final String projectProductId = folder.getProjectId();
            final Product product = productService.findProduct(projectProductId);
            if (product != null) {
                foundedProductFolders.add(folder);
            }
        }
        return foundedProductFolders;
    }

    @Override
    public List<Folder> findAllFoldersOfProject(final String projectId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in FolderServiceImpl#findAllFoldersOfProject", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in FolderServiceImpl#findAllFoldersOfProject", "400");
        logger.info("find all folders of project with id '{}' in FolderServiceImpl#findFoldersBySearchString", projectId);
        return folderRepository.findByProjectId(projectId);
    }

    @Override
    public List<Folder> findByNameAndSuperFolderId(final String name, final String superFolderId) throws BusinessException {
        BusinessAssert.notNull(name, "name is mandatory in FolderServiceImpl#findByNameAndSuperFolderId", "400");
        BusinessAssert.notNull(superFolderId, "superFolderId is mandatory in FolderServiceImpl#findByNameAndSuperFolderId", "400");
        logger.info("final folder by name '{}' and superFolder with id '{}' in FolderServiceImpl#findByNameAndSuperFolderId", name,
            superFolderId);
        return folderRepository.findByNameAndSuperFolderId(name, superFolderId);
    }

    @Override
    public BreadCrumbContainer findBreadCrumbOfFolder(final String folderId) throws BusinessException {
        BusinessAssert.notNull(folderId, "folderId is mandatory in FolderServiceImpl#findBreadCrumbOfFolder", "400");
        logger.info("find breadCrumb of folder with id '{}' in FolderServiceImpl#findBreadCrumbOfFolder");

        final BreadCrumbContainer breadCrumbContainer = new BreadCrumbContainer();
        if (folderId.equals("")) {
            return breadCrumbContainer;
        }
        final List<Folder> breadCrumbFolders = new ArrayList<>();
        final Folder tailFolder = findById(folderId);
        if (tailFolder == null) {
            breadCrumbContainer.setBreadCrumbOfFolders(breadCrumbFolders);
            return breadCrumbContainer;
        }
        tailFolder.setReverseSortOrderNumber(1);
        breadCrumbFolders.add(tailFolder);
        Folder runningFolder = tailFolder;
        int sortOrder = 2;
        while (!runningFolder.getSuperFolderId().isEmpty()) {
            final Folder folder = findById(runningFolder.getSuperFolderId());
            folder.setReverseSortOrderNumber(sortOrder);
            breadCrumbFolders.add(folder);
            runningFolder = folder;
            sortOrder++;
        }
        final Comparator<Folder> comparator = Comparator.comparing(Folder::getReverseSortOrderNumber);
        Collections.sort(breadCrumbFolders, comparator.reversed());
        breadCrumbContainer.setBreadCrumbOfFolders(breadCrumbFolders);
        if (runningFolder.getProjectId() != null && !runningFolder.getProjectId().isEmpty()) {
            String name = "";
            final Project project = projectService.findProject(runningFolder.getProjectId());
            if (project != null) {
                name = project.getName();
            } else {
                final Product product = productService.findProduct(runningFolder.getProjectId());
                name = product.getName();
            }
            breadCrumbContainer.addBreadCrumbStringOfFolders(name);
        }
        for (final Folder folder : breadCrumbFolders) {
            breadCrumbContainer.addBreadCrumbStringOfFolders(folder.getName());
        }
        return breadCrumbContainer;
    }

    @Override
    public List<Folder> findFoldersByBySearchStringAndProjectIdAndFolderId(final String projectId, final String folderId,
            final String searchString) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in FolderServiceImpl#findFoldersByBySearchStringAndProjectIdAndFolderId",
            "400");
        BusinessAssert.isId(projectId, "projectId is mandatory in FolderServiceImpl#findFoldersByBySearchStringAndProjectIdAndFolderId",
            "400");
        BusinessAssert.notNull(searchString,
            "searchString is mandatory in FolderServiceImpl#findFoldersByBySearchStringAndProjectIdAndFolderId", "400");
        logger.info(
            "findl folders of project with id '{}' and folder with id '{}' and searchString '{}' in FolderServiceImpl#findFoldersByBySearchStringAndProjectIdAndFolderId",
            projectId, folderId, searchString);
        final List<Folder> folders = folderRepository.findBySearchStringAndProjectIdAndSuperFolderId(searchString, projectId, folderId);
        return folders;
    }

    @Override
    public List<Folder> getFolders(final String projectId, final String superFolderId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in FolderServiceImpl#create", "400");
        BusinessAssert.isId(projectId, "projectId is mandatory in FolderServiceImpl#create", "400");
        return folderRepository.findFolderByProjectIdAndSuperFolderId(projectId, superFolderId);
    }

    @Override
    public void deleteFolder(String folderId) throws BusinessException {
        BusinessAssert.notNull(folderId, "folderId is mandatory in FolderServiceImpl#create", "400");
        BusinessAssert.isId(folderId, "folderId is mandatory in FolderServiceImpl#create", "400");
        logger.info("delete folder with id '{}' in FolderServiceImpl#deleteFolder");
        final List<Folder> folderChildren = folderRepository.findBySuperFolderId(folderId);
        if (!folderChildren.isEmpty()) {
            throw new BusinessException("folder has children", "400", "FOLDER_UNDELETABLE_BECAUSE_OF_CHILDREN");
        }
        folderRepository.delete(folderId);
    }
}
