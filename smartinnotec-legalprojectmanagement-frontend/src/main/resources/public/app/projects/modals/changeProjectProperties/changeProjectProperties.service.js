(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('changeProjectPropertiesModalService', changeProjectPropertiesModalService);
        
    changeProjectPropertiesModalService.$inject = ['$modal', '$stateParams'];
    
    function changeProjectPropertiesModalService($modal, $stateParams) {
		var service = {
			showChangeProjectPropertiesModal: showChangeProjectPropertiesModal
		};		
		return service;
		
		////////////
				
		function showChangeProjectPropertiesModal(project, projectDataSheet, provinceTypes, currentUser) {
			 var changeProjectPropertiesModal = $modal.open({
				controller: ChangeProjectPropertiesModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	project: function() {
			    		return project;
			    	}, 
			    	projectDataSheet: function() {
			    		return projectDataSheet;
			    	},
			    	provinceTypes: function() {
			    		return provinceTypes;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/changeProjectProperties/changeProjectProperties.modal.html'
			});
			return changeProjectPropertiesModal;
			
			function ChangeProjectPropertiesModalController($modalInstance, $scope, project, projectDataSheet, provinceTypes, currentUser) {
		    	var vm = this; 
		    	vm.project = project;
		    	vm.projectDataSheet = projectDataSheet;
		    	vm.provinceTypes = provinceTypes;
		    	vm.currentUser = currentUser;
		    	
		    	vm.closeChangeProjectPropertiesModal = closeChangeProjectPropertiesModal;
		    	
		    	////////////
		    	
		    	function closeChangeProjectPropertiesModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();