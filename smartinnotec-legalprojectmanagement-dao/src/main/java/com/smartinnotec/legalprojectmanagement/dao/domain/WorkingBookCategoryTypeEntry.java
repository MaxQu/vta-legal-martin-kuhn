package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "WorkingBookCategoryTypeEntry")
public @Data class WorkingBookCategoryTypeEntry {

    @Id
    private String id;
    private String projectId;
    private WorkingBookCategoryTypeEnum workingBookCategoryType;
    @TextIndexed
    private String workingBookCategoryText;
    @TextIndexed
    private String enteredText;
    @DBRef
    private Tenant tenant;

    public WorkingBookCategoryTypeEntry deepCopy() {
        final WorkingBookCategoryTypeEntry workingBookCategoryTypeEntry = new WorkingBookCategoryTypeEntry();
        workingBookCategoryTypeEntry.setProjectId(projectId);
        workingBookCategoryTypeEntry.setWorkingBookCategoryType(workingBookCategoryType);
        workingBookCategoryTypeEntry.setWorkingBookCategoryText(workingBookCategoryText);
        workingBookCategoryTypeEntry.setEnteredText(enteredText);
        workingBookCategoryTypeEntry.setTenant(tenant);
        return workingBookCategoryTypeEntry;
    }
}
