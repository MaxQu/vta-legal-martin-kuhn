package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventScheduleDays;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.CalendarEventScheduleDaysRepository;

@Service("calendarEventScheduleDaysService")
public class CalendarEventScheduleDaysServiceImpl extends AbstractService implements CalendarEventScheduleDaysService {

    @Autowired
    private CalendarEventScheduleDaysRepository calendarEventScheduleDaysRepository;

    @Override
    public List<CalendarEventScheduleDays> findAllByTenant(Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in CalendarEventScheduleDaysServiceImpl#findAllByTenant", "400");
        logger.info("find all calendarEventScheduleDays by tenant with id '{}' in CalendarEventScheduleDaysServiceImpl#findAllByTenant",
            tenant.getId());
        return calendarEventScheduleDaysRepository.findByTenant(tenant);
    }

    @Override
    public List<CalendarEventScheduleDays> findAllByTenant(final User user) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in CalendarEventScheduleDaysServiceImpl#findAllByTenant", "400");
        logger.info("find all calendarEventScheduleDays by tenant with id '{}' in CalendarEventScheduleDaysServiceImpl#findAllByTenant",
            user.getTenant().getId());
        return calendarEventScheduleDaysRepository.findByTenant(user.getTenant());
    }

    @Override
    public CalendarEventScheduleDays create(final CalendarEventScheduleDays calendarEventScheduleDays) throws BusinessException {
        BusinessAssert.notNull(calendarEventScheduleDays,
            "calendarEventScheduleDays is mandatory in CalendarEventScheduleDaysServiceImpl#create", "400");
        logger.info("create calendarEventScheduleDays in CalendarEventScheduleDaysServiceImpl#create");
        return calendarEventScheduleDaysRepository.insert(calendarEventScheduleDays);
    }

    @Override
    public CalendarEventScheduleDays update(final CalendarEventScheduleDays calendarEventScheduleDays) throws BusinessException {
        BusinessAssert.notNull(calendarEventScheduleDays,
            "calendarEventScheduleDays is mandatory in CalendarEventScheduleDaysServiceImpl#update", "400");
        logger.info("update calendarEventScheduleDays in CalendarEventScheduleDaysServiceImpl#update");
        return calendarEventScheduleDaysRepository.save(calendarEventScheduleDays);
    }

    @Override
    public void delete(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in CalendarEventScheduleDaysServiceImpl#delete", "400");
        logger.info("delete calendarEventScheduleDays with id '{}' in CalendarEventScheduleDaysServiceImpl#delete", id);
        calendarEventScheduleDaysRepository.delete(id);
    }

    @Override
    public String toString() {
        return "[CalendarEventScheduleDaysServiceImpl]";
    }
}
