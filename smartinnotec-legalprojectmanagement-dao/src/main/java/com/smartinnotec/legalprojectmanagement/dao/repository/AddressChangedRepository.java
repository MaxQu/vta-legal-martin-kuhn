package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.AddressChanged;

public interface AddressChangedRepository extends MongoRepository<AddressChanged, String> {

}
