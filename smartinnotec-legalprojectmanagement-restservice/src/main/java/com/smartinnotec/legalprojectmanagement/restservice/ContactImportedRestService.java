package com.smartinnotec.legalprojectmanagement.restservice;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.ContactImportedService;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class ContactImportedRestService extends AbstractRestService {

    @Autowired
    private UserService userService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private ContactImportedService contactImportedService;

    @RequestMapping(value = "/contactsimported/contactimported/amount", method = { RequestMethod.GET }, produces = "application/json")
    public Integer getAmountOfContactsImported() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": get amount of contacts imported in ContactImportedRestService#getAmountOfContactsImported");
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        return contactImportedService.getAmountOfContactsImported(user.getTenant());
    }

    @RequestMapping(value = "/contactsimported/contactimported/{id}/delete", method = {
        RequestMethod.DELETE }, produces = "application/json")
    public void deleteContactImported(final @PathVariable("id") String id) throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": delete contactImported with id '{}' in ContactImportedRestService#deleteContactsImported", id);
        final ContactImported contactImported = contactImportedService.findById(id);

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final String message = contactImported.getInstitution();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DELETED_CONTACT_IMPORTED)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ContactImportedRestService#deleteContactsImported", createdHistory.getId());

        contactImportedService.delete(contactImported);
    }

    @RequestMapping(value = "/contactsimported/contactimported/delete/all", method = {
        RequestMethod.DELETE }, produces = "application/json")
    public void deleteAllContactsImported() throws BusinessException {
        logger.info(
            getInvokedRestServiceUserInformation() + ": delete contactsImported in ContactImportedRestService#deleteContactsImported");
        contactImportedService.deleteAllContactsImported();

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final String message = "";
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DELETED_ALL_CONTACTS_IMPORTED)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ContactImportedRestService#deleteAllContactsImported", createdHistory.getId());
    }

    @Override
    public String toString() {
        return "[ContactImportedRestService]";
    }
}
