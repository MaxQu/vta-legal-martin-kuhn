(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('contactsService', contactsService);
        
    contactsService.$inject = ['$http', 'api_config', 'Upload'];
    
    function contactsService($http, api_config, Upload) {
		var service = {
			createContact: createContact, 
			updateContact: updateContact,
			importContacts: importContacts,
			countByTenantAndActive: countByTenantAndActive,
			countCustomerContactsByTenantAndActive: countCustomerContactsByTenantAndActive,
			findContactById: findContactById,
			findAllTenantContacts: findAllTenantContacts,
			findContactBySearchString: findContactBySearchString,
			findPagedContacts: findPagedContacts,
			calculateAmountOfPages: calculateAmountOfPages,
			getContactsReferencedToProduct: getContactsReferencedToProduct,
			getContactPageSize: getContactPageSize,
			addProductToContact: addProductToContact,
			removeProductFromContact: removeProductFromContact,
			getContactImportationDateGroup: getContactImportationDateGroup,
			deleteContact: deleteContact,
			deleteImportedContactsOfBulk: deleteImportedContactsOfBulk,
			deleteAllContacts: deleteAllContacts,
			deleteAllCustomerContacts: deleteAllCustomerContacts,
			deactivateContact: deactivateContact
		};
		
		return service;
		
		////////////
		
		function createContact(contact) {
			return $http.post(api_config.BASE_URL + '/contacts/contact', contact);
		}
		
		function updateContact(contact) {
			return $http.put(api_config.BASE_URL + '/contacts/contact', contact);
		}
		
		function importContacts(file, tenant, successCallback, errorCallback, progressCallback) {
			if(file == null) {
				return;
			}
			Upload.upload({
                url: 'api/contacts/contact/import',
                data: {
                  file: file,
                  tenant: tenant
                }
            }).then(function (resp) {
            	if (successCallback) {
                    successCallback(resp);
                }
            }, function (resp) {
                if(errorCallback) {
                	errorCallback(resp);
                }
            }, function (evt) {
                if (progressCallback) {
                    progressCallback(evt);
                }
            });
		}
		
		function countByTenantAndActive(userId) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + userId + '/amount');
		}
		
		function countCustomerContactsByTenantAndActive(userId) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + userId + '/amount/customer');
		}
		
		function findContactById(id) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + id + '/contact');
		}
		
		function findAllTenantContacts(userId) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + userId);
		}
		
		function findContactBySearchString(searchString) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + searchString + '/search');
		}
		
		function findPagedContacts(page, sortingType) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/page/' + page + '/' + sortingType);
		}
		
		function calculateAmountOfPages() {
			return $http.get(api_config.BASE_URL + '/contacts/contact/amountofpages');
		}
		
		function getContactsReferencedToProduct(productId) {
			return $http.get(api_config.BASE_URL + '/contacts/contact/' + productId + '/contacts');
		}
		
		function getContactPageSize() {
			return $http.get(api_config.BASE_URL + '/contacts/contact/contactpagesize');
		}
		
		function addProductToContact(addressId, productId, contact) {
			return $http.put(api_config.BASE_URL + '/contacts/contact/' + addressId + '/' + productId + '/add', contact);
		}
		
		function removeProductFromContact(addressId, productId, contact) {
			return $http.put(api_config.BASE_URL + '/contacts/contact/' + addressId + '/' + productId + '/remove', contact);
		}
		
		function getContactImportationDateGroup() {
			return $http.get(api_config.BASE_URL + '/contacts/contact/contactimportationdategroup');
		}
		
		function deleteContact(contactId) {
			return $http.delete(api_config.BASE_URL + '/contacts/contact/' + contactId);
		}
		
		function deleteImportedContactsOfBulk(date) {
			return $http.delete(api_config.BASE_URL + '/contacts/contact/' + date + '/importedcontacts');
		}
		
		function deleteAllContacts() {
			return $http.delete(api_config.BASE_URL + '/contacts/contact/all');
		}
		
		function deleteAllCustomerContacts(countryType) {
			return $http.delete(api_config.BASE_URL + '/contacts/contact/all/customers/' + countryType);
		}
		
		function deactivateContact(contactId, state) {
			return $http.patch(api_config.BASE_URL + '/contacts/contact/' + contactId + '/' + state + '/deactivate');
		}
    }
})();