package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class UserServiceTest extends AbstractServiceTest {

    @Test
    public void shouldSearchAllStringsInSearchString() throws BusinessException {
        final User user1 = new User();
        user1.setFirstname("user1");
        user1.setSurname("user1 nachname");
        user1.setProjectUserConnectionRole(ProjectUserConnectionRoleEnum.ADMIN);
        final User createdUser1 = userService.createUser(user1);
        Assert.assertNotNull(createdUser1);

        final User user2 = new User();
        user2.setFirstname("user2");
        user2.setSurname("user2 nachname");
        user2.setProjectUserConnectionRole(ProjectUserConnectionRoleEnum.LABOR);
        final User createdUser2 = userService.createUser(user2);
        Assert.assertNotNull(createdUser2);

        final User user3 = new User();
        user3.setFirstname("user3");
        user3.setSurname("user3 nachname");
        user3.setProjectUserConnectionRole(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE);
        final User createdUser3 = userService.createUser(user3);
        Assert.assertNotNull(createdUser3);

        final List<ProjectUserConnectionRoleEnum> projectUserConnectionRoles1 = new ArrayList<>();
        projectUserConnectionRoles1.add(ProjectUserConnectionRoleEnum.ADMIN);
        final List<User> foundedUsers1 = userService.findUsersByProjectUserConnectionRoles(projectUserConnectionRoles1);
        Assert.assertEquals(1, foundedUsers1.size());

        final List<ProjectUserConnectionRoleEnum> projectUserConnectionRoles2 = new ArrayList<>();
        projectUserConnectionRoles2.add(ProjectUserConnectionRoleEnum.ADMIN);
        projectUserConnectionRoles2.add(ProjectUserConnectionRoleEnum.CUSTOMER_SERVICE);
        final List<User> foundedUsers2 = userService.findUsersByProjectUserConnectionRoles(projectUserConnectionRoles2);
        Assert.assertEquals(2, foundedUsers2.size());
    }
}
