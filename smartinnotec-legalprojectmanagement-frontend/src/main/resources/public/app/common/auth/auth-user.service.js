(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('authUserService', authUserService);
    
	authUserService.$inject = ['$http', '$q', '$location', 'authService', 'authTokenService', 'api_config', 'token_config'];
	
	function authUserService($http, $q, $location, authService, authTokenService, api_config, token_config) {
		var currentUser = false;
		return {
			signin: signin,
			signup: signup,
			logout: logout,
			changePassword: changePassword,
			misrememberPassword: misrememberPassword,
			setCurrentUser: setCurrentUser,
			getCurrentUser: getCurrentUser,
			getLastLogin: getLastLogin
		};
		
		////////////
		
		function signin(username, password) {
			return $http.post(api_config.BASE_URL + '/login/WDM', {
				username: username,
				password: password
			}).success(function(data){
				setCurrentUser(data.user);
				authTokenService.saveToken(data.token);
				authService.loginConfirmed('success', function(config) {
					var token = authTokenService.getToken();
					if (token) {
						config.headers[token_config.AUTH_TOKEN_HEADER] = token;
					}
					return config;
				});
			}).error(function(data) {
				return data;
			});
		}
		
		function signup(newUser) {
			return $http.post(api_config.BASE_URL + '/signup', newUser);
		}
		
		function logout() {
			authUserService.setCurrentUser(false);
			authTokenService.deleteToken();
		}

		function changePassword(changePasswordParams) {
			return $http.put(api_config.BASE_URL + '/changepassword', changePasswordParams);
		}

		function misrememberPassword(emailAddress) {
			return $http.put(api_config.BASE_URL + '/misrememberpassword', emailAddress);
		}
		
		function getCurrentUserFromAPI() {
			return $http.get(api_config.BASE_URL + '/getCurrentLoggedUser');
		}
		
		function getCurrentUser() {
		    if (currentUser) {
		    	return $q.when(currentUser);
		    }
		    return getCurrentUserFromAPI().then(function(response) {
	               if (response != null && response.data != null) {
	                   currentUser = response.data;
	                   return currentUser;
	               } else {
	            	   $location.path('/signin');
	               }     
	           }, function(error) {
	               var token = authTokenService.getToken();
	               if (token) {
	                   authTokenService.deleteToken();
	               }
	               $location.path('/signin');
	           });
		}
		
		function setCurrentUser(newCurrentUserData) {
			currentUser = newCurrentUserData;
			return currentUser;
		}
		
		function getLastLogin(userId) {
			return $http.get(api_config.BASE_URL + '/login/lastlogin/' + userId);
		} 
	}
})();