package com.smartinnotec.legalprojectmanagement.service.schedule;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryAmount;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeTypeEnum;
import com.smartinnotec.legalprojectmanagement.service.AbstractService;
import com.smartinnotec.legalprojectmanagement.service.ActivityService;
import com.smartinnotec.legalprojectmanagement.service.CalendarEventService;
import com.smartinnotec.legalprojectmanagement.service.CommunicationUserConnectionService;
import com.smartinnotec.legalprojectmanagement.service.ContactService;
import com.smartinnotec.legalprojectmanagement.service.DocumentFileService;
import com.smartinnotec.legalprojectmanagement.service.FacilityDetailsService;
import com.smartinnotec.legalprojectmanagement.service.HistoryAmountService;
import com.smartinnotec.legalprojectmanagement.service.ProductService;
import com.smartinnotec.legalprojectmanagement.service.ProjectService;
import com.smartinnotec.legalprojectmanagement.service.ReceipeService;
import com.smartinnotec.legalprojectmanagement.service.ThinningService;

@Service("historyAmountScheduleService")
@Configuration
@EnableAsync
@EnableScheduling
public class HistoryAmountScheduleServiceImpl extends AbstractService implements HistoryAmountScheduleService {

    @Autowired
    private HistoryAmountService historyAmountService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private FacilityDetailsService facilityDetailsService;
    @Autowired
    private DocumentFileService documentFileService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private CalendarEventService calendarEventService;
    @Autowired
    private ReceipeService receipeService;
    @Autowired
    private ThinningService thinningService;
    @Autowired
    private CommunicationUserConnectionService communicationUserConnectionService;
    
    @Override
    @Scheduled(cron = "0 0 3 1/1 * ?") // every day at 03:00 in night
    public void createHistoryAmount() throws BusinessException {
        logger.info("create historyAmount in HistoryScheduleServiceImpl#createHistoryAmount");
        
        final Integer contacts = contactService.countByActive(true);
        final Integer activities = activityService.count();
        final Integer facilityDetails = facilityDetailsService.count();
        final Integer documents = documentFileService.count();
        final Integer products = productService.count();
        final Integer projects = projectService.count();
        final Integer calendarEvents = calendarEventService.count();
        final Integer receipes = receipeService.countByReceipeType(ReceipeTypeEnum.STANDARD_MIXTURE);
        final Integer mixtures = receipeService.countByReceipeType(ReceipeTypeEnum.CHANGED_MIXTURE);
        final Integer thinnings = thinningService.count();
        final Integer communicationUserConnections = communicationUserConnectionService.count();
        
        final HistoryAmount historyAmount = new HistoryAmount();
        historyAmount.setDate(new DateTime(System.currentTimeMillis()));
        historyAmount.setContacts(contacts);
        historyAmount.setActivities(activities);
        historyAmount.setFacilityDetails(facilityDetails);
        historyAmount.setDocuments(documents);
        historyAmount.setProducts(products);
        historyAmount.setProjects(projects);
        historyAmount.setCalendarEvents(calendarEvents);
        historyAmount.setReceipes(receipes);
        historyAmount.setMixtures(mixtures);
        historyAmount.setThinnings(thinnings);
        historyAmount.setCommunicationUserConnections(communicationUserConnections);
        
        final HistoryAmount createdHistoryAmount = historyAmountService.create(historyAmount);
        logger.info("created historyAmount with id '{}' in HistoryAmountService#createHistoryAmount", createdHistoryAmount.getId());
    }
}