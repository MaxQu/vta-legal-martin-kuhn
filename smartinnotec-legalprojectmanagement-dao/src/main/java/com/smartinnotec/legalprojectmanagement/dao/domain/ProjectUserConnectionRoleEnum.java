package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum ProjectUserConnectionRoleEnum {

    ADMIN("ADMIN", 0),
    GENERAL_MANAGER("GENERAL_MANAGER", 1),
    PROJECT_MANAGER("PROJECT_MANAGER", 2),
    LABOR("LABOR", 3),
    PROJECT_WORKER("PROJECT_WORKER", 4),
    PRODUCTION("PRODUCTION", 5),
    MARKETING("MARKETING", 6),
    CUSTOMER_SERVICE("CUSTOMER_SERVICE", 7),
    EXTERNAL_WORKER("EXTERNAL_WORKER", 8),
    GUEST("GUEST", 9),
    CONTACT("CONTACT", 10),
    USER("USER", 11);

    private String value;
    private Integer sortNumber;

    private ProjectUserConnectionRoleEnum(final String value, final Integer sortNumber) {
        this.value = value;
        this.sortNumber = sortNumber;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public Integer getSortNumber() {
        return sortNumber;
    }

    public void setSortNumber(final Integer sortNumber) {
        this.sortNumber = sortNumber;
    }

}
