package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class SearchServiceTest extends AbstractServiceTest {

    @Test
    public void shouldSearchAllStringsInSearchString() {
        final boolean searchAllStrings = searchService.combineSearchStringWithAnd("das ist der Suchstring");
        Assert.assertFalse(searchAllStrings);
    }

    @Test
    public void shouldNotSearchAllStringsInSearchString() {
        final boolean searchAllStrings = searchService.combineSearchStringWithAnd("das +ist +der +Suchstring");
        Assert.assertTrue(searchAllStrings);
    }

    @Test
    public void shouldTestAllMatch() {
        final String searchString = "1001/TEST";
        String properties = "1001/TEST 13";
        properties = properties.toLowerCase();
        final boolean test = Stream.of(searchString.toLowerCase().split(" ")).allMatch(properties::contains);
        Assert.assertTrue(test);
    }
}
