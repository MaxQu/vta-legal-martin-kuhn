package com.smartinnotec.legalprojectmanagement.restservice;

import lombok.Data;

public @Data class ChangePasswordParams {

    private String oldPassword;
    private String newPassword;
}
