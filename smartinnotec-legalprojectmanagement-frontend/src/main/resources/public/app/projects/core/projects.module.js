(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();