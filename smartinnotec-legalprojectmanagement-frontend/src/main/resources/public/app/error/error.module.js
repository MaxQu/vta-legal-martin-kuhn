(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.error', [
    	    'ui.router'
    	]);
})();