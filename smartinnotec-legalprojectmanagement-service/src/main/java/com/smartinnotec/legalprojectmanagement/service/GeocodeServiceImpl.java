package com.smartinnotec.legalprojectmanagement.service;

import com.google.maps.*;
import com.google.maps.model.*;
import com.smartinnotec.legalprojectmanagement.dao.domain.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.core.env.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
public class GeocodeServiceImpl implements GeocodeService {

  protected final Logger logger = LoggerFactory.getLogger(getClass());
  private GeoApiContext context;
  private boolean googleApiKeyAvailable;

  @Autowired
  public GeocodeServiceImpl(Environment env) {
    String googleMapsKey = env.getProperty("google.maps.key");
    this.googleApiKeyAvailable = googleMapsKey != null;
    if (googleApiKeyAvailable) {
      this.context = new GeoApiContext.Builder().apiKey(env.getProperty("google.maps.key")).build();
    } else {
      logger.info("There is no Google Maps Key set (\"google.maps.key\") - No geocoding");
    }
  }

  @Override
  public void geocodeAddress(final Address address) {
    try {

      if (!googleApiKeyAvailable || address == null) {
        return;
      }

      // Build a string for the address param for geocoding
      StringBuilder builder = new StringBuilder(100);

      if (StringUtils.isNotBlank(address.getStreet())) {
        builder.append(address.getStreet());
      }

      if (StringUtils.isNotBlank(address.getPostalCode())) {
        builder.append(", " + address.getPostalCode());
      }

      if (StringUtils.isNotBlank(address.getRegion())) {
        builder.append(" " + address.getRegion());
      }

      if (StringUtils.isNotBlank(address.getCountry()) && !CountryTypeEnum.EMPTY_COUNTRY.getValue().equals(address.getCountry())) {
        builder.append(", " + address.getCountry());
      }

      if (builder.length() > 0) {
        String addressAsString = builder.toString();
        GeocodingResult[] result = GeocodingApi.geocode(context, addressAsString).await();
        if (result.length > 0 && result[0].geometry != null && result[0].geometry.location != null) {
          Geolocation geolocation = new Geolocation(result[0].geometry.location.lat, result[0].geometry.location.lng);
          address.setGeolocation(geolocation);
        } else {
          logger.info("geocoding of address " + addressAsString + " not successful");
        }
      }

    } catch (Exception e) {
      logger.error("geocodeAddress", e);

    }

  }

  @Override
  public Address reverseGeocode(final Geolocation geolocation) {
    if (!googleApiKeyAvailable || geolocation == null) {
      return null;
    }

    try {
      GeocodingResult[] results = GeocodingApi.reverseGeocode(context, new LatLng(geolocation.getLat(), geolocation.getLng())).await();

      final Address address = new Address();
      address.setGeolocation(geolocation);

      Arrays.stream(results).filter(result -> containsType(result, AddressType.STREET_ADDRESS)).findFirst().ifPresent(geocodingResult -> {

        AddressComponent streetNumber = findAddressComponent(geocodingResult, AddressComponentType.STREET_NUMBER);
        AddressComponent street = findAddressComponent(geocodingResult, AddressComponentType.ROUTE);
        AddressComponent city = findAddressComponent(geocodingResult, AddressComponentType.LOCALITY, AddressComponentType.POLITICAL);
        AddressComponent country = findAddressComponent(geocodingResult, AddressComponentType.COUNTRY, AddressComponentType.POLITICAL);
        AddressComponent postalCode = findAddressComponent(geocodingResult, AddressComponentType.POSTAL_CODE);
        // AddressComponent provinceType = findAddressComponent(geocodingResult, AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1, , AddressComponentType.POLITICAL);

        if (street != null || streetNumber != null) {
          address.setStreet((street.longName != null ? street.longName : "") + " " + (streetNumber.longName != null ? streetNumber.longName : "").trim());
        }

        if (city != null) {
          address.setRegion(city.longName);
        }

        if (postalCode != null) {
          address.setPostalCode(postalCode.longName);
        }

        if (country != null) {
          CountryTypeEnum countryEnum = mapCountryString(country.longName);
          if (countryEnum != null) {
            address.setCountry(countryEnum.getValue());
          }
          address.setIntSign(country.shortName);
        }

        /*
        // Needs mapping from String to ProvinceTypeEnum
        if (provinceType != null) {
          address.setProvinceType();
        }
        */

      });

      return address;

    } catch (Exception e) {
      logger.error("reverseGeocode", e);
    }

    return null;

  }

  // TODO
  CountryTypeEnum mapCountryString(String country) {
    if (country != null) {
      String lowerCaseCountry = country.toLowerCase();
      if ("austria".equals(lowerCaseCountry) || "österreich".equals(lowerCaseCountry)) {
        return CountryTypeEnum.AUSTRIA;
      } else if ("germany".equals(lowerCaseCountry) || "deutschland".equals(lowerCaseCountry)) {
        return CountryTypeEnum.GERMANY;
      } else if ("switzerland".equals(lowerCaseCountry) || "schweiz".equals(lowerCaseCountry)) {
        return CountryTypeEnum.SWITZERLAND;
      }
    }
    return null;
  }

  boolean containsType(GeocodingResult result, AddressType type) {
    return Arrays.stream(result.types).anyMatch(currentType -> currentType == type);
  }

  boolean addressComponentContainsType(AddressComponent addressComponent, AddressComponentType... types) {
    return Arrays.stream(types).allMatch(type -> Arrays.stream(addressComponent.types).anyMatch(currentType -> currentType == type));
  }

  AddressComponent findAddressComponent(GeocodingResult geocodingResult, AddressComponentType... types) {
    return Arrays.stream(geocodingResult.addressComponents)
                 .filter((AddressComponent addressComponent) -> addressComponentContainsType(addressComponent, types))
                 .findFirst()
                 .orElse(null);
  }

}
