package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.repository.HistoryRepository;

@Service("historyService")
public class HistoryServiceImpl extends AbstractService implements HistoryService {

    @Autowired
    private HistoryRepository historyRepository;

    @Override
    public History create(final History history) throws BusinessException {
        BusinessAssert.notNull(history, "history is mandatory in HistoryServiceImpl#create", "400");
        logger.info("create history in HistoryServiceImpl#create");
        return historyRepository.save(history);
    }

    @Override
    public List<History> findAll() throws BusinessException {
        logger.info("find all histories in HistoryServiceImpl#findAll");
        return historyRepository.findAll();
    }

    @Override
    public List<History> findByTenantAndDateBetween(final Tenant tenant, final DateTime start, final DateTime end)
            throws BusinessException {
        BusinessAssert.notNull(start, "start is mandatory in HistoryServiceImpl#findByDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in HistoryServiceImpl#findByDateBetween", "400");
        logger.info("find historys between start '{}' and end '{}' in HistoryServiceImpl#findByDateBetween", start, end);
        return historyRepository.findByTenantAndDateBetween(tenant, start, end);
    }

    @Override
    public List<History> findPagedHistoryByTenantAndDateBetween(final Tenant tenant, final DateTime start, final DateTime end,
            final Integer page) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in HistoryServiceImpl#findPagedHistoryByTenantAndDateBetween", "400");
        BusinessAssert.notNull(start, "start is mandatory in HistoryServiceImpl#findPagedHistoryByTenantAndDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in HistoryServiceImpl#findPagedHistoryByTenantAndDateBetween", "400");
        BusinessAssert.notNull(page, "page is mandatory in HistoryServiceImpl#findPagedHistoryByTenantAndDateBetween", "400");
        logger.info(
            "find paged history by tenant with id '{}', start '{}' and end '{}' for page '{}' in HistoryServiceImpl#findPagedHistoryByTenantAndDateBetween",
            tenant.getId(), start, end, page);
        final Pageable pageable = new PageRequest(page, this.getHistoryPageSize(), new Sort(Sort.Direction.ASC, "date"));
        final List<History> pagedHistory = historyRepository.findPagedHistoryByTenantAndDateBetween(tenant, start, end, pageable);
        return pagedHistory;
    }

    @Override
    public Integer calculateAmountOfPages(final Tenant tenant, final DateTime start, final DateTime end) throws BusinessException {
        logger.info("calculate amount of pages in HistoryServiceImpl#calculateAmountOfPages");
        final Integer historyPageSize = this.getHistoryPageSize();
        final Integer amountOfHistorys = historyRepository.countByTenantAndDateBetween(tenant, start, end);
        final Integer amountOfPageSizes = (amountOfHistorys + historyPageSize - 1) / historyPageSize;

        logger.info(
            "calculated amount of pages to '{}' where history date between '{}' and '{}' in ProjectServiceImpl#calculateAmountOfPages",
            amountOfPageSizes, start, end);
        return amountOfPageSizes;
    }

    @Override
    public String toString() {
        return "[HistoryServiceImpl]";
    }
}
