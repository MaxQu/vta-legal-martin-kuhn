package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ActivityTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventSerialDateTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationModeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.FacilityDetailsTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProductAggregateConditionTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProductTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProvinceTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Title;
import com.smartinnotec.legalprojectmanagement.dao.domain.UnityTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.service.TitleService;

@RestController
public class OptionsRestService extends AbstractRestService {

    @Autowired
    private TitleService titleService;

    @RequestMapping(value = "/options/facilitydetails", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getFacilityDetailsTypes() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": get facilityDetails types in OptionsRestService#getFacilityDetailsTypes");
        final FacilityDetailsTypeEnum[] facilityDetailsTypes = FacilityDetailsTypeEnum.values();
        final List<String> facilityDetaisTypeList = new ArrayList<>();
        for (int i = 0; i < facilityDetailsTypes.length; i++) {
            if (facilityDetailsTypes[i].isVisible()) {
                facilityDetaisTypeList.add(facilityDetailsTypes[i].getValue());
            }
        }
        logger.debug("got '{}' possible facilityDetails types '{}' in OptionsRestService#getFacilityDetailsTypes",
            facilityDetaisTypeList.size(), facilityDetaisTypeList);
        return facilityDetaisTypeList;
    }

    @RequestMapping(value = "/options/activitytype/{type}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getActivityTypes(@PathVariable(value = "type")
    final String type) throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": get activity types in OptionsRestService#getActivityTypes");
        final ActivityTypeEnum[] activityTypes = ActivityTypeEnum.values();
        final List<String> activityTypeList = new ArrayList<>();
        for (int i = 0; i < activityTypes.length; i++) {
            if (activityTypes[i].isActive() == false) {
                continue;
            }

            activityTypeList.add(activityTypes[i].getValue());
        }
        logger.debug("got '{}' possible activity types '{}' in OptionsRestService#getActivityTypes", activityTypeList.size(),
            activityTypeList);
        return activityTypeList;
    }

    @RequestMapping(value = "/options/countrytype", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getCountryTypes() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": get country types in OptionsRestService#getCountryTypes");
        final CountryTypeEnum[] countryTypes = CountryTypeEnum.values();
        final List<String> countryTypeList = new ArrayList<>();
        for (int i = 0; i < countryTypes.length; i++) {
            countryTypeList.add(countryTypes[i].getValue());
        }
        logger.debug("got '{}' possible country types '{}' in OptionsRestService#getCountryTypes", countryTypeList.size(), countryTypeList);
        return countryTypeList;
    }

    @RequestMapping(value = "/options/calendareventtypes", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getCalendarEventTypes() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": get calendarEvent types in OptionsRestService#getCalendarEventTypes");
        final CalendarEventTypeEnum[] calendarEventTypes = CalendarEventTypeEnum.values();
        final List<String> calendarEventTypeList = new ArrayList<>();
        for (int i = 0; i < calendarEventTypes.length; i++) {
            if (calendarEventTypes[i].isActive()) {
                calendarEventTypeList.add(calendarEventTypes[i].getValue());
            }
        }
        logger.debug("got '{}' possible calendarEvent types '{}' in OptionsRestService#getCalendarEventTypes", calendarEventTypeList.size(),
            calendarEventTypeList);
        return calendarEventTypeList;
    }

    @RequestMapping(value = "/options/option/provincetypes", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getProvinceTypes() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": get province types in OptionsRestService#getProvinceTypes");
        final ProvinceTypeEnum[] provinceTypes = ProvinceTypeEnum.values();
        final List<String> provinceTypeList = new ArrayList<>();
        for (int i = 0; i < provinceTypes.length; i++) {
            provinceTypeList.add(provinceTypes[i].getValue());
        }
        logger.debug("got '{}' possible province types '{}' in OptionsRestService#getProvinceTypes", provinceTypeList.size(),
            provinceTypeList);
        return provinceTypeList;
    }

    @RequestMapping(value = "/options/option/provincetypes/{countrytype}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getProvinceTypesOfCountryType(final @PathVariable(value = "countrytype") CountryTypeEnum countrytype)
            throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation()
            + ": get province types of countryType '{}' in OptionsRestService#getProvinceTypesOfCountryType", countrytype);

        final List<String> provinceTypeList = new ArrayList<>();
        final ProvinceTypeEnum[] provinceTypes = ProvinceTypeEnum.values();
        switch (countrytype) {
        case AUSTRIA:
            for (int i = 0; i < provinceTypes.length; i++) {
                if (provinceTypes[i].getCountryType() == CountryTypeEnum.AUSTRIA) {
                    provinceTypeList.add(provinceTypes[i].getValue());
                }
            }
            break;
        case SWITZERLAND:
            for (int i = 0; i < provinceTypes.length; i++) {
                if (provinceTypes[i].getCountryType() == CountryTypeEnum.SWITZERLAND) {
                    provinceTypeList.add(provinceTypes[i].getValue());
                }
            }
            break;
        case GERMANY:
            for (int i = 0; i < provinceTypes.length; i++) {
                if (provinceTypes[i].getCountryType() == CountryTypeEnum.GERMANY) {
                    provinceTypeList.add(provinceTypes[i].getValue());
                }
            }
            break;
        default:
            return new ArrayList<>();
        }
        logger.debug("got '{}' possible province types '{}' of country type '{}' in OptionsRestService#getProvinceTypesOfCountryType",
            provinceTypeList.size(), countrytype, provinceTypeList);
        return provinceTypeList;
    }

    @RequestMapping(value = "/options/option/contacttypes", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getContactTypes() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": get contact types in OptionsRestService#getProvinceTypes");
        final ContactTypeEnum[] contactTypes = ContactTypeEnum.values();
        final List<String> contactTypeList = new ArrayList<>();
        for (int i = 0; i < contactTypes.length; i++) {
            if (contactTypes[i].isActive()) {
                contactTypeList.add(contactTypes[i].getValue());
            }
        }
        logger.debug("got '{}' possible contact types '{}' in OptionsRestService#getProvinceTypes", contactTypeList.size(),
            contactTypeList);
        return contactTypeList;
    }

    @RequestMapping(value = "/options/roles", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getRoles() throws BusinessException {
        logger.info(getInvokedRestServiceUserInformation() + ": get possible roles in OptionsRestService#getRoles");
        final ProjectUserConnectionRoleEnum[] projectUserConnectionRoles = ProjectUserConnectionRoleEnum.values();
        final List<String> projectUserConnectionRoleList = new ArrayList<>();
        for (int i = 0; i < projectUserConnectionRoles.length; i++) {
            projectUserConnectionRoleList.add(projectUserConnectionRoles[i].getValue());
        }
        logger.debug("got '{}' possible roles '{}' in OptionsRestService#getRoles", projectUserConnectionRoleList.size(),
            projectUserConnectionRoleList);
        return projectUserConnectionRoleList;
    }

    @RequestMapping(value = "/options/option/titles", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Title> getAllTitles() throws BusinessException {
        logger.info("find all titles in OptionRestService#getAllTitles");
        return titleService.findAllTitles();
    }

    @RequestMapping(value = "/options/option/userroles", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getUserRoles() throws BusinessException {
        logger.info("find all user roles in OptionRestService#getUserRoles");
        final UserRoleEnum[] userRoles = UserRoleEnum.values();
        final List<String> userRoleList = new ArrayList<>();
        for (int i = 0; i < userRoles.length; i++) {
            userRoleList.add(userRoles[i].getValue());
        }
        logger.debug("got '{}' user roles '{}' in OptionsRestService#getUserRoles", userRoleList.size(), userRoleList);
        return userRoleList;
    }

    @RequestMapping(value = "/options/option/producttypes", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getProductTypes() throws BusinessException {
        logger.info("find all product types in OptionRestService#getProductTypes");
        final ProductTypeEnum[] productTypeEnums = ProductTypeEnum.values();
        final List<String> productTypeList = new ArrayList<>();
        for (int i = 0; i < productTypeEnums.length; i++) {
            productTypeList.add(productTypeEnums[i].getValue());
        }
        logger.debug("got '{}' product types '{}' in OptionsRestService#getProductTypes", productTypeList.size(), productTypeList);
        return productTypeList;
    }

    @RequestMapping(value = "/options/option/unitytype", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getUnityTypes() throws BusinessException {
        logger.info("find all unityTypes in OptionRestService#getUnityType");
        final UnityTypeEnum[] unityTypes = UnityTypeEnum.values();
        final List<String> unityTypeList = new ArrayList<>();
        for (int i = 0; i < unityTypes.length; i++) {
            unityTypeList.add(unityTypes[i].getValue());
        }
        logger.debug("got '{}' unityType '{}' in OptionsRestService#getUnityType", unityTypeList.size(), unityTypeList);
        return unityTypeList;
    }

    @RequestMapping(value = "/options/option/workingbookcategories", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getWorkingBookCategoryTypes() throws BusinessException {
        logger.info("find all workingbook categories in OptionRestService#getWorkingBookCategoryTypes");
        final WorkingBookCategoryTypeEnum[] workingBookCategoryTypes = WorkingBookCategoryTypeEnum.values();
        final List<String> workingBookCategoryList = new ArrayList<>();
        for (int i = 0; i < workingBookCategoryTypes.length; i++) {
            workingBookCategoryList.add(workingBookCategoryTypes[i].getValue());
        }
        logger.debug("got '{}' workingbook categories '{}' in OptionsRestService#getWorkingBookCategoryTypes",
            workingBookCategoryList.size(), workingBookCategoryList);
        return workingBookCategoryList;
    }

    @RequestMapping(value = "/options/option/communicationmodes", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getCommunicationModes() throws BusinessException {
        logger.info("find all communicationModes in OptionRestService#getCommunicationModes");
        final CommunicationModeEnum[] communicationModes = CommunicationModeEnum.values();
        final List<String> communicationModeList = new ArrayList<>();
        for (int i = 0; i < communicationModes.length; i++) {
            communicationModeList.add(communicationModes[i].getValue());
        }
        logger.debug("got '{}' communicationModes '{}' in OptionsRestService#getCommunicationModes", communicationModeList.size(),
            communicationModeList);
        return communicationModeList;
    }

    @RequestMapping(value = "/options/option/calendareventserialdates", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getCalendarEventSerialDateTypes() throws BusinessException {
        logger.info("find all calendarEventSerialDateTypes in OptionRestService#getCalendarEventSerialDateTypes");
        final CalendarEventSerialDateTypeEnum[] calendarEventSerialDateType = CalendarEventSerialDateTypeEnum.values();
        final List<String> calendarEventSerialDateTypeList = new ArrayList<>();
        for (int i = 0; i < calendarEventSerialDateType.length; i++) {
            calendarEventSerialDateTypeList.add(calendarEventSerialDateType[i].getValue());
        }
        logger.debug("got '{}' calendarEventSerialDateTypes '{}' in OptionsRestService#getCalendarEventSerialDateTypes",
            calendarEventSerialDateTypeList.size(), calendarEventSerialDateTypeList);
        return calendarEventSerialDateTypeList;
    }

    @RequestMapping(value = "/options/option/productaggregateconditions", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<String> getProductAggregateConditionTypes() throws BusinessException {
        logger.info("find all productAggregateConditionTypes in OptionRestService#getProductAggregateConditionTypes");
        final ProductAggregateConditionTypeEnum[] productAggregateConditionTypes = ProductAggregateConditionTypeEnum.values();
        final List<String> productAggregateConditionTypeList = new ArrayList<>();
        for (int i = 0; i < productAggregateConditionTypes.length; i++) {
            productAggregateConditionTypeList.add(productAggregateConditionTypes[i].getValue());
        }
        logger.debug("got '{}' productAggregateConditionTypes '{}' in OptionsRestService#getProductAggregateConditionTypes",
            productAggregateConditionTypeList.size(), productAggregateConditionTypeList);
        return productAggregateConditionTypeList;
    }

    @Override
    public String toString() {
        return "[OptionsRestService]";
    }
}
