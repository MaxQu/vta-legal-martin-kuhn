package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroup;

public interface CommunicationGroupService {

    CommunicationGroup createCommunicationGroup(final CommunicationGroup messageGroup) throws BusinessException;

    CommunicationGroup findCommunicationGroup(final String id) throws BusinessException;

    CommunicationGroup updateCommunicationGroup(final CommunicationGroup messageGroup) throws BusinessException;

    void deleteCommunicationGroup(final String id) throws BusinessException;

}
