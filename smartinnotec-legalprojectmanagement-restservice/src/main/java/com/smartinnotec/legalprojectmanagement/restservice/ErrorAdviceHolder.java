package com.smartinnotec.legalprojectmanagement.restservice;

import java.sql.Timestamp;

import lombok.Data;

public @Data final class ErrorAdviceHolder {

    private Timestamp timestamp;
    private String status;
    private String error;
    private String errorCodeType;
    private String exception;
    private String message;
    private String path;

}
