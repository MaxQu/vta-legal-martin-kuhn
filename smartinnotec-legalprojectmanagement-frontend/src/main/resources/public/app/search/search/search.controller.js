(function() {
    'use strict';
    
    
    angular
    	.module('legalprojectmanagement.search')
    	.controller('SearchController', SearchController);
    
    SearchController.$inject = ['$scope', '$timeout', 'searchService', 'currentUser', 'searchHistory', 'labelsPredefined', 'userAuthorizationUserContainer', 'unityTypes', 'currentThinningPolymerDateTimeValue', 'titles', 'provinceTypes', 'contactTypes', 'countryTypes', 'roles', 'usersOfProject', 'dateUtilityService', 'projectTemplateService', 'calendarEventUserConnectionService', 'userOfCalendarEventModalService', 'projectsService', 'projectUserConnectionService', 'projectsAssignedToContactModalService', 'printingPDFService', 'documentInformationModalService', 'documentFileService', 'receipeService', 'createReceipeModalService', 'editReceipeModalService', 'historyOfReceipeModalService', 'editReceipeToMixtureModalService', 'thinningService', 'createThinningModalService', 'editThinningModalService', 'utilService', 'userService', 'usersAssignedToContactModalService', 'editContactModalService', 'contactsService', 'projectsOfContactModalService', 'activityService', 'contactActivityModalService', 'facilityDetailsService', 'facilityDetailsOfContactModalService', 'optionsService', 'productsService', 'yesNoContactModalService'];
    
    function SearchController($scope, $timeout, searchService, currentUser, searchHistory, labelsPredefined, userAuthorizationUserContainer, unityTypes, currentThinningPolymerDateTimeValue, titles, provinceTypes, contactTypes, countryTypes, roles, usersOfProject, dateUtilityService, projectTemplateService, calendarEventUserConnectionService, userOfCalendarEventModalService, projectsService, projectUserConnectionService, projectsAssignedToContactModalService, printingPDFService, documentInformationModalService, documentFileService, receipeService, createReceipeModalService, editReceipeModalService, historyOfReceipeModalService, editReceipeToMixtureModalService, thinningService, createThinningModalService, editThinningModalService, utilService, userService, usersAssignedToContactModalService, editContactModalService, contactsService, projectsOfContactModalService, activityService, contactActivityModalService, facilityDetailsService, facilityDetailsOfContactModalService, optionsService, productsService, yesNoContactModalService) {
	    $scope.vm = this; 
    	var vm = this; 
    	
    	vm.currentUser = currentUser;
    	vm.titles = titles.data;
    	vm.provinceTypes = provinceTypes.data;
    	vm.contactTypes = contactTypes.data;
    	vm.countryTypes = countryTypes.data;
    	vm.roles = roles.data;
    	vm.usersOfProject = usersOfProject.data;
    	vm.searchHistory = searchHistory.data;
    	vm.labelsPredefined = labelsPredefined.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.unityTypes = unityTypes.data;
    	vm.categories = [];
    	vm.searchString = null;
    	vm.projects = false;
    	vm.products = false;
    	vm.files = false;
    	vm.calendar = false;
    	vm.contacts = false;
    	vm.communication = false;
    	vm.timeRegistration = false;
    	vm.searchResults = null;
    	vm.lastSearchResultLength = 0;
    	vm.loadingSearchResults = false;
    	vm.noLastReceipeExists = false;
    	vm.labelOrder = 'name';
    	vm.activityTypes = null;
    	vm.facilityDetailsTypes = null;
    	vm.polymerPromille = currentThinningPolymerDateTimeValue.data.value;
    	
    	vm.search = search;
    	vm.removeSearchString = removeSearchString;
    	vm.printContainer = printContainer;
    	vm.updateContact = updateContact;
    	vm.categorieChanged = categorieChanged;
    	vm.createDocumentUrl = createDocumentUrl;
    	vm.createContactDocumentUrl = createContactDocumentUrl;
    	vm.showTemplateModal = showTemplateModal;
    	vm.showContactTemplateModal = showContactTemplateModal;
    	vm.showEditContactModal = showEditContactModal;
    	vm.showProductsOfContact = showProductsOfContact;
    	vm.showContactActivityModal = showContactActivityModal;
    	vm.showFacilityDetailsModal = showFacilityDetailsModal;
    	vm.showDeleteContactModal = showDeleteContactModal;
    	vm.deleteContact = deleteContact;
    	vm.searchForLabel = searchForLabel;
    	vm.showUserOfCalendarEventModal = showUserOfCalendarEventModal;
    	vm.showUsersAssignedToContactModal = showUsersAssignedToContactModal;
    	vm.showProjectsAssignedToContactModal = showProjectsAssignedToContactModal;
    	vm.formatStartEndDateTime = formatStartEndDateTime;
    	vm.loadHistorySearch = loadHistorySearch;
    	vm.showDocumentInformationModal = showDocumentInformationModal;
    	vm.updateDocumentFile = updateDocumentFile;
    	vm.createReceipe = createReceipe;
    	vm.editAndDeleteReceipe = editAndDeleteReceipe;
    	vm.historyOfReceipe = historyOfReceipe;
    	vm.editReceipeToMixture = editReceipeToMixture;
    	vm.editAndDeleteMixture = editAndDeleteMixture;
    	vm.createThinning = createThinning;
    	vm.editAndDeleteThinning = editAndDeleteThinning;
    	vm.purgeSearchString = purgeSearchString;
    	loadActivityTypes();
    	loadFacilityDetailsTypes();
    	
    	////////////
    	
    	function search() {
    		vm.loadingSearchResults = true;
			loadSearchResult();
    	}
    	
    	function setTimeout() {
      		$timeout(function() {
      			vm.noLastReceipeExists = false;
  			   }, 2000); 
      	}
    	
    	function printContainer(container, name) { 
    		if(vm.searchResults.length > 0) {
    			printingPDFService.printSchedule(container, name);
    		}
    	}
    	
    	function loadHistorySearch(searchString) {
    		vm.searchString = searchString;
    		loadSearchResult();
    	}
    	
    	function searchForLabel(label) {
    		if(vm.searchString != null) {
    			vm.searchString = vm.searchString + ' ' + label;
    		} else {
    			vm.searchString = label;
    		}
    	}
    	
    	function removeSearchString() {
    		vm.searchString = null;
    		vm.searchResults = [];
    	}
    	
    	function loadSearchResult() {
    		if(vm.searchString != null && vm.searchString.length >= 3) {
    			var searchTerm = utilService.prepareSearchTerm(vm.searchString);
	    		searchService.search(searchTerm, vm.categories, vm.lastSearchResultLength).then(function successCallback(response) {
					vm.searchResults = prepareLoadedSearchResults(response.data);
					vm.lastSearchResultLength = vm.searchResults.length;
					vm.loadingSearchResults = false;
	  	   		 }, function errorCallback(response) {
	  	   			  console.log('error search.controller#loadSearchResult');
	  	   		 });
    		} else {
				vm.searchResults = null;
				vm.lastSearchResultLength = 0;
				vm.loadingSearchResults = false;
			}
    	}
    	
    	function prepareLoadedSearchResults(searchResults) {
    		for(var i = 0; i < searchResults.length; i++) {
    			if(searchResults[i].documentFile != null) {
    				searchResults[i].documentFile.documentFileVersion = searchResults[i].documentFile.documentFileVersions[searchResults[i].documentFile.documentFileVersions.length-1];
        		}
    		}
    		return searchResults;
    	}
    	
    	function categorieChanged(categorie, state) {
    		if(state === true) {
    			vm.categories.push(categorie);
    		} else if(state === false) {
    			for(var i = 0; i < vm.categories.length; i++) {
    				if(vm.categories[i] == categorie) {
    					vm.categories.splice(i, 1);
    				}
    			}
    		}
    		loadSearchResult();
    	}
    	
    	function showEditContactModal(contact) {
    		editContactModalService.showEditContactModal(contact, vm.titles, vm.provinceTypes, vm.contactTypes, vm.countryTypes, vm, vm.currentUser, vm.roles, vm.usersOfProject);
    	}
    	
    	function showUserOfCalendarEventModal(calendarEvent) {
    		calendarEventUserConnectionService.findCalendarEventUserConnectionsByCalendarEvent(calendarEvent.id).then(function (response) {
	    		 var calendarEventUserConnections = response.data;
	    		 userOfCalendarEventModalService.showUserOfCalendarEventModal(calendarEvent, calendarEventUserConnections, vm);
	    	  }).catch(function (data) {
				console.log('error calendar.search.controller#showUserOfCalendarEventModal: ' + data);
	    	  });
    	}
    	
    	function showProjectsAssignedToContactModal(contact) {
    		projectsService.findProjectsOfUser(currentUser.id).then(function (projectsResponse) {
    			var projects = projectsResponse.data;
    			projectUserConnectionService.findProjectUserConnectionsByContactAndProject(contact.id).then(function (projectUserConnectionResponse) {
        			var projectUserConnections = projectUserConnectionResponse.data;
        			projectsAssignedToContactModalService.showProjectsAssignedToContactModal(contact, projects, projectUserConnections);
    			}).catch(function (data) {
    				console.log('error in search.controller.js#findProjectsOfUser: ' + data);
    			});
			}).catch(function (data) {
				console.log('error in search.controller.js#showProjectsAssignedToContactModal: ' + data);
			});
    	}
    	
    	function showUsersAssignedToContactModal(contact) {
    		userService.findAllUsers().then(function(projectsResponse) {
    			var users = projectsResponse.data;
    			projectUserConnectionService.findProjectUserConnectionsByContactAndUser(contact.id).then(function (projectUserConnectionResponse) {
        			var projectUserConnections = projectUserConnectionResponse.data;
        			usersAssignedToContactModalService.showUsersAssignedToContactModal(contact, users, projectUserConnections);
    			}).catch(function (data) {
    				console.log('error in contacts.controller.js#findProjectsOfUser: ' + data);
    			});
			}).catch(function (data) {
				console.log('error in contacts.controller.js#showProjectsAssignedToContactModal: ' + data);
			});
    	}
    	
    	function formatStartEndDateTime(dateTimeFrom, dateTimeUntil) {
    		return dateUtilityService.formatStartEndDateTime(dateTimeFrom, dateTimeUntil);
    	}
    	
    	function showDocumentInformationModal(documentFile) {
    		documentInformationModalService.showDocumentInformationModal(documentFile, currentUser, vm.userAuthorizationUserContainer, null, vm, false);
    	}
    	
    	function createDocumentUrl(user, projectId, documentFile, documentFileVersion) {
    		if(documentFile == null || documentFileVersion == null) {
    			return '';
    		}
    		var version = documentFileVersion === undefined ? '_Version1' : documentFileVersion.version;
    		return 'api/filedownloads/filedownload/' + user.id + '/' + documentFile.id + '/' + version + '/';
    	}
    	
    	function showTemplateModal(url, documentFile, documentFileVersion) {
    		projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
    	}
    	
    	function createContactDocumentUrl(user, product, receipeAttachment) {		    	
    		var version = receipeAttachment.version === undefined ? '_Version1' : receipeAttachment.version;
    		return 'api/filedownloads/filedownload/' + currentUser.id + '/' + receipeAttachment.documentFileId + '/' + receipeAttachment.version + '/';
    	}
    	
    	function showContactTemplateModal(url, receipeAttachment) {
    		documentFileService.findDocumentFileById(receipeAttachment.documentFileId).then(function(response) {
    			var documentFile = response.data;
    			var documentFileVersion = documentFileService.getVersionObjectOfDocumentFile(documentFile, receipeAttachment.version);
    			projectTemplateService.showTemplateModal(url, documentFile, documentFileVersion);
  	   		 }, function errorCallback(response) {
  	   			 console.log('error search.controller#showTemplateModal'); 
  	   		 });
    	}
    	
    	function updateDocumentFile(documentFile, updateDocumentFileResponsibility) {
    		documentFileService.updateDocumentFile(documentFile, documentFile.projectProductId).then(function(response) {
    			console.log('updated documentFile in search.controller#updateDocumentFile');
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#updateDocumentFile');
  	   		 });
    	} 
    	
    	function updateContact(contact) {
    		contact.importConfirmed = true;
    		contactsService.updateContact(contact).then(function(response) {
			}).catch(function (data) {
				console.log('error in search.controller.js#updateContact: ' + data);
			});
    	}
    	
    	function showProductsOfContact(contact) {
    		contactsService.findContactById(contact.id).then(function(response) {
    			var contactWithProducts = response.data;
    			projectsOfContactModalService.showProductsOfContactModal(contactWithProducts, vm.currentUser, vm.userAuthorizationUserContainer);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#showProductsOfContact');
  	   		 });
    	}
    	
    	function showContactActivityModal(contact) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		activityService.getActivitiesOfContactIdInRange(contact.id, startDateString, endDateString).then(function(response) {
    			var activities = response.data;
    			contactActivityModalService.showContactActivityModal(contact, activities, start, end, vm.activityTypes, vm.userAuthorizationUserContainer);
    		}, function errorCallback(response) {
	   			  console.log('error search.controller.js#showContactActivityModal');
	   		 });
    	}
    	
    	function showFacilityDetailsModal(contact) {  
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		facilityDetailsService.getFacilityDetailsOfContactIdInRange(contact.id, startDateString, endDateString).then(function(response) {
    			var facilityDetails = response.data;
    			facilityDetailsOfContactModalService.showFacilityDetailsOfContactModal(contact, facilityDetails, start, end, vm.activityTypes, vm.facilityDetailsTypes, vm.userAuthorizationUserContainer);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#showFacilityDetailsModal');
  	   		 });
    	}
    	
    	function showDeleteContactModal(contact) {
    		productsService.getProductsReferencedToContact(contact.id).then(function(response) {
    			var productsReferencedToContact = response.data;
    			calendarEventUserConnectionService.findCalendarEventUserConnectionsByContact(contact.id).then(function(response) {
    				var calendarEventUserConnections = response.data;
    				yesNoContactModalService.showYesNoModal(vm, contact, productsReferencedToContact, calendarEventUserConnections);
    			 }, function errorCallback(response) {
    	   			  console.log('error search.controller.js#showDeleteContactModal#findCalendarEventUserConnectionsByContact');
     	   		 });
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#showDeleteContactModal#showDeleteContactModal');
  	   		 });
    	}
    	
    	function deleteContact(contact) {
    		contactsService.deleteContact(contact.id).then(function (response) {
    			for(var i = 0; i < vm.searchResults.length; i++) {
    				if(vm.searchResults[i].contact != null && vm.searchResults[i].contact.id == contact.id) {
    					vm.searchResults.splice(i, 1);
    					break;
    				}
    			}
			}).catch(function (data) {
				console.log('error in search.controller.js#deleteContact: ' + data);
			});
    	}
    	
    	function loadActivityTypes() {     
    		optionsService.getActivityTypes('all').then(function(response) {
    			vm.activityTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#loadActivityTypes');
  	   		 });
    	}
    	
    	function loadFacilityDetailsTypes() {
    		optionsService.getFacilityDetailsTypes().then(function(response) {
    			vm.facilityDetailsTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error search.controller.js#loadFacilityDetailsTypes');
  	   		 });
    	}
    	
    	function createReceipe(product, receipeType) {
    		var receipe = {
	    		productId: '',
	    		date: new Date(),
	    		receipeProducts: [],
	    		information: '',
	    		receipeAttachments: [],
	    		receipeType: null
	    	};
    		receipeService.findAllReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var allReceipesOfProduct = response.data;
    			createReceipeModalService.showCreateReceipeModal(vm.currentUser, product, null, receipe, allReceipesOfProduct, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#createReceipe');
 	   		 });
    	}
    	
    	function editAndDeleteReceipe(product, receipeType) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
    			var receipesOfProduct = response.data;
    			editReceipeModalService.showEditReceipeModal(vm.currentUser, product, null, receipesOfProduct, receipeType, start, end, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#editAndDeleteReceipe');
 	   		 });
    	}
    	
    	function editReceipeToMixture(product, receipeType, $event) {
    		// get last standard receipe of product
    		$event.stopPropagation();
    		receipeService.findLastReceipesOfProductOfType(product.id, receipeType).then(function(response) {
    			var lastReceipeOfProduct = response.data;
    			if(lastReceipeOfProduct == '') {
    				vm.noLastReceipeExists = true;
    				setTimeout();
    			} else {
    				editReceipeToMixtureModalService.showEditReceipeToMixtureModal(vm.currentUser, product, null, lastReceipeOfProduct, vm.userAuthorizationUserContainer, vm.unityTypes);
    			}
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#editReceipeToMixture');
 	   		 });
    	}
    	
    	function editAndDeleteMixture(product, receipeType, $event) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
    			var receipesOfProduct = response.data;
    			editReceipeModalService.showEditReceipeModal(vm.currentUser, product, null, receipesOfProduct, receipeType, start, end, vm.userAuthorizationUserContainer, vm.unityTypes);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#editAndDeleteMixture');
 	   		 });
    	}
    	
    	function historyOfReceipe(product, receipeType) {
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
    			var receipesOfProduct = response.data;
    			historyOfReceipeModalService.showHistoryOfReceipeModal(vm.currentUser, receipeType, product, receipesOfProduct, start, end);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#historyOfReceipe');
 	   		 });
    	}
    	
    	function createThinning(product, $event) { 
    		var thinning = {
            		productId: '',
            		date: new Date(),
            		ironContent: null,
            		targetContent: null,
            		density: null,
            		amountLiter: null,
            		amount: null,
            		iron: null,
            		water: null,
            		polymer: false,
            		ascorbinSaeure: false,
            		polymerSubtract: 1000,
            		ascorbinSaeureSubtract: 2700,
            		waterAmountCalculated: null
            	};
    		thinningService.findAllThinningsOfProduct(product.id).then(function(response) {
    			var allThinningsOfProduct = response.data;
    			createThinningModalService.showCreateThinningModal(vm.currentUser, product, null, thinning, allThinningsOfProduct, vm.userAuthorizationUserContainer, vm.polymerPromille);
 	   		 }, function errorCallback(response) {
 	   			 console.log('error search.controller#createReceipe');
 	   		 });
    	}
    	
    	function editAndDeleteThinning(product) { 
    		var end = new Date();
    		var start = dateUtilityService.oneMonthBack(end);
    		var startDateString = dateUtilityService.formatDateToString(start);
    		var endDateString = dateUtilityService.formatDateToString(end);
    		thinningService.findThinningsOfProductInRange(product.id, startDateString, endDateString).then(function(response) {
    			var thinningsOfProduct = response.data;
    			editThinningModalService.showEditThinningModal(vm.currentUser, product, null, thinningsOfProduct, start, end, vm.userAuthorizationUserContainer, vm.polymerPromille);
    		}, function errorCallback(response) {
	   			 console.log('error search.controller#editAndDeleteThinning');
	   		 });
    	}
    	
    	function purgeSearchString(searchString) {
    		if(searchString.indexOf('*') > -1) {
    			searchString = searchString.substr(0, searchString.length-1);
    		}
    		return searchString;
    	}
	} 
})();