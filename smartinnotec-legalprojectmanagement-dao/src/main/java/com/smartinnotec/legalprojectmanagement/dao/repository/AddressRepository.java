package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.Address;

public interface AddressRepository extends MongoRepository<Address, String> {

	@Query("{$or : [{'additionalInformation': { $regex: ?0, $options:'i' }}, {'street': { $regex: ?0, $options:'i' }}, {'region': { $regex: ?0, $options:'i' }}]}")
    List<Address> findAddressesBySearchString(final String searchString);


    @Query("{'geolocation': null}")
    List<Address> findAddressesWithoutGeolocation();
	
}
