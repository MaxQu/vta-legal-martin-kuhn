(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.controller('DashboardController', DashboardController);
      
    DashboardController.$inject = ['$scope', '$timeout', '$translate', 'ngToast', 'uploadService', 'currentUser', 'amountOfProducts', 'amountOfContactsImported', 'amountOfContactsToExport', 'amountOfContactsPreviousImported', 'amountOfContactsChanged', 'amountOfCommittedContactsChanged', 'calendarEventsOfNextWeeks', 'amountOfContacts', 'communicationUserConnectionsSent', 'communicationUserConnectionsReceived', 'sentAndOpenOrders', 'receivedAndOpenOrders', 'projects', 'lastLogin', 'countryTypes', 'titles', 'provinceTypes', 'contactTypes', 'documentFileService', 'communicationService', 'messageReceiverOverviewModalService', 'communicationMessageService', 'dashboardYesNoModalService', 'projectUserConnectionService', 'dateUtilityService', 'optionsService', 'productsService', 'userService', 'userAuthorizationUserContainer', 'adjustmentModalService', 'systemImportService', 'contactAndContactImportedModalService', 'contactsService', 'contactsImportedService', 'contactsPreviousImportedService', 'contactImportedYesNoModalService', 'contactExportYesNoModalService', 'contactsChangedService', 'contactChangedModalService'];
        
    function DashboardController($scope, $timeout, $translate, ngToast, uploadService, currentUser, amountOfProducts, amountOfContactsImported, amountOfContactsToExport, amountOfContactsPreviousImported, amountOfContactsChanged, amountOfCommittedContactsChanged, calendarEventsOfNextWeeks, amountOfContacts, communicationUserConnectionsSent, communicationUserConnectionsReceived, sentAndOpenOrders, receivedAndOpenOrders, projects, lastLogin, countryTypes, titles, provinceTypes, contactTypes, documentFileService, communicationService, messageReceiverOverviewModalService, communicationMessageService, dashboardYesNoModalService, projectUserConnectionService, dateUtilityService, optionsService, productsService, userService, userAuthorizationUserContainer, adjustmentModalService, systemImportService, contactAndContactImportedModalService, contactsService, contactsImportedService, contactsPreviousImportedService, contactImportedYesNoModalService, contactExportYesNoModalService, contactsChangedService, contactChangedModalService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	vm.teaserMessageLength = 100;
    	vm.currentUser = currentUser;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.projects = projects.data;
    	vm.amountOfProducts = amountOfProducts.data;
    	vm.amountOfContactsImported = amountOfContactsImported.data;
    	vm.amountOfContactsToExport = amountOfContactsToExport.data;
    	vm.amountOfContactsPreviousImported = amountOfContactsPreviousImported.data;
    	vm.amountOfContacts = amountOfContacts.data;
    	vm.amountOfContactsChanged = amountOfContactsChanged.data;
    	vm.amountOfCommittedContactsChanged = amountOfCommittedContactsChanged.data;
    	vm.productsOfUser = null;
    	vm.sentAndOpenOrders = sentAndOpenOrders != null ? sentAndOpenOrders.data : null;
    	vm.receivedAndOpenOrders = receivedAndOpenOrders != null ? receivedAndOpenOrders.data : null;
    	vm.countryTypes = countryTypes.data; 
    	vm.titles = titles.data;
    	vm.provinceTypes = provinceTypes.data;
    	vm.contactTypes = contactTypes.data;
    	vm.communicationUserConnectionSentOrder = '-communication.receiveDate';
    	vm.calendarEventsOfNextWeeks = calendarEventsOfNextWeeks != null ? calendarEventsOfNextWeeks.data : null;
    	vm.projectUserConnections = null;
    	vm.communicationUserConnectionsSent = communicationUserConnectionsSent != null ? communicationUserConnectionsSent.data : null;
    	vm.communicationUserConnectionsReceived = communicationUserConnectionsReceived != null ? communicationUserConnectionsReceived.data : null;
    	vm.communicationUserConnectionOfUserAndPage = null;
    	vm.lastLogin = lastLogin.data;
    	vm.todayDate = new Date(); 
    	vm.sentOrderType = '-sortDate';
    	vm.receivedOrderType = '-sortDate';
    	vm.addedProductSuccess = false;
    	vm.addProductError = false;
    	vm.addedProjectUserConnectionSuccess = false;
    	vm.addedProjectUserConnectionError = false;
    	vm.selectedProduct = null;
    	vm.selectedProjectUserConnection = null;
    	vm.options = {legend: {display: true}};
    	vm.labels = [];
    	vm.data = [];
    	vm.contactAndContactImportedContainers = [];
    	vm.contactsChanged = [];
    	vm.loadingContactsImported = false;
    	vm.contactsImportedPage = 0;
    	vm.contactImportedConfirmed = false;
    	vm.contactImportedDeleted = false;
    	vm.loadingContactsToExport = false;
    	vm.contactsToExportPage = 0;
    	vm.contactToExportConfirmed = false;
    	vm.contactToExportDeleted = false;
    	
    	vm.executionDateAfterToday = executionDateAfterToday;
    	vm.getAmountOfDocumentFilesWithUniqueName = getAmountOfDocumentFilesWithUniqueName;
    	vm.archiveCommunicationUserConnection = archiveCommunicationUserConnection;
    	vm.showMessageReceiverOverview = showMessageReceiverOverview;
    	vm.showCommunicationMessageModal = showCommunicationMessageModal;
    	vm.communicationOrderExecuted = communicationOrderExecuted;
    	vm.showYesNoModal = showYesNoModal;
    	vm.setAsDefaultProject = setAsDefaultProject;
    	vm.datesAreEqual = datesAreEqual;
    	vm.getTeasingMessage = getTeasingMessage;
    	vm.getProductsByTerm = getProductsByTerm;
    	vm.getProjectUserConnectionsByTerm = getProjectUserConnectionsByTerm;
    	vm.removeProduct = removeProduct;
    	vm.removeProjectUserConnection = removeProjectUserConnection;
    	vm.productSelected = productSelected;
    	vm.projectUserConnectionSelected = projectUserConnectionSelected;
    	vm.showAdjustmentModal = showAdjustmentModal;
    	vm.loadingPagedContactsImported = loadingPagedContactsImported;
    	vm.loadingPagedContactsToExport = loadingPagedContactsToExport;
    	vm.loadContactsImported = loadContactsImported;
    	vm.confirmContactImported = confirmContactImported;
    	vm.showContactAndContactImported = showContactAndContactImported;
    	vm.getCustomerNumbers = getCustomerNumbers;
    	vm.showDeleteContactImported = showDeleteContactImported;
    	vm.showDeleteContactExport = showDeleteContactExport;
    	vm.deleteContactImported = deleteContactImported;
    	vm.showContactChanged = showContactChanged;
    	vm.commitContactChanged = commitContactChanged;
    	vm.deleteContactChanged = deleteContactChanged;
    	//getProductsOfUser();
    	//getProjectUserConnectionsOfUser();
    	//getProductTypeOverview();
    	loadContactsImported(vm.contactsImportedPage);
    	loadContactsToExport(vm.contactsToExportPage);
    	
    	vm.releaseToastMsg = ngToast.warning({ 
  		  content: '<span class="glyphicon glyphicon-info-sign infoSign"></span> <span>Version 1.0.26 - Versionsinformationen durch Klicken der Versionsnummer in der Fußzeile</span>',
  		  timeout: 3000,
  		  dismissButton: true
  		});
    	
    	////////////
    	
    	function datesAreEqual(start, end) {
    		var result = dateUtilityService.longDatesAreEqual(start, end);
    		return result;
    	}
    	
    	
    	function getAmountOfContacts() {
			contactsService.countByTenantAndActive(vm.currentUser.id).then(function(response) {
				vm.amountOfContacts = response.data;
			}, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#amountOfContacts');
 	   		 });
		}
    	
		function getAmountOfContactsImported() {
			contactsImportedService.getAmountOfContactsImported().then(function(response) {
				vm.amountOfContactsImported = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#amountOfContactsImported');
 	   		 });
		}
		
		function getAmountOfContactsPreviousImported() {
			contactsPreviousImportedService.getAmountOfContactsPreviousImported().then(function(response) {
				vm.amountOfContactsPreviousImported = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#getAmountOfContactsPreviousImported');
 	   		 });
		}
		
		function getAmountOfContactsChanged() {
			contactsChangedService.getAmountOfContactsChanged().then(function(response) {
				vm.amountOfContactsChanged = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#getAmountOfContactsChanged');
 	   		 });
		}  
		
		function getAmountOfContactsChangedByCommitted(committed, getAmountOfContactsChangedByCommittedCallback) {
			contactsChangedService.getAmountOfContactsChangedByCommitted(committed).then(function(response) {
				var oldValue = null;
				if(committed) {
					oldValue = vm.amountOfCommittedContactsChanged;
					vm.amountOfCommittedContactsChanged = response.data;
					if(getAmountOfContactsChangedByCommittedCallback) {
						getAmountOfContactsChangedByCommittedCallback(oldValue, vm.amountOfCommittedContactsChanged);
					}
				} else {
					oldValue = vm.amountOfContactsToExport;
					vm.amountOfContactsToExport = response.data;
					if(getAmountOfContactsChangedByCommittedCallback) {
						getAmountOfContactsChangedByCommittedCallback(oldValue, vm.amountOfContactsToExport);
					}
				}
 	   		 }, function errorCallback(response) {
 	   			  console.log('error dashboard.controller#getAmountOfContactsChangedByCommitted');
 	   		 });
		}
		
    	function loadingPagedContactsImported() {
    		if(vm.loadingContactsImported == false) {
    			vm.loadingContactsImported = true;
	    		vm.contactsImportedPage = vm.contactsImportedPage + 1;
	    		loadContactsImported(vm.contactsImportedPage);
	    		vm.loadingContactsImported = false;
    		}
    	}
    	
    	function loadingPagedContactsToExport() {
			if(vm.loadingContactsToExport == false) {
    			vm.loadingContactsToExport = true;
	    		vm.contactsToExportPage = vm.contactsToExportPage + 1;
	    		loadContactsToExport(vm.contactsToExportPage);
	    		vm.loadingContactsToExport = false;
    		}
		}
    	
    	function loadContactsImported(page) {
    		if(vm.amountOfContactsImported > vm.contactAndContactImportedContainers.length) {
    			if(vm.contactAndContactImportedContainers == null) {
    				vm.contactAndContactImportedContainers = [];
    			}
    			systemImportService.getPagedContactsImported(page).then(function(response) {
	    			var loadedContactsImported = response.data;
	    			if(loadedContactsImported != null && loadedContactsImported.length > 0) {
	    				vm.contactAndContactImportedContainers.push.apply(vm.contactAndContactImportedContainers, loadedContactsImported);
		    			console.log("push contacts imported:" + vm.contactAndContactImportedContainers.length + ', ' + loadedContactsImported.length);
	    			}
	  	   		 }, function errorCallback(response) {
	  	   			 console.log('error products.controller#loadProducts');
	  	   		 });
    		}
    	}
    	
    	function loadContactsToExport(page) {
    		if(vm.amountOfContactsToExport > vm.contactsChanged.length) {
    			if(vm.contactsChanged == null) {
    				vm.contactsChanged = [];
    			}
    			contactsChangedService.getPagedContactsChangedByCommitted(page, false).then(function(response) {
	    			var loadedContactsToExport = response.data;
	    			if(loadedContactsToExport != null && loadedContactsToExport.length > 0) {
	    				vm.contactsChanged.push.apply(vm.contactsChanged, loadedContactsToExport);
		    			console.log("push contacts to export:" + vm.contactsChanged.length + ', ' + loadedContactsToExport.length);
	    			}
	  	   		 }, function errorCallback(response) {
	  	   			 console.log('error products.controller#loadContactsToExport');
	  	   		 });
    		}
    	}
    	
    	function communicationOrderExecuted(communicationUserConnectionOfUserAndPage) {
    		communicationService.updateCommunicationUserConnection(communicationUserConnectionOfUserAndPage).then(function(response) {
  	   		 }, function errorCallback(response) {
  	   			  console.log('error dashboard.controller#communicationOrderExecuted');
  	   		 }); 
    	}
    	
    	function executionDateAfterToday(executionDateString) {
    		if(executionDateString == null) {
    			return false;
    		}
    		var executionDate = dateUtilityService.convertToDateOrUndefined(executionDateString);
    		if(vm.todayDate.getTime() > executionDate.getTime()) {
    			return true;
    		}
    		return false;
    	}
    	
    	function getTeasingMessage(message) {
    		var teasingMessage = message.substr(0, vm.teaserMessageLength);
    		return teasingMessage;
    	}
    	 
    	function setUpdateProjectUserConnectionTimeout(projectUserConnection) {
      		$timeout(function() {
      			projectUserConnection.updatedSuccessfully = false;
  			   }, 2000); 
      	}
    	
    	function setTimeout() {
      		$timeout(function() {
      			vm.addedProjectUserConnectionSuccess = false;
      			vm.addProjectUserConnectionError = false;
      			vm.addedProductSuccess = false;
      			vm.addProductError = false;
      			vm.contactImportedConfirmed = false;
      	    	vm.contactImportedDeleted = false;
      	    	vm.contactToExportConfirmed = false;
      	    	vm.contactToExportDeleted = false;
  			   }, 3000); 
      	}

    	function setAsDefaultProject(projectUserConnection) {
    		for(var j = 0; j < vm.projectUserConnections.length; j++) {
    			if(vm.projectUserConnections[j].id != projectUserConnection.id && vm.projectUserConnections[j].defaultProject === true) {
						vm.projectUserConnections[j].defaultProject = false;
						projectUserConnectionService.updateProjectUserConnection(vm.projectUserConnections[j]).then(function(response) {
							vm.projectUserConnections[j] = response.data;
 		  	   		 }, function errorCallback(response) {
 		  	   			  console.log('error in dashboard.controller.js#setAsDefaultProject');
 		  	   		 });
				break;
				}
    		}
    		projectUserConnectionService.updateProjectUserConnection(projectUserConnection).then(function(response) {
    			projectUserConnection.updatedSuccessfully = true;
				setUpdateProjectUserConnectionTimeout(projectUserConnection);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error in dashboard.controller.js#setAsDefaultProject');
  	   		 });
    	}
    	
    	function getAmountOfDocumentFilesWithUniqueName(project) {
    		documentFileService.countByProjectIdGroupByOriginalFileName(project.id, false, 'PROJECT').then(function(response) {
    			project.amountOfDocumentFilesWithUniqueName = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error in dashboard.controller.js#getAmountOfDocumentFilesWithUniqueName');
  	   		 });
    	}
    	
    	function archiveCommunicationUserConnection(communicationUserConnection, updateType) {
    		communicationUserConnection.archived = true;
    		communicationService.updateCommunicationUserConnection(communicationUserConnection).then(function(response) {
    			if(updateType == 'SENT') {
    				removeCommunicationUserConnection(vm.communicationUserConnectionsSent, communicationUserConnection);
    			} else if(updateType == 'RECEIVED') {
    				removeCommunicationUserConnection(vm.communicationUserConnectionsReceived, communicationUserConnection);
    			}
  	   		 }, function errorCallback(response) {
  	   			  console.log('error dashboard.controller#archiveCommunicationUserConnection');
  	   		 });
    	}
    	
    	function removeCommunicationUserConnection(communicationUserConnections, communicationUserConnection) {
    		for(var i = 0; i < communicationUserConnections.length; i++) {
    			if(communicationUserConnections[i].id == communicationUserConnection.id) {
    				communicationUserConnections.splice(i, 1);
    				break;
    			}
    		}
    	}
    
    	function showMessageReceiverOverview(communicationUserConnection, confirmationNeeded) {
    		var messageId = communicationUserConnection.communication.id;
    		communicationService.getMessageReceiverOverview(messageId).then(function(response) {
    			var communicationUserConnections = response.data;
    			messageReceiverOverviewModalService.showMessageReceiverOverviewModal(currentUser, communicationUserConnections, confirmationNeeded);
  	   		 }, function errorCallback(response) {
  	   			  console.log('error dashboard.controller#showMessageReceiverOverview');
  	   		 });
    	}
    	
    	function getProjectUserConnectionsByTerm(term) { 
    		return projectUserConnectionService.findProjectUserConnectionsOfUserByProjectSearchString(vm.currentUser.id, term).then(function(response) {
	    		return response.data;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error dashboard.controller#getProjectsByTerm'); 
  	   		 });
		}
    	
    	function projectUserConnectionSelected(projectUserConnection) {
    		if(vm.currentUser.projectUserConnectionIds == null) {
    			vm.currentUser.projectUserConnectionIds = [];
    		}
    		if(projectUserConnectionStillExistsForUser(projectUserConnection.id) == true) {
    			vm.addProjectError = true;
    			setTimeout();
    			return;
    		}
    		vm.currentUser.projectUserConnectionIds.push(projectUserConnection.id);
    		userService.updateUser(vm.currentUser).then(function(response) {
    			vm.addedProjectUserConnectionSuccess = true;
    			vm.selectedProjectUserConnection = null;
    			setTimeout();
    			getProjectUserConnectionsOfUser();
 	   		 }, function errorCallback(response) {
 	   			 console.log('error dashboard.controller#projectUserConnectionSelected'); 
 	   		 });
    	}
    	
    	function getProductsByTerm(term) {
			return productsService.findProductsByTerm(term).then(function(response) {
	    		return response.data;
  	   		 }, function errorCallback(response) {
  	   			 console.log('error dashboard.controller#getProductsByTerm'); 
  	   		 });
		}
    	
    	function productSelected(product) {
    		if(vm.currentUser.productIds == null) {
    			vm.currentUser.productIds = [];
    		}
    		if(productStillExistsForUser(product.id) == true) {
    			vm.addProductError = true;
    			setTimeout();
    			return;
    		}
    		vm.currentUser.productIds.push(product.id);
    		userService.updateUser(vm.currentUser).then(function(response) {
    			vm.addedProductSuccess = true;
    			vm.selectedProduct = null;
    			setTimeout();
    			getProductsOfUser();
 	   		 }, function errorCallback(response) {
 	   			 console.log('error dashboard.controller#productSelected'); 
 	   		 });
    	}
    	
    	function removeProduct(product) {
    		for(var i = 0; i < vm.currentUser.productIds.length; i++) {
    			if(vm.currentUser.productIds[i] == product.id) {
    				vm.currentUser.productIds.splice(i, 1);
    				userService.updateUser(vm.currentUser).then(function(response) {
    	    			vm.addedProductSuccess = true;
    	    			setTimeout();
    	    			getProductsOfUser();
    	 	   		 }, function errorCallback(response) {
    	 	   			 console.log('error dashboard.controller#removeProduct'); 
    	 	   		 });
    				break;
    			}
    		}
    	}
    	
    	function removeProjectUserConnection(projectUserConnection) {
    		for(var i = 0; i < vm.currentUser.projectUserConnectionIds.length; i++) {
    			if(vm.currentUser.projectUserConnectionIds[i] == projectUserConnection.id) {
    				vm.currentUser.projectUserConnectionIds.splice(i, 1);
    				userService.updateUser(vm.currentUser).then(function(response) {
    	    			vm.addedProjectUserConnectionSuccess = true;
    	    			setTimeout();
    	    			getProjectUserConnectionsOfUser();
    	 	   		 }, function errorCallback(response) {
    	 	   			 console.log('error dashboard.controller#removeProjectUserConnection'); 
    	 	   		 });
    				break;
    			}
    		}
    	}
    	
    	function projectUserConnectionStillExistsForUser(projectUserConnectionId) {
    		for(var i = 0; i < vm.currentUser.projectUserConnectionIds.length; i++) {
    			if(vm.currentUser.projectUserConnectionIds[i] == projectUserConnectionId) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function productStillExistsForUser(productId) {
    		for(var i = 0; i < vm.currentUser.productIds.length; i++) {
    			if(vm.currentUser.productIds[i] == productId) {
    				return true;
    			}
    		}
    		return false;
    	}
    	
    	function getProductsOfUser() {
    		productsService.getProductsOfUser(vm.currentUser.id).then(function(response) {
    			vm.productsOfUser = response.data;
 	   		 }, function errorCallback(response) {
 	   			 console.log('error dashboard.controller#getProductsOfUser'); 
 	   		 });
    	}
    	
    	function getProjectUserConnectionsOfUser() { 
    		projectUserConnectionService.findSelectedProjectUserConnectionsByUser(vm.currentUser.id).then(function(response) {
    			vm.projectUserConnections = response.data;
 	   		 }, function errorCallback(response) {
 	   			 console.log('error dashboard.controller#getProjectUserConnectionsOfUser'); 
 	   		 });
    	}
    	
    	function getProductTypeOverview() {
    		productsService.getProductTypeOverview().then(function(response) {
    			var productTypeOverviewContainers = response.data;
    			for(var i = 0; i < productTypeOverviewContainers.length; i++) {
    				$translate(productTypeOverviewContainers[i].productType).then(function(text) {
    					vm.labels.push(text);
            		});
    				vm.data.push(productTypeOverviewContainers[i].amount);
    			}
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#getProductTypeOverview'); 
	   		 });
    	}
    	
    	function showCommunicationMessageModal(communicationUserConnectionSent, type) {
    		communicationMessageService.showCommunicationMessageModal(communicationUserConnectionSent, type);
    	}
    	
    	function showYesNoModal(communicationUserConnection, type) {
    		dashboardYesNoModalService.showYesNoModal(vm, communicationUserConnection, type);
    	}
    	
    	function showAdjustmentModal(type) {
    		adjustmentModalService.showAdjustmentModal(vm.currentUser, type);
    	}
    	
    	function confirmContactImported(contactAndContactImportedContainer, index) {
    		systemImportService.confirmContactImported(contactAndContactImportedContainer).then(function(response) {
    			vm.contactAndContactImportedContainers.splice(index, 1);
    			vm.contactImportedConfirmed = true;
    			getAmountOfContacts();
    			getAmountOfContactsImported();
    			getAmountOfContactsPreviousImported();
    			setTimeout();
    			getAmountOfContactsChangedByCommitted(false, getAmountOfContactsChangedByCommittedCallback);
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#confirmContactImported'); 
	   		 });
    	}
    	
    	function getAmountOfContactsChangedByCommittedCallback(oldValue, newValue) {
			vm.contactsChanged = [];
			vm.contactsToExportPage = 0;
			loadContactsToExport(vm.contactsToExportPage);
			getAmountOfContactsChanged();
    	}
    	
    	function showContactAndContactImported(contactAndContactImportedContainer, index) {
    		contactAndContactImportedModalService.showContactAndContactImportedModal(vm, vm.currentUser, contactAndContactImportedContainer, index, vm.countryTypes, vm.titles, vm.provinceTypes, vm.contactTypes);
    	}
    	
    	function getCustomerNumbers(customerNumberContainers) {
    		var customerNumbers = '';
    		for(var i = 0; i < customerNumberContainers.length; i++) {
    			customerNumbers += customerNumberContainers[i].customerNumber + ' ';
    		}
    		return customerNumbers;
    	}
    	
    	function showDeleteContactImported(contactImported, index) {
    		contactImportedYesNoModalService.showContactImportedYesNoModal(vm, contactImported, index);
    	}
    	
    	function showDeleteContactExport(contactChanged, index) {
    		contactExportYesNoModalService.showContactExportYesNoModal(vm, contactChanged, index);
    	}
    	
    	function showContactChanged(contactChanged, index) {
    		contactChangedModalService.showContactChangedModal(vm, vm.currentUser, contactChanged, index, vm.countryTypes, vm.titles, vm.provinceTypes, vm.contactTypes);
    	}
    	
    	function commitContactChanged(contactChanged, index) {
    		contactsChangedService.commitContactChanged(contactChanged.id).then(function(response) {
    			vm.contactsChanged.splice(index, 1);
    			getAmountOfContactsChanged();
    			getAmountOfContactsChangedByCommitted(true);
    			getAmountOfContactsChangedByCommitted(false);
    			vm.contactToExportConfirmed = true;
    			setTimeout();
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#commitContactChanged'); 
	   		 });
    	}
    	
    	function deleteContactImported(contactImported, index) {
    		contactsImportedService.deleteContactImported(contactImported.id).then(function(response) {
    			vm.contactAndContactImportedContainers.splice(index, 1);
    	    	vm.contactImportedDeleted = true;
    			getAmountOfContacts();
    			getAmountOfContactsImported();
    			getAmountOfContactsPreviousImported();
    			setTimeout();
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#deleteContactImported'); 
	   		 });
    	}
    	
    	function deleteContactChanged(contactChanged, index) {
    		contactsChangedService.deleteContactChanged(contactChanged.id).then(function(response) {
    			vm.contactsChanged.splice(index, 1);
    			getAmountOfContactsChanged();
    			getAmountOfContactsChangedByCommitted(true);
    			getAmountOfContactsChangedByCommitted(false);
    			vm.contactToExportDeleted = true;
    			setTimeout();
	   		 }, function errorCallback(response) {
	   			 console.log('error dashboard.controller#deleteContactChanged'); 
	   		 });
    	}
	} 
})();