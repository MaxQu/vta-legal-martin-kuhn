package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressPreviousImported;

public interface AddressPreviousImportedService {

    AddressPreviousImported create(final AddressPreviousImported addressPreviousImported) throws BusinessException;

    AddressPreviousImported convertFromAddressImportedToAddressPreviousImported(final AddressImported addressImported)
            throws BusinessException;

}
