package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(locations = "classpath:application.properties", ignoreUnknownFields = true, prefix = "legalprojectmanagement.systemImport")
public @Data class SystemImportConfirgurationBean {

    private String pathAddress;
    private String pathContact;

}
