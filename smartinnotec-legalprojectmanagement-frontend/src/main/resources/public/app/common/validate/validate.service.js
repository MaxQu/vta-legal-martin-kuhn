(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.factory('validateService', validateService);
        
    validateService.$inject = ['$http', 'api_config'];
    
    function validateService($http, api_config) {
		var service = {
			checkUniqueUsername: checkUniqueUsername,
			checkUniqueEmail: checkUniqueEmail,
			checkUniqueMandator: checkUniqueMandator,
			checkUniqueProjectName: checkUniqueProjectName,
			checkUniqueProductName: checkUniqueProductName,
			checkUniqueProductNumber: checkUniqueProductNumber
		};
		
		return service;
		
		////////////
		
		function checkUniqueUsername(username) {
			return $http.get(api_config.BASE_URL + '/checkUniqueUsername?username=' + username);
		}
		
		function checkUniqueEmail(email) {
			return $http.get(api_config.BASE_URL + '/checkUniqueEmail?email=' + email);
		}
		
		function checkUniqueMandator(mandator) {
			return $http.get(api_config.BASE_URL + '/checkUniqueMandator?mandator=' + mandator);
		}
		
		function checkUniqueProjectName(projectName) {
			return $http.get(api_config.BASE_URL + '/projects/project/checkuniqueprojectname?projectName=' + projectName);
		}
		
		function checkUniqueProductName(productName, type) {
			var encodedProductName = encodeURIComponent(productName);
			return $http.get(api_config.BASE_URL + '/products/product/checkuniqueproductname/' + type + '?productName=' + encodedProductName);
		}
		
		function checkUniqueProductNumber(productNumber, type) {
			var encodedProductNumber= encodeURIComponent(productNumber);
			return $http.get(api_config.BASE_URL + '/products/product/checkuniqueproductnumber/' + type + '?productNumber=' + encodedProductNumber);
		}
    }
})();