(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectUserConnectionService', projectUserConnectionService);
        
    projectUserConnectionService.$inject = ['$http', 'api_config'];
    
    function projectUserConnectionService($http, api_config) {
		var service = {
			findProjectUserConnectionByUser: findProjectUserConnectionByUser,
			findProjectUserConnectionByProject: findProjectUserConnectionByProject,
			findUsersOfProjectUserConnectionByProject: findUsersOfProjectUserConnectionByProject,
			findProjectUserConnectionById: findProjectUserConnectionById,
			findProjectUserConnectionsByContactAndProject: findProjectUserConnectionsByContactAndProject,
			findProjectUserConnectionsByContactAndUser: findProjectUserConnectionsByContactAndUser,
			findProjectUserConnectionsOfUserByProjectSearchString: findProjectUserConnectionsOfUserByProjectSearchString,
			findSelectedProjectUserConnectionsByUser: findSelectedProjectUserConnectionsByUser,
			createProjectUserConnection: createProjectUserConnection,
			updateProjectUserConnection: updateProjectUserConnection,
			deleteProjectUserConnection: deleteProjectUserConnection,
			deleteProjectUserConnectionOverProjectAndContact: deleteProjectUserConnectionOverProjectAndContact, 
			deleteProjectUserConnectionOverUserAndContact: deleteProjectUserConnectionOverUserAndContact, 
			canProjectUserConnectionRoleSeeConfidentialDocumentFiles: canProjectUserConnectionRoleSeeConfidentialDocumentFiles
		}; 					
		
		return service;
		
		////////////
		
		function findProjectUserConnectionByUser(userId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/user/' + userId);
		}
		
		function findProjectUserConnectionByProject(projectId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/project/' + projectId);
		}
		
		function findUsersOfProjectUserConnectionByProject(projectId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/project/users/' + projectId);
		}
		
		function findProjectUserConnectionById(projectUserConnectionId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/' + projectUserConnectionId);
		}
		
		function findProjectUserConnectionsByContactAndProject(contactId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/contact/' + contactId + '/project');
		}
		
		function findProjectUserConnectionsByContactAndUser(contactId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/contact/' + contactId + '/user');
		}
		
		function findProjectUserConnectionsOfUserByProjectSearchString(userId, searchTerm) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/' + userId + '/' + searchTerm + '/search');
		}
		
		function findSelectedProjectUserConnectionsByUser(userId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/user/' + userId + '/selected');
		}
		
		function createProjectUserConnection(projectUserConnection) {
			return $http.post(api_config.BASE_URL + '/projectuserconnections', projectUserConnection);
		}
		
		function updateProjectUserConnection(projectUserConnection) {
			return $http.put(api_config.BASE_URL + '/projectuserconnections', projectUserConnection);
		}
		
		function deleteProjectUserConnection(projectUserConnectionId) {
			return $http.delete(api_config.BASE_URL + '/projectuserconnections/' + projectUserConnectionId);
		}
		
		function deleteProjectUserConnectionOverProjectAndContact(projectId, contactId) {
			return $http.delete(api_config.BASE_URL + '/projectuserconnections/' + projectId + '/' + contactId + '/project/contact');
		}
		
		function deleteProjectUserConnectionOverUserAndContact(userId, contactId) {
			return $http.delete(api_config.BASE_URL + '/projectuserconnections/' + userId + '/' + contactId + '/user/contact');
		}
		
		function canProjectUserConnectionRoleSeeConfidentialDocumentFiles(projectUserConnectionId) {
			return $http.get(api_config.BASE_URL + '/projectuserconnections/confidentialdocumentfiles/' + projectUserConnectionId);
		}
    }
})();