package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;

public interface AddressImportedService {

    boolean hasAddressImportedChanged(final AddressImported address) throws BusinessException;

    AddressImported create(final AddressImported addressImported) throws BusinessException;

    AddressImported update(final AddressImported addressImported) throws BusinessException;

    void delete(final AddressImported addressImported) throws BusinessException;

    void deleteAll() throws BusinessException;

}
