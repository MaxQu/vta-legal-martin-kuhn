package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroup;

public interface CommunicationGroupRepository extends MongoRepository<CommunicationGroup, String> {

}
