(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('userOfCalendarEventModalService', userOfCalendarEventModalService);
        
    userOfCalendarEventModalService.$inject = ['$modal', '$stateParams', 'calendarService'];
    
    function userOfCalendarEventModalService($modal, $stateParams, calendarService) {
		var service = {
			showUserOfCalendarEventModal: showUserOfCalendarEventModal
		};		
		return service;
		
		////////////
				
		function showUserOfCalendarEventModal(calendarEvent, calendarEventUserConnections, invoker) {
			 var userOfCalendarEventModal = $modal.open({
				controller: UserOfCalendarEventModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEvent: function() {
			    		return calendarEvent;
			    	},
			    	calendarEventUserConnections: function() {
			    		return calendarEventUserConnections;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/userOfCalendarEvent/userOfCalendarEvent.modal.html'
			});
			return userOfCalendarEventModal;
			
			function UserOfCalendarEventModalController($modalInstance, $scope, calendarEvent, calendarEventUserConnections, invoker) {
		    	var vm = this;
		    	vm.calendarEvent = calendarEvent;
		    	vm.calendarEventUserConnections = calendarEventUserConnections;
		    	
		    	vm.closeUserOfCalendarEventModal = closeUserOfCalendarEventModal;
		    	
		    	function closeUserOfCalendarEventModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();