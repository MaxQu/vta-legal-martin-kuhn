package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;

public interface ContactImportedRepository extends MongoRepository<ContactImported, String> {

    ContactImported findByCustomerNumberContainersIn(final CustomerNumberContainer customerNumberContainer);

}
