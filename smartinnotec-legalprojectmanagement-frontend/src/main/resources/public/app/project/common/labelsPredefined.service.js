(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('labelsPredefinedService', labelsPredefinedService);
        
    labelsPredefinedService.$inject = ['$http', 'api_config'];
    
    function labelsPredefinedService($http, api_config) {
		var service = {
			createLabelPredefined: createLabelPredefined,
			getAllLabelsPredefined: getAllLabelsPredefined,
			deleteLabelPredefined: deleteLabelPredefined 
		};
		return service;
		
		////////////

		function createLabelPredefined(labelPredefined) {
			return $http.post(api_config.BASE_URL + '/labelspredefined', labelPredefined);
		}
		
		function getAllLabelsPredefined(userId) {
			return $http.get(api_config.BASE_URL + '/labelspredefined/' + userId);
		}
		
		function deleteLabelPredefined(label) {
			return $http.delete(api_config.BASE_URL + '/labelspredefined/' + label);
		}
    }
})();
