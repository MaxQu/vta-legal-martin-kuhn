package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationUserContainer;

public interface UserAuthorizationUserContainerRepository extends MongoRepository<UserAuthorizationUserContainer, String> {

    UserAuthorizationUserContainer findByUserId(final String userId);

}
