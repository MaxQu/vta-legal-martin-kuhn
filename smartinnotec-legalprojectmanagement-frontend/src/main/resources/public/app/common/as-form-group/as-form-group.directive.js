(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('asFormGroup', asFormGroup);
    					
	function asFormGroup() {
		var directive = {
			restrict: 'E',
			replace: true,
			transclude: true,
			scope: {
				asLabel: '@'
			},
			templateUrl: 'app/common/as-form-group/as-form-group.directive.html'
		};
		
		return directive;
	}
})();