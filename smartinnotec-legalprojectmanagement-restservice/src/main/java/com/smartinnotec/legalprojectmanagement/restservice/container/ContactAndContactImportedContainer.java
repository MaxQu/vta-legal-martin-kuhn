package com.smartinnotec.legalprojectmanagement.restservice.container;

import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;

import lombok.Data;

public @Data class ContactAndContactImportedContainer {

    private Contact contact;
    private ContactImported contactImported;

}
