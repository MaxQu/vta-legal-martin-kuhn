(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.accesscontrol')
    	.factory('printAccessControlModalService', printAccessControlModalService);
        
    printAccessControlModalService.$inject = ['$modal', '$stateParams'];
    
    function printAccessControlModalService($modal, $stateParams) {
		var service = {
			showPrintAccessControlModal: showPrintAccessControlModal
		};		
		return service;
		
		////////////
				
		function showPrintAccessControlModal(accessControls) {
			 var printAccessControlModal = $modal.open({
				controller: PrintAccessControlModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	accessControls: function() { 
			    		return accessControls;
			    	}
			    }, 
				templateUrl: 'app/accesscontrol/modals/printAccessControl/printAccessControl.modal.html'
			});
			return printAccessControlModal;
			
			function PrintAccessControlModalController($modalInstance, $scope, accessControls) {
		    	var vm = this;
		    	vm.today = new Date();
		    	vm.accessControls = accessControls;
		    	
		    	vm.closePrintAccessControlModal = closePrintAccessControlModal;
		    	
		    	////////
		    	
		    	function closePrintAccessControlModal() {  
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();