package com.smartinnotec.legalprojectmanagement.service.base.test;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.SmartinnotecSpringMongoDBConfig;
import com.smartinnotec.legalprojectmanagement.dao.repository.ActivityRepositoryDAO;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactRepositoryDAO;
import com.smartinnotec.legalprojectmanagement.service.ActivityFilesUploadConfigurationBean;
import com.smartinnotec.legalprojectmanagement.service.ActivityServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.AddressChangedServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.AddressService;
import com.smartinnotec.legalprojectmanagement.service.AddressServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.CalendarConfigurationService;
import com.smartinnotec.legalprojectmanagement.service.CalendarConfigurationServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.CalendarEventService;
import com.smartinnotec.legalprojectmanagement.service.CalendarEventServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.CommunicationServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.CommunicationUserConnectionServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.ContactChangedService;
import com.smartinnotec.legalprojectmanagement.service.ContactChangedServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.ContactComparation;
import com.smartinnotec.legalprojectmanagement.service.ContactService;
import com.smartinnotec.legalprojectmanagement.service.ContactServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.DocumentFileConfigurationBean;
import com.smartinnotec.legalprojectmanagement.service.DocumentFileService;
import com.smartinnotec.legalprojectmanagement.service.DocumentFileServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.FacilityDetailsService;
import com.smartinnotec.legalprojectmanagement.service.FacilityDetailsServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.FileUploadServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.FolderServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.HistoryAmountService;
import com.smartinnotec.legalprojectmanagement.service.HistoryAmountServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.HistoryServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.I18NServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.LabelsPredefinedServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.ProductService;
import com.smartinnotec.legalprojectmanagement.service.ProductServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.ProjectService;
import com.smartinnotec.legalprojectmanagement.service.ProjectServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.ProjectUserConnectionService;
import com.smartinnotec.legalprojectmanagement.service.ProjectUserConnectionServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.ReceipeService;
import com.smartinnotec.legalprojectmanagement.service.ReceipeServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.SearchService;
import com.smartinnotec.legalprojectmanagement.service.SearchServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.TenantService;
import com.smartinnotec.legalprojectmanagement.service.TenantServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.ThinningServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.TimeRegistrationServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.UserService;
import com.smartinnotec.legalprojectmanagement.service.UserServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.UtilService;
import com.smartinnotec.legalprojectmanagement.service.UtilServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.WorkingBookServiceImpl;
import com.smartinnotec.legalprojectmanagement.service.schedule.CalendarEventOutsideWorkScheduleService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
    ContactRepositoryDAO.class, SmartinnotecSpringMongoDBConfig.class, ContactComparation.class, DocumentFileServiceImpl.class,
    ProjectServiceImpl.class, ProjectUserConnectionServiceImpl.class, ProductServiceImpl.class, ContactServiceImpl.class,
    LabelsPredefinedServiceImpl.class, ReceipeServiceImpl.class, DocumentFileConfigurationBean.class, HistoryServiceImpl.class,
    AddressServiceImpl.class, UserServiceImpl.class, CalendarConfigurationServiceImpl.class, TimeRegistrationServiceImpl.class,
    CommunicationServiceImpl.class, TenantServiceImpl.class, CommunicationUserConnectionServiceImpl.class,
    SearchServiceImpl.class, ThinningServiceImpl.class,
    I18NServiceImpl.class, FileUploadServiceImpl.class, FolderServiceImpl.class, WorkingBookServiceImpl.class, ActivityServiceImpl.class,
    FacilityDetailsServiceImpl.class, CalendarEventServiceImpl.class,
     AddressChangedServiceImpl.class, UtilServiceImpl.class,
    HistoryAmountServiceImpl.class, ContactChangedServiceImpl.class, ActivityFilesUploadConfigurationBean.class,
    ActivityRepositoryDAO.class })

public abstract class AbstractServiceTest {

    protected DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
    protected DateTimeFormatter formatterDate = DateTimeFormat.forPattern("dd.MM.yyyy");
    protected DateTimeFormatter formatterHours = DateTimeFormat.forPattern("HH:mm");

    @Autowired
    protected ReceipeService receipeService;
    @Autowired
    protected UserService userService;
    @Autowired
    protected AddressService addressService;
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected ProductService productService;
    @Autowired
    protected ContactService contactService;
    @Autowired
    protected TenantService tenantService;
    @Autowired
    protected DocumentFileService documentFileService;
    @Autowired
    protected SearchService searchService;
    @Autowired
    protected FacilityDetailsService facilityDetailsService;
    @Autowired
    protected CalendarEventService calendarEventService;
    @Autowired
    protected CalendarConfigurationService calendarConfigurationService;
    @Autowired
    protected CalendarEventOutsideWorkScheduleService calendarEventOutsideWorkScheduleService;
    @Autowired
    protected ProjectUserConnectionService projectUserConnectionService;
    @Autowired
    protected HistoryAmountService historyAmountService;
    @Autowired
    protected ContactChangedService contactChangedService;
    @Autowired
    protected UtilService utilService;

    @Before
    public void setUp() throws BusinessException {
        /*
         * productService.deleteAll(); tenantService.deleteAll(); userService.deleteAllUsers();
         * calendarEventService.deleteAll(); calendarEventUserConnectionService.deleteAll();
         */
    }
}
