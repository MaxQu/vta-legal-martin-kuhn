(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.home')
    	.config(configure)
    	.run(['$state', function ($state) {}]); //TODO use reoutehelperprovider see styleguide ?
    
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];
    
    function configure($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/signin');
    	
    	$stateProvider
    		.state(getHomeState())
    		.state(getSigninState())
    		.state(getSignupState())
    		.state(getMisrememberPasswordState());
    	
		////////////
		
		function getHomeState() {
			var state = {
				name: 'home',
				abstract: true,
				views: {
					'header': {},
					'content': {
						templateUrl: 'app/home/core/home.html'
					},
					'footer': {}
				}
			};
			return state;
		}
		
    	function getSigninState() {
    		var state = {
    			name: 'home.signin',
				url: '/signin',
				templateUrl: 'app/home/signin/signin.html',
				controller: 'SigninController',
				controllerAs: 'vm'
    		};
    		return state;
    	}
    	
    	function getSignupState() {
    		var state = {
    			name: 'home.signup',
				url: '/signup',
				templateUrl: 'app/home/signup/signup.html',
				controller: 'SignupController',
				controllerAs: 'vm'
    		};
    		return state;
    	}
    	
    	function getMisrememberPasswordState() {
    		var state = {
    			name: 'home.misrememberPassword',
				url: '/misremember-password',
				templateUrl: 'app/home/misremember-password/misremember-password.html',
				controller: 'MisrememberPasswordController',
				controllerAs: 'vm'
    		};
    		return state;
    	}
	}
})();