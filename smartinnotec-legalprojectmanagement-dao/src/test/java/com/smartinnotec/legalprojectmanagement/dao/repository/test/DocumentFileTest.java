package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public class DocumentFileTest extends AbstractDAOTest {

    @Test
    public void shouldCountByProjectIdGroupByFileName() throws BusinessException {

        final User user = new User();
        user.setFirstname("Max");
        user.setSurname("Mustermann");
        final User createdUser = userRepository.insert(user);

        final Project project = new Project();
        project.setName("TestProject1");
        final Project createdProject = projectRepository.insert(project);
        Assert.assertNotNull(createdProject);

        final DocumentFile documentFile1 = new DocumentFile();
        documentFile1.setFileName("DocumentFile1");
        documentFile1.setEnding("jpg");
        documentFile1.setProjectProductId(createdProject.getId());
        documentFile1.addUserIdBlackList(createdUser.getId());
        final DocumentFile createdDocumentFile1 = documentFileRepository.insert(documentFile1);
        Assert.assertNotNull(createdDocumentFile1);

        final DocumentFile documentFile2 = new DocumentFile();
        documentFile2.setFileName("DocumentFile2");
        documentFile2.setEnding("jpg");
        documentFile2.setProjectProductId(createdProject.getId());
        final DocumentFile createdDocumentFile2 = documentFileRepository.insert(documentFile2);
        Assert.assertNotNull(createdDocumentFile2);

        projectRepository.save(createdProject);

        final Integer list1 = documentFileRepository.countByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(project.getId(),
            user.getId(), true, true);
        System.out.println(list1);

        final Integer list2 = documentFileRepository.countByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(project.getId(), "", true,
            true);
        System.out.println(list2);
    }

    @Test
    public void shouldFindPagedDocumentFilesByProjectIdAndUserIdBlackListNotInAndConfidentAndActiveOrUserIdWhiteListIn() {
        final User user = new User();
        user.setFirstname("Max");
        user.setSurname("Mustermann");
        final User createdUser = userRepository.insert(user);

        final Project project = new Project();
        project.setName("TestProject1");
        final Project createdProject = projectRepository.insert(project);
        Assert.assertNotNull(createdProject);

        final DocumentFile documentFile1 = new DocumentFile();
        documentFile1.setFileName("DocumentFile1");
        documentFile1.setEnding("jpg");
        documentFile1.setProjectProductId(createdProject.getId());
        documentFile1.addUserIdBlackList(createdUser.getId());
        documentFile1.setActive(true);
        final DocumentFile createdDocumentFile1 = documentFileRepository.insert(documentFile1);
        Assert.assertNotNull(createdDocumentFile1);

        final DocumentFile documentFile2 = new DocumentFile();
        documentFile2.setFileName("DocumentFile2");
        documentFile2.setEnding("jpg");
        documentFile2.setProjectProductId(createdProject.getId());
        documentFile2.addUserIdWhiteList(createdUser.getId());
        documentFile2.setConfident(true);
        documentFile2.setActive(true);
        final DocumentFile createdDocumentFile2 = documentFileRepository.insert(documentFile2);
        Assert.assertNotNull(createdDocumentFile2);

        projectRepository.save(createdProject);

        final Pageable pageable = new PageRequest(0, 20, new Sort(Sort.Direction.ASC, "fileName"));
        final List<DocumentFile> foundedDocumentFiles = documentFileRepository
            .findPagedDocumentFilesByProjectProductIdAndFolderIdAndUserIdBlackListNotInAndConfidentAndActiveOrUserIdWhiteListIn(project.getId(),
                "", createdUser.getId(), confidentIsFalse(), activeIsTrue(), createdUser.getId(), pageable);
        Assert.assertEquals(1, foundedDocumentFiles.size());
    }

    private boolean confidentIsFalse() {
        return false;
    }

    private boolean activeIsTrue() {
        return true;
    }
}
