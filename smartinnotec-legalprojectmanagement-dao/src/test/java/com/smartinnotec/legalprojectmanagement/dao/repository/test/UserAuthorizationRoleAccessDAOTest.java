package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Test;
import org.springframework.util.Assert;

import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationRoleAccess;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationStateEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationTypeEnum;

public class UserAuthorizationRoleAccessDAOTest extends AbstractDAOTest {

    @Test
    public void shouldCreateUserAuthorizationRoleAccess() {
        final UserAuthorizationRoleAccess userAuthorizationRoleAccess = new UserAuthorizationRoleAccess();
        userAuthorizationRoleAccess.setUserAuthorizationType(UserAuthorizationTypeEnum.PROJECT);
        userAuthorizationRoleAccess.setProjectUserConnectionRole(ProjectUserConnectionRoleEnum.ADMIN);
        userAuthorizationRoleAccess.setUserAuthorizationState(UserAuthorizationStateEnum.READ);
        userAuthorizationRoleAccess.setAccess(true);
        final UserAuthorizationRoleAccess createdUserAuthorizationRoleAccess = userAuthorizationRoleAccessRepository
            .insert(userAuthorizationRoleAccess);
        Assert.notNull(createdUserAuthorizationRoleAccess);
    }
}
