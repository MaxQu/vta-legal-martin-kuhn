(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('systemImportService', systemImportService);
        
    systemImportService.$inject = ['$http', 'api_config'];
    
    function systemImportService($http, api_config) {
		var service = {
			getContactsImported: getContactsImported,
			confirmContactImported: confirmContactImported,
			getPagedContactsImported: getPagedContactsImported,
			mergeContactsWithAddressesAndSave: mergeContactsWithAddressesAndSave,
			mergeContactsWithAddressesAndSaveOfStandardPath: mergeContactsWithAddressesAndSaveOfStandardPath
		};
		
		return service;
		
		////////////
		
		function getContactsImported() {
			return $http.get(api_config.BASE_URL + '/systemimports/systemimport/contactimported');
		}
		
		function confirmContactImported(contactAndContactImportedContainer) {
			return $http.put(api_config.BASE_URL + '/systemimports/systemimport/confirm', contactAndContactImportedContainer);
		}
		
		function getPagedContactsImported(page) {
			return $http.get(api_config.BASE_URL + '/systemimports/systemimport/' + page);			
		}
		  
		function mergeContactsWithAddressesAndSave(pathAddress, pathContact, callback, errorCallback) {
			var systemImportContainer = {};
			systemImportContainer.pathAddress = pathAddress;
			systemImportContainer.pathContact = pathContact;
			return $http.put(api_config.BASE_URL + '/systemimports/systemimport', systemImportContainer).then(function(resp) {
            	if (callback) {
            		callback(resp);
                }
            }, function(response) {
                if(errorCallback) {
                	errorCallback(response);
                }
            });
		}
		
		function mergeContactsWithAddressesAndSaveOfStandardPath() {
			return $http.put(api_config.BASE_URL + '/systemimports/systemimport/ofstandardpath');
		}
    }
})();
