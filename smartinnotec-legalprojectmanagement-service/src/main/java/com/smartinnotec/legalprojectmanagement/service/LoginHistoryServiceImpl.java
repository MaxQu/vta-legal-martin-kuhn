package com.smartinnotec.legalprojectmanagement.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.LoginHistory;
import com.smartinnotec.legalprojectmanagement.dao.domain.LoginHistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.LoginHistoryRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.UserRepository;

@Service("loginHistoryService")
public class LoginHistoryServiceImpl extends AbstractService implements LoginHistoryService {

    @Autowired
    private LoginHistoryRepository LoginHistoryRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public LoginHistory create(final LoginHistory loginHistory) throws BusinessException {
        BusinessAssert.notNull(loginHistory, "loginHistory is mandatory in LoginHistoryServiceImpl#create", "400");
        logger.info("create loginHistory in LoginHistoryServiceImpl#create");
        return LoginHistoryRepository.insert(loginHistory);
    }

    @Override
    public void setCurrentAndLastLoginHistory(final String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in LoginHistoryServiceImpl#setCurrentLoginToLastLogin", "400");
        BusinessAssert.isId(userId, "userId must be an id in LoginHistoryServiceImpl#setCurrentLoginToLastLogin", "400");
        logger.info(
            "set current loginHistory to last loginHistory for user with id '{}' in LoginHistoryServiceImpl#setCurrentLoginToLastLogin",
            userId);
        final User user = userRepository.findOne(userId);

        final LoginHistory currentLoginHistory = LoginHistoryRepository.findByLoginHistoryTypeAndUser(LoginHistoryTypeEnum.CURRENT_LOGIN,
            user);
        final LoginHistory lastLoginHistory = LoginHistoryRepository.findByLoginHistoryTypeAndUser(LoginHistoryTypeEnum.LAST_LOGIN, user);

        if (lastLoginHistory != null && currentLoginHistory != null) {
            lastLoginHistory.setLoginDateTime(currentLoginHistory.getLoginDateTime());
            lastLoginHistory.setLoginHistoryType(LoginHistoryTypeEnum.LAST_LOGIN);
            final LoginHistory updatedLastLoginHistory = LoginHistoryRepository.save(lastLoginHistory);
            logger.info("updated lastLoginHistory with id '{}' in LoginHistoryServiceImpl#setCurrentLoginToLastLogin",
                updatedLastLoginHistory.getId());
            currentLoginHistory.setLoginDateTime(new DateTime(System.currentTimeMillis()));
            final LoginHistory updatedCurrentLoginHistory = LoginHistoryRepository.save(currentLoginHistory);
            logger.info("updated currentLoginHistory with id '{}' in LoginHistoryServiceImpl#setCurrentLoginToLastLogin",
                updatedCurrentLoginHistory.getId());
        } else if (lastLoginHistory == null && currentLoginHistory != null) {
            final LoginHistory newLastLoginHistory = new LoginHistory();
            newLastLoginHistory.setLoginDateTime(currentLoginHistory.getLoginDateTime());
            newLastLoginHistory.setLoginHistoryType(LoginHistoryTypeEnum.LAST_LOGIN);
            newLastLoginHistory.setUser(currentLoginHistory.getUser());
            final LoginHistory createdLoginHistory = LoginHistoryRepository.insert(newLastLoginHistory);
            logger.info("created loginHistory with id '{}' in LoginHistoryServiceImpl#setCurrentLoginToLastLogin",
                createdLoginHistory.getId());
        } else if (currentLoginHistory == null) {
            final LoginHistory newLastLoginHistory = new LoginHistory();
            newLastLoginHistory.setLoginDateTime(new DateTime(System.currentTimeMillis()));
            newLastLoginHistory.setLoginHistoryType(LoginHistoryTypeEnum.CURRENT_LOGIN);
            newLastLoginHistory.setUser(user);
            final LoginHistory createdLoginHistory = LoginHistoryRepository.insert(newLastLoginHistory);
            logger.info("created loginHistory with id '{}' in LoginHistoryServiceImpl#setCurrentLoginToLastLogin",
                createdLoginHistory.getId());
        }

    }

    @Override
    public LoginHistory update(final LoginHistory loginHistory) throws BusinessException {
        BusinessAssert.notNull(loginHistory, "loginHistory is mandatory in LoginHistoryServiceImpl#create", "400");
        logger.info("update loginHistory in LoginHistoryServiceImpl#update");
        return LoginHistoryRepository.save(loginHistory);
    }

    @Override
    public LoginHistory findByLoginHistoryTypeAndUser(final LoginHistoryTypeEnum loginHistoryType, final User user)
            throws BusinessException {
        BusinessAssert.notNull(loginHistoryType, "loginHistoryType is mandatory in LoginHistoryServiceImpl#findByLoginHistoryTypeAndUser",
            "400");
        BusinessAssert.notNull(user, "user is mandatory in LoginHistoryServiceImpl#findByLoginHistoryTypeAndUser", "400");
        logger.info(
            "find loginHistory by loginHistoryType of '{}' and user with id '{}' in LoginHistoryServiceImpl#findByLoginHistoryTypeAndUser",
            loginHistoryType, user.getId());
        return LoginHistoryRepository.findByLoginHistoryTypeAndUser(loginHistoryType, user);
    }
}
