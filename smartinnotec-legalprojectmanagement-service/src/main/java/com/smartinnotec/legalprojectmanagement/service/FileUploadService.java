package com.smartinnotec.legalprojectmanagement.service;

import java.io.File;

import org.springframework.web.multipart.MultipartFile;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;

public interface FileUploadService {

    String uploadFile(final String foldername, final String filename, final MultipartFile multipartFile) throws BusinessException;

    boolean isImage(final File file) throws BusinessException;
}
