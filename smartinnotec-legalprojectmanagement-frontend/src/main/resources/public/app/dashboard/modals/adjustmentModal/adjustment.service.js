(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('adjustmentModalService', adjustmentModalService);
        
    adjustmentModalService.$inject = ['$modal', '$stateParams', 'userService'];
    
    function adjustmentModalService($modal, $stateParams, userService) {
		var service = {
			showAdjustmentModal: showAdjustmentModal
		};		
		return service;
		
		////////////
				
		function showAdjustmentModal(user, type) {
			 var adjustmentModal = $modal.open({
				controller: AdjustmentModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",  
			    resolve: {
			    	user: function() {
			    		return user;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/adjustmentModal/adjustment.modal.html'
			});
			return adjustmentModal;
			
			function AdjustmentModalController($modalInstance, $scope, user, type) {
		    	var vm = this; 
		    	vm.user = user;
		    	vm.type = type;
		    	vm.selectedColor = setSelectedColor(type);
		    	
		    	vm.closeAdjustmentModal = closeAdjustmentModal;
		    	
		    	////////////
		    	
		    	$scope.$watch('vm.selectedColor', function(newValue, oldValue) {  
		    		if(oldValue != newValue) {
		    			switch(type) {
				    		case 'PRODUCTS':
				    			vm.user.colorOfProducts = vm.selectedColor;
				    			break;
				    		case 'PROJECTS':
				    			vm.user.colorOfProjects = vm.selectedColor;
				    			break;
				    		case 'CALENDAR_EVENTS':
				    			vm.user.colorOfCalendarEvents = vm.selectedColor;
				    			break;
							case 'RECEIVED_ORDERS':
				    			vm.user.colorOfReceivedOrders = vm.selectedColor;
				    			break;
							case 'SENT_ORDERS':
								vm.user.colorOfSentOrders = vm.selectedColor;
								break;
							case 'CONTACTS':
								vm.user.colorOfContactsImported = vm.selectedColor;
								break;
				    	}
		    			
		    			userService.updateUser(user).then(function(data) {
		    				closeAdjustmentModal();
		    			}, function errorCallback(response) {
		    	   			  console.log('error in adjustment.service#watch');
		    	   		});
		    			
		    		}
				});
		    	
		    	function setSelectedColor(type) {   
		    		switch(type) {
		    		case 'PRODUCTS':
		    			vm.selectedColor = vm.user.colorOfProducts;
		    			break;
		    		case 'PROJECTS':
		    			vm.selectedColor = vm.user.colorOfProjects;
		    			break;
		    		case 'CALENDAR_EVENTS':
		    			vm.selectedColor = vm.user.colorOfCalendarEvents;
		    			break;
					case 'RECEIVED_ORDERS':
		    			vm.selectedColor = vm.user.colorOfReceivedOrders;
		    			break;
					case 'SENT_ORDERS':
						vm.selectedColor = vm.user.colorOfSentOrders;
						break;
					case 'CONTACTS':
						vm.selectedColor = vm.user.colorOfContactsImported;
						break;
		    		}
		    	}
		    	
		    	function closeAdjustmentModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();