package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroup;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationGroupUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.CommunicationGroupUserConnectionRepository;

@Service("messageGroupUserConnectionService")
public class CommunicationGroupUserConnectionServiceImpl extends AbstractService implements CommunicationGroupUserConnectionService {

    @Autowired(required = true)
    protected CommunicationGroupUserConnectionRepository communicationGroupUserConnectionRepository;

    public CommunicationGroupUserConnectionServiceImpl() {
    }

    @Override
    public CommunicationGroupUserConnection createCommunicationGroupUserConnection(final User user,
            final CommunicationGroup communicationGroup, final Tenant tenant) throws BusinessException {
        BusinessAssert.notNull(user,
            "user is mandartory in CommunicationGroupUserConnectionServiceImpl#createCommunicationGroupUserConnection");
        BusinessAssert.notNull(communicationGroup,
            "communicationGroup is mandartory in CommunicationGroupUserConnectionServiceImpl#createCommunicationGroupUserConnection");
        logger.info(
            "create communicationGroupUserConnection in CommunicationGroupUserConnectionServiceImpl#createCommunicationGroupUserConnection");
        final CommunicationGroupUserConnection communicationGroupUserConnection = new CommunicationGroupUserConnection();
        communicationGroupUserConnection.setUser(user);
        communicationGroupUserConnection.setCommunicationGroup(communicationGroup);
        communicationGroupUserConnection.setTenant(tenant);
        final CommunicationGroupUserConnection createdCommunicationGroupUserConnection = communicationGroupUserConnectionRepository
            .insert(communicationGroupUserConnection);
        logger.debug(
            "created communicationGroupUserConnection with id '{}' in CommunicationGroupUserConnectionServiceImpl#createCommunicationGroupUserConnection",
            createdCommunicationGroupUserConnection.getId());
        return createdCommunicationGroupUserConnection;
    }

    @Override
    public List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionByUser(final User user) throws BusinessException {
        BusinessAssert.notNull(user,
            "user is mandartory in CommunicationGroupUserConnectionServiceImpl#findCommunicationGroupUserConnectionByUser");
        logger.info(
            "find communication group user connection by user with id '{}' in CommunicationGroupUserConnectionServiceImpl#findCommunicationGroupUserConnectionByUser",
            user.getId());
        return communicationGroupUserConnectionRepository.findCommunicationGroupUserConnectionByUser(user);
    }

    @Override
    public List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionByCommunicationGroup(
            final CommunicationGroup communicationGroup) throws BusinessException {
        BusinessAssert.notNull(communicationGroup,
            "communicationGroup is mandartory in CommunicationGroupUserConnectionServiceImpl#findCommunicationGroupUserConnectionByCommunicationGroup");
        logger.info(
            "find message group user connection by message group with id '{}' in CommunicationGroupUserConnectionServiceImpl#findCommunicationGroupUserConnectionByCommunicationGroup",
            communicationGroup.getId());
        return communicationGroupUserConnectionRepository.findCommunicationGroupUserConnectionByCommunicationGroup(communicationGroup);
    }

    @Override
    public List<CommunicationGroupUserConnection> findCommunicationGroupUserConnectionsByUser(final User user) throws BusinessException {
        BusinessAssert.notNull(user,
            "user is mandartory in CommunicationGroupUserConnectionServiceImpl#findCommunicationGroupUserConnectionsByUser");
        logger.info(
            "find communicationGroupUserConnection of user with id '{}' in CommunicationGroupUserConnectionServiceImpl#findCommunicationGroupUserConnectionsByUser",
            user.getId());
        return communicationGroupUserConnectionRepository.findCommunicationGroupUserConnectionsByUser(user);
    }

    @Override
    public void deleteCommunicationGroupUserConnections(final List<CommunicationGroupUserConnection> communicationGroupUserConnections)
            throws BusinessException {
        BusinessAssert.notNull(communicationGroupUserConnections,
            "communicationGroupUserConnections is mandartory in CommunicationGroupUserConnectionServiceImpl#deleteMessageUserConnections");
        logger.info(
            "delete communicationGroupUserConnections in CommunicationGroupUserConnectionServiceImpl#getMessageGroupUserConnectionsOfUser");

        for (final CommunicationGroupUserConnection messageGroupUserConnection : communicationGroupUserConnections) {
            communicationGroupUserConnectionRepository.delete(messageGroupUserConnection);
        }
    }

    @Override
    public String toString() {
        return "[MessageGroupUserConnectionServiceImpl]";
    }
}
