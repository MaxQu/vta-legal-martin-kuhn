package com.smartinnotec.legalprojectmanagement.restservice.container;

public enum FrontentTypeEnum {

    WDM("WDM"),
    ADM("ADM");

    private String value;

    private FrontentTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
