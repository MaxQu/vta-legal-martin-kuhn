package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum DocumentFileParentTypeEnum {

	PROJECT("PROJECT"),
    PRODUCT("PRODUCT");

    private String value;

    private DocumentFileParentTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
