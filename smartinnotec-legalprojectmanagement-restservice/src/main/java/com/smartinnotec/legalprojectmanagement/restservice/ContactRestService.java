package com.smartinnotec.legalprojectmanagement.restservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import com.smartinnotec.legalprojectmanagement.service.*;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Address;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactImportationDateGroup;

@RestController
public class ContactRestService extends AbstractRestService {

  @Autowired
  private AddressService addressService;
  @Autowired
  private ProductService productService;
  @Autowired
  private ContactService contactService;
  @Autowired
  private DocumentFileService documentFileService;
  @Autowired
  private UserService userService;
  @Autowired
  private HistoryService historyService;
  @Autowired
  private GeocodeService geocodeService;

  @RequestMapping(value = "/contacts/contact", method = {RequestMethod.POST}, consumes = "application/json;charset=utf-8")
  public Contact create(final @RequestBody Contact contact) throws BusinessException {
    BusinessAssert.notNull(contact, "contact is mandatory in ContactRestService#create", "400");
    logger.info(
      getInvokedRestServiceUserInformation() + ": creation of contact with institution name '{}' in ContactRestService#create",
      contact.getInstitution());

    final User user = userService.findUser(super.getLoggedInUserId());
    // set tenant of created contact
    final Tenant tenant = userService.getTenantOfUser(super.getLoggedInUserId());
    contact.setTenant(tenant);
    final Contact createdContact = contactService.create(contact, user, true);

    // set documentFile referenceCounter, need for delete
    if (createdContact.getContactAttachments() != null) {
      for (final ContactAttachment contactAttachment : createdContact.getContactAttachments()) {
        final String documentFileId = contactAttachment.getDocumentFileId();
        final DocumentFile documentFile = documentFileService.findById(documentFileId);
        documentFile.setContactReferenceCounter(documentFile.getContactReferenceCounter() + 1);
        final DocumentFile updatedDocumentFile = documentFileService.update(documentFile);
        logger.info("updated documentFile with id '{}' in ContactRestService#create", updatedDocumentFile.getId());
      }
    }

    logger.debug("created contact with id '{}' in ContactRestService#create", createdContact.getId());

    final String message = createdContact.getInstitution();
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.CREATED_NEW_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#create", createdHistory.getId());

    final Contact createdContactWithProducts = contactService.setProductsToContact(createdContact);
    return createdContactWithProducts;
  }

  @RequestMapping(value = "/contacts/contact", method = {RequestMethod.PUT}, consumes = "application/json;charset=utf-8")
  public Contact update(@RequestBody Contact contact) throws BusinessException {
    BusinessAssert.notNull(contact, "contact is mandatory in ContactRestService#update", "400");
    logger.info(getInvokedRestServiceUserInformation() + ": update of contact with institution name '{}' in ContactRestService#update",
      contact.getInstitution());

    final User user = userService.findUser(super.getLoggedInUserId());

    contact = contactService.resetAllChangedFlags(contact);

    // needed because of black lists in address
    if (contact.getAddressesWithProducts() != null) {
      for (final ContactAddressWithProducts contactAddressWithProducts : contact.getAddressesWithProducts()) {
        final Address address = contactAddressWithProducts.getAddress();
        if (address.getId() != null) {
          final Address updatedAddress = addressService.update(address);
          logger.info("updated address with id '{}' in ContactRestService#update", updatedAddress.getId());
        }

      }
    }

    final Contact currentContact = contactService.findContactById(contact.getId());
    contact.setTenant(currentContact.getTenant());
    final List<ContactAttachment> currentContactAttachments = currentContact.getContactAttachments();
    final Contact updatedContact = contactService.update(contact, user, true);

    final String message = updatedContact.getInstitution();
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.UPDATED_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#update", createdHistory.getId());

    final List<ContactAttachment> addedContactAttachments = contactService.getAddedContactAttachments(currentContactAttachments,
      updatedContact.getContactAttachments());
    final List<ContactAttachment> removedContactAttachments = contactService.getRemovedContactAttachments(currentContactAttachments,
      updatedContact.getContactAttachments());

    if (addedContactAttachments != null) {
      for (final ContactAttachment contactAttachment : addedContactAttachments) {
        final String documentFileId = contactAttachment.getDocumentFileId();
        final DocumentFile documentFile = documentFileService.findById(documentFileId);
        documentFile.setContactReferenceCounter(documentFile.getContactReferenceCounter() + 1);
        final DocumentFile updatedDocumentFile = documentFileService.update(documentFile);
        logger.info("updated documentFile with id '{}' in ContactRestService#update", updatedDocumentFile.getId());
      }
    }
    if (removedContactAttachments != null) {
      for (final ContactAttachment contactAttachment : removedContactAttachments) {
        final String documentFileId = contactAttachment.getDocumentFileId();
        final DocumentFile documentFile = documentFileService.findById(documentFileId);
        documentFile.setContactReferenceCounter(documentFile.getContactReferenceCounter() - 1);
        final DocumentFile updatedDocumentFile = documentFileService.update(documentFile);
        logger.info("updated documentFile with id '{}' in ContactRestService#update", updatedDocumentFile.getId());
      }
    }

    final Contact updatedContactWithProducts = contactService.setProductsToContact(updatedContact);
    return updatedContactWithProducts;
  }

  @RequestMapping(value = "/contacts/contact/adm", method = {RequestMethod.PUT}, consumes = "application/json;charset=utf-8")
  public Contact updateContactFromADM(@RequestBody Contact contact) throws BusinessException {
    BusinessAssert.notNull(contact, "contact is mandatory in ContactRestService#updateContactFromADM", "400");
    logger.info(getInvokedRestServiceUserInformation()
                + ": update of contact with institution name '{}' in ContactRestService#updateContactFromADM", contact.getInstitution());

    final User user = userService.findUser(super.getLoggedInUserId());

    contact = contactService.resetAllChangedFlags(contact);

    // needed because of black lists in address
    if (contact.getAddressesWithProducts() != null) {
      for (final ContactAddressWithProducts contactAddressWithProducts : contact.getAddressesWithProducts()) {
        final Address address = contactAddressWithProducts.getAddress();
        if (address.getId() != null) {
          final Address updatedAddress = addressService.update(address);
          logger.info("updated address with id '{}' in ContactRestService#updateContactFromADM", updatedAddress.getId());
        }

      }
    }

    final Contact currentContact = contactService.findContactById(contact.getId());
    contact.setColor(currentContact.getColor());
    contact.setActive(currentContact.isActive());
    contact.setShortCode(currentContact.getShortCode());
    contact.setAgentNumberContainers(currentContact.getAgentNumberContainers());
    contact.setDeliveryNumberContainers(currentContact.getDeliveryNumberContainers());
    contact.setCustomerNumberContainers(currentContact.getCustomerNumberContainers());
    contact.setContactAttachments(currentContact.getContactAttachments());
    contact.setCreationDate(currentContact.getCreationDate());
    contact.setCustomerSince(currentContact.getCustomerSince());
    contact.setImportationDate(currentContact.getImportationDate());
    contact.setImportConfirmed(currentContact.isImportConfirmed());
    contact.setImported(currentContact.isImported());
    contact.setProductIds(currentContact.getProductIds());
    contact.setProducts(currentContact.getProducts());
    contact.setScore(currentContact.getScore());
    contact.setTenant(currentContact.getTenant());

    // Geocoding -> resolve lat/lng for address
    geocodeService.geocodeAddress(contact.getAddress());

    if (currentContact.getAddressesWithProducts() != null) {
      for (final ContactAddressWithProducts currentContactAddressWithProducts : currentContact.getAddressesWithProducts()) {
        for (final ContactAddressWithProducts contactAddressWithProducts : contact.getAddressesWithProducts()) {
          if (currentContactAddressWithProducts.getAddress() != null && contactAddressWithProducts.getAddress() != null
              && currentContactAddressWithProducts.getAddress().getId().equals(contactAddressWithProducts.getAddress().getId())) {
            contactAddressWithProducts.setProductIds(currentContactAddressWithProducts.getProductIds());
            contactAddressWithProducts.setProducts(currentContactAddressWithProducts.getProducts());
          }
        }
      }
    }

    final Contact updatedContact = contactService.update(contact, user, true);

    final String message = updatedContact.getInstitution();
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.UPDATED_CONTACT_IN_ADM)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#updateContactFromADM", createdHistory.getId());

    final Contact updatedContactWithProducts = contactService.setProductsToContact(updatedContact);
    return updatedContactWithProducts;
  }

  @RequestMapping(value = "/contacts/contact/{addressid}/{productid}/add", method = {
    RequestMethod.PUT}, consumes = "application/json;charset=utf-8")
  public Product addProductToContact(final @PathVariable("addressid") String addressId, final @PathVariable("productid") String productId,
                                     final @RequestBody Contact contact)
  throws BusinessException {
    BusinessAssert.notNull(contact, "contact is mandatory in ContactRestService#addProductToContact", "400");
    BusinessAssert.notNull(addressId, "addressId is mandatory in ContactRestService#addProductToContact", "400");
    BusinessAssert.isId(addressId, "addressId must be an id in ContactRestService#addProductToContact", "400");
    BusinessAssert.notNull(productId, "productId is mandatory in ContactRestService#addProductToContact", "400");
    BusinessAssert.isId(productId, "productId must be an id in ContactRestService#addProductToContact", "400");
    logger.info("add product with id '{}' to contact with id '{}' and address '{}' in ContactRestService#addProductToContact",
      productId, contact.getId(), addressId);

    final User user = userService.findUser(super.getLoggedInUserId());

    final Contact foundedContact = contactService.findContactById(contact.getId());
    if (foundedContact.getAddress() != null && addressId.equals(foundedContact.getAddress().getId())) {
      if (foundedContact.getProductIds() != null && foundedContact.getProductIds().contains(productId)) {
        throw new BusinessException("PRODUCT_STILL_ADDED_TO_CONTACT", "400");
      }
      foundedContact.addProductId(productId);
    } else {
      final List<ContactAddressWithProducts> contactAddressesWithProducts = foundedContact.getAddressesWithProducts();
      ContactAddressWithProducts foundedContactAddressWithProducts = null;
      if (contactAddressesWithProducts != null) {
        for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
          if (contactAddressWithProducts.getAddress().getId().equals(addressId)) {
            foundedContactAddressWithProducts = contactAddressWithProducts;
            break;
          }
        }
      }
      if (foundedContactAddressWithProducts != null) {
        if (foundedContactAddressWithProducts.getProductIds() != null
            && foundedContactAddressWithProducts.getProductIds().contains(productId)) {
          throw new BusinessException("PRODUCT_STILL_ADDED_TO_CONTACT", "400");
        }
        foundedContactAddressWithProducts.addProductId(productId);
      }
    }
    final Product product = productService.findProduct(productId);
    final Contact updatedContact = contactService.update(foundedContact, user, false);
    logger.info("updated contact with id '{}' in ContactRestService#addProductToContact", updatedContact.getId());

    final String message = product.getName() + " -> " + updatedContact.getInstitution() + " (" + updatedContact.getProductIds().size()
                           + ")";
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.ADDED_PRODUCT_TO_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#addProductToContact", createdHistory.getId());

    final Product foundedProduct = productService.findProduct(productId);
    return foundedProduct;
  }

  @RequestMapping(value = "/contacts/contact/{addressid}/{productid}/remove", method = {
    RequestMethod.PUT}, consumes = "application/json;charset=utf-8")
  public Product removeProductFromContact(final @PathVariable("addressid") String addressId,
                                          final @PathVariable("productid") String productId, final @RequestBody Contact contact)
  throws BusinessException {
    BusinessAssert.notNull(contact, "contact is mandatory in ContactRestService#removeProductFromContact", "400");
    BusinessAssert.notNull(addressId, "addressId is mandatory in ContactRestService#removeProductFromContact", "400");
    BusinessAssert.isId(addressId, "addressId must be an id in ContactRestService#removeProductFromContact", "400");
    BusinessAssert.notNull(productId, "productId is mandatory in ContactRestService#removeProductFromContact", "400");
    BusinessAssert.isId(productId, "productId must be an id in ContactRestService#removeProductFromContact", "400");
    logger.info("remove product with id '{}' from contact with id '{}' and address '{}' in ContactRestService#removeProductFromContact",
      productId, contact.getId(), addressId);

    final User user = userService.findUser(super.getLoggedInUserId());

    final Contact foundedContact = contactService.findContactById(contact.getId());
    if (foundedContact.getAddress() != null && addressId.equals(foundedContact.getAddress().getId())) {
      foundedContact.removeProductId(productId);
    } else {
      final List<ContactAddressWithProducts> contactAddressesWithProducts = foundedContact.getAddressesWithProducts();
      ContactAddressWithProducts foundedContactAddressWithProducts = null;
      if (contactAddressesWithProducts != null) {
        for (final ContactAddressWithProducts contactAddressWithProducts : contactAddressesWithProducts) {
          if (contactAddressWithProducts.getAddress().getId().equals(addressId)) {
            foundedContactAddressWithProducts = contactAddressWithProducts;
            break;
          }
        }
      }
      if (foundedContactAddressWithProducts != null) {
        foundedContactAddressWithProducts.removeProductId(productId);
      }
    }
    final Product product = productService.findProduct(productId);
    final Contact updatedContact = contactService.update(foundedContact, user, false);
    logger.info("updated contact with id '{}' in ContactRestService#removeProductFromContact", updatedContact.getId());

    final String message = product.getName() + " -> " + updatedContact.getInstitution() + " (" + updatedContact.getProductIds().size()
                           + ")";
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.REMOVED_PRODUCT_TO_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#removeProductFromContact", createdHistory.getId());

    final Product foundedProduct = productService.findProduct(productId);
    return foundedProduct;
  }

  @RequestMapping(value = "/contacts/contact/{id}/contact", method = {RequestMethod.GET})
  public Contact findContactById(final @PathVariable("id") String id) throws BusinessException {
    BusinessAssert.notNull(id, "id is mandatory in ContactRestService#findContactById", "400");
    BusinessAssert.isId(id, "id must be an id in ContactRestService#findContactById", "400");
    logger.info("find contact by id '{}' in ContactRestService#findContactById", id);
    return contactService.findContactById(id);
  }

  @RequestMapping(value = "/contacts/contact/{id}/contact/withoutproducts", method = {RequestMethod.GET})
  public Contact findContactByIdWithoutProducts(final @PathVariable("id") String id) throws BusinessException {
    BusinessAssert.notNull(id, "id is mandatory in ContactRestService#findContactByIdWithoutProducts", "400");
    BusinessAssert.isId(id, "id must be an id in ContactRestService#findContactByIdWithoutProducts", "400");
    logger.info("find contact by id '{}' in ContactRestService#findContactByIdWithoutProducts", id);
    return contactService.findContactByIdWithoutProducts(id);
  }

  @RequestMapping(value = "/contacts/suppliers", method = {RequestMethod.GET})
  public List<Contact> findSupplierContacts() throws BusinessException {
    final Tenant tenant = userService.getTenantOfUser(super.getLoggedInUserId());
    return contactService.findAllSupplierContacts(tenant);
  }

  @RequestMapping(value = "/contacts/contact/{userid}", method = {RequestMethod.GET})
  public List<Contact> findAllTenantContacts(final @PathVariable("userid") String userId) throws BusinessException {
    BusinessAssert.notNull(userId, "userId is mandatory in ContactRestService#findAllTenantContacts", "400");
    BusinessAssert.isId(userId, "userId must be an id in ContactRestService#findAllTenantContacts", "400");
    final Tenant tenant = userService.getTenantOfUser(userId);
    final List<Contact> contactsWithProduct = new ArrayList<>();
    if (tenant != null) {
      logger.info(getInvokedRestServiceUserInformation()
                  + ": get contacts of tenant with id '{}' in ContactRestService#findAllTenantContacts", tenant.getId());
      final List<Contact> allTenantContacts = contactService.findAllTenantContacts(tenant);
      for (final Contact contact : allTenantContacts) {
        final Contact contactWithProducts = contactService.setProductsToContact(contact);
        contactsWithProduct.add(contactWithProducts);
      }
      return contactsWithProduct;
    } else {
      logger.info(getInvokedRestServiceUserInformation() + ": get contacts in ContactRestService#findAllTenantContacts");
      final List<Contact> allContacts = contactService.findAll();
      for (final Contact contact : allContacts) {
        final Contact contactWithProducts = contactService.setProductsToContact(contact);
        contactsWithProduct.add(contactWithProducts);
      }
      return contactsWithProduct;
    }
  }

  @RequestMapping(value = "/contacts/contact/{userid}/withoutproducts", method = {RequestMethod.GET})
  public List<Contact> findAllTenantContactsWithoutProducts(final @PathVariable("userid") String userId) throws BusinessException {
    BusinessAssert.notNull(userId, "userId is mandatory in ContactRestService#findAllTenantContactsWithoutProducts", "400");
    BusinessAssert.isId(userId, "userId must be an id in ContactRestService#findAllTenantContactsWithoutProducts", "400");
    final Tenant tenant = userService.getTenantOfUser(userId);
    if (tenant != null) {
      logger.info(getInvokedRestServiceUserInformation()
                  + ": get contacts of tenant with id '{}' in ContactRestService#findAllTenantContactsWithoutProducts", tenant.getId());
      final List<Contact> allTenantContacts = contactService.findAllTenantContacts(tenant);
      if (allTenantContacts != null && !allTenantContacts.isEmpty()) {
        Collections.sort(allTenantContacts);
      }
      return allTenantContacts;
    } else {
      logger
        .info(getInvokedRestServiceUserInformation() + ": get contacts in ContactRestService#findAllTenantContactsWithoutProducts");
      final List<Contact> allContacts = contactService.findAll();
      return allContacts;
    }
  }

  @RequestMapping(value = "/contacts/contact/user/withoutproducts", method = {RequestMethod.GET})
  public List<Contact> getAllUserContacts() throws BusinessException {
    final String userId = super.getLoggedInUserId();
    return findAllContactsOfUserWithoutProducts(userId);
  }

  @RequestMapping(value = "/contacts/contact/{userid}/user/withoutproducts", method = {RequestMethod.GET})
  public List<Contact> findAllContactsOfUserWithoutProducts(final @PathVariable("userid") String userId) throws BusinessException {
    BusinessAssert.notNull(userId, "userId is mandatory in ContactRestService#findAllContactsOfUserWithoutProducts", "400");
    BusinessAssert.isId(userId, "userId must be an id in ContactRestService#findAllContactsOfUserWithoutProducts", "400");
    logger.info("find all contacts of user with id '{}' without products in ContactRestService#findAllContactsOfUserWithoutProducts",
      userId);
    final User user = userService.findUser(userId);

    if (user.isAllContactsAvailable()) {
      return contactService.findAll();
    }
    final List<Contact> contactsOfUser = contactService.findAllContactsOfUserWithoutProducts(user);
    final List<Contact> contactsAssignedOverAgentNumbers = contactService.findAllContactsAssignedOverAgentNumbers(user);
    final List<Contact> potentialBuyerContacts = contactService.findAllContactsOfContactType(ContactTypeEnum.POTENTIAL_BUYER);
    final List<Contact> userContacts = contactService.findAllContactsOfContactType(ContactTypeEnum.USER_CONTACT);
    final List<Contact> contacts = new ArrayList<>();
    if (contactsOfUser != null && !contactsOfUser.isEmpty()) {
      contacts.addAll(contactsOfUser);
    }
    if (contactsAssignedOverAgentNumbers != null && !contactsAssignedOverAgentNumbers.isEmpty()) {
      for (final Contact contact : contactsAssignedOverAgentNumbers) {
        if (!contacts.contains(contact)) {
          contacts.add(contact);
        }
      }
    }
    if (potentialBuyerContacts != null && !potentialBuyerContacts.isEmpty()) {
      contacts.addAll(potentialBuyerContacts);
    }
    if (userContacts != null && !userContacts.isEmpty()) {
      contacts.addAll(userContacts);
    }

    if (contacts != null && !contacts.isEmpty()) {
      Collections.sort(contacts);
    }
    return contacts;
  }

  @RequestMapping(value = "/contacts/contact/{searchstring}/search", method = {RequestMethod.GET})
  public List<Contact> findContactBySearchString(final @PathVariable("searchstring") String searchString) throws BusinessException {
    logger.info(
      getInvokedRestServiceUserInformation() + ": find contact by searchString '{}' in ContactRestService#findContactBySearchString",
      searchString);
    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);
    List<Contact> foundedContacts = contactService.findContactBySearchString(user, searchString);

    // only to reassign activity and facilitydetails
    final String regex = "[0-9a-f]{24}";
    if (Pattern.matches(regex, searchString)) {
      final Contact contact = contactService.findContactById(searchString);
      foundedContacts = new ArrayList<>();
      foundedContacts.add(contact);
    }

    final String message = searchString;
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.SEARCHED_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#findContactBySearchString", createdHistory.getId());

    return foundedContacts;
  }

  @RequestMapping(value = "/contacts/contact/{searchstring}/user/search", method = {RequestMethod.GET})
  public List<Contact> findContactsOfUserBySearchString(final @PathVariable("searchstring") String searchString)
  throws BusinessException {
    logger.info(getInvokedRestServiceUserInformation()
                + ": find contact of user by searchString '{}' in ContactRestService#findContactOfUserBySearchString", searchString);
    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);

    if (user.isAllContactsAvailable()) {
      final List<Contact> contacts = contactService.findContactBySearchString(user, searchString);

      final String message = searchString;
      final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.SEARCHED_CONTACT)
                                                          .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis()))
                                                          .setMessage(message).setUser(user).setTenant(user.getTenant()).build();
      final History createdHistory = historyService.create(history);
      logger.info("created history with id '{}' in ContactRestService#findContactsOfUserBySearchString", createdHistory.getId());

      return contacts;
    }

    // find contacts assigned to user
    final List<Contact> contactsOfUser = contactService.findContactsOfUserBySearchString(user, searchString);

    // find contacts assigned to user over agent number
    final List<Contact> contactsAssignedOverAgentNumbers = contactService.findAllContactsAssignedOverAgentNumbers(user);
    final List<Contact> contactsAssignedOverAgentNumbersAndContainingSearchString = contactService
      .findContactsByExistingContactsAnSearchString(user, searchString, contactsAssignedOverAgentNumbers);

    final List<Contact> contacts = new ArrayList<>();
    if (contactsOfUser != null && !contactsOfUser.isEmpty()) {
      contacts.addAll(contactsOfUser);
    }
    for (final Contact contact : contactsAssignedOverAgentNumbersAndContainingSearchString) {
      if (!contacts.contains(contact)) {
        contacts.add(contact);
      }
    }

    final String message = searchString;
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.SEARCHED_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#findContactsOfUserBySearchString", createdHistory.getId());

    return contacts;
  }

  @RequestMapping(value = "/contacts/contact/{userid}/amount", method = {RequestMethod.GET})
  public Integer countByTenantAndActive(final @PathVariable("userid") String userId) throws BusinessException {
    BusinessAssert.notNull(userId, "userId is mandatory in ContactRestService#countByTenantAndActive", "400");
    BusinessAssert.isId(userId, "userId must be an id in ContactRestService#countByTenantAndActive", "400");
    logger.info(getInvokedRestServiceUserInformation()
                + ": count contacts by tenant and active of user with id '{}' in ContactRestService#countByTenantAndActive", userId);
    final User user = userService.findUser(userId);
    final Tenant tenant = user.getTenant();
    if (tenant != null) {
      final Integer amount = contactService.countByTenantAndActive(tenant, true);
      return amount;
    } else {
      return contactService.countByActive(true);
    }
  }

  @RequestMapping(value = "/contacts/contact/{userid}/amount/customer", method = {RequestMethod.GET})
  public Integer countCustomerContactsByTenantAndActive(final @PathVariable("userid") String userId) throws BusinessException {
    BusinessAssert.notNull(userId, "userId is mandatory in ContactRestService#countCustomerContactsByTenantAndActive", "400");
    BusinessAssert.isId(userId, "userId must be an id in ContactRestService#countCustomerContactsByTenantAndActive", "400");
    logger.info(
      getInvokedRestServiceUserInformation()
      + ": count contacts by tenant and active of user with id '{}' in ContactRestService#countCustomerContactsByTenantAndActive",
      userId);
    final User user = userService.findUser(userId);
    final Tenant tenant = user.getTenant();
    final Integer amount = contactService.countByTenantAndActiveAndDeliveryNumberContainersIsNull(tenant, true);
    return amount;
  }

  @RequestMapping(value = "/contacts/contact/page/{page}/{sortingtype}", method = {RequestMethod.GET}, produces = "application/json")
  public List<Contact> findPagedContacts(final @PathVariable(value = "page") Integer page,
                                         final @PathVariable(value = "sortingtype") String sortingType)
  throws BusinessException {
    BusinessAssert.notNull(page, "page is mandatory in ContactRestService#findPagedContacts", "400");
    logger.info(getInvokedRestServiceUserInformation() + ": find paged contacts for page '{}' in ContactRestService#findPagedContacts",
      page);
    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);
    final Tenant tenant = userService.getTenantOfUser(user.getId());
    final List<Contact> contacts = contactService.findPagedContactsByTenant(user, tenant, page, sortingType);
    return contacts;
  }

  @RequestMapping(value = "/contacts/contact/amountofpages", method = {RequestMethod.GET})
  public Integer calculateAmountOfPages() throws BusinessException {
    logger.info(getInvokedRestServiceUserInformation() + ": get amount of pages in ContactRestService#calculateAmountOfPages");
    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);
    final Tenant tenant = userService.getTenantOfUser(super.getLoggedInUserId());
    final Integer amountOfPageSizes = contactService.calculateAmountOfPages(user, tenant);
    return amountOfPageSizes;
  }

  @RequestMapping(value = "/contacts/contact/contactpagesize", method = {RequestMethod.GET}, produces = "application/json")
  public Integer getContactPageSize() throws BusinessException {
    logger.info(getInvokedRestServiceUserInformation() + ": get page amount in ContactRestService#getContactPageSize");
    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);
    Integer contactPageSize = contactService.getContactPageSize();
    if (user.getContactPageSize() != null && user.getContactPageSize() > 0) {
      contactPageSize = user.getContactPageSize();
    }
    return contactPageSize;
  }

  @RequestMapping(value = "/contacts/contact/{productid}/contacts", method = {RequestMethod.GET}, produces = "application/json")
  public List<Contact> getContactsReferencedToProduct(final @PathVariable(value = "productid") String productId)
  throws BusinessException {
    BusinessAssert.notNull(productId, "productId is mandatory in ContactRestService#getContactsReferencedToProduct", "400");
    BusinessAssert.isId(productId, "productId is mandatory in ContactRestService#getContactsReferencedToProduct", "400");
    logger.info("get contact referenced by product with id '{}' in ContactRestService#getContactsReferencedToProduct", productId);
    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);
    final List<Contact> contacts = contactService.findAllTenantContacts(user.getTenant());
    final List<Contact> contactsAssignedByProduct = new ArrayList<>();
    for (final Contact contact : contacts) {
      final List<String> allProductIds = contact.getProductIds() != null ? contact.getProductIds() : new ArrayList<>();
      if (contact.getAddressesWithProducts() != null) {
        for (ContactAddressWithProducts contactAddressWithProducts : contact.getAddressesWithProducts()) {
          final List<String> productIds = contactAddressWithProducts.getProductIds();
          if (productIds != null && !productIds.isEmpty()) {
            allProductIds.addAll(productIds);
          }
        }
      }
      if (allProductIds != null && !allProductIds.isEmpty()) {
        for (final String existingProductId : allProductIds) {
          if (existingProductId.equals(productId)) {
            contactsAssignedByProduct.add(contact);
          }
        }
      }
    }
    final List<Contact> contactsWithProducts = new ArrayList<>();
    for (final Contact contact : contactsAssignedByProduct) {
      final Contact contactWithProducts = contactService.setProductsToContact(contact);
      contactsWithProducts.add(contactWithProducts);
    }
    return contactsWithProducts;
  }

  @RequestMapping(value = "/contacts/contact/contactimportationdategroup", method = {RequestMethod.GET}, produces = "application/json")
  public List<ContactImportationDateGroup> getContactImportationDateGroup() throws BusinessException {
    logger.info("get contactCreationDateGroup in ContactRestService#getContactImportationDateGroup");
    return contactService.getContactImportationDateGroups();
  }

  @RequestMapping(value = "/contacts/contact/{contactid}/{state}/deactivate", method = RequestMethod.PATCH)
  public void deactivateContact(final @PathVariable(value = "contactid") String contactId,
                                final @PathVariable(value = "state") boolean state)
  throws BusinessException {
    BusinessAssert.notNull(contactId, "contactId is mandatory in ContactRestService#deactivateContact", "400");
    BusinessAssert.isId(contactId, "contactId must be an id in ContactRestService#deactivateContact", "400");
    logger.info(getInvokedRestServiceUserInformation() + ": deactivate contact with id '{}' in ContactRestService#deactivateContact",
      contactId);

    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);

    final Contact contactToDeactivate = findContactById(contactId);
    contactToDeactivate.setActive(state);
    final Contact updatedContact = contactService.update(contactToDeactivate, user, false);
    logger.info("contact with id '{}' deactivated in ContactRestService#deactivateContact", updatedContact.getId());

    final String message = contactToDeactivate.getInstitution() + " " + (state == true ? "aktiviert" : "deaktiviert");
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DEACTIVATE_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#deactivateContact", createdHistory.getId());
  }

  @RequestMapping(value = "/contacts/contact/import", method = {RequestMethod.POST})
  public Boolean importContacts(@RequestParam("file") MultipartFile multipartFile, @RequestParam("tenant") CountryTypeEnum countryType)
  throws BusinessException {
    logger.info("import contacts in ContactRestService#importContacts");
    final User user = userService.findUser(super.getLoggedInUserId());
    try {
      final byte[] bytes = multipartFile.getBytes();
      if (!multipartFile.isEmpty()) {
        final Boolean imported = contactService.importContacts(user, countryType, bytes);

        final String message = "";
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.IMPORTED_CONTACT)
                                                            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis()))
                                                            .setMessage(message).setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ContactRestService#importContacts", createdHistory.getId());

        return imported;
      }
    } catch (IOException e) {
      throw new BusinessException(e.getMessage(), "400");
    }
    return false;
  }

  @RequestMapping(value = "/contacts/contact/{contactid}", method = RequestMethod.DELETE)
  public void deleteContact(final @PathVariable(value = "contactid") String contactId) throws BusinessException {
    BusinessAssert.notNull(contactId, "contactId is mandatory in ContactRestService#deleteContact", "400");
    BusinessAssert.isId(contactId, "contactId must be an id in ContactRestService#deleteContact", "400");
    logger.info(getInvokedRestServiceUserInformation() + ": delete contact with id '{}' in ContactRestService#deleteContact",
      contactId);

    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);

    final Contact contactToDelete = findContactById(contactId);
    contactService.deleteContact(contactId, user);

    final String message = contactToDelete.getInstitution();
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DELETED_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#deleteContact", createdHistory.getId());
  }

  @RequestMapping(value = "/contacts/contact/{date}/importedcontacts", method = RequestMethod.DELETE)
  public boolean deleteImportedContactsOfBulk(final @PathVariable(value = "date") String date) throws BusinessException {
    logger.info(getInvokedRestServiceUserInformation() + ": delete imported contacts in ContactRestService#deleteImportedContacts");
    final String userId = super.getLoggedInUserId();
    final User user = userService.findUser(userId);
    final DateTime dateTime = dateTimeFormatter.parseDateTime(date);

    final List<Contact> importedContacts = contactService.findByTenantAndImportedAndImportConfirmedAndImportationDate(user.getTenant(),
      true, false, dateTime);

    for (final Contact contact : importedContacts) {
      this.deleteContact(contact.getId());
    }

    final String message = "";
    final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DELETED_IMPORTED_CONTACT)
                                                        .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
                                                        .setUser(user).setTenant(user.getTenant()).build();
    final History createdHistory = historyService.create(history);
    logger.info("created history with id '{}' in ContactRestService#deleteImportedContactsOfBulk", createdHistory.getId());

    return true;
  }

  @RequestMapping(value = "/contacts/contact/all", method = RequestMethod.DELETE)
  public void deleteAllContacts() throws BusinessException {
    logger.info("delete all contacts in ContactRestService#deleteAllContacts");
    contactService.deleteAll();
  }

  @RequestMapping(value = "/contacts/contact/all/customers/{countrytype}", method = RequestMethod.DELETE)
  public void deleteAllCustomerContacts(final @PathVariable(value = "countrytype") CountryTypeEnum countryType) throws BusinessException {
    logger.info("delete all contacts of country '{}' in ContactRestService#deleteAllCustomerContacts", countryType);
    contactService.deleteAllCustomerContacts(countryType);
  }



  @Override
  public String toString() {
    return "[ContactRestService]";
  }
}
