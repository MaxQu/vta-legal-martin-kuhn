(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('pdfTemplateModalService', pdfTemplateModalService);
        
    pdfTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function pdfTemplateModalService($modal, $stateParams) {
		var service = {
			showPdfTemplateModal: showPdfTemplateModal
		};		
		return service;
		
		////////////
				
		function showPdfTemplateModal(pdfUrl, documentName, ending, version) {
			 var pdfTemplateModal = $modal.open({
				controller: PdfTemplateModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	pdfUrl: function() {
			    		return pdfUrl;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/pdfTemplateModal/pdfTemplate.modal.html'
			});
			return pdfTemplateModal;
			
			function PdfTemplateModalController($modalInstance, $scope, pdfUrl, documentName, ending, version) {
		    	var vm = this; 
		    	$scope.pdfUrl = preparePdfUrl(pdfUrl, ending);
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;
		    	
		    	vm.preparePdfUrl = preparePdfUrl;
		    	vm.closePdfTemplateModal = closePdfTemplateModal;
		    	
		    	////////////
		    	
		    	function preparePdfUrl(pdfUrl, ending) {
		    		if(ending.indexOf('pdf') === -1 && ending.indexOf('PDF') === -1) {
		    			return pdfUrl + 'previewpdf';
		    		}
		    		return pdfUrl;
		    	}
		    	
		    	function closePdfTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
