package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventScheduleDays;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface CalendarEventScheduleDaysService {

    List<CalendarEventScheduleDays> findAllByTenant(final Tenant tenant) throws BusinessException;

    List<CalendarEventScheduleDays> findAllByTenant(final User user) throws BusinessException;

    CalendarEventScheduleDays create(final CalendarEventScheduleDays calendarEventScheduleDays) throws BusinessException;

    CalendarEventScheduleDays update(final CalendarEventScheduleDays calendarEventScheduleDays) throws BusinessException;

    void delete(final String id) throws BusinessException;

}
