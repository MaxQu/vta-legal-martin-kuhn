package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.SmartinnotecSpringMongoDBConfig;
import com.smartinnotec.legalprojectmanagement.dao.repository.ActivityRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.ActivityRepositoryDAO;
import com.smartinnotec.legalprojectmanagement.dao.repository.CalendarConfigurationRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.CalendarEventRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.CalendarEventScheduleDaysRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.CalendarEventUserConnectionRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.CommunicationUserConnectionRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.ContactRepositoryDAO;
import com.smartinnotec.legalprojectmanagement.dao.repository.DocumentFileRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.LabelsPredefinedRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.ProductRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.ProjectRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.ReceipeRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.TenantRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.ThinningPolymerDateTimeValueRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.UserAuthorizationRoleAccessRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.UserAuthorizationUserContainerRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.UserRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.WorkingBookCategoryTypeEntryRepository;
import com.smartinnotec.legalprojectmanagement.dao.repository.WorkingBookRepository;

@ContextConfiguration(classes = { SmartinnotecSpringMongoDBConfig.class, ContactRepositoryDAO.class, ActivityRepositoryDAO.class })
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractDAOTest {

    protected org.joda.time.format.DateTimeFormatter dateFormatter;
    protected org.joda.time.format.DateTimeFormatter dateTimeFormatter;

    @Autowired
    protected LabelsPredefinedRepository labelsPredefinedRepository;
    @Autowired
    protected DocumentFileRepository documentFileRepository;
    @Autowired
    protected TenantRepository tenantRepository;
    @Autowired
    protected WorkingBookRepository workingBookRepository;
    @Autowired
    protected CalendarEventRepository calendarEventRepository;
    @Autowired
    protected ActivityRepository activityRepository;
    @Autowired
    protected ProjectRepository projectRepository;
    @Autowired
    protected ProductRepository productRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected ContactRepository contactRepository;
    @Autowired
    protected ContactRepositoryDAO contactRepositoryDAO;
    @Autowired
    protected ActivityRepositoryDAO activityRepositoryDAO;
    @Autowired
    protected ReceipeRepository receipeRepository;
    @Autowired
    protected CalendarEventUserConnectionRepository calendarEventUserConnectionRepository;
    @Autowired
    protected CommunicationUserConnectionRepository communicationUserConnectionRepository;
    @Autowired
    protected CalendarConfigurationRepository calendarConfigurationRepository;
    @Autowired
    protected UserAuthorizationUserContainerRepository userAuthorizationUserContainerRepository;
    @Autowired
    protected UserAuthorizationRoleAccessRepository userAuthorizationRoleAccessRepository;
    @Autowired
    protected CalendarEventScheduleDaysRepository calendarEventScheduleDaysRepository;
    @Autowired
    protected WorkingBookCategoryTypeEntryRepository workingBookCategoryTypeEntryRepository;
    @Autowired
    protected ThinningPolymerDateTimeValueRepository thinningPolymerDateTimeValueRepository;

    @Before
    public void setUp() throws BusinessException {
        dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        dateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        contactRepository.deleteAll();
        activityRepository.deleteAll();
    }
}
