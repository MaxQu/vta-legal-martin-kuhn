(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('editReceipeModalService', editReceipeModalService);
        
    editReceipeModalService.$inject = ['$modal', '$stateParams', 'productsService', 'receipeService', 'dateUtilityService'];
    
    function editReceipeModalService($modal, $stateParams, productsService, receipeService, dateUtilityService) {
		var service = {
			showEditReceipeModal: showEditReceipeModal
		};		
		return service;
		
		////////////
				
		function showEditReceipeModal(currentUser, product, products, receipesOfProduct, receipeType, start, end, userAuthorizationUserContainer, unityTypes) {
			 var editReceipeModal = $modal.open({
				controller: EditReceipeModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	receipesOfProduct: function() {
			    		return receipesOfProduct;
			    	},
			    	receipeType: function() {
			    		return receipeType;
			    	},
			    	start: function() {
			    		return start;
			    	},
			    	end: function() { 
			    		return end;
			    	},
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	},
			    	unityTypes: function() {
			    		return unityTypes;
			    	}
			    }, 
				templateUrl: '/app/products/modals/editReceipeModal/editReceipe.modal.html'
			});
			return editReceipeModal;
			
			function EditReceipeModalController($modalInstance, $scope, currentUser, product, products, receipesOfProduct, receipeType, start, end, userAuthorizationUserContainer, unityTypes) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.receipesOfProduct = setContactOfReceipes(receipesOfProduct);
		    	vm.receipeType = receipeType;
		    	vm.searchReceipeDateFromDatePicker = false;
		    	vm.searchReceipeDateUntilDatePicker = false;
		    	vm.searchReceipeTerm = null;
		    	vm.receipeDateFrom = start;
		    	vm.receipeDateUntil = end;
		    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer;
		    	vm.unityTypes = unityTypes;
		    	
		    	vm.updateProduct = updateProduct;
		    	vm.deleteReceipe = deleteReceipe;
		    	vm.openSearchReceipeFromDatePicker = openSearchReceipeFromDatePicker;
		    	vm.openSearchReceipeUntilDatePicker = openSearchReceipeUntilDatePicker;
		    	vm.closeEditReceipeModal = closeEditReceipeModal;
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	$scope.$watch('vm.receipeDateFrom', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.receipeDateFrom, vm.receipeDateUntil);
			    		if(vm.receipeDateFrom != null && vm.receipeDateUntil != null && checkDateRange == false) {
			    			searchReceipesInTimeRange();
			    		}
		    		}
		        });
		    	
		    	$scope.$watch('vm.receipeDateUntil', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
			    		var checkDateRange = dateUtilityService.isDateBeforeOrEqualOtherDate(vm.receipeDateFrom, vm.receipeDateUntil);
			    		if(vm.receipeDateFrom != null && vm.receipeDateUntil != null && checkDateRange == false) {
			    			searchReceipesInTimeRange();
			    		}
		    		}
		        });
		    	
		    	function setContactOfReceipes(receipes) {
		    		for(var i = 0; i < receipes.length; i++) {
		    			if(receipes[i].contactText != null) {
		    				receipes[i].contactOrContactText = receipes[i].contactText;
		    			} else if(receipes[i].contact != null) {
		    				receipes[i].contactOrContactText = receipes[i].contact;
		    			}
		    		}
		    		return receipes;
		    	}
		  
		    	function openSearchReceipeFromDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchReceipeDateFromDatePicker = true; 
				}
		    	
		    	function openSearchReceipeUntilDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.searchReceipeDateUntilDatePicker = true; 
				}
		    	
		    	function deleteReceipe(receipe) {
		    		for(var i = 0; i < vm.receipesOfProduct.length; i++) {
		    			if(vm.receipesOfProduct[i].id == receipe.id) {
		    				vm.receipesOfProduct.splice(i, 1);
		    				updateProduct(vm.product.id);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function updateProduct(productId) {
		    		productsService.findProduct(productId).then(function(response) {
		    			var product = response.data;
		    			if(vm.products != null) {
			    			for(var i = 0; i < vm.products.length; i++) {
			    				if(vm.products[i].id == productId) {
			    					vm.products[i] = product;
			    					break;
			    				}
			    			}
		    			}
		  	   		 }, function errorCallback(response) {
		  	   			 console.log('error editReceipe.service#updateProduct'); 
		  	   		 });
		    	}
		    	
		    	function findReceipesOfProductOfTypeInLastMonth() {
					vm.receipeDateFrom = dateUtilityService.oneMonthBack(new Date());
			    	vm.receipeDateUntil = new Date();
			    	var startDateString = dateUtilityService.formatDateToString(start);
		    		var endDateString = dateUtilityService.formatDateToString(end);
			    	vm.searchReceipeTerm = null;
			    	receipeService.findReceipesOfProductOfTypeInRange(product.id, receipeType, startDateString, endDateString).then(function(response) {
			    		vm.receipesOfProduct = response.data;    			
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error editReceipe.controller#findReceipesOfProductOfTypeInLastMonth');
		 	   		 });
				}
		    	
		    	function searchReceipesInTimeRange() {
					var start = vm.receipeDateFrom instanceof Date ? dateUtilityService.formatDateToString(vm.receipeDateFrom) : vm.receipeDateFrom;
					var end = vm.receipeDateUntil instanceof Date ? dateUtilityService.formatDateToString(vm.receipeDateUntil) : vm.receipeDateUntil;
					receipeService.findReceipesOfProductOfTypeInRange(vm.product.id, vm.receipeType, start, end).then(function(response) {
						vm.receipesOfProduct = response.data;
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error editReceipe.service#searchReceipesInTimeRange');
		 	   		 });
				}
		    	
		    	function closeEditReceipeModal() {
					$modalInstance.dismiss('cancel');  
				}
			} 
		}
    }
})();