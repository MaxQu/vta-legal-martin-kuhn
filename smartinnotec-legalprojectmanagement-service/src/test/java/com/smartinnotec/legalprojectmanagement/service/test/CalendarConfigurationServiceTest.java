package com.smartinnotec.legalprojectmanagement.service.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class CalendarConfigurationServiceTest extends AbstractServiceTest {

    @Test
    public void shouldGetCalendarData() throws BusinessException, FileNotFoundException {
        final String calendarHeader = calendarConfigurationService.getCalendarConfigurationHeader();
        final String calendarMessage = calendarConfigurationService.getCalendarConfigurationMessage("", "", "", "",
            System.currentTimeMillis(), System.currentTimeMillis());
        final String calendarHeaderEnd = calendarConfigurationService.getCalendarConfigurationHeaderEnd();
        final String dates = calendarHeader + "\n" + calendarMessage + "\n" + calendarHeaderEnd;
        Assert.assertNotNull(dates);

        final String filePath = "C:\\Users\\05906\\Desktop\\CalendarTestFile.txt";

        final File f = new File(filePath);
        final PrintWriter pw = new PrintWriter(f);
        pw.write(dates);
        pw.flush();
        pw.close();
    }
}
