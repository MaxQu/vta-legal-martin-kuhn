package com.smartinnotec.legalprojectmanagement.dao.repository;


import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryAmount;

public interface HistoryAmountRepository extends MongoRepository<HistoryAmount, String> {

	List<HistoryAmount> getHistoryAmountByDateBetween(final DateTime start, final DateTime end);
	
}
