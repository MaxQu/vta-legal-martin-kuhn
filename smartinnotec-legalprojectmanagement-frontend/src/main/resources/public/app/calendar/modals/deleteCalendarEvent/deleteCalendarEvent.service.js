(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('deleteCalendarEventModalService', deleteCalendarEventModalService);
        
    deleteCalendarEventModalService.$inject = ['$modal', '$stateParams', 'calendarService'];
    
    function deleteCalendarEventModalService($modal, $stateParams, calendarService) {
		var service = {
			showDeleteCalendarEventModal: showDeleteCalendarEventModal
		};		
		return service;
		
		////////////
				
		function showDeleteCalendarEventModal(calendarEvent, invoker) {
			 var deleteCalendarEventModal = $modal.open({
				controller: DeleteCalendarEventModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEvent: function() {
			    		return calendarEvent;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/deleteCalendarEvent/deleteCalendarEvent.modal.html'
			});
			return deleteCalendarEventModal;
			
			function DeleteCalendarEventModalController($modalInstance, $scope, calendarEvent, invoker) {
		    	var vm = this;
		    	vm.calendarEvent = calendarEvent;
		    	vm.deleteSerialDates = false;
		    	vm.deleteUniqueDate = false;
		    	
		    	$scope.$watch('vm.deleteSerialDates', function () {
		    		if(vm.deleteSerialDates === true) {
		    			vm.deleteUniqueDate = false;
		    		}
		        });
		    	
		    	$scope.$watch('vm.deleteUniqueDate', function () {
		    		if(vm.deleteUniqueDate === true) {
		    			vm.deleteSerialDates = false;
		    		}
		        });
		    	
		    	vm.closeCalendarEventDeletionModal = closeCalendarEventDeletionModal;
		    	vm.calendarEventDeletion = calendarEventDeletion;
		    	
		    	function calendarEventDeletion() {
		    		calendarService.deleteCalendarEvent(vm.calendarEvent.id, vm.deleteSerialDates).then(function (response) {
		    			invoker.deleteCalendarEvent(vm.calendarEvent, vm.deleteSerialDates);
		    			closeCalendarEventDeletionModal();
		    		}).catch(function (data) {
						console.log('error deleteCalendarEvent.service#calendarEventDeletion: ' + data);
					});
		    	}
		    	
		    	function closeCalendarEventDeletionModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();