package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;

import lombok.Data;

@Document(collection = "Tenant")
public @Data class Tenant {

    @Id
    @JsonView(RestServiceResponseView.TenantPublic.class)
    private String id;
    @Indexed(unique = true)
    @JsonView(RestServiceResponseView.TenantPublic.class)
    private String name;

}
