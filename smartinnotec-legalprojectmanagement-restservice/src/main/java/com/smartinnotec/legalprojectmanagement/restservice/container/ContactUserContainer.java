package com.smartinnotec.legalprojectmanagement.restservice.container;

import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

import lombok.Data;

public @Data class ContactUserContainer {

    private String id;
    private String institution;
    private String firstname;
    private String surname;
    private String email;
    private ContactUserTypeEnum contactUserType;
    private Contact contact;
    private User user;

}
