(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.directive('contactActivityDirective', contactActivityDirective);
    
    contactActivityDirective.$inject = ['$timeout', 'dateUtilityService', 'yesNoActivityContactModalService', 'contactActivityCreateEditModalService'];
    
	function contactActivityDirective($timeout, dateUtilityService, yesNoActivityContactModalService, contactActivityCreateEditModalService) {
		var directive = {
			restrict: 'E',    
			scope: {
				invoker: '=',
				calendarEvent: '=',
				activity: '=',
				activityTypes: '=',
				user: '=',
				contact: '=',
				userAuthorizationUserContainer: '=',
				type: '='
			},
			templateUrl: 'app/contacts/directives/contactActivityDirective/contactActivity.html',
			link: function($scope) {
				
				$scope.formatStartEndDateTime = function(start, end) {
					var startDate = new Date(start);
		    		var formattedStartDate = dateUtilityService.formatDateToDateTimeString(startDate);
		    		var endDate = new Date(end);
		    		var formattedEndDate = dateUtilityService.formatDateToDateTimeString(endDate);
		    		return dateUtilityService.formatStartEndDateTime(formattedStartDate, formattedEndDate);
				};
				
				$scope.showRemoveActivity = function() {
					yesNoActivityContactModalService.showYesNoModal($scope.invoker, $scope.contact, $scope.activity);
				};
				
				$scope.showContactActivityCreateEditModal = function(activity) {
					contactActivityCreateEditModalService.showContactActivityCreateEditModal($scope.contact, $scope.activity, $scope.activityTypes);
				};
				
				$scope.getFileNameWithoutExtension = function(activityDocumentFile) {
					var fileName = activityDocumentFile.fileName;
					var fileNameWithoutEnding = activityDocumentFile.fileName;
					var fileNameEnding = fileName.substr(fileName.lastIndexOf('.')+1, fileName.length);
			        if(fileNameEnding.length === 3) {
			            fileNameWithoutEnding = fileName.substr(0, fileName.lastIndexOf('.'));
			            return fileNameWithoutEnding;
			        } else {
			        	return fileName;
			        }
				};
				
				$scope.getFileNameExtension = function(activityDocumentFile) {
					var fileName = activityDocumentFile.fileName;
					var fileNameEnding = fileName.substr(fileName.lastIndexOf('.')+1, fileName.length);
			        if(fileNameEnding.length === 3) {
			            return fileNameEnding;
			        } else {
			        	return 'NO_ENDING';
			        }
				};
			 }
		};
		return directive;
		
		////////////
	}
})();