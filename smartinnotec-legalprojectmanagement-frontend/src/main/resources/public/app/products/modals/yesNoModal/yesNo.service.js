(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('yesNoDeleteProductModalService', yesNoDeleteProductModalService);
        
    yesNoDeleteProductModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoDeleteProductModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(product, invoker, contactsReferencedToProduct) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	product: function() {
			    		return product;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contactsReferencedToProduct: function() {
			    		return contactsReferencedToProduct;
			    	}
			    }, 
				templateUrl: 'app/products/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, product, invoker, contactsReferencedToProduct) {
		    	var vm = this; 
		    	vm.product = product;
		    	vm.invoker = invoker;
		    	vm.contactsReferencedToProduct = contactsReferencedToProduct;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeProductYesNoModal = closeProductYesNoModal; 
		    	
		    	////////////
		    	
		    	function yes() {
		    		invoker.deleteProduct(vm.product); 
		    		vm.closeProductYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeProductYesNoModal();
		    	}
		    	
		    	function closeProductYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();