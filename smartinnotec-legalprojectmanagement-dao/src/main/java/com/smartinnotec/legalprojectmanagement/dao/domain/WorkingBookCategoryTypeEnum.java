package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum WorkingBookCategoryTypeEnum {

    TAEGLICH("TAEGLICH"),
    WOECHENTLICH("WOECHENTLICH"),
    MONATLICH("MONATLICH"),
    VIERTELJAEHRLICH("VIERTELJAEHRLICH"),
    HALBJAEHRLICH("HALBJAEHRLICH"),
    JAEHRLICH("JAEHRLICH"),
    SONSTIGES("SONSTIGES");

    private String value;

    private WorkingBookCategoryTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
