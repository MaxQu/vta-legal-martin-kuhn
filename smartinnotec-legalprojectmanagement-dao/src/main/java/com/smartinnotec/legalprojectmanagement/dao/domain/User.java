package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.Iterator;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;

import lombok.Data;

@Document(collection = "User")
public @Data class User implements Comparable<User> {

    @JsonView(RestServiceResponseView.UserPublic.class)
    @Id
    protected String id;
    @JsonView(RestServiceResponseView.UserPublic.class)
    protected String title;
    @JsonView(RestServiceResponseView.UserPublic.class)
    @TextIndexed
    protected String firstname;
    @JsonView(RestServiceResponseView.UserPublic.class)
    @TextIndexed
    protected String surname;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private SexEnum sex;
    @JsonView(RestServiceResponseView.UserPublic.class)
    protected String telephone;
    @JsonView(RestServiceResponseView.UserPublic.class)
    protected List<String> anotherTelephones;
    @JsonView(RestServiceResponseView.UserPublic.class)
    @TextIndexed
    protected String email;
    @JsonView(RestServiceResponseView.UserPublic.class)
    protected List<String> anotherEmails;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private boolean emailActive;
    @JsonView(RestServiceResponseView.UserPublic.class)
    @TextIndexed
    private String username;
    private String password;
    @JsonView(RestServiceResponseView.UserPublicAndAddress.class)
    protected Address address;
    @JsonView(RestServiceResponseView.UserPublic.class)
    protected boolean active;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String color;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private UserRoleEnum userRole;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String profileImagePath;
    @DBRef
    @JsonView(RestServiceResponseView.UserPublic.class)
    private Tenant tenant;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private CommunicationModeEnum communicationMode;
    @DBRef
    @JsonView(RestServiceResponseView.UserPublic.class)
    private UserAuthorizationUserContainer userAuthorizationUserContainer;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private Integer searchHistoryAmount;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private Integer contactPageSize;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private List<String> productIds; // products at dashboard
    @JsonView(RestServiceResponseView.UserPublic.class)
    private List<String> projectUserConnectionIds; // projects at dashboard
    @JsonView(RestServiceResponseView.UserPublic.class)
    private ProjectUserConnectionRoleEnum projectUserConnectionRole;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private List<AgentNumberContainer> agentNumberContainers; // Vertreternummern

    @JsonView(RestServiceResponseView.UserPublic.class)
    private String colorOfProducts;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String colorOfProjects;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String colorOfCalendarEvents;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String colorOfReceivedOrders;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String colorOfSentOrders;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private String colorOfContactsImported;
    @JsonView(RestServiceResponseView.UserPublic.class)
    private boolean allContactsAvailable;

    public User() {
        communicationMode = CommunicationModeEnum.TODO; // default
        allContactsAvailable = false;
    }

    public User(final UserBuilder userBuilder) {
        this.title = userBuilder.getTitle();
        this.firstname = userBuilder.getFirstname();
        this.surname = userBuilder.getSurname();
        this.telephone = userBuilder.getTelephone();
        this.email = userBuilder.getEmail();
        this.emailActive = userBuilder.isEmailActive();
        this.username = userBuilder.getUsername();
        this.password = userBuilder.getPassword();
        this.sex = userBuilder.getSex();
        this.active = userBuilder.isActive();
        this.color = userBuilder.getColor();
        this.userRole = userBuilder.getUserRole();
        this.profileImagePath = userBuilder.getProfileImagePath();
        this.searchHistoryAmount = 5;
        this.contactPageSize = 10;
        this.productIds = userBuilder.getProductIds();
        this.projectUserConnectionIds = userBuilder.getProjectUserConnectionIds();
        this.address = new Address();
        this.address.setPostalCode(userBuilder.getPostalCode());
        this.address.setStreet(userBuilder.getStreet());
        this.address.setRegion(userBuilder.getRegion());
        this.address.setCountry(userBuilder.getCountry().toString());
        this.agentNumberContainers = userBuilder.getAgentNumberContainers();
        this.colorOfProducts = userBuilder.getColorOfProducts();
        this.colorOfProjects = userBuilder.getColorOfProjects();
        this.colorOfCalendarEvents = userBuilder.getColorOfCalendarEvents();
        this.colorOfReceivedOrders = userBuilder.getColorOfReceivedOrders();
        this.colorOfSentOrders = userBuilder.getColorOfSentOrders();
        this.colorOfContactsImported = userBuilder.getColorOfContactsImported();
        this.allContactsAvailable = userBuilder.isAllContactsAvailable();
    }

    public void removeProductId(final String productIdToRemove) {
        if (productIds != null && !productIds.isEmpty()) {
            for (final Iterator<String> iterator = productIds.iterator(); iterator.hasNext();) {
                final String productId = iterator.next();
                if (productId.equals(productIdToRemove)) {
                    iterator.remove();
                    break;
                }
            }
        }
    }

    public void removeProjectUserConnectionId(final String projectUserConnectionIdToRemove) {
        if (projectUserConnectionIds != null && !projectUserConnectionIds.isEmpty()) {
            for (final Iterator<String> iterator = projectUserConnectionIds.iterator(); iterator.hasNext();) {
                final String projectUserConnectionId = iterator.next();
                if (projectUserConnectionId.equals(projectUserConnectionIdToRemove)) {
                    iterator.remove();
                    break;
                }
            }
        }
    }

    @Override
    public int compareTo(final User user) {
        if (this.id.equals(user.getId())) {
            return 0;
        } else {
            return -1;
        }
    }
}
