package com.smartinnotec.legalprojectmanagement.restservice.passwordencoding;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncoderTest {

    final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");

    @Test
    public void shouldEncodePassword() {
        final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        final String passwordEncoded = passwordEncoder.encode("aDMIn!WDM!Access#");
        System.out.println("Admin password: " + passwordEncoded); // $2a$10$4hfQQ0DaXOI/2q5No5NGwuYiinLkJCPcDzPrb8pm7I0VMMZnifuMC
    }

    @Test
    public void shouldTestJodaTimeInterval() {
        final DateTime start = dateTimeFormatter.parseDateTime("14.09.2016 11:00");
        final DateTime end = dateTimeFormatter.parseDateTime("15.09.2016 11:00");

        final DateTime test = dateTimeFormatter.parseDateTime("14.09.2016 11:00");

        Interval interval = new Interval(start, end);

        final Boolean check = interval.contains(test);
        Assert.assertTrue(check);
    }
}
