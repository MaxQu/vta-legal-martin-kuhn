package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Receipe;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeTypeEnum;

public class ReceipeTest extends AbstractDAOTest {

    @Test
    public void shouldGetLatestReceipe() throws BusinessException {
        final Receipe receipe1 = new Receipe();
        receipe1.setProductId("1111");
        receipe1.setReceipeType(ReceipeTypeEnum.STANDARD_MIXTURE);
        receipe1.setDate(dateTimeFormatter.parseDateTime("12.07.2017 11:24"));
        final Receipe createdReceipe1 = receipeRepository.insert(receipe1);
        Assert.assertNotNull(createdReceipe1);

        final Receipe receipe2 = new Receipe();
        receipe2.setProductId("1111");
        receipe2.setReceipeType(ReceipeTypeEnum.STANDARD_MIXTURE);
        receipe2.setDate(dateTimeFormatter.parseDateTime("12.07.2017 11:30"));
        final Receipe createdReceipe2 = receipeRepository.insert(receipe2);
        Assert.assertNotNull(createdReceipe2);

        final Receipe latestReceipe = receipeRepository.findByProductIdAndReceipeTypeOrderByDateDesc("1111",
            ReceipeTypeEnum.STANDARD_MIXTURE);
        Assert.assertNotNull(latestReceipe);
        Assert.assertEquals(dateTimeFormatter.print(latestReceipe.getDate()), "12.07.2017 11:30");
    }
}
