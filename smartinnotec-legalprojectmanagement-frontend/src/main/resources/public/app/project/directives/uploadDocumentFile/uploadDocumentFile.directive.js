(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common') 
    	.directive('uploadDocumentFileDirective', uploadDocumentFileDirective);
    
    uploadDocumentFileDirective.$inject = ['$timeout'];
    
	function uploadDocumentFileDirective($timeout) {
		var directive = {
			restrict: 'E',  
			scope: {
				files: '=', 
				uploadFiles: '=',
				successfulUploadedFiles: '=',
				failedUploadedFiles: '='
			},
			templateUrl: 'app/project/directives/uploadDocumentFile/uploadDocumentFile.html',
			link: function($scope) {
				
			}
		};
		return directive;
		
		////////////
	}
})();
