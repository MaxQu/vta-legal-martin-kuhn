(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.dashboard')
    	.factory('contactExportYesNoModalService', contactExportYesNoModalService);
        
    contactExportYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function contactExportYesNoModalService($modal, $stateParams) {
		var service = {
			showContactExportYesNoModal: showContactExportYesNoModal
		};		
		return service;
		
		////////////
				
		function showContactExportYesNoModal(invoker, contactChanged, index) {
			 var yesNoModal = $modal.open({
				controller: ContactExportYesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	contactChanged: function() {
			    		return contactChanged;
			    	},
			    	index: function() {
			    		return index;
			    	}
			    }, 
				templateUrl: 'app/dashboard/modals/contactExportYesNoModal/contactExportYesNo.modal.html'
			});
			return yesNoModal;
			
			function ContactExportYesNoModalController($modalInstance, $scope, invoker, contactChanged, index) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.contactChanged = contactChanged;
		    	vm.index = index;
		    	
		    	vm.deleteContactExportYesNoModal = deleteContactExportYesNoModal;
		    	vm.closeContactExportYesNoModal = closeContactExportYesNoModal;
		    	
		    	////////////
		    	
		    	function deleteContactExportYesNoModal() {   
		    		invoker.deleteContactChanged(vm.contactChanged, vm.index);
		    		vm.closeContactExportYesNoModal();
		    	}
		    	
		    	function closeContactExportYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();