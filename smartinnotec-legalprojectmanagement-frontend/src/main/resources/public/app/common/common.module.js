(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.common', [
    		'legalprojectmanagement.constants',
    		'ui.bootstrap.showErrors',
		    'LocalStorageModule',
		    'http-auth-interceptor'
    	]);
})();