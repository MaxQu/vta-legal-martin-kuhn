(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationService', communicationService);
        
    communicationService.$inject = ['$http', 'api_config'];
    
    function communicationService($http, api_config) {
		var service = {
			createCommunicationUserConnectionMessages: createCommunicationUserConnectionMessages,
			updateCommunicationUserConnectionMessages: updateCommunicationUserConnectionMessages,
			createCommunicationGroup: createCommunicationGroup,
			createCommunicationGroupUserConnections: createCommunicationGroupUserConnections,
			findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween: findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween,
			findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween: findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween,
			findCommunicationGroupUserConnectionsByUser: findCommunicationGroupUserConnectionsByUser,
			findCommunicationGroupUserConnectionsByCommunicationGroup: findCommunicationGroupUserConnectionsByCommunicationGroup,
			findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived: findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived,
			findCommunicationUserConnectionByUserCreatedMessageNotAndAndUserReceivedMessageAndTenantAndArchived: findCommunicationUserConnectionByUserCreatedMessageNotAndAndUserReceivedMessageAndTenantAndArchived,
			getCommunicationUserConnectionsOfUserAndPage: getCommunicationUserConnectionsOfUserAndPage,
			getMessageReceiverOverview: getMessageReceiverOverview,
			updateCommunicationGroup: updateCommunicationGroup,
			updateCommunicationGroupUserConnections: updateCommunicationGroupUserConnections,
			updateCommunicationUserConnection: updateCommunicationUserConnection,
			updateCommunicationMessage: updateCommunicationMessage,
			deleteCommunicationGroup: deleteCommunicationGroup,
			findSentAndOpenOrdersWithConfirmation: findSentAndOpenOrdersWithConfirmation,
			findReceivedAndOpenOrdersWithConfirmation: findReceivedAndOpenOrdersWithConfirmation,
			findSentAndOpenOrders: findSentAndOpenOrders,
			findReceivedAndOpenOrders: findReceivedAndOpenOrders
		};
		
		return service;
		
		////////////
		
		function createCommunicationUserConnectionMessages(userIdSender, communicationMessage) {
			return $http.post(api_config.BASE_URL + '/communications/communication/' + userIdSender, communicationMessage);
		}
		
		function updateCommunicationUserConnectionMessages(communicationMessage) {
			return $http.put(api_config.BASE_URL + '/communications/communication/communicationmessage', communicationMessage);
		}
		
		function createCommunicationGroup(communicationGroup) {
			return $http.post(api_config.BASE_URL + '/communications/communication/communicationgroup', communicationGroup);
		}
		
		function createCommunicationGroupUserConnections(communicationGroupId, userIds) {
			return $http.post(api_config.BASE_URL + '/communications/communication/communicationgroupuserconnection/' + communicationGroupId, userIds);
		}
		
		function findCommunicationUserConnectionByUserCreatedMessageAndSortDateBetween(userIdCreatedMessage, start, end) {
			return $http.get(api_config.BASE_URL + '/communications/communication/sent/range/' + userIdCreatedMessage + '/' + start + '/' + end + '/');
		}
		
		function findCommunicationUserConnectionByUserReceivedMessageAndSortDateBetween(userIdReceivedMessage, start, end) {
			return $http.get(api_config.BASE_URL + '/communications/communication/received/range/' + userIdReceivedMessage + '/' + start + '/' + end + '/');
		}
		
		function findCommunicationGroupUserConnectionsByUser(userId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/communicationgroupuserconnection/' + userId);
		}
		
		function findCommunicationGroupUserConnectionsByCommunicationGroup(communicationGroupId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/communicationgroupuserconnection/communicationgroup/' + communicationGroupId);
		}

		function findCommunicationUserConnectionByUserCreatedMessageAndUserReceivedMessageAndTenantAndArchived(userId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/usercreated/' + userId);
		}

		function findCommunicationUserConnectionByUserCreatedMessageNotAndAndUserReceivedMessageAndTenantAndArchived(userId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/userreceived/' + userId);
		}

		function getCommunicationUserConnectionsOfUserAndPage(userId, page) {
			return $http.get(api_config.BASE_URL + '/communications/communication/' + userId + '/' + page);
		}
		
		function getMessageReceiverOverview(messageId) {
			return $http.get(api_config.BASE_URL + '/communications/communication/receiver/' + messageId);
		}
		
		function updateCommunicationGroup(communicationGroup) {
			return $http.put(api_config.BASE_URL + '/communications/communication', communicationGroup);
		}
		
		function updateCommunicationGroupUserConnections(communicationGroupId, userIds) {
			return $http.put(api_config.BASE_URL + '/communications/communication/communicationgroupuserconnection/' + communicationGroupId, userIds);
		}
		
		function updateCommunicationUserConnection(communicationUserConnection) {
			return $http.put(api_config.BASE_URL + '/communications/communication/communicationuserconnection/' + communicationUserConnection.id, communicationUserConnection);
		}
		
		function updateCommunicationMessage(communicationId, communicationUserConnection) {
			return $http.put(api_config.BASE_URL + '/communications/communication/communication/' + communicationId, communicationUserConnection);
		}
		
		function deleteCommunicationGroup(communicationGroupId) {
			return $http.delete(api_config.BASE_URL + '/communications/communication/' + communicationGroupId);
		}
		
		function findSentAndOpenOrdersWithConfirmation(userIdCreatedMessage, confirmationNeeded, read, executed) {
			return $http.get(api_config.BASE_URL + '/communications/communication/sentandopenorderswithconfirmation/' + userIdCreatedMessage + '/' + confirmationNeeded + '/' + read + '/' + executed);
		}
		
		function findReceivedAndOpenOrdersWithConfirmation(userIdReceivedMessage, confirmationNeeded, read, executed) {
			return $http.get(api_config.BASE_URL + '/communications/communication/sentandopenorderswithconfirmation/' + userIdReceivedMessage + '/' + confirmationNeeded + '/' + read + '/' + executed);
		}
		
		function findSentAndOpenOrders(userIdCreatedMessage, executed) {
			return $http.get(api_config.BASE_URL + '/communications/communication/sentandopenorders/' + userIdCreatedMessage + '/' + executed);
		}
		
		function findReceivedAndOpenOrders(userIdReceivedMessage, executed) {
			return $http.get(api_config.BASE_URL + '/communications/communication/receivedandopenorders/' + userIdReceivedMessage + '/' + executed);
		}
    }
})();