package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;

public interface WorkingBookRepository extends MongoRepository<WorkingBook, String> {

    List<WorkingBook> findByProject(final Project project);

    List<WorkingBook> findByProjectAndTenant(final Project project, final Tenant tenant);

    List<WorkingBook> findByProjectAndTenantAndDateTimeFromBetween(final Project project, final Tenant tenant, final DateTime start,
            final DateTime end);

    Page<WorkingBook> findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetweenOrderByCreationDateTimeDesc(final Pageable pageable,
            final Project project, final Tenant tenant, final DateTime start, final DateTime end);

    Integer countWorkingBookByProjectAndTenantAndDateTimeFromBetween(final Project project, final Tenant tenant, final DateTime start,
            final DateTime end);

    WorkingBook findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc(final Project project, final Tenant tenant,
            final WorkingBookCategoryTypeEnum categoryType);

    @Query("{$text : { $search : ?0 } }")
    List<WorkingBook> findWorkingBookBySearchString(final String searchString);
}
