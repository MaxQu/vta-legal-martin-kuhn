package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationStateEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationUserContainer;

public interface UserAuthorizationUserContainerService {

    UserAuthorizationUserContainer findById(final String id) throws BusinessException;

    boolean isUserAuthorized(final User user, final UserAuthorizationTypeEnum userAuthorizationType,
            final UserAuthorizationStateEnum userAuthorizationState) throws BusinessException;

    UserAuthorizationUserContainer findUserAuthorizationUserContainerByUserId(final String userId) throws BusinessException;

    UserAuthorizationUserContainer create(final UserAuthorizationUserContainer userAuthorizationUserContainer) throws BusinessException;

    UserAuthorizationUserContainer create(final String userId, final ProjectUserConnectionRoleEnum projectUserConnectionRole)
            throws BusinessException;

    UserAuthorizationUserContainer update(final UserAuthorizationUserContainer userAuthorizationUserContainer) throws BusinessException;

    void delete(final String id) throws BusinessException;

    void deleteOfUserId(final String userId) throws BusinessException;

}
