(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.controller('ProjectsController', ProjectsController);
    
    ProjectsController.$inject = ['$scope', '$timeout', 'currentUser', 'projectUserConnectionRoles', 'provinceTypes', 'userAuthorizationUserContainer', 'dateUtilityService', 'projectsService', 'projectUserConnectionService', 'userService', 'yesNoDeleteProjectModalService', 'changeProjectPropertiesModalService', 'projectUserManagementModalService', 'contactAndUserService', 'documentFileService', 'projectDataSheetService', 'printingPDFService', 'utilService'];
     
    function ProjectsController($scope, $timeout, currentUser, projectUserConnectionRoles, provinceTypes, userAuthorizationUserContainer, dateUtilityService, projectsService, projectUserConnectionService, userService, yesNoDeleteProjectModalService, changeProjectPropertiesModalService, projectUserManagementModalService, contactAndUserService, documentFileService, projectDataSheetService, printingPDFService, utilService) {
	    $scope.vm = this; 
    	var vm = this;
    	
    	vm.currentUser = currentUser;
    	vm.generalAvailableProjects = null;
    	vm.projectUserConnections = null;
    	vm.projectUserConnectionRoles = projectUserConnectionRoles.data;
    	vm.provinceTypes = provinceTypes.data;
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.dateUtilityService = dateUtilityService;
    	vm.showProjectCreationTrigger = false;
    	vm.project = {
    			name: '',
    			description: '',
    			creationUserId: '',
    			start: null,
    			end: null,
    			color: '#007ab9', 
    			logoName: ''
    			};
    	vm.projectDataSheet = { 
				operatorName: '',
			    operatorStreet: '',
			    operatorNumber: 0,
			    operatorZip: 1234,
			    operatorLocation: '',
			    operatorProvinceType: 'EMPTY_PROVINCE',

			    locationStreet: '',
			    locationNumber: 0,
			    locationZip: 1234,
			    locationLocation: '',
			    locationProvinceType: 'EMPTY_PROVINCE',
			    locationRegion: '',
			    locationEstateNumber: '',
			    locationArchiveNumber: '',
			    locationCastralCommunity: '',

			    projectDataSheetIdentificationFacilities: [{
			    	identificationFacilityName: '',
				    identificationFacilityPower: '',
				    identificationFacilityType: ''
			    }],

			    projectDataSheetApprovalOrders: [{
			    	approvalOrderAgency: '',
				    approvalOrderNumber: '',
				    approvalOrderDate: null
			    }],

			    authorityName: '',
			    authorityStreet: '',
			    authorityNumber: 0,
			    authorityZip: 1234,
			    authorityLocation: '',
			    authorityProvinceType: 'EMPTY_PROVINCE',
			    authorityApprovalPerson: '',

			    projectDataSheetChanges: [{
			    	changeContent: '',
				    chanceResponsibleAgency: '',
				    changeNumber: '',
				    changeDate: null
			    }]
			};
    	vm.projectUserConnectionsOfSelectedProject = [];
    	vm.selectedUser = null;
    	vm.selectedProject = null;
    	vm.selectedProjectUserConnectionRole = null;
    	vm.orderType = 'name';
    	vm.projectNameFilter = null;
    	
    	vm.printContainer = printContainer;
    	vm.deleteProject = deleteProject;
    	vm.getAmountOfDocumentFilesWithUniqueName = getAmountOfDocumentFilesWithUniqueName;
    	vm.findContactAndUserBySearchString = findContactAndUserBySearchString;
    	vm.projectFilter = projectFilter;
    	vm.showYesNoDeleteModal = showYesNoDeleteModal;
    	vm.showProjectUserManagementModal = showProjectUserManagementModal;
    	vm.showChangeProjectPropertiesModalService = showChangeProjectPropertiesModalService;
    	vm.showProjectDataSheetModal = showProjectDataSheetModal;
    	loadGeneralAvailableProjects();
    	loadProjectUserConnections();
    	
    	////////////
    	
    	function loadGeneralAvailableProjects() {
    		projectsService.findProjectsGeneralAvailable().then(function(response) {
    			vm.generalAvailableProjects = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error in projects.controller.js#loadProjectUserConnections');
 	   		 });
    	}
    	
    	function loadProjectUserConnections() {
    		projectUserConnectionService.findProjectUserConnectionByUser(vm.currentUser.id).then(function(response) {
    			vm.projectUserConnections = response.data;
 	   		 }, function errorCallback(response) {
 	   			  console.log('error in projects.controller.js#loadProjectUserConnections');
 	   		 });
    	}
    	
    	function printContainer(container, name) {
    		printingPDFService.printSchedule(container, name);
    	}
    	
    	function getAmountOfDocumentFilesWithUniqueName(project) { 
    		documentFileService.countByProjectIdGroupByOriginalFileName(project.id, false, 'PROJECT').then(function(response) {
    			project.amountOfDocumentFilesWithUniqueName = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error in projects.controller.js#getAmountOfDocumentFilesWithUniqueName');
  	   		 });
    	}
    	
    	function showYesNoDeleteModal(project) {
    		yesNoDeleteProjectModalService.showYesNoModal(project, vm, 'PROJECT');
    	}
    	
    	function showChangeProjectPropertiesModalService(project) {
    		projectsService.findProjectDataSheet(project.id).then(function successCallback(response) {
    			var projectDataSheet = response.data;
    			if(project.start instanceof Date) {
    				project.start = dateUtilityService.formatDateToDateTimeString(project.start);
    			} 
    			if(project.end instanceof Date) {
    				project.end = dateUtilityService.formatDateToDateTimeString(project.end);
    			} 
    			changeProjectPropertiesModalService.showChangeProjectPropertiesModal(project, projectDataSheet, vm.provinceTypes, vm.currentUser);
   	   		 }, function errorCallback(response) {
   	   			  console.log('error projectUserManagementModalService#showChangeProjectPropertiesModalService');
   	   		 });
    	}
    	
    	function showProjectUserManagementModal(project, projectUserConnection) {
    		projectUserConnectionService.findProjectUserConnectionByProject(project.id).then(function(response) {
    			unsetProjectUserConnectionsSelection();
    			if(projectUserConnection != null) {
    				projectUserConnection.selected = true;
    			}
        		projectUserManagementModalService.showProjectUserManagementModal(project, response.data, vm.projectUserConnections, vm.projectUserConnectionRoles, currentUser, vm);
   	   		 }, function errorCallback(response) {
   	   			  console.log('error projectUserManagementModalService#showProjectUserManagementModal');
   	   		 });
    	}
    	
    	function deleteProject(project) { 
    		if(project.documentFiles != null && project.documentFiles.length > 0) {
    			project.deletionError = true;
    			return;
    		}
    		projectsService.deleteProject(project.id).then(function successCallback(response) {
    			var keepGoing = true;
    			angular.forEach(vm.projectUserConnections, function(value, key) {
    				if(keepGoing) {
	  				  if(value.project.id == project.id) {
	  					  vm.projectUserConnections.splice(key, 1);
	  					  keepGoing = false;
	  				  }
    				}
  				});
  	   		 }, function errorCallback(response) {
  	   			 project.deletionError = true;
  	   			 project.deletionErrorMessage = response.data.error;
  	   			 console.log('error projects.controller#deleteProject: ' + response.data.error + ', message: ' + response.data.message + ', status: ' + response.data.status);
  	   		 });
    	}
    	
    	function unsetProjectUserConnectionsSelection() {
    		for(var i = 0; i < vm.projectUserConnections.length; i++) {
    			vm.projectUserConnections[i].selected = false;
    		}
    	}
    	
    	function findContactAndUserBySearchString(searchString) { 
    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function (response) {
    			return response.data;
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findContactBySearchString: ' + data);
			});
    	}

    	function projectFilter(projectNameFilter) {
    		return function(item) {
    			if(projectNameFilter == null ||projectNameFilter === '') {
    				return true;
    			}
    			projectNameFilter = utilService.replaceAll(projectNameFilter, '\\*', '');
    			return item.project.name.indexOf(projectNameFilter) > -1;
    		  };
    	}
    	
    	function showProjectDataSheetModal(project) {
    		projectsService.findProjectDataSheet(project.id).then(function (response) {
    			projectDataSheetService.showProjectDataSheetModal(response.data); 
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findContactBySearchString: ' + data);
			});
    	}
	} 
})();