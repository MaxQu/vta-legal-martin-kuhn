(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('yesNoModalService', yesNoModalService);
        
    yesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(documentFile, documentFileVersion, invoker, type) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	documentFile: function() {
			    		return documentFile;
			    	},
			    	documentFileVersion: function() {
			    		return documentFileVersion;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	},
			    	type: function() {
			    		return type;
			    	}
			    }, 
				templateUrl: 'app/project/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, documentFile, documentFileVersion, invoker, type) {
		    	var vm = this; 
		    	vm.documentFile = documentFile;
		    	vm.documentFileVersion = documentFileVersion;
		    	vm.invoker = invoker;
		    	vm.type = type;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		if(vm.type == 'DELETE_VERSION') {
		    			invoker.deleteDocumentFileVersion(documentFile, documentFileVersion);
		    		} else if(vm.type == 'DELETE_ALL_VERSIONS') {
		    			invoker.deleteAllDocumentVersions(documentFile);
		    		}
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();