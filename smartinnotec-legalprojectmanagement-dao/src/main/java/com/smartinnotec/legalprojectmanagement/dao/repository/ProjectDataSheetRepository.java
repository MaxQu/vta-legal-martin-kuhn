package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectDataSheet;

public interface ProjectDataSheetRepository extends MongoRepository<ProjectDataSheet, String> {

    ProjectDataSheet findByProjectId(final String projectId);

}
