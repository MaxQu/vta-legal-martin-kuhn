package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.AgentNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAddressWithProducts;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface ContactRepository extends MongoRepository<Contact, String> {

    List<Contact> findByContactAttachmentsNotNull();

    List<Contact> findByTenantAndImportedAndActive(final Tenant tenant, final boolean imported, final boolean active);

    List<Contact> findByTenantAndImportedAndImportConfirmedAndImportationDateAndActive(final Tenant tenant, final boolean imported,
            final boolean importConfirmed, final DateTime importationDate, final boolean active);

    List<Contact> findByTenantAndActive(final Tenant tenant, final boolean active);

    Contact findByCustomerNumberContainersIn(final CustomerNumberContainer customerNumberContainer);

    Integer countByTenantAndActive(final Tenant tenant, final boolean active);

    Integer countByActive(final boolean active);

    List<Contact> findPagedContactsByTenantAndActive(final Tenant tenant, final Pageable pageable, final boolean active);

    List<Contact> findByAgentNumberContainersIn(final List<AgentNumberContainer> agentNumberContainers);

    List<Contact> findByAddressesWithProductsExists(final boolean exists);

    List<Contact> findByContactTypesIn(final ContactTypeEnum contactType);

    List<Contact> findByTenantAndContactTypesIn(final Tenant tenant, final ContactTypeEnum contactType);

    Contact findContactByAddressesWithProductsIn(final ContactAddressWithProducts contactAddressWithProducts);

    @Query("{$or : [{'institution': { $regex: ?0, $options:'i' }}, {'information': { $regex: ?0, $options:'i' }}]}")
    List<Contact> findContactByRegexString(final String regexString);

    @Query("{$text : { $search : ?0 } }")
    List<Contact> findContactBySearchString(final String searchString);
}
