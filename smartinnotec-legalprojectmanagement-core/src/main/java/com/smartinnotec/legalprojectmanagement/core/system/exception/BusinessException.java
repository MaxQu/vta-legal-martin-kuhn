package com.smartinnotec.legalprojectmanagement.core.system.exception;

public class BusinessException extends Exception implements ErrorCodeAwareAble {

    /**
     * 
     */
    private static final long serialVersionUID = -5187015503111816320L;

    /**
     * the error code.
     */
    private String errorCode;
    private String errorCodeType;

    /**
     * Constructor.
     */
    public BusinessException() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param message the message of the exception.
     * @param cause the cause of the exception.
     */
    public BusinessException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     * 
     * @param message the message of the exception.
     * @param errorCode the errorCode of the exception.
     * @param cause the cause of the exception.
     */
    public BusinessException(final String message, final String errorCode, final Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * Constructor.
     * 
     * @param message the message of the exception.
     */
    public BusinessException(final String message) {
        super(message);
    }

    public BusinessException(final String message, final String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * Constructor.
     * 
     * @param message the message of the exception.
     * @param errorCode the errorCode of the exception.
     */
    public BusinessException(final String message, final String errorCode, final String errorCodeType) {
        super(message);
        this.errorCode = errorCode;
        this.errorCodeType = errorCodeType.toString();
    }

    /**
     * Constructor.
     * 
     * @param cause the cause of the exception.
     */
    public BusinessException(final Throwable cause) {
        super(cause);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorCodeType() {
        return errorCodeType;
    }

    public void setErrorCodeType(final String errorCodeType) {
        this.errorCodeType = errorCodeType;
    }

}
