(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.superadministration')
    	.factory('superAdministrationService', superAdministrationService);
        
    superAdministrationService.$inject = ['$http', 'api_config'];
    
    function superAdministrationService($http, api_config) {
		var service = {
			findAllTenants: findAllTenants,
			createTenant: createTenant,
			deleteTenant: deleteTenant
		};
		
		return service;
		
		////////////
		
		function findAllTenants() {
			return $http.get(api_config.BASE_URL + '/tenants/tenant/all');
		}
		
		function createTenant(tenant) {
			return $http.post(api_config.BASE_URL + '/tenants/tenant', tenant);
		}
		
		function deleteTenant(id) {
			return $http.delete(api_config.BASE_URL + '/tenants/tenant/' + id);
		}
    }
})();


