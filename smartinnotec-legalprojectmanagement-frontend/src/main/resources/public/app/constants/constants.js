(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.constants', [])
    	.constant('api_config', api_config())
    	.constant('token_config', token_config());
    
    function api_config() {
    	var constant = {
			BASE_URL: '/api'
    	};
    	return constant;
    }
    
    function token_config() {
    	var constant = {
			AUTH_TOKEN_KEY: 'legalProjectManagement-AuthToken',
			AUTH_TOKEN_HEADER: 'X-AUTH-TOKEN'
    	};
    	return constant;
    }  
})();