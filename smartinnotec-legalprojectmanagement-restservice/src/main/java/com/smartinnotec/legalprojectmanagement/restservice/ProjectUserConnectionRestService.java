package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.CalendarEventService;
import com.smartinnotec.legalprojectmanagement.service.ContactService;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.ProjectService;
import com.smartinnotec.legalprojectmanagement.service.ProjectUserConnectionService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class ProjectUserConnectionRestService extends AbstractRestService {

    private static final String PROJECT_START;
    private static final String PROJECT_END;

    static {
        PROJECT_START = "Projektstart";
        PROJECT_END = "Projektende";
    }

    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private CalendarEventService calendarEventService;
   
    @Autowired
    private HistoryService historyService;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;

    @RequestMapping(value = "/projectuserconnections/{projectuserconnectionid}", method = {
        RequestMethod.GET }, produces = "application/json")
    public ProjectUserConnection findProjectUserConnection(final @PathVariable("projectuserconnectionid") String projectUserConnectionId)
            throws BusinessException {
        BusinessAssert.notNull(projectUserConnectionId,
            "projectUserConnectionId is mandatory in ProjectUserConnectionRestService#findProjectUserConnection", "400");
        BusinessAssert.isId(projectUserConnectionId,
            "projectUserConnectionId must be an id in ProjectUserConnectionRestService#findProjectUserConnection", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get projectUserConnection with id '{}' in ProjectUserConnectionRestService#findProjectUserConnection",
            projectUserConnectionId);
        final ProjectUserConnection projectUserConnection = projectUserConnectionService.findProjectUserConnection(projectUserConnectionId);
        logger.debug("found projectUserConnection with id '{}' in ProjectUserConnectionRestService#findProjectUserConnection",
            projectUserConnection.getId());
        return projectUserConnection;
    }

    @RequestMapping(value = "/projectuserconnections/user/{userid}", method = { RequestMethod.GET }, produces = "application/json")
    public List<ProjectUserConnection> findProjectUserConnectionsByUser(final @PathVariable("userid") String userId)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in ProjectUserConnectionRestService#findProjectUserConnectionByUser", "400");
        BusinessAssert.isId(userId, "userId must be an id in ProjectUserConnectionRestService#findProjectUserConnectionByUser", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get projectUserConnections of user with id '{}' in ProjectUserConnectionRestService#findProjectUserConnectionByUser",
            userId);
        final User user = userService.findUser(userId);
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);
        logger.debug(
            "found '{}' projectUserConnections for user with id '{}' in ProjectUserConnectionRestService#findProjectUserConnectionByUser",
            projectUserConnections.size(), user.getId());
        return projectUserConnections;
    }

    @RequestMapping(value = "/projectuserconnections/user/{userid}/selected", method = { RequestMethod.GET }, produces = "application/json")
    public List<ProjectUserConnection> findSelectedProjectUserConnectionsByUser(final @PathVariable("userid") String userId)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in ProjectUserConnectionRestService#findSelectedProjectUserConnectionsByUser",
            "400");
        BusinessAssert.isId(userId, "userId must be an id in ProjectUserConnectionRestService#findSelectedProjectUserConnectionsByUser",
            "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get projectUserConnections of user with id '{}' in ProjectUserConnectionRestService#findSelectedProjectUserConnectionsByUser",
            userId);
        final User user = userService.findUser(userId);
        final List<String> projectUserConnectionIds = user.getProjectUserConnectionIds();
        if (projectUserConnectionIds == null || projectUserConnectionIds.isEmpty()) {
            return new ArrayList<>();
        }
        final List<ProjectUserConnection> projectUserConnections = new ArrayList<>();
        for (final String projectUserConnectionId : projectUserConnectionIds) {
            final ProjectUserConnection projectUserConnection = projectUserConnectionService
                .findProjectUserConnection(projectUserConnectionId);
            projectUserConnections.add(projectUserConnection);
        }
        logger.debug(
            "found '{}' selected projectUserConnections for user with id '{}' in ProjectUserConnectionRestService#findSelectedProjectUserConnectionsByUser",
            projectUserConnections.size(), user.getId());
        return projectUserConnections;
    }

    @RequestMapping(value = "/projectuserconnections/project/{projectid}", method = { RequestMethod.GET }, produces = "application/json")
    public List<ProjectUserConnection> findProjectUserConnectionByProject(final @PathVariable("projectid") String projectId)
            throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in ProjectUserConnectionRestService#findProjectUserConnectionByProject",
            "400");
        BusinessAssert.isId(projectId, "projectId must be an id in ProjectUserConnectionRestService#findProjectUserConnectionByProject",
            "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get projectUserConnections of user with id '{}' in ProjectUserConnectionRestService#findProjectUserConnectionByProject",
            projectId);
        final Project project = projectService.findProject(projectId);
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByProjectAndActive(project, true);
        logger.debug(
            "found '{}' projectUserConnections for user with id '{}' in ProjectUserConnectionRestService#findProjectUserConnectionByProject",
            projectUserConnections.size(), project.getId());
        return projectUserConnections;
    }

    @RequestMapping(value = "/projectuserconnections/project/users/{projectid}", method = {
        RequestMethod.GET }, produces = "application/json")
    public List<ProjectUserConnection> findUsersOfProjectUserConnectionByProject(final @PathVariable("projectid") String projectId)
            throws BusinessException {
        BusinessAssert.notNull(projectId,
            "projectId is mandatory in ProjectUserConnectionRestService#findUsersOfProjectUserConnectionByProject", "400");
        BusinessAssert.isId(projectId,
            "projectId must be an id in ProjectUserConnectionRestService#findUsersOfProjectUserConnectionByProject", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get projectUserConnections of user with id '{}' in ProjectUserConnectionRestService#findUsersOfProjectUserConnectionByProject",
            projectId);
        final Project project = projectService.findProject(projectId);
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByProjectAndActive(project, true);
        logger.debug(
            "found '{}' projectUserConnections for user with id '{}' in ProjectUserConnectionRestService#findUsersOfProjectUserConnectionByProject",
            projectUserConnections.size(), project.getId());
        final List<User> users = new ArrayList<>();
        projectUserConnections.forEach(s -> users.add(s.getUser()));
        return projectUserConnections;
    }

    @RequestMapping(value = "/projectuserconnections/contact/{contactid}/project", method = { RequestMethod.GET })
    public List<ProjectUserConnection> findProjectUserConnectionsByContactAndProject(final @PathVariable("contactid") String contactId)
            throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId is mandatory in ProjectRestService#findProjectUserConnectionsByContactAndProject",
            "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ProjectRestService#findProjectUserConnectionsByContactAndProject",
            "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get projectUserConnections related with project of contact with id '{}' in ProjectRestService#findProjectUserConnectionsByContactAndProject",
            contactId);
        final List<ProjectUserConnection> projectUserConnectionsOfContact = projectUserConnectionService
            .findProjectUserConnectionsOfContactAndProject(contactId);
        return projectUserConnectionsOfContact;
    }

    @RequestMapping(value = "/projectuserconnections/contact/{contactid}/user", method = { RequestMethod.GET })
    public List<ProjectUserConnection> findProjectUserConnectionsByContactAndUser(final @PathVariable("contactid") String contactId)
            throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId is mandatory in ProjectRestService#findProjectUserConnectionsByContactAndUser", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ProjectRestService#findProjectUserConnectionsByContactAndUser", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get projectUserConnections related with user of contact with id '{}' in ProjectRestService#findProjectUserConnectionsByContactAndUser",
            contactId);
        final List<ProjectUserConnection> projectUserConnectionsOfContact = projectUserConnectionService
            .findProjectUserConnectionsOfContactAndUser(contactId);
        return projectUserConnectionsOfContact;
    }

    @RequestMapping(value = "/projectuserconnections/confidentialdocumentfiles/{projectuserconnectionid}", method = { RequestMethod.GET })
    public boolean canProjectUserConnectionRoleSeeConfidentialDocumentFiles(
            final @PathVariable("projectuserconnectionid") String projectUserConnectionId) throws BusinessException {
        BusinessAssert.notNull(projectUserConnectionId,
            "projectUserConnectionId is mandatory in ProjectRestService#canProjectUserConnectionRoleSeeConfidentialDocumentFiles", "400");
        BusinessAssert.isId(projectUserConnectionId,
            "projectUserConnectionId must be an id in ProjectRestService#canProjectUserConnectionRoleSeeConfidentialDocumentFiles", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": can projectUserConnection with id '{}' see confidential documentFiles in ProjectRestService#canProjectUserConnectionRoleSeeConfidentialDocumentFiles",
            projectUserConnectionId);
        final ProjectUserConnection projectUserConnection = projectUserConnectionService.findProjectUserConnection(projectUserConnectionId);
        return projectUserConnectionService
            .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(projectUserConnection.getProjectUserConnectionRole());
    }

    @RequestMapping(value = "/projectuserconnections/{userid}/{searchstring}/search", method = { RequestMethod.GET })
    public List<ProjectUserConnection> findProjectUserConnectionsOfUserByProjectSearchString(final @PathVariable("userid") String userId,
            final @PathVariable("searchstring") String searchString) throws BusinessException {
        BusinessAssert.notNull(userId, "userid is mandatory in ProjectRestService#findProjectUserConnectionsOfUserByProjectSearchString",
            "400");
        BusinessAssert.isId(userId, "userid must be an id in ProjectRestService#findProjectUserConnectionsOfUserByProjectSearchString",
            "400");
        BusinessAssert.notNull(searchString,
            "searchString is mandatory in ProjectRestService#findProjectUserConnectionsOfUserByProjectSearchString", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get projects of user with id '{}' by search string '{}' in ProjectRestService#findProjectUserConnectionsOfUserByProjectSearchString",
            userId, searchString);
        final List<ProjectUserConnection> foundedProjectUserConnections = projectUserConnectionService
            .findProjectUserConnectionsOfUserByProjectSearchString(userId, searchString);
        return foundedProjectUserConnections;
    }

    @RequestMapping(value = "/projectuserconnections", method = { RequestMethod.POST }, consumes = "application/json;charset=utf-8")
    public ProjectUserConnection create(final @RequestBody ProjectUserConnection projectUserConnection) throws BusinessException {
        BusinessAssert.notNull(projectUserConnection, "projectUserConnection is mandatory in ProjectUserConnectionRestService#create",
            "400");
        logger.info(
            getInvokedRestServiceUserInformation() + ": creation of projectUserConnection in ProjectUserConnectionRestService#create");

        final ProjectUserConnection createdProjectUserConnection = projectUserConnectionService.create(projectUserConnection);
        logger.debug("created createdProjectUserConnection with id '{}' in ProjectUserConnectionRestService#create",
            createdProjectUserConnection.getId());

        final User user = userService.findUser(super.getLoggedInUserId());
        
        // create calendarUserConnection for projectStart and projectEnd
        if (createdProjectUserConnection.getUser() != null && createdProjectUserConnection.getProject() != null) {
            final Project project = createdProjectUserConnection.getProject();

            final CalendarEvent calendarEventProjectStart = calendarEventService.findByProjectAndLocation(project, PROJECT_START);
            if (calendarEventProjectStart != null) {
                final CalendarEventUserConnection calendarEventUserConnection = new CalendarEventUserConnection();
                calendarEventUserConnection.setCalendarEvent(calendarEventProjectStart);
                calendarEventUserConnection.setEmailActive(true);
                calendarEventUserConnection.setUser(createdProjectUserConnection.getUser());
      
            }

            final CalendarEvent calendarEventProjectEnd = calendarEventService.findByProjectAndLocation(project, PROJECT_END);
            if (calendarEventProjectEnd != null) {
                final CalendarEventUserConnection calendarEventUserConnection = new CalendarEventUserConnection();
                calendarEventUserConnection.setCalendarEvent(calendarEventProjectEnd);
                calendarEventUserConnection.setEmailActive(true);
                calendarEventUserConnection.setUser(createdProjectUserConnection.getUser());
                
            }
        }

        String message = "";
        if (createdProjectUserConnection.getUser() != null) {
            message = createdProjectUserConnection.getUser().getFirstname() + " " + createdProjectUserConnection.getUser().getSurname();
        } else if (createdProjectUserConnection.getContact() != null) {
            message = createdProjectUserConnection.getContact().getInstitution();
        }
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.ADD_USER_OR_CONTACT_TO_PROJECT)
            .setProjectName(createdProjectUserConnection.getProject() != null ? createdProjectUserConnection.getProject().getName() : "")
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in ProjectRestService#create", createdHistory.getId());

        return createdProjectUserConnection;
    }

    @RequestMapping(value = "/projectuserconnections", method = { RequestMethod.PUT }, produces = "application/json")
    public ProjectUserConnection update(final @RequestBody ProjectUserConnection projectUserConnection) throws BusinessException {
        BusinessAssert.notNull(projectUserConnection, "projectUserconnectionsId is mandatory in ProjectUserConnectionRestService#update",
            "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": update projectUserConnection with id '{}' in ProjectUserConnectionRestService#update", projectUserConnection.getId());
        return projectUserConnectionService.update(projectUserConnection);
    }

    @RequestMapping(value = "/projectuserconnections/{projectuserconnectionid}", method = RequestMethod.DELETE)
    public void deleteProjectUserConnection(final @PathVariable(value = "projectuserconnectionid") String projectUserConnectionId)
            throws BusinessException {
        BusinessAssert.notNull(projectUserConnectionId,
            "projectUserconnectionsId is mandatory in ProjectUserConnectionRestService#deleteProjectUserConnection", "400");
        BusinessAssert.isId(projectUserConnectionId,
            "projectUserConnectionId must be an id in ProjectUserConnectionRestService#deleteProjectUserConnection", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": delete projectUserConnection with id '{}' in ProjectUserConnectionRestService#deleteProjectUserConnection",
            projectUserConnectionId);
        final User loggedInUser = userService.findUser(super.getLoggedInUserId());
        final ProjectUserConnection projectUserConnection = projectUserConnectionService.findProjectUserConnection(projectUserConnectionId);
        // delete calendarEventUserConnection of project
        final User user = projectUserConnection.getUser();
        if (user != null) {
            final CalendarEvent calendarEventProjectStart = calendarEventService
                .findByProjectAndLocation(projectUserConnection.getProject(), PROJECT_START);
            if (calendarEventProjectStart != null) {
               
            }
            final CalendarEvent calendarEventProjectEnd = calendarEventService.findByProjectAndLocation(projectUserConnection.getProject(),
                PROJECT_END);
           
        }
        projectUserConnectionService.delete(projectUserConnectionId, user);
    }

    @RequestMapping(value = "/projectuserconnections/{projectid}/{contactid}/project/contact", method = RequestMethod.DELETE)
    public ProjectUserConnection deleteProjectUserConnectionOverProjectAndContact(final @PathVariable(value = "projectid") String projectId,
            final @PathVariable(value = "contactid") String contactId) throws BusinessException {
        BusinessAssert.notNull(projectId,
            "projectId is mandatory in ProjectUserConnectionRestService#deleteProjectUserConnectionOverProjectAndContact", "400");
        BusinessAssert.isId(projectId,
            "projectId must be an id in ProjectUserConnectionRestService#deleteProjectUserConnectionOverProjectAndContact", "400");
        BusinessAssert.notNull(contactId,
            "contactId is mandatory in ProjectUserConnectionRestService#deleteProjectUserConnectionOverProjectAndContact", "400");
        BusinessAssert.isId(contactId,
            "contactId must be an id in ProjectUserConnectionRestService#deleteProjectUserConnectionOverProjectAndContact", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": delete projectUserConnection of project with id '{}' and contact with id '{}' in ProjectUserConnectionRestService#deleteProjectUserConnectionOverProjectAndContact",
            projectId, contactId);
        final Project project = projectService.findProject(projectId);
        final Contact contact = contactService.findContactById(contactId);
        final ProjectUserConnection projectUserConnection = projectUserConnectionService.findByProjectAndContactAndActive(project, contact,
            true);
        final User user = projectUserConnection.getUser();
        final String projectUserConnectionId = projectUserConnection.getId();
        projectUserConnectionService.delete(projectUserConnectionId, user);
        return projectUserConnection;
    }

    @RequestMapping(value = "/projectuserconnections/{userid}/{contactid}/user/contact", method = RequestMethod.DELETE)
    public ProjectUserConnection deleteProjectUserConnectionOverUserAndContact(final @PathVariable(value = "userid") String userId,
            final @PathVariable(value = "contactid") String contactId) throws BusinessException {
        BusinessAssert.notNull(userId,
            "userId is mandatory in ProjectUserConnectionRestService#deleteProjectUserConnectionOverUserAndContact", "400");
        BusinessAssert.isId(userId,
            "userId must be an id in ProjectUserConnectionRestService#deleteProjectUserConnectionOverUserAndContact", "400");
        BusinessAssert.notNull(contactId,
            "contactId is mandatory in ProjectUserConnectionRestService#deleteProjectUserConnectionOverUserAndContact", "400");
        BusinessAssert.isId(contactId,
            "contactId must be an id in ProjectUserConnectionRestService#deleteProjectUserConnectionOverUserAndContact", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": delete projectUserConnection of user with id '{}' and contact with id '{}' in ProjectUserConnectionRestService#deleteProjectUserConnectionOverUserAndContact",
            userId, contactId);
        User user = userService.findUser(userId);
        final Contact contact = contactService.findContactById(contactId);
        final ProjectUserConnection projectUserConnection = projectUserConnectionService.findByUserAndContactAndActive(user, contact, true);
        user = projectUserConnection.getUser();
        final String projectUserConnectionId = projectUserConnection.getId();
        projectUserConnectionService.delete(projectUserConnectionId, user);
        return projectUserConnection;
    }

    @Override
    public String toString() {
        return "[ProjectUserConnectionRestService]";
    }
}
