(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.error')
    	.config(configure);
    			
    configure.$inject = ['$httpProvider'];
    
	function configure($httpProvider) {
		$httpProvider.interceptors.push(errorInterceptor);
		
		////////////
		
		function errorInterceptor($injector, $q, $location) {
			var interceptor = {
					responseError: responseError
			};
			
			return interceptor;
			
			////////////
						
			function responseError(response) {
				switch(response.status) {
				case 401:
					console.log('HTTP 401 error occured and user will redirected to signin page in error.config.js#responseError');
					$location.path('/signin');
					break;
				case 403:
					console.log('HTTP 403 error occured and user will redirected to signin page in error.config.js#responseError');
					$location.path('/signin');
					break;
				case 400:
					console.log('HTTP 400 error occured in error.config.js#responseError');
					break;
				default:
					console.log('a general error occured in error.config.js#responseError: ' + response.message);
					break;
				}
				return $q.reject(response);
			}
		}
	}
})();