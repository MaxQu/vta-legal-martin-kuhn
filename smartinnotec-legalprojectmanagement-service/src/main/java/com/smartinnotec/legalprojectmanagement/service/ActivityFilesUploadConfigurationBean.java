package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(locations = "classpath:application.properties", ignoreUnknownFields = true, prefix = "legalprojectmanagement.activityFiles")
public @Data class ActivityFilesUploadConfigurationBean {

    private String activityFilesUploadPath;
    private String activityFilesFolderPrefixName;

}
