(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.administration')
    	.factory('contactsImportedYesNoModalService', contactsImportedYesNoModalService);
        
    contactsImportedYesNoModalService.$inject = ['$modal', '$stateParams'];
    
    function contactsImportedYesNoModalService($modal, $stateParams) {
		var service = {
			showContactsImportedYesNoModal: showContactsImportedYesNoModal
		};		
		return service;
		
		////////////
				
		function showContactsImportedYesNoModal(invoker) {
			 var contactsImportedYesNoModal = $modal.open({
				controller: ContactsImportedYesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/administration/modals/contactsImportedYesNoModal/contactsImportedYesNo.modal.html'
			});
			return contactsImportedYesNoModal;
			
			function ContactsImportedYesNoModalController($modalInstance, $scope, invoker) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	
		    	vm.contactsImportedDeleteAll = contactsImportedDeleteAll;
		    	vm.closeContactsImportedYesNoModal = closeContactsImportedYesNoModal;
		    	
		    	////////////
		    	
		    	function contactsImportedDeleteAll() {
		    		vm.invoker.deleteAllContactsImported();
		    		vm.closeContactsImportedYesNoModal();
		    	}
		    	
		    	function closeContactsImportedYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();