package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventSerialDateTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.container.CalendarEventPreparedContainer;

public interface CalendarEventService {

	Integer count() throws BusinessException;
	
    List<CalendarEvent> findByProject(final Project project) throws BusinessException;

    CalendarEvent findById(final String id) throws BusinessException;

    List<CalendarEvent> findAll() throws BusinessException;

    List<CalendarEvent> findBySerialDateNumber(final Long serialDateNumber) throws BusinessException;

    List<CalendarEvent> findCalendarEventsBySearchString(final String searchString) throws BusinessException;

    CalendarEvent findByLocationAndProject(final String location, final Project project) throws BusinessException;

    List<CalendarEvent> findAllCalendarEventsByTenant(final Tenant tenant) throws BusinessException;

    CalendarEvent findByProjectAndLocation(final Project project, final String location) throws BusinessException;

    List<CalendarEvent> getCalendarEventsInRange(final List<CalendarEvent> calendarEvents, final Long start, final Long end)
            throws BusinessException;

    List<CalendarEventPreparedContainer> prepareCalendarEvents(final List<CalendarEvent> calendarEvents,
            final boolean addCalendarEventUserConnections) throws BusinessException;

    List<CalendarEventPreparedContainer> getPreparedCalendarEvents(final Tenant tenant, final CalendarEventViewTypeEnum calendarViewType,
            final Long start, final Long end) throws BusinessException;

    List<CalendarEvent> getCalendarEvents(final Tenant tenant, final CalendarEventViewTypeEnum calendarViewType, final Long start,
            final Long end, final boolean archived) throws BusinessException;

    List<CalendarEventPreparedContainer> getCalendarEvents(final Tenant tenant, final Long start, final Long end, final boolean archived,
            final boolean addCalendarEventUserConnections) throws BusinessException;

    CalendarEventPreparedContainer prepareCalendarEvent(final CalendarEvent calendarEvent, final boolean addCalendarEventUserConnections)
            throws BusinessException;

    List<CalendarEvent> create(final CalendarEvent calendarEvent, final User user, final Boolean serialDate, final DateTime serialEndDate,
            final CalendarEventSerialDateTypeEnum calendarEventSerialDateType) throws BusinessException;

    void createCalendarEventsForProject(final Project project, final User user) throws BusinessException;

    List<CalendarEvent> update(final CalendarEvent calendarEvent, final User user) throws BusinessException;

    void deactivate(final User user, final CalendarEvent calendarEvent, final Boolean deleteSerialDates, final Boolean deleteForRecreation,
            final HistoryTypeEnum activationHistoryType, final String information) throws BusinessException;

    void delete(final User user, final CalendarEvent calendarEvent, final Boolean deleteSerialDates, final Boolean deleteForRecreation)
            throws BusinessException;

    void deleteAll() throws BusinessException;

    DateTime calculateNextSerialDateTime(final CalendarEventSerialDateTypeEnum calendarEventSerialDateType, DateTime date)
            throws BusinessException;

    List<CalendarEvent> findByCalendarEventTypAndConfirmed(final CalendarEventTypeEnum calendarEventType, final boolean confirmed)
            throws BusinessException;
}
