package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class ProjectDataSheetIdentificationFacility {

    private String identificationFacilityName;
    private String identificationFacilityPower;
    private String identificationFacilityType;

}
