package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "ProjectDataSheet")
public @Data class ProjectDataSheet {

    @Id
    private String id;

    private String projectId;

    private String operatorName;
    private String operatorStreet;
    private String operatorNumber;
    private Integer operatorZip;
    private String operatorLocation;
    private String operatorProvinceType;

    private String locationStreet;
    private String locationNumber;
    private Integer locationZip;
    private String locationLocation;
    private String locationProvinceType;
    private String locationRegion;
    private String locationEstateNumber;
    private String locationArchiveNumber;
    private String locationCastralCommunity;

    private List<ProjectDataSheetIdentificationFacility> projectDataSheetIdentificationFacilities;

    private List<ProjectDataSheetApprovalOrder> projectDataSheetApprovalOrders;

    private List<ProjectDataSheetAuthority> projectDataSheetAuthorities;

    private List<ProjectDataSheetChange> projectDataSheetChanges;
}
