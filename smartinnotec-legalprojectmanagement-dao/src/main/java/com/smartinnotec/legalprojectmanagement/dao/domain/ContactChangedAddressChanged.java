package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.Data;

public @Data class ContactChangedAddressChanged {

    @DBRef
    private AddressChanged address;

    @Override
    public boolean equals(final Object obj) {
        final ContactChangedAddressChanged contactChangedAddressChanged = (ContactChangedAddressChanged)obj;
        if (this.address == null || contactChangedAddressChanged.getAddress() == null) {
            return false;
        } else if (this.address.getId().equals(contactChangedAddressChanged.getAddress().getId())) {
            return true;
        }
        return false;
    }

    public ContactChangedAddressChanged deepCopy() {
        final ContactChangedAddressChanged contactChangedAddressChanged = new ContactChangedAddressChanged();
        final AddressChanged copiedAddressChanged = this.address.deepCopy();
        contactChangedAddressChanged.setAddress(copiedAddressChanged);
        return contactChangedAddressChanged;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        return result;
    }
}
