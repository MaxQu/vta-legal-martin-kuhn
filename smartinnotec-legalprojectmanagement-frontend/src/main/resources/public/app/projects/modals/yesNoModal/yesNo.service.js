(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('yesNoDeleteProjectModalService', yesNoDeleteProjectModalService);
        
    yesNoDeleteProjectModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoDeleteProjectModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(project, invoker) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	project: function() {
			    		return project;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, project, invoker) {
		    	var vm = this; 
		    	vm.project = project;
		    	vm.invoker = invoker;
		    	
		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	
		    	function yes() {
		    		invoker.deleteProject(vm.project);
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();