(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products') 
    	.directive('createThinningDirective', createThinningDirective);
    
    createThinningDirective.$inject = ['$timeout', 'dateUtilityService', 'thinningService', 'productsService', 'printingPDFService', 'printThinningModalService'];
    
	function createThinningDirective($timeout, dateUtilityService, thinningService, productsService, printingPDFService, printThinningModalService) {
		var directive = {
			restrict: 'E',
			scope: {
				index: '=',
				thinning: '=',
				product: '=',
				products: '=',
				invoker: '=',
				type: '=',
				currentUser: '=',
				userAuthorizationUserContainer: '=',
				polymerPromille: '='
			},
			templateUrl: 'app/products/directives/createThinning/createThinning.html',
			link: function($scope) { 
				
				$scope.createThinningDateDatePicker = false;
				$scope.thinningDateTime = $scope.thinning.date instanceof Date ? $scope.thinning.date : dateUtilityService.convertToDateTimeOrUndefined($scope.thinning.date);
				
				$scope.setTimeout = function() {
		      		$timeout(function() {
		      			$scope.thinning.updateSuccess = false;
		      			$scope.thinning.densityError = false;
		      			$scope.thinning.densityOfProductError = false;
		  			   }, 2000); 
		      	};
		      	
		      	$scope.openCreateThinningDatePicker = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					$scope.createThinningDateDatePicker = true; 
				};
		      	
				$scope.createThinning = function() {  
					$scope.thinning.productId = $scope.product.id;
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		$scope.thinning.date = dateUtilityService.joinDateObjectsToDateTimeObject($scope.thinning.date, $scope.thinningDateTime);
		    		thinningService.createThinning($scope.thinning).then(function(response) {
		    			productsService.findProduct($scope.product.id).then(function(response) {
		    				$scope.product = response.data;
		    				for(var i = 0; i < $scope.products.length; i++) {
		    					if($scope.products[i].id == $scope.product.id) {
		    						var selected = $scope.products[i].selected;
		    						$scope.products[i] = $scope.product;
		    						$scope.products[i].selected = selected;
		    						break;
		    					}
		    				}
		    				$scope.resetThinning();
		    			}, function errorCallback(response) {
			 	   			 console.log('error createThinning.directive#createThinning');
			 	   		});
		    			$scope.invoker.closeCreateThinningModal();
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error createThinning.directive#createThinning');
		 	   		 });
		    	};
		    	
		    	$scope.resetThinning = function() {
		    		$scope.thinning.productId = '';
		    		$scope.thinning.date = new Date();
		    		$scope.thinning.ironContent = null;
		    		$scope.thinning.targetContent = null;
		    		$scope.thinning.density = null;
		    		$scope.thinning.amount = null;
		    		$scope.thinning.amountLiter = null;
		    		$scope.thinning.iron = null;
		    		$scope.thinning.water = null;
		    		$scope.thinning.polymer = false;
		    		$scope.thinning.ascorbinSaeure = false;
		    		$scope.thinning.polymerSubtract = 1000;
		    		$scope.thinning.ascorbinSaeureSubtract = 2700;
		    		$scope.thinning.waterAmountCalculated = null;
		    		$scope.thinning.informationTop = null;
		    		$scope.thinning.information = null;
		    		$scope.thinning.labelLeft1 = 'Fe- Gehalt';
		    		$scope.thinning.labelLeft2 = 'Fe- Gehalt';
		    		$scope.thinning.labelRight1 = 'Eisenlösung';
		    	};
		    	
		    	$scope.updateThinning = function() {
		    		Date.prototype.toJSON = function () { return this.toLocaleString(); };
		    		var date = dateUtilityService.convertToDateOrUndefined($scope.thinning.date);
		    		$scope.thinning.date = dateUtilityService.joinDateObjectsToDateTimeObject(date, $scope.thinningDateTime);
		    		thinningService.updateThinning($scope.thinning).then(function(response) {
		    			$scope.thinning.updateSuccess = true;
		    			$scope.setTimeout();
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error createThinning.directive#editThinning');
		 	   		 });
		    	};		    	
		    	
		    	$scope.deleteThinning = function() {
		    		thinningService.deleteThinning($scope.thinning.id).then(function(response) {
		    			$scope.invoker.deleteThinning($scope.thinning);
		 	   		 }, function errorCallback(response) {
		 	   			 console.log('error createThinning.directive#editThinning');
		 	   		 });
		    	};
		    	
		    	$scope.$watch('thinning.targetContent', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.calculateAmount();
		    		}
				});
		    	
		    	$scope.$watch('thinning.ironContent', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.calculateAmount();
		    		}
				});
		    	
		    	$scope.$watch('thinning.density', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.calculateAmount();
		    		}
				});
		    	
		    	$scope.$watch('thinning.amount', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.calculateAmount();
		    		}
				});
		    	
		    	$scope.$watch('thinning.polymer', function(newValue, oldValue) {
		    		if(oldValue != newValue) { 
		    			$scope.addRemoveLiterOfPolymer(newValue, oldValue);   
		    		}
				});
		    	
		    	$scope.$watch('thinning.ascorbinSaeure', function(newValue, oldValue) {
		    		if(oldValue != newValue) {
		    			$scope.addRemoveLiterOfAscorbinsaeure(newValue, oldValue);
		    		}
				});
		    	
		    	$scope.calculateAmount = function() {
		    		if($scope.product.density == null) {
		    			$scope.thinning.densityOfProductError = true;
		    			$scope.setTimeout();
		    			return;
		    		}
		    		$scope.targetMass = $scope.thinning.amountLiter * $scope.product.density; // (Masse (Ziel))
		    		$scope.ironInKg =  $scope.targetMass * ($scope.thinning.targetContent / $scope.thinning.ironContent);
		    		$scope.thinning.iron = $scope.ironInKg / $scope.thinning.density;
		    		$scope.thinning.water = $scope.thinning.amountLiter - $scope.thinning.iron;
		    		$scope.thinning.waterAmountCalculated = $scope.thinning.water;
		    		
		    		if($scope.thinning.polymer == true) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated - $scope.thinning.polymerSubtract;
		    		}
		    		if($scope.thinning.ascorbinSaeure == true) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated - $scope.thinning.ascorbinSaeureSubtract;
		    		}
		    	};
		    	
		    	$scope.addRemoveLiterOfPolymer = function(newValue, oldValue) {
		    		if(newValue == true && oldValue == false) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated - $scope.thinning.polymerSubtract;
		    		} else if(newValue == false && oldValue == true) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated + $scope.thinning.polymerSubtract;
		    		}
		    	};
		    	
		    	$scope.addRemoveLiterOfAscorbinsaeure = function(newValue, oldValue) {
		    		if(newValue == true && oldValue == false) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated - $scope.thinning.ascorbinSaeureSubtract;
		    		} else if(newValue == false && oldValue == true) {
		    			$scope.thinning.waterAmountCalculated = $scope.thinning.waterAmountCalculated + $scope.thinning.ascorbinSaeureSubtract;
		    		}
		    	};
		    	
		    	$scope.amountInLiterChanged = function() {
		    		if($scope.product.density == null) {
		    			$scope.thinning.densityOfProductError = true;
		    			$scope.setTimeout();
		    			return;
		    		}
		    		$scope.thinning.amount = $scope.thinning.amountLiter * $scope.product.density;
		    	};
		    	
		    	$scope.printContainer = function(container, name) {
		    		printingPDFService.printSchedule(container, name);
		    	};
		    	
		    	$scope.showPrintThinningModal = function(thinningOfProduct) {
		    		printThinningModalService.showPrintThinningModal($scope.product, thinningOfProduct, $scope.currentUser, $scope.polymerPromille);
		    	};
		    	
		    	$scope.resetReceipe = function() { 
		    		$scope.thinning.ironContent = null;
		    		$scope.thinning.targetContent = null;
		    		$scope.thinning.density = null;
		    		$scope.thinning.amountLiter = null;
		    		$scope.thinning.amount = null;
		    		$scope.thinning.informationTop = null;
		    		$scope.thinning.information = null;
		    	};
		    	
		    	$scope.closeCreateThinningModal = function() {
		    		$scope.resetThinning();
					$scope.invoker.closeCreateThinningModal();
				};
			}
		};
		return directive;
		
		////////////
	}
})();
