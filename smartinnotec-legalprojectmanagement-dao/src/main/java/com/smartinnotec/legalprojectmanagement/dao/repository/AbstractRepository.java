package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractRepository {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public AbstractRepository() {
    }

    @Override
    public String toString() {
        return "[AbstractRepository]";
    }
}
