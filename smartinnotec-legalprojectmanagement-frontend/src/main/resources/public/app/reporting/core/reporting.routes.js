(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.reporting')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getReportingState());
    	 
    	////////////
			    	
    	function getReportingState() {
    		var state = {
    			name: 'auth.reporting',
				url: '/reporting/:userId',
				templateUrl: 'app/reporting/reporting/reporting.html',
				controller: 'ReportingController',
				controllerAs: 'vm',
				resolve: {
					userService: 'userService',
					userAuthorizationUserContainer: function($stateParams, userService) {
			    		return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
			    	}
				}
    		};
    		return state;
    	}
	}
})();