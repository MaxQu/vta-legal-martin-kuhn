package com.smartinnotec.legalprojectmanagement.service.test;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryAmount;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;


public class HistoryAmountServiceTest extends AbstractServiceTest {

    @Test
    public void shouldCreateHistoryAmounts() throws BusinessException {
    	final HistoryAmount historyAmount1 = new HistoryAmount();
    	historyAmount1.setDate(formatterDate.parseDateTime("07.08.2018"));
    	historyAmount1.setContacts(3);
    	historyAmount1.setActivities(4);
    	historyAmount1.setFacilityDetails(5);
    	historyAmount1.setDocuments(2);
    	historyAmount1.setProducts(7);
    	historyAmount1.setProjects(1);
    	historyAmount1.setCalendarEvents(5);
    	historyAmount1.setReceipes(4);
    	historyAmount1.setMixtures(5);
    	historyAmount1.setThinnings(8);
    	final HistoryAmount createdHistoryAmount1 = historyAmountService.create(historyAmount1);
    	Assert.assertNotNull(createdHistoryAmount1);
    	
    	final HistoryAmount historyAmount2 = new HistoryAmount();
    	historyAmount2.setDate(formatterDate.parseDateTime("09.08.2018"));
    	historyAmount2.setContacts(1);
    	historyAmount2.setActivities(2);
    	historyAmount2.setFacilityDetails(3);
    	historyAmount2.setDocuments(4);
    	historyAmount2.setProducts(5);
    	historyAmount2.setProjects(6);
    	historyAmount2.setCalendarEvents(7);
    	historyAmount2.setReceipes(8);
    	historyAmount2.setMixtures(9);
    	historyAmount2.setThinnings(10);
    	final HistoryAmount createdHistoryAmount2 = historyAmountService.create(historyAmount2);
    	Assert.assertNotNull(createdHistoryAmount2);

    	final HistoryAmount historyAmount3 = new HistoryAmount();
    	historyAmount3.setDate(formatterDate.parseDateTime("10.08.2018"));
    	historyAmount3.setContacts(9);
    	historyAmount3.setActivities(8);
    	historyAmount3.setFacilityDetails(7);
    	historyAmount3.setDocuments(6);
    	historyAmount3.setProducts(5);
    	historyAmount3.setProjects(4);
    	historyAmount3.setCalendarEvents(3);
    	historyAmount3.setReceipes(2);
    	historyAmount3.setMixtures(1);
    	historyAmount3.setThinnings(1);
    	final HistoryAmount createdHistoryAmount3 = historyAmountService.create(historyAmount3);
    	Assert.assertNotNull(createdHistoryAmount3);
    }
}
