package com.smartinnotec.legalprojectmanagement.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Receipe;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ProductRepository;

@Service("productService")
public class ProductServiceImpl extends AbstractService implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ReceipeService receipeService;
    @Autowired
    private ThinningService thinningService;
    @Autowired
    private UserService userService;
    @Autowired
    private SearchService searchService;

    @Override
    public Integer count() throws BusinessException {
        logger.info("count products in ProductServiceImpl#count()");
        final Long products = productRepository.count();
        return products.intValue();
    }

    @Override
    public Boolean checkUniqueProductNumber(final User user, final String productNumber, final String type, final String productIdToUpdate)
            throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ProductServiceImpl#checkUniqueProductNumber", "400");
        logger.info("check zunique productName in ProductServiceImpl#checkUniqueProductNumber");
        final List<Product> products = productRepository.getByNumberEquals(productNumber);
        if (type.equals("CREATE")) {
            return products == null || products.isEmpty();
        } else {
            for (final Product product : products) {
                if (!product.getId().equals(productIdToUpdate)) {
                    return false;
                }
            }
            return true;
        }
    }

    @Override
    public Boolean checkUniqueProductName(final User user, final String productName, final String type, final String productIdToUpdate)
            throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in ProductServiceImpl#checkUniqueProductName", "400");
        logger.info("check unique productName in ProductServiceImpl#checkUniqueProductName");
        final List<Product> products = productRepository.getByNameEquals(productName);
        if (type.equals("CREATE")) {
            return products == null || products.isEmpty();
        } else {
            for (final Product product : products) {
                if (!product.getId().equals(productIdToUpdate)) {
                    return false;
                }
            }
            return true;
        }
    }

    @Override
    public Product findProduct(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ProductServiceImpl#findProduct", "400");
        BusinessAssert.isId(id, "id must be an id in ProductServiceImpl#findProduct", "400");
        logger.info("find product with id '{}' in ProductServiceImpl#findProduct", id);
        final Product product = productRepository.findOne(id);
        if (product == null) {
            return null;
        }
        final Long amountOfReceipes = receipeService.countByProductIdAndReceipeType(product.getId(), ReceipeTypeEnum.STANDARD_MIXTURE);
        final Long amountOfMixtures = receipeService.countByProductIdAndReceipeType(product.getId(), ReceipeTypeEnum.CHANGED_MIXTURE);
        final Long amountOfThinnings = thinningService.countByProductId(product.getId());
        product.setAmountOfReceipes(amountOfReceipes);
        product.setAmountOfMixtures(amountOfMixtures);
        product.setAmountOfThinnings(amountOfThinnings);
        return product;
    }

    @Override
    public List<Product> findAllProductsByTenant(final Tenant tenant) throws BusinessException {
        logger.info("find all products by tenant with id '{}' in ProductServiceImpl#findAllProductsByTenant", tenant.getId());
        final List<Product> products = productRepository.findByTenant(tenant);
        for (final Product product : products) {
            final Long amountOfReceipes = receipeService.countByProductIdAndReceipeType(product.getId(), ReceipeTypeEnum.STANDARD_MIXTURE);
            final Long amountOfMixtures = receipeService.countByProductIdAndReceipeType(product.getId(), ReceipeTypeEnum.CHANGED_MIXTURE);
            final Long amountOfThinnings = thinningService.countByProductId(product.getId());
            product.setAmountOfReceipes(amountOfReceipes);
            product.setAmountOfMixtures(amountOfMixtures);
            product.setAmountOfThinnings(amountOfThinnings);
        }
        return products;
    }

    @Override
    public List<Product> findPagedProductsByTenant(final Tenant tenant, final Integer page, String sortingType) throws BusinessException {
        BusinessAssert.notNull(tenant, "tenant is mandatory in ProductServiceImpl#findPagedProductsByTenant", "400");
        logger.info(
            "find paged products of page '{}' and tenant with id '{}' and sortType '{}' in ProductServiceImpl#findPagedProductsByTenant",
            page, tenant.getId(), sortingType);

        Direction sortDirection = Sort.Direction.ASC;
        if (sortingType.startsWith("-")) {
            sortDirection = Sort.Direction.DESC;
            sortingType = sortingType.substring(1);
        }
        final Pageable pageable = new PageRequest(page, this.getProductPageSize(), new Sort(sortDirection, sortingType));
        final List<Product> pagedProducts = productRepository.findPagedProductsByTenant(tenant, pageable);
        for (final Product product : pagedProducts) {
            final Long amountOfReceipes = receipeService.countByProductIdAndReceipeType(product.getId(), ReceipeTypeEnum.STANDARD_MIXTURE);
            final Long amountOfMixtures = receipeService.countByProductIdAndReceipeType(product.getId(), ReceipeTypeEnum.CHANGED_MIXTURE);
            final Long amountOfThinnings = thinningService.countByProductId(product.getId());
            product.setAmountOfReceipes(amountOfReceipes);
            product.setAmountOfMixtures(amountOfMixtures);
            product.setAmountOfThinnings(amountOfThinnings);
        }
        return pagedProducts;
    }

    @Override
    public List<Product> findProductsFacade(String searchString) throws BusinessException {
        logger.info("find products by term parts of searchString '{}' in ProductServiceImpl#findProductByTermParts");
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        if (combineSearchStringWithAnd) {
            searchString = searchService.prepareSearchTerm(searchString);
            searchString = searchString.replaceAll("\\+", "");

            List<Product> products = new ArrayList<>();
            if (searchString.contains("*")) {
                searchString = searchString.replaceAll("\\*", "");
                products = productRepository.findProductByRegexString(searchString);
            } else {
                products = findProductByTerm(searchString);
            }

            for (final Iterator<Product> iterator = products.iterator(); iterator.hasNext();) {
                final Product product = iterator.next();
                String productProperties = product.getNumber() + product.getName() + product.getTypes() + product.getNames();
                productProperties = productProperties.toLowerCase();
                if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(productProperties::contains)) {
                    iterator.remove();
                }
            }
            return products;
        } else {
            return findProductByTerm(searchString);
        }
    }

    @Override
    public List<Product> findProductsByRegex(final String searchTerm) throws BusinessException {
        BusinessAssert.notNull(searchTerm, "searchTerm is mandatory in ProductServiceImpl#findProductsByRegex", "400");
        logger.info("find products by searchTerm '{}' in ProductServiceImpl#findProductsByRegex", searchTerm);
        final List<Product> products = productRepository.findProductByRegexString(searchTerm);
        return products;
    }

    @Override
    public List<Product> findProductByTerm(String searchTerm) throws BusinessException {
        BusinessAssert.notNull(searchTerm, "searchTerm is mandatory in ProductServiceImpl#findProductByTerm", "400");
        logger.info("find product by term '{}' in ProductServiceImpl#findProductByTerm", searchTerm);
        searchTerm = searchService.prepareSearchTerm(searchTerm);
        final List<Product> products = productRepository.findProductBySearchString(searchTerm);
        for (final Product product : products) {
            final Long amountOfReceipes = receipeService.countByProductIdAndReceipeType(product.getId(), ReceipeTypeEnum.STANDARD_MIXTURE);
            final Long amountOfMixtures = receipeService.countByProductIdAndReceipeType(product.getId(), ReceipeTypeEnum.CHANGED_MIXTURE);
            final Long amountOfThinnings = thinningService.countByProductId(product.getId());
            product.setAmountOfReceipes(amountOfReceipes);
            product.setAmountOfMixtures(amountOfMixtures);
            product.setAmountOfThinnings(amountOfThinnings);
        }
        return products;
    }

    @Override
    public List<Product> findProductsOfReceipes(final List<Receipe> receipes) throws BusinessException {
        logger.info("find products of receipes in ProductServiceImpl#findProductsOfReceipes");
        final List<Product> products = new ArrayList<>();
        final List<String> productIds = new ArrayList<>();
        for (final Receipe receipe : receipes) {
            final String productId = receipe.getProductId();
            final Product product = productRepository.findOne(productId);
            if (!productIds.contains(product.getId())) {
                final Long amountOfReceipes = receipeService.countByProductIdAndReceipeType(product.getId(),
                    ReceipeTypeEnum.STANDARD_MIXTURE);
                final Long amountOfMixtures = receipeService.countByProductIdAndReceipeType(product.getId(),
                    ReceipeTypeEnum.CHANGED_MIXTURE);
                final Long amountOfThinnings = thinningService.countByProductId(product.getId());
                product.setAmountOfReceipes(amountOfReceipes);
                product.setAmountOfMixtures(amountOfMixtures);
                product.setAmountOfThinnings(amountOfThinnings);
                products.add(product);
                productIds.add(product.getId());
            }
        }
        return products;
    }

    @Override
    public Integer getAmountOfProducts(final Tenant tenant) throws BusinessException {
        if (tenant == null) {
            return 0;
        }
        logger.info("get amount of products of tenant with id '{}' in ProductServiceImpl#getAmountOfProducts", tenant.getId());
        final Long amountOfProducts = productRepository.countByTenant(tenant);
        return amountOfProducts.intValue();
    }

    @Override
    public boolean checkIfProductExists(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ProductServiceImpl#countById", "400");
        BusinessAssert.isId(id, "id must be and id in ProductServiceImpl#countById", "400");
        logger.info("count product by id '{}' in ProductServiceImpl#countById", id);
        final Long amountOfProducts = productRepository.countById(id);
        return amountOfProducts > 0;
    }

    @Override
    public Product create(final Product product) throws BusinessException {
        BusinessAssert.notNull(product, "product is mandatory in ProductServiceImpl#create", "400");
        logger.info("create product with name '{}' in  ProductServiceImpl#create", product.getName());
        final Product createdProduct = productRepository.insert(product);
        logger.debug("created product with id '{}' in  ProductServiceImpl#create", createdProduct.getId());
        return createdProduct;
    }

    @Override
    public Product update(final Product product) throws BusinessException {
        BusinessAssert.notNull(product, "product is mandatory in ProductServiceImpl#update", "400");
        logger.info("save product with id '{}' in ProductServiceImpl#update", product.getId());
        return productRepository.save(product);
    }

    @Override
    public void deleteProduct(final String productId, final String userId, final String username) throws BusinessException {
        BusinessAssert.notNull(productId, "productId is mandatory in ProductServiceImpl#deleteProduct", "400");
        BusinessAssert.isId(productId, "productId must be an id in ProductServiceImpl#deleteProduct", "400");
        logger.info("delete product with id '{}' in ProductServiceImpl#deleteProduct", productId);
        // delete product
        productRepository.delete(productId);
        // remove productId from user if exists and update user
        final User user = userService.findUser(userId);
        user.removeProductId(productId);
        final User updatedUser = userService.updateUser(user);
        logger.debug("deleted product with id '{}' and updated user with id '{}' in ProductServiceImpl#deleteProduct", productId,
            updatedUser.getId());
    }

    @Override
    public void deleteAll() throws BusinessException {
        logger.info("delete all products in ProductServiceImpl#deleteAll");
        productRepository.deleteAll();
    }

    @Override
    public boolean productContainedInProducts(final Product product, final List<Product> products) throws BusinessException {
        for (final Product productOfProducts : products) {
            if (productOfProducts.getId().equals(product.getId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean renameFolder(final String productId, final String username) throws BusinessException {
        // add deleted, date and username to folder deleted in database
        final Product product = this.findProduct(productId);
        final String productName = product.getName();

        final DateTime deletionDate = new DateTime(System.currentTimeMillis());
        final String deletePostFix = "#Del#" + username + "#" + dateTimeFormatter2.print(deletionDate);
        final File currentFolder = new File(uploadPath + File.separator + productName);
        final File deletedFolder = new File(uploadPath + File.separator + productName + deletePostFix);
        if (currentFolder.isDirectory()) {
            currentFolder.renameTo(deletedFolder);
            logger.info("folder '{}' renamed to '{}' in ProductServiceImpl#renameFolder", currentFolder.getAbsolutePath(),
                deletedFolder.getAbsolutePath());
        } else {
            logger.error("path '{}' is not a folder in ProductServiceImpl#renameFolder", currentFolder.getAbsolutePath());
        }
        return true;
    }

    @Override
    public String toString() {
        return "[ProductServiceImpl]";
    }
}
