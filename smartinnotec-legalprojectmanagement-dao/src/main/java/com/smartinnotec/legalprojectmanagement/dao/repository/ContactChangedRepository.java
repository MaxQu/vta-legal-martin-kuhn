package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.ContactChanged;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface ContactChangedRepository extends MongoRepository<ContactChanged, String> {

    Integer countByTenantAndCommitted(final Tenant tenant, final boolean committed);

    List<ContactChanged> findContactsChangedByCommitted(final boolean committed);

    ContactChanged findContactChangedByContactId(final String contactId);

    List<ContactChanged> findPagedContactsChangedByCommitted(final boolean committed,
            final Pageable pageable);

}
