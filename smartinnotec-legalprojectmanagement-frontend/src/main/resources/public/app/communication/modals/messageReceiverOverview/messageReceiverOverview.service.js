(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('messageReceiverOverviewModalService', messageReceiverOverviewModalService);
        
    messageReceiverOverviewModalService.$inject = ['$modal', '$stateParams'];
    
    function messageReceiverOverviewModalService($modal, $stateParams) {
		var service = {
			showMessageReceiverOverviewModal: showMessageReceiverOverviewModal
		};		
		return service;
		
		////////////
				
		function showMessageReceiverOverviewModal(currentUser, communicationUserConnections, confirmationNeeded) {
			 var messageReceiverOverviewModal = $modal.open({
				controller: MessageReceiverOverviewModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	communicationUserConnections: function() {
			    		return communicationUserConnections;
			    	},
			    	confirmationNeeded: function() {
			    		return confirmationNeeded;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/messageReceiverOverview/messageReceiverOverview.modal.html'
			});
			return messageReceiverOverviewModal;
			
			function MessageReceiverOverviewModalController($modalInstance, $scope, currentUser, communicationUserConnections, confirmationNeeded) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.communicationUserConnections = communicationUserConnections;
		    	vm.confirmationNeeded = confirmationNeeded;
		    	
		    	vm.closeMessageReceiverOverviewModal = closeMessageReceiverOverviewModal;
		    	
		    	function closeMessageReceiverOverviewModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
