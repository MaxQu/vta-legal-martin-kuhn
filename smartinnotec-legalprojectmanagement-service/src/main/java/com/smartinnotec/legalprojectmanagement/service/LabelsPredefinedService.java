package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;
import java.util.Set;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.LabelsPredefined;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface LabelsPredefinedService {

    List<LabelsPredefined> getAllLabelsPredefinedByTenant(final Tenant tenant) throws BusinessException;

    LabelsPredefined findLabelsPredefinedById(final String id) throws BusinessException;

    Set<LabelsPredefined> findLabelsBySearchString(final String searchString) throws BusinessException;

    LabelsPredefined create(final LabelsPredefined labelPredefined) throws BusinessException;

    void delete(final String id) throws BusinessException;

}
