package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum UserAuthorizationTypeEnum {

    PROJECT("PROJECT"),
    PRODUCT("PRODUCT"),
    RECEIPE("RECEIPE"),
    MIXTURE("MIXTURE"),
    THINNING("THINNING"),
    CALENDAR("CALENDAR"),
    CONTACT("CONTACT"),
    WORKING_BOOK("WORKING_BOOK"),
    COMMUNICATION("COMMUNICATION"),
    HISTORY("HISTORY"),
    ADMINISTRATION("ADMINISTRATION"),
    TIME_REGISTRATION("TIMEREGISTRATION"),
    ACCESS_CONTROL("ACCESS_CONTROL"),
    ACTIVITY("ACTIVITY"),
    FACILITY_DETAILS("FACILITY_DETAILS"),
    REPORTING("REPORTING");

    private String value;

    private UserAuthorizationTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
