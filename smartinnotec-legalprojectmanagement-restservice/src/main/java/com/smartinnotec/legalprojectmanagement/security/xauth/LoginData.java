package com.smartinnotec.legalprojectmanagement.security.xauth;

import java.io.Serializable;

import lombok.Data;

public @Data class LoginData implements Serializable {

    private static final long serialVersionUID = -2102055830632784089L;
    private String username;
    private String password;
}
