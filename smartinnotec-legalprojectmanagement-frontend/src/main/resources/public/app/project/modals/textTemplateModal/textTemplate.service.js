(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('textTemplateModalService', textTemplateModalService);
        
    textTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function textTemplateModalService($modal, $stateParams) {
		var service = {
			showTextTemplateModal: showTextTemplateModal
		};		
		return service;
		
		////////////
				
		function showTextTemplateModal(url, documentName, ending, version) {
			 var textTemplateModal = $modal.open({
				controller: TextTemplateModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	url: function() {
			    		return url;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/textTemplateModal/textTemplate.modal.html'
			});
			return textTemplateModal;
			
			function TextTemplateModalController($modalInstance, $scope, url, documentName, ending, version) {
		    	var vm = this; 
		    	vm.url = prepareTextUrl(url, ending);
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;	
		    	
		    	vm.prepareTextUrl = prepareTextUrl;
		    	vm.closeTextTemplateModal = closeTextTemplateModal;
		    	
		    	////////////
		    	
		    	function prepareTextUrl(url, ending) {
		    		return url + 'previewtext';
		    	}
		    	
		    	function closeTextTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();