(function() {
    'use strict'; 
    
    angular 
    	.module('legalprojectmanagement.products')
    	.factory('productsService', productsService);
        
    productsService.$inject = ['$http', 'api_config'];
    
    function productsService($http, api_config) {
		var service = {
			createProduct: createProduct,
			getAllProductsOfTenant: getAllProductsOfTenant,
			findProduct: findProduct,
			findProductsByTerm: findProductsByTerm,
			findProductsByDocumentFileSearchTerm: findProductsByDocumentFileSearchTerm,
			getProductsOfUser: getProductsOfUser,
			getProductTypeOverview: getProductTypeOverview,
			getPagedProductsOfTenant: getPagedProductsOfTenant,
			getProductsReferencedToContact: getProductsReferencedToContact,
			getAmountOfProducts: getAmountOfProducts,
			getProductsWithSameReceipe: getProductsWithSameReceipe,
			updateProduct: updateProduct, 
			deleteProduct: deleteProduct
		};
		
		return service;
		
		////////////
		
		function createProduct(product) {
			return $http.post(api_config.BASE_URL + '/products/product', product);
		}
		
		function getAllProductsOfTenant() {
			return $http.get(api_config.BASE_URL + '/products/product/all');
		}
		
		function findProduct(productId) {
			return $http.get(api_config.BASE_URL + '/products/product/' + productId);
		}
		
		function findProductsByTerm(searchString) {
			return $http.get(api_config.BASE_URL + '/products/product/' + searchString + '/search');
		}
		
		function findProductsByDocumentFileSearchTerm(searchString) {
			return $http.get(api_config.BASE_URL + '/products/product/documentfile/' + searchString + '/search');
		}
		
		function getProductsOfUser(userId) {
			return $http.get(api_config.BASE_URL + '/products/product/user/' + userId);
		}
		
		function getProductTypeOverview() {
			return $http.get(api_config.BASE_URL + '/products/product/typeoverview');
		}
		
		function getProductsReferencedToContact(contactId) {
			return $http.get(api_config.BASE_URL + '/products/product/' + contactId + '/products');
		}
		
		function getPagedProductsOfTenant(page, sortingType) {
			return $http.get(api_config.BASE_URL + '/products/product/page/' + page, {
				params: {
					sortingtype: sortingType
				} 
			});
		}
		
		function getAmountOfProducts() {
			return $http.get(api_config.BASE_URL + '/products/product/amount');
		}
		
		function getProductsWithSameReceipe(receipe) {
			return $http.put(api_config.BASE_URL + '/products/product/samereceipe', receipe);
		}
		
		function updateProduct(product) {
			return $http.put(api_config.BASE_URL + '/products/product', product);
		}
		
		function deleteProduct(productId) {
			return $http.delete(api_config.BASE_URL + '/products/product/' + productId);
		}
    }
})();
