(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.communication')
    	.factory('communicationGroupDeletionModalService', communicationGroupDeletionModalService);
        
    communicationGroupDeletionModalService.$inject = ['$modal', '$stateParams', 'communicationService'];
    
    function communicationGroupDeletionModalService($modal, $stateParams, communicationService) {
		var service = {
			showCommunicationGroupDeletionModal: showCommunicationGroupDeletionModal
		};		
		return service;
		
		////////////
				
		function showCommunicationGroupDeletionModal(communicationGroup, communicationGroupUserConnections) {
			 var communicationGroupDeletionModal = $modal.open({
				controller: CommunicationGroupDeletionnModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	communicationGroup: function() {
			    		return communicationGroup;
			    	},
			    	communicationGroupUserConnections: function() {
			    		return communicationGroupUserConnections;
			    	}
			    }, 
				templateUrl: 'app/communication/modals/communicationGroupDeletion/communicationGroupDeletion.modal.html'
			});
			return communicationGroupDeletionModal;
			
			function CommunicationGroupDeletionnModalController($modalInstance, $scope, communicationGroup, communicationGroupUserConnections) {
		    	var vm = this; 
		    	vm.communicationGroup = communicationGroup;
		    	vm.communicationGroupUserConnections = communicationGroupUserConnections;
		    	
		    	vm.deleteCommunicationGroup = deleteCommunicationGroup;
		    	vm.closeCommunicationGroupDeletionModal = closeCommunicationGroupDeletionModal;
		    	
		    	////////////
		    	
		    	function deleteCommunicationGroup() {
	      			communicationService.deleteCommunicationGroup(communicationGroup.id).then(function (response) {
	            		for(var i = 0; i < vm.communicationGroupUserConnections.length; i++) {
	            			if(vm.communicationGroupUserConnections[i].communicationGroup.id === communicationGroup.id) {
	            				communicationGroupUserConnections.splice(i, 1);
	            				vm.closeCommunicationGroupDeletionModal();
	            				break;
	            			}
	            		}
	        		}).catch(function (data) {
	        			console.log('error communicationGroupDeletion.service#deleteCommunicationGroup: ' + data);
	        		});
		    	}
		    	
		    	function closeCommunicationGroupDeletionModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
