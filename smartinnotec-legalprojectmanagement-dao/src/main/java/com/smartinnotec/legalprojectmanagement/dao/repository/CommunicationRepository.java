package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.Communication;

public interface CommunicationRepository extends MongoRepository<Communication, String> {

    List<Communication> findByAttachedFileUrlsNotNull();

}
