package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "CalendarConfiguration")
public @Data class CalendarConfiguration {

    @Id
    private String id;
    private String key;
    private String value;
    private CalendarConfigurationTypeEnum calendarConfigurationType;
    private int orderNumber;
}
