(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('htmlTemplateModalService', htmlTemplateModalService);
        
    htmlTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function htmlTemplateModalService($modal, $stateParams) {
		var service = {
			showHtmlTemplateModal: showHtmlTemplateModal
		};		
		return service;
		
		////////////
				
		function showHtmlTemplateModal(url, documentName, ending, version) {
			 var htmlTemplateModal = $modal.open({
				controller: HtmlTemplateModalController,
			    controllerAs: 'vm',
			    size: 'md',
			    windowClass: "modal fade in",
			    resolve: {
			    	url: function() {
			    		return url;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/htmlTemplateModal/htmlTemplate.modal.html'
			});
			return htmlTemplateModal;
			
			function HtmlTemplateModalController($modalInstance, $scope, url, documentName, ending, version) {
		    	var vm = this; 
		    	vm.url = prepareHtmlUrl(url, ending);
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;	
		    	
		    	vm.prepareHtmlUrl = prepareHtmlUrl;
		    	vm.closeHtmlTemplateModal = closeHtmlTemplateModal;
		    	
		    	////////////
		    	
		    	function prepareHtmlUrl(url, ending) {
		    		return url + 'previewhtml';
		    	}
		    	
		    	function closeHtmlTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();