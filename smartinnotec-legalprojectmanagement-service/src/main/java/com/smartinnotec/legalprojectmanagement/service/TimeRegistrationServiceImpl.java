package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.TimeRegistration;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.TimeRegistrationRepository;

@Service("timeRegistrationService")
public class TimeRegistrationServiceImpl extends AbstractService implements TimeRegistrationService {

    @Autowired
    private TimeRegistrationRepository timeRegistrationRepository;
    @Autowired
    private SearchService searchService;

    @Override
    public TimeRegistration findTimeRegistrationById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in TimeRegistrationServiceImpl#findTimeRegistrationById", "400");
        BusinessAssert.isId(id, "id is mandatory in TimeRegistrationServiceImpl#findTimeRegistrationById", "400");
        logger.info("find timeRegistration by id '{}' in TimeRegistrationServiceImpl#findTimeRegistrationById", id);
        return timeRegistrationRepository.findOne(id);
    }

    @Override
    public List<TimeRegistration> findTimeRegistrationsBySearchStringAndProjectAndUser(String searchString, final Project project,
            final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in TimeRegistrationServiceImpl#findTimeRegistrationBySearchString",
            "400");
        logger.info("find timeRegistrations by searchString '{}' in TimeRegistrationServiceImpl#findTimeRegistrationBySearchString",
            searchString);

        List<TimeRegistration> timeRegistrations = new ArrayList<>();
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        if (combineSearchStringWithAnd) {
            searchString = searchString.replaceAll("\\+", "");
            timeRegistrations = timeRegistrationRepository.findBySearchString(searchString);
            for (final Iterator<TimeRegistration> iterator = timeRegistrations.iterator(); iterator.hasNext();) {
                final TimeRegistration timeRegistration = iterator.next();
                final String timeRegistrationProperties = timeRegistration.getText();
                if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(timeRegistrationProperties.toLowerCase()::contains)) {
                    iterator.remove();
                }
            }
        } else {
            timeRegistrations = timeRegistrationRepository.findBySearchString(searchString);
        }

        timeRegistrations.removeIf(s -> s.getProject() == null || !s.getProject().getId().equals(project.getId())
            || !s.getProject().getTenant().getId().equals(user.getTenant().getId()));
        return timeRegistrations;
    }

    @Override
    public List<TimeRegistration> findTimeRegistrationByProject(final Project project) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in TimeRegistrationServiceImpl#findTimeRegistrationByProject", "400");
        logger.info("find timeRegistration by project with id '{}' in TimeRegistrationServiceImpl#findTimeRegistrationByProject",
            project.getId());
        return timeRegistrationRepository.findByProject(project);
    }

    @Override
    public List<TimeRegistration> findTimeRegistrationsOfProjectBetweenTimeRange(final Project project, final DateTime start,
            final DateTime end) throws BusinessException {
        BusinessAssert.notNull(project, "project is mandatory in TimeRegistrationServiceImpl#findTimeRegistrationsOfProject", "400");
        BusinessAssert.notNull(start, "start is mandatory in TimeRegistrationServiceImpl#findTimeRegistrationsOfProject", "400");
        BusinessAssert.notNull(end, "end is mandatory in TimeRegistrationServiceImpl#findTimeRegistrationsOfProject", "400");
        logger.info("find timeRegistrations of project with id '{}' in TimeRegistrationServiceImpl#findTimeRegistrationsOfProject",
            project.getId());
        final List<TimeRegistration> timeRegistrationsStart = timeRegistrationRepository.findByProjectAndDateTimeFromBetween(project, start,
            end);
        final List<TimeRegistration> timeRegistrationsEnd = timeRegistrationRepository.findByProjectAndDateTimeFromBetween(project, start,
            end);

        for (final TimeRegistration timeRegistrationEnd : timeRegistrationsEnd) {
            boolean add = true;
            for (final TimeRegistration timeRegistrationStart : timeRegistrationsStart) {
                if (timeRegistrationEnd.getId().equals(timeRegistrationStart.getId())) {
                    add = false;
                    break;
                }
            }
            if (add) {
                timeRegistrationsStart.add(timeRegistrationEnd);
            }
        }

        for (final TimeRegistration timeRegistration : timeRegistrationsStart) {
            final Duration duration = new Duration(timeRegistration.getDateTimeFrom(), timeRegistration.getDateTimeUntil());
            timeRegistration.setTimeRange(calculateTimeRangeDifference(duration));
        }
        return timeRegistrationsStart;
    }

    @Override
    public String getTimeRegistrationsSumOfProjectBetweenTimeRange(final Project project, final DateTime start, final DateTime end)
            throws BusinessException {
        BusinessAssert.notNull(project,
            "project is mandatory in TimeRegistrationServiceImpl#getTimeRegistrationsSumOfProjectBetweenTimeRange", "400");
        BusinessAssert.notNull(start, "start is mandatory in TimeRegistrationServiceImpl#getTimeRegistrationsSumOfProjectBetweenTimeRange",
            "400");
        BusinessAssert.notNull(end, "end is mandatory in TimeRegistrationServiceImpl#getTimeRegistrationsSumOfProjectBetweenTimeRange",
            "400");
        logger.info(
            "find timeRegistrations of project with id '{}' in TimeRegistrationServiceImpl#getTimeRegistrationsSumOfProjectBetweenTimeRange",
            project.getId());
        final List<TimeRegistration> timeRegistrationsStart = timeRegistrationRepository.findByProjectAndDateTimeFromBetween(project, start,
            end);
        final List<TimeRegistration> timeRegistrationsEnd = timeRegistrationRepository.findByProjectAndDateTimeFromBetween(project, start,
            end);
        for (final TimeRegistration timeRegistrationEnd : timeRegistrationsEnd) {
            boolean add = true;
            for (final TimeRegistration timeRegistrationStart : timeRegistrationsStart) {
                if (timeRegistrationEnd.getId().equals(timeRegistrationStart.getId())) {
                    add = false;
                    break;
                }
            }
            if (add) {
                timeRegistrationsStart.add(timeRegistrationEnd);
            }
        }
        Duration sumOfDuration = new Duration(0L);
        for (final TimeRegistration timeRegistration : timeRegistrationsStart) {
            Duration duration = new Duration(timeRegistration.getDateTimeFrom(), timeRegistration.getDateTimeUntil());
            sumOfDuration = sumOfDuration.withDurationAdded(duration, 1);
        }
        final String timeRangeSum = calculateTimeRangeDifference(sumOfDuration);
        return timeRangeSum;
    }

    @Override
    public TimeRegistration create(final TimeRegistration timeRegistration) throws BusinessException {
        BusinessAssert.notNull(timeRegistration, "timeRegistration is mandatory in TimeRegistrationServiceImpl#create", "400");
        logger.info("create timeRegistration in TimeRegistrationServiceImpl#create");
        final TimeRegistration createdTimeRegistration = timeRegistrationRepository.insert(timeRegistration);
        final Duration duration = new Duration(createdTimeRegistration.getDateTimeFrom(), createdTimeRegistration.getDateTimeUntil());
        createdTimeRegistration.setTimeRange(calculateTimeRangeDifference(duration));
        return createdTimeRegistration;
    }

    @Override
    public TimeRegistration update(final TimeRegistration timeRegistration) throws BusinessException {
        BusinessAssert.notNull(timeRegistration, "timeRegistration is mandatory in TimeRegistrationServiceImpl#update", "400");
        logger.info("update timeRegistration with id {}' in TimeRegistrationServiceImpl#update", timeRegistration.getId());
        final TimeRegistration updatedTimeRegistration = timeRegistrationRepository.save(timeRegistration);
        final Duration duration = new Duration(updatedTimeRegistration.getDateTimeFrom(), updatedTimeRegistration.getDateTimeUntil());
        updatedTimeRegistration.setTimeRange(calculateTimeRangeDifference(duration));
        return updatedTimeRegistration;
    }

    @Override
    public void delete(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in TimeRegistrationServiceImpl#delete", "400");
        logger.info("delete timeRegistration with id '{}' in TimeRegistrationServiceImpl#delete", id);
        timeRegistrationRepository.delete(id);
    }

    private String calculateTimeRangeDifference(final Duration duration) {
        final String days = duration.getStandardDays() > 0 ? duration.getStandardDays() + "T " : "";
        final String hours = duration.getStandardHours() > 0 ? duration.getStandardHours() % 24 + "h " : "";
        final String minutes = duration.getStandardMinutes() > 0 ? duration.getStandardMinutes() % 60 + "m " : "";
        return days + hours + minutes;
    }
}
