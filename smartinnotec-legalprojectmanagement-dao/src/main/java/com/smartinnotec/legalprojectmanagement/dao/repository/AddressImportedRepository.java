package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;

public interface AddressImportedRepository extends MongoRepository<AddressImported, String> {

}
