package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum CalendarEventSerialDateTypeEnum {

    NO_SERIAL_DATE("NO_SERIAL_DATE"),
    EVERY_WEEK("EVERY_WEEK"),
    EVERY_SECOND_WEEK("EVERY_SECOND_WEEK"),
    EVERY_THIRD_WEEK("EVERY_THIRD_WEEK"),
    EVERY_FOURTH_WEEK("EVERY_FOURTH_WEEK"),
    EVERY_FIFTH_WEEK("EVERY_FIFTH_WEEK"),
    EVERY_SIXTH_WEEK("EVERY_SIXTH_WEEK"),
    EVERY_SEVENTH_WEEK("EVERY_SEVENTH_WEEK"),
    EVERY_EIGHT_WEEK("EVERY_EIGHT_WEEK"),
    EVERY_NINTH_WEEK("EVERY_NINTH_WEEK"),
    EVERY_TENTH_WEEK("EVERY_TENTH_WEEK"),
    EVERY_ELEVENTH_WEEK("EVERY_ELEVENTH_WEEK"),
    EVERY_TWELFETH_WEEK("EVERY_TWELFETH_WEEK");

    private String value;

    private CalendarEventSerialDateTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
