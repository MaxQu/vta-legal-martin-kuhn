(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('thinningPolymerDateTimeValueService', thinningPolymerDateTimeValueService);
        
    thinningPolymerDateTimeValueService.$inject = ['$http', 'api_config'];
    
    function thinningPolymerDateTimeValueService($http, api_config) {
		var service = {
			create: create,
			update: update,
			findAll: findAll,
			findThinningPolymerDateTimeValueOfDate: findThinningPolymerDateTimeValueOfDate,
			deleteThinningPolymerDateTimeValue: deleteThinningPolymerDateTimeValue
		};
		
		return service;
		
		////////////
		
		function create(thinningPolymerDateTimeValue) {
			return $http.post(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue', thinningPolymerDateTimeValue);
		}
		
		function update(thinningPolymerDateTimeValue) {
			return $http.put(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue', thinningPolymerDateTimeValue);
		}
		
		function findAll() {
			return $http.get(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue');
		}
		
		function findThinningPolymerDateTimeValueOfDate(date) {
			return $http.get(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue/' + date + '/');
		}
		
		function deleteThinningPolymerDateTimeValue(thinningPolymerDateTimeValueId) {
			return $http.delete(api_config.BASE_URL + '/thinningpolymerdatetimevalues/thinningpolymerdatetimevalue/' + thinningPolymerDateTimeValueId);
		}
    }
})();
