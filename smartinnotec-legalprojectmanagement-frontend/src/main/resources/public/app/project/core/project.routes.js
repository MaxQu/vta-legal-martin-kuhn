(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.project')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getProjectState());
    	 
    	////////////
			    	
    	function getProjectState() {
    		var state = {
    			name: 'auth.project',
				url: '/project/:userId/:type/:projectUserConnectionId/:projectOrProductId/:folderId',
				templateUrl: 'app/project/project/project.html',
				controller: 'ProjectController',
				controllerAs: 'vm',
				resolve: {
					projectUserConnectionService: 'projectUserConnectionService',
					projectsService: 'projectsService',
					productsService: 'productsService',
					userService: 'userService',
					documentFileService: 'documentFileService',
					labelsPredefinedService: 'labelsPredefinedService',
					folderService: 'folderService',	
					type: function($stateParams) {
						return $stateParams.type;
					},
					projectOrProduct: function findProjectById(projectsService, productsService, $stateParams) {
						if($stateParams.type == 'PROJECT') {
							return projectsService.findProject($stateParams.projectOrProductId);
						} else if($stateParams.type == 'PRODUCT') {
							var projectOrProduct = productsService.findProduct($stateParams.projectOrProductId);
							return projectOrProduct;
						}
					},
					amountOfDocumentFilesWithUniqueName: function countByProjectIdGroupByOriginalFileName(documentFileService, $stateParams) {
						return documentFileService.countByProjectIdGroupByOriginalFileName($stateParams.projectOrProductId, false, $stateParams.type);
					},
					amountOfPages: function calculateAmountOfPages(documentFileService, $stateParams) {
						return documentFileService.calculateAmountOfPages($stateParams.projectOrProductId, $stateParams.type, $stateParams.folderId);
					}, 
					documentFilePageSize: function getDocumentFilePageSize(documentFileService) {
						return documentFileService.getDocumentFilePageSize(); 
					},
					pagedDocumentFiles: function getPagedDocumentFiles(documentFileService, $stateParams) {
						return documentFileService.findPagedDocumentFilesByProjectIdAndFolderId($stateParams.projectOrProductId, $stateParams.folderId, 0, 'fileName', false, $stateParams.type);
					},
					folders: function findFolders(folderService, $stateParams) {
						return folderService.findFolders($stateParams.projectOrProductId, $stateParams.folderId);
					},
					folderId: function getFolderId($stateParams) {
						return $stateParams.folderId;
					},
					projectUserConnectionsOfProject: function findProjectUserConnectionByProject(projectUserConnectionService, $stateParams) {
						if($stateParams.type == 'PROJECT') {
							return projectUserConnectionService.findProjectUserConnectionByProject($stateParams.projectOrProductId);
						} else {
							return null;
						}
					},
					usersOfProject: function findProjectUserConnectionByProject(projectUserConnectionService, userService, $stateParams) {
						if($stateParams.type == 'PROJECT') {
							return projectUserConnectionService.findUsersOfProjectUserConnectionByProject($stateParams.projectOrProductId);
						} else if($stateParams.type == 'PRODUCT') {
							return userService.findAllUsers();
						}
					},
					allLabelsPredefined: function getAllLabelsPredefined($stateParams, labelsPredefinedService) {
						return labelsPredefinedService.getAllLabelsPredefined($stateParams.userId);
					},
					userAuthorizationUserContainer: function($stateParams, userService) {
						return userService.getUserAuthorizationUserContainerOfUser($stateParams.userId);
					},
					canSeeConfidentialDocumentFiles: function($stateParams, projectUserConnectionService, userService) {
						if($stateParams.type == 'PROJECT') {
							return projectUserConnectionService.canProjectUserConnectionRoleSeeConfidentialDocumentFiles($stateParams.projectUserConnectionId);
						} else if($stateParams.type == 'PRODUCT') {							
							return userService.canUserSeeConfidentialDocumentFiles($stateParams.userId);
						}
					},
					roles: function(optionsService) {
						return optionsService.getRoles();
					}
				}
    		};
    		return state;
    	}
	}
})();