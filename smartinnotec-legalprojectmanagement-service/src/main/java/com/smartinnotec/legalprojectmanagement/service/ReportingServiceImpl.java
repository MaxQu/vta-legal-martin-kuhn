package com.smartinnotec.legalprojectmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.FacilityDetails;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.container.ReportingContainer;

@Service("reportingService")
public class ReportingServiceImpl extends AbstractService implements ReportingService {

    @Autowired
    private UserService userService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private FacilityDetailsService facilityDetailsService;
    
    @Override
    public List<ReportingContainer> findByUserId(final String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in ReportingServiceImpl#findByUserId", "400");
        BusinessAssert.isId(userId, "userId must be an id in ReportingServiceImpl#findByUserId", "400");
        logger.info("find by user with id '{}' in ReportingServiceImpl#findByUserId", userId);
        final User user = userService.findUser(userId);
        return null;
    }
}
