package com.smartinnotec.legalprojectmanagement.service;

public enum CalendarEventViewTypeEnum {

    DAY("DAY"),
    WEEK("WEEK"),
    MONTH("MONTH"),
    YEAR("YEAR");

    private String value;

    private CalendarEventViewTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
