package com.smartinnotec.legalprojectmanagement.service.container;

import com.smartinnotec.legalprojectmanagement.dao.domain.User;

import lombok.Data;

public @Data class CalendarEventOutsideWorkContainer {

    private User user;
    private String message;
    private int daysBetween;

}
