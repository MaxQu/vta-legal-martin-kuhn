(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.directive('contactFacilityDetailsDirective', contactFacilityDetailsDirective);
    
    contactFacilityDetailsDirective.$inject = ['$timeout', 'dateUtilityService', 'yesNoFacilityDetailsContactModalService', 'contactFacilityDetailCreateEditModalService'];
    
	function contactFacilityDetailsDirective($timeout, dateUtilityService, yesNoFacilityDetailsContactModalService, contactFacilityDetailCreateEditModalService) {
		var directive = {
			restrict: 'E',    
			scope: {
				invoker: '=',
				calendarEvent: '=',
				facilityDetail: '=',
				activityTypes: '=',
				facilityDetailsTypes: '=',
				user: '=',
				contact: '=',
				userAuthorizationUserContainer: '=',
				type: '='
			},
			templateUrl: 'app/contacts/directives/contactFacilityDetailsDirective/contactFacilityDetails.html',
			link: function($scope) {
				
				$scope.formatStartEndDateTime = function(start, end) {
					var startDate = new Date(start);
		    		var formattedStartDate = dateUtilityService.formatDateToDateTimeString(startDate);
		    		var endDate = new Date(end);
		    		var formattedEndDate = dateUtilityService.formatDateToDateTimeString(endDate);
		    		return dateUtilityService.formatStartEndDateTime(formattedStartDate, formattedEndDate);
				};
				
				$scope.showRemoveFacilityDetail = function() {
					yesNoFacilityDetailsContactModalService.showYesNoModal($scope.invoker, $scope.contact, $scope.facilityDetail);
				};
				
				$scope.showContactFacilityDetailCreateEditModal = function(activity) {                          
					contactFacilityDetailCreateEditModalService.showContactFacilityDetailCreateEditModal($scope.contact, $scope.facilityDetail, $scope.activityTypes, $scope.facilityDetailsTypes);
				};
			 }
		};
		return directive;
		
		////////////
	}
})();