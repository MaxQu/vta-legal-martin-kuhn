package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum UserAuthorizationStateEnum {

    READ("READ"),
    CHANGE("CHANGE"),
    DELETE("DELETE");

    private String value;

    private UserAuthorizationStateEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
