(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.contacts')
    	.factory('facilityDetailsService', facilityDetailsService);
        
    facilityDetailsService.$inject = ['$http', 'api_config'];
    
    function facilityDetailsService($http, api_config) {
		var service = {
			getFacilityDetailsOfContactId: getFacilityDetailsOfContactId,
			getFacilityDetailsOfContactIdInRange: getFacilityDetailsOfContactIdInRange,
			update: update,
			deleteFacilityDetail: deleteFacilityDetail
		};
		
		return service;
		
		////////////
		
		function getFacilityDetailsOfContactId(contactId) {
			return $http.get(api_config.BASE_URL + '/facilitydetails/facilitydetail/' + contactId + '/contact');
		}
		
		function getFacilityDetailsOfContactIdInRange(contactId, start, end) {
			return $http.get(api_config.BASE_URL + '/facilitydetails/facilitydetail/' + contactId + '/contact/' + start + '/' + end + '/');
		}
		
		function update(facilityDetail) {
			return $http.put(api_config.BASE_URL + '/facilitydetails/facilitydetail', facilityDetail);
		}
		
		function deleteFacilityDetail(facilityDetail) {
			return $http.delete(api_config.BASE_URL + '/facilitydetails/facilitydetail/' + facilityDetail.id);
		}
    }
})();