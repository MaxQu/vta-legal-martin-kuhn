package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "Contact")
public @Data class Contact implements Comparable<Contact> {

    @Id
    private String id;
    private boolean newContact;
    @TextIndexed
    private String institution;
    private boolean institutionChanged;
    @TextIndexed
    private String additionalNameInformation;
    private boolean additionalNameInformationChanged;
    @TextIndexed
    private String contactPerson; // zuhanden von vom foxfakt
    private boolean contactPersonChanged;
    private String number; //
    private String shortCode;
    private boolean shortCodeChanged;
    @TextIndexed
    private List<CustomerNumberContainer> customerNumberContainers;
    private boolean customerNumberContainersChanged;
    @TextIndexed
    private List<DeliveryNumberContainer> deliveryNumberContainers;
    private boolean deliveryNumberContainersChanged;
    @TextIndexed
    private List<AgentNumberContainer> agentNumberContainers; // Vertreternummern
    private boolean agentNumberContainersChanged;
    @TextIndexed
    private List<String> emails;
    private boolean emailsChanged;
    @TextIndexed
    private List<String> telephones;
    private boolean telephonesChanged;
    @DBRef
    protected Address address;
    @TextIndexed
    protected String information;
    private boolean informationChanged;
    @DBRef
    protected Address accountAddress;
    @TextIndexed
    private List<ContactAddressWithProducts> addressesWithProducts; // weitere Standorte
    private boolean addressesWithProductsChanged;
    @TextIndexed
    private List<ContactPerson> contactPersons; // Kontaktpersonen
    @TextIndexed
    private List<ContactTypeEnum> contactTypes;
    @TextIndexed
    private String homepage;
    private String color;
    private List<String> productIds;
    @Transient
    private List<Product> products;
    @DBRef
    private Tenant tenant;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime creationDate;
    private boolean active;
    private List<ContactAttachment> contactAttachments;
    @TextScore
    private Float score;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime importationDate;
    private boolean imported;
    private boolean importConfirmed;
    private boolean laubfrosch;
    private boolean newsletter;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime customerSince;

    public Contact() {
        active = true;
        imported = false;
        importConfirmed = false;
        laubfrosch = false;
        newsletter = false;
    }

    public void addProduct(final Product product) {
        if (products == null) {
            products = new ArrayList<>();
        }
        products.add(product);
    }

    public void addProductId(final String productId) {
        if (productIds == null) {
            productIds = new ArrayList<>();
        }
        productIds.add(productId);
    }

    public void removeProductId(final String productId) {
        if (productIds == null) {
            return;
        }
        for (final Iterator<String> iterator = productIds.iterator(); iterator.hasNext();) {
            final String next = iterator.next();
            if (next.equals(productId)) {
                iterator.remove();
                break;
            }
        }
    }

    public void addAddressWithProducts(final ContactAddressWithProducts contactAddressWithProducts) {
        if (addressesWithProducts == null) {
            addressesWithProducts = new ArrayList<>();
        }
        addressesWithProducts.add(contactAddressWithProducts);
    }

    @Override
    public int compareTo(Contact contact) {
        if (contact == null || this.getInstitution() == null || contact.getInstitution() == null) {
            return 0;
        }
        return this.getInstitution().compareTo(contact.getInstitution());
    }

    @Override
    public boolean equals(final Object obj) {
        final Contact contact = (Contact)obj;
        if (this.getId().equals(contact.getId())) {
            return true;
        }
        return false;
    }

    public Contact deepCopy() {
        final Contact copiedContact = new Contact();
        copiedContact.setInstitution(this.getInstitution());
        copiedContact.setInstitutionChanged(this.isInstitutionChanged());
        copiedContact.setAdditionalNameInformation(this.getAdditionalNameInformation());
        copiedContact.setAdditionalNameInformationChanged(this.isAdditionalNameInformationChanged());
        copiedContact.setNumber(this.number);
        copiedContact.setContactPerson(this.contactPerson);
        copiedContact.setContactPersonChanged(this.contactPersonChanged);
        copiedContact.setShortCode(this.getShortCode());
        copiedContact.setShortCodeChanged(this.isShortCodeChanged());
        if (this.getCustomerNumberContainers() != null && !this.getCustomerNumberContainers().isEmpty()) {
            final List<CustomerNumberContainer> newCustomerNumberContainers = new ArrayList<>();
            for (final CustomerNumberContainer customerNumberContainer : this.getCustomerNumberContainers()) {
                final CustomerNumberContainer newCustomerNumberContainer = new CustomerNumberContainer();
                newCustomerNumberContainer.setContactTenant(customerNumberContainer.getContactTenant());
                newCustomerNumberContainer.setCustomerNumber(customerNumberContainer.getCustomerNumber());
                newCustomerNumberContainers.add(newCustomerNumberContainer);
            }
            copiedContact.setCustomerNumberContainers(newCustomerNumberContainers);
            copiedContact.setCustomerNumberContainersChanged(this.isCustomerNumberContainersChanged());
        }
        if (this.getDeliveryNumberContainers() != null && !this.getDeliveryNumberContainers().isEmpty()) {
            final List<DeliveryNumberContainer> newDeliveryNumberContainers = new ArrayList<>();
            for (final DeliveryNumberContainer deliveryNumberContainer : this.getDeliveryNumberContainers()) {
                final DeliveryNumberContainer newDeliveryNumberContainer = new DeliveryNumberContainer();
                newDeliveryNumberContainer.setContactTenant(deliveryNumberContainer.getContactTenant());
                newDeliveryNumberContainer.setDeliveryNumber(deliveryNumberContainer.getDeliveryNumber());
                newDeliveryNumberContainers.add(newDeliveryNumberContainer);
            }
            copiedContact.setDeliveryNumberContainers(newDeliveryNumberContainers);
            copiedContact.setDeliveryNumberContainersChanged(this.isDeliveryNumberContainersChanged());
        }
        if (this.getAgentNumberContainers() != null && !this.getAgentNumberContainers().isEmpty()) {
            final List<AgentNumberContainer> newAgentNumberContainers = new ArrayList<>();
            for (final AgentNumberContainer agentNumberContainer : this.getAgentNumberContainers()) {
                final AgentNumberContainer newAgentNumberContainer = new AgentNumberContainer();
                newAgentNumberContainer.setCustomerNumber(agentNumberContainer.getCustomerNumber());
                newAgentNumberContainers.add(newAgentNumberContainer);
            }
            copiedContact.setAgentNumberContainers(newAgentNumberContainers);
            copiedContact.setAgentNumberContainersChanged(this.isAgentNumberContainersChanged());
        }

        final Address address = this.getAddress();
        if (address != null) {
            final Address copiedAddress = address.deepCopy();
            copiedContact.setAddress(copiedAddress);
        }

        if (this.emails != null && !this.emails.isEmpty()) {
            final List<String> newEmails = new ArrayList<>();
            for (final String email : this.emails) {
                newEmails.add(email);
            }
            copiedContact.setEmails(newEmails);
            copiedContact.setEmailsChanged(this.isEmailsChanged());
        }
        if (this.telephones != null && !this.telephones.isEmpty()) {
            final List<String> newTelephones = new ArrayList<>();
            for (final String telephone : this.telephones) {
                newTelephones.add(telephone);
            }
            copiedContact.setTelephones(newTelephones);
            copiedContact.setTelephonesChanged(this.isTelephonesChanged());
        }
        copiedContact.setInformation(this.getInformation());
        copiedContact.setInformationChanged(this.isInformationChanged());

        if (this.getAccountAddress() != null) {
            final Address copiedAddress = this.getAccountAddress().deepCopy();
            copiedContact.setAccountAddress(copiedAddress);
        }

        if (this.getAddressesWithProducts() != null && !this.getAddressesWithProducts().isEmpty()) {
            final List<ContactAddressWithProducts> copiedAddressesWithProducts = new ArrayList<>();
            for (final ContactAddressWithProducts contactAddressWithProducts : this.getAddressesWithProducts()) {
                final ContactAddressWithProducts newContactAddressWithProducts = new ContactAddressWithProducts();
                final Address addressOfContactAddressWithProducts = contactAddressWithProducts.getAddress();
                final Address copiedAddress = addressOfContactAddressWithProducts.deepCopy();
                newContactAddressWithProducts.setAddress(copiedAddress);
                copiedAddressesWithProducts.add(newContactAddressWithProducts);
            }
            copiedContact.setAddressesWithProducts(copiedAddressesWithProducts);
            copiedContact.setAddressesWithProductsChanged(this.isAddressesWithProductsChanged());
        }

        copiedContact.setHomepage(this.getHomepage());
        copiedContact.setColor(this.getColor());
        copiedContact.setTenant(this.getTenant());
        copiedContact.setCreationDate(this.getCreationDate());
        copiedContact.setActive(this.isActive());
        copiedContact.setScore(this.getScore());
        copiedContact.setLaubfrosch(this.isLaubfrosch());
        copiedContact.setImportationDate(this.getImportationDate());
        copiedContact.setImported(this.isImported());
        copiedContact.setImportConfirmed(this.isImportConfirmed());
        copiedContact.setCustomerSince(this.getCustomerSince());

        return copiedContact;
    }

    public boolean containsContactType(final ContactTypeEnum contactType) {
        if (contactTypes == null || contactTypes.isEmpty() || contactType == null) {
            return false;
        }
        return contactTypes.contains(contactType);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
