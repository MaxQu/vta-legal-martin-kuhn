(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarEventInListModalService', calendarEventInListModalService);
        
    calendarEventInListModalService.$inject = ['$modal', '$stateParams', 'dateUtilityService', 'calendarService', 'contactAndUserService'];
    
    function calendarEventInListModalService($modal, $stateParams, dateUtilityService, calendarService, contactAndUserService) {
		var service = {
			showCalendarEventInListModal: showCalendarEventInListModal
		};		
		return service;
		
		////////////
				
		function showCalendarEventInListModal(start, end, calendarEventTypes) {  
			 var calendarEventInListModal = $modal.open({
				controller: CalendarEventInListModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	start: function() {
			    		return start;
			    	},
			    	end: function() {
			    		return end;
			    	}, 
			    	calendarEventTypes: function() {
			    		return calendarEventTypes;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/calendarEventInList/calendarEventInList.modal.html'
			});
			return calendarEventInListModal;
			
			function CalendarEventInListModalController($modalInstance, $scope, start, end, calendarEventTypes) {
		    	var vm = this;
		    	vm.filterStartDate = start;
		    	vm.filterEndDate = end;
		    	vm.calendarEventTypes = calendarEventTypes;
		    	vm.calendarEvents = null;
		    	vm.filterStartDatePicker = false;
		    	vm.filterEndDatePicker = false;
		    	vm.selectedUser = null;
		    	vm.selectedCalendarEventType = null;
		    	
		    	vm.searchUsersByTerm = searchUsersByTerm;
		    	vm.openFilterStartDatePicker = openFilterStartDatePicker;
		    	vm.openFilterEndDatePicker = openFilterEndDatePicker;
		    	vm.searchCalendarEvents = searchCalendarEvents;
		    	vm.resetSearchCriteria = resetSearchCriteria;
		    	vm.closeCalendarEventInListModal = closeCalendarEventInListModal;
		    	searchCalendarEvents();
		    	
		    	////////////
		    	
		    	function openFilterStartDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterStartDatePicker = true;
					vm.searchCriteriaChangedTrigger = true;
		        }
		    	
		    	function openFilterEndDatePicker($event) {
					$event.preventDefault();
					$event.stopPropagation();
					vm.filterEndDatePicker = true;
					vm.searchCriteriaChangedTrigger = true;
		        }
		    	
		    	function searchCalendarEvents() {
		    		var startString = dateUtilityService.formatDateToString(vm.filterStartDate);
		     		var endString = dateUtilityService.formatDateToString(vm.filterEndDate);
		     		
		     		var calendarEventSearchCriteriaContainer = {};
		     		calendarEventSearchCriteriaContainer.start = startString;
		     		calendarEventSearchCriteriaContainer.end = endString;
		     		calendarEventSearchCriteriaContainer.userId = vm.selectedUser != null ? vm.selectedUser.id : null;
		     		calendarEventSearchCriteriaContainer.calendarEventType = vm.selectedCalendarEventType != null ? vm.selectedCalendarEventType : null;
		     		
		    		calendarService.findCalendarEventOfCriteria(calendarEventSearchCriteriaContainer).then(function(response) {
		    			vm.calendarEvents = response.data;
			    	}).catch(function (data) {
						console.log('error calendarEventInList.service#searchCalendarEvents: ' + data);
			    	});
		    	}
		    	
		    	function searchUsersByTerm(searchString) {   
		    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function(response) {
		    			var usersAndContacts = response.data;
		    			var users = [];
		    			for(var i = 0; i < usersAndContacts.length; i++) {
		    				if(usersAndContacts[i].contact == null) {
		    					users.push(usersAndContacts[i]);
		    				}
		    			}
		    			return users;
		    		}).catch(function (data) {
						console.log('error calendarEventInList.service#contactSearch#searchUsersByTerm: ' + data);
					});
		    	}
		    	
		    	function resetSearchCriteria() {
		    		vm.filterStartDate = dateUtilityService.oneMonthBack(new Date());
		    		vm.filterEndDate = new Date();
		    		vm.selectedUser = null;
			    	vm.selectedCalendarEventType = null;
		    	}
		    	
		    	function closeCalendarEventInListModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();