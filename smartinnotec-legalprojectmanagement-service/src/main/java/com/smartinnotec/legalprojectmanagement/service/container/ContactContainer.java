package com.smartinnotec.legalprojectmanagement.service.container;

import lombok.Data;

public @Data class ContactContainer {

    private String tenant; // country, first row
    private String contactChangeType; // needed for export
    private String accountNumber; // ktonr
    private String shortCode; // kurzcode
    private String name1; // name1
    private String name2; // name2
    private String contactPerson; // Ansprechpartner
    private String street; // Strasse
    private String intSign; // intkz
    private String postalCode; // plz
    private String region; // ort
    private String telephone; // telefon
    private String mobile; // mobile
    private String email; // email
    private String customerSince; // kunde seit
    private String agent; // vertreter
}
