(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarEventScheduleService', calendarEventScheduleService);
        
    calendarEventScheduleService.$inject = ['$http', 'api_config'];
    
    function calendarEventScheduleService($http, api_config) {
		var service = {
			sendCalendarEventRememberingMessage: sendCalendarEventRememberingMessage,
			sendCalendarEventOutsideWorkRememberingMessage: sendCalendarEventOutsideWorkRememberingMessage
		};
		
		return service;
		
		////////////
		
		function sendCalendarEventRememberingMessage() {
			return $http.get(api_config.BASE_URL + '/calendareventschedules/calendareventschedule/calendareventrememberingmessage/send');
		}
		
		function sendCalendarEventOutsideWorkRememberingMessage() {
			return $http.get(api_config.BASE_URL + '/calendareventschedules/calendareventschedule/calendareventoutsideworkmessage/send');
		}
    }
})();