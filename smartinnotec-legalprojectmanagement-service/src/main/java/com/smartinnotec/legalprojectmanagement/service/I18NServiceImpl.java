package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProvinceTypeEnum;

@Service("i18NService")
public class I18NServiceImpl extends AbstractService implements I18NService {

    @Override
    public String translateCountry(final String country) throws BusinessException {
        if (country == null || country.equals("")) {
            return "";
        }
        final CountryTypeEnum countryType = CountryTypeEnum.valueOf(country);
        switch (countryType) {
        case UKRAINE:
            return "Ukraine";
        case MACEDONIA:
            return "Mazedonien";
        case ESTLAND:
            return "Estland";
        case TUNIS:
            return "Tunesien";
        case GREECE:
            return "Griechenland";
        case USA:
            return "USA";
        case CANADA:
            return "Kanada";
        case PORTUGAL:
            return "Portugal";
        case YUGOSLAWIA:
            return "Yugoslawien";
        case KOREA:
            return "Korea";
        case CHINA:
            return "China";
        case BELGIUM:
            return "Belgien";
        case AUSTRIA:
            return "Österreich";
        case BULGARIA:
            return "Bulgarien";
        case LICHTENSTEIN:
            return "Lichtenstein";
        case CROATIA:
            return "Kroatien";
        case CZECH_REPUBLIC:
            return "Tschechien";
        case EMPTY_COUNTRY:
            return "";
        case FINNLAND:
            return "Finnland";
        case FRANCE:
            return "Frankreich";
        case GERMANY:
            return "Deutschland";
        case GREAT_BRITAN:
            return "Groß Britannien";
        case IRELAND:
            return "Irland";
        case HUNGARY:
            return "Ungarn";
        case ITALY:
            return "Italien";
        case NETHERLANDS:
            return "Niederlande";
        case NORWAY:
            return "Norwegen";
        case POLAND:
            return "Polen";
        case SOUTH_AFRICA:
            return "Südafrika";
        case ROMANIA:
            return "Rumänien";
        case RUSSIA:
            return "Russland";
        case SERBIA:
            return "Serbien";
        case SLOVAKIA:
            return "Slowakei";
        case SLOVENIA:
            return "Slowenien";
        case SPAIN:
            return "Spanien";
        case SWEDEN:
            return "Schweden";
        case SWITZERLAND:
            return "Schweiz";
        case TURKEY:
            return "Türkei";
        default:
            return "";
        }
    }

    @Override
    public String translateProvinceType(final ProvinceTypeEnum provinceType) throws BusinessException {
        if (provinceType == null) {
            return "";
        }
        switch (provinceType) {
        case AARGAU:
            return "Aargau";
        case APPENZELL_AUSSERRHODEN:
            return "Appenzell Ausserrhoden";
        case APPENZELL_INNERRHODEN:
            return "Appenzell Innerrhoden";
        case BADEN_WUERTTEMBERG:
            return "Baden Württemberg";
        case BASEL_LANDSCHAFT:
            return "Basel Landschaft";
        case BASEL_STADT:
            return "Basel Stadt";
        case BAYERN:
            return "Bayern";
        case BERLIN:
            return "Berlin";
        case BERN:
            return "Bern";
        case BRANDENBURG:
            return "Brandenburg";
        case BREMEN:
            return "Bremen";
        case BURGENLAND:
            return "Burgenland";
        case EMPTY_PROVINCE:
            return "";
        case FREIBURG:
            return "Freiburg";
        case GENF:
            return "Genf";
        case GLARUS:
            return "Glarus";
        case GRAUBUENDEN:
            return "Graubünden";
        case HAMBURG:
            return "Hamburg";
        case HESSEN:
            return "Hessen";
        case JURA:
            return "Jura";
        case KAERNTEN:
            return "Kärnten";
        case LUZERN:
            return "Luzern";
        case MECKLENBURG_VORPOMMEN:
            return "Mecklenburg Vorpommen";
        case NEUENBURG:
            return "Neuenburg";
        case NIDWALDEN:
            return "Nidwalden";
        case NIEDEROESTERREICH:
            return "Niederösterreich";
        case NIEDERSACHSEN:
            return "Niedersachsen";
        case NORDREIN_WESTFALEN:
            return "Nordrein Westfalen";
        case OBEROESTERREICH:
            return "Oberösterreich";
        case OBWALDEN:
            return "Obwalden";
        case RHEINLAND_PFALZ:
            return "Rheinland Pfalz";
        case SAALLAND:
            return "Saalland";
        case SACHSEN:
            return "Sachsen";
        case SACHSEN_ANHALT:
            return "Sachsen Anhalt";
        case SALZBURG:
            return "Salzburg";
        case SCHAFFHAUSEN:
            return "Schaffenhausen";
        case SCHLESWIG_HOLSTEIN:
            return "Schleswig Holstein";
        case SCHWYZ:
            return "Schwyz";
        case SOLOTHURN:
            return "Solothurn";
        case STEIERMARK:
            return "Steiermark";
        case ST_GALLEN:
            return "St. Gallen";
        case TESSIN:
            return "Tessin";
        case THUERINGEN:
            return "Thüringen";
        case THURGAU:
            return "Thurgau";
        case TIROL:
            return "Tirol";
        case URI:
            return "Uri";
        case VORARLBERG:
            return "Vorarlberg";
        case WAADT:
            return "Waadt";
        case WALLIS:
            return "Wallis";
        case WIEN:
            return "Wien";
        case ZUERICH:
            return "Zürich";
        case ZUG:
            return "Zug";
        default:
            break;
        }
        return "";
    }

    @Override
    public String getIntSignOfCountryType(final CountryTypeEnum countryType) {
        if (countryType == null) {
            return "";
        }
        switch (countryType) {
        case FRANCE:
            return "FR";
        case TUNIS:
            return "TN";
        case GREECE:
            return "GR";
        case USA:
            return "US";
        case CANADA:
            return "CAN";
        case PORTUGAL:
            return "PT";
        case YUGOSLAWIA:
            return "YU";
        case KOREA:
            return "KR";
        case CHINA:
            return "CN";
        case LICHTENSTEIN:
            return "FL";
        case BELGIUM:
            return "BE";
        case DENMARK:
            return "DK";
        case GERMANY:
            return "DE";
        case QUATAR:
            return "QA";
        case AUSTRIA:
            return "AT";
        case CZECH_REPUBLIC:
            return "CZ";
        case SWITZERLAND:
            return "CH";
        case POLAND:
            return "PL";
        case ITALY:
            return "IT";
        case CROATIA:
            return "HR";
        case SLOVENIA:
            return "SI";
        case ISRAEL:
            return "IL";
        case IRELAND:
            return "IE";
        case SOUTH_AFRICA:
            return "ZA";
        case BOSNIA_HERZEGOVINA:
            return "BA";
        case BULGARIA:
            return "BG";
        case EGYPT:
            return "EG";
        case FINNLAND:
            return "FI";
        case GREAT_BRITAN:
            return "GB";
        case NETHERLANDS:
            return "NL";
        case NORWAY:
            return "N";
        case ROMANIA:
            return "RO";
        case RUSSIA:
            return "RU";
        case SWEDEN:
            return "S";
        case UKRAINE:
            return "UA";
        case MACEDONIA:
            return "MK";
        case SERBIA:
            return "SRB";
        case ESTLAND:
            return "EE";
        case SLOVAKIA:
            return "SK";
        case SPAIN:
            return "E";
        case TURKEY:
            return "TR";
        case HUNGARY:
            return "HU";
        case EMPTY_COUNTRY:
            return "";
        }
        return "";
    }

    @Override
    public CountryTypeEnum getCountryTypeOfIntSign(final String intSign) {
        if (intSign == null) {
            return CountryTypeEnum.EMPTY_COUNTRY;
        }
        switch (intSign) {
        case "TN":
            return CountryTypeEnum.TUNIS;
        case "GR":
            return CountryTypeEnum.GREECE;
        case "US":
            return CountryTypeEnum.USA;
        case "CAN":
            return CountryTypeEnum.CANADA;
        case "PT":
            return CountryTypeEnum.PORTUGAL;
        case "YU":
            return CountryTypeEnum.YUGOSLAWIA;
        case "KR":
            return CountryTypeEnum.KOREA;
        case "CN":
            return CountryTypeEnum.CHINA;
        case "FL":
            return CountryTypeEnum.LICHTENSTEIN;
        case "BE":
            return CountryTypeEnum.BELGIUM;
        case "DK":
            return CountryTypeEnum.DENMARK;
        case "DE":
            return CountryTypeEnum.GERMANY;
        case "QA":
            return CountryTypeEnum.QUATAR;
        case "AT":
            return CountryTypeEnum.AUSTRIA;
        case "CZ":
            return CountryTypeEnum.CZECH_REPUBLIC;
        case "CH":
            return CountryTypeEnum.SWITZERLAND;
        case "FR":
            return CountryTypeEnum.FRANCE;
        case "PL":
            return CountryTypeEnum.POLAND;
        case "IT":
            return CountryTypeEnum.ITALY;
        case "HR":
            return CountryTypeEnum.CROATIA;
        case "SI":
            return CountryTypeEnum.SLOVENIA;
        case "IL":
            return CountryTypeEnum.ISRAEL;
        case "IE":
            return CountryTypeEnum.IRELAND;
        case "ZA":
            return CountryTypeEnum.SOUTH_AFRICA;
        case "BA":
        case "BIH":
            return CountryTypeEnum.BOSNIA_HERZEGOVINA;
        case "BG":
            return CountryTypeEnum.BULGARIA;
        case "EG":
            return CountryTypeEnum.EGYPT;
        case "FI":
        case "FIN":
            return CountryTypeEnum.FINNLAND;
        case "GB":
            return CountryTypeEnum.GREAT_BRITAN;
        case "NL":
            return CountryTypeEnum.NETHERLANDS;
        case "N":
            return CountryTypeEnum.NORWAY;
        case "RO":
            return CountryTypeEnum.ROMANIA;
        case "RU":
        case "RUS":
            return CountryTypeEnum.RUSSIA;
        case "S":
            return CountryTypeEnum.SWEDEN;
        case "UA":
            return CountryTypeEnum.UKRAINE;
        case "MK":
            return CountryTypeEnum.MACEDONIA;
        case "SRB":
            return CountryTypeEnum.SERBIA;
        case "EE":
            return CountryTypeEnum.ESTLAND;
        case "SK":
        case "SO":
        case "SQ":
            return CountryTypeEnum.SLOVAKIA;
        case "ES":
        case "E":
            return CountryTypeEnum.SPAIN;
        case "TR":
            return CountryTypeEnum.TURKEY;
        case "HU":
            return CountryTypeEnum.HUNGARY;
        }
        return CountryTypeEnum.EMPTY_COUNTRY;
    }

    @Override
    public ContactTypeEnum getContactTypeOfString(String searchString) throws BusinessException {
        if (searchString == null) {
            return null;
        }
        searchString = searchString.toLowerCase();
        switch (searchString) {
        case "kunde":
            return ContactTypeEnum.CUSTOMER;
        case "laubfrosch":
            return ContactTypeEnum.LAUB_FROSCH;
        case "newsletter":
            return ContactTypeEnum.NEWS_LETTER;
        case "interessent":
            return ContactTypeEnum.POTENTIAL_BUYER;
        case "hersteller":
            return ContactTypeEnum.SUPPLIER;
        case "lieferant":
            return ContactTypeEnum.PURCHASER;
        }
        return null;
    }
}
