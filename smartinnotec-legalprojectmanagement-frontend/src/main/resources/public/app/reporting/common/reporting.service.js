(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.reporting')
    	.factory('reportingService', reportingService);
        
    reportingService.$inject = ['$http', 'api_config'];
    
    function reportingService($http, api_config) {
		var service = {
			findByReportingSearchCriteria: findByReportingSearchCriteria,
			getActivitiesBySearchCriteria: getActivitiesBySearchCriteria,
			getFacilityDetailsBySearchCriteria: getFacilityDetailsBySearchCriteria
		};
		return service;
		
		////////////
		
		function findByReportingSearchCriteria(start, end, reportingSearchCriteria) {
			return $http.put(api_config.BASE_URL + '/reportings/reporting/' + start + '/' + end + '/', reportingSearchCriteria);
		}
		
		function getActivitiesBySearchCriteria(start, end, reportingSearchCriteria) {
			return $http.put(api_config.BASE_URL + '/reportings/reporting/activities/' + start + '/' + end + '/', reportingSearchCriteria);
		}
		
		function getFacilityDetailsBySearchCriteria(start, end, reportingSearchCriteria) {
			return $http.put(api_config.BASE_URL + '/reportings/reporting/facilitydetails/' + start + '/' + end + '/', reportingSearchCriteria);
		}
    }
})();
