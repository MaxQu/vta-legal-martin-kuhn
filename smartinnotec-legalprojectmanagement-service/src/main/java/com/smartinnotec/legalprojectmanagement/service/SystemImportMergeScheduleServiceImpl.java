package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.AddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactImportedAddressImported;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CustomerNumberContainer;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.service.AbstractService;
import com.smartinnotec.legalprojectmanagement.service.AddressImportedService;
import com.smartinnotec.legalprojectmanagement.service.ContactImportedService;
import com.smartinnotec.legalprojectmanagement.service.ContactService;
import com.smartinnotec.legalprojectmanagement.service.I18NService;
import com.smartinnotec.legalprojectmanagement.service.SystemImportConfirgurationBean;
import com.smartinnotec.legalprojectmanagement.service.TenantService;
import com.smartinnotec.legalprojectmanagement.service.container.AddressContainer;
import com.smartinnotec.legalprojectmanagement.service.container.ContactContainer;

@Service("systemImportMergeScheduleService")
@Configuration
@EnableAsync
@EnableScheduling
public class SystemImportMergeScheduleServiceImpl extends AbstractService implements SystemImportMergeScheduleService {

    private static final String TENANT_NAME;

    static {
        TENANT_NAME = "vta_rottenbach";
    }

    @Autowired
    private I18NService i18NService;
    @Autowired
    private SystemImportMergeService systemImportMergeService;
    @Autowired
    private SystemImportService systemImportService;
    @Autowired
    private ContactImportedService contactImportedService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private AddressImportedService addressImportedService;
    @Autowired
    private SystemImportContactComparation systemImportContactComparation;
    @Autowired
    private SystemImportContactImportedComparation systemImportContactImportedComparation;
    @Autowired
    private TenantService tenantService;
    @Autowired
    private SystemImportConfirgurationBean systemImportConfirgurationBean;

    @Scheduled(cron = "0 0 3 1/1 * ?") // every day at 3:00 in night
    @Override
    public void startContactsImport() throws BusinessException {
        logger.info("start contact import in SystemImportMergeScheduleServiceImpl#startContactsImport");
        final String pathAddress = systemImportConfirgurationBean.getPathAddress();
        final String pathContact = systemImportConfirgurationBean.getPathContact();
        if (!pathAddress.equals("NOT_SET") && !pathContact.equals("NOT_SET")) {
            mergeContactsWithAddressesAndSave(pathAddress, pathContact);
        }
    }

    @Override
    public void mergeContactsWithAddressesOfStandardPathAndSave() throws BusinessException {
        logger.info(
            "merge contacts with addresse of standard path in SystemImportMergeScheduleServiceImpl#mergeContactsWithAddressesOfStandardPathAndSave");
        final String pathAddress = systemImportConfirgurationBean.getPathAddress();
        final String pathContact = systemImportConfirgurationBean.getPathContact();
        if (!pathAddress.equals("NOT_SET") && !pathContact.equals("NOT_SET")) {
            mergeContactsWithAddressesAndSave(pathAddress, pathContact);
        }
    }

    @Override
    public void mergeContactsWithAddressesAndSave(final String pathAddress, final String pathContact) throws BusinessException {
        logger.info("merge contacts with addresse in SystemImportMergeScheduleServiceImpl#mergeContactsWithAddressesAndSave");
        if (pathContact == null || pathContact.isEmpty()) {
            throw new BusinessException("NO_PATH_CONTACT", "400");
        }
        if (pathAddress == null || pathAddress.isEmpty()) {
            throw new BusinessException("NO_PATH_ADDRESS", "400");
        }
        final List<ContactContainer> importedContactContainers = systemImportService.importContactCSV(pathAddress);
        final List<AddressContainer> importedAddressContainers = systemImportService.importAddressCSV(pathContact);
        importContacts(importedContactContainers, importedAddressContainers);
    }

    @Override
    public void importContacts(final List<ContactContainer> importedContactContainers,
            final List<AddressContainer> importedAddressContainers)
            throws BusinessException {
        BusinessAssert.notNull(importedContactContainers,
            "importedContactContainers are mandatory in SystemImportMergeScheduleServiceImpl#importContacts");
        BusinessAssert.notNull(importedAddressContainers,
            "importedAddressContainers are mandatory in SystemImportMergeScheduleServiceImpl#importContacts");
        logger.info(
            "import contacts (amount of importedContactContainers '{}', amount of importedAddressContainers '{}') in SystemImportMergeScheduleServiceImpl#importContacts",
            importedContactContainers.size(), importedAddressContainers.size());
        final List<ContactImported> mergedContactsImported = systemImportMergeService.mergeContactsWithAddresses(importedContactContainers,
            importedAddressContainers);
        for (final ContactImported mergedContactImported : mergedContactsImported) {
            // set country
            final AddressImported addressImported = mergedContactImported.getAddress();
            final String intSign = addressImported.getIntSign();
            final CountryTypeEnum country = i18NService.getCountryTypeOfIntSign(intSign);
            addressImported.setCountry(country);
            final CustomerNumberContainer customerNumberContainer = mergedContactImported.getCustomerNumberContainers().get(0);
            final Contact contact = contactService.findByCustomerNumberContainersIn(customerNumberContainer);
            final ContactImported contactImported = contactImportedService.findByCustomerNumberContainersIn(customerNumberContainer);
            // dont import contactImported if contactImported with same parameters still exists
            if (systemImportContactImportedComparation.contactImportedChanged(contactImported, mergedContactImported) == false) {
                continue;
            }
            // dont import contact if it exists and no changes
            if (systemImportContactComparation.contactChanged(contact, mergedContactImported) == false) {
                continue;
            }
            createContactImport(mergedContactImported);
        }
    }

    private void createContactImport(final ContactImported mergedContactImported) throws BusinessException {
        // create address
        final AddressImported addressImported = mergedContactImported.getAddress();
        if (addressImported != null) {
            final AddressImported createdAddressImported = addressImportedService.create(addressImported);
            mergedContactImported.setAddress(createdAddressImported);
        }
        // create contactAddressWithProducts
        final List<ContactImportedAddressImported> contactsImportedAddressImported = mergedContactImported.getAddressesWithProducts();
        if (contactsImportedAddressImported != null && !contactsImportedAddressImported.isEmpty()) {
            for (final ContactImportedAddressImported contactImportedAddressImported : contactsImportedAddressImported) {
                final AddressImported additionalAddressImported = contactImportedAddressImported.getAddress();
                if (additionalAddressImported != null) {
                    final AddressImported createdAdditionalAddressImported = addressImportedService.create(additionalAddressImported);
                    contactImportedAddressImported.setAddress(createdAdditionalAddressImported);
                }
            }
        }

        final Tenant tenant = tenantService.findTenantByName(TENANT_NAME);
        mergedContactImported.setTenant(tenant);

        final ContactImported createdMergedContactImported = contactImportedService.create(mergedContactImported);
        logger.info("created mergedContactImported with id '{}' in SystemImportMergeScheduleServiceImpl#createContactImport",
            createdMergedContactImported.getId());
    }

    public String toString() {
        return "[SystemImportMergeScheduleServiceImpl]";
    }
}
