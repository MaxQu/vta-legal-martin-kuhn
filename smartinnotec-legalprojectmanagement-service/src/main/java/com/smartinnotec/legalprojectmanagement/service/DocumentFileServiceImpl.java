package com.smartinnotec.legalprojectmanagement.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Communication;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationAttachedDocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileParentTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileVersion;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Receipe;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeAttachment;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.DocumentFileRepository;

@Service("documentFileService")
public class DocumentFileServiceImpl extends AbstractService implements DocumentFileService {

    @Autowired
    private ProjectService projectService;
    @Autowired
    private UserService userService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private ReceipeService receipeService;
    @Autowired
    private DocumentFileRepository documentFileRepository;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;
    @Autowired
    private DocumentFileConfigurationBean documentFileConfigurationBean;
    @Autowired
    private CommunicationService communicationService;
    @Autowired
    private SearchService searchService;

    @Override
    public Integer count() throws BusinessException {
        logger.info("count documentFiles in DocumentFileService#count");
        final Long documentFiles = documentFileRepository.count();
        return documentFiles.intValue();
    }

    @Override
    public List<DocumentFile> findAll() throws BusinessException {
        logger.info("find all documentFiles in DocumentFileServiceImpl#findAll");
        return documentFileRepository.findAll();
    }

    @Override
    public DocumentFile create(final DocumentFile documentFile) throws BusinessException {
        BusinessAssert.notNull(documentFile, "documentFile is mandatory in DocumentFileServiceImpl#create", "400");
        logger.info("create documentFile in DocumentFileServiceImpl#create");
        return documentFileRepository.insert(documentFile);
    }

    @Override
    public DocumentFile update(final DocumentFile documentFile) throws BusinessException {
        BusinessAssert.notNull(documentFile, "documentFile is mandatory in DocumentFileServiceImpl#update", "400");
        logger.info("update documentFile in DocumentFileServiceImpl#create");
        return documentFileRepository.save(documentFile);
    }

    @Override
    public DocumentFile update(final DocumentFile documentFile, final Project project) throws BusinessException {
        BusinessAssert.notNull(documentFile, "documentFile is mandatory in DocumentFileServiceImpl#update", "400");
        logger.info("update documentFile in DocumentFileServiceImpl#update");
        // set responsibilityStrings for documentFileVersions @TextIndexed for @DBRef not possible
        for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
            final List<String> relevantResponsibilityStrings = documentFileVersion.getRelevantResponsibilityStrings();
            documentFileVersion.setResponsibilityStrings(relevantResponsibilityStrings);
        }
        final DocumentFile updatedDocumentFile = documentFileRepository.save(documentFile);
        if (project != null) {
            projectService.update(project);
        }
        logger.debug("updated document file with id '{}' in DocumentFileServiceImpl#update", updatedDocumentFile.getId());
        return documentFile;
    }

    @Override
    public void delete(final String documentFileId, final String username) throws BusinessException {
        BusinessAssert.notNull(documentFileId, "documentFileId is mandatory in DocumentFileServiceImpl#delete", "400");
        BusinessAssert.isId(documentFileId, "documentFileId must be an id in DocumentFileServiceImpl#delete", "400");
        logger.info("delete documentFile in DocumentFileServiceImpl#delete");

        // Search in Receipe, Contact, communication for DocumentFile referencing
        final List<Contact> contacts = contactService.findByContactAttachmentsNotNull();
        for (final Contact contact : contacts) {
            final List<ContactAttachment> contactAttachments = contact.getContactAttachments();
            if (contactAttachments != null) {
                for (final ContactAttachment contactAttachment : contactAttachments) {
                    if (contactAttachment.getDocumentFileId() != null && contactAttachment.getDocumentFileId().equals(documentFileId)) {
                        throw new BusinessException("REFERENCE_TO_CONTACT", "400");
                    }
                }
            }
        }
        final List<Receipe> receipes = receipeService.findByReceipeAttachmentsNotNull();
        for (final Receipe receipe : receipes) {
            final List<ReceipeAttachment> receipeAttachments = receipe.getReceipeAttachments();
            if (receipeAttachments != null) {
                for (final ReceipeAttachment receipeAttachment : receipeAttachments) {
                    if (receipeAttachment.getDocumentFileId() != null && receipeAttachment.getDocumentFileId().equals(documentFileId)) {
                        throw new BusinessException("REFERENCE_TO_RECIPE", "400");
                    }
                }
            }
        }
        final List<Communication> communications = communicationService.findByAttachedFileUrlsNotNull();
        for (final Communication communication : communications) {
            final List<CommunicationAttachedDocumentFile> communicationAttachedDocumentFiles = communication.getAttachedFileUrls();
            if (communicationAttachedDocumentFiles != null) {
                for (final CommunicationAttachedDocumentFile communicationAttachedDocumentFile : communicationAttachedDocumentFiles) {
                    if (communicationAttachedDocumentFile.getDocumentFileId() != null
                        && communicationAttachedDocumentFile.getDocumentFileId().equals(documentFileId)) {
                        throw new BusinessException("REFERENCE_TO_COMMUNICATION", "400");
                    }
                }
            }
        }
        renameDeletionFile(documentFileId, username);
        documentFileRepository.delete(documentFileId);
    }

    @Override
    public DocumentFile findById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in DocumentFileServiceImpl#findById", "400");
        logger.info("get documentFile with id '{}' in DocumentFileServiceImpl#findById", id);
        return documentFileRepository.findOne(id);
    }

    @Override
    public DocumentFile findByProjectIdAndFileName(String projectId, String fileName) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in DocumentFileServiceImpl#findByProjectIdAndFileName", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in DocumentFileServiceImpl#findByProjectIdAndFileName", "400");
        BusinessAssert.notNull(fileName, "fileName is mandatory in DocumentFileServiceImpl#findByProjectIdAndFileName", "400");
        logger.info("find document file by project with id '{}' and filename '{}' in DocumentFileServiceImpl#findByProjectIdAndFileName",
            projectId, fileName);
        return documentFileRepository.findByProjectProductIdAndFileName(projectId, fileName);
    }

    // get all documentFiles of project or product (also in all folders)
    @Override
    public List<DocumentFile> findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(final String projectProductId,
            final String userId, final boolean confident, final boolean active)
            throws BusinessException {
        BusinessAssert.notNull(projectProductId,
            "projectProductId is mandatory in DocumentFileServiceImpl#findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive",
            "400");
        BusinessAssert.isId(projectProductId,
            "projectProductId must be an id in DocumentFileServiceImpl#findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive",
            "400");
        BusinessAssert.notNull(projectProductId,
            "userId is mandatory in DocumentFileServiceImpl#findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive", "400");
        BusinessAssert.isId(projectProductId,
            "userId must be an id in DocumentFileServiceImpl#findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive", "400");
        logger.info(
            "find documentFiles of projectProductId '{}', userId '{}' + confident '{}' and active '{}' in DocumentFileServiceImpl#findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive",
            projectProductId, userId, confident, active);
        final List<DocumentFile> documentFiles = documentFileRepository
            .findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(projectProductId, userId, confident, active);

        final User user = userService.findUser(userId);
        return removeBlackListedRolesDocumentFiles(documentFiles, user.getProjectUserConnectionRole());
    }

    @Override
    public DocumentFile findByProjectIdAndFileNameAndFolderId(final String projectId, final String fileName, final String folderId)
            throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in DocumentFileServiceImpl#findByProjectIdAndFileName", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in DocumentFileServiceImpl#findByProjectIdAndFileName", "400");
        BusinessAssert.notNull(fileName, "fileName is mandatory in DocumentFileServiceImpl#findByProjectIdAndFileName", "400");
        BusinessAssert.notNull(folderId, "folderId is mandatory in DocumentFileServiceImpl#findByProjectIdAndFileName", "400");
        logger.info(
            "find document file by project with id '{}' and filename '{}' and folder with id '{}' in DocumentFileServiceImpl#findByProjectIdAndFileName",
            projectId, fileName, folderId);
        return documentFileRepository.findByProjectProductIdAndFileNameAndFolderId(projectId, fileName, folderId);
    }

    @Override
    public List<DocumentFile> removeBlackListedDocumentFiles(final List<DocumentFile> documentFiles, final String userId)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in DocumentFileRestService#removeBlackListedDocumentFiles", "400");
        BusinessAssert.isId(userId, "userId must be an id in DocumentFileRestService#removeBlackListedDocumentFiles", "400");
        logger.info("remove blackListed documentFiles of user with id '{}' in DocumentFileRestService#removeBlackListedDocumentFiles",
            userId);
        if (documentFiles == null || documentFiles.isEmpty()) {
            return new ArrayList<>();
        }
        documentFiles.removeIf(s -> s.getUserIdBlackList() != null && s.getUserIdBlackList().contains(userId));
        return documentFiles;
    }

    @Override
    public List<DocumentFile> removeBlackListedRolesDocumentFiles(final List<DocumentFile> documentFiles,
            final ProjectUserConnectionRoleEnum projectUserConnectionRole)
            throws BusinessException {
        BusinessAssert.notNull(projectUserConnectionRole,
            "projectUserConnectionRole is mandatory in DocumentFileServiceImpl#removeBlackListedRolesDocumentFiles", "400");
        logger.info("remove black listed roles '{}' of documentFiles in DocumentFileServiceImpl#removeBlackListedRolesDocumentFiles",
            projectUserConnectionRole);
        // if role of user is not set than no documentFile is returned
        if (documentFiles == null || documentFiles.isEmpty() || projectUserConnectionRole == null) {
            return new ArrayList<>();
        }
        documentFiles.removeIf(s -> s.getRolesBlackList() != null && s.getRolesBlackList().contains(projectUserConnectionRole));
        return documentFiles;
    }

    @Override
    public List<DocumentFile> findPagedDocumentFilesByProjectIdAndFolderId(final String projectId, final String folderId,
            final String userId, final Integer page, String sortingType, final DocumentFileParentTypeEnum documentFileParentType)
            throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in DocumentFileServiceImpl#findPagedDocumentFilesByProjectId", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in DocumentFileServiceImpl#findPagedDocumentFilesByProjectId", "400");
        BusinessAssert.notNull(userId, "userId is mandatory in DocumentFileServiceImpl#findPagedDocumentFilesByProjectId", "400");
        BusinessAssert.isId(userId, "userId must be an id in DocumentFileServiceImpl#findPagedDocumentFilesByProjectId", "400");
        BusinessAssert.notNull(folderId, "folderId is mandatory in DocumentFileServiceImpl#findPagedDocumentFilesByProjectId", "400");
        BusinessAssert.notNull(page, "page is mandatory in DocumentFileServiceImpl#findPagedDocumentFilesByProjectId", "400");
        BusinessAssert.notNull(documentFileParentType,
            "documentFileParentType is mandatory in DocumentFileServiceImpl#findPagedDocumentFilesByProjectId", "400");
        logger.info(
            "find document files by project with id '{}' and user with id '{}' and page '{}' in DocumentFileServiceImpl#findPagedDocumentFilesByProjectId",
            projectId, userId, page);
        final User user = userService.findUser(userId);
        Project project = null;
        boolean canUserSeeConfidentialDocumentFiles = false;
        switch (documentFileParentType) {
        case PROJECT:
            project = projectService.findProject(projectId);
            final ProjectUserConnection projectUserConnection = projectUserConnectionService.findProjectUserConnectionByUserAndProject(user,
                project);
            canUserSeeConfidentialDocumentFiles = projectUserConnectionService
                .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(projectUserConnection.getProjectUserConnectionRole());
            break;
        case PRODUCT:
            canUserSeeConfidentialDocumentFiles = projectUserConnectionService
                .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());
            break;
        }
        Direction sortDirection = Sort.Direction.ASC;
        if (sortingType.startsWith("-")) {
            sortDirection = Sort.Direction.DESC;
            sortingType = sortingType.substring(1);
        }
        final Pageable pageable = new PageRequest(page, this.getDocumentPageSize(), new Sort(sortDirection, sortingType));
        if (canUserSeeConfidentialDocumentFiles) {
            final List<DocumentFile> pagedDocumentFiles = documentFileRepository
                .findPagedDocumentFilesByProjectProductIdAndFolderIdAndUserIdBlackListNotInAndActive(projectId, folderId, userId, true,
                    pageable);
            return pagedDocumentFiles;
        }
        final List<DocumentFile> pagedDocumentFiles = documentFileRepository
            .findPagedDocumentFilesByProjectProductIdAndFolderIdAndUserIdBlackListNotInAndConfidentAndActiveOrUserIdWhiteListIn(projectId,
                folderId, userId, canUserSeeConfidentialDocumentFiles, true, userId, pageable);
        // needed because of ...OrUserIdWhiteListIn
        if (projectId != null) {
            pagedDocumentFiles.removeIf(s -> !s.getProjectProductId().equals(projectId));
        }
        return pagedDocumentFiles;
    }

    @Override
    public List<DocumentFile> findAssignedDocumentFilesByProjectId(final String projectId, final String userId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in DocumentFileServiceImpl#findPagedAndAssignedDocumentFilesByProjectId",
            "400");
        BusinessAssert.isId(projectId, "projectId must be an id in DocumentFileServiceImpl#findPagedAndAssignedDocumentFilesByProjectId",
            "400");
        BusinessAssert.notNull(userId, "userId is mandatory in DocumentFileServiceImpl#findPagedAndAssignedDocumentFilesByProjectId",
            "400");
        BusinessAssert.isId(userId, "userId must be an id in DocumentFileServiceImpl#findPagedAndAssignedDocumentFilesByProjectId", "400");
        logger.info(
            "find assigned document files by project with id '{}' and user with id '{}' in DocumentFileServiceImpl#findPagedAndAssignedDocumentFilesByProjectId",
            projectId, userId);
        final List<DocumentFile> pagedDocumentFiles = documentFileRepository
            .findDocumentFilesByProjectProductIdAndUserIdWhiteListInAndActive(projectId, userId, true);
        return pagedDocumentFiles;
    }

    @Override
    public List<DocumentFile> findPagedAndConfidentDocumentFiles(final String projectId, final String folderId, final String userId,
            final boolean confident, final Integer page, String sortingType)
            throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in DocumentFileServiceImpl#findPagedAndConfidentDocumentFiles", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in DocumentFileServiceImpl#findPagedAndConfidentDocumentFiles", "400");
        BusinessAssert.notNull(userId, "userId is mandatory in DocumentFileServiceImpl#findPagedAndConfidentDocumentFiles", "400");
        BusinessAssert.notNull(folderId, "folderId is mandatory in DocumentFileServiceImpl#findPagedAndConfidentDocumentFiles", "400");
        BusinessAssert.isId(userId, "userId must be an id in DocumentFileServiceImpl#findPagedAndConfidentDocumentFiles", "400");
        logger.info(
            "find paged and confident documentfiles by project with id '{}', folder with id '{}', user with id '{}', confident '{}', page '{}' and sortingType '{}' in DocumentFileServiceImpl#findPagedAndConfidentDocumentFiles",
            projectId, folderId, userId, confident, page, sortingType);
        Direction sortDirection = Sort.Direction.ASC;
        if (sortingType.startsWith("-")) {
            sortDirection = Sort.Direction.DESC;
            sortingType = sortingType.substring(1);
        }
        final Pageable pageable = new PageRequest(page, this.getDocumentPageSize(), new Sort(sortDirection, sortingType));
        final List<DocumentFile> documentFiles = documentFileRepository
            .findPagedAndConfidentDocumentFilesByProjectProductIdAndFolderIdAndUserIdBlackListNotInAndConfidentAndActive(projectId,
                folderId, userId, confident, true, pageable);
        if (projectId != null) {
            documentFiles.removeIf(s -> !s.getProjectProductId().equals(projectId));
        }
        return documentFiles;
    }

    @Override
    public Integer countByProjectProductIdGroupByFileName(final String projectId, final String userId, final Boolean onlyConfident,
            final DocumentFileParentTypeEnum documentFileParentType)
            throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in DocumentFileServiceImpl#countByProjectIdGroupByOriginalFileName",
            "400");
        BusinessAssert.isId(projectId, "projectId is mandatory in DocumentFileServiceImpl#countByProjectIdGroupByFileName", "400");
        logger.info("count documentFiles by project with id '{}' in DocumentFileServiceImpl#countByProjectIdGroupByFileName", projectId);
        final User user = userService.findUser(userId);
        Project project = null;
        boolean canUserSeeConfidentialDocumentFiles = false; // TODO: for products
        switch (documentFileParentType) {
        case PROJECT:
            project = projectService.findProject(projectId);
            final ProjectUserConnection projectUserConnection = projectUserConnectionService.findProjectUserConnectionByUserAndProject(user,
                project);
            canUserSeeConfidentialDocumentFiles = projectUserConnectionService
                .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(projectUserConnection.getProjectUserConnectionRole());
            break;
        }
        if (onlyConfident) {
            return documentFileRepository.countByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(projectId, userId,
                onlyConfident, true);
        } else if (canUserSeeConfidentialDocumentFiles) {
            return documentFileRepository.countByProjectProductIdAndUserIdBlackListNotInAndActive(projectId, userId, true);
        }
        return documentFileRepository.countByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActiveOrUserIdWhiteListIn(projectId,
            userId, canUserSeeConfidentialDocumentFiles, true, userId);
    }

    @Override
    public Integer countByProjectIdAndFolderIdGroupByFileName(final String projectId, final String folderId, final String userId,
            final Boolean onlyConfident, final DocumentFileParentTypeEnum documentFileParentType)
            throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in DocumentFileServiceImpl#countByProjectIdAndFolderIdGroupByFileName",
            "400");
        BusinessAssert.isId(projectId, "projectId is mandatory in DocumentFileServiceImpl#countByProjectIdAndFolderIdGroupByFileName",
            "400");
        BusinessAssert.notNull(folderId, "folderId is mandatory in DocumentFileServiceImpl#countByProjectIdAndFolderIdGroupByFileName",
            "400");
        BusinessAssert.notNull(userId, "userId is mandatory in DocumentFileServiceImpl#countByProjectIdAndFolderIdGroupByFileName", "400");
        BusinessAssert.isId(userId, "userId is mandatory in DocumentFileServiceImpl#countByProjectIdAndFolderIdGroupByFileName", "400");
        BusinessAssert.notNull(onlyConfident,
            "onlyConfident is mandatory in DocumentFileServiceImpl#countByProjectIdAndFolderIdGroupByFileName", "400");
        logger.info(
            "count documentFiles by project with id '{}' and folder with id '{}' and user with id '{}' in DocumentFileServiceImpl#countByProjectIdAndFolderIdGroupByFileName",
            projectId, folderId, userId);
        final User user = userService.findUser(userId);

        Project project = null;
        boolean canUserSeeConfidentialDocumentFiles = false; // TODO: for products
        switch (documentFileParentType) {
        case PROJECT:
            project = projectService.findProject(projectId);
            final ProjectUserConnection projectUserConnection = projectUserConnectionService.findProjectUserConnectionByUserAndProject(user,
                project);
            canUserSeeConfidentialDocumentFiles = projectUserConnectionService
                .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(projectUserConnection.getProjectUserConnectionRole());
            break;
        }
        if (onlyConfident) {
            return documentFileRepository.countByProjectProductIdAndUserIdBlackListNotInAndFolderIdAndConfidentAndActive(projectId, userId,
                onlyConfident, true);
        } else if (canUserSeeConfidentialDocumentFiles) {
            return documentFileRepository.countByProjectProductIdAndUserIdBlackListNotInAndFolderIdAndActive(projectId, userId, folderId,
                true);
        }
        return documentFileRepository.countByProjectProductIdAndUserIdBlackListNotInAndFolderIdAndConfidentAndActiveOrUserIdWhiteListIn(
            projectId, userId, folderId, canUserSeeConfidentialDocumentFiles, true, userId);
    }

    @Override
    public DocumentFileVersion findDocumentFileVersion(final DocumentFile documentFile, final String version) throws BusinessException {
        BusinessAssert.notNull(documentFile, "documentFile is mandatory in DocumentFileServiceImpl#findDocumentFileVersion", "400");
        BusinessAssert.notNull(version, "version is mandatory in DocumentFileServiceImpl#findDocumentFileVersion", "400");
        logger.info("find document file version of version '{}' in DocumentFileServiceImpl#findDocumentFileVersion", version);
        final List<DocumentFileVersion> documentFileVersions = documentFile.getDocumentFileVersions().stream()
            .filter(s -> s.getVersion().equals(version)).collect(Collectors.toList());
        if (documentFileVersions != null && !documentFileVersions.isEmpty()) {
            return documentFileVersions.get(0);
        }
        return null;
    }

    @Override
    public boolean renameUpdatedFile(final DocumentFile currentDocumentFile, final DocumentFile newDocumentFile) throws BusinessException {
        final String subFilePath = currentDocumentFile.getSubFilePath();
        final List<DocumentFileVersion> documentFileVersions = currentDocumentFile.getDocumentFileVersions();
        for (final DocumentFileVersion documentFileVersion : documentFileVersions) {
            final File file = new File(this.getFilePathName(subFilePath, documentFileVersion.getFilePathName()));
            final String absolutePath = file.getAbsolutePath();
            final String filePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));

            final String fileName = newDocumentFile.getFileName();
            final String fileNameWithoutExtension = fileName.contains(".") ? fileName.substring(0, fileName.lastIndexOf(".")) : fileName;
            final String extension = fileName.contains(".") ? fileName.substring(fileName.lastIndexOf(".")) : "";
            final String newFileName = fileNameWithoutExtension + documentFileConfigurationBean.getFolderIdPrefix()
                + newDocumentFile.getFolderId() + documentFileVersion.getVersion() + extension;

            final File renameFile = new File(filePath + File.separator + newFileName);
            try {

                final DocumentFileVersion newDocumentFileVersion = getDocumentFileVersionOfVersion(documentFileVersion.getVersion(),
                    newDocumentFile);
                newDocumentFileVersion.setFilePathName(newFileName);
                documentFileVersion.setFilePathName(newFileName);
                final DocumentFile updatedDocumentFile = this.update(currentDocumentFile);
                logger.info("document file with id '{}' updated and renamed to '{}' in DocumentFileServiceImpl#renameDeletionFile",
                    updatedDocumentFile.getId(), renameFile.getAbsoluteFile());
            } catch (Exception e) {
                logger.error(
                    "document file '{}' could not be renamed because of this message '{}' in DocumentFileServiceImpl#renameDeletionFile",
                    file.getAbsoluteFile(), e.getMessage());
                return false;
            }
        }
        return true;
    }

    private DocumentFileVersion getDocumentFileVersionOfVersion(final String version, final DocumentFile documentFile) {
        final List<DocumentFileVersion> documentFileVersions = documentFile.getDocumentFileVersions();
        for (final DocumentFileVersion documentFileVersion : documentFileVersions) {
            if (documentFileVersion.getVersion().equals(version)) {
                return documentFileVersion;
            }
        }
        return null;
    }

    @Override
    public boolean renameDeletionFile(final String documentFileId, final String username) throws BusinessException {
        // add deleted, date and username to files deleted in database
        final DocumentFile documentFile = documentFileRepository.findOne(documentFileId);
        for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
            final DateTime deletionDate = new DateTime(System.currentTimeMillis());
            final File file = new File(this.getFilePathName(documentFile.getSubFilePath(), documentFileVersion.getFilePathName()));

            String newFileName = file.getAbsolutePath();
            final String absolutePath = file.getAbsolutePath();
            if (absolutePath.indexOf(".") > -1) {
                final String pathWithoutExtension = absolutePath.substring(0, absolutePath.lastIndexOf("."));
                final String extension = absolutePath.substring(absolutePath.lastIndexOf("."));
                newFileName = pathWithoutExtension + "#Del#" + username + "#" + dateTimeFormatter2.print(deletionDate) + extension;
            }

            File file2 = new File(newFileName);
            if (file2.exists()) {
                logger.error("file with name '{}' still exists - file cannot be renamed in DocumentFileServiceImpl#renameDeletionFile",
                    file2.getAbsolutePath());
                continue;
            }

            try {
                logger.info("file renamed to '{}' in DocumentFileServiceImpl#renameDeletionFile", file.getAbsoluteFile());
            } catch (Exception e) {
                logger.error("file '{}' could not be renamed in DocumentFileServiceImpl#renameDeletionFile", file.getAbsoluteFile());
            }
        }
        return true;
    }

    @Override
    public DocumentFileVersion getDocumentFileVersionOfLastVersion(final DocumentFile documentFile) throws BusinessException {
        BusinessAssert.notNull(documentFile, "documentFile must not be null in DocumentFileServiceImpl#getDocumentFileVersionOfLastVersion",
            "400");
        logger.info(
            "get documentFileVersion of last version of documentFile with id '{}' in DocumentFileServiceImpl#getDocumentFileVersionOfLastVersion",
            documentFile.getId());

        // initialy set a version
        DocumentFileVersion documentFileVersionOfLastVersion = null;
        for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
            if (documentFileVersion.isActive() == true) {
                documentFileVersionOfLastVersion = documentFileVersion;
            }
        }
        if (documentFileVersionOfLastVersion == null) {
            return null;
        }
        // get highest active version
        for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
            if (documentFileVersion.isActive()
                && documentFileVersion.getVersion().compareTo(documentFileVersionOfLastVersion.getVersion()) > 1) {
                documentFileVersionOfLastVersion = documentFileVersion;
            }
        }
        return documentFileVersionOfLastVersion;
    }

    @Override
    public List<DocumentFile> findBySearchStringAndProjectIdAndFolderId(final User user, String searchString, final String projectId,
            final String folderId)
            throws BusinessException {
        BusinessAssert.notNull(searchString,
            "searchString must not be null in DocumentFileServiceImpl#findBySearchStringAndProjectIdAndFolderId", "400");
        BusinessAssert.notNull(user, "user must not be null in DocumentFileServiceImpl#findBySearchStringAndProjectIdAndFolderId", "400");
        BusinessAssert.notNull(projectId, "projectId must not be null in DocumentFileServiceImpl#findBySearchStringAndProjectIdAndFolderId",
            "400");
        BusinessAssert.isId(projectId, "projectId must not be null in DocumentFileServiceImpl#findBySearchStringAndProjectIdAndFolderId",
            "400");
        logger.info(
            "find documentFiles by searchString '{}' and projectId '{}' in DocumentFileServiceImpl#findBySearchStringAndProjectIdAndFolderId",
            searchString, projectId);
        final boolean canSeeConfidentDocumentFiles = projectUserConnectionService
            .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());

        List<DocumentFile> documentFiles = new ArrayList<>();
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        if (combineSearchStringWithAnd) {
            searchString = searchString.replaceAll("\\+", "");

            if (searchString.contains("*")) {
                searchString = searchString.replaceAll("\\*", "");
                documentFiles = documentFileRepository.findByRegexString(searchString);
                documentFiles.removeIf(s -> !s.getProjectProductId().equals(projectId));
                documentFiles.removeIf(s -> !s.getFolderId().equals(folderId));
            } else {
                documentFiles = documentFileRepository.findBySearchString(searchString);
                documentFiles.removeIf(s -> !s.getProjectProductId().equals(projectId));
                documentFiles.removeIf(s -> !s.getFolderId().equals(folderId));
            }

            for (final Iterator<DocumentFile> iterator = documentFiles.iterator(); iterator.hasNext();) {
                final DocumentFile documentFile = iterator.next();
                final StringBuilder sb = new StringBuilder();
                for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
                    sb.append(documentFileVersion.getLabels());
                }
                final String documentFileProperties = documentFile.getFileName() + documentFile.getInformation() + documentFile.getEnding()
                    + sb.toString();
                if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(documentFileProperties.toLowerCase()::contains)) {
                    iterator.remove();
                }
            }
        } else {
            documentFiles = documentFileRepository.findBySearchStringAndProjectProductIdAndFolderId(searchString, projectId, folderId);
        }

        if (canSeeConfidentDocumentFiles == false) {
            documentFiles.removeIf(s -> s.isConfident() == true);
        }
        return documentFiles;
    }

    // user must NOT be member of project
    @Override
    public List<DocumentFile> findDocumentFilesBySearchStringOverAllDocuments(final User user, String searchString)
            throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in DocumentFileServiceImpl#findDocumentFilesBySearchString", "400");
        logger.info("find document files by search string '{}' in DocumentFileServiceImpl#findDocumentFilesBySearchString", searchString);

        // find documentFiles of project
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        final List<DocumentFile> foundedDocumentFiles = new ArrayList<>();
        if (combineSearchStringWithAnd) {
            searchString = searchString.replaceAll("\\+", "");
            List<DocumentFile> documentFiles = new ArrayList<>();
            if (searchString.contains("*")) {
                searchString = searchString.replaceAll("\\*", "");
                documentFiles = documentFileRepository.findByRegexString(searchString);
                foundedDocumentFiles.addAll(documentFiles);
            } else {
                documentFiles = documentFileRepository.findBySearchString(searchString);
                for (final Iterator<DocumentFile> iterator = documentFiles.iterator(); iterator.hasNext();) {
                    final DocumentFile documentFile = iterator.next();
                    final StringBuilder sb = new StringBuilder();
                    for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
                        sb.append(documentFileVersion.getLabels());
                    }
                    final String documentFileProperties = documentFile.getFileName() + documentFile.getInformation()
                        + documentFile.getEnding() + sb.toString();
                    if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(documentFileProperties.toLowerCase()::contains)) {
                        iterator.remove();
                    }
                    foundedDocumentFiles.addAll(documentFiles);
                }
            }
        } else {
            final List<DocumentFile> documentFiles = documentFileRepository.findBySearchString(searchString);
            foundedDocumentFiles.addAll(documentFiles);
        }
        return foundedDocumentFiles;
    }

    // user must be member of project
    @Override
    public List<DocumentFile> findDocumentFilesBySearchString(final User user, String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in DocumentFileServiceImpl#findDocumentFilesBySearchString", "400");
        logger.info("find document files by search string '{}' in DocumentFileServiceImpl#findDocumentFilesBySearchString", searchString);
        // get projects of user
        final List<ProjectUserConnection> projectUserConnections = projectUserConnectionService.findByUserAndActive(user, true);
        final List<String> projectIds = new ArrayList<>();
        for (final ProjectUserConnection projectUserConnection : projectUserConnections) {
            projectIds.add(projectUserConnection.getProject().getId());
        }
        // find documentFiles of project
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        final List<DocumentFile> foundedDocumentFiles = new ArrayList<>();
        for (final String projectId : projectIds) {
            if (combineSearchStringWithAnd) {
                searchString = searchString.replaceAll("\\+", "");
                List<DocumentFile> documentFiles = new ArrayList<>();
                if (searchString.contains("*")) {
                    searchString = searchString.replaceAll("\\*", "");
                    documentFiles = documentFileRepository.findByRegexString(searchString);
                    documentFiles.removeIf(s -> !s.getProjectProductId().equals(projectId));
                    foundedDocumentFiles.addAll(documentFiles);
                } else {
                    documentFiles = documentFileRepository.findBySearchString(searchString);
                    documentFiles.removeIf(s -> !s.getProjectProductId().equals(projectId));
                    for (final Iterator<DocumentFile> iterator = documentFiles.iterator(); iterator.hasNext();) {
                        final DocumentFile documentFile = iterator.next();
                        final StringBuilder sb = new StringBuilder();
                        for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
                            sb.append(documentFileVersion.getLabels());
                        }
                        final String documentFileProperties = documentFile.getFileName() + documentFile.getInformation()
                            + documentFile.getEnding() + sb.toString();
                        if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(documentFileProperties.toLowerCase()::contains)) {
                            iterator.remove();
                        }
                    }
                    foundedDocumentFiles.addAll(documentFiles);
                }
            } else {
                final List<DocumentFile> documentFiles = documentFileRepository.findBySearchStringAndProjectProductId(searchString,
                    projectId);
                foundedDocumentFiles.addAll(documentFiles);
            }
        }
        return foundedDocumentFiles;
    }

    // search directly in documentFile
    @Override
    public List<DocumentFile> findBySearchString(String searchString, final User user) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString must not be null in DocumentFileServiceImpl#findBySearchString", "400");
        logger.info("find documentFiles by searchString '{}' in DocumentFileServiceImpl#findBySearchString", searchString);
        final boolean canSeeConfidentDocumentFiles = projectUserConnectionService
            .canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());

        List<DocumentFile> documentFiles = new ArrayList<>();
        final boolean combineSearchStringWithAnd = searchService.combineSearchStringWithAnd(searchString);
        if (combineSearchStringWithAnd) {
            searchString = searchString.replaceAll("\\+", "");
            if (searchString.contains("*")) {
                searchString = searchString.replaceAll("\\*", "");
                documentFiles = documentFileRepository.findByRegexString(searchString);
            } else {
                documentFiles = documentFileRepository.findBySearchString(searchString);
            }
        }

        documentFiles.removeIf(s -> s.getUserIdBlackList() != null && s.getUserIdBlackList().contains(user.getId()));
        documentFiles.removeIf(s -> s.getRolesBlackList() != null && s.getRolesBlackList().contains(user.getProjectUserConnectionRole()));
        if (canSeeConfidentDocumentFiles == false) {
            documentFiles.removeIf(s -> s.isConfident() == true);
        }

        // remove files where user is not in project (projectUserConnection)
        for (final Iterator<DocumentFile> iterator = documentFiles.iterator(); iterator.hasNext();) {
            final DocumentFile documentFile = iterator.next();
            final String projectProductId = documentFile.getProjectProductId();
            final Project project = projectService.findProject(projectProductId);
            if (project != null) {
                final ProjectUserConnection projectUserConnection = projectUserConnectionService
                    .findProjectUserConnectionByUserAndProject(user, project);
                if (projectUserConnection == null) {
                    iterator.remove();
                    continue;
                }
            }
            if (combineSearchStringWithAnd) {
                final StringBuilder sb = new StringBuilder();
                for (final DocumentFileVersion documentFileVersion : documentFile.getDocumentFileVersions()) {
                    sb.append(documentFileVersion.getLabels());
                }
                final String documentFileProperties = documentFile.getFileName() + documentFile.getInformation() + documentFile.getEnding()
                    + sb.toString();
                if (!Stream.of(searchString.toLowerCase().split(" ")).allMatch(documentFileProperties.toLowerCase()::contains)) {
                    iterator.remove();
                }
            }
        }
        return documentFiles;
    }

    @Override
    public List<DocumentFile> findByProjectId(final String projectId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId must not be null in DocumentFileServiceImpl#findByProjectIdAndSearchString", "400");
        BusinessAssert.isId(projectId, "projectId must not be null in DocumentFileServiceImpl#findByProjectIdAndSearchString", "400");
        logger.info("find document files by project with id '{}' in DocumentFileServiceImpl#findByProjectIdAndSearchString", projectId);
        return documentFileRepository.findByProjectProductId(projectId);
    }

    @Override
    public String getFilePathName(final String subFilePath, final String filePathName) {
        return subFilePath + File.separator + filePathName;
    }

    @Override
    public Integer calculateAmountOfPages(final String projectId, final String userId, final String folderId,
            final DocumentFileParentTypeEnum documentFileParentType)
            throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in DocumentFileServiceImpl#calculateAmountOfPages", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in DocumentFileServiceImpl#calculateAmountOfPages", "400");
        logger.info("calculate amount of pages for project with id '{}' in DocumentFileServiceImpl#calculateAmountOfPages", projectId);
        final Integer projectPageSize = this.getDocumentPageSize();
        final Integer amountOfDocumentFilesOfProject = this.countByProjectIdAndFolderIdGroupByFileName(projectId, folderId, userId, false,
            documentFileParentType);
        final Integer amountOfPageSizes = (amountOfDocumentFilesOfProject + projectPageSize - 1) / projectPageSize;
        logger.info("calculated amount of pages to '{}' for project with id '{}' in DocumentFileServiceImpl#calculateAmountOfPages",
            amountOfPageSizes, projectId);
        return amountOfPageSizes;
    }

    @Override
    public Integer getDocumentFilePageSize() throws BusinessException {
        return super.getDocumentPageSize();
    }

    @Override
    public String toString() {
        return "[DocumentFileServiceImpl]";
    }
}
