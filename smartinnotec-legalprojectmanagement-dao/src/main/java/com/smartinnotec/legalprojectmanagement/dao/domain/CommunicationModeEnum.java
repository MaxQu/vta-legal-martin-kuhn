package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum CommunicationModeEnum {

    CHAT("CHAT"),
    TODO("TODO");

    private String value;

    private CommunicationModeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
