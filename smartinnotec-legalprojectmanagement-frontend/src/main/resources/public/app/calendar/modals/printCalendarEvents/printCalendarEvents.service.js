(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('printCalendarEventsModalService', printCalendarEventsModalService);
        
    printCalendarEventsModalService.$inject = ['$modal', '$stateParams', 'calendarService', 'dateUtilityService'];
    
    function printCalendarEventsModalService($modal, $stateParams, calendarService, dateUtilityService) {
		var service = {
			showPrintCalendarEventsModal: showPrintCalendarEventsModal
		};		
		return service;
		
		////////////
				
		function showPrintCalendarEventsModal(calendarEvents, calendarEventSearchString, allCalendarEventsTrigger) {
			 var printCalendarEventModal = $modal.open({
				controller: PrintCalendarEventsModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	calendarEvents: function() {
			    		return calendarEvents;
			    	}, 
			    	calendarEventSearchString: function() {
			    		return calendarEventSearchString;
			    	}, 
			    	allCalendarEventsTrigger: function() {
			    		return allCalendarEventsTrigger;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/printCalendarEvents/printCalendarEvents.modal.html'
			});
			return printCalendarEventModal;
			
			function PrintCalendarEventsModalController($modalInstance, $scope, calendarEvents, calendarEventSearchString, allCalendarEventsTrigger) {
		    	var vm = this;		
		    	vm.calendarEvents = calendarEvents;
		    	vm.calendarEventSearchString = calendarEventSearchString;
		    	vm.allCalendarEventsTrigger = allCalendarEventsTrigger;
		    	
		    	vm.orderType = 'startsAt';
		    	
		    	vm.changeLongDateToFormattedDate = changeLongDateToFormattedDate;
		    	vm.closeCalendarEventPrintModal = closeCalendarEventPrintModal;
		    	
		    	function changeLongDateToFormattedDate(dateInLong) {
	      			  var formattedDate = dateUtilityService.getFormattedDateFromLongDate(dateInLong);
	      			  var formattedTime = dateUtilityService.getFormattedTimeFromLongDate(dateInLong);
	      			  return formattedDate + ' <b>' + formattedTime + '</b>';
	      		  }
		    	
		    	function closeCalendarEventPrintModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();