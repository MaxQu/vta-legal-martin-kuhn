(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.nav')
    	.directive('asUserNavPanel', asUserNavPanel);
    					
	function asUserNavPanel() {
		var directive = {
			restrict: 'E',
			replace: true,
			scope: {
				asUser: '='
			},
			controller: UserNavPanelController,
			controllerAs: 'vm',
			bindToController: true,
			templateUrl: 'app/nav/as-user-nav-panel/as-user-nav-panel.directive.html'
		};
		
		return directive;
	}
	
	UserNavPanelController.$inject = ['$scope', 'userService', '$state'];
	
	function UserNavPanelController($scope, userService, $state) {
		$scope.vm = this;
		var vm = this;

		vm.user = vm.asUser;
		vm.logout = logout;
		vm.imageUrl = 'api/files/profileimage/' + vm.user.profileImagePath + '/';
		
		////////////
		
		function logout() {
			userService.logout();
			$state.go('home.signin');
		}
	}
})();