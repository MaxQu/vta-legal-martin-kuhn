(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();