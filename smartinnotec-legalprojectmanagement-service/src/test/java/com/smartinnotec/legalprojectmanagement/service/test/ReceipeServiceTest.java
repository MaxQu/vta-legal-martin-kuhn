package com.smartinnotec.legalprojectmanagement.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeProduct;
import com.smartinnotec.legalprojectmanagement.service.base.test.AbstractServiceTest;

public class ReceipeServiceTest extends AbstractServiceTest {

    @Test
    public void shouldFindSameReceipes() throws BusinessException {

        receipeService.deleteAll();

        final Product p1 = new Product();
        p1.setName("Product1");
        final Product createdP1 = productService.create(p1);
        Assert.assertNotNull(createdP1);

        final Product p2 = new Product();
        p2.setName("Product2");
        final Product createdP2 = productService.create(p2);
        Assert.assertNotNull(createdP2);

        final Product p3 = new Product();
        p3.setName("Product3");
        final Product createdP3 = productService.create(p3);
        Assert.assertNotNull(createdP3);

        // 1
        final List<ReceipeProduct> receipeProducts1 = new ArrayList<>();
        final ReceipeProduct receipeProduct1 = new ReceipeProduct();
        receipeProduct1.setDensity(2f);
        receipeProduct1.setInformation("receipeProduct1");
        receipeProduct1.setPercentage(20f);
        receipeProduct1.setProduct(p1);
        receipeProducts1.add(receipeProduct1);
        final ReceipeProduct receipeProduct2 = new ReceipeProduct();
        receipeProduct2.setDensity(2f);
        receipeProduct2.setInformation("receipeProduct2");
        receipeProduct2.setPercentage(20f);
        receipeProduct2.setProduct(p2);
        receipeProducts1.add(receipeProduct2);
        final ReceipeProduct receipeProduct3 = new ReceipeProduct();
        receipeProduct3.setDensity(2f);
        receipeProduct3.setInformation("receipeProduct3");
        receipeProduct3.setPercentage(20f);
        receipeProduct3.setProduct(p3);
        receipeProducts1.add(receipeProduct3);

        // 2
        final List<ReceipeProduct> receipeProducts2 = new ArrayList<>();
        final ReceipeProduct receipeProduct21 = new ReceipeProduct();
        receipeProduct21.setDensity(2f);
        receipeProduct21.setInformation("receipeProduct1");
        receipeProduct21.setPercentage(20f);
        receipeProduct21.setProduct(p1);
        receipeProducts2.add(receipeProduct21);
        final ReceipeProduct receipeProduct22 = new ReceipeProduct();
        receipeProduct22.setDensity(2f);
        receipeProduct22.setInformation("receipeProduct2");
        receipeProduct22.setPercentage(20f);
        receipeProduct22.setProduct(p2);
        receipeProducts2.add(receipeProduct22);
        final ReceipeProduct receipeProduct32 = new ReceipeProduct();
        receipeProduct32.setDensity(2f);
        receipeProduct32.setInformation("receipeProduct3");
        receipeProduct32.setPercentage(20f);
        receipeProduct32.setProduct(p3);
        receipeProducts2.add(receipeProduct32);

        final boolean retainAll = receipeProducts2.retainAll(receipeProducts1);
        Assert.assertTrue(retainAll);

    }
}
