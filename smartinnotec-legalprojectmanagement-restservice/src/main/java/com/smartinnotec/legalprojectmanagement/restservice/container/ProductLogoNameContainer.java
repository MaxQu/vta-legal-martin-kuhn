package com.smartinnotec.legalprojectmanagement.restservice.container;

import lombok.Data;

public @Data class ProductLogoNameContainer {

    private String logoName;

}