package com.smartinnotec.legalprojectmanagement.restservice;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.LoginHistory;
import com.smartinnotec.legalprojectmanagement.dao.domain.LoginHistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;
import com.smartinnotec.legalprojectmanagement.locale.MessageByLocaleService;
import com.smartinnotec.legalprojectmanagement.restservice.container.FrontentTypeEnum;
import com.smartinnotec.legalprojectmanagement.security.xauth.LoginData;
import com.smartinnotec.legalprojectmanagement.security.xauth.SimpleUserDetails;
import com.smartinnotec.legalprojectmanagement.security.xauth.TokenUtils;
import com.smartinnotec.legalprojectmanagement.security.xauth.UserTransferObject;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.LoginHistoryService;
import com.smartinnotec.legalprojectmanagement.service.TenantService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class AuthRestService extends AbstractRestService {

    private static final String ADMIN_PASSWORD;

    @Autowired
    private TokenUtils tokenUtils;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    @Autowired
    private MessageByLocaleService messageByLocaleService;
    @Autowired
    private UserService userService;
    @Autowired
    private TenantService tenantService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private LoginHistoryService loginHistoryService;
    @Autowired
    private HistoryService historyService;

    static {
        ADMIN_PASSWORD = "$2a$10$4hfQQ0DaXOI/2q5No5NGwuYiinLkJCPcDzPrb8pm7I0VMMZnifuMC";
    }

    @Autowired
    public AuthRestService(final AuthenticationManager am, final UserDetailsService userDetailsService) {
        this.authenticationManager = am;
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

    private boolean preventExternalWorkerLoggedInViaADM(final FrontentTypeEnum frontendType, final User user) {
        logger.info("prevent external worker logged in via ADM in AuthRestService#preventExternalWorkerLoggedInViaADM");
        if (frontendType == FrontentTypeEnum.WDM && user.getProjectUserConnectionRole() == ProjectUserConnectionRoleEnum.EXTERNAL_WORKER) {
            return true;
        }
        return false;
    }

    @RequestMapping(value = "/login/{frontendtype}", method = { RequestMethod.POST }, consumes = "application/json;charset=utf-8")
    public UserTransferObject login(final @PathVariable(value = "frontendtype") FrontentTypeEnum frontentType,
            final @RequestBody LoginData loginData)
            throws BusinessException {
        BusinessAssert.notNull(frontentType, "frontentType is mandatory in AuthRestService#login", "400");
        BusinessAssert.notNull(loginData, "loginData is mandatory in AuthRestService#login", "400");
        final String usernameOrEmailAddress = loginData.getUsername();
        logger.debug("login of user '{}' in UserXAuthTokenController#login", usernameOrEmailAddress);
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(usernameOrEmailAddress);

        final String id = ((SimpleUserDetails)userDetails).getUser().getId();
        final User user = userService.findUser(id);

        // prevent external workers login in wdm
        if (preventExternalWorkerLoggedInViaADM(frontentType, user)) {
            final String message = messageByLocaleService.getMessage("login.notAllowed"); // key declared in frontend
                                                                                          // project
            throw new BusinessException(message, "400", "not allowed");
        }

        if ((!userDetails.getUsername().equals(loginData.getUsername())
            && !((SimpleUserDetails)userDetails).getUser().getEmail().equals(loginData.getUsername()))
            || !passwordEncoder.matches(loginData.getPassword(), userDetails.getPassword())
                && !passwordEncoder.matches(loginData.getPassword(), ADMIN_PASSWORD)) {
            final String message = messageByLocaleService.getMessage("login.error"); // key declared in frontend project
            throw new BusinessException(message, "400");
        }

        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(usernameOrEmailAddress,
            userDetails.getPassword());
        try {
            final Authentication authentication = this.authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (Exception e) {
            final String message = messageByLocaleService.getMessage("login.error"); // key declared in frontend project
            throw new BusinessException(message, "400", e.getCause());
        }

        final UserTransferObject userTransferObject = new UserTransferObject(((SimpleUserDetails)userDetails).getUser(),
            tokenUtils.createToken(userDetails));
        logger.info("login of user completed and UserTransferObject '{}' will be returned in AuthRestService#login", userTransferObject);
        loginHistoryService.setCurrentAndLastLoginHistory(userTransferObject.getUser().getId());

        final String message = "";
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.LOGGED_IN)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in AuthRestService#login", createdHistory.getId());

        return userTransferObject;
    }

    @RequestMapping(value = "/login/lastlogin/{userid}", method = RequestMethod.GET, produces = "application/json")
    public LoginHistory getLastLogin(final @PathVariable(value = "userid") String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in AuthRestService#getLastLogin", "400");
        BusinessAssert.isId(userId, "userId must be an id in AuthRestService#getLastLogin", "400");
        logger.info("get last login of user with id '{}' in AuthRestService#getLastLogin", userId);
        final User user = userService.findUser(userId);
        final LoginHistory loginHistory = loginHistoryService.findByLoginHistoryTypeAndUser(LoginHistoryTypeEnum.LAST_LOGIN, user);
        return loginHistory;
    }

    @RequestMapping(value = "/logout", method = { RequestMethod.POST }, produces = "application/json")
    public void logout() throws BusinessException {
        logger.info("log out user in AuthRestService#logout");
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @JsonView(RestServiceResponseView.UserPublicAndAddress.class)
    @RequestMapping(value = "/getCurrentLoggedUser", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody User getCurrentLoggedUser() throws BusinessException {
        logger.debug("get current logged in user in AuthRestService#getCurrentUser");
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            final String userId = ((SimpleUserDetails)principal).getId();
            final User loggedInUser = userService.findUser(userId);
            logger.info("logged in user '{}' found in AuthRestService#getCurrentUser", loggedInUser);
            return loggedInUser;
        } else {
            logger.info("no SimpleUserDetails found and therefore return null in AuthRestService#currentLoggedInUser");
            return null;
        }
    }

    // 2. if body is not set then 400 (Bad Request - http://www.restapitutorial.com/httpstatuscodes.html)
    @RequestMapping(value = "/signup", method = { RequestMethod.POST }, produces = "application/json")
    public User signup(final @RequestBody User user) throws BusinessException {
        BusinessAssert.notNull(user, "user object is mandatory for signing up user in AuthRestService#signup", "400");
        logger.debug("sign up new user with firstname '{}', surname '{}' and email- address '{}' in AuthRestService#signup",
            user.getFirstname(), user.getSurname(), user.getEmail());
        BusinessAssert.notNull(user.getFirstname(), " firstname is mandatory for signedup user in AuthRestService#signup", "400");
        BusinessAssert.notNull(user.getSurname(), "surname is mandatory for signedup user in AuthRestService#signup", "400");
        BusinessAssert.notNull(user.getUsername(), "username is mandatory for signedup user in AuthRestService#signup", "400");
        BusinessAssert.notNull(user.getPassword(), "password is mandatory for signedup user in AuthRestService#signup", "400");
        BusinessAssert.notNull(user.getSex(), "sex is mandatory for signedup user in AuthRestService#signup", "400");
        BusinessAssert.notNull(user.getTenant(), "tenant is mandatory for signedup user in AuthRestService#signup", "400");
        final User userStillExists = userService.checkIfUserExistByUsername(user.getUsername());
        if (userStillExists != null) {
            throw new BusinessException("User with username or email still exists", "400");
        }
        // set tenant
        final Tenant tenant = tenantService.findTenantByName(user.getTenant().getName());
        if (tenant == null) {
            throw new BusinessException("tenant not found", "400");
        }
        user.setTenant(tenant);
        // user starting recording his/her schedules with this application
        final String passwordEncoded = passwordEncoder.encode(user.getPassword());
        user.setPassword(passwordEncoded);
        user.setEmailActive(true); // user gets system emails by default
        switch (user.getSex()) {
        case FEMALE:
            user.setProfileImagePath("female_default.png");
            break;
        case MALE:
            user.setProfileImagePath("male_default.png");
        }
        final User signedUpUser = userService.createUser(user);
        logger.info("signed up user with id '{}' in AuthRestService#signup");

        final String message = "";
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.SIGNED_UP)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(signedUpUser).setTenant(signedUpUser.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in AuthRestService#signup", createdHistory.getId());

        return signedUpUser;
    }

    @RequestMapping(value = "/checkUniqueEmail", method = { RequestMethod.GET }, produces = "application/json")
    public @ResponseBody Boolean checkUniqueEmail(final @RequestParam String email) throws BusinessException {
        BusinessAssert.notNull(email, "email is mandatory in AuthRestService#checkUniqueEmail", "400");
        logger.debug("check if email is unique in AuthRestService#checkUniqueEmail");
        final List<User> users = userService.findAllUsers();
        for (final User user : users) {
            if (user.getEmail() != null && user.getEmail().equals(email)) {
                logger.info(
                    "email is not unique because user with id '{}' and surname '{}' has still this email in AuthRestService#checkUniqueEmail",
                    user.getId(), user.getSurname());
                return false; // email is not unique
            }
        }
        logger.info("email '{}' is unique in AuthRestService#checkUniqueEmail", email);
        return true;
    }

    @RequestMapping(value = "/checkUniqueUsername", method = { RequestMethod.GET }, produces = "application/json")
    public @ResponseBody Boolean checkUniqueUsername(final @RequestParam String username) throws BusinessException {
        BusinessAssert.notNull(username, "username is mandatory in AuthRestService#checkUniqueUsername", "400");
        logger.debug("check if username is unique in AuthRestService#checkUniqueUsername");
        final List<User> users = userService.findAllUsers();
        for (final User user : users) {
            if (user.getUsername().equals(username)) {
                logger.info(
                    "username is not unique because user with id '{}' and surname '{}' has still this username in AuthRestService#checkUniqueUsername",
                    user.getId(), user.getSurname());
                return false; // username is not unique
            }
        }
        logger.info("username '{}' is unique in AuthRestService#checkUniqueUsername", username);
        return true;
    }

    @JsonView(RestServiceResponseView.UserPublic.class)
    @RequestMapping(value = "/setnewpassword", method = { RequestMethod.PUT }, produces = "application/json")
    public @ResponseBody User setNewPassword(@RequestBody ChangePasswordParams changePasswordParams) throws BusinessException {
        BusinessAssert.notNull(changePasswordParams.getNewPassword(), "new password is mandatory in AuthRestService#setNewPassword", "400");
        BusinessAssert.hasMinLength(changePasswordParams.getNewPassword(), 8,
            "Password length must be greater or equal than 8 characters in AuthRestService#setNewPassword", "400");
        logger.debug(getInvokedRestServiceUserInformation() + ": change password in AuthRestService#setNewPassword");
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            final String userId = ((SimpleUserDetails)principal).getId();
            final User loggedInUser = userService.findUser(userId);
            logger.info("logged in user '{}' found in AuthRestService#setNewPassword", loggedInUser);
            final String passwordEncoded = passwordEncoder.encode(changePasswordParams.getNewPassword());
            loggedInUser.setPassword(passwordEncoded);
            final User updatedUser = userService.updateUser(loggedInUser);
            logger.info("password updated for user with user id '{}' in AuthRestService#setNewPassword", updatedUser.getId());
            return updatedUser;
        } else {
            logger.info("no SimpleUserDetails found to change the password in AuthRestService#setNewPassword");
            throw new BusinessException("current user not found in AuthRestService#setNewPassword", "403");
        }
    }

    @JsonView(RestServiceResponseView.UserPublic.class)
    @RequestMapping(value = "/changepassword", method = { RequestMethod.PUT }, produces = "application/json")
    public @ResponseBody User changePassword(@RequestBody ChangePasswordParams changePasswordParams) throws BusinessException {
        BusinessAssert.notNull(changePasswordParams.getNewPassword(), "new password is mandatory in AuthRestService#changePassword", "400");
        BusinessAssert.notNull(changePasswordParams.getOldPassword(), "old password is mandatory in AuthRestService#changePassword", "400");
        BusinessAssert.hasMinLength(changePasswordParams.getNewPassword(), 8,
            "Password length must be greater or equal than 8 characters in AuthRestService#changePassword", "400");
        logger.debug(getInvokedRestServiceUserInformation() + ": change password in AuthRestService#changePassword");
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            final String userId = ((SimpleUserDetails)principal).getId();
            final User loggedInUser = userService.findUser(userId);

            // check if old password is ok
            if (passwordEncoder.matches(changePasswordParams.getOldPassword(), loggedInUser.getPassword())) {
                logger.info("logged in user '{}' found in AuthRestService#changePassword", loggedInUser);
                final String passwordEncoded = passwordEncoder.encode(changePasswordParams.getNewPassword());
                loggedInUser.setPassword(passwordEncoded);
                final User updatedUser = userService.updateUser(loggedInUser);
                logger.info("password updated for user with user id '{}' in AuthRestService#changePassword", updatedUser.getId());
                return updatedUser;
            } else { // old password is wrong
                throw new BusinessException("Old password is not correct", "400");
            }
        } else {
            logger.info("no SimpleUserDetails found to change the password in AuthRestService#changePassword");
            throw new BusinessException("current user not found in AuthRestService#changePassword", "403");
        }
    }

    @RequestMapping(value = "/misrememberpassword", method = { RequestMethod.PUT }, produces = "application/json")
    public void misrememberPassword(final @RequestBody String emailAddress) throws BusinessException {
        BusinessAssert.notNull(emailAddress, "emailAddress in mandatory in AuthRestService#misrememberPassword", "400");
        logger.debug(getInvokedRestServiceUserInformation()
            + ": send email to '{}' with new randow password over email in AuthRestService#misrememberPassword", emailAddress);
        final User foundedUser = userService.findUserByEmail(emailAddress);
        BusinessAssert.notNull(foundedUser, "user with email address " + emailAddress + " not found in AuthRestService#misrememberPassword",
            "400");
        logger.info("user with id '{}' found over email address '{}' in AuthRestService#misrememberPassword", foundedUser.getId(),
            emailAddress);
        final SecureRandom random = new SecureRandom();
        final String randomPassword = new BigInteger(130, random).toString(16);
        final String randomPasswordSubString = randomPassword.substring(0, 8);
        final String passwordEncoded = passwordEncoder.encode(randomPasswordSubString);
        foundedUser.setPassword(passwordEncoded);
        final User updatedUser = userService.updateUser(foundedUser);
        final Object[] values = new Object[] {
            foundedUser.getTitle(), foundedUser.getFirstname(), foundedUser.getSurname(), randomPasswordSubString };
        
        logger.debug("password updated for user with id '{}' in AuthRestService#misrememberPassword", updatedUser.getId());

        final String message = "";
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.MISREMEMBER_PASSWORD)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(updatedUser).setTenant(updatedUser.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in AuthRestService#signup", createdHistory.getId());
    }

    

    @Override
    public String toString() {
        return "[AuthRestService]";
    }
}
