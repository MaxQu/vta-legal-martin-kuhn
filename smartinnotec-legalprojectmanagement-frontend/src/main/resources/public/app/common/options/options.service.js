(function() {
	'use strict';
    
	angular
		.module('legalprojectmanagement.common')
		.factory('optionsService', optionsService);
    
	optionsService.$inject = ['$http', '$q', 'api_config'];
	
	function optionsService($http, $q, api_config) {
		var roles = false;
		var service = {		
			getCountryTypes: getCountryTypes,
			getRoles: getRoles,
			getAllTitles: getAllTitles,
			getProvinceTypes: getProvinceTypes,
			getProvinceTypesOfCountryType: getProvinceTypesOfCountryType,
			getUserRoles: getUserRoles,
			getCommunicationModes: getCommunicationModes,
			getCalendarEventSerialDateTypes: getCalendarEventSerialDateTypes,
			getProductTypes: getProductTypes,
			getContactTypes: getContactTypes,
			getUnityTypes: getUnityTypes,
			getProductAggregateConditionTypes: getProductAggregateConditionTypes,
			getActivityTypes: getActivityTypes,
			getFacilityDetailsTypes: getFacilityDetailsTypes,
			getCalendarEventTypes: getCalendarEventTypes
		};
		
		return service;
		
		////////////
		
		function getCountryTypes() {
			return $http.get(api_config.BASE_URL + '/options/countrytype');
		}
		
		function getRoles() {
			return $http.get(api_config.BASE_URL + '/options/roles');
		}
		
		function getAllTitles() {
			return $http.get(api_config.BASE_URL + '/options/option/titles');
		}
		
		function getProvinceTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/provincetypes');
		}
		
		function getProvinceTypesOfCountryType(countryType) {
			return $http.get(api_config.BASE_URL + '/options/option/provincetypes/' + countryType);
		}
		
		function getUserRoles() {
			return $http.get(api_config.BASE_URL + '/options/option/userroles');
		}
		
		function getCommunicationModes() {
			return $http.get(api_config.BASE_URL + '/options/option/communicationmodes');
		}
		
		function getCalendarEventSerialDateTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/calendareventserialdates');
		}
		
		function getProductTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/producttypes');
		}
		
		function getContactTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/contacttypes');
		}
		
		function getUnityTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/unitytype');
		}
		
		function getProductAggregateConditionTypes() {
			return $http.get(api_config.BASE_URL + '/options/option/productaggregateconditions');
		}
		
		function getActivityTypes(type) {
			return $http.get(api_config.BASE_URL + '/options/activitytype/' + type);
		}
		
		function getFacilityDetailsTypes() {
			return $http.get(api_config.BASE_URL + '/options/facilitydetails');
		}
		
		function getCalendarEventTypes() {
			return $http.get(api_config.BASE_URL + '/options/calendareventtypes');
		}
	}
})();