package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Fuel;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.FuelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("fuelService")
public class FuelServiceImpl extends AbstractService implements FuelService {

    @Autowired
    protected FuelRepository fuelRepository;

    @Override
    public List<Fuel> findAll(final Tenant tenant) throws BusinessException {
        return fuelRepository.findAllByTenant(tenant, new Sort(Sort.Direction.DESC, "date"));
    }

    @Override
    public Fuel save(Fuel fuel) {
        return fuelRepository.save(fuel);
    }

    @Override
    public List<Fuel> findPagedFuelsByTenant(final Tenant tenant, final Integer page) throws BusinessException {
        BusinessAssert.notNull(page, "page is mandatory in FuelServiceImpl#findPagedFuelsByTenant", "400");
        final Pageable pageable = new PageRequest(page, this.getFuelPageSize(), new Sort(Sort.Direction.DESC, "date"));
        return fuelRepository.findPagedByTenant(tenant, pageable);
    }

    @Override
    public Integer calculateAmountOfPages(final Tenant tenant) {
        final Integer amountOfFuels = fuelRepository.countByTenant(tenant);
        final Integer amountOfPageSizes = (amountOfFuels + this.getFuelPageSize() - 1) / this.getFuelPageSize();
        return amountOfPageSizes;
    }


    @Override
    public void deleteFuel(final User user, final String fuelId) throws BusinessException {

        Fuel fuel = fuelRepository.findOne(fuelId);

        if (fuel.getTenant().getId().equals(user.getTenant().getId())) {
            BusinessAssert.isNotAllowed("Wrong tenant - operation is not allowed", "403");
        }

        fuelRepository.delete(fuelId);
        logger.info("delete fuel with id '{}' in FuelServiceImpl#deleteFuel", fuelId);
    }


}
