(function() {
	'use strict';

	angular
		.module('legalprojectmanagement.lang')
		.config(configure);

	configure.$inject =['$translateProvider'];

	function configure($translateProvider) {
		$translateProvider.useStaticFilesLoader({
			files: [{
				prefix: 'app/lang/i18n/lang-',
				suffix: '.json'
			}]
		});
		
		$translateProvider.preferredLanguage('de_DE');
		$translateProvider.useSanitizeValueStrategy('escape');
		$translateProvider.fallbackLanguage('de_DE');
	}
})();