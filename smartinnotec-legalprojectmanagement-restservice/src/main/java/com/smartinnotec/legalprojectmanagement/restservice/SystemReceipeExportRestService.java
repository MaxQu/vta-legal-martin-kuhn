package com.smartinnotec.legalprojectmanagement.restservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Receipe;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeProduct;
import com.smartinnotec.legalprojectmanagement.dao.domain.ReceipeTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.RecipeContainer;
import com.smartinnotec.legalprojectmanagement.restservice.container.SystemReceipeExportCsvResponse;
import com.smartinnotec.legalprojectmanagement.service.ProductService;
import com.smartinnotec.legalprojectmanagement.service.ReceipeService;

@RestController
public class SystemReceipeExportRestService extends AbstractRestService {

    @Autowired
    private ReceipeService receipeService;
    @Autowired
    private ProductService productService;
    @Autowired
    private SystemReceipeExportCsvResponse systemReceipeExportCsvResponse;

    // http://localhost:8088/api/recipesexport/recipeexport
    @RequestMapping(value = "/recipesexport/recipeexport", produces = "text/csv")
    public void getReceipes(final HttpServletResponse response) throws BusinessException, IOException {
        logger.info("get receipes in SystemReceipeExportRestService#getReceipes");
        final List<Receipe> receipes = receipeService.findByReceipeType(ReceipeTypeEnum.STANDARD_MIXTURE);

        if (receipes != null && !receipes.isEmpty()) {
            Collections.sort(receipes, new Comparator<Receipe>() {

                public int compare(Receipe r1, Receipe r2) {
                    return r1.getDate().compareTo(r2.getDate());
                }
            });
        }

        final List<RecipeContainer> receipeContainers = new ArrayList<>();
        if (receipes != null && !receipes.isEmpty()) {
            for (final Receipe receipe : receipes) {
                final RecipeContainer receipeContainer = new RecipeContainer();
                receipeContainer.setDate(dateTimeFormatter.print(receipe.getDate()));

                final String productId = receipe.getProductId();
                final Product product = productService.findProduct(productId);

                if (product == null) {
                    continue;
                }

                final StringBuilder sb = new StringBuilder();
                sb.append("(").append(receipe.getReceipeProducts().size()).append(")").append("\n");
                for (final ReceipeProduct receipeProduct : receipe.getReceipeProducts()) {
                    sb.append("Produkt: ")
                        .append(receipeProduct.getProduct() != null ? receipeProduct.getProduct().getName() : "PRODUCT DELETED").append(",")
                        .append(" Prozent: ").append(receipeProduct.getPercentage()).append("%,").append(" Liter: ")
                        .append(receipeProduct.getLiter() == null ? "--" : receipeProduct.getLiter()).append(",").append(" Kilogramm: ")
                        .append(receipeProduct.getKilogram() == null ? "--" : receipeProduct.getKilogram()).append("Kg,")
                        .append(" Dichte: ").append(receipeProduct.getDensity() == null ? "--" : receipeProduct.getDensity()).append(", ")
                        .append("Information: ").append(receipeProduct.getInformation() == null ? "--" : receipeProduct.getInformation())
                        .append("\n");
                }
                receipeContainer.setProductName(product.getName() + " (" + product.getNumber() + ")");
                receipeContainer.setRecipeProducts(sb.toString());
                receipeContainer.setInformation(receipe.getInformation());
                receipeContainer.setAmountInKilo(receipe.getAmountInKilo());
                receipeContainer.setRecipeType(receipe.getReceipeType().toString());
                receipeContainer.setCreationDate(dateTimeFormatter.print(receipe.getCreationDate()));

                receipeContainers.add(receipeContainer);
            }
        }
        systemReceipeExportCsvResponse.writeReceipes(response.getWriter(), receipeContainers);
    }

    @Override
    public String toString() {
        return "[SystemReceipeExportRestService]";
    }
}
