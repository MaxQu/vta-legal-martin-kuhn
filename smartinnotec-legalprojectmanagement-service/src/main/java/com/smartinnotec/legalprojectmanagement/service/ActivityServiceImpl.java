package com.smartinnotec.legalprojectmanagement.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.IOUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.Activity;
import com.smartinnotec.legalprojectmanagement.dao.domain.ActivityDocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.repository.ActivityRepository;

@Service("activityService")
public class ActivityServiceImpl extends AbstractService implements ActivityService {

    @Autowired
    private ActivityFilesUploadConfigurationBean activityFilesUploadConfigurationBean;
    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private CalendarEventService calendarEventService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private UserService userService;
    @Autowired
    private FileUploadService fileUploadService;

    @Override
    public Integer count() throws BusinessException {
        logger.info("count activities in ActivityServiceImpl#count");
        final Long amount = activityRepository.count();
        return amount.intValue();
    }

    @Override
    public Activity create(final Activity activity) throws BusinessException {
        BusinessAssert.notNull(activity, "activity is mandatory in ActivityServiceImpl#create", "400");
        logger.info("create activity in ActivityServiceImpl#create");
        activity.setCreationDate(new DateTime());
        return activityRepository.insert(activity);
    }

    @Override
    public Activity update(final Activity activity) throws BusinessException {
        BusinessAssert.notNull(activity, "activity is mandatory in ActivityServiceImpl#update", "400");
        logger.info("update activity with id '{}' in ActivityServiceImpl#update", activity.getId());
        return activityRepository.save(activity);
    }

    @Override
    public Activity findById(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ActivityServiceImpl#findById", "400");
        BusinessAssert.isId(id, "id is mandatory in ActivityServiceImpl#findById", "400");
        logger.info("find activity by id '{}' in ActivityServiceImpl#findById", id);
        return activityRepository.findOne(id);
    }

    @Override
    public List<Activity> getActivitiesByCalendarEventId(final String calendarEventId) throws BusinessException {
        BusinessAssert.notNull(calendarEventId, "calendarEventId is mandatory in ActivityServiceImpl#getActivityByCalendarEventId", "400");
        BusinessAssert.isId(calendarEventId, "calendarEventId must be an id in ActivityServiceImpl#getActivityByCalendarEventId", "400");
        logger.info("get activity by calendarEventId '{}' in ActivityServiceImpl#getActivityByCalendarEventId", calendarEventId);
        return activityRepository.findByCalendarEventId(calendarEventId);
    }

    @Override
    public Activity getLastActivityOfCalendarEventId(final String calendarEventId) throws BusinessException {
        BusinessAssert.notNull(calendarEventId, "calendarEventId is mandatory in ActivityServiceImpl#getLastActivityOfCalendarEventId",
            "400");
        BusinessAssert.isId(calendarEventId, "calendarEventId must be an id in ActivityServiceImpl#getLastActivityOfCalendarEventId",
            "400");
        logger.info("get last activity of calendarEvent with id '{}' in ActivityServiceImpl#getLastActivityOfCalendarEventId",
            calendarEventId);
        return activityRepository.findFirst1ByCalendarEventIdOrderByCreationDateDesc(calendarEventId);
    }

    @Override
    public List<Activity> getByContactId(final String contactId) throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId is mandatory in ActivityServiceImpl#getByContactId", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ActivityServiceImpl#getByContactId", "400");
        logger.info("get activities of contact with id '{}' in ActivityServiceImpl#getByContactId", contactId);
        final List<Activity> activities = activityRepository.findByContactId(contactId);
        for (final Activity activity : activities) {
            final CalendarEvent calendarEvent = calendarEventService.findById(activity.getCalendarEventId());
            final Contact contact = contactService.findContactById(activity.getContactId());
            final User user = userService.findUser(activity.getUserId());
            activity.setCalendarEvent(calendarEvent);
            activity.setContact(contact);
            activity.setUser(user);
        }
        return activities;
    }

    @Override
    public List<Activity> getByContactIdInRange(final String contactId, final DateTime start, final DateTime end) throws BusinessException {
        BusinessAssert.notNull(contactId, "contactId is mandatory in ActivityServiceImpl#getByContactIdInRange", "400");
        BusinessAssert.isId(contactId, "contactId must be an id in ActivityServiceImpl#getByContactIdInRange", "400");
        BusinessAssert.notNull(start, "start is mandatory in ActivityServiceImpl#getByContactIdInRange", "400");
        BusinessAssert.notNull(end, "end is mandatory in ActivityServiceImpl#getByContactIdInRange", "400");
        logger.info("get activities of contact with id '{}' and start '{}' to end '{}' in ActivityServiceImpl#getByContactIdInRange",
            contactId, start, end);
        final List<Activity> activities = activityRepository.findByContactIdAndCalendarEventStartDateBetween(contactId, start, end);
        for (final Activity activity : activities) {
            final CalendarEvent calendarEvent = calendarEventService.findById(activity.getCalendarEventId());
            final Contact contact = contactService.findContactById(activity.getContactId());
            final User user = userService.findUser(activity.getUserId());
            activity.setCalendarEvent(calendarEvent);
            activity.setContact(contact);
            activity.setUser(user);
        }
        return activities;
    }

    @Override
    public List<Activity> findByCalendarEventStartDateBetween(final DateTime start, final DateTime end) throws BusinessException {
        BusinessAssert.notNull(start, "start is mandatory in ActivityServiceImpl#findByCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in ActivityServiceImpl#findByCalendarEventStartDateBetween", "400");
        logger.info("find activities by start '{}' and end '{}' in ActivityServiceImpl#findByCalendarEventStartDateBetween", start, end);
        return activityRepository.findByCalendarEventStartDateBetween(start, end);
    }

    @Override
    public List<Activity> findByCalendarEventIdAndCalendarEventStartDateBetween(final String calendarEventId, final DateTime start,
            final DateTime end)
            throws BusinessException {
        BusinessAssert.notNull(calendarEventId,
            "calendarEventId is mandatory in ActivityServiceImpl#findByCalendarEventIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.isId(calendarEventId,
            "calendarEventId must be an id in ActivityServiceImpl#findByCalendarEventIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(start, "start is mandatory in ActivityServiceImpl#findByCalendarEventIdAndCalendarEventStartDateBetween",
            "400");
        BusinessAssert.notNull(end, "end is mandatory in ActivityServiceImpl#findByCalendarEventIdAndCalendarEventStartDateBetween", "400");
        logger.info(
            "find activities by calendarEventId '{}', start '{}' and end '{}' in ActivityServiceImpl#findByCalendarEventStartDateBetween",
            calendarEventId, start, end);
        return activityRepository.findByCalendarEventIdAndCalendarEventStartDateBetween(calendarEventId, start, end);
    }

    @Override
    public List<Activity> findByUserIdAndCalendarEventStartDateBetween(final String userId, final DateTime start, final DateTime end)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in ActivityServiceImpl#findByUserIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.isId(userId, "userId must be an id in ActivityServiceImpl#findByUserIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(start, "start is mandatory in ActivityServiceImpl#findByUserIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(end, "end is mandatory in ActivityServiceImpl#findByUserIdAndCalendarEventStartDateBetween", "400");
        logger.info(
            "find activities by userId '{}', start '{}' and end '{}' in ActivityServiceImpl#findByUserIdAndCalendarEventStartDateBetween",
            userId, start, end);
        return activityRepository.findByUserIdAndCalendarEventStartDateBetween(userId, start, end);
    }

    @Override
    public List<Activity> findByUserIdAndContactIdAndCalendarEventStartDateBetween(final String userId, final String contactId,
            DateTime start, final DateTime end)
            throws BusinessException {
        BusinessAssert.notNull(userId,
            "userId is mandatory in ActivityServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.isId(userId, "userId must be an id in ActivityServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween",
            "400");
        BusinessAssert.notNull(contactId,
            "contactId is mandatory in ActivityServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.isId(contactId,
            "contactId must be an id in ActivityServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween", "400");
        BusinessAssert.notNull(start, "start is mandatory in ActivityServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween",
            "400");
        BusinessAssert.notNull(end, "end is mandatory in ActivityServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween",
            "400");
        logger.info(
            "find activities by userId '{}', contactId '{}', start '{}' and end '{}' in ActivityServiceImpl#findByUserIdAndContactIdAndCalendarEventStartDateBetween",
            userId, start, end);
        return activityRepository.findByUserIdAndContactIdAndCalendarEventStartDateBetween(userId, contactId, start, end);
    }

    @Override
    public List<Activity> findActivitiesBySearchString(final String searchString) throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in ActivityServiceImpl#findActivitiesBySearchString", "400");
        logger.info("find activities by search string '{}' in ActivityServiceImpl#findActivitiesBySearchString", searchString);
        final List<Activity> activities = activityRepository.findActivitiesBySearchString(searchString);
        return activities;
    }

    @Override
    public void delete(final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in ActivityServiceImpl#delete", "400");
        BusinessAssert.isId(id, "id must be an id in ActivityServiceImpl#delete", "400");
        logger.info("delete activity of id '{}' in ActivityServiceImpl#delete", id);
        activityRepository.delete(id);
    }

    @Override
    public ActivityDocumentFile uploadActivityFile(final MultipartFile multipartFile) throws BusinessException {
        logger.info("upload activity file for activity with id '{}' in ActivityServiceImpl#uploadActivityFile");
        final String activityFilesFolderPrefixName = activityFilesUploadConfigurationBean.getActivityFilesFolderPrefixName();
        final String activityFilesUploadPath = activityFilesUploadConfigurationBean.getActivityFilesUploadPath();

        final String filename = dateTimeFormatter2.print(System.currentTimeMillis()) + "__" + multipartFile.getOriginalFilename();
        final String dirName = File.separator + activityFilesUploadPath + File.separator + activityFilesFolderPrefixName;
        final File folder = new File(dirName);
        final File file = new File(dirName + File.separator + filename);
        if (!folder.exists()) {
            final boolean dirCreated = folder.mkdirs();
            logger.info("directory was created = '{}' in ActivityServiceImpl#uploadActivityFile", dirCreated);
        }
        try {
            final byte[] bytes = multipartFile.getBytes();
            final FileOutputStream fos = new FileOutputStream(file);
            final BufferedOutputStream stream = new BufferedOutputStream(fos);
            stream.write(bytes);
            fos.flush();
            stream.flush();
            fos.close();
            stream.close();
        } catch (Exception e) {
            logger.error("Exception '{}' in ActivityServiceImpl#uploadActivityFile", e.getMessage());
            throw new BusinessException(e.getMessage());
        }

        final ActivityDocumentFile activityDocumentFile = new ActivityDocumentFile();
        activityDocumentFile.setOriginalFileName(multipartFile.getOriginalFilename());
        activityDocumentFile.setFileName(filename);
        activityDocumentFile.setUrl(dirName);
        activityDocumentFile.setImage(fileUploadService.isImage(file));
        activityDocumentFile.setVersion("1.0");

        return activityDocumentFile;
    }

    @Override
    public void downloadActivityFile(final HttpServletResponse response, final String filename, final String ending)
            throws BusinessException {
        BusinessAssert.notNull(filename, "filename ist mandatory in ActivityServiceImpl#downloadActivityFile", "400");
        logger.info("download activity file with name '{}' in ActivityServiceImpl#downloadActivityFile", filename);

        final String activityFilesFolderPrefixName = activityFilesUploadConfigurationBean.getActivityFilesFolderPrefixName();
        final String activityFilesUploadPath = activityFilesUploadConfigurationBean.getActivityFilesUploadPath();

        final String filenameWithEnding = filename + (!ending.equals("NO_ENDING") ? "." + ending : "");

        final String dirName = File.separator + activityFilesUploadPath + File.separator + activityFilesFolderPrefixName;
        final File file = new File(dirName + File.separator + filenameWithEnding);

        try {
            final InputStream is = new FileInputStream(file);
            String fileType = Files.probeContentType(file.toPath());
            response.setContentType(fileType);
            response.setHeader("Content-disposition", "attachment; filename=\"" + filenameWithEnding + "\"");
            response.setContentLength((int)file.length());
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
            is.close();
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), "400");
        }
    }

    @Override
    public String toString() {
        return "[ActivityServiceImpl]";
    }
}
