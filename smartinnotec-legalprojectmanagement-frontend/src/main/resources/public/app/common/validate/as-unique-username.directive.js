(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('asUniqueUsername', asUniqueUsername);
    
    asUniqueUsername.$inject = ['validateService'];
    
	function asUniqueUsername(validateService) {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link
		};
		
		return directive;
		
		////////////

		//noinspection JSUnusedLocalSymbols
		function link(scope, element, attrs, ngModel) {
			ngModel.$parsers.unshift(parser);
			ngModel.$formatters.unshift(formatter);
			
			////////////
			
			function parser(value){
				if(value.length > 3) {
					validateService.checkUniqueUsername(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
			
			function formatter(value) {
				if(value.length > 3) {
					validateService.checkUniqueUsername(value).then(function (response) {
						ngModel.$setValidity('unique', response.data);
					});
				}
				return value;
			}
		}
	}
})();