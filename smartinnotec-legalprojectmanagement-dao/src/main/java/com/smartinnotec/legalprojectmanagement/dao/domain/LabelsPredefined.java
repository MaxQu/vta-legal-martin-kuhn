package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "LabelsPredefined")
public @Data class LabelsPredefined {

    @Id
    private String id;
    @TextIndexed(weight = 1)
    private String name;
    private LabelsPredefinedTypeEnum labelsPredefinedType;
    @DBRef
    private Tenant tenant;

}
