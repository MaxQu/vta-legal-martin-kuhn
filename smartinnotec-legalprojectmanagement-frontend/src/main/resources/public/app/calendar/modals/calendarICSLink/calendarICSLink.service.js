(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.calendar')
    	.factory('calendarICSLinkModalService', calendarICSLinkModalService);
        
    calendarICSLinkModalService.$inject = ['$modal', '$stateParams'];
    
    function calendarICSLinkModalService($modal, $stateParams) {
		var service = {
			showCalendarICSLinkModal: showCalendarICSLinkModal
		};		
		return service;
		
		////////////
				
		function showCalendarICSLinkModal(currentUser, calendarHandlingBean) {
			 var calendarICSLinkModal = $modal.open({
				controller: CalendarICSLinkModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	}, 
			    	calendarHandlingBean: function() {
			    		return calendarHandlingBean;
			    	}
			    }, 
				templateUrl: 'app/calendar/modals/calendarICSLink/calendarICSLink.modal.html'
			});
			return calendarICSLinkModal;
			
			function CalendarICSLinkModalController($modalInstance, $scope, currentUser, calendarHandlingBean) {
		    	var vm = this;
		    	vm.currentUser = currentUser;
		    	vm.calendarHandlingBean = calendarHandlingBean;
		    	
		    	vm.closeCalendarICSLinkModal = closeCalendarICSLinkModal;
		    	
		    	function closeCalendarICSLinkModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();