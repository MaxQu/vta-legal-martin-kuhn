(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.factory('dateUtilityService', dateUtilityService);
    
    dateUtilityService.$inject = [];
    
    function dateUtilityService() {
		var service = {
			longDatesAreEqual: longDatesAreEqual,
			convertToDateOrUndefined: convertToDateOrUndefined,
			convertToDateTimeOrUndefined: convertToDateTimeOrUndefined,
			isDateValid: isDateValid,
			isTimeValid: isTimeValid,
			isAmountOfTimeValid: isAmountOfTimeValid,
			isTimeBeforeOther: isTimeBeforeOther,
			isDateBeforeOrEqualOtherDate: isDateBeforeOrEqualOtherDate, 
			formatDateToString: formatDateToString,
			formatDateToDateTimeString: formatDateToDateTimeString,
			overlapping: overlapping,
			checkArrayConcerningOverlapping: checkArrayConcerningOverlapping,
			createDate: createDate,
			joinDateObjectsToDateTimeObject: joinDateObjectsToDateTimeObject,
			formatStartEndDateTime: formatStartEndDateTime, 
			oneMonthBack: oneMonthBack
		};
		
		return service;
		
		////////////

		function longDatesAreEqual(longDate1, longDate2) {
			var start = new Date(longDate1);
			var end = new Date(longDate2);
			
			var startDay = start.getDate();
			var startMonth = start.getMonth();
			var startYear = start.getFullYear();
			
			var endDay = end.getDate();
			var endMonth = end.getMonth();
			var endYear = end.getFullYear();
			
			if(startDay == endDay && startMonth == endMonth && startYear == endYear) {
				return true;
			}
			return false;
		}
		
		function convertToDateOrUndefined(dateString) {
    		if(dateString instanceof Date) { 
    			return dateString;
    		}
    		if(dateString == null) {
    			return undefined;
    		}
    		return new Date(dateString.replace(/(\d{2})\.(\d{2})\.(\d{4})/,'$3-$2-$1'));
    	}
		
		function convertToDateTimeOrUndefined(dateString) {
			if(dateString instanceof Date || dateString == null) { 
    			return undefined;
    		}
			return new Date(dateString.replace(/(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2})/,'$3-$2-$1 $4:$5'));
		}
		
		function isDateValid(dateString) {
			var regex_date =/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/;
			if(regex_date.test(dateString) === false) {
				return false;
			}
			var parts   = dateString.split(".");
			var day     = parseInt(parts[0], 10);
			var month   = parseInt(parts[1], 10);
			var year    = parseInt(parts[2], 10);
			if(year < 1000 || year > 3000 || month === 0 || month > 12) {
				return false;
			}
			var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
			if(year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
				monthLength[1] = 29;
			}
			return day > 0 && day <= monthLength[month - 1];
		}
		
		function isTimeValid(timeString) {
			if(timeString.startsWith('-')) {
				timeString = timeString.substring(1, timeString.length);
			}
			
			var regex_time =/^([0-9]{2})\:([0-9]{2})$/;
			if(regex_time.test(timeString) === false) {
				return false;
			}
			var parts = timeString.split(":");
			var hours = parseInt(parts[0], 10);
			var minutes = parseInt(parts[1], 10);
			if(hours >= 0 && hours < 24 && minutes >= 0 && minutes < 60) {
				return true;
			}
			return false;
		} 
		
		function isAmountOfTimeValid(amountOfTime) {
			if(amountOfTime.indexOf(':') == -1) {
				return false;
			}
			var pattern = new RegExp('^-?\\d{1,3}(:\\d{1,2})?$');
			var result = pattern.test(amountOfTime);
			return result;
		}
		
		function isTimeBeforeOther(time1, time2) {
			var t1 = parseFloat( time1.replace(':', '.') );
			var t2 = parseFloat( time2.replace(':', '.') );
			if(t1 < t2) {
				return true;
			}
			return false;
		}
		
		function isDateBeforeOrEqualOtherDate(date1, date2) {
			return date1 > date2;
		}
		
		function formatDateToString(date) {
		   var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
		   var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
		   var yyyy = date.getFullYear();
		   return (dd + "." + MM + "." + yyyy);
		}
		
		function formatDateToDateTimeString(date) {
			   var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
			   var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
			   var yyyy = date.getFullYear();
			   
			   var hh = (date.getHours() < 10 ? '0' : '') + date.getHours();
			   var mm = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
			   
			   return (dd + "." + MM + "." + yyyy + ' ' + hh + ':' + mm);
			}
		
		function overlapping(a, b) {
		    function getMinutes(s) {
		        var p = s.split(':').map(Number);
		        return p[0] * 60 + p[1];
		    }
		    return getMinutes(a.endTime) > getMinutes(b.startTime) && getMinutes(b.endTime) > getMinutes(a.startTime);
		}
		
		function checkArrayConcerningOverlapping(array) {
			var overlap = false;
			for(var n = 0; n < array.length; n++) {
				for(var i = 0; i < n; i++) {
				   var result = overlapping(array[n], array[i]);
				   if(result === true) {
					 overlap = result;
					 break;
				   }
				}
			}
			return overlap;
		}
		
		function createDate(input) {
    		if(typeof input != 'undefined') {
    			var dateString = input.split(' ')[1];
    			var dateTerms = dateString.split('.');
    			var initDate = new Date(dateTerms[2], dateTerms[1], dateTerms[0]);
    			return initDate;
    		}
        }
		
		function joinDateObjectsToDateTimeObject(dateObject, timeObject) {
    		var hours = timeObject.getHours();
    		var minutes = timeObject.getMinutes();
    		var seconds = timeObject.getSeconds();
    		dateObject.setHours(hours);
    		dateObject.setMinutes(minutes);
    		dateObject.setSeconds(seconds);
    		return dateObject;
    	}
		
		function formatStartEndDateTime(dateTimeFrom, dateTimeUntil) {
    		var dateAndTimeFrom = dateTimeFrom.substring(0, dateTimeFrom.indexOf(' '));
    		var dateAndTimeUntil = dateTimeUntil.substring(0, dateTimeUntil.indexOf(' '));
    		if(dateAndTimeFrom == dateAndTimeUntil) {
    			return dateAndTimeFrom + ': <b>' + dateTimeFrom.substring(dateTimeFrom.indexOf(' ')) + '</b> - ' + ' <b>' + dateTimeUntil.substring(dateTimeFrom.indexOf(' ')) + '</b>'; 
    		} else {
    			return dateAndTimeFrom + ': <b>' + dateTimeFrom.substring(dateTimeFrom.indexOf(' ')) + '</b> - ' + '<i>' + dateAndTimeUntil + '</i>: <b>' + dateTimeUntil.substring(dateTimeFrom.indexOf(' ')) + '</b>'; 
    		}
    	}
		
		function oneMonthBack(date) {
			var day = date.getDate();
			var month = date.getMonth();
			var year = date.getFullYear();
    		if(month > 1) {
    			month = month-1;
    		} else {
    			month = 11;
    			year = year-1;
    		}
    		return new Date(year, month, day);
    	}
	}
})();