package com.smartinnotec.legalprojectmanagement.service.schedule;

import java.util.List;
import java.util.Map;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEvent;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.service.container.CalendarEventOutsideWorkContainer;

public interface CalendarEventOutsideWorkScheduleService {

    void sendCalendarEventOutsideWorkRememberingMessage() throws BusinessException;

    String combineCalendarEventOutsideWorkContainerMessage(List<CalendarEventOutsideWorkContainer> calendarEventOutsideWorkContainers);

    String combineCalendarEventOutsideWorkContainerMessageForBoss(
            List<CalendarEventOutsideWorkContainer> calendarEventOutsideWorkContainers, final User user);

    Map<String, List<CalendarEventOutsideWorkContainer>> getCalendarEventOutsideWorkContainersOfUsersOneWeek(
            final List<CalendarEvent> calendarEvents) throws BusinessException;

    Map<String, List<CalendarEventOutsideWorkContainer>> getCalendarEventOutsideWorkContainersOfUsersTwoWeeks(
            final List<CalendarEvent> calendarEvents) throws BusinessException;

}
