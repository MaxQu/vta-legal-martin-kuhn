package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookCategoryTypeEnum;

public interface WorkingBookService {

    WorkingBook create(final User user, final WorkingBook workingBook) throws BusinessException;

    WorkingBook update(final WorkingBook workingBook) throws BusinessException;

    WorkingBook findById(final String id) throws BusinessException;

    List<WorkingBook> findByProject(final Project project) throws BusinessException;

    List<WorkingBook> findByProjectAndTenant(final Project project, final Tenant tenant) throws BusinessException;

    List<WorkingBook> findByProjectAndTenantAndDateTimeFromBetween(final Project project, final Tenant tenant, final DateTime start,
            final DateTime end) throws BusinessException;

    List<WorkingBook> findPagedWorkingBookByProjectAndTenantAndDateTimeFromBetween(final Integer page, final String sortType,
            final Project project, final Tenant tenant, final DateTime start, final DateTime end) throws BusinessException;

    List<WorkingBook> findWorkingBookByProjectAndTenantAndSearchString(final Project project, final Tenant tenant,
            final String searchString) throws BusinessException;

    Integer getWorkingBookPagesByProjectAndTenantAndDateTimeFromBetween(final Project project, final Tenant tenant, final DateTime start,
            final DateTime end) throws BusinessException;

    Integer getWorkingBookPageSize() throws BusinessException;

    Integer countWorkingBookByProjectAndTenantAndDateTimeFromBetween(final Project project, final Tenant tenant, final DateTime start,
            final DateTime end) throws BusinessException;

    WorkingBook findFirst1ByProjectAndTenantAndCategoryTypeOrderByCreationDateTimeDesc(final Project project, final Tenant tenant,
            final WorkingBookCategoryTypeEnum categoryType) throws BusinessException;

    void delete(final String workingBookEntryId) throws BusinessException;

}
