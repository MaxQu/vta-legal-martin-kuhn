(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.products')
    	.factory('createThinningModalService', createThinningModalService);
        
    createThinningModalService.$inject = ['$modal', '$stateParams', 'productsService', 'thinningService', 'dateUtilityService'];
    
    function createThinningModalService($modal, $stateParams, productsService, thinningService, dateUtilityService) { 
		var service = {
			showCreateThinningModal: showCreateThinningModal
		};		
		return service;
		
		////////////
				
		function showCreateThinningModal(currentUser, product, products, thinning, allThinningsOfProduct, userAuthorizationUserContainer, polymerPromille) {
			 var createThinningModal = $modal.open({
				controller: CreateThinningModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	product: function() {
			    		return product;
			    	},
			    	products: function() {
			    		return products;
			    	},
			    	thinning: function() {
			    		return thinning;
			    	},
			    	allThinningsOfProduct: function() {
			    		return allThinningsOfProduct;
			    	}, 
			    	userAuthorizationUserContainer: function() {
			    		return userAuthorizationUserContainer;
			    	}, 
			    	polymerPromille: function() {
			    		return polymerPromille;
			    	}
			    }, 
				templateUrl: 'app/products/modals/createThinningModal/createThinning.modal.html'
			});
			return createThinningModal;
			
			function CreateThinningModalController($modalInstance, $scope, currentUser, product, products, thinning, allThinningsOfProduct, polymerPromille) {
		    	var vm = this; 
		    	vm.currentUser = currentUser;
		    	vm.product = product;
		    	vm.products = products;
		    	vm.thinning = thinning;
		    	vm.allThinningsOfProduct = allThinningsOfProduct;
		    	vm.polymerPromille = polymerPromille;

		    	vm.closeCreateThinningModal = closeCreateThinningModal;  
		    	markProduct();
		    	
		    	////////////
		    	
		    	function markProduct() {
		    		vm.product.selected = true;
		    	}
		    	
		    	function closeCreateThinningModal() {
		    		vm.thinning.productId = '';
		    		vm.thinning.date = new Date();
		    		vm.thinning.ironContent = null;
		    		vm.thinning.targetContent = null;
		    		vm.thinning.density = null;
		    		vm.thinning.amount = null;
		    		vm.thinning.amountLiter = null;
		    		vm.thinning.iron = null;
		    		vm.thinning.water = null;
		    		vm.thinning.polymer = false;
		    		vm.thinning.ascorbinSaeure = false;
		    		vm.thinning.polymerSubtract = 1000;
		    		vm.thinning.ascorbinSaeureSubtract = 2700;
		    		vm.thinning.waterAmountCalculated = null;
		    		vm.thinning.information = null;
		    		vm.thinning.labelLeft1 = 'Fe- Gehalt';
		    		vm.thinning.labelLeft2 = 'Fe- Gehalt';
		    		vm.thinning.labelRight1 = 'Eisenlösung';
					$modalInstance.dismiss('cancel'); 
				}
			} 
		}
    }
})();