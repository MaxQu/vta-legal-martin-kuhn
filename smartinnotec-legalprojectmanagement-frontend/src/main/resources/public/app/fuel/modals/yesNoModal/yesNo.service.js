(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.fuel')
    	.factory('yesNoFuelService', yesNoFuelModalService);
        
    yesNoFuelModalService.$inject = ['$modal', '$stateParams'];
    
    function yesNoFuelModalService($modal, $stateParams) {
		var service = {
			showYesNoModal: showYesNoModal
		};		
		return service;
		
		////////////
				
		function showYesNoModal(invoker, headerContent, detailContent, yesFunction) {
			 var yesNoModal = $modal.open({
				controller: YesNoModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	invoker: function() {
			    		return invoker;
			    	},
			    	headerContent: function() {
			    		return headerContent;
			    	},
					detailContent: function() {
						return detailContent;
					},
					yesFunction: function() {
						return yesFunction;
					}
			    }, 
				templateUrl: 'app/fuel/modals/yesNoModal/yesNo.modal.html'
			});
			return yesNoModal;
			
			function YesNoModalController($modalInstance, $scope, invoker, headerContent, detailContent, yesFunction) {
		    	var vm = this; 
		    	vm.invoker = invoker;
		    	vm.headerContent = headerContent;
		    	vm.detailContent = detailContent;
		    	vm.yesFunction = yesFunction;

		    	vm.yes = yes;
		    	vm.no = no;
		    	vm.closeYesNoModal = closeYesNoModal;
		    	
		    	////////////
		    	function yes() {
		    		vm.yesFunction();
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function no() {
		    		vm.closeYesNoModal();
		    	}
		    	
		    	function closeYesNoModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
