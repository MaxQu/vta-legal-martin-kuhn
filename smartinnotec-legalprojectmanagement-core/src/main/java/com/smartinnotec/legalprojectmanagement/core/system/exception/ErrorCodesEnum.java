package com.smartinnotec.legalprojectmanagement.core.system.exception;

public enum ErrorCodesEnum {

    ENDTIME_BEFORE_STARTTIME("ENDTIME_BEFORE_STARTTIME"),
    ENTRYTIME_BEFORE_CURRENTTIME("ENTRYTIME_BEFORE_CURRENTTIME");

    private String value;

    private ErrorCodesEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
