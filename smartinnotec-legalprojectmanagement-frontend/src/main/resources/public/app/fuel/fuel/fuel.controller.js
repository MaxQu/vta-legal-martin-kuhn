(function () {
    'use strict';

    angular
        .module('legalprojectmanagement.fuel')
        .controller('FuelController', FuelController);


    FuelController.$inject = ['$scope', '$timeout', '$window', '$location', '$anchorScroll', 'currentUser', 'fuelService', 'fuelList', 'amountOfPages', 'fuelPageSize', 'editFuelModalService', 'yesNoFuelService'];


    function FuelController($scope, $timeout, $window, $location, $anchorScroll, currentUser, fuelService, fuelList, amountOfPages, fuelPageSize, editFuelModalService, yesNoFuelService) {

        $scope.vm = this;
        var vm = this;

        // TODO handle it
        vm.loadingInProgress = false;

        vm.searchTrigger = false;
        vm.fuelList = fuelList.data;
        vm.fuelPageSize = fuelPageSize.data;
        vm.amountOfPages = amountOfPages;
        vm.currentPage = 0;
        vm.currentUser = currentUser;
        // vm.userAuthorizationUserContainer = vm.currentUser.userAuthorizationUserContainer;
        vm.showFuelForm = false;
        vm.showEditFuelModal = showEditFuelModal;
        vm.printContainer = printContainer;
        vm.gotoTop = gotoTop;
        vm.saveFuel = saveFuel;
        vm.deleteFuel = deleteFuel;
        vm.searchFuels = searchFuels;
        vm.markFuelAsPaid = markFuelAsPaid;
        vm.showYesNoDeleteModal = showYesNoDeleteModal;
        vm.pageChange = pageChange;


        //////////////
        /*    $scope.$watch('vm.orderType', function(newValue, oldValue) {
                if(oldValue != newValue) {
                    removeSearchResults();
                }
            });
        */


        function errorCallback(response) {
            vm.failedUploadedFiles += 1;
        }


        function setTimeout() {
            $timeout(function () {
                vm.noLastReceipeExists = false;
            }, 2000);
        }

        function showEditFuelModal(fuel) {

            editFuelModalService.showEditFuelModal(fuel, vm, vm.currentUser, vm.roles, vm.usersOfProject);
        }

        function markFuelAsPaid(fuel) {
            if (!fuel.paid) {
                fuel.paid = true;
                vm.saveFuel(fuel);
            }
        }

        function printContainer(container, name) {
            printingPDFService.printSchedule(container, name);
        }

        function pageChange(page) {
            if (vm.currentPage != page) {
                vm.loadingInProgress = true;
                fuelService.findPagedFuels(page).then(function (result) {
                    vm.fuelList = result.data;
                }).finally(function () {
                    vm.loadingInProgress = false;
                    vm.currentPage = page;
                });
            }
        }


        function saveFuel(fuel) {
            var isCreate = !fuel.id;
            return fuelService.saveFuel(fuel).then(function () {
                if (isCreate) {
                    updateList();
                }
            });
        }

        function updateList() {
            fuelService.calculateAmountOfPages().then(function (result) {
                vm.amountOfPages = result.data;
            });

            fuelService.findPagedFuels(vm.currentPage).then(function (result) {
                vm.fuelList = result.data;
            });
        }

        function showYesNoDeleteModal(fuel) {
            var detailContentHmtl = 'Möchten Sie die Anlieferung mit dem Lieferschein "<b>' + fuel.deliveryNote + '</b>" löschen?';
            yesNoFuelService.showYesNoModal(this, 'Brennstoff Anlieferung <b>löschen</b>', detailContentHmtl, function () {
                console.log('!!!!YES called');
                deleteFuel(fuel)
            })
        }

        function deleteFuel(fuel) {
            fuelService.deleteFuel(fuel.id).then(function (response) {
                vm.updateList();
            }, function errorCallback(response) {
                console.log('error fuel.controller#deleteFuel');
            });
        }

        function removeFuel(fuel) {
            for (var i = 0; i < vm.fuel_list.length; i++) {
                if (vm.fuel_list[i].id == fuel.id) {
                    vm.fuel_list.splice(i, 1);
                    break;
                }
            }
        }

        function searchFuels() {
            console.log('Search not implemented yet');
        }

        function gotoTop() {
            $location.hash('top');
            $anchorScroll();
        }

    }
})();
