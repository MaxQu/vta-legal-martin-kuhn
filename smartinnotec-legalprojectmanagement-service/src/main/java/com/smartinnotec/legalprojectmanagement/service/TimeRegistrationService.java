package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import org.joda.time.DateTime;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.TimeRegistration;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface TimeRegistrationService {

    TimeRegistration findTimeRegistrationById(final String id) throws BusinessException;

    List<TimeRegistration> findTimeRegistrationsBySearchStringAndProjectAndUser(final String searchString, final Project project,
            final User user) throws BusinessException;

    List<TimeRegistration> findTimeRegistrationByProject(final Project project) throws BusinessException;

    List<TimeRegistration> findTimeRegistrationsOfProjectBetweenTimeRange(final Project project, final DateTime start, final DateTime end)
            throws BusinessException;

    String getTimeRegistrationsSumOfProjectBetweenTimeRange(final Project project, final DateTime start, final DateTime end)
            throws BusinessException;

    TimeRegistration create(final TimeRegistration timeRegistration) throws BusinessException;

    TimeRegistration update(final TimeRegistration timeRegistration) throws BusinessException;

    void delete(final String id) throws BusinessException;

}
