package com.smartinnotec.legalprojectmanagement.dao.domain;

import lombok.Data;

public @Data class VersionIdContainer {

    private String id;
    private String version;
}
