package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface HistoryRepository extends MongoRepository<History, String> {

    List<History> findByTenantAndDateBetween(final Tenant tenant, final DateTime start, final DateTime end);

    List<History> findPagedHistoryByTenantAndDateBetween(final Tenant tenant, final DateTime start, final DateTime end,
            final Pageable pageable);

    Integer countByTenantAndDateBetween(final Tenant tenant, final DateTime start, final DateTime end);

}
