(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.projects')
    	.factory('projectUserManagementModalService', projectUserManagementModalService);
        
    projectUserManagementModalService.$inject = ['$modal', '$stateParams', '$timeout', 'projectUserConnectionService'];
    
    function projectUserManagementModalService($modal, $stateParams, $timeout, projectUserConnectionService) {
		var service = {
			showProjectUserManagementModal: showProjectUserManagementModal
		};		
		return service;
		
		////////////
				
		function showProjectUserManagementModal(project, projectUserConnectionsOfProject, currentProjectUserConnections, projectUserConnectionRoles, currentUser, invoker) {
			 var projectUserManagementModal = $modal.open({
				controller: ProjectUserManagementModalController,
			    controllerAs: 'vm',
			    size: 'lg',
			    windowClass: "modal fade in",
			    resolve: {
			    	project: function() {
			    		return project;
			    	},
			    	projectUserConnectionsOfProject: function() {
			    		return projectUserConnectionsOfProject;
			    	},
			    	currentProjectUserConnections: function() {
			    		return currentProjectUserConnections;
			    	},
			    	projectUserConnectionRoles: function() {
			    		return projectUserConnectionRoles;
			    	},
			    	currentUser: function() {
			    		return currentUser;
			    	},
			    	invoker: function() {
			    		return invoker;
			    	}
			    }, 
				templateUrl: 'app/projects/modals/projectUserManagement/projectUserManagement.modal.html'
			});
			return projectUserManagementModal;
			
			function ProjectUserManagementModalController($modalInstance, $scope, project, projectUserConnectionsOfProject, currentProjectUserConnections, projectUserConnectionRoles, currentUser, invoker) {
		    	var vm = this; 
		    	vm.project = project;
		    	vm.projectUserConnectionsOfProject = projectUserConnectionsOfProject;
		    	vm.currentProjectUserConnections = currentProjectUserConnections;
		    	vm.projectUserConnectionRoles = projectUserConnectionRoles;
		    	vm.currentUser = currentUser;
		    	vm.invoker = invoker;
		    	vm.selectedUser = null;
		    	vm.userStillExist = false;
		    	vm.documentsAssignerError = false;
		    	
		    	vm.addUserToProject = addUserToProject;
		    	vm.removeUserFromProject = removeUserFromProject;
		    	vm.changeProjectUserConnectionRole = changeProjectUserConnectionRole;
		    	vm.findContactAndUserBySearchString = findContactAndUserBySearchString;
		    	vm.closeProjectUserManagementModal = closeProjectUserManagementModal;
		    	vm.userOrContactSelected = userOrContactSelected;
		    	
		    	////////////
		    	
		    	function setUpdateTimeout(projectUserConnection) {
		      		$timeout(function() {
		      			projectUserConnection.updateSuccessfully = false;
		  			   }, 2000); 
		      	}
		    	
		    	function setDeleteTimeout() {
		      		$timeout(function() {
		      			vm.documentsAssignerError = false;
		  			   }, 2000); 
		      	}
		    	
		    	function addUserToProject() {
		    		if(vm.selectedUser == null || typeof vm.selectedUser === 'undefined') {
		    			vm.selectedUser = null;
						return;
					  }
					if(userOrContactStillExists(vm.selectedUser)) {
						vm.userStillExist = true;
						vm.selectedUser = null;
						return;
					}  
		    		vm.messageGroupCreationError = false;
		    		var projectUserConnection = {};
		    		if(vm.selectedUser.contactUserType == 'CONTACT') {
		    			projectUserConnection.contact = vm.selectedUser;
		    		} else {
		    			projectUserConnection.user = vm.selectedUser;
		    		}
					
					projectUserConnection.project = vm.project;
					projectUserConnection.projectUserConnectionRole = vm.selectedProjectUserConnectionRole;
					projectUserConnectionService.createProjectUserConnection(projectUserConnection).then(function(response) {
						var projectUserConnection = response.data;
						vm.projectUserConnectionsOfProject.push(projectUserConnection);
						vm.selectedUser = null;
						vm.userStillExist = false;
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error projectsUserManagement.service#addUserToProject');
		   	   		 });	
		    	}
		    	
		    	function changeProjectUserConnectionRole(projectUserConnection) {
		    		projectUserConnectionService.updateProjectUserConnection(projectUserConnection).then(function(response) {
		    			projectUserConnection = response.data;
		    			for(var i = 0; i < currentProjectUserConnections.length; i++) {
		    				if(currentProjectUserConnections[i].id == projectUserConnection.id) {
		    					currentProjectUserConnections[i] = projectUserConnection;
		    				}
		    			}
		    			setUpdateFlagOfProjectUserConnection(projectUserConnection);
		   	   		 }, function errorCallback(response) {
		   	   			  console.log('error projectsUserManagement.service#addUserToProject');
		   	   		 });	
		    	}
		    	
		    	function setUpdateFlagOfProjectUserConnection(projectUserConnection) {
		    		for(var i = 0; vm.projectUserConnectionsOfProject.length; i++) {
		    			if(vm.projectUserConnectionsOfProject[i].id == projectUserConnection.id) {
		    				vm.projectUserConnectionsOfProject[i].updateSuccessfully = true;
		    				setUpdateTimeout(vm.projectUserConnectionsOfProject[i]);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function removeUserFromProject(projectUserConnectionOfProject) {
		    		projectUserConnectionService.deleteProjectUserConnection(projectUserConnectionOfProject.id).then(function(response) {
		    			removeProjectUserConnection(projectUserConnectionOfProject);
		   	   		 }, function errorCallback(response) {
		   	   			 if(response.data.error == 'ASSIGNED_DOCUMENTS_TO_USER') {
		   	   				 vm.documentsAssignerError = true;
		   	   				 setDeleteTimeout();
		   	   			 }
		   	   			  console.log('error projectsUserManagement.service#removeUserFromProject');
		   	   		 });	
		    	}
		    	
		    	function removeProjectUserConnection(projectUserConnectionOfProject) {
		    		for(var i = 0; vm.projectUserConnectionsOfProject.length; i++) {
		    			if(vm.projectUserConnectionsOfProject[i].id == projectUserConnectionOfProject.id) {
		    				vm.projectUserConnectionsOfProject.splice(i, 1);
		    				break;
		    			}
		    		}
		    	}
		    	
		    	function userOrContactSelected()  {
		    		if(vm.selectedUser.contactUserType == 'CONTACT') {
		    			vm.selectedProjectUserConnectionRole = 'CONTACT';
		    		}
		    	}
		    	
		    	function userOrContactStillExists(userOrContact) {
		    		for(var i = 0; i < vm.projectUserConnectionsOfProject.length; i++) {
		    			var user = vm.projectUserConnectionsOfProject[i].user;
		    			var contact = vm.projectUserConnectionsOfProject[i].contact;
		    			if(user != null) {
			    			if(vm.projectUserConnectionsOfProject[i].user.id === userOrContact.id) {
			    				return true;
			    			}
		    			} else if(contact != null) {
		    				if(vm.projectUserConnectionsOfProject[i].contact.id === userOrContact.id) {
			    				return true;
			    			}
		    			}
		    		}
		    		return false;
		    	}
		    	
		    	function findContactAndUserBySearchString(term) {
		    		return invoker.findContactAndUserBySearchString(term);
		    	}
		    	
		    	function closeProjectUserManagementModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();