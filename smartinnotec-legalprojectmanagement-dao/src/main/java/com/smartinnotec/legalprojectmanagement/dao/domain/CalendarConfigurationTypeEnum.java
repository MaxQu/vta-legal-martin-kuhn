package com.smartinnotec.legalprojectmanagement.dao.domain;

public enum CalendarConfigurationTypeEnum {

    HEADER("HEADER"),
    HEADER_END("HEADER_END"),
    MESSAGE("MESSAGE");

    private String value;

    private CalendarConfigurationTypeEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
