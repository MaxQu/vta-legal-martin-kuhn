package com.smartinnotec.legalprojectmanagement.service.container;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.dao.domain.CalendarEventUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.CommunicationUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.Contact;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.Folder;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnection;
import com.smartinnotec.legalprojectmanagement.dao.domain.TimeRegistration;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBook;
import com.smartinnotec.legalprojectmanagement.service.ApplicationCategorieEnum;
import com.smartinnotec.legalprojectmanagement.service.SearchTypeEnum;

import lombok.Data;

public @Data class SearchResult {

    private ApplicationCategorieEnum applicationCategorie;
    private ProjectUserConnection projectUserConnection;
    private DocumentFile documentFile;
    private Folder folder;
    private CalendarEventUserConnection calendarEventUserConnection;
    private Contact contact;
    private WorkingBook workingBook;
    private Product product;
    private CommunicationUserConnection communicationUserConnectionCreated;
    private CommunicationUserConnection communicationUserConnectionReceived;
    private TimeRegistration timeRegistration;
    private List<String> additionalInformation;
    private SearchTypeEnum searchType;
}
