package com.smartinnotec.legalprojectmanagement.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationRoleAccess;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationStateEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationTypeEnum;

public interface UserAuthorizationRoleAccessRepository extends MongoRepository<UserAuthorizationRoleAccess, String> {

    UserAuthorizationRoleAccess findByProjectUserConnectionRoleAndUserAuthorizationStateAndUserAuthorizationType(
            final ProjectUserConnectionRoleEnum projectUserConnectionRole, final UserAuthorizationStateEnum userAuthorizationState,
            final UserAuthorizationTypeEnum userAuthorizationType);

}
