package com.smartinnotec.legalprojectmanagement.restservice;

import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonView;
import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.UserAuthorizationUserContainer;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;
import com.smartinnotec.legalprojectmanagement.service.FileHandlingService;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.ProfileImageHandlingBean;
import com.smartinnotec.legalprojectmanagement.service.ProjectUserConnectionService;
import com.smartinnotec.legalprojectmanagement.service.UserAuthorizationUserContainerService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class UserRestService extends AbstractRestService {

    @Autowired
    private UserService userService;
    @Autowired
    private ProfileImageHandlingBean profileImageHandlingBean;
    @Autowired
    private FileHandlingService fileHandlingService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserAuthorizationUserContainerService userAuthorizationUserContainerService;
    @Autowired
    private ProjectUserConnectionService projectUserConnectionService;

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<User> findAllUsers() throws BusinessException {
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        final List<User> users = userService.findAllUsersByTenant(user.getTenant());
        userService.removeSuperAdmin(users);
        logger.info(getInvokedRestServiceUserInformation() + ": got '{}' users in UserRestService#users", users.size());
        return users;
    }

    @JsonView(RestServiceResponseView.UserPublicAndAddress.class)
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody User get(@PathVariable
    final String id) throws BusinessException {
        BusinessAssert.notNull(id, "id must be not null in UserRestService#get", "400");
        final User foundedUser = userService.findUser(id);
        BusinessAssert.notNull(foundedUser, "user with id '" + id + "' not found in UserRestService#get", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": user with id '{}' found in UserRestService#get", foundedUser.getId());
        return foundedUser;
    }

    @JsonView(RestServiceResponseView.UserPublicAndAddress.class)
    @RequestMapping(value = "/users/{searchString}/search", method = { RequestMethod.GET }, produces = "application/json")
    public @ResponseBody List<User> findUserBySearchString(final @PathVariable("searchString") String searchString)
            throws BusinessException {
        BusinessAssert.notNull(searchString, "searchString is mandatory in UserRestService#findUserBySearchString", "400");
        logger.info(
            getInvokedRestServiceUserInformation() + ": search user by search string '{}' in UserRestService#findUserBySearchString",
            searchString);
        // user which invokes this rest service
        final List<User> foundedUsers = userService.findUserBySearchString(searchString);
        logger.info("found '{}' users in UserRestService#findUserBySearchString", foundedUsers.size());
        return foundedUsers;
    }

    @RequestMapping(value = "/user/confidentialdocumentfiles/{userid}", method = { RequestMethod.GET })
    public boolean canUserSeeConfidentialDocumentFiles(final @PathVariable("userid") String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in UserRestService#canUserSeeConfidentialDocumentFiles", "400");
        BusinessAssert.isId(userId, "userId must be an id in UserRestService#canUserSeeConfidentialDocumentFiles", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": can user with id '{}' see confidential documentFiles in UserRestService#canUserSeeConfidentialDocumentFiles", userId);
        final User user = userService.findUser(userId);
        return projectUserConnectionService.canProjectUserConnectionRoleSeeConfidentialDocumentFiles(user.getProjectUserConnectionRole());
    }

    @RequestMapping(value = "/users", method = { RequestMethod.POST }, produces = "application/json")
    public @ResponseBody String create(final @RequestBody User user) throws BusinessException {
        BusinessAssert.notNull(user, "user is mandatory in UserRestService#create", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": create user in UserRestService#create");
        switch (user.getSex()) {
        case FEMALE:
            user.setProfileImagePath("female_default.png");
            break;
        case MALE:
            user.setProfileImagePath("male_default.png");
        }
        final User createdUser = userService.createUser(user);
        logger.info("created user with id '{}' in UserRestService#create", createdUser.getId());

        final String title = createdUser.getTitle() != null ? createdUser.getTitle() : "";
        final String message = title + " " + createdUser.getFirstname() + " " + createdUser.getSurname();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.CREATED_USER)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in UserRestService#create", createdHistory.getId());

        return createdUser.getId();
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public void delete(final @PathVariable String id) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in UserRestService#delete", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": deleted user with id '{}' in UserRestService#delete", id);
        final User userToDelete = userService.findUser(id);
        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);
        final String title = userToDelete.getTitle() != null ? userToDelete.getTitle() : "";
        final String message = title + " " + userToDelete.getFirstname() + " " + userToDelete.getSurname();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DELETED_USER)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in UserServiceImpl#deleteUser", createdHistory.getId());

        userService.deleteUser(id);
    }

    @JsonView(RestServiceResponseView.UserPublicAndAddress.class)
    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public @ResponseBody User update(final @PathVariable String id, final @RequestBody User user) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in UserRestService#update", "400");
        BusinessAssert.isId(id, "id must be an id in UserRestService#update", "400");
        BusinessAssert.notNull(user, "user is mandatory in UserRestService#update", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": update user with id '{}' in UserRestService#update", id);

        final User foundedUser = userService.findUser(user.getId());
        user.setPassword(foundedUser.getPassword());

        final User updatedUser = userService.updateUser(user);

        final String title = updatedUser.getTitle() != null ? updatedUser.getTitle() : "";
        final String message = title + " " + updatedUser.getFirstname() + " " + updatedUser.getSurname();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.UPDATED_USER)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in UserRestService#update", createdHistory.getId());

        return updatedUser;
    }

    @JsonView(RestServiceResponseView.UserPublicAndAddress.class)
    @RequestMapping(value = "/users/{id}/password", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public @ResponseBody User updatePassword(final @PathVariable String id, final @RequestBody String password) throws BusinessException {
        BusinessAssert.notNull(id, "id is mandatory in UserRestService#update", "400");
        BusinessAssert.isId(id, "id must be an id in UserRestService#update", "400");
        BusinessAssert.hasMinLength(password, 8, "password must be of min length of eight in UserRestService#updatePassword", "400");
        logger.info(getInvokedRestServiceUserInformation() + ": update password for user with id '{}' in UserRestService#updatePassword",
            id);
        final User user = userService.findUser(id);
        final String passwordEncoded = passwordEncoder.encode(password);
        user.setPassword(passwordEncoded);

        final String title = user.getTitle() != null ? user.getTitle() : "";
        final String message = title + " " + user.getFirstname() + " " + user.getSurname();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.UPDATED_USER_CHANGED_PASSWORD)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in UserRestService#updatePassword", createdHistory.getId());

        return userService.updateUser(user);
    }

    @JsonView(RestServiceResponseView.UserPublic.class)
    @RequestMapping(value = "/users/profile/upload/{userid}", method = RequestMethod.POST)
    public @ResponseBody User handleProfileImageUpload(@PathVariable(value = "userid")
    final String userId, @RequestParam("file") MultipartFile multipartFile) throws BusinessException {

        BusinessAssert.notNull(userId, "userId is mandatory in UserRestService#handleProfileImageUpload", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": handle profile image upload for user with id '{}' in UserRestService#handleProfileImageUpload", userId);
        final User user = userService.findUser(userId);
        final String originalFilename = multipartFile.getOriginalFilename();
        if (!multipartFile.isEmpty()) {
            try {
                final String folderName = profileImageHandlingBean.getProfileFolderPrefixName();
                final String dirName = profileImageHandlingBean.getProfileUploadPath() + "/" + folderName;
                final String absolutePath = fileHandlingService.storeFile(dirName, originalFilename, multipartFile);
                logger.info("profile image uploaded to path '{}' in UserRestService#handleProfileImageUpload", absolutePath);
                user.setProfileImagePath(originalFilename);
                final User updatedUser = userService.updateUser(user);
                logger.info("profile image path updated for user with id '{}' and surname '{}' in UserRestService#handleProfileImageUpload",
                    user.getId(), user.getSurname());

                return updatedUser;
            } catch (Exception e) {
                logger.error("error '{}' in UserRestService#handleProfileImageUpload: " + e.getMessage());
            }
        }
        return null;
    }

    @RequestMapping(value = "/users/userauthorizationusercontainer/{userid}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody UserAuthorizationUserContainer getUserAuthorizationUserContainerOfUser(@PathVariable(value = "userid")
    final String userId) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in UserRestService#getUserAuthorizationUserContainerOfUser", "400");
        BusinessAssert.isId(userId, "userId must be an id in UserRestService#getUserAuthorizationUserContainerOfUser", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": get userAuthorizationUserContainer of user with id '{}' in UserRestService#getUserAuthorizationUserContainerOfUser",
            userId);
        final UserAuthorizationUserContainer userAuthorizationUserContainer = userAuthorizationUserContainerService
            .findUserAuthorizationUserContainerByUserId(userId);

        if (userAuthorizationUserContainer != null && userAuthorizationUserContainer.getUserAuthorizations() != null
            && !userAuthorizationUserContainer.getUserAuthorizations().isEmpty()) {
            Collections.sort(userAuthorizationUserContainer.getUserAuthorizations());
        }
        return userAuthorizationUserContainer;
    }

    @RequestMapping(value = "/users/userauthorizationusercontainer/{userid}/{projectuserconnectionrole}", method = {
        RequestMethod.POST }, produces = "application/json")
    public @ResponseBody UserAuthorizationUserContainer createUserAuthorizationUserContainer(
            final @PathVariable(value = "userid") String userId,
            final @PathVariable(value = "projectuserconnectionrole") ProjectUserConnectionRoleEnum projectUserConnectionRole)
            throws BusinessException {
        BusinessAssert.notNull(projectUserConnectionRole,
            "projectUserConnectionRole is mandatory in UserRestService#createUserAuthorizationUserContainer", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": create userAuthorizationUserContainer of projectUserConnectionRole '{}' in UserRestService#createUserAuthorizationUserContainer",
            projectUserConnectionRole);
        final UserAuthorizationUserContainer userAuthorizationUserContainer = userAuthorizationUserContainerService.create(userId,
            projectUserConnectionRole);

        final String loggedInUserId = super.getLoggedInUserId();
        final User user = userService.findUser(loggedInUserId);

        final User changedUser = userService.findUser(userId);
        changedUser.setUserAuthorizationUserContainer(userAuthorizationUserContainer);
        changedUser.setProjectUserConnectionRole(projectUserConnectionRole);
        final User updatedUser = userService.updateUser(changedUser);
        logger.info("updated user with id '{}' in UserRestService#createUserAuthorizationUserContainer", updatedUser.getId());

        final UserAuthorizationUserContainer foundedUserAuthorizationUserContainer = userAuthorizationUserContainerService
            .findUserAuthorizationUserContainerByUserId(updatedUser.getId());

        if (foundedUserAuthorizationUserContainer != null) {
            final UserAuthorizationUserContainer updatedUserAuthorizationUserContainer = userAuthorizationUserContainerService
                .update(userAuthorizationUserContainer);
            logger.info("updated userAuthorizationUserContainer with id '{}' in UserRestService#createUserAuthorizationUserContainer",
                updatedUserAuthorizationUserContainer.getId());
        } else {
            final UserAuthorizationUserContainer createdUserAuthorizationUserContainer = userAuthorizationUserContainerService
                .create(userAuthorizationUserContainer);
            logger.info("created userAuthorizationUserContainer with id '{}' in UserRestService#createUserAuthorizationUserContainer",
                createdUserAuthorizationUserContainer.getId());
        }

        final String title = changedUser.getTitle() != null ? changedUser.getTitle() : "";
        final String message = title + " " + changedUser.getFirstname() + " " + changedUser.getSurname();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.CREATED_USER_AUTHORIZATION_USER_CONTAINER)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in UserRestService#createUserAuthorizationUserContainer", createdHistory.getId());

        return userAuthorizationUserContainer;
    }

    @RequestMapping(value = "/users/userauthorizationusercontainer", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public @ResponseBody UserAuthorizationUserContainer updateUserAuthorizationUserContainerOfUser(
            final @RequestBody UserAuthorizationUserContainer userAuthorizationUserContainer)
            throws BusinessException {
        BusinessAssert.notNull(userAuthorizationUserContainer,
            "userAuthorizationUserContainer is mandatory in UserRestService#updateUserAuthorizationUserContainerOfUser", "400");
        logger.info(
            getInvokedRestServiceUserInformation()
                + ": update userAuthorizationUserContainer with id '{}' in UserRestService#updateUserAuthorizationUserContainerOfUser",
            userAuthorizationUserContainer.getId());

        final String userId = super.getLoggedInUserId();
        final User user = userService.findUser(userId);

        final String userIdOfChangedUser = userAuthorizationUserContainer.getUserId();
        final User changedUser = userService.findUser(userIdOfChangedUser);

        final UserAuthorizationUserContainer updatedUserAuthorizationUserContainer = userAuthorizationUserContainerService
            .update(userAuthorizationUserContainer);

        final String title = changedUser.getTitle() != null ? changedUser.getTitle() : "";
        final String message = title + " " + changedUser.getFirstname() + " " + changedUser.getSurname();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.UPDATED_USER_AUTHORIZATION_USER_CONTAINER)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in UserRestService#updateUserAuthorizationUserContainerOfUser", createdHistory.getId());

        return updatedUserAuthorizationUserContainer;
    }

    @RequestMapping(value = "/users/userauthorizationusercontainer/{userid}/{id}", method = RequestMethod.DELETE)
    public void deleteUserAuthorizationUserContainerOfUser(final @PathVariable(value = "userid") String userId,
            final @PathVariable(value = "id") String id)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in UserRestService#deleteUserAuthorizationUserContainerOfUser", "400");
        BusinessAssert.isId(userId, "userId must be an id in UserRestService#deleteUserAuthorizationUserContainerOfUser", "400");
        BusinessAssert.notNull(id, "id is mandatory in UserRestService#deleteUserAuthorizationUserContainerOfUser", "400");
        BusinessAssert.isId(id, "id must be an id in UserRestService#deleteUserAuthorizationUserContainerOfUser", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": delete userAuthorizationUserContainer with id '{}' of user with id '{}' in UserRestService#deleteUserAuthorizationUserContainerOfUser",
            id, userId);

        final String loggedInUserId = super.getLoggedInUserId();
        final User user = userService.findUser(loggedInUserId);
        final User changedUser = userService.findUser(userId);
        changedUser.setProjectUserConnectionRole(null);
        userService.updateUser(changedUser);

        final String title = changedUser.getTitle() != null ? changedUser.getTitle() : "";
        final String message = title + " " + changedUser.getFirstname() + " " + changedUser.getSurname();
        final History history = new History.HistoryBuilder().setHistoryType(HistoryTypeEnum.DELETED_USER_AUTHORIZATION_USER_CONTAINER)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(new DateTime(System.currentTimeMillis())).setMessage(message)
            .setUser(user).setTenant(user.getTenant()).build();
        final History createdHistory = historyService.create(history);
        logger.info("created history with id '{}' in UserRestService#deleteUserAuthorizationUserContainerOfUser", createdHistory.getId());

        userAuthorizationUserContainerService.delete(id);
    }

    @Override
    public String toString() {
        return "[UserRestService]";
    }
}
