(function() {
    'use strict'; 
    
    angular
    	.module('legalprojectmanagement.history')
    	.factory('historyService', historyService);
        
    historyService.$inject = ['$http', 'api_config'];
    
    function historyService($http, api_config) {
		var service = {
			findHistoryBetweenDates: findHistoryBetweenDates,
			findPagedHistoryBetweenDates: findPagedHistoryBetweenDates,
			getHistoryPageSize: getHistoryPageSize,
			calculateAmountOfPages: calculateAmountOfPages,
			sendHistoryMessages: sendHistoryMessages
		};
		
		return service;
		
		////////////
		
		function findHistoryBetweenDates(startDate, endDate) {
			return $http.get(api_config.BASE_URL + '/histories/history/' + startDate + '/' + endDate + '/');
		}
		
		function findPagedHistoryBetweenDates(startDate, endDate, page) {
			return $http.get(api_config.BASE_URL + '/histories/history/' + startDate + '/' + endDate + '/' + page);
		}
		
		function getHistoryPageSize() {
			return $http.get(api_config.BASE_URL + '/histories/history/historypagesize');
		}
		
		function calculateAmountOfPages(start, end) {
			return $http.get(api_config.BASE_URL + '/histories/history/amountofpages/' + start + '/' + end + '/');
		}
		
		function sendHistoryMessages() {
			return $http.get(api_config.BASE_URL + '/histories/history/send');
		}
    }
})();
