package com.smartinnotec.legalprojectmanagement.dao.domain;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "Receipe")
public @Data class Receipe implements Comparable<Receipe> {

    @Id
    private String id;
    private String productId;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime date;
    @DBRef
    private Contact contact;
    private String contactText;
    private String purchaseOrderNumber;
    private List<ReceipeProduct> receipeProducts;
    private List<ReceipeAdditional> receipeAdditionals;
    private String information;
    private Integer amountInKilo;
    private List<ReceipeAttachment> receipeAttachments;
    private ReceipeTypeEnum receipeType;
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime creationDate;
    @DBRef
    private Tenant tenant;

    @Override
    public int compareTo(final Receipe receipe) {
        return this.date.compareTo(receipe.date);
    }
}
