package com.smartinnotec.legalprojectmanagement.dao.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.smartinnotec.legalprojectmanagement.dao.objectview.RestServiceResponseView;
import com.smartinnotec.legalprojectmanagement.serializer.DateSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateTimeSerializer;
import com.smartinnotec.legalprojectmanagement.serializer.DateToDateDeserializer;

import lombok.Data;

@Document(collection = "Project")
public @Data class Project {

    @Id
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    private String id;
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @TextIndexed(weight = 2)
    private String name;
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @TextIndexed
    private String description;
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime creationDate;
    private String creationUserId;
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime start;
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    @JsonDeserialize(using = DateToDateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private DateTime end;
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    private String color;
    @DBRef
    private Tenant tenant;
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    private String logoName;
    @JsonView(RestServiceResponseView.ProjectPublic.class)
    private Boolean generalAvailable; // fuer alle sichtbar

    public Project() {
        generalAvailable = false;
    }
}
