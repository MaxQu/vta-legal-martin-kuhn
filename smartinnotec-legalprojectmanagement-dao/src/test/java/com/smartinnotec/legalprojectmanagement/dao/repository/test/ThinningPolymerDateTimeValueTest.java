package com.smartinnotec.legalprojectmanagement.dao.repository.test;

import org.junit.Assert;
import org.junit.Test;

import com.smartinnotec.legalprojectmanagement.dao.domain.ThinningPolymerDateTimeValue;

public class ThinningPolymerDateTimeValueTest extends AbstractDAOTest {

    @Test
    public void shouldCreateTenant() {
        final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue1 = new ThinningPolymerDateTimeValue();
        thinningPolymerDateTimeValue1.setDateTime(dateTimeFormatter.parseDateTime("01.02.2018"));
        thinningPolymerDateTimeValue1.setValue("0.00332");
        final ThinningPolymerDateTimeValue createdThinningPolymerDateTimeValue1 = thinningPolymerDateTimeValueRepository
            .insert(thinningPolymerDateTimeValue1);
        Assert.assertNotNull(createdThinningPolymerDateTimeValue1);

        final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue2 = new ThinningPolymerDateTimeValue();
        thinningPolymerDateTimeValue2.setDateTime(dateTimeFormatter.parseDateTime("08.02.2018"));
        thinningPolymerDateTimeValue2.setValue("0.00331");
        final ThinningPolymerDateTimeValue createdThinningPolymerDateTimeValue2 = thinningPolymerDateTimeValueRepository
            .insert(thinningPolymerDateTimeValue2);
        Assert.assertNotNull(createdThinningPolymerDateTimeValue2);

        final ThinningPolymerDateTimeValue thinningPolymerDateTimeValue3 = new ThinningPolymerDateTimeValue();
        thinningPolymerDateTimeValue3.setDateTime(dateTimeFormatter.parseDateTime("02.02.2018"));
        thinningPolymerDateTimeValue3.setValue("0.00330");
        final ThinningPolymerDateTimeValue createdThinningPolymerDateTimeValue3 = thinningPolymerDateTimeValueRepository
            .insert(thinningPolymerDateTimeValue3);
        Assert.assertNotNull(createdThinningPolymerDateTimeValue3);
    }

}
