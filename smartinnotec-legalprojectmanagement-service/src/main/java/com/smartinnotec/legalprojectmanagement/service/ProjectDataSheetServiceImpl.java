package com.smartinnotec.legalprojectmanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectDataSheet;
import com.smartinnotec.legalprojectmanagement.dao.repository.ProjectDataSheetRepository;

@Service("projectDataSheetService")
public class ProjectDataSheetServiceImpl extends AbstractService implements ProjectDataSheetService {

    @Autowired
    private ProjectDataSheetRepository projectDataSheetRepository;

    @Override
    public ProjectDataSheet create(final ProjectDataSheet projectDataSheet) throws BusinessException {
        BusinessAssert.notNull(projectDataSheet, "projectDataSheet is mandatory in ProjectDataSheetServiceImpl#create", "400");
        logger.info("create projectDataSheet in ProjectDataSheetServiceImpl#create");
        final ProjectDataSheet createdProjectDataSheet = projectDataSheetRepository.insert(projectDataSheet);
        logger.debug("created projectDataSheet with id '{}' in  ProjectDataSheetServiceImpl#create", createdProjectDataSheet.getId());
        return projectDataSheet;
    }

    @Override
    public ProjectDataSheet update(final ProjectDataSheet projectDataSheet) throws BusinessException {
        BusinessAssert.notNull(projectDataSheet, "projectDataSheet is mandatory in ProjectDataSheetServiceImpl#update", "400");
        logger.info("update projectDataSheet with id '{}' in ProjectDataSheetServiceImpl#create", projectDataSheet.getId());
        return projectDataSheetRepository.save(projectDataSheet);
    }

    @Override
    public ProjectDataSheet findProjectDataSheetByProjectId(final String projectId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in ProjectDataSheetServiceImpl#findProjectDataSheetByProjectId", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in ProjectDataSheetServiceImpl#findProjectDataSheetByProjectId", "400");
        logger.info("find projectDataSheet of project with id '{}' in ProjectDataSheetServiceImpl#findProjectDataSheetByProjectId",
            projectId);
        return projectDataSheetRepository.findByProjectId(projectId);
    }

    @Override
    public void delete(String projectId) throws BusinessException {
        BusinessAssert.notNull(projectId, "projectId is mandatory in ProjectDataSheetServiceImpl#delete", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in ProjectDataSheetServiceImpl#delete", "400");
        logger.info("delete projectDataSheet of project with id '{}' in ProjectDataSheetServiceImpl#delete", projectId);
        final ProjectDataSheet projectDataSheet = projectDataSheetRepository.findByProjectId(projectId);
        if (projectDataSheet == null) {
            return;
        }
        projectDataSheetRepository.delete(projectDataSheet.getId());
    }
}
