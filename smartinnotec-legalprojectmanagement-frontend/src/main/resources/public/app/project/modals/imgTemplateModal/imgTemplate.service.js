(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.project')
    	.factory('imgTemplateModalService', imgTemplateModalService);
        
    imgTemplateModalService.$inject = ['$modal', '$stateParams'];
    
    function imgTemplateModalService($modal, $stateParams) {
		var service = {
			showImgTemplateModal: showImgTemplateModal
		};		
		return service;
		
		////////////
				
		function showImgTemplateModal(url, documentName, ending, version) {
			 var imgTemplateModal = $modal.open({
				controller: ImgTemplateModalController,
			    controllerAs: 'vm',
			    windowClass: "modal fade in",
			    resolve: {
			    	url: function() {
			    		return url;
			    	},
			    	documentName: function() {
			    		return documentName;
			    	},
			    	ending: function() {
			    		return ending;
			    	},
			    	version: function() {
			    		return version;
			    	}
			    }, 
				templateUrl: 'app/project/modals/imgTemplateModal/imgTemplate.modal.html'
			});
			return imgTemplateModal;
			
			function ImgTemplateModalController($modalInstance, $scope, url, documentName, ending, version) {
		    	var vm = this; 
		    	vm.url = url;
		    	vm.documentName = documentName;
		    	vm.ending = ending;
		    	vm.version = version;
		    	
		    	vm.closeImgTemplateModal = closeImgTemplateModal;
		    	
		    	////////////
		    	
		    	function closeImgTemplateModal() {
					$modalInstance.dismiss('cancel');
				}
			} 
		}
    }
})();
