(function() {
    'use strict';

    angular
    	.module('legalprojectmanagement.superadministration')
    	.config(configure);
    	
    configure.$inject = ['$stateProvider']; 
    
    function configure($stateProvider) {
    	
    	 $stateProvider
	    	.state(getSuperAdministrationState());
    	 
    	////////////
			    	
    	function getSuperAdministrationState() {
    		var state = {
    			name: 'auth.superadministration',
				url: '/superadministration/:userId',
				templateUrl: 'app/superadministration/superadministration/superadministration.html',
				controller: 'SuperAdministrationController',
				controllerAs: 'vm',
				resolve: {
					superAdministrationService: 'superAdministrationService',
					thinningPolymerDateTimeValueService: 'thinningPolymerDateTimeValueService',
					tenants: function findAllTenants(superAdministrationService) {
						return superAdministrationService.findAllTenants();
					}, 
					thinningPolymerDateTimeValues: function getAll(thinningPolymerDateTimeValueService) {
						return thinningPolymerDateTimeValueService.findAll();
					}, 
					currentThinningPolymerDateTimeValue: function findThinningPolymerDateTimeValueOfDate(thinningPolymerDateTimeValueService) {
						var date = new Date();
						var dateString = date.getDate() + '.' + (date.getMonth()+1) + '.' + date.getFullYear();
 						return thinningPolymerDateTimeValueService.findThinningPolymerDateTimeValueOfDate(dateString);
					}
				}
    		};
    		return state;
    	}
	}
})();