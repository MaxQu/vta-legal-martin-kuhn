(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.reporting')
    	.controller('ReportingController', ReportingController);
    
    ReportingController.$inject = ['$scope', 'dateUtilityService', 'reportingService', 'userAuthorizationUserContainer', 'calendarService', 'optionsService', 'userService', 'contactAndUserService'];
     
    function ReportingController($scope, dateUtilityService, reportingService, userAuthorizationUserContainer, calendarService, optionsService, userService, contactAndUserService) {
	    $scope.vm = this; 
    	var vm = this; 
    	
    	vm.userAuthorizationUserContainer = userAuthorizationUserContainer.data;
    	vm.from = new Date();
    	vm.until = new Date();
    	vm.selectedFacilityDetailsAndActivitiesUser = null;
    	vm.selectedFacilityDetailsAndActivitiesContact = null;
    	vm.selectedFacilityDetailsAndActivitiesCalendarEvent = null;
    	vm.activities = [];
    	vm.facilityDetails = [];
    	vm.facilityDetailsTypes = [];
    	vm.reportingContainers = [];
    	vm.activityTypes = [];
    	vm.calendarEvents = [];
    	vm.fromTrigger = false;
    	vm.untilTrigger = false;
    	vm.loadingTrigger = false;
    	
    	vm.openFrom = openFrom;
    	vm.openUntil = openUntil;
    	vm.loadActivityTypes = loadActivityTypes;
    	vm.getActivitiesBySearchCriteria = getActivitiesBySearchCriteria;
    	vm.getFacilityDetailsBySearchCriteria = getFacilityDetailsBySearchCriteria;
    	vm.searchCalendarEvents = searchCalendarEvents;
    	vm.search = search;
    	vm.findUsersOrContactsBySearchString = findUsersOrContactsBySearchString;
    	loadActivityTypes();
    	loadFacilityDetailTypes();
    	
    	////////////
    	
    	function openFrom($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.fromTrigger = true;
		}
    	
    	function openUntil($event) {
			$event.preventDefault();
			$event.stopPropagation();
			vm.untilTrigger = true;
		}
    	
    	function loadActivityTypes() {     
    		optionsService.getActivityTypes('all').then(function(response) {
    			vm.activityTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#loadActivityTypes');
  	   		 });
    	}
    	
    	function loadFacilityDetailTypes() {     
    		optionsService.getFacilityDetailsTypes().then(function(response) {
    			vm.facilityDetailsTypes = response.data;
  	   		 }, function errorCallback(response) {
  	   			  console.log('error administration.controller#loadFacilityDetailTypes');
  	   		 });
    	}
    	
    	function findUsersOrContactsBySearchString(searchString) { 
    		return contactAndUserService.findContactsAndUsersBySearchString(searchString).then(function (response) {
    			return response.data;
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findContactBySearchString: ' + data);
			});
    	}
    	
    	function search() {
    		if(vm.selectedUser == null) {
    			return;
    		}
    		vm.loadingTrigger = true;
    		vm.reportingContainers = [];
    		var start = dateUtilityService.formatDateToString(vm.from);
    		var end = dateUtilityService.formatDateToString(vm.until);
    		var reportingSearchCriteria = {};
    		if(vm.selectedUser.contactUserType == 'USER') {
    			reportingSearchCriteria.userId = vm.selectedUser.id;
    			reportingSearchCriteria.contactId = null; 
    		} else if (vm.selectedUser.contactUserType == 'CONTACT') {
    			reportingSearchCriteria.contactId = vm.selectedUser.id;
    			reportingSearchCriteria.userId = null;
    		}
    		reportingService.findByReportingSearchCriteria(start, end, reportingSearchCriteria).then(function (response) {
    			vm.reportingContainers = response.data;
    			for(var i = 0; i < vm.reportingContainers.length; i++) {
    				vm.reportingContainers[i].collapse = false;
    			}
    			vm.loadingTrigger = false;
    		}).catch(function (data) {
				console.log('error project.controller#contactSearch#findUsersOrContactsBySearchString: ' + data);
			});
    	}
    	
    	function searchCalendarEvents() {
    		vm.loadingTrigger = true;
    		var start = dateUtilityService.formatDateToString(vm.from);
    		var end = dateUtilityService.formatDateToString(vm.until);
     		
     		var calendarEventSearchCriteriaContainer = {};
     		calendarEventSearchCriteriaContainer.start = start;
     		calendarEventSearchCriteriaContainer.end = end;
     		calendarEventSearchCriteriaContainer.userId = vm.selectedFacilityDetailsAndActivitiesUser != null ? vm.selectedFacilityDetailsAndActivitiesUser.id : null;
     		calendarEventSearchCriteriaContainer.calendarEventType = vm.selectedCalendarEventType != null ? vm.selectedCalendarEventType : null;
     		
    		calendarService.findCalendarEventOfCriteria(calendarEventSearchCriteriaContainer).then(function(response) {
    			vm.calendarEvents = response.data;
    			vm.loadingTrigger = false;
	    	}).catch(function (data) {
				console.log('error calendarEventInList.service#searchCalendarEvents: ' + data);
	    	});
    	}
    	
    	function getActivitiesBySearchCriteria() {
    		vm.loadingTrigger = true;
    		var start = dateUtilityService.formatDateToString(vm.from);
    		var end = dateUtilityService.formatDateToString(vm.until);
    		var reportingSearchCriteria = {};
    		reportingSearchCriteria.userId = vm.selectedFacilityDetailsAndActivitiesUser != null ? vm.selectedFacilityDetailsAndActivitiesUser.id : null;
    		reportingSearchCriteria.calendarEventId = vm.selectedFacilityDetailsAndActivitiesCalendarEvent != null ? vm.selectedFacilityDetailsAndActivitiesCalendarEvent.id : null;
    		reportingSearchCriteria.contactId = vm.selectedFacilityDetailsAndActivitiesContact != null ? vm.selectedFacilityDetailsAndActivitiesContact.id : null;
    		reportingService.getActivitiesBySearchCriteria(start, end, reportingSearchCriteria).then(function(response) {
    			vm.activities = response.data;
    			vm.loadingTrigger = false;
	   		 }, function errorCallback(response) {
	   			console.log('error reporting.controller#getActivitiesBySearchCriteria');
	   		 });
    	}
    	
    	function getFacilityDetailsBySearchCriteria() {
    		vm.loadingTrigger = true;
    		var start = dateUtilityService.formatDateToString(vm.from);
    		var end = dateUtilityService.formatDateToString(vm.until);
    		var reportingSearchCriteria = {};
    		reportingSearchCriteria.userId = vm.selectedFacilityDetailsAndActivitiesUser != null ? vm.selectedFacilityDetailsAndActivitiesUser.id : null;
    		reportingSearchCriteria.calendarEventId = vm.selectedFacilityDetailsAndActivitiesCalendarEvent != null ? vm.selectedFacilityDetailsAndActivitiesCalendarEvent.id : null;
    		reportingSearchCriteria.contactId = vm.selectedFacilityDetailsAndActivitiesContact != null ? vm.selectedFacilityDetailsAndActivitiesContact.id : null;
    		reportingService.getFacilityDetailsBySearchCriteria(start, end, reportingSearchCriteria).then(function(response) {
    			vm.facilityDetails = response.data;
    			vm.loadingTrigger = false;
	   		 }, function errorCallback(response) {
	   			console.log('error reporting.controller#getFacilityDetailsBySearchCriteria');
	   		 });
    	}
    }
})();