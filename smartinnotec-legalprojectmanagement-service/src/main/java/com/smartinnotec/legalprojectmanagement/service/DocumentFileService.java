package com.smartinnotec.legalprojectmanagement.service;

import java.util.List;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileParentTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileVersion;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProjectUserConnectionRoleEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;

public interface DocumentFileService {

    Integer count() throws BusinessException;

    List<DocumentFile> findAll() throws BusinessException;

    DocumentFile create(final DocumentFile documentFile) throws BusinessException;

    DocumentFile update(final DocumentFile documentFile) throws BusinessException;

    DocumentFile update(final DocumentFile documentFile, final Project project) throws BusinessException;

    void delete(final String documentFileId, final String username) throws BusinessException;

    DocumentFile findById(final String id) throws BusinessException;

    Integer getDocumentFilePageSize() throws BusinessException;

    DocumentFile findByProjectIdAndFileName(final String projectId, final String fileName) throws BusinessException;

    List<DocumentFile> findByProjectProductIdAndUserIdBlackListNotInAndConfidentAndActive(final String projectProductId,
            final String userId, final boolean confident, final boolean active)
            throws BusinessException;

    DocumentFile findByProjectIdAndFileNameAndFolderId(final String projectId, final String fileName, final String folderId)
            throws BusinessException;

    List<DocumentFile> findPagedDocumentFilesByProjectIdAndFolderId(final String projectId, final String folderId, final String userId,
            final Integer page, final String sort, final DocumentFileParentTypeEnum documentFileParentType)
            throws BusinessException;

    List<DocumentFile> findAssignedDocumentFilesByProjectId(final String projectId, final String userId) throws BusinessException;

    List<DocumentFile> findPagedAndConfidentDocumentFiles(final String projectId, final String folderId, final String userId,
            final boolean confident, final Integer page, final String sort)
            throws BusinessException;

    List<DocumentFile> removeBlackListedDocumentFiles(final List<DocumentFile> documentFiles, final String userId) throws BusinessException;

    List<DocumentFile> removeBlackListedRolesDocumentFiles(final List<DocumentFile> documentFiles,
            final ProjectUserConnectionRoleEnum projectUserConnectionRole)
            throws BusinessException;

    Integer countByProjectProductIdGroupByFileName(final String projectId, final String userId, final Boolean onlyConfident,
            final DocumentFileParentTypeEnum documentFileParentType)
            throws BusinessException;

    Integer countByProjectIdAndFolderIdGroupByFileName(final String projectId, final String folderId, final String userId,
            final Boolean onlyConfident, final DocumentFileParentTypeEnum documentFileParentType)
            throws BusinessException;

    DocumentFileVersion findDocumentFileVersion(final DocumentFile documentFile, final String version) throws BusinessException;

    boolean renameUpdatedFile(final DocumentFile currentDocumentFile, final DocumentFile newDocumentFile) throws BusinessException;

    boolean renameDeletionFile(final String documentFileId, final String username) throws BusinessException;

    List<DocumentFile> findDocumentFilesBySearchString(final User user, final String searchString) throws BusinessException;

    List<DocumentFile> findDocumentFilesBySearchStringOverAllDocuments(final User user, String searchString) throws BusinessException;

    DocumentFileVersion getDocumentFileVersionOfLastVersion(final DocumentFile documentFile) throws BusinessException;

    List<DocumentFile> findByProjectId(final String projectId) throws BusinessException;

    List<DocumentFile> findBySearchStringAndProjectIdAndFolderId(final User user, final String searchString, final String projectId,
            final String folderId)
            throws BusinessException;

    List<DocumentFile> findBySearchString(final String searchString, final User user) throws BusinessException;

    String getFilePathName(final String subFilePath, final String filePathName);

    Integer calculateAmountOfPages(final String projectId, final String userId, final String folderId,
            final DocumentFileParentTypeEnum documentFileParentType)
            throws BusinessException;

}
