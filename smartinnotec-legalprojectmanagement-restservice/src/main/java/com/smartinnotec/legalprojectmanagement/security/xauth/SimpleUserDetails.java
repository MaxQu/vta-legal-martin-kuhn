package com.smartinnotec.legalprojectmanagement.security.xauth;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.smartinnotec.legalprojectmanagement.dao.domain.User;

@SuppressWarnings("serial")
public class SimpleUserDetails implements UserDetails {

    private String id;
    private String username;
    private String password;
    private boolean enabled = true;
    private User user;
    private Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();

    public SimpleUserDetails(final String id, final String username, final String pw, final User user, final String... extraRoles) {
        this.id = id;
        this.username = username;
        this.password = pw;
        this.user = user;
        // setup roles
        final Set<String> roles = new HashSet<String>();
        roles.addAll(Arrays.<String> asList(null == extraRoles ? new String[0] : extraRoles));
        // export them as part of authorities
        for (final String r : roles) {
            authorities.add(new SimpleGrantedAuthority(role(r)));
        }
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.enabled;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.enabled;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    private String role(final String i) {
        return "ROLE_" + i;
    }

    public User getUser() {
        return user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    public String toString() {
        return "{enabled:" + isEnabled() + ", id: '" + id + "', username:'" + getUsername() + "', password:'" + getPassword() + "'}";
    }
}
