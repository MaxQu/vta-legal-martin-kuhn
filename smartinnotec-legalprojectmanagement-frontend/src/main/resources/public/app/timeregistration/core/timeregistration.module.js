(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.timeregistration', [
    	    'legalprojectmanagement.common',
    	    'ui.router', 
    		'ui.bootstrap',
    		'xeditable'
        ]);
})();