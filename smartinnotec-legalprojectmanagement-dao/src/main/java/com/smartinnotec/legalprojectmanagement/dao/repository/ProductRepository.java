package com.smartinnotec.legalprojectmanagement.dao.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Tenant;

public interface ProductRepository extends MongoRepository<Product, String> {

    List<Product> findByTenant(final Tenant tenant);

    @Query("{$or : [{'name': { $regex: ?0, $options:'i' }}, {'description': { $regex: ?0, $options:'i' }}, {'names': { $regex: ?0, $options:'i' }}]}")
    List<Product> findProductByRegexString(final String regexString);

    @Query("{$text : { $search : ?0 }}")
    List<Product> findProductBySearchString(final String searchString);

    List<Product> findPagedProductsByTenant(final Tenant tenant, final Pageable pageable);

    Long countByTenant(final Tenant tenant);

    Long countById(final String id);

    List<Product> getByNameEquals(final String name);

    List<Product> getByNumberEquals(final String number);

}
