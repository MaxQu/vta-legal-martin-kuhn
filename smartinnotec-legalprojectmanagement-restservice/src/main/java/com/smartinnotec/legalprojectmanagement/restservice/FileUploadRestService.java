package com.smartinnotec.legalprojectmanagement.restservice;

import java.io.File;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.core.util.BusinessAssert;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFile;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileParentTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.DocumentFileVersion;
import com.smartinnotec.legalprojectmanagement.dao.domain.History;
import com.smartinnotec.legalprojectmanagement.dao.domain.HistoryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.Product;
import com.smartinnotec.legalprojectmanagement.dao.domain.Project;
import com.smartinnotec.legalprojectmanagement.dao.domain.User;
import com.smartinnotec.legalprojectmanagement.dao.domain.WorkingBookAttachment;
import com.smartinnotec.legalprojectmanagement.restservice.container.SystemImportContainer;
import com.smartinnotec.legalprojectmanagement.restservice.container.SystemImportUploadFileTypeEnum;
import com.smartinnotec.legalprojectmanagement.service.DocumentFileConfigurationBean;
import com.smartinnotec.legalprojectmanagement.service.DocumentFileService;
import com.smartinnotec.legalprojectmanagement.service.FileUploadService;
import com.smartinnotec.legalprojectmanagement.service.HistoryService;
import com.smartinnotec.legalprojectmanagement.service.ProductService;
import com.smartinnotec.legalprojectmanagement.service.ProjectService;
import com.smartinnotec.legalprojectmanagement.service.UserService;

@RestController
public class FileUploadRestService extends AbstractRestService {

    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProductService productService;
    @Autowired
    private FileUploadService fileUploadService;
    @Autowired
    private DocumentFileService documentFileService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private UserService userService;
    @Autowired
    private DocumentFileConfigurationBean documentFileConfigurationBean;

    @RequestMapping(value = "/fileuploads/fileupload/{userid}/{projectid}/{documentfileparenttype}", method = RequestMethod.POST)
    public synchronized DocumentFile filesUpload(@PathVariable(value = "userid")
    final String userId, @PathVariable(value = "projectid")
    final String projectId, @PathVariable(value = "documentfileparenttype")
    final DocumentFileParentTypeEnum documentFileParentType, @RequestParam(value = "superfolderid", required = false) String superFolderId,
            @RequestParam("file") MultipartFile multipartFile)
            throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in FileUploadRestService#filesUpload", "400");
        BusinessAssert.isId(userId, "userId must be an id in FileUploadRestService#filesUpload", "400");
        BusinessAssert.notNull(projectId, "projectId is mandatory in FileUploadRestService#filesUpload", "400");
        BusinessAssert.isId(projectId, "userId must be an id in FileUploadRestService#filesUpload", "400");
        BusinessAssert.notNull(documentFileParentType, "documentFileParentType is mandatory in FileUploadRestService#filesUpload", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": upload files for user with id '{}' and project with id '{}' in FileUploadRestService#filesUpload", userId, projectId);

        Project project = null;
        Product product = null;
        switch (documentFileParentType) {
        case PRODUCT:
            product = productService.findProduct(projectId);
            break;
        case PROJECT:
            project = projectService.findProject(projectId);
            break;
        }

        final String fileName = multipartFile.getOriginalFilename();
        final String[] fileNameParts = fileName.split("\\.");
        final String ending = fileNameParts[fileNameParts.length - 1];

        if (!multipartFile.isEmpty()) {
            DocumentFile documentFile = documentFileService.findByProjectIdAndFileNameAndFolderId(projectId, fileName, superFolderId);
            final boolean documentFileStillExists = documentFile != null ? true : false;

            Long amount = 1L;
            DocumentFileVersion documentFileVersionWithLatestActiveVersion = null;
            if (documentFile == null) {
                documentFile = new DocumentFile();
                documentFile.setFileName(fileName);
                documentFile.setEnding(ending.toLowerCase());
                documentFile.setProjectProductId(project != null ? project.getId() : product.getId());
                documentFile.setFolderId(superFolderId);
            } else {
                amount = (long)documentFile.getDocumentFileVersions().size() + 1;
                documentFileVersionWithLatestActiveVersion = documentFileService.getDocumentFileVersionOfLastVersion(documentFile);
            }

            final String fileNameWithVersion = prepareFileName(fileName, superFolderId, amount);
            final String storedFilePath = fileUploadService.uploadFile(project != null ? project.getName() : product.getName(),
                fileNameWithVersion, multipartFile);
            logger.info("stored file under path '{}' in FileUploadRestService#fileUpload", storedFilePath);

            final String filePathWithoutExtension = storedFilePath.substring(0, storedFilePath.lastIndexOf("."));
            final String subFilePath = filePathWithoutExtension.substring(0, filePathWithoutExtension.lastIndexOf(File.separator));
            documentFile.setSubFilePath(subFilePath);

            final User user = userService.findUser(userId);

            final DocumentFileVersion documentFileVersion = new DocumentFileVersion();
            documentFileVersion.setFilePathName(fileNameWithVersion);
            documentFileVersion.setUserUploadedId(user.getId());
            documentFileVersion.setVersion(documentFileConfigurationBean.getVersionPrefix() + amount);
            documentFileVersion.setLabels(
                documentFileVersionWithLatestActiveVersion != null ? documentFileVersionWithLatestActiveVersion.getLabels() : null);
            documentFileVersion.setUploadTime(new DateTime(System.currentTimeMillis()));
            documentFileVersion.setSize(multipartFile.getSize());

            documentFile.addDocumentFileVersion(documentFileVersion);

            if (documentFileStillExists) {
                final DocumentFile updatedDocumentFile = documentFileService.update(documentFile);
                createHistory(updatedDocumentFile, documentFileVersion, user, project != null ? project.getName() : product.getName(),
                    HistoryTypeEnum.NEW_VERSION_UPLOADED);
            } else {
                final DocumentFile createdDocumentFile = documentFileService.create(documentFile);
                createHistory(createdDocumentFile, documentFileVersion, user, project != null ? project.getName() : product.getName(),
                    HistoryTypeEnum.NEW_FILE_UPLOADED);
            }

            switch (documentFileParentType) {
            case PRODUCT:
                final Product updatedProduct = productService.update(product);
                logger.debug("updated product with id '{}' in FileUploadRestService#filesUpload", updatedProduct.getId());
                break;
            case PROJECT:
                final Project updatedProject = projectService.update(project);
                logger.debug("updated project with id '{}' in FileUploadRestService#filesUpload", updatedProject.getId());
                break;
            }

            return documentFile;
        }
        return null;
    }

    @RequestMapping(value = "/fileuploads/fileupload/documentversion/{userid}/{projectid}/{documentfileid}", method = RequestMethod.POST)
    public synchronized DocumentFile uploadNewDocumentVersion(@PathVariable(value = "userid")
    final String userId, @PathVariable(value = "projectid")
    final String projectId, @PathVariable(value = "documentfileid")
    final String documentFileId, @RequestParam("file") MultipartFile multipartFile) throws BusinessException {
        BusinessAssert.notNull(userId, "userId is mandatory in FileUploadRestService#uploadNewDocumentVersion", "400");
        BusinessAssert.isId(userId, "userId must be an id in FileUploadRestService#uploadNewDocumentVersion", "400");
        BusinessAssert.notNull(projectId, "projectId is mandatory in FileUploadRestService#uploadNewDocumentVersion", "400");
        BusinessAssert.isId(projectId, "projectId must be an id in FileUploadRestService#uploadNewDocumentVersion", "400");
        BusinessAssert.notNull(documentFileId, "documentFileId is mandatory in FileUploadRestService#uploadNewDocumentVersion", "400");
        BusinessAssert.isId(documentFileId, "documentFileId must be an id in FileUploadRestService#uploadNewDocumentVersion", "400");
        logger.info(getInvokedRestServiceUserInformation()
            + ": upload new document version for user with id '{}', project with id '{}' and document file with id '{}' in FileUploadRestService#uploadNewDocumentVersion",
            userId, projectId, documentFileId);

        final Project project = projectService.findProject(projectId);
        final Product product = productService.findProduct(projectId);
        final String folderName = project != null ? project.getName() : product.getName();
        final DocumentFile documentFile = documentFileService.findById(documentFileId);
        final DocumentFileVersion documentFileVersionWithLatestActiveVersion = documentFileService
            .getDocumentFileVersionOfLastVersion(documentFile);
        final Long amount = (long)documentFile.getDocumentFileVersions().size() + 1;
        final String fileName = documentFile.getFileName();

        final String fileNameWithVersion = this.prepareFileName(fileName, documentFile.getFolderId(), amount);
        final String storedFilePath = fileUploadService.uploadFile(folderName, fileNameWithVersion, multipartFile);
        logger.info("stored file under path '{}' in FileUploadRestService#uploadNewDocumentVersion", storedFilePath);

        final User user = userService.findUser(userId);

        final DocumentFileVersion documentFileVersion = new DocumentFileVersion();
        documentFileVersion.setFilePathName(fileNameWithVersion);
        documentFileVersion.setUserUploadedId(user.getId());
        documentFileVersion.setVersion(documentFileConfigurationBean.getVersionPrefix() + amount);
        documentFileVersion
            .setLabels(documentFileVersionWithLatestActiveVersion != null ? documentFileVersionWithLatestActiveVersion.getLabels() : null);
        documentFileVersion.setUploadTime(new DateTime(System.currentTimeMillis()));

        documentFile.addDocumentFileVersion(documentFileVersion);

        final DocumentFile updatedDocumentFile = documentFileService.update(documentFile);
        createHistory(updatedDocumentFile, documentFileVersion, user, folderName, HistoryTypeEnum.NEW_VERSION_UPLOADED);

        logger.debug("added new version to document file with id '{}' in FileUploadRestService#uploadNewDocumentVersion",
            updatedDocumentFile.getId());
        return updatedDocumentFile;
    }

    @RequestMapping(value = "/fileuploads/fileupload/{userid}/systemimport/{systemimportuploadfiletype}", method = RequestMethod.POST)
    public synchronized SystemImportContainer uploadSystemImportContactFile(@PathVariable(value = "userid")
    final String userId, @PathVariable(value = "systemimportuploadfiletype")
    final SystemImportUploadFileTypeEnum systemImportUploadFileType, @RequestParam("file") MultipartFile multipartFile)
            throws BusinessException {
        if (!multipartFile.isEmpty()) {
            final String fileName = multipartFile.getOriginalFilename();
            final String formattedDateTime = dateTimeFormatterWithoutSeparator.print(System.currentTimeMillis());
            final String formattedFilename = formattedDateTime + "_" + fileName;

            final String storedFilePath = fileUploadService.uploadFile(SYSTEM_IMPORT_CONTACT_FOLDER, formattedFilename, multipartFile);
            logger.info(getInvokedRestServiceUserInformation()
                + ": uploaded file with fileName '{}' and changed to fileName '{}' to path '{}' in FileUploadRestService#uploadSystemImportContactFile",
                fileName, formattedFilename, storedFilePath);
            final SystemImportContainer systemImportContainer = new SystemImportContainer();
            switch (systemImportUploadFileType) {
            case ADDRESS_FILE:
                systemImportContainer.setPathAddress(storedFilePath);
                break;
            case CONTACT_FILE:
                systemImportContainer.setPathContact(storedFilePath);
                break;
            }
            return systemImportContainer;
        }
        return null;
    }

    @RequestMapping(value = "/fileuploads/fileupload/communication", method = RequestMethod.POST)
    public synchronized WorkingBookAttachment uploadcommunicationFile(@RequestParam("file") MultipartFile multipartFile)
            throws BusinessException {
        if (!multipartFile.isEmpty()) {
            final String fileName = multipartFile.getOriginalFilename();
            final String formattedDateTime = dateTimeFormatterWithoutSeparator.print(System.currentTimeMillis());
            final String formattedFilename = formattedDateTime + "_" + fileName;

            final String storedFilePath = fileUploadService.uploadFile(COMMUNICATION_FOLDER, formattedFilename, multipartFile);
            logger.info(getInvokedRestServiceUserInformation()
                + ": uploaded file with fileName '{}' and changed to fileName '{}' to path '{}' in FileUploadRestService#uploadcommunicationFile",
                fileName, formattedFilename, storedFilePath);
            final WorkingBookAttachment workingBookAttachment = new WorkingBookAttachment();
            workingBookAttachment.setFilepath(storedFilePath);
            workingBookAttachment.setFilename(formattedFilename);
            workingBookAttachment.setOriginalFilename(fileName);
            workingBookAttachment.setImage(fileUploadService.isImage(new File(storedFilePath)));
            return workingBookAttachment;
        }
        return null;
    }

    public String prepareFileName(final String fileName, final String folderId, final Long versionNumber) {
        final String[] fileNameParts = fileName.split("\\.");
        final String ending = fileNameParts[fileNameParts.length - 1];

        final StringBuilder preparedFileName = new StringBuilder();
        for (int i = 0; i < fileNameParts.length - 1; i++) {
            preparedFileName.append(fileNameParts[i]).append(i != (fileNameParts.length - 2) ? "." : "");
        }
        preparedFileName.append(documentFileConfigurationBean.getFolderIdPrefix());
        preparedFileName.append(folderId);
        preparedFileName.append(documentFileConfigurationBean.getVersionPrefix());
        preparedFileName.append(versionNumber);
        preparedFileName.append(".").append(ending);
        return preparedFileName.toString();
    }

    private void createHistory(final DocumentFile documentFile, final DocumentFileVersion documentFileVersion, final User user,
            final String name, final HistoryTypeEnum historyType)
            throws BusinessException {
        final String filePath = documentFileService.getFilePathName(documentFile.getSubFilePath(), documentFileVersion.getFilePathName());

        final String fileInfo = documentFile.getFileName() + " "
            + documentFileVersion.getVersion().substring(1, documentFileVersion.getVersion().length());
        final History history = new History.HistoryBuilder().setHistoryType(historyType).setProjectName(name)
            .setDate(new DateTime(System.currentTimeMillis())).setActionDate(documentFileVersion.getUploadTime()).setMessage(fileInfo)
            .setUser(user).setFilePath(filePath).setTenant(user.getTenant()).build();
        historyService.create(history);
    }

    @Override
    public String toString() {
        return "[FileUploadRestService]";
    }
}
