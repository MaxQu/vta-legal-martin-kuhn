package com.smartinnotec.legalprojectmanagement.service;

import com.smartinnotec.legalprojectmanagement.core.system.exception.BusinessException;
import com.smartinnotec.legalprojectmanagement.dao.domain.ContactTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.CountryTypeEnum;
import com.smartinnotec.legalprojectmanagement.dao.domain.ProvinceTypeEnum;

public interface I18NService {

    String translateCountry(final String country) throws BusinessException;

    String translateProvinceType(final ProvinceTypeEnum provinceType) throws BusinessException;

    CountryTypeEnum getCountryTypeOfIntSign(final String intSign) throws BusinessException;

    String getIntSignOfCountryType(final CountryTypeEnum countryType) throws BusinessException;

    ContactTypeEnum getContactTypeOfString(final String searchString) throws BusinessException;
}
