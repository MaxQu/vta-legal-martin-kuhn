(function() {
    'use strict';
    
    angular
    	.module('legalprojectmanagement.common')
    	.directive('numberDrop', numberDrop);
    
	function numberDrop() {
		function getRange(start, offset, range) {
	        var array = [];
	        for (var i = 0; i < range + 1; i++) {
	            array.push(start + offset + i);
	        } 
	        return array;
	    }
		var directive = {
			restrict: 'A',
	        scope: {
	            bindModel: '=',
	            start: '=',
	            range: '=',
	            offset: '='
	        },
	        link: function(scope,element,attrs) {
	            scope.array = getRange(+scope.start, +scope.offset, +scope.range);
	        },
	        template: '<select ng-model="bindModel" ng-options="y for y in array"></select>'
		};
		return directive;	
	}
})();