(function() {
    'use strict';
     
    angular
    	.module('legalprojectmanagement.common')
    	.run(runBlock);
    
    runBlock.$inject = ['$http', '$templateCache'];
    
    function runBlock($http, $templateCache) {
    	$http.get('app/common/as-form-group/as-form-group.directive.html', { cache: $templateCache });
    }
})();